<?php
class Product extends MY_Controller {

	function Product()
	{
		session_start();
		parent::MY_Controller();
	}

	function manage($id='')
	{

		$this->template->metas($metas);

		$data = array();
		$resultDisplay = '';
		$status = '';
		$error = '';

		$product_manage_form= '';
		if ($this->input->server('REQUEST_METHOD') == 'GET') {
			$product_manage_form= !empty($_POST['product_manage_form']) ? $_POST['product_manage_form'] : "";
		}

		if ($this->input->server('REQUEST_METHOD') == 'POST') {

			$data = array(
				'cat'           => $this->input->post('cat')
			,'type'          => $this->input->post('type')
			,'color'         => $this->input->post('color')
			,'retail'        => $this->input->post('retail')
			,'saleprice'    => $this->input->post('saleprice')
                ...
			,'wholesale'    => $this->input->post('wholesale')
			,'purch'        => $this->input->post('purch_date')
			,'weight'       => $this->input->post('weight')
			,'volume'       => $this->input->post('volume')
			);

			if($_POST['subType'] == "edit")
			{
				$data['invid'] = $this->input->post('invid');
				$data['clearAmt'] = $this->input->post('clearAmt');

				$this->db->where('invid', $this->input->post('invid'));
				$this->db->update('inven_mj2', $data);

			}
			elseif($_POST['subType'] == "new")
			{
				$data['clearAmt'] = $this->input->post('clearAmt');
				$this->db->insert('inven_mj2', $data);
			}
			elseif($_POST['subType'] == 'clearance')
			{
				$data['clearAmt'] = $this->input->post('invamount');
				$this->db->insert('inven_mj2', $data);
			}

			$resultDisplay = "showHide('result','prodType');";
			$status = "Success:";
			$error = "Your posting has succeeded";

		}

		if(!isset($_SESSION['cleanup']) OR $_SESSION['cleanup'] != true){
			$this->db->simple_query("DELETE FROM inven_mj2 WHERE (clearAmt = 0 OR isNull(clearAmt)) AND invid != 5002 AND invid != 5001 AND cat = 'clearance'"); //5001 and 5002 reserved for grab bags
			$_SESSION['cleanup'] = true;
		}
		$data['resultDisplay'] = $resultDisplay;
		$data['status'] = $status;
		$data['error'] = $error;
		$data['product_manage_form'] = $product_manage_form;

		if ($id == "orig") {
			$this->template->display('inventory/product_orig', $data, '3column');
		} else {
		  $this->template->display('inventory/product', $data, '3column');
		}



	}


	function get_product( $id, $result= 'json' ) {
		$product_struct= $this->product_model->getProductById($id);
		header('Content-type: application/json');
		echo json_encode(array('ErrorMessage' => '', 'ErrorCode' => 0, 'product_struct'=> $product_struct ) );
	}


	function get_images_list( $id, $session_id ) {
		$config =& get_config();
		$config_images_ext= $config['images_ext'];
		if ( strtolower($id)== "new") {
			$config_uploads_products_dir= '/uploads/temp_products/product-' . $session_id;
		} else {
	  	$config_uploads_products_dir= '/uploads/products/product-' . $id;
		}
		$config_images_base_url= 'http://www.site.com/testing/';
		$this->load->model('product_model');

		if ( strtolower($id) == "new") {
			$product_images_array= array();
			$A= isset($_SESSION['newproduct_images_list']) ? $_SESSION['newproduct_images_list'] : array();
			foreach( $A as $key=> $product_image ) {
				$NewImage = new stdClass;
				if ( is_object($product_image) ) {
				  $NewImage->path= $product_image->path;
				  $NewImage->alt= $product_image->alt;
				}
				if ( is_array($product_image) ) {
					$NewImage->path= $product_image['path'];
					$NewImage->alt= $product_image['alt'];
				}
				$product_images_array[]= $NewImage;
			}
		} else {
			$product_images_array= $this->product_model->getProductById($id, "img_into_struct");
		}
		$this->template->display('inventory/get_images_list', array( 'product_images_array'=> $product_images_array, 'rows_count' => count($product_images_array),
		 'config_uploads_products_dir'=> $config_uploads_products_dir, 'config_images_base_url'=>$config_images_base_url, 'config_images_ext'=>$config_images_ext,	'minimal' ));
	}


	function save_image_path_alt( $id, $original_filename, $new_path_value, $new_alt_value, $session_id ) {
		$config =& get_config();
		$this->load->model('product_model');
		$config_images_ext= $config['images_ext'];

		if ( strtolower($id) == "new") {
			$config_uploads_products_dir= 'uploads/temp_products/product-' . $session_id;
			$product_images_array= isset($_SESSION['newproduct_images_list']) ? $_SESSION['newproduct_images_list'] : array();
		} else {
			$product_images_array= $this->product_model->getProductById($id, "img_into_struct");
			$config_uploads_products_dir= 'uploads/products/product-' . $id;
		}
		foreach( $product_images_array as $key=> $product_image ) {
			if ( is_object($product_image) ) {
  			if ( $product_image->path == urldecode($original_filename) ) {
	  			$product_images_array[$key]->path = urldecode($new_path_value);
					$product_images_array[$key]->alt = urldecode($new_alt_value);
		  		break;
				}
			}
			if ( is_array($product_image) ) {
				if ( $product_image['path'] == urldecode($original_filename) ) {
					$product_images_array[$key]['path'] = urldecode($new_path_value);
					$product_images_array[$key]['alt'] = urldecode($new_alt_value);
					break;
				}
			}
		}
		$DocumentRoot= $config['DocumentRoot'];
		$full_directory_path= $DocumentRoot . DIRECTORY_SEPARATOR . $config_uploads_products_dir. DIRECTORY_SEPARATOR;

		if ( strtolower($id) == "new") {
			$_SESSION['newproduct_images_list']= $product_images_array;
		} else {
			$Res= $this->product_model->UpdateProduct( $id, array('img'=>json_encode(  $product_images_array )) );
		}

		if ( file_exists($full_directory_path . urldecode($original_filename) . '.' . $config_images_ext) ) {
		  $Res= rename( $full_directory_path . urldecode($original_filename) . '.' . $config_images_ext,
			  $full_directory_path . urldecode($new_path_value) . '.' . $config_images_ext );
			$src_filename= urldecode($original_filename) . '.' . $config_images_ext;
			$dest_filename= urldecode($new_path_value) . '.' . $config_images_ext;
			if ( strtolower($id) != "new") { // Do not copy temporary files to ftp
			  $FtpRes= $this->RenameFileToFTPDir( $id, $full_directory_path , $src_filename, $dest_filename, $config_uploads_products_dir. DIRECTORY_SEPARATOR );
			}
		}

		$thumbs_sizes_list= $config['thumbs_sizes_list'];
		foreach( $thumbs_sizes_list as $thumbs_size_title => $thumbs_size ) {
			if ( file_exists($full_directory_path . urldecode($original_filename) . $this->getThumbnailExt( $thumbs_size['width'], $thumbs_size['height'] ) . '.' . $config_images_ext) ) {
			  $Res= rename( $full_directory_path . urldecode($original_filename) . $this->getThumbnailExt( $thumbs_size['width'], $thumbs_size['height'] ) . '.' . $config_images_ext,
				  $full_directory_path . urldecode($new_path_value) . $this->getThumbnailExt( $thumbs_size['width'], $thumbs_size['height'] ) . '.' . $config_images_ext );

				$src_filename= urldecode($original_filename) . $this->getThumbnailExt( $thumbs_size['width'], $thumbs_size['height'] ) . '.' . $config_images_ext;
				$dest_filename= urldecode($new_path_value) . $this->getThumbnailExt( $thumbs_size['width'], $thumbs_size['height'] ) . '.' . $config_images_ext;
				if ( strtolower($id) != "new") { // Do not copy temporary files to ftp
  				$FtpRes= $this->RenameFileToFTPDir( $id, $full_directory_path, $src_filename, $dest_filename, $config_uploads_products_dir. DIRECTORY_SEPARATOR );
				}

			}
		}

		header('Content-type: application/json');
		echo json_encode(array('ErrorMessage' => '', 'ErrorCode' => 0 ) );
	}

	function delete_image($id, $filename,  $session_id  ) {
		$config =& get_config();
		$this->load->model('product_model');
		$config_images_ext= $config['images_ext'];

		$product_images_array= array();
		if ( strtolower($id) == "new") {
			$Arr= array();
			$A= isset($_SESSION['newproduct_images_list']) ? $_SESSION['newproduct_images_list'] : array();
			foreach( $A as $key=> $product_image ) {
				$NewImage = new stdClass;
				if ( is_object($product_image) ) {
  				$NewImage->path= $product_image->path;
	  			$NewImage->alt= $product_image->alt;
				}
				if ( is_array($product_image) ) {
					$NewImage->path= $product_image['path'];
					$NewImage->alt= $product_image['alt'];
				}
				$Arr[]= $NewImage;
			}

		} else {
		  $Arr= $this->product_model->getProductById($id, "img_into_struct");
		}

		foreach( $Arr as $key=> $product_image ) {
			if ( $product_image->path != urldecode($filename) ) {
				$product_images_array[]= $product_image;
			}
		}
		if ( strtolower($id) == "new") {
			$_SESSION['newproduct_images_list']= $product_images_array;
		} else {
		  $Res= $this->product_model->UpdateProduct( $id, array('img'=>json_encode(  $product_images_array )) );
		}

		if ( strtolower($id) == "new") {
			$config_uploads_products_dir= 'uploads/temp_products/product-' . $session_id;
		} else {
		  $config_uploads_products_dir= 'uploads/products/product-' . $id;
		}
		$DocumentRoot= $config['DocumentRoot'];

		$full_directory_path= $DocumentRoot . DIRECTORY_SEPARATOR . $config_uploads_products_dir. DIRECTORY_SEPARATOR;
		if ( file_exists($full_directory_path . urldecode($filename) . '.' . $config_images_ext) ) {
		  $Res= @unlink( $full_directory_path . urldecode($filename) . '.' . $config_images_ext );
			if ( strtolower($id) != "new") {
			  $FtpRes= $this->DeleteFileFromFTPDir( $id, $full_directory_path , urldecode($filename) . '.' . $config_images_ext, $config_uploads_products_dir. DIRECTORY_SEPARATOR );
			}
		}

		$thumbs_sizes_list= $config['thumbs_sizes_list'];
		foreach( $thumbs_sizes_list as $thumbs_size_title => $thumbs_size ) {
			$DeletedFile= $full_directory_path . urldecode($filename) . $this->getThumbnailExt( $thumbs_size['width'], $thumbs_size['height'] ) . '.' . $config_images_ext;
			if ( file_exists($DeletedFile) ) {
			  $Res= @unlink( $DeletedFile );
				if ( strtolower($id) != "new") {
  				$FtpRes= $this->DeleteFileFromFTPDir( $id, $full_directory_path , urldecode($filename) . $this->getThumbnailExt( $thumbs_size['width'], $thumbs_size['height'] ) . '.' . $config_images_ext, $config_uploads_products_dir. DIRECTORY_SEPARATOR );
				}
			}
		}

		header('Content-type: application/json');
		echo json_encode(array('ErrorMessage' => '', 'ErrorCode' => 0 ) );
	}



function save_uploaded_image_path($id, $new_path_value, $session_id ) {
		$config =& get_config();
		$this->load->model('product_model');
		$A= preg_split('/\./',$new_path_value);
		if ( count($A)> 0 ) {
			$Res= '';
			for( $I= 0; $I< count($A) - 1; $I++ ) {
				$Res.= $A[$I] . ( $I< count($A) - 2 ? "." : "" ) ;
			}
			$new_path_value= $Res;
		}
	  if ( $id == "new" ) {
			$product_images_array= isset($_SESSION['newproduct_images_list']) ? $_SESSION['newproduct_images_list'] : array();
			$product_images_array[]= array( 'path'=> $new_path_value,	'alt'=> '' );
			$_SESSION['newproduct_images_list']= $product_images_array;
		} else {
		  $product_images_array= $this->product_model->getProductById($id, "img_into_struct");
		  $NewImage = new stdClass;
		  $NewImage->path= $new_path_value;
		  $NewImage->alt= '';
		  $product_images_array[]= $NewImage;
		  $Res= $this->product_model->UpdateProduct( $id, array('img'=>json_encode(  $product_images_array )) );
		}
		header('Content-type: application/json');
		echo json_encode(array('ErrorMessage' => '', 'ErrorCode' => 0 /*, '_SESSION'=>print_r($_SESSION,true) */ ) );

	}


	function create_images_thumbnails($id, $create_images_thumbnails, $session_id ) {
		set_time_limit(0);
		$create_images_thumbnails_array= json_decode( urldecode($create_images_thumbnails) );
		$config =& get_config();
		$thumbs_sizes_list= $config['thumbs_sizes_list'];
		$DocumentRoot= $config['DocumentRoot'];

		if ( strtolower($id) == "new") {
			$config_uploads_products_dir= 'uploads/temp_products/product-' . $session_id;
		} else {
			$config_uploads_products_dir= 'uploads/products/product-' . $id;
		}
		$full_directory_path= $DocumentRoot . DIRECTORY_SEPARATOR . $config_uploads_products_dir. DIRECTORY_SEPARATOR;

		$CreatedthumbnailsList= array();
		foreach( $create_images_thumbnails_array as $images_src_filename ) {
		  foreach( $thumbs_sizes_list as $thumbs_size_title => $thumbs_size ) {
				if ( strtolower($id) != "new") { // Do not copy temporary files to ftp
				  $FtpRes= $this->CopyFileToFTPDir( $id, $full_directory_path , $images_src_filename, $config_uploads_products_dir. DIRECTORY_SEPARATOR );
				}
				$CreateThumbnail= $this->CreateThumbnail ( $id, $full_directory_path, $images_src_filename, $thumbs_size['width'], $thumbs_size['height'], $config_uploads_products_dir );
				if ( !empty($CreateThumbnail) ) $CreatedthumbnailsList[]= $CreateThumbnail;
		  }
		}
		header('Content-type: application/json');
		echo json_encode(array('ErrorMessage' => '', 'ErrorCode' => 0, 'CreatedthumbnailsListLength'=> count($CreatedthumbnailsList) ) );

	}

	function CreateThumbnail ( $id, $full_directory_path, $images_src_filename, $thumbs_width, $thumbs_height, $config_uploads_products_dir ) { // http://www.php.net/manual/en/imagick.thumbnailimage.php
		$config =& get_config();
		$maxsize=$thumbs_height;
		if ( $maxsize > $thumbs_width  ) $maxsize= $thumbs_width;
		$A= preg_split('/\./',$images_src_filename);
		$base_filename= '';
		$ext_filename= '';
		if ( count($A)> 0 ) {
			$Res= '';
			for( $I= 0; $I< count($A) - 1; $I++ ) {
				$Res.= $A[$I] . ( $I< count($A) - 2 ? "." : "" ) ;
			}
			$base_filename= $Res;
			$ext_filename= $A[ count($A) - 1 ];
		}
		$dest_filename= $base_filename . $this->getThumbnailExt($thumbs_width, $thumbs_height) . '.'. $ext_filename;
		if (!file_exists($full_directory_path . $images_src_filename)) return false;

		$image = new Imagick( $full_directory_path . $images_src_filename ); // create new Imagick object

        ...

		$image->destroy();
		if ( strtolower($id) != "new") { // Do not copy temporary files to ftp
			$FtpRes= $this->CopyFileToFTPDir( $id, $full_directory_path , $dest_filename, $config_uploads_products_dir. DIRECTORY_SEPARATOR );
		}
		if ( $Res ) {
			return $full_directory_path . $dest_filename;
		}
	}

	private function getThumbnailExt( $thumbs_size_width, $thumbs_size_height ) {
		return '_'.$thumbs_size_width . 'x' . $thumbs_size_height;
	}

	private function CopyFileToFTPDir( $id, $src_full_directory_path , $src_filename, $dst_directory ) {
		restore_error_handler();
	
		$src = '/home/site/public_html/testing/uploads/products/product-' . $id . '/' . $src_filename;
		if ( !file_exists( $src ) ) {
			return false;
		}

		$config =& get_config();
		$ftp_server= $config['ftp_site'];
		$ftp_user_name= $config['ftp_user'];
		$ftp_user_pass= $config['ftp_pwd'];

        ...

		$res = ftp_put($conn_id, 'uploads/products/product-' . $id . '/' . $src_filename, $src, FTP_BINARY);
		ftp_close($conn_id);
		return $res !== FALSE;
	}

	private function RenameFileToFTPDir( $id, $src_full_directory_path , $src_filename, $dest_filename, $dst_directory ) {
		$config =& get_config();
		$ftp_server= $config['ftp_site'];
		$ftp_user_name= $config['ftp_user'];
		$ftp_user_pass= $config['ftp_pwd'];

		$conn_id = ftp_connect($ftp_server); // set up basic connection
        ...
		if (ftp_rename($conn_id, $dst_directory . $src_filename, $dst_directory . $dest_filename)) { // try to rename $old_file to $new_file
			$Res= true;
		} else {
			$Res= false;
		}
		ftp_close($conn_id);
		return $Res;  // close the connection
	}

	private function DeleteFileFromFTPDir( $id, $src_full_directory_path , $dest_filename, $dst_directory ) {
		$config =& get_config();
		$ftp_server= $config['ftp_site'];
		$ftp_user_name= $config['ftp_user'];
		$ftp_user_pass= $config['ftp_pwd'];

		$conn_id = ftp_connect($ftp_server); // set up basic connection
		$login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass); // login with username and password
        ...
		if (ftp_delete($conn_id, $dst_directory . $dest_filename)) {
			$Res= true;
		} else {
			$Res= false;
		}
		ftp_close($conn_id);
		return $Res;  // close the connection
	}

	function save_ordered_items( $id, $session_id, $li_sorted_items ) {
		$this->load->model('product_model');
		if ( strtolower($id) == "new") {
			$product_images_array= isset($_SESSION['newproduct_images_list']) ? $_SESSION['newproduct_images_list'] : array();
		} else {
			$product_images_array= $this->product_model->getProductById($id, "img_into_struct");
		}
		$li_sorted_items= json_decode( urldecode($li_sorted_items) );
		$DataSorted_items_array= array();
		foreach( $li_sorted_items as $li_sorted_item ) {
			foreach( $product_images_array as $product_image ) {

				if ( is_object($product_image) ) {
  				if ( $product_image->path == $li_sorted_item ) {
	  				$DataSorted_items_array[]= array( 'path'=> $li_sorted_item, 'alt'=> $product_image->alt );
		 			  break;
					}
				}
				if ( is_array($product_image) ) {
					if ( $product_image['path'] == $li_sorted_item ) {
						$DataSorted_items_array[]= array( 'path'=> $li_sorted_item, 'alt'=> $product_image['alt'] );
						break;
					}
				}

			}
		}
		if ( strtolower($id) == "new") {
			$_SESSION['newproduct_images_list']= $DataSorted_items_array;
		} else {
			$Res= $this->product_model->UpdateProduct( $id, array('img'=>json_encode( $DataSorted_items_array )) );
		}
		header('Content-type: application/json');
		echo json_encode(array('ErrorMessage' => '', 'ErrorCode' => 0 ) );

	}

	function update_product () {
		$this->load->model('product_model');
		$config =& get_config();
		$thumbs_sizes_list= $config['thumbs_sizes_list'];
		$DocumentRoot= $config['DocumentRoot'];
		$config_images_ext= $config['images_ext'];
		$session_id= $_POST['session_id'];

		$current_is_insert= false;
		if ( !empty($_POST['current_is_insert']) and (int)$_POST['current_is_insert']== 1 ) {
			$current_is_insert= true;
		}
		if ( $current_is_insert ) {
			$id= null;
		} else {
			$id= $_POST['invid'];
		}
		unset( $_POST['invid'] );
		unset( $_POST['current_is_insert'] );
		unset( $_POST['session_id'] );

		$product_images_array= array();
		if ( !empty($_SESSION['newproduct_images_list']) ) {
			$_POST['img']= json_encode( $_SESSION['newproduct_images_list'] );
			$product_images_array= $_SESSION['newproduct_images_list'];
			unset($_SESSION['newproduct_images_list']);
		}


		$Res= $this->product_model->UpdateProduct( $id, $_POST );
		$filesMovedCount= 0;
		if (  $current_is_insert and !empty($product_images_array) and is_array( $product_images_array) and $Res  ) { // move all images from "/temp_products" to "/products"
			$id= $Res;

			$temp_src_config_uploads_products_dir= '/uploads/temp_products/product-' . $session_id;
			$dst_config_uploads_products_dir= '/uploads/products/product-' . $Res;

			if ( !file_exists( $DocumentRoot . '/uploads/products' ) ) {
			    ...
			}
			if ( !file_exists( $DocumentRoot . $dst_config_uploads_products_dir ) ) {
				mkdir( $DocumentRoot . $dst_config_uploads_products_dir );
			}

      foreach( $product_images_array as $product_image ) {
				if ( is_object($product_image) ) {
				    ...
				}
				if ( is_array($product_image) ) {
					$product_image_path= urldecode($product_image['path']);
				}
				$srcFilename= $DocumentRoot . $temp_src_config_uploads_products_dir . DIRECTORY_SEPARATOR . $product_image_path . '.' . $config_images_ext;
				$dstFilename= $DocumentRoot . $dst_config_uploads_products_dir . DIRECTORY_SEPARATOR . $product_image_path . '.' . $config_images_ext;
				$renameRes= rename( $srcFilename,	$dstFilename );
				$filesMovedCount++;

				$FtpRes= $this->CopyFileToFTPDir( $id, $DocumentRoot . $dst_config_uploads_products_dir . DIRECTORY_SEPARATOR, $product_image_path . '.' . $config_images_ext, $dst_config_uploads_products_dir. DIRECTORY_SEPARATOR );

				foreach( $thumbs_sizes_list as $thumbs_size_title => $thumbs_size ) {
					$srcThumbnailFilename= $DocumentRoot . $temp_src_config_uploads_products_dir . DIRECTORY_SEPARATOR . $product_image_path . $this->getThumbnailExt( $thumbs_size['width'], $thumbs_size['height'] ) . '.' . $config_images_ext;
					$dstThumbnailFilename= $DocumentRoot . $dst_config_uploads_products_dir . DIRECTORY_SEPARATOR . $product_image_path . $this->getThumbnailExt( $thumbs_size['width'], $thumbs_size['height'] ) . '.' . $config_images_ext;

					if ( file_exists($srcThumbnailFilename) ) {
					    ...
					}
				}
			} // foreach( $product_images_array as $product_image ) {

			DeleteDirectory( $DocumentRoot . $temp_src_config_uploads_products_dir );
		} // current_is_insert

		header('Content-type: application/json');
		echo json_encode(array('ErrorMessage' => '', 'ErrorCode' => 0, 'id'=> $Res, 'Res'=> $Res, 'current_is_insert'=> $current_is_insert, 'filesMovedCount'=> $filesMovedCount, '$_POST'=> print_r($_POST,true) ) );

	}



	function copy_images_from_src_product_to_new_product( $src_invid, $session_id ) {
		$this->load->model('product_model');
		$config =& get_config();
		$thumbs_sizes_list= $config['thumbs_sizes_list'];
		$DocumentRoot= $config['DocumentRoot'];
		$config_images_ext= $config['images_ext'];

		$temp_src_config_uploads_products_dir= '/uploads/products/product-' . $src_invid;
		$dst_config_uploads_products_dir= '/uploads/temp_products/product-' . $session_id;
		$product_images_array= $this->product_model->getProductById($src_invid, "img_into_struct");

		if ( !file_exists( $DocumentRoot . '/uploads/temp_products' ) ) {
			mkdir( $DocumentRoot . '/uploads/temp_products' );
		}
		if ( !file_exists( $DocumentRoot . $dst_config_uploads_products_dir ) ) {
			mkdir( $DocumentRoot . $dst_config_uploads_products_dir );
		}

		$filesMovedCount= 0;
		foreach( $product_images_array as $product_image ) {
			$srcFilename= $DocumentRoot . $temp_src_config_uploads_products_dir . DIRECTORY_SEPARATOR . urldecode($product_image->path) . '.' . $config_images_ext;
			$dstFilename= $DocumentRoot . $dst_config_uploads_products_dir . DIRECTORY_SEPARATOR . urldecode($product_image->path) . '.' . $config_images_ext;
			$CopyRes= copy( $srcFilename,	$dstFilename );


			foreach( $thumbs_sizes_list as $thumbs_size_title => $thumbs_size ) {
				$srcThumbnailFilename= $DocumentRoot . $temp_src_config_uploads_products_dir . DIRECTORY_SEPARATOR . urldecode($product_image->path) . $this->getThumbnailExt( $thumbs_size['width'], $thumbs_size['height'] ) . '.' . $config_images_ext;

				$dstThumbnailFilename= $DocumentRoot . $dst_config_uploads_products_dir . DIRECTORY_SEPARATOR . urldecode($product_image->path) . $this->getThumbnailExt( $thumbs_size['width'], $thumbs_size['height'] ) . '.' . $config_images_ext;
				if ( file_exists($srcThumbnailFilename) ) {
					$Res= copy( $srcThumbnailFilename, $dstThumbnailFilename);
				}
			}


			$filesMovedCount++;
		}
		$_SESSION['newproduct_images_list']= $product_images_array;
		header('Content-type: application/json');
		echo json_encode(array('ErrorMessage' => '', 'ErrorCode' => 0, 'src_invid'=> $src_invid, 'filesMovedCount'=> $filesMovedCount ) );

	}

}


 function DeleteDirectory($DirectoryName) {
	$H = OpenDir($DirectoryName);
	while ($NextFile = readdir($H)) { // All files in dir
		if ($NextFile == "." or $NextFile == "..")
			continue;
		//AppUtils::deb($DirectoryName . DIRECTORY_SEPARATOR . $NextFile, '$DirectoryName . DIRECTORY_SEPARATOR . $NextFile::');
		unlink($DirectoryName . DIRECTORY_SEPARATOR . $NextFile);
	}
	closedir($H);
	rmdir($DirectoryName);
}


/* End of file product.php */
/* Location: ./system/application/controllers/inventory/product.php */