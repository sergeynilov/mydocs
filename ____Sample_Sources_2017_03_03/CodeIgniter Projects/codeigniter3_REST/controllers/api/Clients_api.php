<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 * @package        CodeIgniter
 * @subpackage    Rest Server
 * @category    Controller
 * @author        Phil Sturgeon
 * @link        http://philsturgeon.co.uk/code/
 */

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST/REST_Controller.php';

class Clients_api extends REST_Controller
{
    function client_get() // http://test-server.com/api/clients_api/client/id/2/format/json
    {
        $client_id = $this->get('id');
        if (!$client_id) {
            $this->response(null, 400);
        }
        $this->load->model('mclient', '', true);
        $clientObj = $this->mclient->getRowById($client_id);
        if ($clientObj) {
            $this->response($clientObj, 200); // 200 being the HTTP response code
        } else {
            $this->response(array('error' => 'Client with id c= ' . $client_id . ' was not be found!'), 404);
        }
    }

    function client_post()
    {
        if (empty($_POST['name']) ) {
            $this->response( array('errorMessage' => 'Name of client must be set ! ', 'errorCode' => 1, 'client_id' => ''), 400);
            return;
        }
        $this->load->model('mclient', '', true);
        $clientAlreadyExists = $this->mclient->getRowByFieldName('name', $_POST['name']);
        if (!empty($clientAlreadyExists)) {
            $this->response(array('errorMessage' => 'There is client with name "' . $_POST['name'] . '"! Client name must be unique in system.', 'errorCode' => 2, 'client_id' => ''), 404);
            return;
        }
        $insertedClientId = $this->mclient->UpdateClient(null, $_POST);
        $retArray = array('errorMessage' => '', 'errorCode' => 0, 'client_id' => $insertedClientId);
        $this->response($retArray, 200); // 200 being the HTTP response code
    }

    function client_delete()
    {
        $this->mclient->DeleteClient( $this->get('id') );
        $message = array('id' => $this->get('id'), 'message' => 'DELETED!');
        $this->response($message, 200); // 200 being the HTTP response code
    }

    function clients_get() // http://test-server.com/api/clients_api/clients/format/json
    {
        $this->load->model('mclient', '', true);
        $sort = $this->get('sort', 'client.name'); 
        $sort_direction = $this->get('sort_direction', 'desc');

        $offset = $this->get('offset');
        $limit = $this->get('limit'); 
        $clientsList = $this->mclient->getClientsList(false, '', array('limit' => $limit, 'offset' => $offset), $sort, $sort_direction);
        if ($clientsList) {
            $this->response($clientsList, 200); // 200 being the HTTP response code
        } else {
            $this->response(array('error' => 'Couldn\'t find any clients!'), 404);
        }
    }


}