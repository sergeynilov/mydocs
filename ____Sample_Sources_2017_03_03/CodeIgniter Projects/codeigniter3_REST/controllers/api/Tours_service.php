<?php
require_once("application/libraries/REST_Controller.php");
class Tours_service extends REST_Controller
{
    protected $app_config;
    public $ion_auth_config_tables;
    function __construct()
    {
        parent::__construct();
        $this->load->config('ion_auth', TRUE);
        $this->ion_auth_config_tables  = $this->config->item('tables', 'ion_auth');

    }


    /////////////////  USERS BLOCK START ////////////////
    public function users_get() { // GET request - get 1 user or listing of users
        $this->load->model('muser', '', true);
        $sort= $this->get('sort', "");
        $sort_direction= $this->get('sort_direction', "");
        $limit= $this->get('limit', "");
        $offset= $this->get('offset', "");
        $page= $this->get('page', "");
        $per_page= $this->get('per_page', "");
        $fields= $this->get('fields', "");
        $id = $this->get('id');
        $show_subscriptions_count = $this->get('show_subscriptions_count');
        $show_tours_count = $this->get('show_tours_count');
        $show_avatar_url = $this->get('show_avatar_url');
        $show_groups_name = $this->get('show_groups_name');
        $show_groups_id = $this->get('show_groups_id');
        $filter_username = $this->get('filter_username');
        $filter_users_groups = AppUtils::MakeArraySplitted( $this->get('filter_users_groups') );
        $filter_active = $this->get('filter_active');

        if (!empty($id)) { // get 1 user row by id
            $user = $this->muser->getRowById( $id, array('fields_for_select'=>$fields, 'show_subscriptions_count'=>$show_subscriptions_count, 'show_tours_count'=>$show_tours_count, 'show_groups_id'=>$show_groups_id, 'show_groups_name'=>$show_groups_name, 'show_avatar_url'=>$show_avatar_url) );
            $db_error = $this->db->error();
            if (!empty($db_error['code']) and !empty($db_error['message'])) {
                $db_error['error_type']= 'invalid_request';
                $this->response(array(
                    'errors' => $this->get_error_codes($db_error, __FILE__, __LINE__),
                ), REST_Controller::HTTP_BAD_REQUEST /*HTTP_NOT_FOUND*/); // NOT_FOUND (404) being the HTTP response code
            } else {
                if (empty($user)) {
                    $this->response(array(
                        'errors' => $this->get_error_codes(array( 'code'=>'-1', 'message'=> 'Invalid ID specified', 'error_type'=>'invalid_request'), __FILE__, __LINE__),
                    ), REST_Controller::HTTP_NOT_FOUND); //

                } else {
                    $this->response($this->format_data($user), REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
                }
            }
        } else {  // get users rows by filters/limits
            $users_list= $this->muser->getUserWithGroupsList(false, $page, array('fields_for_select'=>$fields, 'sort'=>$sort, 'sort_direction'=>$sort_direction, 'limit'=>$limit,'offset'=>$offset, 'per_page'=>$per_page, 'show_subscriptions_count'=>$show_subscriptions_count, 'show_tours_count'=>$show_tours_count, 'show_groups_id'=>$show_groups_id, 'active'=>$filter_active, 'users_groups'=>$filter_users_groups, 'username'=>$filter_username, 'show_groups_name'=>$show_groups_name, 'show_avatar_url'=>$show_avatar_url) );
            $db_error = $this->db->error();
            if (!empty($db_error['code']) and !empty($db_error['message'])) {
                $db_error['error_type']= 'invalid_request';
                $this->response(array(
                    'errors' => $this->get_error_codes($db_error, __FILE__, __LINE__),
                ), REST_Controller::HTTP_BAD_REQUEST /*HTTP_NOT_FOUND*/); // NOT_FOUND (404) being the HTTP response code
                exit;
            } else {
                $this->response($this->format_data($users_list), REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            }
        }
    } //public function users_get() { // GET request - get 1 user or listing of users


    public function users_post() { // POST request - add new user
        $this->load->model('muser', '', true);
        $username = $this->post('username');
        $ip_address = !empty($_SERVER["REMOTE_ADDR"]) ? $_SERVER["REMOTE_ADDR"] : "";
        $email = $this->post('email');
        $avatar = $this->post('avatar');
        $avatar_path = $this->post('avatar_path');
        $avatar_dir_path = $this->post('avatar_dir_path');
        $website = $this->post('website');
        $birthday = $this->post('birthday');
        $active = $this->post('active');
        $user_groups  = $this->post('user_groups');
        $first_name = $this->post('first_name');
        $last_name = $this->post('last_name');
        $company = $this->post('company');
        $phone = $this->post('phone');
        $discount_type = $this->post('discount_type');
        $max_discount = $this->post('max_discount');
        $modified_by = $this->post('modified_by');
        $activation_code= ($active ? "" : AppUtils::GenerateActivationCode());
        $additional_data= array( 'ip_address'=> $ip_address, 'active'=> $active, 'first_name'=> $first_name, 'company'=> $company, 'phone'=> $phone, 'discount_type'=> $discount_type, 'max_discount'=> $max_discount, 'modified_by'=> $modified_by, 'last_name'=> $last_name, 'website'=> $website, 'birthday'=> $birthday, 'created_on'=>now(),  'created_at'=>strftime(AppUtils::getDateTimeMySqlFormat()), 'activation_code'=> $activation_code, 'backend_template_id'=>1, 'frontend_template_id'=>1 );
        $password= AppUtils::GeneratePassword();
        $validation_errors= array();
        include_once("validateData.php");
        $validateDataObj= new validateData($this);
        $validateDataObj->set_validation_required_fields(  array( 'username', 'email', 'active', 'user_groups', 'modified_by', 'discount_type' ) );
        $validateDataObj->set_validation_email_fields(  array( 'email' ) );
        $validateDataObj->set_validation_not_negative_fields(  array( 'max_discount' ) );
        $validateDataObj->set_validation_integer_fields(  array( 'modified_by' ) );
        $validateDataObj->set_validation_float_fields(  array( 'max_discount' ) );
        $validateDataObj->set_validation_name_fields(  array( 'first_name', 'last_name' ) );
        $validateDataObj->set_validation_url_fields(  array( 'website' ) );
        $validateDataObj->set_validation_date_fields(  array( 'birthday' ) );
        $validateDataObj->set_validation_phone_fields(  array( 'phone' ) );
        $validateDataObj->set_validation_enum_fields(  array( array( 'field_name'=>'discount_type', 'values'=>array('S','B','G' ) ) ) );
        if (!empty($avatar)) {
            $validateDataObj->set_validation_uploaded_files(array('avatar' => array('file_url' => $avatar, 'file_path' => $avatar_path)));
        }
        $validateDataObj->set_validation_method('post');
        $validateDataObj->runValidation();
        if ( $validateDataObj->has_validation_errors() ) {
            $validation_errors= $validateDataObj->get_validation_errors();
        }

        if ( !$this->muser->checkIsEmailUnique($email) and empty($validation_errors['email']) ) {
            $validation_errors['email']= array('email'=> $email, 'error_type'=>'validation_email_is_not_unique');
        }
        if ( !$this->muser->checkIsUsernameUnique($username) and empty($validation_errors['username']) ) {
            $validation_errors['username']= array('username'=> $username, 'error_type'=>'validation_username_is_not_unique');
        }


        if ( count($validation_errors) > 0 ) {
            $this->response(array(
                'errors' => $this->get_error_codes( $validation_errors , __FILE__, __LINE__),
            ), REST_Controller::HTTP_BAD_REQUEST /*HTTP_NOT_FOUND*/); // NOT_FOUND (404) being the HTTP response code
            exit;
        } else {
            $groups= AppUtils::MakeArraySplitted($user_groups);
            $ret_user_id= $this->ion_auth->register( $username, $password, $email, $additional_data, $groups ) ;
            $return_data_array= array( 'inserted_user_id'=> $ret_user_id );
            $this->response($this->format_data($return_data_array), REST_Controller::HTTP_OK); // OK (200) being the HTTP response code

        }

    } // public function users_post() { // POST request - add new user

    public function users_put() {
        $this->load->model('muser', '', true);
        $id = $this->put('id');
        //$email = $this->put('email');
        $avatar = $this->put('avatar');
        $avatar_path = $this->put('avatar_path');
        $avatar_dir_path = $this->put('avatar_dir_path');
        $website = $this->put('website');
        $birthday = $this->put('birthday');
        $active = $this->put('active');
        $user_groups  = $this->put('user_groups');
        $first_name = $this->put('first_name');
        $last_name = $this->put('last_name');
        $company = $this->put('company');
        $phone = $this->put('phone');
        $discount_type = $this->put('discount_type');
        $max_discount = $this->put('max_discount');
        $modified_by = $this->put('modified_by');
        $additional_data= array( /*'ip_address'=> $ip_address, */ 'active'=> $active, 'first_name'=> $first_name, 'company'=> $company, 'phone'=> $phone, 'discount_type'=> $discount_type, 'max_discount'=> $max_discount, 'modified_by'=> $modified_by, 'last_name'=> $last_name, 'website'=> $website, 'birthday'=> $birthday, 'updated_at'=>strftime(AppUtils::getDateTimeMySqlFormat()) );

        $validation_errors= array();
        include_once("validateData.php");
        $validateDataObj= new validateData($this);
        $validateDataObj->set_validation_required_fields(  array( /*'username', 'email', */ 'active', 'user_groups', 'modified_by', 'discount_type' ) );
        $validateDataObj->set_validation_not_negative_fields(  array( 'max_discount' ) );
        $validateDataObj->set_validation_integer_fields(  array( 'modified_by' ) );
        $validateDataObj->set_validation_float_fields(  array( 'max_discount' ) );
        $validateDataObj->set_validation_name_fields(  array( 'first_name', 'last_name' ) );
        $validateDataObj->set_validation_url_fields(  array( 'website' ) );
        $validateDataObj->set_validation_date_fields(  array( 'birthday' ) );
        $validateDataObj->set_validation_phone_fields(  array( 'phone' ) );
        $validateDataObj->set_validation_enum_fields(  array( array( 'field_name'=>'discount_type', 'values'=>array('S','B','G' ) ) ) );
        if (!empty($avatar)) {
            $validateDataObj->set_validation_uploaded_files(array('avatar' => array('file_url' => $avatar, 'file_path' => $avatar_path)));
        }
        $validateDataObj->set_validation_method('put');
        $validateDataObj->runValidation();
        if ( $validateDataObj->has_validation_errors() ) {
            $validation_errors= $validateDataObj->get_validation_errors();
        }

        if ( count($validation_errors) > 0 ) {
            $this->response(array(
                'errors' => $this->get_error_codes( $validation_errors , __FILE__, __LINE__),
            ), REST_Controller::HTTP_BAD_REQUEST /*HTTP_NOT_FOUND*/); // NOT_FOUND (404) being the HTTP response code
            exit;
        } else {
            $groups= AppUtils::MakeArraySplitted($user_groups);

            $ret_user_id= $this->muser->UpdateUser($id, $additional_data, $groups );//( $username, $password, $email, $additional_data, $groups ) ;
            $return_data_array= array( 'user_id'=> $ret_user_id );

            $this->response($this->format_data($return_data_array), REST_Controller::HTTP_OK); // OK (200) being the HTTP response code

        }

    } // public function users_put() { // POST request - add new user

    public function users_delete() { // DELETE request - delete user with all related data by user id
        $ci = & get_instance();
        $config_object = $ci->config;
        $app_config = $config_object->config;

        $id = (int) $this->get('id');

        if ($id <= 0)
        {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

        $this->load->model('muser', '', true);
        $this->load->model('mhostel', '', true);
        $this->load->model('mhostel_inquery', '', true);
        $this->load->model('mhostel_facility', '', true);
        $this->load->model('mhostel_payment_plan', '', true);
        $this->load->model('mhostel_review', '', true);
        $this->load->model('mhostel_room', '', true);
        $this->load->model('msent_emails_history', '', true);
        $this->load->model('mhostel_extra_details', '', true);
        $this->load->model('mcms_item', '', true);
        $this->load->model('msubscription', '', true);

        $this->load->model('mtour', '', true);
        $this->load->model('mtour_inquery', '', true);
        $this->load->model('mtour_highlight', '', true);
        $this->load->model('mtour_review', '', true);
        $userForDelete = $this->muser->getRowById($id);
        if (empty($userForDelete)) {
            $this->response(array(
                'errors' => $this->get_error_codes(array( 'code'=>'-1', 'message'=> 'Invalid ID specified', 'error_type'=>'invalid_request'), __FILE__, __LINE__),
            ), REST_Controller::HTTP_NOT_FOUND); //
        }
        $this->db->trans_start();
        $HostelsList = $this->mhostel->getHostelsList(false, '', $id, '', '', '', '', '', '', '', '', '', false);
        $Res = $this->msent_emails_history->DeleteSent_Emails_HistorysByAuthorId($id);
        $Res = $this->mcms_item->DeleteCms_ItemsByAuthorlId($id);

        $HostelsDeletedList = array();
        foreach ($HostelsList as $Hostel) {
            $Res = $this->mhostel_inquery->DeleteHostel_InquerysByHostelId($Hostel['id']);
            $Res = $this->mhostel_facility->DeleteHostel_FacilitysByHostelId($Hostel['id']);
            $Res = $this->mhostel_payment_plan->DeleteHostel_Payment_PlansByHostelId($Hostel['id']);
            $Res = $this->mhostel_review->DeleteHostel_ReviewsByHostelId($Hostel['id']);
            $Res = $this->mhostel_room->DeleteHostel_RoomsByHostelId($Hostel['id']);
            $Res = $this->mhostel_extra_details->DeleteHostel_Extra_DetailsByHostelId($Hostel['id']);
            $OkResult = $this->mhostel->DeleteHostel($Hostel['id']);
            $HostelsDeletedList[] = $Hostel['id'];
        }

        $ToursList = $this->mtour->getToursList(false, '', array('filter_user_id' => $id), '', '');

        $ToursIdDeletedList = array();
        foreach ($ToursList as $Tour) {
            $Res = $this->mtour_inquery->DeleteTour_InquerysByTourId($Tour['id']);
            $Res = $this->mtour_highlight->DeleteTour_HighlightsByTourId($Tour['id']);
            $Res = $this->mtour_review->DeleteTour_ReviewsByTourId($Tour['id']);
            $OkResult = $this->mtour->DeleteTour($Tour['id']);
            $ToursIdDeletedList[] = $Tour['id'];
        }

        $this->msubscription->DeleteSubscriptionByUserId( $id );
        $this->ion_auth->remove_from_group( false, $id );

        if ($Res) {
            $OkResult = $this->muser->DeleteUser($id);
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();

                foreach ($HostelsDeletedList as $HostelsDeleted) {
                    $dir_path = $app_config['document_root'] . DIRECTORY_SEPARATOR . $app_config['uploads_hostels_dir'] . '-hostel-' . $HostelsDeleted['id'] . DIRECTORY_SEPARATOR;
                    AppUtils::DeleteDirectory($dir_path);
                }
                foreach ($ToursIdDeletedList as $TourIdDeleted) {
                    $dir_path = $app_config['document_root'] . DIRECTORY_SEPARATOR . $app_config['uploads_tours_dir'] . '-tour-' . $TourIdDeleted . DIRECTORY_SEPARATOR;
                    AppUtils::DeleteDirectory($dir_path);
                }
            }
            if ($OkResult) {
                $message = [
                    'id' => $id,
                    'message' => 'User deleted with all related data.'
                ];
                $this->set_response($message, REST_Controller::HTTP_NO_CONTENT); // NO_CONTENT (204) being the HTTP response code
            }
        }


    } // public function users_delete() { // DELETE request - delete user with all related data by user id
    /////////////////  USERS BLOCK END  ////////////////


    /////////////////  CATEGORIES BLOCK START  ////////////////
    public function categories_get() { // GET request - get 1 category or listing of categories
        $this->load->model('mcategory', '', true);
        $sort= $this->get('sort', "");
        $sort_direction= $this->get('sort_direction', "");
        $limit= $this->get('limit', "");
        $offset= $this->get('offset', "");
        $page= $this->get('page', "");
        $per_page= $this->get('per_page', "");
        $fields= $this->get('fields', "");
        $id = $this->get('id','');
        $show_tours_count = $this->get('show_tours_count');
        $filter_name = $this->get('filter_name');

        if (!empty($id)) { // get 1 category row by id
            $category = $this->mcategory->getRowById( $id, array('fields_for_select'=>$fields, 'show_tours_count'=>$show_tours_count ) );
            $db_error = $this->db->error();
            if (!empty($db_error['code']) and !empty($db_error['message'])) {
                $db_error['error_type']= 'invalid_request';
                $this->response(array(
                    'errors' => $this->get_error_codes($db_error, __FILE__, __LINE__),
                ), REST_Controller::HTTP_BAD_REQUEST /*HTTP_NOT_FOUND*/); // NOT_FOUND (404) being the HTTP response code
            } else {
                if (empty($category)) {
                    $this->response(array(
                        'errors' => $this->get_error_codes(array( 'code'=>'-1', 'message'=> 'Invalid ID specified', 'error_type'=>'invalid_request'), __FILE__, __LINE__),
                    ), REST_Controller::HTTP_NOT_FOUND); //

                } else {
                    $this->response($this->format_data($category), REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
                }
            }
        } else {  // get categories rows by filters/limits
            $categories_list= $this->mcategory->getCategorysList(false, $page, array('fields_for_select'=>$fields, 'limit'=>$limit,'offset'=>$offset,  'per_page'=> $per_page,  'show_tours_count'=>$show_tours_count, 'name'=>$filter_name), $sort, $sort_direction);
            $db_error = $this->db->error();
            if (!empty($db_error['code']) and !empty($db_error['message'])) {
                $db_error['error_type']= 'invalid_request';
                $this->response(array(
                    'errors' => $this->get_error_codes($db_error, __FILE__, __LINE__),
                ), REST_Controller::HTTP_BAD_REQUEST /*HTTP_NOT_FOUND*/); // NOT_FOUND (404) being the HTTP response code
                exit;
            } else {
                $this->response($this->format_data($categories_list), REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            }
        }
    } //public function categories_get() { // GET request - get 1 category or listing of categories

    public function categories_post() { // POST request - add new user
        $this->load->model('mcategory', '', true);
        $name = $this->post('name');

        $validation_errors= array();
        include_once("validateData.php");
        $validateDataObj= new validateData($this);
        $validateDataObj->set_validation_required_fields(  array( 'name' ) );
        $validateDataObj->set_validation_method('post');
        $validateDataObj->runValidation();
        if ( $validateDataObj->has_validation_errors() ) {
            $validation_errors= $validateDataObj->get_validation_errors();
        }

        if ( !$this->mcategory->checkIsNameUnique($name) and empty($validation_errors['name']) ) {
            $validation_errors['name']= array('name'=> $name, 'error_type'=>'validation_name_is_not_unique');
        }

        if ( count($validation_errors) > 0 ) {
            $this->response(array(
                'errors' => $this->get_error_codes( $validation_errors , __FILE__, __LINE__),
            ), REST_Controller::HTTP_BAD_REQUEST /*HTTP_NOT_FOUND*/); // NOT_FOUND (404) being the HTTP response code
            exit;
        } else {
            $ret_category_id= $this->mcategory->UpdateCategory( '', array('name'=>$name ) ) ;
            $return_data_array= array( 'inserted_category_id'=> $ret_category_id );
            if ( $ret_category_id ) { // some fields must be returned
                $returned_category = $this->mcategory->getRowById($ret_category_id,'id, created_at');
                foreach( $returned_category as $next_key=>$next_value ) {
                    $return_data_array[$next_key]= $next_value;
                }
            } // if (!empty($return_fields)) { // some fields must be returned
            $this->response($this->format_data($return_data_array), REST_Controller::HTTP_OK); // OK (200) being the HTTP response code

        }

    } // public function categories_post() { // POST request - add new category

    public function categories_put() { // PUT request - add new user
        $this->load->model('mcategory', '', true);
        $id = $this->put('id');
        $name = $this->put('name');
        $validation_errors= array();
        include_once("validateData.php");
        $validateDataObj= new validateData($this);
        $validateDataObj->set_validation_required_fields(  array( 'name' ) );
        $validateDataObj->set_validation_method('put');
        $validateDataObj->runValidation();
        if ( $validateDataObj->has_validation_errors() ) {
            $validation_errors= $validateDataObj->get_validation_errors();
        }

        if ( !$this->mcategory->checkIsNameUnique($name, $id) and empty($validation_errors['name']) ) {
            $validation_errors['name']= array('name'=> $name, 'error_type'=>'validation_name_is_not_unique');
        }


        if ( count($validation_errors) > 0 ) {
            $this->response(array(
                'errors' => $this->get_error_codes( $validation_errors , __FILE__, __LINE__),
            ), REST_Controller::HTTP_BAD_REQUEST /*HTTP_NOT_FOUND*/); // NOT_FOUND (404) being the HTTP response code
            exit;
        } else {
            $ret_category_id= $this->mcategory->UpdateCategory( $id, array('name'=>$name ) ) ;
            $return_data_array= array( 'id'=> $ret_category_id );
            $this->response($this->format_data($return_data_array), REST_Controller::HTTP_OK); // OK (200) being the HTTP response code

        }

    } // public function categories_put() { // PUT request - add new category


    public function categories_delete() { // DELETE request - delete category with all related data by category id
        $ci = & get_instance();
        $config_object = $ci->config;
        $app_config = $config_object->config;

        $id = (int) $this->get('id');

        // Validate the id.
        if ($id <= 0)
        {
            // Set the response and exit
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

        $this->load->model('mcategory', '', true);
        $this->load->model('mtour', '', true);
        $this->load->model('mtour_inquery', '', true);
        $this->load->model('mtour_highlight', '', true);
        $this->load->model('mtour_review', '', true);
        $categoryForDelete = $this->mcategory->getRowById($id);
        if (empty($categoryForDelete)) {
            $this->response(array(
                'errors' => $this->get_error_codes(array( 'code'=>'-1', 'message'=> 'Invalid ID specified', 'error_type'=>'invalid_request'), __FILE__, __LINE__),
            ), REST_Controller::HTTP_NOT_FOUND); //
        }
        $this->db->trans_start();

        $ToursList = $this->mtour->getToursList(false, '', array('filter_category_id' => $id), '', '');

        $ToursIdDeletedList = array();
        foreach ($ToursList as $Tour) {
            $Res = $this->mtour_inquery->DeleteTour_InquerysByTourId($Tour['id']);
            $Res = $this->mtour_highlight->DeleteTour_HighlightsByTourId($Tour['id']);
            $Res = $this->mtour_review->DeleteTour_ReviewsByTourId($Tour['id']);
            $OkResult = $this->mtour->DeleteTour($Tour['id']);
            $ToursIdDeletedList[] = $Tour['id'];
        }


        $OkResult = $this->mcategory->DeleteCategory($id);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        } else {
            $this->db->trans_commit();
            foreach ($ToursIdDeletedList as $TourIdDeleted) {
                $dir_path = $app_config['document_root'] . DIRECTORY_SEPARATOR . $app_config['uploads_tours_dir'] . '-tour-' . $TourIdDeleted . DIRECTORY_SEPARATOR;
                AppUtils::DeleteDirectory($dir_path);
            }
        }
        if ($OkResult) {
            $message = [
                'id' => $id,
                'message' => 'Category deleted with all related data.'
            ];
            $this->set_response($message, REST_Controller::HTTP_NO_CONTENT); // NO_CONTENT (204) being the HTTP response code
        }


    } // public function categories_delete() { // DELETE request - delete category with all related data by category id

    /////////////////  CATEGORIES BLOCK END  ////////////////


    /////////////////  TOURS BLOCK START  ////////////////
    public function tours_get() { // GET request - get 1 tour or listing of tours
        $this->load->model('mtour', '', true);
        $this->load->model('mtour_review', '', true);
        $this->load->model('mtour_highlight', '', true);
        $sort= $this->get('sort', "");
        $sort_direction= $this->get('sort_direction', "");
        $limit= $this->get('limit', "");
        $offset= $this->get('offset', "");
        $page= $this->get('page', "");
        $per_page= $this->get('per_page', "");
        $fields= $this->get('fields', "");
        $id = $this->get('id','');
        $show_tours_count = $this->get('show_tours_count');
        $filter_name = $this->get('filter_name');
        $filter_only_featured = $this->get('filter_only_featured');
        $filter_only_not_featured = $this->get('filter_only_not_featured');
        $filter_only_is_priority_listing = $this->get('filter_only_is_priority_listing');
        $filter_only_is_homepage_spotlight = $this->get('filter_only_is_homepage_spotlight');
        $filter_user_id = $this->get('filter_user_id');
        $filter_category_id = $this->get('filter_category_id');
        $filter_state_id = $this->get('filter_state_id');
        $filter_region_id = $this->get('filter_region_id');
        $filter_subregion_id = $this->get('filter_subregion_id');
        $filter_status = $this->get('filter_status');
        $show_tour_reviews = $this->get('show_tour_reviews');
        $show_tour_highlights = $this->get('show_tour_highlights');

        if (!empty($id)) { // get 1 tour row by id
            $data_shown= false;
            if (!empty($show_tour_reviews)) {
                $data_tour= $this->mtour_review->getTour_ReviewsList(false, '', '', $id, 'A', '', '');
                $data_shown= true;
            }
            if (!empty($show_tour_highlights)) {
                $data_tour= $this->mtour_highlight->getTour_HighlightsList(false, '', '', $id );
                $data_shown= true;
            }
            if ( !$data_shown ) {
                $data_tour = $this->mtour->getRowById($id, true, array('fields_for_select' => $fields));
            }
            $db_error = $this->db->error();
            if (!empty($db_error['code']) and !empty($db_error['message'])) {
                $db_error['error_type']= 'invalid_request';
                $this->response(array(
                    'errors' => $this->get_error_codes($db_error, __FILE__, __LINE__),
                ), REST_Controller::HTTP_BAD_REQUEST /*HTTP_NOT_FOUND*/); // NOT_FOUND (404) being the HTTP response code
            } else {
                if (empty($data_tour)) {
                    $this->response(array(
                        'errors' => $this->get_error_codes(array( 'code'=>'-1', 'message'=> 'Invalid ID specified', 'error_type'=>'invalid_request'), __FILE__, __LINE__),
                    ), REST_Controller::HTTP_NOT_FOUND); //

                } else {
                    $this->response($this->format_data($data_tour), REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
                }
            }
        } else {  // get tours rows by filters/limits
            $data_shown= false;
            if (!empty($show_tour_reviews)) {
                $data_list= $this->mtour_review->getTour_ReviewsList(false, '', '', '', /*$Tour['id'],*/ 'A', '', '');
                $data_shown= true;
            }
            if (!empty($show_tour_highlights)) {
                $data_list= $this->mtour_highlight->getTour_HighlightsList(false, '', '', '');
                $data_shown= true;
            }
            if ( !$data_shown ) {
                $data_list = $this->mtour->getToursList(false, $page, array('fields_for_select' => $fields, 'limit' => $limit, 'offset' => $offset, 'per_page' => $per_page, 'show_tours_count' => $show_tours_count, 'filter_name' => $filter_name, 'filter_only_featured' => $filter_only_featured, 'filter_only_not_featured' => $filter_only_not_featured, 'filter_only_is_priority_listing' => $filter_only_is_priority_listing, 'filter_only_is_homepage_spotlight' => $filter_only_is_homepage_spotlight, 'filter_user_id' => $filter_user_id, 'filter_category_id' => $filter_category_id, 'filter_state_id' => $filter_state_id, 'filter_region_id' => $filter_region_id, 'filter_subregion_id' => $filter_subregion_id, 'filter_status' => $filter_status), $sort, $sort_direction);
            }
            $db_error = $this->db->error();
            if (!empty($db_error['code']) and !empty($db_error['message'])) {
                $db_error['error_type']= 'invalid_request';
                $this->response(array(
                    'errors' => $this->get_error_codes($db_error, __FILE__, __LINE__),
                ), REST_Controller::HTTP_BAD_REQUEST /*HTTP_NOT_FOUND*/); // NOT_FOUND (404) being the HTTP response code
                exit;
            } else {
                $this->response($this->format_data($data_list), REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            }
        }
    } //public function tours_get() { // GET request - get 1 tour or listing of tours

    public function tours_post() { // POST request - add new user
        // tours from a data store e.g. database
        $this->load->model('mtour', '', true);
        $name = $this->post('name');
        $user_id = (int)$this->post('user_id');
        $region_id = $this->post('region_id');
        $subregion_id = $this->post('subregion_id');
        $state_id = $this->post('state_id');
        $category_id = (int)$this->post('category_id');
        $status = $this->post('status');
        $feature = $this->post('feature');
        $email = $this->post('email');
        $website = $this->post('website');
        $book_now_link = $this->post('book_now_link');
        $price = $this->post('price');
        $is_priority_listing = $this->post('is_priority_listing');
        $is_homepage_spotlight = $this->post('is_homepage_spotlight');
        $short_descr = $this->post('short_descr');
        $descr = $this->post('descr');
        $distance = $this->post('distance');
        $view_count = $this->post('view_count');
        $description = $this->post('description');
        $about_us = $this->post('about_us');
        $schedule_pricing = $this->post('schedule_pricing');
        $additional_info = $this->post('additional_info');
        $modified_by = $this->post('modified_by');

        $validation_errors= array();
        include_once("validateData.php");
        $validateDataObj= new validateData($this);
        $validateDataObj->set_validation_required_fields(  array( 'name', 'user_id', 'state_id', 'feature', 'email', 'price', 'is_priority_listing', 'is_homepage_spotlight', 'short_descr', 'descr', 'status', 'description', 'about_us', 'modified_by' ) );
        $validateDataObj->set_validation_email_fields(  array( 'email' ) );
        $validateDataObj->set_validation_not_negative_fields(  array( 'distance', 'rating' ) );
        $validateDataObj->set_validation_integer_fields(  array( 'modified_by', 'distance', 'rating' ) );
        $validateDataObj->set_validation_float_fields(  array( 'price' ) );
        $validateDataObj->set_validation_url_fields(  array( 'website', 'book_now_link' ) );
        $validateDataObj->set_validation_enum_fields(  array(
            array( 'field_name'=>'status', 'values'=>array('A','I','N' ) ),
            array( 'field_name'=>'is_priority_listing', 'values'=>array( 'Y','N' ) ),
            array( 'field_name'=>'is_homepage_spotlight', 'values'=>array( 'Y','N' ) ),
            array( 'field_name'=>'feature', 'values'=>array( 'F','S' ) )
        ) );
        $validateDataObj->set_validation_reference_links(  array(
            array( 'field_name'=>"user_id", 'ref_table'=>$this->ion_auth_config_tables['users'], 'ref_column'=>'id' ),
            array( 'field_name'=>"category_id", 'ref_table'=>$this->ion_auth_config_tables['category'], 'ref_column'=>'id' ),
            array( 'field_name'=>"region_id", 'ref_table'=>$this->ion_auth_config_tables['region'], 'ref_column'=>'id' ),
            array( 'field_name'=>"subregion_id", 'ref_table'=>$this->ion_auth_config_tables['subregion'], 'ref_column'=>'id' ),
            array( 'field_name'=>"state_id", 'ref_table'=>$this->ion_auth_config_tables['state'], 'ref_column'=>'id' ),
        ) );
        $validateDataObj->set_validation_method('post');
        $validateDataObj->runValidation();
        if ( $validateDataObj->has_validation_errors() ) {
            $validation_errors= $validateDataObj->get_validation_errors();
        }

        if ( !$this->mtour->checkIsNameUnique($name) and empty($validation_errors['name']) ) {
            $validation_errors['name']= array('name'=> $name, 'error_type'=>'validation_name_is_not_unique');
        }


        if ( count($validation_errors) > 0 ) {
            $this->response(array(
                'errors' => $this->get_error_codes( $validation_errors , __FILE__, __LINE__),
            ), REST_Controller::HTTP_BAD_REQUEST /*HTTP_NOT_FOUND*/); // NOT_FOUND (404) being the HTTP response code
            exit;
        } else {
            $this->load->library('uuid');
            $uid= $this->uuid->v3($name);
            $slug_config = array(
                'field' => 'slug',
                'title' => 'name',
                'table' => $this->ion_auth_config_tables['tour'],
                'id' => 'id',
            );
            $this->load->library('slug', $slug_config);
            $generated_slug = $this->slug->create_uri(array('name' => $name));
            $ret_tour_id= $this->mtour->UpdateTour( '', array('uid'=>$uid, 'name'=>$name, 'user_id'=>$user_id, 'region_id'=> $region_id, 'subregion_id'=> $subregion_id,
                'state_id'=> $state_id, 'category_id'=> $category_id, 'status'=> $status, 'feature'=>$feature, 'email'=> $email, 'website'=> $website,
                'book_now_link'=> $book_now_link, 'slug'=> $generated_slug, 'price'=> $price, 'is_priority_listing'=> $is_priority_listing, 'is_homepage_spotlight'=> $is_homepage_spotlight,
                'short_descr'=>$short_descr, 'descr'=> $descr, 'distance'=> $distance, 'view_count'=> $view_count, 'description'=> $description, 'about_us'=>$about_us,
                'schedule_pricing'=> $schedule_pricing, 'additional_info'=> $additional_info, 'modified_by'=> $modified_by ) ) ;
            $return_data_array= array( 'inserted_tour_id'=> $ret_tour_id, 'slug'=> $generated_slug );
            if ( $ret_tour_id ) { // some fields must be returned
                $returned_tour = $this->mtour->getRowById($ret_tour_id,'id, created_at');
                if (empty($return_data_array['uid']) and !empty($returned_tour['uid']) ) {
                    $return_data_array['uid'] = $returned_tour['uid'];
                }
                if (empty($return_data_array['created_at']) and !empty($returned_tour['created_at']) ) {
                    $return_data_array['created_at'] = $returned_tour['created_at'];
                }
                if (empty($return_data_array['view_count']) and !empty($returned_tour['view_count']) ) {
                    $return_data_array['view_count'] = $returned_tour['view_count'];
                }
            } // if (!empty($return_fields)) { // some fields must be returned
            $this->response($this->format_data($return_data_array), REST_Controller::HTTP_OK); // OK (200) being the HTTP response code

        }

    } // public function tours_post() { // POST request - add new tour

    public function tours_put() { // PUT request - add new user
        $this->load->model('mtour', '', true);
        $id = $this->put('id');
        $name = $this->put('name');
        $user_id = (int)$this->put('user_id');
        $region_id = $this->put('region_id');
        $subregion_id = $this->put('subregion_id');
        $state_id = $this->put('state_id');
        $category_id = (int)$this->put('category_id');
        $status = $this->put('status');
        $feature = $this->put('feature');
        $website = $this->put('website');
        $book_now_link = $this->put('book_now_link');
        $price = $this->put('price');
        $is_priority_listing = $this->put('is_priority_listing');
        $is_homepage_spotlight = $this->put('is_homepage_spotlight');
        $short_descr = $this->put('short_descr');
        $descr = $this->put('descr');
        $distance = $this->put('distance');
        $view_count = $this->put('view_count');
        $description = $this->put('description');
        $about_us = $this->put('about_us');
        $schedule_pricing = $this->put('schedule_pricing');
        $additional_info = $this->put('additional_info');
        $modified_by = $this->put('modified_by');

        $validation_errors= array();
        include_once("validateData.php");
        $validateDataObj= new validateData($this);
        $validateDataObj= new validateData($this);
        $validateDataObj->set_validation_required_fields(  array( 'name', 'user_id', 'state_id', 'feature', 'price', 'is_priority_listing', 'is_homepage_spotlight', 'short_descr', 'descr', 'status', 'description', 'about_us', 'modified_by' ) );
        $validateDataObj->set_validation_not_negative_fields(  array( 'distance', 'rating' ) );
        $validateDataObj->set_validation_integer_fields(  array( 'modified_by', 'distance', 'rating' ) );
        $validateDataObj->set_validation_float_fields(  array( 'price' ) );
        $validateDataObj->set_validation_url_fields(  array( 'website', 'book_now_link' ) );
        $validateDataObj->set_validation_enum_fields(  array(
            array( 'field_name'=>'status', 'values'=>array('A','I','N' ) ),
            array( 'field_name'=>'is_priority_listing', 'values'=>array( 'Y','N' ) ),
            array( 'field_name'=>'is_homepage_spotlight', 'values'=>array( 'Y','N' ) ),
            array( 'field_name'=>'feature', 'values'=>array( 'F','S' ) )
        ) );
        $validateDataObj->set_validation_reference_links(  array(
            array( 'field_name'=>"user_id", 'ref_table'=>$this->ion_auth_config_tables['users'], 'ref_column'=>'id' ),
            array( 'field_name'=>"category_id", 'ref_table'=>$this->ion_auth_config_tables['category'], 'ref_column'=>'id' ),
            array( 'field_name'=>"region_id", 'ref_table'=>$this->ion_auth_config_tables['region'], 'ref_column'=>'id' ),
            array( 'field_name'=>"subregion_id", 'ref_table'=>$this->ion_auth_config_tables['subregion'], 'ref_column'=>'id' ),
            array( 'field_name'=>"state_id", 'ref_table'=>$this->ion_auth_config_tables['state'], 'ref_column'=>'id' ),
        ) );

        $validateDataObj->set_validation_method('put');
        $validateDataObj->runValidation();
        if ( $validateDataObj->has_validation_errors() ) {
            $validation_errors= $validateDataObj->get_validation_errors();
        }

        if ( !$this->mtour->checkIsNameUnique($name, $id) and empty($validation_errors['name']) ) {
            $validation_errors['name']= array('name'=> $name, 'error_type'=>'validation_name_is_not_unique');
        }

        if ( count($validation_errors) > 0 ) {
            $this->response(array(
                'errors' => $this->get_error_codes( $validation_errors , __FILE__, __LINE__),
            ), REST_Controller::HTTP_BAD_REQUEST /*HTTP_NOT_FOUND*/); // NOT_FOUND (404) being the HTTP response code
            exit;
        } else {
            $updated_at= AppUtils::ShowFormattedDateTime(null,'MySql');
            $ret_tour_id= $this->mtour->UpdateTour( $id, array('name'=>$name, 'user_id'=>$user_id, 'region_id'=> $region_id, 'subregion_id'=> $subregion_id,
                'state_id'=> $state_id, 'category_id'=> $category_id, 'status'=> $status, 'feature'=>$feature, 'website'=> $website,
                'book_now_link'=> $book_now_link, 'price'=> $price, 'is_priority_listing'=> $is_priority_listing, 'is_homepage_spotlight'=> $is_homepage_spotlight,
                'short_descr'=>$short_descr, 'descr'=> $descr, 'distance'=> $distance, 'view_count'=> $view_count, 'description'=> $description, 'about_us'=>$about_us,
                'schedule_pricing'=> $schedule_pricing, 'additional_info'=> $additional_info, 'modified_by'=> $modified_by, 'updated_at'=> $updated_at ) ) ;
            $return_data_array= array( 'id'=> $ret_tour_id, 'updated_at'=> $updated_at );
            $this->response($this->format_data($return_data_array), REST_Controller::HTTP_OK); // OK (200) being the HTTP response code

        }

    } // public function tours_put() { // PUT request - add new tour


    public function tours_delete() { // DELETE request - delete tour with all related data by tour id
        $ci = & get_instance();
        $config_object = $ci->config;
        $app_config = $config_object->config;

        $id = (int) $this->get('id');

        // Validate the id.
        if ($id <= 0)
        {
            // Set the response and exit
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

        $this->load->model('mtour', '', true);
        $this->load->model('mtour', '', true);
        $this->load->model('mtour_inquery', '', true);
        $this->load->model('mtour_highlight', '', true);
        $this->load->model('mtour_review', '', true);
        $tourForDelete = $this->mtour->getRowById($id);
        if (empty($tourForDelete)) {
            $this->response(array(
                'errors' => $this->get_error_codes(array( 'code'=>'-1', 'message'=> 'Invalid ID specified', 'error_type'=>'invalid_request'), __FILE__, __LINE__),
            ), REST_Controller::HTTP_NOT_FOUND); //
        }
        $this->db->trans_start();

        $ToursList = $this->mtour->getToursList(false, '', array('filter_tour_id' => $id), '', '');
        $ToursIdDeletedList = array();
        foreach ($ToursList as $Tour) {
            $Res = $this->mtour_inquery->DeleteTour_InquerysByTourId($Tour['id']);
            $Res = $this->mtour_highlight->DeleteTour_HighlightsByTourId($Tour['id']);
            $Res = $this->mtour_review->DeleteTour_ReviewsByTourId($Tour['id']);
            $OkResult = $this->mtour->DeleteTour($Tour['id']);
            $ToursIdDeletedList[] = $Tour['id'];
        }


        $OkResult = $this->mtour->DeleteTour($id);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        } else {
            $this->db->trans_commit();
            foreach ($ToursIdDeletedList as $TourIdDeleted) {
                $dir_path = $app_config['document_root'] . DIRECTORY_SEPARATOR . $app_config['uploads_tours_dir'] . '-tour-' . $TourIdDeleted . DIRECTORY_SEPARATOR;
                AppUtils::DeleteDirectory($dir_path);
            }
        }
        if ($OkResult) {
            $message = [
                'id' => $id,
                'message' => 'Tour deleted with all related data.'
            ];
            $this->set_response($message, REST_Controller::HTTP_NO_CONTENT); // NO_CONTENT (204) being the HTTP response code
        }


    } // public function tours_delete() { // DELETE request - delete tour with all related data by tour id


    /////////////////  TOURS BLOCK END  ////////////////


    private function get_error_codes($db_error, $_file, $_line)
    {
        $ret_errors = array();
        if ( !empty($db_error) and is_array($db_error) ) {
            foreach( $db_error as $next_key=>$next_value ) {
                $ret_errors[$next_key]= $next_value;
            }
        }
        //$ret_errors = array('code'=>$db_error['code'], 'message'=>$db_error['message'] );
        if ( $this->input->get('d') == 99 ) {
            $ret_errors['file']= $_file;
            $ret_errors['line']= $_line;
        }
        return $ret_errors;
        //return json_encode($ret_errors);
    }
    public function get_ion_auth_config_tables() {
        return $this->ion_auth_config_tables;
    }





}
