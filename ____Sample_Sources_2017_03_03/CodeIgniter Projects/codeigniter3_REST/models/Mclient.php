<?php

class Mclient extends CI_Model
{
    private $db_prefix= 'jsn_';
    private $clientActiveLabelValueArray = Array('Y' => 'Active', 'N' => 'Inactive');

    public function __construct()
    {
        parent::__construct();
    }

    public function getClientStatusValueArray()
    {
        $ResArray = array();
        foreach ($this->clientActiveLabelValueArray as $Key => $Value) {
            $ResArray[] = array('key' => $Key, 'value' => $Value);
        }
        return $ResArray;
    }

    public function getClientStatusLabel($isActive)
    {
        if (!empty($this->clientActiveLabelValueArray[$isActive])) {
            return $this->clientActiveLabelValueArray[$isActive];
        }
        return '';
    }


    public function getClientsList($OutputFormatCount = false, $page = '', $filters = array(), $sort = '', $sort_direction = '')
    {
        $this->db->set_dbprefix($this->db_prefix);
        if (empty($sort)) {
            $sort = 'client.name';
            $sort_direction = 'asc';
        }
        $config_data = $this->config->config;
        if (empty($per_page)) $per_page = $config_data['per_page'];
        $limit = '';
        $offset = '';
        if (!empty($config_data) and $page > 0) {
            $limit = $per_page;
            $offset = ($page - 1) * $per_page;
        }

        if ( !empty($filters['limit']) ) {
            $limit= $filters['limit'];
        }
        if ( !empty($filters['offset']) ) {
            $offset= $filters['offset'];
        }

        if (!empty($rows_limit) and $rows_limit > 0) {
            $limit = $rows_limit;
            $this->db->limit($limit);
        }
        if (!$OutputFormatCount and isset($limit) and (int)$limit > 0 and isset($offset)/* and is_numeric($page) */) {
            $this->db->limit($limit, $offset);
        }

        if (!empty($filters['filter_is_active'])) {
            $this->db->where('client.is_active', $filters['filter_is_active']);
        }

        if (!empty($filters['filter_name'])) {
            $this->db->like('name', $filters['filter_name']);
        }

        if (!empty($sort)) {
            $this->db->order_by($sort, ( ( strtolower($sort_direction) == 'desc' or strtolower($sort_direction) == 'asc' ) ? $sort_direction : ''));
        }

        if ($OutputFormatCount) {
            return $this->db->count_all_results('client');
        } else {
            $this->db->distinct();
            $query = $this->db->from('client');
            return $query->get()->result('array');
        }
    }



    public function getRowById($id)
    {
        $this->db->set_dbprefix($this->db_prefix);
        $query = $this->db->get_where('client', array('client.client_id' => $id), 1, 0);
        $ResultRow = $query->result('array');
        if (!empty($ResultRow[0])) {
            return $ResultRow[0];
        }
        return false;
    }

    public function getRowByFieldName($field_name, $value, $get_multy_rows = false )
    {
        $this->db->set_dbprefix($this->db_prefix);
        if (!$get_multy_rows) {
            $query = $this->db->get_where('client', array($field_name => $value), 1, 0);
            $ResultRow = $query->result('array');
            if (!empty($ResultRow[0])) {
                return $ResultRow[0];
            }
        } else {
            $query = $this->db->get_where('client', array($field_name => $value));
            $ResultRows = $query->result('array');
            if (!empty($ResultRows)) {
                return $ResultRows;
            }
        }
        return false;
    }

    public function UpdateClient($id, $DataArray)
    {
        $this->db->set_dbprefix($this->db_prefix);
        if (empty($id)) {
            $DataArray['created_at'] = AppUtils::ShowFormattedDateTime(time(), 'MySql');
            $Res = $this->db->insert('client', $DataArray);
            if ($Res)
                return AppUtils::getMySqlInsertId($this->db->conn_id);
        } else {
            $Res = $this->db->update('client', $DataArray, array('client_id' => $id));
            if ($Res)
                return $id;
        }
    }

    public function DeleteClient($id)
    {
        $this->db->set_dbprefix($this->db_prefix);
        if (!empty($id)) {
            $this->db->where('client_id', $id);
            $Res = $this->db->delete('client');
            return $Res;
        }
    }

    public function getRowByPassword($password)
    {
        $this->db->set_dbprefix($this->db_prefix);
        $query = $this->db->get_where('client', array('password' => $password), 1, 0);
        $ResultRow = $query->result('array');
        if (!empty($ResultRow[0])) {
            return $ResultRow[0];
        }
        return false;
    }


}