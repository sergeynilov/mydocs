I used codeigniter-restserver library from ( https://github.com/chriskacerguis/codeigniter-restserver )
 to implement REST functionality in codeigniter 3 project.
 
 I made some small modifications in file /libraries/REST/REST_Controller.php for data format issue.
 
 As my task needed data checking I used validateData object in validateData.php file. Methods of this object were used in 2 controls 
 
 Clients_api.php
 and 
 Tours_service.php (the main file)
 
 Clients_api.php :
 adds/updates/delete/get client(s). On adding there is checks that name of client is unique. If checks fails error code and error message are returned.
 returning list of clients rows limit and offset could be set with sorting.
 
 Also look at related modal file /models/Mclient.php
 
 Tours_service.php  :
 It has methods for working with Users, Categories and Tours.
 Pay attention at validations data checking : required, valid integer, valid name field etc... Also for enum fields
 checks valid value given and for fields which are reference to other table checks reference for given value.
 I this task I tried to exclude situations of untreated errors for tables with many fields of different types and references to other tables.
 Also checks for unique values of fields.
 Deleting data with related rows in other tables I delete referenced rows at first and the data  after.
 Like user deleting...
 
 I use transactions when several sql-statements are run by request.
 
 Selecting sets of data filters and ordering can be set, they have corrisponding methods in model file. 
 
 
 Also look at related modal file /models/Mtour.php
 
 