<link type="text/css" rel="stylesheet" href="<?= base_url('assets/common') ?>/css/jQuery-File-Upload/jquery.fileupload.css" media="screen"/>
<script type="text/javascript" src="<?= base_url('assets/common') ?>/js/jQuery-File-Upload/vendor/jquery.ui.widget.js" charset="UTF-8"></script>
<script type="text/javascript" src="<?= base_url('assets/common') ?>/js/jQuery-File-Upload/jquery.iframe-transport.js" charset="UTF-8"></script>
<script type="text/javascript" src="<?= base_url('assets/common') ?>/js/jQuery-File-Upload/jquery.fileupload.js" charset="UTF-8"></script>


<script type="text/javascript" language="JavaScript" src="<?= base_url('assets/common') ?>/js/jquery.fancybox.pack.js"></script>
<link rel="stylesheet" href="<?= base_url('assets/common') ?>/css/jquery.fancybox.css" type="text/css" media="all"/>

<nav class="navbar navbar-default subnav hidden-xs" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Filters:</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

		    ...

        </div>
        <!-- /.navbar-collapse -->
    </div>
</nav>
<? if ($locations == TRUE): ?>
    <div id="main-content-box">
        <div id="state-map-canvas">
            <div id="no_results"><i class="fa fa-cog fa-spin fa-5x"></i><br>Loading map data...</div>
        </div>
        <div id="filter-link">
            <a href="#filterModal" data-backdrop="false" data-toggle="modal" class="btn btn-xs btn-primary visible-xs"><i class="fa fa-filter"></i> Edit Filters</a>

        </div>
        <div id="state-map-sidebar" class="hidden-xs" style="height:100%">
            <div id="results" class="container results-sm">
                <div id="property-listings">

                    <div id="no_results" style="display:none"><i class="fa fa-exclamation-triangle fa-5x"></i><br>No results found. Your filter parameters may be too strict. Try
                        changing them.
                    </div>
                    <div id="loc_results" class="row">
                        <div class="col-lg-12 nopad">

                            <? foreach ($locations as $location): ?>

                                <? if ($location->active == 1): ?>
                                    <div id="box_<?= $location->id ?>" class="property-listing visible">
                                        <div class="list-item">


                                            <div class="clearfix visible-sm"></div>

                                            <div class="listing-body fnt-smaller">

											    ...

                                                <div class="more_info sbtabs">


                                                    <ul class="nav nav-tabs" role="tablist">
                                                        <li class="active"><a href="#overview_<?= $location->id ?>" role="tab" data-toggle="tab">Overview</a></li>
                                                        <li><a href="#w2g_<?= $location->id ?>" role="tab" data-toggle="tab">Hunting</a></li>

                                                        <li><a href="#links_<?= $location->id ?>" role="tab" data-toggle="tab">More</a></li>
                                                        <li><a href="#photos_<?= $location->id ?>" role="tab" data-toggle="tab">Photos</a></li>
                                                        <li><a href="#reviews_<?= $location->id ?>" role="tab" data-toggle="tab">Reviews</a></li>

                                                    </ul>

                                                    <!-- Tab panes -->
                                                    <div class="tab-content">

                                                        <div class="tab-pane active" class="overview_tab" id="overview_<?= $location->id ?>">

                                                            ...     

                                                        </div>

                                                        <div class="tab-pane" class="w2g_tab" id="w2g_<?= $location->id ?>" style="overflow:hidden">

                                                            ...
															
                                                        </div>

                                                        <div class="tab-pane" class="links_tab" id="links_<?= $location->id ?>" style="overflow:hidden">
                                        
                                                            ...

                                                        </div>

                                                        <div class="tab-pane" class="photos_tab" id="photos_<?= $location->id ?>" style="overflow:hidden">


                                                            <div id="div_upload_photo_<?= $location->id ?>">
                                                                <?php if ($is_user_logged_in) : ?>
                                                                Upload your photo:&nbsp;
                                                                <span class="btn btn-success fileinput-button">
                                                                    <i class="glyphicon glyphicon-plus"></i>
                                                                    <span>Select file</span>
                                                                    <input id="fileupload" class="land_fileupload" type="file" name="files[]" multiple>
                                                                </span>
                                                            <?php else: ?>
                                                                <span ><b>You must login to upload photo</b></span>
                                                            <?php endif; ?>
                                                            </div>

                                                            <div id="div_save_upload_photo_<?= $location->id ?>" style="display: none">
                                                                <table>
                                                                    <tr>
                                                                      <td rowspan="3">
                                                                          <img id="img_preview_image_<?= $location->id ?>" alt="Preview" title="Preview" width="128" height="128" style="border: 0px dotted red;">
                                                                          <input type="hidden" id="hidden_selected_photo_<?= $location->id ?>">
                                                                      </td>
                                                                    </tr>
                                                                    <tr>
                                                                      <td colspan="1">&nbsp;&nbsp;
                                                                          <input id="input_photo_description_<?= $location->id ?>" type="text" maxlength="120" size="40" placeholder="Enter photo description here">
                                                                      </td>
                                                                    </tr>
                                                                    <tr>
                                                                      <td >&nbsp;&nbsp;
                                                                          <button type="button" class="btn btn-default" data-dismiss="modal" onclick="javascript:UploadPhoto('<?= $location->id ?>');"><i></i>Upload Photo</button>&nbsp;&nbsp;&nbsp;&nbsp;
                                                                          <button type="button" class="btn btn-default" data-dismiss="modal" onclick="javascript:CancelUploadPhoto('<?= $location->id ?>');"><i></i>Cancel</button>
                                                                      </td>
                                                                    </tr>
                                                                </table>
                                                            </div>

                                                            <div id="div_loaded_photos_<?= $location->id ?>" ></div>


                                                        </div>
                                                        <div class="tab-pane" class="reviews_tab" id="reviews_<?= $location->id ?>" style="overflow:hidden">
                                                            <?php if ($is_user_logged_in) : ?>
                                                            <div id="div_add_your_own_comment_<?= $location->id ?>">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal" onclick="javascript:showAddReview('<?= $location->id ?>');"><i></i>Add Your Own Comment</button>
                                                            </div>
                                                            <?php else: ?>
                                                                <span ><b>You must login to add review</b></span>
                                                            <?php endif; ?>

                                                            <div id="div_loaded_reviews_<?= $location->id ?>" ></div>
                                                            <div id="div_your_own_comment_block_<?= $location->id ?>" style="display:none">
                                                                <textarea id="textarea_add_your_own_comment_<?= $location->id ?>" rows="10" cols="60"></textarea><br>
                                                                <button type="button" class="btn btn-default" onclick="javascript:saveReview('<?= $location->id ?>');">Submit</button>
                                                                &nbsp;&nbsp;&nbsp;
                                                                <button type="button" class="btn btn-cancelt" onclick="javascript:cancelReview('<?= $location->id ?>');">Cancel</button>
                                                            </div>


                                                        </div>
                                                        <ul class="bot_links list-inline" style="padding-top:10px;border-top:1px solid #C0CEAD;margin-top:10px;margin-bottom:-15px">
                                                            <li><a href="/land/<?= $location->id ?>" target="_blank"><i class="fa fa-link"></i> Permalink</a></li>
                                                            <li><a href="http://maps.google.com/?q=<?= $location->lat ?>,<?= $location->lng ?>" target="_blank"><i
                                                                        class="fa fa-globe"></i> View on google maps</a></li>
                                                            <li><a href="#" class="suggest hidden-xs" data-land-id="<?= $location->id ?>" data-land-name="<?= $location->name ?>"><i
                                                                        class="fa fa-eraser"></i> Suggest a change or correction</a></li>
                                                        </ul>


                                                    </div>


                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- End Listing-->
                                <? endif; ?>
                            <? endforeach; ?>


                        </div>


                    </div>
                </div>
                <div id="working" style="display:none">Updating results...<br><i class="fa fa-cog fa-spin fa-5x"></i></div>
            </div>
        </div>
    </div>


    <div id="filters" class="container" style="display:none;border-top:1px solid #fff">


        <div class="row imgBkTxt" id="filter-tools">
            <div class="col-lg-12" style="padding-top:20px;padding-bottom:20px">
         
		        ...

            </div>
        </div>
    </div>

    <div id="filterModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
   
                ...
				
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


    <div class="modal fade" id="mobileLand" tabindex="-1" role="dialog" aria-labelledby="mobileLandLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h3 id="mobileLandLabel"></h3>
                </div>
                <div class="modal-body">


                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#overview_mobile" role="tab" data-toggle="tab">Overview</a></li>
                        <li><a href="#w2g_mobile" role="tab" data-toggle="tab">Hunting</a></li>
                        <li><a href="#links_mobile" role="tab" data-toggle="tab">More</a></li>
                        <li><a href="#links_hotos" role="tab" data-toggle="tab">Photos</a></li>
                        <li><a href="#links_reviews" role="tab" data-toggle="tab">Reviews</a></li>
                        <? foreach ($location->w2g as $w2g): ?>
                            <!-- <li><a href="#<?= $w2g['system_name'] ?>_<?= $location->id ?>" role="tab" data-toggle="tab"><?= $w2g['name'] ?></a></li> -->
                        <? endforeach; ?>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">

                        <div class="tab-pane active" class="overview_tab" id="overview_mobile">

                        </div>

                        <div class="tab-pane" class="w2g_tab" id="w2g_mobile" style="overflow:hidden">


                        </div>

                        <div class="tab-pane" class="links_tab" id="links_mobile" style="overflow:hidden">


                        </div>

                        <div class="tab-pane" class="photos_tab" id="photos_mobile" style="overflow:hidden">


                        </div>
                        '
                        <div class="tab-pane" class="reviews_tab" id="reviews_mobile" style="overflow:hidden">


                        </div>


                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDacAVvTv3dOmcO3h5RNGdGPi5RHQpEprQ"></script>
    <script src="<?= base_url() ?>assets/common/js/markerclusterer.js"></script>

    <script src="<?= base_url() ?>assets/frontend/js/jquery.media.js"></script>

    <script type="text/javascript">

        var reviewsCurrentPage= 1
        function serialize_form() {
            return $('#myform').serialize();
        }
        /*  PHOTOS BLOCK START  */

        function UploadPhoto(location_id) {
            var input_photo_description= jQuery.trim( $("#input_photo_description_" + location_id).val() );
            var hidden_selected_photo= $("#hidden_selected_photo_" + location_id).val();

            if ( input_photo_description == "" ) {
                alert( "Enter photo description !" )
                $("#input_photo_description_" + location_id).focus()
                return;
            }
            $.get( "/json/save_photo/", { land_id: location_id, description : input_photo_description, photo_name : hidden_selected_photo  }, function( data, status ) {
                CancelUploadPhoto(location_id)
                loadLandPhotos(location_id)
            });

        }

        function CancelUploadPhoto(location_id) {
            $("#div_upload_photo_" + location_id).css("display", "block");
            $("#div_save_upload_photo_" + location_id).css("display", "none");
            $("#img_preview_image_" + location_id).attr( "src","" );
            $("#input_photo_description_" + location_id).val( "" );
            $("#hidden_selected_photo_" + location_id).val( "" );
        }

        function loadLandPhotos(location_id) {
            $('.land_fileupload').fileupload({
                url: '/json/upload_state_photo?land_id=' + location_id,
                dataType: 'json',
                done: function (e, data) {

                    $("#div_upload_photo_" + location_id).css("display", "none");
                    $("#div_save_upload_photo_" + location_id).css("display", "block");
                    $("#img_preview_image_" + location_id).attr( "src",data.result.files.url );

                    $("#img_preview_image_" + location_id).attr( "width",data.result.files.FilenameInfo.Width );
                    $("#img_preview_image_" + location_id).attr( "height",data.result.files.FilenameInfo.Height );

                    $("#hidden_selected_photo_" + location_id).val( data.result.files.name );
                },
                progressall: function (e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('#progress .progress-bar').css(
                        'width',
                        progress + '%'
                    );
                }
            }).prop('disabled', !$.support.fileInput)
                .parent().addClass($.support.fileInput ? undefined : 'disabled');


            $.get("/json/load_photos/", {land_id: location_id }, function (data, status) {
                $("#div_loaded_photos_" + location_id).html(data);
            });

            $(".fancybox-button").fancybox({ 
                prevEffect: 'none',
                nextEffect: 'none',
                closeBtn: true,
                openEffect: 'none',
                closeEffect: 'none',
                'titlePosition' 	: 'over',
                helpers: {
                    title: { type: 'inside' },
                    buttons: {}
                }
            });
        }

        function setPhotoAsInappropriate(land_id, photo_id) {
            if ( !confirm("Do you want to set this Photo As Inappropriate ?") ) return;
            $.get( "/json/set_photo_as_inappropriate/", { land_id : land_id, photo_id: photo_id  }, function( data, status ) {
                CancelUploadPhoto(land_id)
                loadLandPhotos(land_id)
            });

        }

        /*  PHOTOS BLOCK END  */


        /*  REVIEWS BLOCK START  */
        function showAddReview(location_id) {
            $("#div_add_your_own_comment_" + location_id).css("display", "none");
            $("#div_your_own_comment_block_" + location_id).css("display", "block");
            $("#textarea_add_your_own_comment_" + location_id).val("");
            $("#textarea_add_your_own_comment_" + location_id).focus();
        }

        function cancelReview(location_id) {
            $("#div_your_own_comment_block_" + location_id).css("display", "none");
            $("#div_add_your_own_comment_" + location_id).css("display", "block");
            $("#textarea_add_your_own_comment_" + location_id).val("");
        }

        function loadLandReviews(location_id, page, obj) {
            reviewsCurrentPage= page
            $.get("/json/load_reviews/", {land_id: location_id, page : page }, function (data, status) {
                $("#div_loaded_reviews_" + location_id).html(data);
            });
        }

        function setReviewAsInappropriate(land_id, review_id) {
            if ( !confirm("Do you want to set this Review As Inappropriate ?") ) return;
            $.get( "/json/set_review_as_inappropriate/", { land_id : land_id, review_id: review_id  }, function( data, status ) {
                cancelReview(land_id)
                loadLandReviews(land_id, reviewsCurrentPage)
            });
        }

        function saveReview(location_id) {
            var reviewText=  jQuery.trim( $("#textarea_add_your_own_comment_" + location_id).val() );
            if ( reviewText == "" ) {
                alert( "Enter review text !" )
                $("#textarea_add_your_own_comment_" + location_id).focus()
                return;
            }
                $.get( "/json/save_review/", { land_id: location_id, review_text : reviewText  }, function( data, status ) {
                cancelReview(location_id)
                loadLandReviews( location_id, 1 )
            });
        }  // function saveReview(location_id) {
        /*  REVIEWS BLOCK END  */


        $(function () {


             ...
            function initialize() {
                var skip_map= GetParameterValue('skip_map')
                //alert( "skip_map::"+skip_map)
                if (skip_map == '1') return;

				...
				
                });
            }

            google.maps.event.addDomListener(window, 'load', initialize);
            process_selectors();
        });

        function var_dump(oElem) {
            var sStr = '';
            if (typeof(oElem) == 'string' || typeof(oElem) == 'number') {
                sStr = oElem;
            } else {
                var sValue = '';
                for (var oItem in oElem) {
                    sValue = oElem[oItem];
                    if (typeof(oElem) == 'innerHTML' || typeof(oElem) == 'outerHTML') {
                        sValue = sValue.replace(/</g, '&lt;').replace(/>/g, '&gt;');
                    }
                    sStr += 'obj.' + oItem + ' = ' + sValue + '\n';
                }
            }
            return sStr;
        }


        function GetParameterValue(ParameterName) {
            if (location.search.length > 1) {
                var QueryString = location.search.substring(1, location.search.length);
                var ParameterValueArr = QueryString.split('&');
                for (var I = 0; I < ParameterValueArr.length; I++) {
                    var Element = ParameterValueArr[I].split('=');
                    if (ParameterName == Element[0]) {
                        return Element[1];
                    }
                }
            }
            return '';
        }

    </script>
<? endif; ?>