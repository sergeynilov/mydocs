<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Categories extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        if (!$this->authentication->status()) {
            redirect('login?rt=dashboard', 'refresh');
        }
        // Forbid cache
        $this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
        $this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
        $this->output->set_header('Pragma: no-cache');
    }

    function index()
    {
        $data['filter_name']        = !empty($_REQUEST['filter_name']) ? $_REQUEST['filter_name'] : '';;
        $data['userdata']           = $this->session->userdata();
        $data['editor_message']     = $this->Usermanagement->get_editor_message();
        $data['FlyerTemplates']     = $this->FlyerTemplates;
        $data['categoryList']       = $this->FlyerTemplates->getCategoriesList(false,  '', array('name'=> $data['filter_name'], 'show_related_count'=>1) );
        $this->load->view( 'categories/list', $data );
    }

    function edit()
    {
        $uriArray = $this->uri->uri_to_assoc(3);
        $isInsert= (!empty($uriArray['edit']) and $uriArray['edit'] =='new');
        $data['userdata']           = $this->session->userdata();
        $data['editor_message']     = $this->Usermanagement->get_editor_message();
        $data['isInsert']           = $isInsert;
        $data['id']                 = $isInsert ? '' : $uriArray['edit'];
        if (!$isInsert) {
            $data['category']= $this->FlyerTemplates->getCategoryRowById($data['id']);
            if (empty($data['category'])) {
                $this->Usermanagement->set_editor_message("Category '" . $data['id'] . "' not found !" );
                redirect( site_url( "categories/index/" ) );
                return;
            }
        } else {
            $data['category']= array('id'=>'new', 'name'=>'', 'description'=>'', 'status'=> '', 'wp_id'=> '');
        }

        if ($isInsert) {
            $this->form_validation->set_rules('name', 'Name', 'required|is_unique[dp_categories.name]');
        } else {
            $this->form_validation->set_rules('name', 'Name', 'required|is_unique[dp_categories.name.id.' . $data['id'] . ']');
        }
        $this->form_validation->set_rules('description', 'Description', 'required');

        $form_status = 'edit';
        if (!empty($_POST)) { // form was submitted
            $validation_status = $this->form_validation->run();
            if ($validation_status != FALSE) {
                $ret_id = $this->FlyerTemplates->UpdateCategory($this->input->post('id'), Array( 'name' => AppUtils::slashStr($this->input->post('name')),  'description' => AppUtils::slashStr($this->input->post('description') ) ), true );
                if ($ret_id) {
                    $this->Usermanagement->set_editor_message("Category '" . $this->input->post('name') . "' was " . ($isInsert ? "inserted" : "updated"));
                    redirect( site_url( ( $isInsert ?  "categories/edit/" . $ret_id : "categories/index/" ) ) );
                    return;
                }
            } else {
                $form_status = 'invalid';
                $data['category']= array('id'=> ( $isInsert ? "new" : $data['id'] ), 'status'=> ( !empty($data['category']['status']) ? $data['category']['status'] : '' ), 'name'=> set_value('name'), 'description'=>set_value('description'), 'status'=> set_value('status'), 'wp_id'=> set_value('wp_id') );
                $data['validation_errors_text']= validation_errors();

            }
        } // if (!empty($_POST)) { // form was submitted

        $data['csrf_token_name_hidden'] = '';
        $data['form_status'] = $form_status;
        $data['FlyerTemplates']       = $this->FlyerTemplates;
        $this->load->view( 'categories/edit', $data );
    }


    function is_category_unique() {
        $categoriesList = $this->FlyerTemplates->getCategoriesList(false,  '', array('strict_name'=> ( !empty($_REQUEST['name']) ? $_REQUEST['name'] : '' ), 'id!='=> ( ( !empty($_REQUEST['id']) and $_REQUEST['id']!= 'new') ? $_REQUEST['id'] : '' ) ) );
        if (count($categoriesList)> 0) {
            echo "false";
        } else {
            echo "true";
        }
    }

    function load_category_images() {
        $category_id= ( !empty($_REQUEST['category_id']) ? $_REQUEST['category_id'] : '');
        $category_name= ( !empty($_REQUEST['category_name']) ? $_REQUEST['category_name'] : '');

        $base_dir= '';
        $CIObj =& get_instance();
        if ( !empty( $CIObj->config->config['base_dir'] ) ) {
            $base_dir = $CIObj->config->config['base_dir'];
        }
        $category_path= $base_dir . '/static/uploads/themes/'. $category_id;
        $category_dir_url= base_url() . 'static/uploads/themes/'. $category_id;

        $categoryImagesList = get_dir_file_info( $base_dir . '/static/uploads/themes/'. $category_id, false );
        if (count($categoryImagesList)== 0) {
            echo json_encode( array('result'=>'success', 'ret_html'=> 'No Images Found ! ' ) );
        } else {
            $ret_html= '<table class="" border="1px dotted grey">';
            $Counter = 0;
            $orig_width=100;
            $orig_height= 80;
            $galleryImagesPerRow= 5;
            foreach( $categoryImagesList as $nextCategoryImage ) {
                $filename_path = $category_path . '/'. $nextCategoryImage['name'];
                $filename_url = $category_dir_url . '/'. $nextCategoryImage['name'];
                $filesizeLabel = $this->FlyerTemplates->getFileSizeAsString( $nextCategoryImage['size'] );
                $FilenameInfo = $this->FlyerTemplates->GetImageShowSize($filename_path, $orig_width, $orig_height, false);

                $deletelinkHTML = $nextCategoryImage['name'] .  '<a style="cursor:pointer" onclick="javascript:deleteCategoryImage(\'' . $nextCategoryImage['name'] . '\',\'' . $category_id . '\');" ><img src="' .base_url() . 'static/images/delete.png"></a>';

                $ret_html .= (($Counter % $galleryImagesPerRow) == 0 ? '</tr><tr>' : '') . '<td style="padding:2px;"><a class="fancybox-button" rel="fancybox-button" alt="' . $nextCategoryImage['name'] . '"  title="' . $nextCategoryImage['name'] . '" href="' . $filename_url . '"><img style="padding:2px;" src="' . $filename_url . '" width="' . $FilenameInfo['Width'] . '" height="' .
                    $FilenameInfo['Height'] . '" /></a>&nbsp;&nbsp;<br>&nbsp;' . $deletelinkHTML . '&nbsp;&nbsp;' . $filesizeLabel . ', &nbsp;' . $FilenameInfo['OriginalWidth'] . '*' . $FilenameInfo['OriginalHeight'] . '</td>';

                $Counter++;
            }

            $ret_html.= '</table>';
            echo json_encode( array('result'=>'success', 'ret_html'=> $ret_html ) );
        }
        exit;
    }

    function delete_category_image()
    {
        $category_id = (!empty($_REQUEST['category_id']) ? $_REQUEST['category_id'] : '');
        $base_dir= '';
        $CIObj =& get_instance();
        if ( !empty( $CIObj->config->config['base_dir'] ) ) {
            $base_dir = $CIObj->config->config['base_dir'];
        }
        $category_path= $base_dir . '/static/uploads/themes/'. $category_id;

        $image_name = (!empty($_REQUEST['image_name']) ? $_REQUEST['image_name'] : '');
        $filepath= $category_path . '/' . $image_name;
        if ( file_exists($filepath) and !is_dir($filepath)) {
            $del= unlink($filepath);
        }
        echo json_encode( array('result'=>'success', 'filepath'=> $filepath ) );

    }

    function upload_category_image()
    {
        $base_dir= '';
        $CIObj =& get_instance();
        if ( !empty( $CIObj->config->config['base_dir'] ) ) {
            $base_dir = $CIObj->config->config['base_dir'];
        }

        $category_name = (!empty($_REQUEST['category_name']) ? $_REQUEST['category_name'] : '');
        $category_id = (!empty($_REQUEST['category_id']) ? $_REQUEST['category_id'] : '');
        $orig_width = 100;
        $orig_height = 80;
        if (empty($_FILES['files']['tmp_name'][0]) or empty($_FILES['files']['name'][0])) {
            return json_encode(array('ErrorMessage' => 'File not uploaded', 'ErrorCode' => 1, 'dest_filename' => '', 'src_filename' => ''));
        }
        $src_filepath = $_FILES['files']['tmp_name'][0]; // path of loaded file
        $src_filename = $_FILES['files']['name'][0];  // name of loaded file

        $tmp_dest_dirname = $base_dir . '/static/uploads/tmp/category-images/category-image-' . $category_id . '-' . session_id();
        $tmp_dest_dirname_url = base_url() . '/static/uploads/tmp/category-images/category-image-' . $category_id . '-' . session_id();
        $tmp_dest_filename = $tmp_dest_dirname . '/' . $src_filename;
        $tmpCategoryImagesDirs = array( $base_dir . '/static/uploads', $base_dir . '/static/uploads/tmp', $base_dir . '/static/uploads/tmp/category-images/', $tmp_dest_dirname);
        $this->FlyerTemplates->create_dir($tmpCategoryImagesDirs);
        $ret = move_uploaded_file($src_filepath, $tmp_dest_filename);

        $filesize = filesize($tmp_dest_filename);
        $resArray = array("files" => array("name" => $src_filename,
            "size" => $filesize,
            'FilenameInfo' =>$this->FlyerTemplates->GetImageShowSize($tmp_dest_filename, $orig_width, $orig_height),
            "sizeLabel" => $this->FlyerTemplates->getFileSizeAsString($filesize),
            "url" => $tmp_dest_dirname_url . '/' .  $src_filename . '?tm=' . time(),
        ));
        echo json_encode($resArray);
    } // function upload_state_photo()


    function save_category_image()
    {
        $category_id = $this->input->get('category_id');
        $category_image = $this->input->get('category_image');
        $category_name = $this->input->get('category_name');

        $base_dir= '';
        $CIObj =& get_instance();
        if ( !empty( $CIObj->config->config['base_dir'] ) ) {
            $base_dir = $CIObj->config->config['base_dir'];
        }

        $tmp_src_dirname= $base_dir . '/static/uploads/tmp/category-images/category-image-' . $category_id . '-' . session_id();
        $tmp_src_filename= $tmp_src_dirname . '/' . $category_image;

        $dest_dirname= $base_dir . '/static/uploads/themes/'. $category_id;


        $dest_filename= $dest_dirname . '/' . $category_image;
        $categoriesImagesDirs= array( $base_dir.'uploads', $base_dir.'/static/uploads/category-images/', $dest_dirname );
        $this->FlyerTemplates->create_dir( $categoriesImagesDirs );
        $ret= copy($tmp_src_filename, $dest_filename);

        if (!$ret ) {
            $this->output->set_content_type('application/json')->set_output(json_encode(array('ErrorMessage' => 'Can not copy file from tmp directory!', 'ErrorCode' => 1, 'new_image_id' => null)));
            return;
        }
        $this->FlyerTemplates->DeleteDirectory($tmp_src_dirname);
        echo json_encode( array('result'=>'success', 'filepath'=> $dest_filename ) );
    }

    ////////////////////////
    function add_category_image_from_template()
    {

        $category_id = !empty($_GET['category_id']) ? $_GET['category_id'] : '';
        $category_image = $_FILES['files']['name'][0];

        $base_dir= '';
        $CIObj =& get_instance();
        if ( !empty( $CIObj->config->config['base_dir'] ) ) {
            $base_dir = $CIObj->config->config['base_dir'];
        }

        $tmp_src_filename= $_FILES['files']['tmp_name'][0];

        $dest_dirname= $base_dir . '/static/uploads/themes/'. $category_id;
        $dest_filename_url= base_url() . '/static/uploads/themes/'. $category_id . '/' . $category_image;
        $dest_filename= $dest_dirname . '/' . $category_image;
        $categoriesImagesDirs= array( $base_dir.'uploads', $base_dir.'/static/uploads/category-images/', $dest_dirname );

        $this->FlyerTemplates->create_dir( $categoriesImagesDirs );
        $ret= copy($tmp_src_filename, $dest_filename);

        if (!$ret ) {
            echo json_encode( array('result'=>'failure', 'ErrorMessage' => 'Can not copy file from tmp directory!' ) );
            return;
        }
        $orig_width= 100;
        $orig_height= 80;
        $resArray = array("files" => array("name" => $category_image,
            "size" => filesize($dest_filename),
            'FilenameInfo' =>$this->FlyerTemplates->GetImageShowSize($dest_filename, $orig_width, $orig_height),
            "sizeLabel" => $this->FlyerTemplates->getFileSizeAsString(filesize($dest_filename)),
            "url" => $dest_filename_url . '?tm=' . time(),
        ));
        echo json_encode($resArray);

    }

    function delete()
    {
        $uriArray = $this->uri->uri_to_assoc(3);
        $category= $this->FlyerTemplates->getCategoryRowById($uriArray['delete']);
        if (empty($category)) {
            $this->Usermanagement->set_editor_message( "Category not found !" );
            redirect(site_url(  "categories/index/" ));
            return;
        }

        $this->FlyerTemplates->DeleteCategory($uriArray['delete']);


        $this->Usermanagement->set_editor_message( "Category was deleted !" );
        redirect(site_url(  "categories/index/" ));
    }

    function get_image_size()
    {
        $file_url = $this->input->get('file_url');
        $server_path = $this->input->get('server_path');
        $FilenameInfo = $this->FlyerTemplates->GetImageShowSize(base_url().$file_url, 100, 80, false);
        if ( empty($FilenameInfo) ) {
            echo json_encode(array('result' => 'failure', 'file_info' => array('file_url' => $file_url)));

        } else {
            echo json_encode(array('result' => 'success', 'file_info' => array('file_url' => base_url().$file_url, 'width'=>$FilenameInfo['Width'],
                'height'=>$FilenameInfo['Height'], 'original_width'=>$FilenameInfo['OriginalWidth'],
                'original_height'=>$FilenameInfo['OriginalHeight'], "sizeLabel" => $this->FlyerTemplates->getFileSizeAsString(filesize($server_path))) ) );
        }
    }

}
/* End of file Categories.php */
/* Location: application/controllers/Categories.php */