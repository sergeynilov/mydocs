<?php
$this->load->view( 'common/header' );
load_menu();
$section_title = lang( 'ux_flyers' );

$CIObj =& get_instance(); // nsn
$base_root_dir= '';
if ( !empty( $CIObj->config->config['base_root_dir'] ) ) {
    $base_root_dir = $CIObj->config->config['base_root_dir'];
}

?>
<div id="saving-overlay"></div>
<div class="flyers-section">

    <script type="text/javascript" src="<?php echo base_url() ?>static/scripts/bootstrap-select.min.js" charset="utf-8"></script>
    <script type="text/javascript" charset="utf-8">
        //<![CDATA[

        function uploadedFlyerPathClicked( filename, fileUrl ) {
            jQuery.ajax({
                url: "<?php echo site_url( 'categories/get_image_size?file_url=' ) ?>"+fileUrl,
                type: 'GET',
                dataType: 'json',
                success: function(result) {
                    //alert( "result::"+var_dump(result) )
                    if (result.result == 'success' ) {
                        var url = "<?php echo base_url(); ?>" + fileUrl + "?tm=" + ( new Date().getTime() )
                        $("#img_preview").attr( "src", result.file_info.file_url )
                        $("#img_preview").css("width", result.file_info.width)
                        $("#img_preview").css("height", result.file_info.height)
                        $("#span_img_preview_info").html( "  "+result.file_info.sizeLabel+ ", " + result.file_info.original_width +"*"+ result.file_info.original_height )
                    }
                }
            });

        }

        function templ_edit_category_idOnChange() {
            // Upload Folder
            var templateCategory = $("#templ_edit_category_id option:selected").text();
            var templateCategoryID = $("#templ_edit_category_id option:selected").val();

            var url = "<?php echo site_url('categories/add_category_image_from_template?category_id') ?>=" + templateCategoryID+"&category_name="+encodeURIComponent(templateCategory)
            // Upload Function
            jQuery('#fileupload').fileupload({
                url: url,
                dataType: 'json',
                done: function (e, data) {
                    jQuery.each(data.result.files, function (index, file) {
                        //alert( "index::"+var_dump(index) + "  file::"+var_dump(file) )
                        if( file.error !== undefined ) {
                            window.alert( file.error );
                        } else {
                            if( file.name != null ) {
                                var imageInput = jQuery('<div class="row" id="'+file.name+'"><div class="col-lg-10"><input type="text" class="imageSrc form-control" value="'+url+file.url+'" /></div><div class="col-lg-2"><button class="btn btn-danger deleteSrc" data-src="'+file.name+'"><?php echo lang("ux_remove"); ?></button></div></div>');
                                imageInput.appendTo('#files');
                            }
                            jQuery('.deleteSrc').on('click', function(){
                                jQuery('#files #'+jQuery(this).attr('data-src')).remove();
                            });
                        }
                    });
                    // Load files
                    var templateCategory = $("#templ_edit_category_id option:selected").text();
                    var templateCategoryID = $("#templ_edit_category_id option:selected").val();
                    _uploadedImages( templateCategory, templateCategoryID );
                },
                progressall: function (e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    jQuery('#progress .progress-bar').css(
                        'width',
                        progress + '%'
                    );
                    if( progress == '100' ) {
                        jQuery('#progress .progress-bar').addClass('progress-bar-success');
                    }
                }
            }).prop('disabled', !jQuery.support.fileInput)
                .parent().addClass(jQuery.support.fileInput ? undefined : 'disabled');
        }

        /**
         * Load Uploaded Flyers
         */
        function _uploadedImages(templateCategory, templateCategoryID) {
            // Clean container
            jQuery('.uploadedFlyers').empty();
            // Load list
            jQuery.ajax({
                url: "<?php echo site_url( 'dashboard/getUploadedFlyers' ) ?>",
                type: 'GET',
                success: function(result) {
                    // Parse JSON Data
                    var data = jQuery.parseJSON(result);
                    // Loop through data
                    jQuery.each(data, function(){
                        // Build links for every file except thumbnails
                        if( this.name != 'thumbnail' && this.category_name == templateCategoryID ) {
                            var relativeUrl= this.server_path.substring(this.server_path.indexOf("/static/"))
                            var imageItem = jQuery('<a class="list-group-item uploadedFlyerPath"  onclick="javascript:uploadedFlyerPathClicked( \''+this.name+'\',  \''+relativeUrl+'\');' +     '" ></a>');
                            imageItem.text(this.name);
                            imageItem.attr('href', this.server_path.substring(this.server_path.indexOf("/static/")));
                            imageItem.attr('id', this.name.replace('.',''));
                            imageItem.append('<span class="badge badge-warning"><span class="glyphicon glyphicon-remove-circle deleteImage"></span></span>');
                            jQuery('.uploadedFlyers').append(imageItem);
                        }
                    });
                }
            });
            // Detect mouse events on Uploaded Flyers & Files window
            jQuery('.uploadedFlyers, .files').on('mouseover', function(n) {
                // Act when a link tag has been pointed
                if( n.target.tagName == 'A' ) {
                    // Remove image from server
                    jQuery('.deleteImage').unbind('click').on('click', function(){
                        // Ask for confirmation
                        if( window.confirm(' <?php echo lang("ux_confirmDialog"); ?>') ) {
                            // Ajax request
                            jQuery.ajax({
                                url: "<?php echo site_url( 'dashboard/deleteBgImage' ) ?>",
                                type: 'GET',
                                data: {
                            <?php echo $this->security->get_csrf_token_name(); ?>:"<?php echo $this->security->get_csrf_hash(); ?>",
                                file: jQuery(this).parent().parent().attr('href').substring(jQuery(this).parent().parent().attr('href').indexOf("/static/"))
                        },
                        success: function(result){
                            // Reload images list
                            if( result === 'true' ) {
                                var templateCategory = $("#templ_edit_category_id option:selected").text();
                                var templateCategoryID = $("#templ_edit_category_id option:selected").val();
                                _uploadedImages( templateCategory, templateCategoryID );
                            }
                        }
                    });
                }
            });
            // Add image source
            jQuery(this).on('click', function(e){
                // Prevent link default behavior
                e.preventDefault();
                // Clean files wrapper
                jQuery('#files').empty();
                // Target link to image sources only
                if( e.target.href != undefined && e.target.href.match(/static/) != null ) {
                    var imageInput = jQuery('<div class="row" id="'+e.target.id+'"><div class="col-lg-10"><input type="text" class="imageSrc form-control" value="'+e.target.href.substring(e.target.href.indexOf("/static/"))+'" /></div><div class="col-lg-2"><button class="btn btn-danger deleteSrc" data-src="'+e.target.id+'"><?php echo lang("ux_remove"); ?></button></div></div>');
                    imageInput.appendTo('#files');
                }
                jQuery('.deleteSrc').on('click', function(){
                    jQuery('#files #'+jQuery(this).attr('data-src')).remove();
                });
            });
        }


        });
        }

        jQuery(document).ready(function ($) {
            $("#name").focus()

            $("#form_template_edit").validate({
                rules: {
                    templ_edit_name: {
                        required: true,
                        remote: "<?php echo site_url("templates/is_template_unique?id=" . $template['id'] . '&') ?>",
                    },
                    templ_edit_category_id : {
                        required: true
                    },
                    templ_edit_description : {
                        required: true
                    },
                    templ_edit_format : {
                        required: true
                    },
                    templ_edit_fonts : {
                        required: true
                    },
                },
                messages: {
                    templ_edit_name: {
                        required:"Enter name of template",
                        remote: "This template name is already taken! Try another."
                    },
                    templ_edit_category_id: { required: "Select category of template" },
                    templ_edit_description: { required: "Select description of template" },
                    templ_edit_format: { required: "Select format of template" },
                    templ_edit_fonts: { required: "Select fonts of template" }
                },
                submitHandler: function(form) {
                    stage.deactivateAll();
                    // Update stage
                    _stageUpdate();
                    _saveTemplate();
                }
            });


            /**
             * Select Pickers
             */
            jQuery('.flyerSize').selectpicker({
                style: 'btn btn-default',
                size: 'auto',
                width: '20%',
            });
            jQuery('.flyerOrientation').selectpicker({
                style: 'btn btn-default',
                size: 'auto',
                width: '25%',
            });
            /**
             * Color Picker
             */
            var textColorPicker = jQuery('.fontColor').colorpicker({
                parts:          'full',
                alpha:          true,
                showOn:         'click focus',
                colorFormat: 	'RGBA',
            });
            var backgroundColorPicker = jQuery('.backColor').colorpicker({
                parts:          'full',
                alpha:          true,
                showOn:         'click focus',
                colorFormat: 	'RGBA',
            });
            var stageColorPicker = jQuery('.stageColor').colorpicker({
                parts:          'full',
                alpha:          true,
                showOn:         'click focus',
                colorFormat: 	'RGBA',
            });
            /**
             * Change Background Color
             */
            jQuery('.openBackColorPicker').on('click tap', function(event){
                event.stopPropagation();
                stageColorPicker.colorpicker('open');
                jQuery('.stageColor').attrchange({
                    trackValues: true,
                    callback: function(event) {
                        if( event.attributeName == 'value' ) {
                            // Background color
                            _stageBackgroundColor( event.newValue, '' );
                            // Make history
                            _makeHistory( stage );
                        }
                    }
                });
            });
            /**
             * Parameters
             */
            var params 				= { width: '', height: '' };
            var stage 				= '';
            var stageChildren 		= '';
            var templateFonts 		= '';
            var countOrigChildren 	= 0;
            var initialScale 		= '';
            var initialWidth 		= '';
            var initialHeight 		= '';
            var width 				= '';
            var height 				= '';
            var xScale 				= '';
            var yScale 				= '';
            var newScale 			= '';
            var selectedSize 		= '';
            var selectedObject 		= '';
            var clonedObject		= '';
            var cloneFlag 			= false;
            var imageSources 		= {};
            var canvas 				= document.createElement("canvas");
            var ctx 				= canvas.getContext("2d");
            var usersOptions 		= {};
            var background_global 	= '';
            var historyState 		= [];
            var historyStep 		= 0;
            var _textOptions 		= '';
            var _canvasEditor 		= jQuery('#canvasEditor');
            var _canvasContainer 	= '';
            var _sizeList 			= jQuery('.flyerSize');
            var _orientationList 	= jQuery('.flyerOrientation');
            var _backgroundOptions 	= '';
            /**
             * FabricJS Stage
             */
            var stage = new fabric.Canvas('canvasEditor');
            /**
             * Stage Defaults
             */
            params.width 	= ( params.width 	== '' ) ? (105*300)/25.4 : params.width;
            params.height 	= ( params.height 	== '' ) ? (148*300)/25.4 : params.height;
            _drawStage( 'a6', '' );
            _stageBackgroundColor( '', '' );
            /**
             * Set Flyer Sizes
             */
            function _flyerSizes( size ) {
                params.width 	= '';
                params.height 	= '';
                switch( size ) {
                    case 'a6':
                        params.width 	= (105*300)/25.4;
                        params.height 	= (148*300)/25.4;
                        selectedSize 	= 'a6';
                        break;
                    case 'a5':
                        params.width 	= (148*300)/25.4;
                        params.height 	= (210*300)/25.4;
                        selectedSize 	= 'a5';
                        break;
                    case 'a4':
                        params.width 	= (210*300)/25.4;
                        params.height 	= (297*300)/25.4;
                        selectedSize 	= 'a4';
                        break;
                    case 'a3':
                        params.width 	= (297*300)/25.4;
                        params.height 	= (420*300)/25.4;
                        selectedSize 	= 'a3';
                        break;
                    case 'a2':
                        params.width 	= (420*300)/25.4;
                        params.height 	= (594*300)/25.4;
                        selectedSize 	= 'a2';
                        break;
                    case 'fb_cov':
                        params.width 	= (223.04375*300)/25.4;
                        params.height 	= (106.627083333*300)/25.4;
                        selectedSize 	= 'fb_cov';
                        break;
                }
            }
            /**
             * Set Flyer Size on Change
             */
            _sizeList.on('change', function(){
                var size = jQuery(this).val();
                switch( size ) {
                    case 'a6':
                        selectedSize = 'a6';
                        _flyerSizes( 'a6' );
                        break;
                    case 'a5':
                        selectedSize = 'a5';
                        _flyerSizes( 'a5' );
                        break;
                    case 'a4':
                        selectedSize = 'a4';
                        _flyerSizes( 'a4' );
                        break;
                    case 'a3':
                        selectedSize = 'a3';
                        _flyerSizes( 'a3' );
                        break;
                    case 'a2':
                        selectedSize = 'a2';
                        _flyerSizes( 'a2' );
                        break;
                    case 'fb_cov':
                        selectedSize = 'fb_cov';
                        _flyerSizes( 'fb_cov' );
                        break;
                }
                _drawStage( selectedSize, '' );
                _stageBackgroundColor( stage.backgroundColor, '' );
                _dynamicScale( params );
            });
            /**
             * Draw Stage
             * Sets size, scale and orientation. Then draws
             * a canvas stage.
             *
             * @param size string
             * @param orientation string
             */
            function _drawStage( size, orientation ) {
                // Set Flyer Size
                _flyerSizes( size );
                // Detect Orientation
                if( orientation == '' ) { orientation = 'portrait'; }
                switch( orientation ) {
                    case 'portrait':
                        drawWidth 	= params.width;
                        drawHeight 	= params.height;
                        break;
                    case 'landscape':
                        drawWidth 	= params.height;
                        drawHeight 	= params.width;
                        break;
                }
                // Set Size
                stage.setWidth( drawWidth );
                stage.setHeight( drawHeight );
                // Set Canvas Wrapper Size
                _canvasEditor.css({
                    width: drawWidth,
                    height: drawHeight
                });
                // Fit Canvas into Viewport
                _dynamicScale( params );
                // Keep original sta
                // Bind canvas container
                _bindCanvas();
                // Keep changes
                stage.on(
                    'object:added', 		function() {

                    },
                    'object:modified', 		function() {

                    },
                    'object:moving', 		function() {
                        _destroyOptions();
                        _destroyBOptions();
                    },
                    'object:rotating', 		function() {
                        _destroyOptions();
                        _destroyBOptions();
                    },
                    'object:scaling', 		function() {
                        _destroyOptions();
                        _destroyBOptions();
                    },
                    'object:selected', 		function() {
                        _imageOptions();
                    },
                    'path:created', 		function() {
                    },
                    'selection:cleared', 	function() {
                        _destroyOptions();
                    },
                    'mousedown', 			function() {
                        _imageOptions();
                    }
                );
            }
            /**
             * Destroy Menu Options
             */
            function _destroyOptions() {
                _textOptions = jQuery('#textOptions');
                if( _textOptions.length > 0 ) {
                    _textOptions.remove();
                }
                jQuery('.backColor').attrchange('disconnect');
                jQuery('.fontColor').attrchange('disconnect');
            }
            function _destroyBOptions() {
                jQuery('.backColor').attrchange('disconnect');
                jQuery('.fontColor').attrchange('disconnect');
                _backgroundOptions = jQuery('#backgroundOptions');
                if( _backgroundOptions.length > 0 ) {
                    _backgroundOptions.empty();
                    _backgroundOptions.remove();
                }
            }
            /**
             * Background Image Options
             */
            function _imageOptions() {
                selectedObject = stage.getActiveObject();
                _gutter 	= 200;
                _x 			= stage.__mousedownX;
                _y 			= stage.__mousedownY - _gutter;
                var _menuOptions = jQuery('<div id="backgroundOptions"></div>');
                _menuOptions.append( '<button class="btn btn-sm btn-danger imgRemove"><span class="fa fa-trash-o fa-fw"></span></button>' );
                _menuOptions.append( '<button class="btn btn-sm btn-default imgUp"><span class="fa fa-chevron-up fa-fw"></span></button>' );
                _menuOptions.append( '<button class="btn btn-sm btn-default imgDown"><span class="fa fa-chevron-down fa-fw"></span></button>' );
                _menuOptions.append( '<button class="btn btn-sm btn-default alphaUp"><span class="fa fa-circle fa-fw"></span></button>' );
                _menuOptions.append( '<button class="btn btn-sm btn-default alphaDown"><span class="fa fa-circle-o fa-fw"></span></button>' );
                _menuOptions.css({
                    background: 'transparent',
                    height: '32px',
                    display: 'inline-block',
                    position: 'absolute',
                    top: _y,
                    left: _x,
                    zIndex: '9'
                });
                if( stage.backgroundImage != '' || /bgImage/i.test(stage.getActiveObject().id) ) {
                    _destroyBOptions();
                    _stageAppend( _menuOptions );
                }
                jQuery('.imgRemove').unbind('click tap').on('click tap', function(){
                    /**
                     * Remove image
                     */
                    // Ask for confirmation before remove
                    if( window.confirm('<?php echo lang("ux_confirmDialog"); ?>') ) {
                        // Destroy menu options
                        _destroyBOptions();
                        // Remove background image
                        if( stage.backgroundImage != '' ) {
                            stage.backgroundImage = '';
                            _stageUpdate();
                        }
                        if( stage.getActiveObject() != null ) { stage.remove( stage.getActiveObject() ); }
                        // Make history
                        return false;
                    }
                });
                jQuery('.imgUp').unbind('click tap').on('click tap', function(){
                    // Layer Switching
                    object = stage.getActiveObject();
                    if( object.type == 'image' ) {
                        stage.moveTo(object, stage.getObjects().indexOf(object) + 1 );
                    }
                });
                jQuery('.imgDown').unbind('click tap').on('click tap', function(){
                    object = stage.getActiveObject();
                    if( object.type == 'image' ) {
                        objectIndex = stage.getObjects().indexOf(object);
                        if( objectIndex > 0 ) {
                            stage.moveTo(object, stage.getObjects().indexOf(object) - 1 );

                        }
                    }
                });
                jQuery('.alphaUp').unbind('click tap').on('click tap', function(){
                    object = stage.getActiveObject();
                    if( object.type == 'image' ) {
                        objectAlpha = parseFloat( object.getOpacity() );
                        if( objectAlpha < 1 && object !== 1 ) {
                            newAlpha = parseFloat( objectAlpha + 0.1 );
                            object.setOpacity( parseFloat( newAlpha.toFixed(2) ) );
                            _stageUpdate();
                        }
                    }
                });
                jQuery('.alphaDown').unbind('click tap').on('click tap', function(){
                    object = stage.getActiveObject();
                    if( object.type == 'image' ) {
                        objectAlpha = parseFloat( object.getOpacity() );
                        if( objectAlpha > 0 && objectAlpha !== 0 ) {
                            newAlpha = parseFloat( objectAlpha - 0.1 );
                            object.setOpacity( parseFloat( newAlpha.toFixed(2) ) );
                            _stageUpdate();
                        }
                    }
                });
            }
            /**
             *  Dynamic Scale
             *  Dynamically scales stages to make them
             *  fit into port view.
             *
             *  @param params array
             */
            function _dynamicScale( params ) {
                gutter 			= 200;
                initialWidth 	= _canvasEditor.innerWidth();
                initialHeight 	= _canvasEditor.innerHeight();
                width 			= initialWidth / initialHeight * ( jQuery(window.top).innerHeight() - gutter );
                height 			= jQuery(window.top).innerHeight() - gutter;
                params.width 	= width;
                params.height 	= height;
                jQuery('.canvas-container, .canvas-container canvas, #canvasEditor').css({
                    width: 	params.width,
                    height: params.height
                });
            }
            _dynamicScale( params );
            /**
             * Dynamic Scale on Window Resize
             */
            jQuery(window).on('resize', function(){
                _dynamicScale( params );
            });
            /**
             * Flyer Orientation
             */
            _orientationList.on('change', function(){
                _flyerSizes( selectedSize );
                var orientation = jQuery(this).val();
                _drawStage( selectedSize, orientation );
                _stageBackgroundColor( stage.backgroundColor, '' );
            });
            /**
             * Add Stage Background
             */
            function _stageBackgroundColor( color, alpha ) {
                if( color == '' && alpha == '' ) { color = 'rgba(255,255,255,0)'; }
                if( color == '' && alpha != '' ) { color = 'rgba(255,255,255,'+alpha+')'; }
                stage.setBackgroundColor( color, stage.renderAll.bind(stage) );
                stage.on('click tap', function(e){
                    var layer_name = e.target.parent.attrs.id;
                    if( /background_layer/i.test(layer_name) || /bgImage/i.test( layer_name ) ) {
                        jQuery('.optionButtons').empty();
                        jQuery('.optionButtons').remove();
                        jQuery('.rotateDiv').remove();
                        jQuery('.textInPlace').remove();
                        if( window.text != undefined ) text.show();
                        stage.get('.topRight').destroy();
                        stage.get('.topLeft').destroy();
                        stage.get('.bottomRight').destroy();
                        stage.get('.bottomLeft').destroy();
                        stage.get('.rotateAnchor').destroy();
                        stage.draw();
                    }
                    jQuery('.backColor').attrchange('disconnect');
                });
            }
            /**
             * Category autocomplete
             */
            var cache = {};
            jQuery( "#templateCategoryAuto" ).autocomplete({
                minLength: 2,
                source: function( request, response ) {
                    var term = request.term;
                    if( term in cache ) {
                        response( cache[ term ] );
                        return;
                    }
                    jQuery.getJSON( "<?php echo site_url( 'dashboard/templateCategories' ) ?>", request, function( data, status, xhr ) {
                        cache[ term ] = data;
                        response( data );
                    });
                }
            });
            /**
             * Fonts Object
             */
            var fonts = <?php print json_encode( $fonts ); ?>;
            /**
             * Load Google Fonts
             */
            var gFonts = '';
            var gFamilies = [];
            var _firstKey = '';
            jQuery.each(fonts, function(key, value){
                if( _firstKey == '' ) {
                    _firstKey = value.id;
                }
                gFamilies[value.id] = ({
                    'group_name': 	value.group_name,
                    'group_fonts': 	value.group_fonts,
                    'group_id': 	value.id
                });
                jQuery('.fontFamilies optgroup').append('<option value="'+value.id+'">'+value.group_name.charAt(0).toUpperCase()+value.group_name.substring(1)+'</option>');
            });
            jQuery('.fontFamilies').selectpicker({
                style: 'btn btn-default',
                size: 'auto',
                width: '25%',
            });
            jQuery('.fontFamilies').on('change', function(){
                // Get new font group
                var element = parseInt( jQuery(this).val() );
                gFonts = gFamilies[element].group_fonts;
                // Load new font faces
                _loadFonts();
                // On font group change, update all strings
                jQuery.each(stage.getObjects(), function( key, obj ){
                    // For each text object
                    if( obj.text != '' && obj.text != undefined ) {
                        obj.set({
                            fontFamily: gFonts[0]
                        });
                        // Update stage
                        _stageUpdate();
                    }
                });
            });
            // Remove duplicates
            function unique(list) {
                var result = [];
                jQuery.each(list, function(i, e) {
                    if (jQuery.inArray(e, result) == -1 && e !== '' && e !== 'undefined' ) result.push(e);
                });
                return result;
            }
            gFonts = gFamilies[_firstKey].group_fonts;
            function _loadFonts(){
                gFonts = gFonts.split(',');
                gFonts = unique( gFonts );
                // Load fonts
                if( gFonts.length > 0 ) {
                    WebFont.load({
                        google: {
                            families: gFonts
                        }
                    });
                }
            }
            _loadFonts();
            /**
             * History Events
             */
            function _makeHistory() {
                json = JSON.stringify( stage );
                historyState.push( json );
            }
            function _undoHistory() {
                if( historyStep < historyState.length ) {
                    var _flag = ( ( parseInt(historyState.length) - 1 ) - parseInt(historyStep) );
                    stage.clear().renderAll();
                    console.log( historyStep + ' ' + historyState.length + ' ' + _flag );
                    stage.loadFromJSON( historyState[_flag], stage.renderAll.bind(stage), function( _object, _klass ) {
                        if( _object.type == 'image' ){
                            _klass.set({
                                id: 'bgImage'+_getLayerCount()
                            });
                            _klass.on(
                                'mousedown', function(){
                                    _destroyOptions();
                                }
                            );
                        } else {
                            _klass.id = 'layer'+_getLayerCount();
                            _textStyleMenu( _klass );
                            _textListener( _klass );
                            _klass.set({
                                borderColor: 'rgba(52, 152, 219, .5)',
                                cornerColor: 'rgba(42, 142, 209, .5)',
                                cornerSize: 55,
                                transparentCorners: false
                            });
                        }
                        stage.on(
                            'object:added', 		function() {

                            },
                            'object:modified', 		function() {

                            },
                            'object:moving', 		function() {
                                _destroyOptions();
                                _destroyBOptions();

                            },
                            'object:rotating', 		function() {
                                _destroyOptions();
                                _destroyBOptions();

                            },
                            'object:scaling', 		function() {
                                _destroyOptions();
                                _destroyBOptions();

                            },
                            'object:selected', 		function() {
                                _imageOptions();
                            },
                            'path:created', 		function() {

                            },
                            'selection:cleared', 	function() {
                                _destroyOptions();

                            },
                            'mousedown', 			function() {
                                _imageOptions();
                            }
                        );
                        _dynamicScale( params );
                        // Bind canvas container
                        _bindCanvas();
                        _stageUpdate();
                    } );
                    historyStep++;
                }
            }
            function _redoHistory() {
                if( historyStep > 0 ) {
                    stage.clear().renderAll();
                    stage.loadFromJSON( historyState[ historyState.length - 1 - historyStep + 1 ] );
                    stage.renderAll();
                    historyStep -= 1;
                }
            }
            jQuery('#undo').on('click tap', function(){
                _undoHistory();
            });
            jQuery('#redo').on('click tap', function(){
                _redoHistory();
            });
            /**
             * Keyboard Event Listener
             */
            jQuery(window).keydown(function( event ){
                // Undo (ctrl/cmd z)
                if( event.which == 90 && ( event.ctrlKey || event.metaKey ) ) {
                    event.preventDefault();
                    _undoHistory();
                }
                // Redo (ctrl/cmd y)
                if( event.which == 89 && ( event.ctrlKey || event.metaKey ) ) {
                    event.preventDefault();
                    _redoHistory();
                }
                // Deselect text (esc)
                if( event.which == 27 ) {
                    event.preventDefault();
                    _destroyOptions();
                    _destroyBOptions();
                }
                // Copy object (ctrl/cmd c)
                if( event.which == 67 && ( event.ctrlKey || event.metaKey ) ) {
                    if( !selectedObject.isEditing ) {
                        event.preventDefault();
                        // Set cloned flag
                        cloneFlag = true;
                        // Clone object
                        _cloneObject();
                    }
                }
                // Paste object (ctrl/cmd v)
                if( event.which == 86 && ( event.ctrlKey || event.metaKey ) ) {
                    if( !selectedObject.isEditing ) {
                        event.preventDefault();
                        if( cloneFlag ) {
                            // Clone Object
                            _cloneObject();
                        }
                        // New selected object
                        clonedObject.set({
                            top: selectedObject.top+50,
                        });
                        _setHandlers( clonedObject );
                        /**
                         * Make History on Drag Start & End
                         */
                        clonedObject.on(
                            'moving', function(){

                            }
                        );
                        // Add layer to stage
                        stage.add( clonedObject );
                        // Text Listener
                        _textListener( clonedObject );
                        // Styling Menu
                        _textStyleMenu( clonedObject );
                        // Make history
                    }
                }
                // Move object
                switch( event.which ) {
                    // Left
                    case 37:
                        if( selectedObject != '' ) {
                            event.preventDefault();
                            if( event.shiftKey ) { move = 4; } else { move = 1; }
                            selectedObject.set({
                                left: selectedObject.left - move
                            });

                            _stageUpdate();
                        }
                        break;
                    // Up
                    case 38:
                        if( selectedObject != '' ) {
                            event.preventDefault();
                            if( event.shiftKey ) { move = 4; } else { move = 1; }
                            selectedObject.set({
                                top: selectedObject.top - move
                            });

                            _stageUpdate();
                        }
                        break;
                    // Right
                    case 39:
                        if( selectedObject != '' ) {
                            event.preventDefault();
                            if( event.shiftKey ) { move = 4; } else { move = 1; }
                            selectedObject.set({
                                left: selectedObject.left + move
                            });

                            _stageUpdate();
                        }
                        break;
                    // Down
                    case 40:
                        if( selectedObject != '' ) {
                            event.preventDefault();
                            if( event.shiftKey ) { move = 4; } else { move = 1; }
                            selectedObject.set({
                                top: selectedObject.top + move
                            });

                            _stageUpdate();
                        }
                        break;
                }
                // Save Flyer
                if( event.which == 83 && ( event.ctrlKey || event.metaKey ) ) {
                    event.preventDefault();
                    // Deselect objects
                    stage.deactivateAll();
                    // Update stage
                    _stageUpdate();
                    _saveTemplate();
                }
                // Export Flyer
                if( event.which == 68 && ( event.ctrlKey || event.metaKey ) ) {
                    event.preventDefault();
                    // Deselect objects
                    stage.deactivateAll();
                    // Update stage
                    _stageUpdate();
                    _downloadFlyer();
                }
                // Delete Object
                if( event.which == 46 ) {
                    event.preventDefault();
                    if( window.confirm(' <?php echo lang("ux_confirmDialog"); ?>') ) {
                        // Remove object
                        stage.remove( stage.getActiveObject() );
                        // Remove button set
                        _destroyOptions();
                        _destroyBOptions();
                        // Make history
                        _stageUpdate();
                        return false;
                    }
                }
            });
            /**
             * Text Style Menu
             */
            function _textStyleMenu( object ) {
                if( object != '' && object != undefined ) {
                    object.on(
                        'mousedown', 	function(){
                            _textMenuOptions( object );
                            _setHandlers( object );
                            jQuery('.backColor').attrchange('disconnect');
                            jQuery('.fontColor').attrchange('disconnect');
                        },
                        'text:changed', 		function() {

                        },
                        'text:selection:cleared', function() {
                            _destroyOptions();
                        },
                        'text:scaling', 		function() {

                            _destroyOptions();
                        }
                    );
                }
            }
            /**
             * Text Menu Options
             */
            function _textMenuOptions( object ) {
                _gutter 		= 60;
                _this 			= object;
                _width 			= 400;
                _editorX 		= _canvasContainer.offset()['left'];
                _editorY 		= _canvasContainer.offset()['top'];
                _x 				= _editorX - _gutter;
                _y 				= _editorY - _gutter;
                _menuOptions 	= jQuery('<div id="textOptions" class="'+_this.id+'"></div>');
                _fontSizes 		= [ '12', '14', '16', '18', '21', '24', '28',
                    '32', '36', '42', '48', '56', '64', '72',
                    '80', '88', '96', '104', '120', '144' ];
                _fontValues 	= '';
                jQuery.each(_fontSizes, function(key, val){
                    if( _this.fontSize == val ) {
                        _fontValues += '<option value="'+val+'" selected="selected">'+val+'</option>';
                    } else {
                        _fontValues += '<option value="'+val+'">'+val+'</option>';
                    }
                });
                _fontSize =
                    '<select class="fontSize">'+
                    _fontValues +
                    '</select>'
                ;
                jQuery('.fontColor').val( _this.fill );
                jQuery('.backColor').val( _this.backgroundColor );
                _fontList = '';
                jQuery.each(gFonts, function(key, val){
                    if( _this.fontFamily == val ) {
                        _fontList += '<option value="'+val+'" selected="selected">'+val+'</option>';
                    } else {
                        _fontList += '<option value="'+val+'">'+val+'</option>';
                    }
                });
                _fontFamily =
                    '<select class="fontFamily">'+
                    _fontList+
                    '</select>'
                ;
                _moreOptions =
                    '<select class="fontStyling">'+
                    '<optgroup label="">'+
                    '<option value="" data-content="<i class=\'fa fa-fire fa-fw\'></i> <?php echo lang("ux_options"); ?>"><?php echo lang("ux_options"); ?></option>'+
                    '<option value="left" data-content="<i class=\'fa fa-align-left fa-fw\'></i> <?php echo lang("txt_left"); ?>"><?php echo lang("txt_left"); ?></option>'+
                    '<option value="center" data-content="<i class=\'fa fa-align-center fa-fw\'></i> <?php echo lang("txt_center"); ?>"><?php echo lang("txt_center"); ?></option>'+
                    '<option value="right" data-content="<i class=\'fa fa-align-right fa-fw\'></i> <?php echo lang("txt_right"); ?>"><?php echo lang("txt_right"); ?></option>'+
                    '<option value="txtUp" data-content="<i class=\'fa fa-chevron-up fa-fw\'></i> <?php echo lang("ux_move_up"); ?>"><?php echo lang("ux_move_up"); ?></option>'+
                    '<option value="txtDown" data-content="<i class=\'fa fa-chevron-down fa-fw\'></i> <?php echo lang("ux_move_down"); ?>"><?php echo lang("ux_move_down"); ?></option>'+
                    '</optgroup>'+
                    '</select>'
                ;
                // Put fonts into dropdown box
                _menuOptions.append( _fontFamily );
                _menuOptions.append( _fontSize );
                _menuOptions.append( '<button class="btn btn-sm btn-default openTextColorPicker" title="<?php echo lang("txt_color"); ?>"><i class="fa fa-paint-brush fa-fw"></i></button>' );
                _menuOptions.append( '<button class="btn btn-sm btn-default openTextBackColorPicker" title="<?php echo lang("txt_backcolor"); ?>"><i class="fa fa-tachometer fa-fw"></i></button>' );
                _menuOptions.append( '<button class="btn btn-sm btn-default fontStyle" data-value="bold"><span class="fa fa-bold fa-fw"></span></button>' );
                _menuOptions.append( '<button class="btn btn-sm btn-default fontStyle" data-value="italic"><span class="fa fa-italic fa-fw"></span></button>' );
                _menuOptions.append( '<button class="btn btn-sm btn-default fontStyle" data-value="underline"><span class="fa fa-underline fa-fw"></span></button>' );
                _menuOptions.append( _moreOptions );
                _menuOptions.append( '<button class="btn btn-sm btn-danger txtRemove"><span class="fa fa-trash-o fa-fw"></span></button>' );
                _menuOptions.css({
                    background: 'transparent',
                    width: _width,
                    height: '32px',
                    display: 'inline-block',
                    position: 'absolute',
                    top: _y,
                    left: _x,
                    zIndex: '9'
                });
                _textOptions = jQuery('#textOptions');
                if( _textOptions.length > 0 ) {
                    if( _this.id != _textOptions.attr('class') ) {
                        _destroyOptions();
                        _stageAppend( _menuOptions );
                    }
                } else {
                    _stageAppend( _menuOptions );
                }
                jQuery('.fontFamily').selectpicker({
                    style: 'btn btn-sm btn-default',
                    size: 'auto',
                    width: '25%',
                });
                jQuery('.fontFamily').selectpicker('render');
                jQuery('.fontSize').selectpicker({
                    style: 'btn btn-sm btn-default',
                    size: 'auto',
                    width: '25%',
                });
                jQuery('.fontSize').selectpicker('render');
                jQuery('.fontStyling').selectpicker({
                    style: 'btn btn-sm btn-default',
                    size: 10,
                    width: '25%',
                });
                jQuery('.fontStyling').selectpicker('render');
                jQuery('.fontFamily span.text').each(function(){
                    jQuery(this).css('font-family', "'"+jQuery(this).text()+"'");
                });
                _textListener( _this );
            }
            /**
             * Add String
             */
            function _addString() {
                // String Parameters
                _rotation	= 0;
                _padding	= 0;
                _size 		= 80;
                _string		= "<?php echo lang('opt_add_text'); ?>";
                _color 		= 'rgba(0,0,0,1)';
                _font 		= gFonts[0];
                _width 		= 300;
                _x 			= (stage.width/2)-(_width/2);
                _y 			= (stage.height/4);
                _alignment 	= 'left';
                _style 		= '';
                _background = 'rgba(255,255,255,0)';
                _lineheight = 1;
                // New Text
                _text 		= new fabric.Textbox(
                    _string,
                    {
                        id: 				'_text-'+_getLayerCount(),
                        left: 				_x,
                        top: 				_y,
                        fontFamily: 		_font,
                        fill: 				_color,
                        width: 				_width,
                        fontSize: 			_size,
                        backgroundColor: 	_background,
                        textAlign: 			_alignment,
                        padding: 			_padding,
                        lineHeight: 		_lineheight
                    }
                );
                _text.set({
                    borderColor: 'rgba(52, 152, 219, .5)',
                    cornerColor: 'rgba(42, 142, 209, .5)',
                    cornerSize: 55,
                    transparentCorners: true,
                    hasBorders: false
                });
                _text.on(
                    'mousedown', 	function(){
                        _destroyBOptions();
                        _destroyOptions();
                    },
                    'text:changed', 		function() {
                    },
                    'text:selection:cleared', function() {
                        _destroyBOptions();
                        _destroyOptions();
                    },
                    'text:scaling', 		function() {
                        _destroyBOptions();
                        _destroyOptions();
                    }
                );
                _textListener( _text );
                _textStyleMenu( _text );
                // Add layer to stage
                stage.add( _text );
                _text.setCoords();
                _stageUpdate();
                // Make histo
                return false;
            }
            jQuery('.addString').on('click tap', function(){
                _addString();
            });
            /**
             * Listen Text Menu Events
             */
            function _textListener( object ) {
                if( object != '' && object != undefined ) {
                    if( selectedObject == '' ) {
                        // Set a new selected object
                        selectedObject 	= object;
                    }
                    /**
                     * Change Font Size
                     */
                    jQuery('.fontSize').on('change', function(){
                        if (!object.isEditing) {
                            // Set size
                            object.set({
                                fontSize: parseFloat( jQuery(this).val() )
                            });
                        } else {
                            object.setSelectionStyles({
                                fontSize: parseFloat( jQuery(this).val() )
                            });
                        }
                        _stageUpdate();
                        return false;
                        // Make history
                    });
                    /**
                     * Change Font Color
                     */
                    jQuery('.openTextColorPicker').on('click tap', function(event){
                        event.stopPropagation();
                        textColorPicker.colorpicker('open');
                        jQuery('.fontColor').attrchange({
                            callback: function() {
                                if (!object.isEditing) {
                                    object.set({
                                        fill: (this.value.indexOf('#') === -1 && this.value.indexOf('rgba') === -1 ) ? ('#'+this.value).toString() : (this.value).toString(),
                                    });
                                } else {
                                    object.setSelectionStyles({
                                        fill: (this.value.indexOf('#') === -1 && this.value.indexOf('rgba') === -1 ) ? ('#'+this.value).toString() : (this.value).toString(),
                                    });
                                }
                                _stageUpdate();
                                // Make history

                            }
                        });
                        return false;
                    });
                    /**
                     * Change Background Color
                     */
                    jQuery('.openTextBackColorPicker').on('click tap', function(event){
                        event.stopPropagation();
                        backgroundColorPicker.colorpicker('open');
                        jQuery('.backColor').attrchange({
                            trackValues: true,
                            callback: function(event) {
                                if( event.attributeName == 'value' ) {
                                    if (!object.isEditing) {
                                        object.set({
                                            backgroundColor: ( event.newValue.indexOf('#') === -1 && event.newValue.indexOf('rgba') === -1 ) ? ('#'+event.newValue).toString() : (event.newValue).toString(),
                                        });
                                    } else {
                                        console.log( event.newValue );
                                        object.setSelectionStyles({
                                            backgroundColor: ( event.newValue.indexOf('#') === -1 && event.newValue.indexOf('rgba') === -1 ) ? ('#'+event.newValue).toString() : (event.newValue).toString(),
                                        });
                                    }
                                    _stageUpdate();
                                    // Make history

                                }
                            }
                        });
                        return false;
                    });



                    jQuery('.fa-align-left').on('click', function(){
                        object.textAlign= 'left'
                        _stageUpdate();
                        // Make history
                        _makeHistory();
                        return false;
                    });

                    jQuery('.fa-align-center').on('click', function(){
                        object.textAlign= 'center'
                        _stageUpdate();
                        // Make history
                        _makeHistory();
                        return false;
                    });

                    jQuery('.fa-align-right').on('click', function(){
                        object.textAlign= 'right'
                        _stageUpdate();
                        // Make history
                        _makeHistory();
                        return false;
                    });



                    /**
                     * Change Font Family
                     */
                    jQuery('.fontFamily').on('change', function(){
                        if (!object.isEditing) {
                            // Set font family
                            object.set({
                                fontFamily: jQuery(this).val().toString()
                            });
                        } else {
                            object.setSelectionStyles({
                                fontFamily: jQuery(this).val().toString()
                            });
                        }
                        _stageUpdate();
                        // Make history
                        return false;
                    });
                    /**
                     * Text Styling
                     */
                        // Font Style
                    jQuery('.fontStyle').on('click tap', function(){
                        var option 		= jQuery(this).attr('data-value');
                        var _styles 	= object.fontStyle.split(' ');
                        var newStyle 	= '';
                        if( option != '' ) {
                            if( option != 'underline' ) {
                                if( _styles.indexOf( option ) < 0 ) {
                                    _styles.push( option );
                                } else {
                                    _styles.splice( _styles.indexOf( option ), 1 );
                                }
                                newStyle = _styles.join(' ');
                                if (!object.isEditing) {
                                    // Draw text
                                    object.set({
                                        fontStyle: newStyle,
                                    });
                                } else {
                                    object.setSelectionStyles({
                                        fontStyle: newStyle,
                                    });
                                }
                            }
                            if( option == 'underline' ) {
                                if (!object.isEditing) {
                                    if( object.getTextDecoration() != 'underline' ) {
                                        //object.setTextDecoration('underline');
                                        object.set({
                                            textDecoration: 'underline'
                                        });
                                    } else {
                                        object.set({
                                            textDecoration: ''
                                        });
                                    }
                                } else {
                                    if( object.getSelectionStyles()['textDecoration'] != 'underline' ) {
                                        //object.setTextDecoration('underline');
                                        object.setSelectionStyles({
                                            textDecoration: 'underline'
                                        });
                                    } else {
                                        object.setSelectionStyles({
                                            textDecoration: ''
                                        });
                                    }
                                }
                            }
                            _stageUpdate();
                            // Make history

                        }
                        return false;
                    });
                    jQuery('.fontStyling').on('change', function(){
                        var option 		= jQuery(this).val();
                        var alignment 	= [ 'left', 'center', 'right' ];
                        var layerSwitch = [ 'txtUp', 'txtDown' ];
                        var newStyle 	= '';
                        if( option != '' ) {
                            // Text Alignment
                            if( alignment.indexOf( option ) > -1 ) {
                                if (!object.isEditing) {
                                    // Set font alignment
                                    object.set({
                                        textAlign: option.toString()
                                    });
                                } else {
                                    object.setSelectionStyles({
                                        textAlign: option.toString()
                                    });
                                }
                                _stageUpdate();
                                // Make history

                            }
                            // Layer Switching
                            if( layerSwitch.indexOf( option ) > -1 ) {
                                if( option == 'txtUp' ) {
                                    stage.moveTo(object, stage.getObjects().indexOf(object) + 1 );

                                }
                                if( option == 'txtDown' ) {
                                    objectIndex = stage.getObjects().indexOf(object);
                                    if( objectIndex > 0 ) {
                                        if( objectIndex-1 == 0 || objectIndex-1 == 1 ) {
                                            if( !/bgImage/i.test( stage.item(objectIndex-1).id ) &&
                                                !/canvasColor-Layer/i.test( stage.item(objectIndex-1).id ) ) {
                                                stage.moveTo(object, stage.getObjects().indexOf(object) - 1 );

                                            }
                                        } else {
                                            stage.moveTo(object, stage.getObjects().indexOf(object) - 1 );

                                        }
                                    }
                                }
                            }
                        }
                        jQuery(this).val('');
                        return false;
                    });
                }
                /**
                 * Remove text
                 */
                jQuery('.'+object.id+' .txtRemove').unbind('click tap').on('click tap', function(){
                    // Ask for confirmation before remove
                    if( window.confirm(' <?php echo lang("ux_confirmDialog"); ?>') ) {
                        _destroyOptions();
                        // Remove object
                        stage.remove( object );
                        // Make history
                    }
                    return false;
                });
            }
            /**
             * Background Images
             */
            jQuery('.backImage').on('click', function(){

                var templateName = jQuery.trim( $("#templ_edit_name").val() );
                var templateCategory = $("#templ_edit_category_id option:selected").text();
                var templateCategoryID = $("#templ_edit_category_id option:selected").val();
                var templateFontFamilies = $("#templ_edit_fontFamilies option:selected").val();
                if( ( templateCategoryID.length > 0 && templateCategoryID != '') && templateFontFamilies != '' && templateName != '' ) {
                    var flyerCat = 'themes/'+templateCategory.toLowerCase().replace(/ /g, '-')+'/';
                    jQuery('input[name="uploadCategory"]').val( flyerCat );

                    _uploadedImages(templateCategory, templateCategoryID);
                    jQuery('#fitBtn').button();
                    jQuery('#progress .progress-bar').css('width','0%');
                    jQuery('#progress .progress-bar').removeClass('progress-bar-success');
                    jQuery('.imageTools').dialog({
                        height: jQuery(window).height(),
                        width: 'auto',
                        modal: false,
                        buttons: {
                            "Ok": function() {
                                _stageBackgroundImage( '' );
                                _dynamicScale( params );
                                jQuery('.imageX').val('0');
                                jQuery('.imageY').val('0');
                                jQuery('.imageSrc').remove('');
                                jQuery('#fileupload').val('');
                                jQuery('#files').empty();
                                jQuery('.fit').attr('checked', false);
                                jQuery('label[for="fitBtn"]').removeClass('ui-state-active');
                                jQuery(this).dialog('close');
                            }
                        }
                    });
                } else {
                    jQuery( "<div><?php echo lang('ux_template_info'); ?></div>" ).dialog({
                        height: 140,
                        width: 400,
                        modal: true
                    });
                }
            });
            jQuery('#fileupload').on('click', function(){
                jQuery('#progress .progress-bar').css('width','0%');
                jQuery('#progress .progress-bar').removeClass('progress-bar-success');
            });
            /**
             * Image Upload
             */
            jQuery(function() {
                'use strict';
                templ_edit_category_idOnChange()


            });
            /**
             * Background Image Loader
             */
            function _stageBackgroundImage( portview ) {
                /**
                 * Load Images into background
                 */
                // Count Stage Layers
                var layerCount 		= _getLayerCount();
                var origLayerCount 	= layerCount;
                if( portview == 'print' ) {
                    // Get real size
                    _flyerSizes( selectedSize );
                }
                // Loop through each image source
                jQuery('.imageSrc').each(function() {
                    var imageUrl = jQuery(this).attr('value');
                    // Count Canvas Layers
                    var idLayer = 'bgImage'+layerCount;
                    // Get X and Y positions
                    var posX = ( parseFloat(jQuery('.imageX').val()) != null || parseFloat(jQuery('.imageX').val()) != '' ) ? parseFloat(jQuery('.imageX').val()) : 0;
                    var posY = ( parseFloat(jQuery('.imageY').val()) != null || parseFloat(jQuery('.imageY').val()) != '' ) ? parseFloat(jQuery('.imageY').val()) : 0;
                    // Set an Image Object
                    var backgroundImage_idLayer 	= new Image();
                    var setWidth;
                    var setHeight;
                    var fit = jQuery('.fit').is(':checked');
                    backgroundImage_idLayer.onload = function() {
                        if( fit ) {
                            setWidth 	= stage.width;
                            setHeight 	= stage.height;
                            stage.setBackgroundImage( backgroundImage_idLayer.src, stage.renderAll.bind(stage), {
                                originX: 'left',
                                originY: 'top',
                                left: posX,
                                top: posY,
                                width: setWidth,
                                height: setHeight
                            } );
                        } else {
                            var backgroundImage = new fabric.Image( backgroundImage_idLayer );
                            backgroundImage.set({
                                id: idLayer,
                                borderColor: 'rgba(52, 152, 219, .5)',
                                cornerColor: 'rgba(42, 142, 209, .5)',
                                cornerSize: 45,
                                transparentCorners: true
                            });
                            stage.centerObject( backgroundImage );
                            stage.add( backgroundImage );
                            backgroundImage.moveTo(0);
                            _stageUpdate();
                        }
                        _bindCanvas();
                    };
                    // Loading spin
                    var opts = {
                        lines: 17, // The number of lines to draw
                        length: 0, // The length of each line
                        width: 7, // The line thickness
                        radius: 15, // The radius of the inner circle
                        corners: 1, // Corner roundness (0..1)
                        rotate: 0, // The rotation offset
                        direction: 1, // 1: clockwise, -1: counterclockwise
                        color: '#2980b9', // #rgb or #rrggbb or array of colors
                        speed: 1.5, // Rounds per second
                        trail: 50, // Afterglow percentage
                        shadow: false, // Whether to render a shadow
                        hwaccel: false, // Whether to use hardware acceleration
                        className: 'spinner', // The CSS class to assign to the spinner
                        zIndex: 2e9, // The z-index (defaults to 2000000000)
                        top: '10', // Top position relative to parent in px
                        left: 'auto' // Left position relative to parent in px
                    };
                    // Spin target
                    var target = jQuery('.canvas-container')[0];
                    // Spinner
                    var spinner = new Spinner(opts).spin(target);
                    // Load image from source
                    backgroundImage_idLayer.src = '<?php echo $base_root_dir ?>'+jQuery(this).val();
                    // Hide loader spinner
                    jQuery(backgroundImage_idLayer).load(function(){
                        spinner.stop();
                    });
                    var sourceID = 'bgImage'+layerCount;
                    // Save image sources to store into database
                    imageSources[sourceID] = /* FCPATH + */ jQuery(this).val();
                    // If multiple files then increment layer count
                    layerCount++;
                });
            }


        /**
         * User Options
         */
        jQuery('.userOptions').on('click', function(){
            // Dialog Window
            jQuery('.userOptionsTools').dialog({
                height: jQuery(window).height(),
                width: jQuery(window).width(),
                modal: false,
                buttons: {
                    "Ok": function() {
                        jQuery('input[id^="opt-"]').each(function(){
                            if( jQuery(this).is(':checked') ) {
                                // Reset checked options
                                jQuery(this).val(true);
                                usersOptions[jQuery(this).attr('id')] = true;
                            }
                        });
                        // Close dialog
                        jQuery(this).dialog('close');
                    },
                    "Cancel": function() {
                        // Close dialog
                        jQuery(this).dialog('close');
                    }
                }
            });
            // User Options
            jQuery('span[id^="opt-"]').each(function(){
                // Set options to false
                usersOptions[jQuery(this).attr('id')] = false;
                // Check clicked options
                jQuery(this).on('click tap', function() {
                    // Keep options list
                    var elID = jQuery(this).attr('id');
                    usersOptions[elID] = jQuery('input[id="'+elID+'"]').is(':checked');
                });
            });
        });
        /**
         * Layer Counter
         */
        function _getLayerCount() {
            var counter = stage.getObjects().length+1;
            return counter;
        }
        /**
         * Clone Object
         */
        function _cloneObject() {
            // Clear selection
            _destroyOptions();
            if( selectedObject != '' ) {
                clonedObject 	= selectedObject.clone();
                clonedObject.id = 'layer'+_getLayerCount();
                return clonedObject;
            } else {
                return false;
            }
        }
        /**
         * Object Handlers
         */
        function _setHandlers( object ) {
            if( object != undefined && object != '' ) {
                object.set({
                    borderColor: 'rgba(52, 152, 219, .5)',
                    cornerColor: 'rgba(42, 142, 209, .5)',
                    cornerSize: 55,
                    transparentCorners: true
                });
                _stageUpdate();
            }
        }
        /**
         * Append Object to Canvas
         */
        function _stageAppend( object ) {
            if( object != '' && object != undefined ) {
                jQuery('.canvas-container').append( object );
            }
        }
        /**
         * Get Object zIndex
         */
        function _stageIndex( object ) {
            var _index = '';
            if( object != '' && object != undefined ) {
                _index = stage.getObjects().indexOf( object );
            }
            return _index;
        }
        /**
         * Save Custom Template
         */
        function _saveTemplate() {
            // stage.toDataURL({ format: 'jpeg', quality: '0.8'});
            _stageJSON = JSON.stringify( stage.toJSON(['selectable', 'lockMovementX', 'lockMovementY', 'lockRotation', 'lockScalingX', 'lockScalingY', 'lockUniScaling']) );
            // Template Name
            templateName 		= jQuery('.templateName').val().toLowerCase();
            // Template Category
            templateCategory 	= jQuery('.templateCategory').val().toLowerCase();
            // Template SKU
            templateSKU 		= jQuery('.templateSKU').val();
            templateDescription 		= jQuery('.templateDescription').val();
            // Template Format
            templateFormat 		= jQuery('.flyerSize').val();

            if( typeof( imageSources ) == 'object' ) {
                // Stringify Image Sources
                imageList 		= JSON.stringify(imageSources);
            }
            if( typeof( usersOptions ) ) {
                // Stringify Users Options
                optionsList 	= JSON.stringify(usersOptions);
            }

            // Identification provided
            if( templateName != '' && templateCategory != '' && templateSKU != '' && templateFormat != '' ) {
                _flyerSizes( selectedSize );
                // Send data through AJAX to save into database
                jQuery.ajax({
                    url: '<?php echo site_url( "dashboard/save_templates" ) ?>',
                    type: 'POST',
                    dataType: "json",
                    // Template data
                    data: {
                <?php echo $this->security->get_csrf_token_name(); ?>:"<?php echo $this->security->get_csrf_hash(); ?>",
                    id: 			'<?php echo $template['id'] ?>',
                    is_insert: 	    '<?php echo $isInsert ?>',
                    name: 			templateName,
                    category_id:    templateCategory,
                    description:    templateDescription,
                    width: 			stage.width,
                    height: 		stage.height,
                    template: 		_stageJSON,
                    images: 		imageList,
                    fonts: 			gFonts.toString(),
                    user_options: 	optionsList,
                    format: 		templateFormat.toString(),
                    sku: 			templateSKU.toString()
            },
            success: function(result) {
                // Data saved
                if( result.result == 'success' ) {
                    jQuery( "<div><?php echo lang('ux_saved'); ?></div>" ).dialog({
                        height: 140,
                        width: 400,
                        modal: true
                    });
                    var hRef= "<?php echo site_url("templates/edit") ?>/" + result.ret
                    document.location = hRef
                } else {
                    jQuery( "<div><?php echo lang('ux_tryagain'); ?></div>" ).dialog({
                        height: 140,
                        width: 400,
                        modal: true
                    });
                }
            }
        });
        // Identification required
        } else {
            jQuery('#saving-overlay').css({
                opacity: 0,
                visibility: 'none'
            });
            jQuery( "<div><?php echo lang('ux_template_info'); ?></div>" ).dialog({
                height: 140,
                width: 400,
                modal: true
            });
        }
        }
        /**
         * Save Template
         */
        jQuery('.saveFlyer').on('click tap', function(){
            // Deselect objects
            stage.deactivateAll();
            // Update stage
            _stageUpdate();
            _saveTemplate();
        });
        function _stageUpdate() {
            // Refresh stage
            stage.renderAll();
        }
        /**
         * Bind Canvas Events
         */
        function _bindCanvas() {
            _canvasContainer 	= jQuery('.canvas-container');
            _canvasContainer.unbind('click tap').on('click tap', function() {
                if( stage.getActiveObject() != null ) {
                    if( stage.getActiveObject() == null || /bgImage/i.test(stage.getActiveObject().id) ) {
                        _destroyOptions();
                    }
                    if( stage.backgroundImage != null || /bgImage/i.test(stage.getActiveObject().id) ) {
                        _imageOptions();
                    }
                } else {
                    _destroyOptions();
                }
            });
        }



        });




        function onSubmit() {
            var theForm = $("#form_template_edit");
            theForm.submit();
        }

        //]]>
    </script>

    <!-- onsubmit="javascript : return onSubmit();" -->
    <form action=<?php echo site_url("templates/edit/".$template['id']) ?> method="post" accept-charset="utf-8" id="form_template_edit" name="form_template_edit" enctype="multipart/form-data" >
        <input type="hidden" id="page" name="page" value="1">
        <input type="hidden" id="sort" name="sort" value="">
        <input type="hidden" id="sort_direction" name="sort_direction" value="">

        <div class="panel panel-default bootstrap-admin-no-table-panel col-lg-6 col-xs-6">
            <div class="panel-heading">
                <div class="text-muted bootstrap-admin-box-title"><?php echo ( $isInsert ? "Add" : "Edit" ) ?>&nbsp;Template</div>
            </div>

            <?php if (!empty($validation_errors_text) ) : ?>
            <div class="alert alert-danger""><?php echo $validation_errors_text ?></div>
        <?php endif; ?>
        <?php if (!empty($editor_message) ) : ?>
            <div class="alert alert-success">
                <?php echo $editor_message ?>
            </div>
        <?php endif; ?>

        <div class="bootstrap-admin-no-table-panel-content bootstrap-admin-panel-content collapse in">


            <?php if (!$isInsert) : ?>
                <div class="row editor_row">
                    <label for="id" class="col-lg-2 control-label">Id </label>

                    <div class="col-lg-10">
                        <input type="text" tabindex="-1" readonly="" size="8" value="<?php echo $template['id']?>" name="templ_edit_id" id="templ_edit_id" class="form-control">
                    </div>
                </div>
            <?php endif;?>

            <div class="row editor_row">
                <label for="name" class="col-lg-2 control-label">Name </label>
                <div class="col-lg-10">
                    <input type="text" autofocus="" maxlength="50" size="30" placeholder="Enter name" value="<?php echo $template['name']?>" name="templ_edit_name" id="templ_edit_name" class="form-control templateName">
                </div>
            </div>




            <div class="row editor_row">
                <label for="category_id" class="col-lg-2 control-label">Category </label>
                <div class="col-lg-10">
                    <select id="templ_edit_category_id" name="templ_edit_category_id"  class="form-control templateCategory" onchange="javascript:templ_edit_category_idOnChange()">
                        <option value="">  -Select Category-  </option>
                        <?php foreach( $categoriesSelectionList as $nextCategory ) { ?>
                            <option <?php echo ( /*$category_id == $nextCategory['key']*/'' ? 'selected' : '')?> value="<?php echo $nextCategory['key'] ?>"><?php echo $nextCategory['value'] ?></option>
                        <?php }?>
                    </select>
                </div>
            </div>

            <div class="row editor_row">
                <label for="templ_edit_description" class="col-lg-2 control-label">Description </label>
                <div class="col-lg-10">
                    <textarea  id="templ_edit_description" rows="8" name="templ_edit_description"  class="form-control templateDescription"><?php echo $template['description']
                        ?></textarea>
                </div>
            </div>



            <div class="row editor_row">
                <label for="sku" class="col-lg-2 control-label">Sku </label>
                <div class="col-lg-10">
                    <input type="text" autofocus="" maxlength="10" size="10" placeholder="Enter sku" value="<?php echo $template['sku']?>" name="templ_edit_sku" id="templ_edit_sku" class="form-control templateSKU" readonly>
                </div>
            </div>


            <div class="row editor_row">
                <label for="flyerSize" class="col-lg-2 control-label">Size </label>
                <div class="col-lg-10">
                    <select id="templ_edit_flyerSize" name="templ_edit_flyerSize"  class="form-control flyerSize" style="width: 300px;">
                        <option value="">  -Select Size-  </option>
                        <?php foreach( $templateFormatValueArray as $nextTemplateFormat ) { ?>
                            <option <?php echo ( $template['format'] == $nextTemplateFormat['key'] ? 'selected' : '') ?> value="<?php echo $nextTemplateFormat['key'] ?>"><?php echo $nextTemplateFormat['value'] ?></option>
                        <?php }?>
                    </select>
                </div>
            </div>

            <div class="row editor_row">
                <label for="flyerOrientation" class="col-lg-2 control-label">Orientation </label>
                <div class="col-lg-10">
                    <select id="templ_edit_flyerOrientation" name="templ_edit_flyerOrientation"  class="form-control flyerOrientation" style="width: 300px;">
                        <option value="">  -Select Orientation-  </option>
                        <?php foreach( $templateOrientationValueArray as $nextFlyerOrientation ) { ?>
                            <option <?php echo ( $template['orientation'] == $nextFlyerOrientation['key'] ? 'selected' : '')?> value="<?php echo $nextFlyerOrientation['key'] ?>"><?php echo $nextFlyerOrientation['value'] ?></option>
                        <?php }?>
                        </optgroup>
                    </select>
                </div>
            </div>


            <div class="row editor_row">
                <label for="fontFamilies" class="col-lg-2 control-label">Font Family </label>
                <div class="col-lg-10">
                    <select id="templ_edit_fontFamilies" name="templ_edit_fontFamilies"  class="form-control fontFamilies" style="width: 300px;">
                        <option value="">  -Select Font Family-  </option>
                        <?php foreach( $fonts as $nextFont ) { ?>
                            <option <?php echo ( $template['fonts'] == $nextFont->group_fonts ? 'selected' : '')?> value="<?php echo $nextFont->id ?>"><?php echo $nextFont->group_name
                                ?></option>
                        <?php }?>
                    </select>
                </div>
            </div>




            <div class="row editor_row">
                <label class="col-lg-2 control-label">Background Color </label>
                <div class="col-lg-10">
                    <a class="openBackColorPicker btn btn-default" title="<?php echo lang('txt_backcolor'); ?>"><i class="fa fa-tachometer fa-lg"></i></a>
                </div>
            </div>


            <div class="row editor_row">
                <label class="col-lg-2 control-label">Background Image </label>
                <div class="col-lg-10">
                    <a class="backImage btn btn-default" title="<?php echo lang('ux_backImages'); ?>"><i class="fa fa-image fa-lg"></i></a>
                </div>
            </div>


            <div class="row editor_row">
                <label class="col-lg-2 control-label">User Options </label>
                <div class="col-lg-10">
                    <a class="userOptions btn btn-default" title="<?php echo lang('ux_user'); ?>"><i class="fa fa-unlock fa-lg"></i></a>
                </div>
            </div>


            <div class="row editor_row">
                <label class="col-lg-2 control-label">add text</label>
                <div class="col-lg-10">
                    <a href="#" class="addString btn btn-default"><i class="fa fa-font fa-fw"></i> <?php echo lang('opt_add_text'); ?></a>
                </div>
            </div>




            <div class="row editor_row">
                <label class="col-lg-2 control-label"> </label>
                <div class="col-lg-10">

                    <!-- Image Tools -->
                    <div class="imageTools" style="display:none;">
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- List of flyers -->
                                <div class="flyersFiles">
                                    <h3><?php echo lang('ux_backImages'); ?></h3>
                                    <div class="uploadedFlyers list-group"></div>
                                    <form action="<?php echo $base_root_dir ?>/static/uploads/" method="POST" enctype="multipart/form-data">
                                        <input type="hidden" name="uploadCategory" />
                                        <input id="fileupload" type="file" name="files[]" multiple>
                                    </form>
                                    <!-- The global progress bar -->
                                    <div id="progress" class="progress">
                                        <div class="progress-bar"></div>
                                    </div>
                                    <!-- Flyer files -->
                                    <div id="files" class="files"></div>
                                </div>
                            </div>
                        </div>
                        <hr />
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="input-group">
                                    <span class="input-group-addon"><?php echo lang('txt_horizontal'); ?></span>
                                    <input type="text" placeholder="<?php echo lang('txt_inpixels'); ?>" value="0" data-field="horizontal" class="imageX form-control" />
					<span class="input-group-btn">
						<span class="btn btn-default qtyplus" data-field-name="horizontal" type="button">
							<span class="glyphicon glyphicon-chevron-up"></span>
						</span>
					</span>
					<span class="input-group-btn">
						<span class="btn btn-default qtyminus" data-field-name="horizontal" type="button">
							<span class="glyphicon glyphicon-chevron-down"></span>
						</span>
					</span>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="input-group">
                                    <span class="input-group-addon"><?php echo lang('txt_vertical'); ?></span>
                                    <input type="text" placeholder="<?php echo lang('txt_inpixels'); ?>" value="0" data-field="vertical" class="imageY form-control" />
					<span class="input-group-btn">
						<span class="btn btn-default qtyplus" data-field-name="vertical" type="button">
							<span class="glyphicon glyphicon-chevron-up"></span>
						</span>
					</span>
					<span class="input-group-btn">
						<span class="btn btn-default qtyminus" data-field-name="vertical" type="button">
							<span class="glyphicon glyphicon-chevron-down"></span>
						</span>
					</span>
                                </div>
                            </div>
                        </div>
                        <hr />
                        <div class="row">
                            <div class="col-lg-12">

				<span class="input-group-addon" style="height:90px; ">
                    <table style="padding: 5px; text-align: center;align-content: center;" width="100%" >
                        <tr>
                            <td style="align:right;" width="35%">
                                &nbsp;
                            </td>

                            <td style="align:right;" width="10%">
                                <input type="checkbox" class="fit" id="fitBtn"><label for="fitBtn"><?php echo lang('ux_scale_fit'); ?></label>&nbsp;
                            </td>

                            <td width="10%">
                                <img style="border:0px dotted grey;" id="img_preview" src="<?php echo base_url() ?>static/images/blank.gif" >
                            </td>

                            <td style="align: left" width="10%">
                                <span id="span_img_preview_info" ></span>
                            </td>

                            <td style="align:right;" width="35%" >
                                &nbsp;
                            </td>

                        </tr>
                    </table>
			    </span>

                            </div>
                        </div>
                    </div>
                    <!-- User Options -->
                    <div class="userOptionsTools" style="display:none;">
                        <hr />
                        <h3><?php echo lang('ux_user'); ?></h3>
                        <hr />
                        <div class="col-lg-12">
                            <table class="table">
                                <tbody>
                                <tr>
                                    <td><label for="opt-add_text"><?php echo lang('opt_add_text'); ?></label></td>
                                    <td><input type="checkbox" class="js-switch opt-add_text" id="opt-add_text" /></td>
                                </tr>
                                <tr>
                                    <td><label for="opt-font_color"><?php echo lang('opt_font_color'); ?></label></td>
                                    <td><input type="checkbox" class="js-switch opt-font_color" id="opt-font_color" /></td>
                                </tr>
                                <tr>
                                    <td><label for="opt-font_background"><?php echo lang('opt_font_background'); ?></label></td>
                                    <td><input type="checkbox" class="js-switch opt-font_background" id="opt-font_background" /></td>
                                </tr>
                                <tr>
                                    <td><label for="opt-font_size"><?php echo lang('opt_font_size'); ?></label></td>
                                    <td><input type="checkbox" class="js-switch opt-font_size" id="opt-font_size" /></td>
                                </tr>
                                <tr>
                                    <td><label for="opt-font_face"><?php echo lang('opt_font_face'); ?></label></td>
                                    <td><input type="checkbox" class="js-switch opt-font_face" id="opt-font_face" /></td>
                                </tr>
                                <tr>
                                    <td><label for="opt-font_align_left"><?php echo lang('opt_font_align_left'); ?></label></td>
                                    <td><input type="checkbox" class="js-switch opt-font_align_left" id="opt-font_align_left" /></td>
                                </tr>
                                <tr>
                                    <td><label for="opt-font_align_center"><?php echo lang('opt_font_align_center'); ?></label></td>
                                    <td><input type="checkbox" class="js-switch opt-font_align_center" id="opt-font_align_center" /></td>
                                </tr>
                                <tr>
                                    <td><label for="opt-font_align_right"><?php echo lang('opt_font_align_right'); ?></label></td>
                                    <td><input type="checkbox" class="js-switch opt-font_align_right" id="opt-font_align_right" /></td>
                                </tr>
                                <tr>
                                    <td><label for="opt-font_bold"><?php echo lang('opt_font_bold'); ?></label></td>
                                    <td><input type="checkbox" class="js-switch opt-font_bold" id="opt-font_bold" /></td>
                                </tr>
                                <tr>
                                    <td><label for="opt-font_italic"><?php echo lang('opt_font_italic'); ?></label></td>
                                    <td><input type="checkbox" class="js-switch opt-font_italic" id="opt-font_italic" /></td>
                                </tr>
                                <tr>
                                    <td><label for="opt-font_rotate"><?php echo lang('opt_font_rotate'); ?></label></td>
                                    <td><input type="checkbox" class="js-switch opt-font_rotate" id="opt-font_rotate" /></td>
                                </tr>
                                </tbody>
                            </table>
                            </table>
                        </div>
                    </div>
                    <br />
                    <!-- Spinner -->
                    <div id='loading'></div>
                    <div id='flyer-export'></div>
                    <canvas id="canvasEditor"></canvas>
                    <input type="hidden" class="fontColor" value="" />
                    <input type="hidden" class="backColor" value="" />
                    <input type="hidden" class="stageColor" value="" />
                </div>
            </div>



            <?php if (!$isInsert) : ?>
                <div class="row editor_row">
                    <label for="status" class="col-lg-2 control-label">Status</label>
                    <div class="col-lg-10">
                        <input type="text" tabindex="-1" readonly="" size="30" value="<?php echo $FlyerTemplates->getItemStatusLabel($template['status']) ?>" name="status" id="status" class="form-control">
                    </div>
                </div>
            <?php endif;?>

            <?php if (!$isInsert) : ?>
                <div class="row editor_row">
                    <label for="wp_id" class="col-lg-2 control-label">Wordpress ID link</label>

                    <div class="col-lg-10">
                        <input type="text" tabindex="-1" readonly="" size="30" value="<?php echo $template['wp_id'] ?>" name="wp_id" id="wp_id" class="form-control">
                    </div>
                </div>
            <?php endif;?>


        </div> <!-- div class="bootstrap-admin-no-table-panel-content bootstrap-admin-panel-content collapse in" END -->


        <div>
            <input class="grey" type="button" id="btn_submit" name="btn_submit" onclick="javascript:onSubmit();" value="<?php echo ( $isInsert ? "Create" : "Update" ) ?>">
            <input class="grey" type="button" id="btn_cancel" name="btn_cancel" value="Cancel" onclick="javascript:document.location= '<?php echo site_url("templates/index")  ?>'">
        </div>

</div>

</form>

<?php $this->load->view( 'common/footer' ); ?>
</div>