<?php
$this->load->view( 'common/header' );
load_menu();
?>
<div id="saving-overlay"></div>
<div class="flyers-section">

    <script type="text/javascript" charset="utf-8">
        //<![CDATA[

        function deleteTemplate(template_id, template_name) {
            //alert( "deleteTemplate template_id::"+var_dump(template_id) )
            if (!confirm("Do you want to delete '"+template_name+"' with all its data ?")) return
            document.location = "<?php echo site_url("templates/delete") ?>/" + template_id
        }

        function FilterApplied() {
            $("#page").val(1)
            var theForm = document.getElementById("form_templates");
            theForm.submit();
        }
        //]]>
    </script>

    <form action=<?php echo site_url("templates/index/page/1") ?> method="post" accept-charset="utf-8" id="form_templates" name="form_templates" enctype="multipart/form-data">
        <input type="hidden" id="page" name="page" value="1">
        <input type="hidden" id="sort" name="sort" value="">
        <input type="hidden" id="sort_direction" name="sort_direction" value="">

        <div class="panel panel-default">
            <div class="panel-heading" style="height: 70px;">
                <h4 style="margin-top: -10px;">Templates List</h4>

                <div  class=" panel-heading col-md-12" style="margin-top: -10px;">

                    <div  class="col-md-3">
                    <input value="<?php echo $filter_name?>" placeholder="Enter filter for name" maxlength="50" id="filter_name" name="filter_name">
                    &nbsp;&nbsp;
                    <select id="filter_format" name="filter_format">
                        <option value="">  -Select Format-  </option>
                        <?php foreach( $templateFormatValueArray as $nextFormat ) { ?>
                        <option <?php echo ( $filter_format == $nextFormat['key'] ? 'selected' : '')?> value="<?php echo $nextFormat['key'] ?>"><?php echo $nextFormat['value']
                            ?></option>
                        <?php }?>
                    </select>
                    </div>

                    <div  class="col-md-3">
                    <input value="<?php echo $filter_sku?>" placeholder="Enter filter for sku" maxlength="10" id="filter_sku" name="filter_sku">
                    &nbsp;&nbsp;
                    <select id="filter_category_id" name="filter_category_id">
                        <option value="">  -Select Category-  </option>
                        <?php foreach( $categoriesSelectionList as $nextCategory ) { ?>
                        <option <?php echo ( $filter_category_id == $nextCategory['key'] ? 'selected' : '')?> value="<?php echo $nextCategory['key'] ?>"><?php echo $nextCategory['value'] ?></option>
                        <?php }?>
                    </select>
                    </div>



                    <div  class="col-md-6">
                    <button onclick="javascript:FilterApplied();" class="btn-default">
                        &nbsp;Apply&nbsp;Filter
                    </button>
                    &nbsp;<?php echo count($templateList) ?>&nbsp;item(s)
                    </div>
                </div>
            </div>
            <!-- div class="panel-heading" END -->
            <?php if (!empty($editor_message) ) : ?>
                <div class="alert alert-success">
                    <?php echo $editor_message ?>
                </div>
            <?php endif; ?>

            <div class="bootstrap-admin-panel-content">

                <?php if ( count($templateList) > 0 ) : ?>
                    <table class="table table-striped table-bordered dataTable">
                        <thead>
                        <tr>
                            <th>
                                Name
                            </th>
                            <th>
                                Sku
                            </th>
                            <th>
                                Category
                            </th>
                            <th>
                                Width
                            </th>
                            <th>
                                Height
                            </th>
                            <th>
                                Format
                            </th>

                            <th>
                                Status
                            </th>
                            <th>
                                WP ID
                            </th>
                            <th colspan="2"></th>
                        </tr>
                        </thead>
                        <tbody>


                        <?php $i=0;  foreach( $templateList as $nextTemplate ) { ?>

                            <tr class="<?php echo ($i % 2 == 0 ? 'even' : 'odd' ) ?> gradeX">
                                <td>
                                    <?php echo $nextTemplate['id'].'->'.$nextTemplate['name']; ?>
                                </td>
                                <td>
                                    <?php echo $nextTemplate['sku']; ?>
                                </td>
                                <td>
                                    <?php echo $nextTemplate['category_name']; ?>
                                </td>
                                <td>
                                    <?php echo $nextTemplate['width']; ?>
                                </td>
                                <td>
                                    <?php echo $nextTemplate['height']; ?>
                                </td>
                                <td>
                                    <?php echo $nextTemplate['format']; ?>
                                </td>

                                <td>
                                    <?php echo $FlyerTemplates->getItemStatusLabel($nextTemplate['status']); ?>
                                </td>

                                <td class="left">
                                    <?php echo $nextTemplate['wp_id']; ?>
                                </td>

                                <td class="left" >
                                    <a href="<?php echo site_url("templates/edit/".$nextTemplate['id']) ?>" class="edit">Edit</a>&nbsp;&nbsp;&nbsp;&nbsp;
                                </td>

                                <td class="left" >
                                    <a href="javascript:deleteTemplate( '<?php echo $nextTemplate['id']; ?>', '<?php echo $nextTemplate['name']; ?>' )" class="delete">Delete</a>
                                </td>

                            </tr>
                            <?php $i++; }?>

                        </tbody>
                    </table>
                <?php endif; ?>

                <a href="<?php echo site_url("templates/edit/new") ?>" class="grey">New Template</a>

            </div>
        </div>

    </form>

    <?php $this->load->view( 'common/footer' ); ?>
</div>