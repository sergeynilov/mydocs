<?php

class Article extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->load->model('mcms_item', '', true);
        $this->load->model('mcategory', '', true);
        $this->load->model('muser', '', true);
        $uriParameters = $this->uri->uri_to_assoc(1);

        $categoriesList = $this->mcategory->getCategoriesList(false, 0, array( 'product_status'=>'A', 'show_products_count'=> 1), 'products_count', 'desc' );

        $cms_Item= $this->mcms_item->getSimilarByAlias( $uriParameters['article'] );
        if ( empty($cms_Item) )	redirect('/', 'refresh');

        $cms_Item= $this->mcms_item->getRowById( $cms_Item['id'],['show_size_restricted_image'=>1] );

        $render_text_type= 'text';
        if ( $cms_Item['alias'] == 'faq' or $cms_Item['alias'] == 'security_privacy' ) {
            $cms_Item['content_array']= appUtils::splitRegisterInfo($cms_Item['content']);
            $render_text_type= 'accordion';
        }

        $contactUs = null;

        $twig_template= 'article/article';
        if ( $cms_Item['alias'] == 'contact_us' ) {
            $twig_template= 'article/contact_us';
            $this->load->library('form_validation');
            $this->form_validation->set_rules('contact_us_name', 'Name', 'trim|required');
            $this->form_validation->set_rules('contact_us_email', 'Email', 'trim|required|valid_email');
            $this->form_validation->set_rules('contact_us_phone', 'Phone', 'trim|required');
            $this->form_validation->set_rules('contact_us_message', 'message', 'trim|required');


            if (!empty($this->m_postArray)) { // form was submitted
                $validation_status = $this->form_validation->run();
                if ($validation_status != FALSE) { // REQUEST IS SUCCESSFULLY VALIDATED


//                    $this->email_send();
//                    $Content = $this->mcms_item->getBodyContentByAlias('user_was_banned',
//                        array('display_name' => $User['first_name'] . ' ' . $User['last_name'],
//                            'username' => $User['username'],
//                            'email' => $User['email'],
//                            'site_name' => $site_name,
//                            'support_signature' => $support_signature,
//                            'site_url' => $this->m_appConfig['base_root_url'],
//                        ), true);
//                    echo '<pre>$Title::'.print_r($Title,true).'</pre>';
//                    echo '<pre>$Content::'.print_r($Content,true).'</pre>';
//                    die("-1 XXZ");
//                    appUtils::debToFile(' ban_user $Content::' . print_r($Content, true), false);
//                    $email_output = appUtils::sendEmail($User['email'], $Title, $Content, true, $this->mcms_item->getCmsItemIdByAlias('user_was_banned'));
//                    echo '<pre>$email_output::'.print_r($email_output,true).'</pre>';
//                    $this->output->set_content_type('application/json')->set_output(json_encode(array('error_message' => '', 'error_code' => 0, 'Title' => $Title, 'Content' => $Content, 'id' => $ret, 'email_output' => $email_output)));

                } else {
                    $contactUs = $this->setInvalidEditableData($contactUs);
                } // if ($validation_status != FALSE) { // REQUEST IS SUCCESSFULLY VALIDATED
            } // if (!empty($this->m_postArray)) { // form was submitted

        }
        $data = array('cms_Item' => $cms_Item, 'categoriesList'=> $categoriesList, 'render_text_type'=> $render_text_type );
        $this->setPageTitle($cms_Item['title']);
        $this->setBreadcrumbs(array(
            array('title'=>'Home','url'=>base_url(),'icon'=>'fa fa-home','active'=>false),
            array('title'=>$cms_Item['title'], 'url'=>'','icon'=>'fa fa-address-card-o','active'=>true)
        ));
        $this->showTwigTemplate($twig_template, $data, false, true, true);
    }

    private function setInvalidEditableData($contactUs)
    {
        $contactUs['contact_us_name'] = set_value('contact_us_name');
        $contactUs['contact_us_email'] = set_value('contact_us_email');
        $contactUs['contact_us_phone'] = set_value('contact_us_phone');
        $contactUs['contact_us_message'] = set_value('contact_us_message');
        return $contactUs;
    }


}