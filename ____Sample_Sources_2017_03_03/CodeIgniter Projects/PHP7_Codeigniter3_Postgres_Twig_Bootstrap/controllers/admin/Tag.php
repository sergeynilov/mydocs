<?php
class Tag extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ( !$this->ion_auth->logged_in() or !$this->m_has_tag_editor_access or empty($this->session->userdata['is_logged']) ) {
            redirect(base_url() . 'admin/admin/msg/type/warning/msg/' . urlencode ( 'You have no access to this page !').'/action/show-login' );
        }
        $this->load->model('mtag', '', true);
    }

    public function index()
    {
        $this->load->helper('text');
        $layoutConfig= $this->getTemplateParams(array() , 'layout');
        $uriParameters = $this->uri->uri_to_assoc(4);
        $editor_message = $this->session->flashdata('editor_message');

        $filter_name = appUtils::getParameter($this, $uriParameters, $this->m_postArray, 'filter_name');
        $do_action = appUtils::getParameter($this, $uriParameters, $this->m_postArray, 'do_action');
        $new_tag = appUtils::getParameter($this, $uriParameters, $this->m_postArray, 'new_tag');


        $filter_created_at_from = appUtils::getParameter($this, $uriParameters, $this->m_postArray, 'filter_created_at_from');
        $filter_created_at_till = appUtils::getParameter($this, $uriParameters, $this->m_postArray, 'filter_created_at_till');
        $filter_created_at_from_formatted= appUtils::convertFromMySqlToCalendarFormat($filter_created_at_from);
        $filter_created_at_till_formatted= appUtils::convertFromMySqlToCalendarFormat($filter_created_at_till); //2016-09-05 -> 5 September, 2016

        $page = appUtils::getParameter($this, $uriParameters, $this->m_postArray, 'page', 1);
        $sort = appUtils::getParameter($this, $uriParameters, $this->m_postArray, 'sort');
        $sort_direction = appUtils::getParameter($this, $uriParameters, $this->m_postArray, 'sort_direction');
        $page_parameters_with_sort = $this->preparePageParameters($uriParameters, $this->m_postArray, false, true);
        $page_parameters_without_sort = $this->preparePageParameters($uriParameters, $this->m_postArray, false, false);


        if ( $do_action == "addNewTag" and !empty($new_tag) ) {
            get_instance()->load->library('form_validation');
	        $this->form_validation->set_rules('new_tag', 'Tag Name', 'trim|callback_check_new_tag_is_unique');

            $validation_status = $this->form_validation->run();
            if ($validation_status) { // there is no error
                $TagId = $this->mtag->updateTag( 0, array( 'name'=>$new_tag ) );
                $this->session->set_flashdata('editor_message', "Attribute '" . $new_tag . "' was added! ");
                redirect(base_url() . 'admin/tag/index' . $page_parameters_with_sort);
            }
        }

        $this->load->library('pagination');
        $paginationConfig= $this->getTemplateParams(array(),'pagination');
        $paginationConfig['base_url'] = base_url() . 'admin/tag/index/page/';

        $rows_in_request = $this->mtag->getTagsList(true, 0, array('name'=> $filter_name, 'created_at_from'=> $filter_created_at_from, 'created_at_till'=> $filter_created_at_till), $sort, $sort_direction);
        $paginationConfig['total_rows'] = $rows_in_request;
        $this->pagination->initialize($paginationConfig);
        $TagsList = array();
        if ($rows_in_request > 0) {
            $TagsList = $this->mtag->getTagsList(false, $page, array('name'=> $filter_name, 'created_at_from'=> $filter_created_at_from, 'created_at_till'=> $filter_created_at_till, 'show_products_count'=> 1), $sort, $sort_direction, '');
        }

        $this->pagination->suffix = $this->preparePageParameters($uriParameters, $this->m_postArray, false, true);
        $this->pagination->cur_page= $page;
        $pagination_links = $this->pagination->create_links();
        $this->setPageTitle('Tags');
        $this->setBreadcrumbs( array(
            array( 'title'=>'Dashboard', 'url'=>base_url().'admin/dashboard/index', 'icon'=> 'fa fa-dashboard', 'active'=> false ),
            array( 'title'=>'Tags List', 'url'=>base_url().'admin/tag/index' . $page_parameters_with_sort, 'icon'=>'fa fa-tag', 'active'=> true ),
        )    );
        $data = array( 'TagsList' => $TagsList, 'rows_in_request' => $rows_in_request, 'page' => $page, 'page_parameters_with_sort' => $page_parameters_with_sort, 'page_parameters_without_sort' => $page_parameters_without_sort, 'sort' => $sort, 'sort_direction' => $sort_direction, 'pagination_links' => $pagination_links, 'editor_message' => $editor_message,  'filter_created_at_from' => $filter_created_at_from, 'filter_created_at_till' => $filter_created_at_till, 'filter_created_at_from_formatted' => $filter_created_at_from_formatted, 'filter_created_at_till_formatted' => $filter_created_at_till_formatted, 'filter_name' => $filter_name, 'new_tag'=> $new_tag,

            'validation_errors_text' => validation_errors($layoutConfig['backend_error_icon_start'], $layoutConfig['backend_error_icon_end'])
            );
        $this->showTwigTemplate('tags_list', $data, true, true, true);
    }

    public function check_new_tag_is_unique($new_tag)
    {
        if (empty($new_tag)) {
            $this->form_validation->set_message('check_$ew_tag_is_unique', " The tag field is required ! ");
            return FALSE;
        }
        $ret= $this->mtag->getSimilarByName( $new_tag );
        if ($ret) {
            $this->form_validation->set_message('check_new_tag_is_unique', "Tag name '".$new_tag."' must be unique ! ");
            return FALSE;
        }
        return TRUE;
    }


    public function delete($id = 0)
    {
        $this->load->model('mtag', '', true);

        if (!empty($id)) {
            $uriParameters = $this->uri->uri_to_assoc(5);
            $page_parameters_with_sort = $this->preparePageParameters($uriParameters, null, false, true);
            $redirect_url = base_url().'admin/tag/index' . $page_parameters_with_sort;
            $Tag = $this->mtag->getRowById($id);
            if (empty($Tag)) {
                $this->session->set_flashdata('editor_message', "Tag '" . $id . "' not found");
                redirect($redirect_url);
                return;
            }
            $tagName = $Tag['name'];
            $ret = $this->mtag->DeleteTag($id);
            if ($ret) {
                $this->session->set_flashdata('editor_message',  $id . " : Tag" . " '" . $tagName . "' was deleted");
                redirect($redirect_url);
                return;
            }
        }

    }

    private function preparePageParameters($uriParameters, $postArray, $with_page, $with_sort)
    {
        $res_str = '';
        if (!empty($postArray)) { // form was submitted
            if ($with_page) {
                $page = $this->input->post('page');
                $res_str .= !empty($page) ? 'page/' . $page . '/' : 'page/1/';
            }
            $filter_name = $this->input->post('filter_name');
            $res_str .= !empty($filter_name) ? 'filter_name/' . $filter_name . '/' : '';
            $filter_created_at_from = $this->input->post('filter_created_at_from');
            $res_str .= !empty($filter_created_at_from) ? 'filter_created_at_from/' . $filter_created_at_from . '/' : '';
            $filter_created_at_till = $this->input->post('filter_created_at_till');
            $res_str .= !empty($filter_created_at_till) ? 'filter_created_at_till/' . $filter_created_at_till . '/' : '';
            if ($with_sort) {
                $sort_direction = $this->input->post('sort_direction');
                $res_str .= !empty($sort_direction) ? 'sort_direction/' . $sort_direction . '/' : '';
                $sort = $this->input->post('sort');
                $res_str .= !empty($sort) ? 'sort/' . $sort . '/' : '';
            }
        } else {
            if ($with_page) {
                $res_str .= !empty($uriParameters['page']) ? 'page/' . $uriParameters['page'] . '/' : 'page/1/';
            }
            $res_str .= !empty($uriParameters['filter_name']) ? 'filter_name/' . $uriParameters['filter_name'] . '/' : '';
            $res_str .= !empty($uriParameters['filter_created_at_from']) ? 'filter_created_at_from/' . $uriParameters['filter_created_at_from'] . '/' : '';
            $res_str .= !empty($uriParameters['filter_created_at_till']) ? 'filter_created_at_till/' . $uriParameters['filter_created_at_till'] . '/' : '';
            if ($with_sort) {
                $res_str .= !empty($uriParameters['sort_direction']) ? 'sort_direction/' . $uriParameters['sort_direction'] . '/' : '';
                $res_str .= !empty($uriParameters['sort']) ? 'sort/' . $uriParameters['sort'] . '/' : '';
            }
        }
        if (substr($res_str, strlen($res_str) - 1, 1) == '/') {
            $res_str = substr($res_str, 0, strlen($res_str) - 1);
        }
        return '/' . $res_str;
    }


}

