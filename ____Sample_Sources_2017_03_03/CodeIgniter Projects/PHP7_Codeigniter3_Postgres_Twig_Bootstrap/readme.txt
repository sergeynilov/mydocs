
1) Originally, that was a task to import Wordpress/wooCommerce products(using csv data files from Product CSV Import Export(BASIC) plugin ) into another application with PostgreSQL 
and crud operations must be done with PostgreSQL functions on server side.

2) I extended Product CSV Import Export(BASIC) plugin to export some more data like attributes, users, shipping classes and images of exported products were uploaded into subdirectory with 
resulting csv file. Wordpress users were copied too, with flag that they are from wordpress, so in the system there 2 authantification methods: native ci(Ion_auth) and wordpress login method.

3) file /lib/csv_import.php - demo file of import functionality.

4) In update_dictionaries.sql file : some CRUD functions for simple dictionaries.

5) In update_products.sql file :  CRUD functions for products manipulating.

6) In update_orders.sql file :  functions for orders manipulating.

7) In update_reports.sql file :  functions for reports/output listings/forms.

8) In views subdirectory there are twig files for backend and frontend.
File /lib/AppTwig.php is collection of filters and functions for using in twig templates.

9) In models subdirectory there are model files with methods specific for working with PostgreSQL for data updating/deleting/retrieving.

10) In controllers subdirectory there controllers for backend and frontend pages.

11) My task were to make import part and backend application(codeigniter3, jquery, bootstrap, twig, scss), while another developer(on client's side) was making frontend using yii.

12) For this demo I added orders and Charts reports and some other functionality.

13) Listing of orders looks like : http://imgur.com/a/Rjmic
Filters on many fields : http://imgur.com/a/TOCRb

14) Product editor has a lot of info : http://imgur.com/a/ovhCi
with categories, attributes, images(possible upload new image without page reloading http://imgur.com/a/TFz05), tags, comments, shipping information

15) All editors has data validation system, http://imgur.com/a/DYZgX
All listing has sorting/filters functionality on main fields, as in links above.
many editors has popup dialog pages for adding additive information in convenient way : http://imgur.com/a/FkI19

16) Chart reports :http://imgur.com/a/4s62P
with raw rows and detalization for any resulting row : http://imgur.com/a/z6tBj

