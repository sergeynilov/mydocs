<?php

class Mproduct extends CI_Model
{
    private $m_product_table;
    private $m_product_category_table;
    private $m_product_affiliated_user_table;
    private $m_tag_table;
    private $m_product_tag_table;
    private $m_product_image_table;
    private $m_product_shipping_table;
    private $m_product_attribute_table;
    private $m_category_table;
	private $m_product_bookmark_table;
    private $m_ProductStatusLabelValueArray = Array('A' => 'Active', 'I' => 'Inactive', 'P' => 'Pending Review', 'D' => 'Draft');
    private $m_ProductHasAttributesLabelValueArray = Array("Y" => 'Has Attributes', "N" => 'Has No Attributes');
    private $m_ProductDownloadableValueArray = Array("Y" => 'Is Downloadable', "N" => 'Is Not Downloadable');
    private $m_ProductVirtualLabelValueArray = Array("Y" => 'Is Virtual', "N" => 'Is Not Virtual');

    private $m_ProductInStockLabelValueArray = Array("Y" => 'In Stock', "N" => 'Out Of Stock');
    private $m_ProductIs_HomepageLabelValueArray = Array("Y" => 'Is Homepage', "N" => 'Is Not Homepage');
    private $m_ProductIs_FeaturedLabelValueArray = Array("Y" => 'Is Featured', "N" => 'Is Not Featured');
    private $m_fieldsMappingArray = [

        [ 'field'=>"in_stock", "field_type"=> "bool",  "values" => [ ['client_value'=>'Y', 'server_value'=>'TRUE', 'has_single_quote'=>false ],
        [ 'client_value'=>'N', 'server_value'=>'FALSE', 'has_single_quote'=>false ] ] ],

        [ 'field'=>"is_homepage", "field_type"=> "bool", "values" => [ ['client_value'=>'Y', 'server_value'=>'TRUE', 'has_single_quote'=>false ],
        [ 'client_value'=>'N', 'server_value'=>'FALSE', 'has_single_quote'=>false ] ] ],

        [ 'field'=>"is_featured", "field_type"=> "bool", "values" => [ ['client_value'=>'Y', 'server_value'=>'TRUE', 'has_single_quote'=>false ],
        [ 'client_value'=>'N', 'server_value'=>'FALSE', 'has_single_quote'=>false ] ] ],

        [ 'field'=>"has_attributes", "field_type"=> "bool", "values" => [ ['client_value'=>'Y', 'server_value'=>'TRUE', 'has_single_quote'=>false ],
        [ 'client_value'=>'N', 'server_value'=>'FALSE', 'has_single_quote'=>false ] ] ],

        [ 'field'=>"downloadable", "field_type"=> "bool", "values" => [ ['client_value'=>'Y', 'server_value'=>'TRUE', 'has_single_quote'=>false ],
        [ 'client_value'=>'N', 'server_value'=>'FALSE', 'has_single_quote'=>false ] ] ],

        [ 'field'=>"in_description", "field_type"=> "bool", "values" => [ ['client_value'=>'Y', 'server_value'=>'TRUE', 'has_single_quote'=>false ],
        [ 'client_value'=>'N', 'server_value'=>'FALSE', 'has_single_quote'=>false ] ] ],

        [ 'field'=>"virtual", "field_type"=> "bool", "values" => [ ['client_value'=>'Y', 'server_value'=>'TRUE', 'has_single_quote'=>false ],
        [ 'client_value'=>'N', 'server_value'=>'FALSE', 'has_single_quote'=>false ] ] ],
    ];
    public function __construct()
    {
        parent::__construct();
        $ci = & get_instance();
        $this->m_ionAuthConfigTables= $ci->get_ionAuthConfigTables();
        $this->m_users_table= $this->m_ionAuthConfigTables['users'];
        $this->m_category_table= $this->m_ionAuthConfigTables['category'];
        $this->m_product_table= $this->m_ionAuthConfigTables['product'];
        $this->m_product_category_table= $this->m_ionAuthConfigTables['product_category'];
        $this->m_product_affiliated_user_table= $this->m_ionAuthConfigTables['product_affiliated_user'];
        $this->m_tag_table= $this->m_ionAuthConfigTables['tag'];
        $this->m_product_tag_table= $this->m_ionAuthConfigTables['product_tag'];
        $this->m_product_image_table= $this->m_ionAuthConfigTables['product_image'];
        $this->m_product_shipping_table= $this->m_ionAuthConfigTables['product_shipping'];
        $this->m_product_attribute_table= $this->m_ionAuthConfigTables['product_attribute'];
	    $this->m_product_bookmark_table= $this->m_ionAuthConfigTables['product_bookmark'];
    }


	public function getRegionsGroupedByStatesSelectionList(  ) {
		$StatesList = $this->mstate->getStatesList(false, '', '', '', 'state.name', '', 'state.id, state.name');
		$res_array = array();
		foreach($StatesList as $State) {
			$RegionsList = $this->mregion->getRegionsList(false, '', $State['id'], '', 'region.name', '', 'region.id, region.name');
			$res_sub_array = array();
			foreach($RegionsList as $lRegion) {
				$res_sub_array[ $lRegion['id'] ] = $lRegion['name'];
			}
			$res_array[]= array( 'state_id'=> $State['id'], 'state_name'=> $State['name'], 'regions_sub_array'=> $res_sub_array );
		}
		return $res_array;
	}


	public function getProductStatusValueArray()
    {
        $ResArray = array();
        foreach ($this->m_ProductStatusLabelValueArray as $Key => $Value) {
            $ResArray[] = array('key' => $Key, 'value' => $Value);
        }
        return $ResArray;
    }

    public function getProductStatusLabel($status)
    {
        if (!empty($this->m_ProductStatusLabelValueArray[$status])) {
            return $this->m_ProductStatusLabelValueArray[$status];
        }
        return '';
    }


    public function getProductHasAttributesValueArray()
    {
        $ResArray = array();
        foreach ($this->m_ProductHasAttributesLabelValueArray as $Key => $Value) {
            $ResArray[] = array('key' => $Key, 'value' => $Value);
        }
        return $ResArray;
    }
    public function getProductHasAttributesLabel($has_attribute)
    {
        if (!empty($this->m_ProductHasAttributesLabelValueArray[$has_attribute])) {
            return $this->m_ProductHasAttributesLabelValueArray[$has_attribute];
        }
        return '';
    }

    ...

    public function getPricesList(bool $output_return_count = false, int $page = 0, array $filtersArray = array(), string $sort = '', string $sort_direction = '') {
        $ci = & get_instance();
        $filtersArray= $ci->makeSavingFieldsMapping( $filtersArray, $this->m_fieldsMappingArray );
        $filtersArray= [];
        $limit = '';
        $offset = '';
        $has_limit= false;
        if ( !$output_return_count ) {
            $config_data = $this->config->config;

            if ( !empty($filtersArray['per_page']) and appUtils::is_positive_integer($filtersArray['per_page'])  ) {
                $items_per_page= $filtersArray['per_page'];
            } else {
                $items_per_page = $ci->getSettings('price_list_items_per_page', $config_data['default_per_page']);
            }
            $limit = !empty($filtersArray['limit']) ? $filtersArray['limit'] : '';
            $offset = !empty($filtersArray['offset']) ? $filtersArray['offset'] : '';
            $is_page_positive_integer = appUtils::is_positive_integer($page);
            if (!empty($config_data) and $is_page_positive_integer) {
                $per_page = (!empty($filtersArray['default_per_page']) and appUtils::is_positive_integer($filtersArray['default_per_page'])) ? $filtersArray['default_per_page'] : $items_per_page;
                $limit = $per_page;
                $offset = ($page - 1) * $per_page;
                $has_limit= true;
            }
        }

        if ( $has_limit ) {
            $filtersArray[]= [ 'field_name'=> 'p_limit', 'value'=> appUtils::pgEscape($limit), 'type'=> 'N', 'is_array'=> false ];
            $filtersArray[]= [ 'field_name'=> 'p_offset', 'value'=> appUtils::pgEscape($offset), 'type'=> 'N', 'is_array'=> false ];
        }

        if ( !empty($filtersArray['title']) ) {
            $filtersArray[]= [ 'field_name'=> 'p_title', 'value'=> appUtils::pgEscape($filtersArray['title']), 'type'=> 'S', 'is_array'=> false ];
        }

        if ( !empty($filtersArray['status']) ) {
            $filtersArray[]= [ 'field_name'=> 'p_status', 'value'=> appUtils::pgEscape($filtersArray['status']), 'type'=> 'S', 'is_array'=> false ];
        }

        if ( !empty($filtersArray['in_stock']) ) {
            $filtersArray[]= [ 'field_name'=> 'p_in_stock', 'value'=>$filtersArray['in_stock'], 'type'=> 'B', 'is_array'=> false ];
        }

        if ( !empty($filtersArray['downloadable']) ) {
            $filtersArray[]= [ 'field_name'=> 'p_downloadable', 'value'=>$filtersArray['downloadable'], 'type'=> 'B', 'is_array'=> false ];
        }

        if ( !empty($filtersArray['virtual']) ) {
            $filtersArray[]= [ 'field_name'=> 'p_virtual', 'value'=>$filtersArray['virtual'], 'type'=> 'B', 'is_array'=> false ];
        }

        if ( !empty($filtersArray['sku']) ) {
            $filtersArray[]= [ 'field_name'=> 'p_sku', 'value'=> appUtils::pgEscape($filtersArray['sku']), 'type'=> 'S', 'is_array'=> false ];
        }

        if ( !empty($filtersArray['sale_price_from']) ) {
            $filtersArray[]= [ 'field_name'=> 'p_sale_price_from', 'value'=> appUtils::pgEscape($filtersArray['sale_price_from']), 'type'=> 'N', 'is_array'=> false ];
        }

        if ( !empty($filtersArray['sale_price_till']) ) {
            $filtersArray[]= [ 'field_name'=> 'p_sale_price_till', 'value'=> appUtils::pgEscape($filtersArray['sale_price_till']), 'type'=> 'N', 'is_array'=> false ];
        }

        if ( !empty($filtersArray['rating_from']) ) {
            $filtersArray[]= [ 'field_name'=> 'p_rating_from', 'value'=> appUtils::pgEscape($filtersArray['rating_from']), 'type'=> 'N', 'is_array'=> false ];
        }

        if ( !empty($filtersArray['rating_till']) ) {
            $filtersArray[]= [ 'field_name'=> 'p_rating_till', 'value'=> appUtils::pgEscape($filtersArray['rating_till']), 'type'=> 'N', 'is_array'=> false ];
        }

        if ( !empty($filtersArray['category_list']) ) {
            $filtersArray[]= [ 'field_name'=> 'p_category_list', 'value'=>$filtersArray['category_list'], 'type'=> 'N', 'is_array'=> true ];
        }

        if ( !empty($sort_direction) and !$output_return_count ) {
            $filtersArray[]= [ 'field_name'=> 'p_sort_direction', 'value'=> appUtils::pgEscape($sort_direction), 'type'=> 'S', 'is_array'=> false ];
        }

        if ( !empty($sort) and !$output_return_count ) {
            $filtersArray[]= [ 'field_name'=> 'p_sort', 'value'=> appUtils::pgEscape($sort), 'type'=> 'S', 'is_array'=> false ];
        }

        $sql_params_str= $ci->updateValuesArrayRoString($filtersArray);
        $funcname= $this->db->dbprefix."get_product_prices".($output_return_count?"_rows_count":"");
        $sql= " select * from ".$funcname."( " . $sql_params_str . " )";
        $retArray= $ci->dbquery(   $sql   );
        if (  $output_return_count ) {
            if ( !empty($retArray[0][$this->db->dbprefix.'get_product_prices_rows_count']) ) {
                return $retArray[0][$this->db->dbprefix.'get_product_prices_rows_count'];
            }
            return false;
        }
        $price_list_default_product_image_path = $this->m_appConfig['document_root'] . $this->m_appConfig['uploads_settings_dir'] . $ci->getSettings('price_list_default_product_image');
        $default_product_image = $ci->getSettings('price_list_default_product_image');
        $price_list_default_product_image_url = $this->m_appConfig['base_url'] . '/' . $this->m_appConfig['uploads_settings_dir'] . $ci->getSettings('price_list_default_product_image');

        foreach( $retArray as $next_key=>$next_Product ) { // all products in result set
            $retArray[$next_key]= $ci->makeRetreivingFieldsMapping( $next_Product, $this->m_fieldsMappingArray );

            if ( !empty($filtersArray['show_all_images']) ) {
                $product_ImagesList= $ci->mproduct_image->getProduct_ImagesList( false, 0, array('product_id'=> $next_Product['id'],  'show_size_restricted_image' => ( !empty($filtersArray['show_size_restricted_image']) ? $filtersArray['show_size_restricted_image'] : '' ), 'show_default_image'=> ( !empty($filtersArray['show_default_image']) ? $filtersArray['show_default_image'] : '' ) ) );
                if (!empty($product_ImagesList)) {
                    $retArray[$next_key]['product_ImagesList'] = $product_ImagesList;
                } else {

                }
            }

            if ( !empty($filtersArray['show_main_image_info']) ) {
                $product_main_image = $this->getProduct_ImageDir($next_Product['id']) . $next_Product['main_image'];
                $retArray[$next_key]['file_info'] = '';
                if ( file_exists($product_main_image) and !is_dir($product_main_image) ) {
                    $file_info = $next_Product['main_image'];
                    $file_info .= ', ' . appUtils::getFileSizeAsString(filesize($product_main_image));
                    $fileArray = @getimagesize($product_main_image);
                    if (!empty($fileArray)) {
                        $file_info .= ', ' . $fileArray[0] . 'x' . $fileArray[1];
                    }
                    $retArray[$next_key]['file_info'] = $file_info;
                }
            }
            if (!empty($filtersArray['show_main_image'])) {
                if (  !empty($next_Product['main_image']) ) {
                    $product_main_image = $this->getProduct_ImageDir($next_Product['id']) . $next_Product['main_image'];
                    $product_main_image_url = $this->getProduct_ImageImageUrl($next_Product['id'], $next_Product['main_image']);
                } else {
                    $product_main_image = $price_list_default_product_image_path;
                    $product_main_image_url = $price_list_default_product_image_url;
                    $retArray[$next_key]['main_image'] = $default_product_image;
                }
                if (  !empty($ci->m_appConfig['frontend_images_size_restrictions']) and is_array($ci->m_appConfig['frontend_images_size_restrictions']) ) {
                    $imageSizesRestrictions= appUtils::getImageRestrictionSizes( $product_main_image, $ci->m_appConfig['frontend_images_size_restrictions'] );
                    $retArray[$next_key]['imageSizesRestrictions']= $imageSizesRestrictions;
                    $retArray[$next_key]['main_image_path']= $product_main_image;
                    $retArray[$next_key]['main_image_url']= $product_main_image_url;
                }
            }

        } // foreach( $retArray as $next_key=>$next_Product ) { // all products in result set
        return $retArray;
    }


    public function getProductsList(bool $output_return_count = false, int $page = 0, array $filtersArray = array(), string $sorting = '', string $sort_direction = '')
    {
        $ci = &get_instance();
        $filtersArray = $ci->makeSavingFieldsMapping($filtersArray, $this->m_fieldsMappingArray);
        if (empty($sorting))
            $sorting = 'title';
        $config_data = $this->config->config;
        $items_per_page = $ci->getSettings('items_per_page', $config_data['default_per_page']);
        $limit = !empty($filtersArray['limit']) ? $filtersArray['limit'] : '';
        $offset = !empty($filtersArray['offset']) ? $filtersArray['offset'] : '';
        $is_page_positive_integer = appUtils::is_positive_integer($page);
        if (!empty($page) and $is_page_positive_integer) {
            $limit = '';
            $offset = '';
        }
        if (!empty($config_data) and $is_page_positive_integer) {
            $per_page = (!empty($filtersArray['per_page']) and appUtils::is_positive_integer($filtersArray['per_page'])) ? $filtersArray['per_page'] : $items_per_page;
            $limit = $per_page;
            $offset = ($page - 1) * $per_page;
        }

        if (!empty($filtersArray['title'])) {
            if (empty($filtersArray['in_description'])) {
                $this->db->where(appUtils::strLower($this->m_product_table . '.title', false, false) . ' like ' . appUtils::strLower($filtersArray['title'], true, true), '', false);
            } else {
                $this->db->where('( ' .
                    appUtils::strLower($this->m_product_table . '.title', false, false) . ' like ' . appUtils::strLower($filtersArray['title'], true, true) . ' OR ' .
                    appUtils::strLower($this->m_product_table . '.short_description', false, false) . ' like ' . appUtils::strLower($filtersArray['title'], true, true) . ' OR ' .
                    appUtils::strLower($this->m_product_table . '.description', false, false) . ' like ' . appUtils::strLower($filtersArray['title'], true, true) .
                    ' )', '', false);
            }
        }

        if (!empty($filtersArray['created_at_from'])) {
            $this->db->where($this->m_product_table . '.created_at >= ' . "'" . $filtersArray['created_at_from'] . "'");
        }
        if (!empty($filtersArray['created_at_till'])) {
            $this->db->where($this->m_product_table . '.created_at <= ' . "'" . $filtersArray['created_at_till'] . " 23:59:59'");
        }
        if (!empty($filtersArray['sale_price_from'])) {
            $this->db->where($this->m_product_table . '.sale_price >= ' . $filtersArray['sale_price_from']);
        }
        if (!empty($filtersArray['sale_price_till'])) {
            $this->db->where($this->m_product_table . '.sale_price <= ' . $filtersArray['sale_price_till']);
        }
        $is_product_category_joined = false;
        $is_category_joined = false;
	    $is_bookmark_user_joined = false;
	    $is_product_shipping_joined = false;
	    $is_with_affiliated_user_joined = false;


	    if ( !empty($filtersArray['not_ids']) and is_array($filtersArray['not_ids']) ) {
            $this->db->where_not_in($this->m_product_table . '.id', $filtersArray['not_ids']);
        }
	    if ( !empty($filtersArray['ids']) and is_array($filtersArray['ids']) ) {
            $this->db->where_in($this->m_product_table . '.id', $filtersArray['ids']);
        }

        if (!empty($filtersArray['with_affiliated_user'])) {
        	if ( in_array($filtersArray['with_affiliated_user'],['A','I','N']) ) {
		        $this->db->where( '   exists ( select from ' . $this->m_product_affiliated_user_table . ' as au where "au"."product_id" = "' . $this->m_product_table . '"."id" and "au"."status" = \'' . $filtersArray['with_affiliated_user'] . '\')   ', '', false );
	        }
	        if ( in_array($filtersArray['with_affiliated_user'],['Y']) ) {
		        $this->db->where( '   exists ( select from ' . $this->m_product_affiliated_user_table . ' as au where "au"."product_id" = "' . $this->m_product_table . '"."id" )   ', '', false );
	        }
	        $is_with_affiliated_user_joined = true;
        }


        if (!empty($filtersArray['category'])) {
            $this->db->join($this->m_product_category_table, $this->m_product_category_table . '.product_id = ' . $this->m_product_table . '.id', 'left');
            $this->db->where_in($this->m_product_category_table . '.category_id', appUtils::pregSplit('/,/', $filtersArray['category']));
            $is_product_category_joined = true;
        }

        if (!empty($filtersArray['tags_id'])) {
            $this->db->join($this->m_product_tag_table, $this->m_product_tag_table . '.product_id = ' . $this->m_product_table . '.id', 'left');
            if ( is_array($filtersArray['tags_id']) ) {
                $this->db->where_in($this->m_product_tag_table . '.tag_id', $filtersArray['tags_id'] );
            }   else {
                $this->db->where_in($this->m_product_tag_table . '.tag_id', appUtils::pregSplit('/,/', $filtersArray['tags_id']));
            }
            $is_product_category_joined = true;
        }

        if (!empty($filtersArray['status'])) {
            $this->db->where($this->m_product_table . '.status = ' . "'" . $filtersArray['status'] . "'");
        }
        if (!empty($filtersArray['in_stock'])) {
            $this->db->where($this->m_product_table . '.in_stock = ' . $filtersArray['in_stock']);
        }

        if (!empty($filtersArray['shipping_class_id'])) {
        	if ( !$is_product_shipping_joined ) {
		        $is_product_shipping_joined = true;
		        $this->db->join( $this->m_product_shipping_table, $this->m_product_shipping_table . '.id = ' . $this->m_product_table . '.id', 'left' );
	        }
            $this->db->where($this->m_product_shipping_table . '.shipping_class_id = ' . $filtersArray['shipping_class_id']);
        }

        if (!empty($filtersArray['is_homepage'])) {
            $this->db->where($this->m_product_table . '.is_homepage = ' . $filtersArray['is_homepage']);
        }
        if (!empty($filtersArray['is_featured'])) {
            $this->db->where($this->m_product_table . '.is_featured = ' . $filtersArray['is_featured']);
        }

        $additive_fields_for_select = "  ";
        $fields_for_select = $this->m_product_table . ".*, CAST( floor( rating_summary / NULLIF(rating_count,0)  ) AS INTEGER ) as rating ";

        if ( ( !empty($filtersArray['show_main_image']) OR !empty($filtersArray['show_main_image']) ) and !$output_return_count ) {
            $additive_fields_for_select .= ", ( select image FROM ".$this->m_product_image_table." as pi WHERE pi.product_id = ".$this->m_product_table.".id AND pi.is_main= TRUE ) as main_image ";
        }
        if (  !empty($filtersArray['show_username'])  ) {
            $this->db->join( $this->m_users_table, $this->m_users_table . '.id = ' . $this->m_product_table . '.user_id', 'left' );
            $additive_fields_for_select.= ', '.$this->m_users_table.'.username as username ';
        }

        if ( !empty($filtersArray['show_in_categories']) and !$output_return_count) {
            $additive_fields_for_select .= ", array_to_string(array_agg(  ".$this->m_category_table.".name  ORDER BY ".$this->m_category_table.".name   ),',' ) as category_name ";
            if ( !$is_product_category_joined ) {
                $this->db->join($this->m_product_category_table, $this->m_product_category_table . '.product_id = ' . $this->m_product_table . '.id', 'left');
                $is_product_category_joined= true;
            }
            if ( !$is_category_joined ) {

                $this->db->join($this->m_category_table, $this->m_category_table . '.id = ' . $this->m_product_category_table . '.category_id', 'left');
                $is_category_joined= true;
            }
        }


	    if (!empty($filtersArray['bookmark_user_id'])) {
		    $additive_fields_for_select .= ', ( select count(*) from ' . $this->m_product_bookmark_table . ' where ' . $this->m_product_bookmark_table . '.product_id = ' . $this->m_product_table . '.id and '.$this->m_product_bookmark_table.'.user_id = ' . $filtersArray['bookmark_user_id'] . ' ) as bookmarks_count ';
		    $is_bookmark_user_joined = true;
	    }

        if ( !$output_return_count) {
            $this->db->group_by( $this->m_product_table . '.id' );
            if (  !empty($filtersArray['show_username'])  ) {
                $this->db->group_by( 'username' );
            }
        }
        if ( !empty($filtersArray['fields_for_select']) ) {
            $fields_for_select= $filtersArray['fields_for_select'];
        }

        if ( ( !empty($limit) and appUtils::is_positive_integer($limit) ) and ( !empty($offset) and appUtils::is_positive_integer($offset) ) ) {
            $this->db->limit($limit, $offset);
        }

        if ( ( !empty($limit) and appUtils::is_positive_integer($limit) ) ) {
            $this->db->limit($limit);
        }

        $fields_for_select.= ' ' . $additive_fields_for_select;

        if (!empty($sorting)) {
            $this->db->order_by($sorting, ((strtolower($sort_direction) == 'desc' or strtolower($sort_direction) == 'asc') ? $sort_direction : ''));
        }


        if ($output_return_count) {
            return $this->db->count_all_results($this->m_product_table);
        } else {
            $query = $this->db->from($this->m_product_table);
            if (strlen(trim($fields_for_select)) > 0) {
                $query->select($fields_for_select);
            }
            $retArray= $query->get()->result('array');
            $price_list_default_product_image_path = $this->m_appConfig['document_root'] . $this->m_appConfig['uploads_settings_dir'] . $ci->getSettings('price_list_default_product_image');
            $default_product_image = $ci->getSettings('price_list_default_product_image');
            $price_list_default_product_image_url = $this->m_appConfig['base_url'] . '/' . $this->m_appConfig['uploads_settings_dir'] . $ci->getSettings('price_list_default_product_image');

            foreach( $retArray as $next_key=>$next_Product ) { // all products in result set
                $retArray[$next_key]= $ci->makeRetreivingFieldsMapping( $next_Product, $this->m_fieldsMappingArray );
                if ( !empty($filtersArray['show_all_images']) ) {
                    $product_ImagesList= $ci->mproduct_image->getProduct_ImagesList( false, 0, array('product_id'=> $next_Product['id'],  'show_size_restricted_image' => ( !empty($filtersArray['show_size_restricted_image']) ? $filtersArray['show_size_restricted_image'] : '' ), 'show_default_image'=> ( !empty($filtersArray['show_default_image']) ? $filtersArray['show_default_image'] : '' ) ) );
                    $retArray[$next_key]['product_ImagesList']= $product_ImagesList;
                }

                if ( !empty($filtersArray['show_main_image_info']) ) {
                    $product_main_image = $this->getProduct_ImageDir($next_Product['id']) . $next_Product['main_image'];
                    $retArray[$next_key]['file_info'] = '';
                    if (file_exists($product_main_image)) {
                        $file_info = $next_Product['main_image'];
                        $file_info .= ', ' . appUtils::getFileSizeAsString(filesize($product_main_image));
                        $fileArray = @getimagesize($product_main_image);
                        if (!empty($fileArray)) {
                            $file_info .= ', ' . $fileArray[0] . 'x' . $fileArray[1];
                        }
                        $retArray[$next_key]['file_info'] = $file_info;
                    }
                }
                if (!empty($filtersArray['show_main_image'])) {
                    if (  !empty($next_Product['main_image']) ) {
                        $product_main_image = $this->getProduct_ImageDir($next_Product['id']) . $next_Product['main_image'];
                        $product_main_image_url = $this->getProduct_ImageImageUrl($next_Product['id'], $next_Product['main_image']);
                    } else {
                        $product_main_image = $price_list_default_product_image_path;
                        $product_main_image_url = $price_list_default_product_image_url;
                        $retArray[$next_key]['main_image'] = $default_product_image;
                    }
                    if (  !empty($ci->m_appConfig['frontend_images_size_restrictions']) and is_array($ci->m_appConfig['frontend_images_size_restrictions']) ) {
                        $imageSizesRestrictions= appUtils::getImageRestrictionSizes( $product_main_image, $ci->m_appConfig['frontend_images_size_restrictions'] );
                        $retArray[$next_key]['imageSizesRestrictions']= $imageSizesRestrictions;
                        $retArray[$next_key]['main_image_path']= $product_main_image;
                        $retArray[$next_key]['main_image_url']= $product_main_image_url;
                    }

                }

            } // foreach( $retArray as $next_key=>$next_Product ) { // all products in result set
            return $retArray;
        }
    }


    public function getSimilarBySku(string $sku, string $product_id='')
    {
        $fields_for_select=$this->m_product_table.".*";
        $ci = & get_instance();
        $this->db->where(appUtils::strLower($this->m_product_table.'.sku',false,false).' =  TRIM( ' . appUtils::strLower($sku,true,false).' ) ','',false);
        if (!empty($product_id)) {
            $this->db->where($this->m_product_table.'.id != ' . $product_id);
        }
        $query = $this->db->from($this->m_product_table);
        $query->select($fields_for_select);
        $ret= $query->get()->result('array');
        if (empty($ret[0])) return false;
        return $ci->makeRetreivingFieldsMapping( $ret[0], $this->m_fieldsMappingArray );
    }

    public function getSimilarBySlug(string $slug, string $product_id='')
    {
        $fields_for_select=$this->m_product_table.".*";
        $ci = & get_instance();
        $this->db->where(appUtils::strLower($this->m_product_table.'.slug',false,false).' =  TRIM( ' . appUtils::strLower($slug,true,false).' ) ','',false);
        if (!empty($product_id)) {
            $this->db->where($this->m_product_table.'.id != ' . $product_id);
        }
        $query = $this->db->from($this->m_product_table);
        $query->select($fields_for_select);
        $ret= $query->get()->result('array');
        if (empty($ret[0])) return false;
        return $ci->makeRetreivingFieldsMapping( $ret[0], $this->m_fieldsMappingArray );
    }

    public function getRowById( int $id, array  $additiveParams= array( ) )
    {
        $additive_fields_for_select= '';

        $fields_for_select= $this->m_product_table . '.*';
        if (!empty( $additiveParams['fields_for_select'] )) {
            $fields_for_select= $additiveParams['fields_for_select'];
        }
        if ( !empty($additiveParams['show_products_count']) ) {
            $additive_fields_for_select.= ', ( select count(*) from ' . $this->m_product_table . ' where ' . $this->m_product_table.'
            .product_id = ' . $this->m_product_table . '.id ) as product_product_count ';
        }

        if ( !empty($additiveParams['show_product_categories'])  ) {
            $additive_fields_for_select.= ',  ( select array_agg(c.name) from ' . $this->m_category_table . ' as c, ' . $this->m_product_category_table . ' as pd where c.id= pd.category_id and pd.product_id = ' . $this->m_product_table . '.id) as product_categories, ' .
                ',  ( select array_agg(c.slug) from ' . $this->m_category_table . ' as c, ' . $this->m_product_category_table . ' as pd where c.id= pd.category_id and pd.product_id = ' . $this->m_product_table . '.id) as product_categories_slug, ' .
'( select array_agg(c.id) from ' . $this->m_category_table . ' as c, ' . $this->m_product_category_table . ' as pd where c.id= pd.category_id and pd.product_id = ' . $this->m_product_table . '.id) as product_categories_id ';
        }

        if ( !empty($additiveParams['show_product_tags'])  ) {
            $additive_fields_for_select.= ', ( select array_agg(t.name) from ' . $this->m_tag_table . ' as t, ' . $this->m_product_tag_table . ' as pt where t.id= pt.tag_id and pt.product_id = ' . $this->m_product_table . '.id) as product_tags,
( select array_agg(t.id) from ' . $this->m_tag_table . ' as t, ' . $this->m_product_tag_table . ' as pt where t.id= pt.tag_id and pt.product_id = ' . $this->m_product_table . '.id) as product_tags_id ';
        }

        if ( !empty($additiveParams['show_product_attributes'])  ) {
            $additive_fields_for_select.= ', ( select pa.attributes_data from ' . $this->m_product_attribute_table . ' as pa where pa.product_id= ' . $this->m_product_table . '.id ) as product_attribute ';
        }

        if ( !empty($additiveParams['show_main_image']) ) {
            $additive_fields_for_select .= ", ( select image FROM ".$this->m_product_image_table." as pi WHERE pi.product_id = ".$this->m_product_table.".id AND pi.is_main= TRUE ) as main_image ";
        }

	    if (!empty($additiveParams['bookmark_user_id'])) {
		    $additive_fields_for_select .= ', ( select count(*) from ' . $this->m_product_bookmark_table . ' where ' . $this->m_product_bookmark_table . '.product_id = ' . $this->m_product_table . '.id and '.$this->m_product_bookmark_table.'.user_id = ' . $additiveParams['bookmark_user_id'] . ' ) as bookmarks_count ';
	    }


	    $fields_for_select.= $additive_fields_for_select;
        $this->db->where( $this->m_product_table . '.id', $id);

        $this->db->select( $fields_for_select );

        $ci = & get_instance();
        $query = $this->db->from($this->m_product_table);
        $resultRows = $query->get()->result('array');
        if ( !empty($resultRows[0]) ) {
            $productRow= $resultRows[0];
            $productRow['rating']= !empty($productRow['rating_summary']) ? (int) floor($productRow['rating_summary'] / $productRow['rating_summary']) : 0;
            if (!empty($additiveParams['show_file_info']) and !empty($productRow['img'])) {
                $cms_item_img = $this->getProduct_ImageDir($id) . $productRow['img'];
                $productRow['file_info'] = '';
                if ( file_exists($cms_item_img) ) {
                    $file_info= $productRow['img'];
                    $file_info.= ', '.appUtils::getFileSizeAsString( filesize($cms_item_img) );
                    $fileArray = @getimagesize($cms_item_img);
                    if (!empty($fileArray)) {
                        $file_info.= ', '.$fileArray[0].'x'.$fileArray[1];
                    }
                    $productRow['file_info'] = $file_info;
                }
            }

            if (!empty($additiveParams['show_main_image'])) {
                if (!empty($productRow['main_image'])) {
                    $product_main_image = $this->getProduct_ImageDir($id) . $productRow['main_image'];
                    $product_main_image_url = $this->getProduct_ImageImageUrl($id, $productRow['main_image']);
	                $productRow['main_image_url'] = $product_main_image_url;
	                $productRow['main_image_path'] = $product_main_image;
	                if ( !empty($additiveParams['main_image_width']) and !empty($additiveParams['main_image_height']) ) {
		                $filenameInfo =appUtils::getImageShowSize($product_main_image, $additiveParams['main_image_width'], $additiveParams['main_image_height']);
		                if ( !empty($filenameInfo['Width']) and !empty($filenameInfo['Height']) ) {
			                $productRow['main_image_width'] = $filenameInfo['Width'];
			                $productRow['main_image_height'] = $filenameInfo['Height'];
		                }
	                }
                } else {
                    $price_list_default_product_image_path = $this->m_appConfig['document_root'] . $this->m_appConfig['uploads_settings_dir'] . $ci->getSettings('price_list_default_product_image');
                    $default_product_image = $ci->getSettings('price_list_default_product_image');
                    $price_list_default_product_image_url = $this->m_appConfig['base_url'] . '/' . $this->m_appConfig['uploads_settings_dir'] . $ci->getSettings('price_list_default_product_image');

                    $product_main_image = $price_list_default_product_image_path;
                    $product_main_image_url = $price_list_default_product_image_url;
                    $productRow['main_image'] = $default_product_image;
	                $productRow['main_image_url'] = $product_main_image_url;
	                $productRow['main_image_path'] = $product_main_image;
	                $filenameInfo =appUtils::getImageShowSize($product_main_image, $additiveParams['main_image_width'], $additiveParams['main_image_height']);
	                if ( !empty($filenameInfo['Width']) and !empty($filenameInfo['Height']) ) {
		                $productRow['main_image_width'] = $filenameInfo['Width'];
		                $productRow['main_image_height'] = $filenameInfo['Height'];
	                }
                }
            }
            if ( !empty($additiveParams['show_all_images']) ) {
                $product_ImagesList= $ci->mproduct_image->getProduct_ImagesList( false, 0, array('product_id'=> $id,  'show_size_restricted_image' => ( !empty($additiveParams['show_size_restricted_image']) ? $additiveParams['show_size_restricted_image'] : '' ), 'show_default_image'=> ( !empty($additiveParams['show_default_image']) ? $additiveParams['show_default_image'] : '' ) ) );
                $productRow['product_ImagesList']= $product_ImagesList;
            }
            return $ci->makeRetreivingFieldsMapping($productRow, $this->m_fieldsMappingArray);
        }
        return false;
    }




    public function updateProduct(int $id, array $dataArray) : int
    {
        $ci = & get_instance();
        if (empty($id)) {
            $id= 0;
        }
        $funcname= $this->db->dbprefix."update_product";
        $dataArray= $ci->makeSavingFieldsMapping( $dataArray, $this->m_fieldsMappingArray );
        $dataArray['title']= substr($dataArray['title'], 0, 255);
        if ( empty($dataArray['sku']) ) {
            $slug_config = array(
                'field' => 'sku',
                'title' => 'title',
                'table' => $this->m_product_table,
                'id' => 'id',
            );

            $ci->load->library('slug', $slug_config);
            $generated_sku = $ci->slug->create_uri(array('title' => $dataArray['title']), $id, 'sku', 'title');
            $dataArray['sku']= $generated_sku;
        }

        if ( empty($dataArray['slug']) ) {
            $slug_config = array(
                'field' => 'slug',
                'title' => 'title',
                'table' => $this->m_product_table,
                'id' => 'id',
            );

            $ci->load->library('slug', $slug_config);
            $generated_slug = $ci->slug->create_uri(array('title' => $dataArray['title']), $id, 'slug', 'title');
            $dataArray['slug']= $generated_slug;
        }
        $dataArray['slug']= substr($dataArray['slug'], 0, 255);
        $dataArray['sku']= substr($dataArray['sku'], 0, 255);
        $dataArray['short_description']= substr($dataArray['short_description'], 0, 255);
        $user_id= $ci->muser->getLoggedUser(true);
        $attribute_string_array= appUtils::splitStrIntoArray( $dataArray['attribute_list'],',', ':' ,'string_2_array' );
        $updateValuesArray= [
            [ 'field_name'=> 'p_id', 'value'=> $id, 'type'=> 'N', 'is_array'=> false ],
            [ 'field_name'=> 'p_title', 'value'=> appUtils::pgEscape($dataArray['title']), 'type'=> 'S', 'is_array'=> false ],
            [ 'field_name'=> 'p_status', 'value'=> $dataArray['status'], 'type'=> 'S', 'is_array'=> false ],
            [ 'field_name'=> 'p_sku', 'value'=> appUtils::pgEscape($dataArray['sku']), 'type'=> 'S', 'is_array'=> false ],
            [ 'field_name'=> 'p_slug', 'value'=> appUtils::pgEscape($dataArray['slug']), 'type'=> 'S', 'is_array'=> false ],
            [ 'field_name'=> 'p_user_id', 'value'=> $user_id, 'type'=> 'S', 'is_array'=> false ],
            [ 'field_name'=> 'p_regular_price', 'value'=> ( !empty($dataArray['regular_price']) ? $dataArray['regular_price'] : 'null'), 'type'=> 'N', 'is_array'=> false ],
            [ 'field_name'=> 'p_sale_price', 'value'=> $dataArray['sale_price'], 'type'=> 'N', 'is_array'=> false ],
            [ 'field_name'=> 'p_in_stock', 'value'=> $dataArray['in_stock'], 'type'=> 'B', 'is_array'=> false ],
            [ 'field_name'=> 'p_is_homepage', 'value'=> $dataArray['is_homepage'], 'type'=> 'B', 'is_array'=> false ],
            [ 'field_name'=> 'p_is_featured', 'value'=> $dataArray['is_featured'], 'type'=> 'B', 'is_array'=> false ],
            [ 'field_name'=> 'p_short_description', 'value'=> appUtils::pgEscape($dataArray['short_description']), 'type'=> 'S', 'is_array'=> false ],
            [ 'field_name'=> 'p_description', 'value'=> appUtils::pgEscape($dataArray['description']), 'type'=> 'S', 'is_array'=> false ],
            [ 'field_name'=> 'p_has_attributes', 'value'=> $dataArray['has_attributes'], 'type'=> 'B', 'is_array'=> false ],
            [ 'field_name'=> 'p_virtual', 'value'=> $dataArray['virtual'], 'type'=> 'B', 'is_array'=> false ],
	        [ 'field_name'=> 'p_downloadable', 'value'=> $dataArray['downloadable'], 'type'=> 'B', 'is_array'=> false ],
            [ 'field_name'=> 'p_category_list', 'value'=> appUtils::trimRightSubString( $dataArray['category_list'],',' ), 'type'=> 'N', 'is_array'=> true ],
            [ 'field_name'=> 'p_tag_list', 'value'=> appUtils::trimRightSubString( $dataArray['tag_list'],',' ), 'type'=> 'N', 'is_array'=> true ],
            [ 'field_name'=> 'p_attribute_list', 'value'=> $attribute_string_array, 'type'=> 'S', 'is_array'=> false ],
        ];

        if ( !empty($dataArray['created_at']) ) {
            $updateValuesArray[]= [ 'field_name'=> 'p_created_at', 'value'=> $dataArray['created_at'], 'type'=> 'S', 'is_array'=> false ];
        }
        if ( !empty($dataArray['published_at']) ) {
            $updateValuesArray[]= [ 'field_name'=> 'p_published_at', 'value'=> $dataArray['published_at'], 'type'=> 'S', 'is_array'=> false ];
        }

	    $sql_params_str= $ci->updateValuesArrayRoString($updateValuesArray);
        $sql= " select * from ".$funcname."( " . $sql_params_str . " )";
        $func_ret = $ci->dbquery(   $sql   );
	    $product_id = !empty($func_ret[0]['pd_update_product']) ? (int)$func_ret[0]['pd_update_product'] : '';
	    $shipping_class_id= '';
	    if ( !empty($dataArray['Shipping Class']) ) {
		    $dataArray['Shipping Class'];
		    $shipping_class_id= '';
	    }
	    if ( !empty($func_ret[0]['pd_update_product']) and (!empty($shipping_class_id) or !empty($dataArray['weight']) or !empty($dataArray['length']) or !empty( $dataArray['width']) or !empty($dataArray['height'])) ) {

		    $updateValuesArray = [
			    [ 'field_name' => 'p_id', 'value' => $product_id, 'type' => 'N', 'is_array' => false ],
			    [
				    'field_name' => 'p_shipping_class_id',
				    'value'      => ( ! empty( $dataArray['shipping_class_id'] ) ? $dataArray['shipping_class_id'] : 'null' ),
				    'type'       => 'N',
				    'is_array'   => false
			    ],
			    [ 'field_name' => 'p_weight', 'value' => ( ! empty( $dataArray['weight'] ) ? $dataArray['weight'] : 'null' ), 'type' => 'N', 'is_array' => false ],
			    [ 'field_name' => 'p_length', 'value' => ( ! empty( $dataArray['length'] ) ? $dataArray['length'] : 'null' ), 'type' => 'N', 'is_array' => false ],
			    [ 'field_name' => 'p_width', 'value' => ( ! empty( $dataArray['width'] ) ? $dataArray['width'] : 'null' ), 'type' => 'N', 'is_array' => false ],
			    [ 'field_name' => 'p_height', 'value' => ( ! empty( $dataArray['height'] ) ? $dataArray['height'] : 'null' ), 'type' => 'N', 'is_array' => false ],
		    ];
		    if ( ! empty( $dataArray['shipping_created_at'] ) ) {
			    $updateValuesArray[] = [ 'field_name' => 'p_created_at', 'value' => $dataArray['shipping_created_at'], 'type' => 'S', 'is_array' => false ];
		    }

		    if ( ! empty( $dataArray['shipping_updated_at'] ) ) {
			    $updateValuesArray[] = [ 'field_name' => 'p_updated_at', 'value' => $dataArray['shipping_updated_at'], 'type' => 'S', 'is_array' => false ];
		    }

		    $funcname = $this->db->dbprefix . "update_product_shipping";
		    $sql_params_str = $ci->updateValuesArrayRoString( $updateValuesArray );
		    $sql = " select * from " . $funcname . "( " . $sql_params_str . " )";
		    $func_ret = $ci->dbquery( $sql );
		    if ( empty( $func_ret[0][ $funcname ] ) ) {
			    return false;
		    }

		    return (int)$func_ret[0][ $funcname ];
	    }
	    return (int)$product_id;
    }

    public function deleteProduct(int $id) : bool
    {
        if (empty($id)) return false;
        $ci = & get_instance();
        $funcname= $this->db->dbprefix."delete_product";
        $sql= " select * from ".$funcname."( ".$id." )";
        $func_ret = $ci->dbquery(   $sql   );
        if (empty($func_ret[0][$funcname])) return false;
        return $id;
    }

    public function getProduct_ImagesDir() : string
    {
        $ci = & get_instance();
        return $ci->m_appConfig['document_root'] . $ci->m_appConfig['uploads_products_dir'];
    }

    public function getProduct_ImageDir($product_id= '') : string
    {
        $ci = & get_instance();
        return $ci->m_appConfig['document_root'] . $ci->m_appConfig['uploads_products_dir'].'-product-' . $product_id.'/';
    }

    public function getProduct_ImageImageUrl($product_id, $img) : string
    {
        $ci = & get_instance();
        return $ci->m_appConfig['base_url'] .'/'. $ci->m_appConfig['uploads_products_dir'].'-product-' . $product_id.'/' . $img;
    }

    public function getProduct_ImageImagePath($product_id, $img) : string
    {
        $ci = & get_instance();
        return $ci->m_appConfig['document_root'] . $ci->m_appConfig['uploads_products_dir'].'-product-' . $product_id.'/' . $img;
    }

}

?>