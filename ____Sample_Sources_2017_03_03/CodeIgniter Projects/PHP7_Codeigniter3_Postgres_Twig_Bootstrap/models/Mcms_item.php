<?php

class Mcms_item extends CI_Model
{
    private $m_Cms_ItemPage_TypeValueArray = Array('E' => 'Email Template', 'P' => 'Page', 'B' => 'Blog Article');
    private $m_Cms_ItemPublishedValueArray = Array('N' => 'Not Published', 'Y' => 'Published');
    private $m_cms_item_table;
    private $m_users_table;
    private $m_fieldsMappingArray = [
        [ 'field'=>"published", "field_type"=> "bool", "values" => [ ['client_value'=>'Y', 'server_value'=>'TRUE', 'has_single_quote'=>false ],
            [ 'client_value'=>'N', 'server_value'=>'FALSE', 'has_single_quote'=>false ] ] ],
    ];
    public function __construct()
    {
        parent::__construct();
        $ci = & get_instance();
        $this->m_ionAuthConfigTables= $ci->get_ionAuthConfigTables();
        $this->m_cms_item_table= $this->m_ionAuthConfigTables['cms_item'];
        $this->m_users_table= $this->m_ionAuthConfigTables['users'];
    }

    public function getCms_ItemPage_TypeValueArray()
    {
        $ResArray = array();
        foreach ($this->m_Cms_ItemPage_TypeValueArray as $Key => $Value) {
            $ResArray[] = array('key' => $Key, 'value' => $Value);
        }
        return $ResArray;
    }

    public function getPage_TypeLabel($page_type)
    {
        if (!empty($this->m_Cms_ItemPage_TypeValueArray[$page_type])) {
            return $this->m_Cms_ItemPage_TypeValueArray[$page_type];
        }
        return '';
    }

    public function getCms_ItemPublishedValueArray()
    {
        $ResArray = array();
        foreach ($this->m_Cms_ItemPublishedValueArray as $Key => $Value) {
            $ResArray[] = array('key' => $Key, 'value' => $Value);
        }
        return $ResArray;
    }

    public function getCms_Item_PublishedLabel($published)
    {
        if (!empty($this->m_Cms_ItemPublishedValueArray[$published])) {
            return $this->m_Cms_ItemPublishedValueArray[$published];
        }
        return '';
    }

    public function getCms_ItemsSelectionList( array $filtersArray = array(), string $sorting = 'title', string $sort_direction = 'asc') : array
    {
        $cms_ItemsList = $this->mcms_item->getCms_ItemsList(false, 0, $filtersArray, $sorting, $sort_direction);
        $ResArray = array();
        foreach ($cms_ItemsList as $lcms_item) {
            $ResArray[] = array('key' => $lcms_item['id'], 'value' => $lcms_item['title']);
        }
        return $ResArray;
    }

    public function getCms_ItemsList(bool $output_return_count = false, int $page = 0, array $filtersArray = array(), string $sorting = '', string $sort_direction = '')
    {
        if (empty($sorting))
            $sorting = 'title';
        $config_data = $this->config->config;
        $ci = & get_instance();
        $items_per_page= $ci->getSettings('items_per_page', $config_data['default_per_page']);
        $limit = !empty($filtersArray['limit']) ? $filtersArray['limit'] : '';
        $offset = !empty($filtersArray['offset']) ? $filtersArray['offset'] : '';
        $is_page_positive_integer= appUtils::is_positive_integer($page);
        if ( !empty($page) and $is_page_positive_integer ) {
            $limit = '';
            $offset = '';
        }
        if (!empty($config_data) and $is_page_positive_integer) {
            $per_page= ( !empty($filtersArray['per_page']) and appUtils::is_positive_integer($filtersArray['per_page']) ) ? $filtersArray['per_page'] : $items_per_page;
            $limit = $per_page;
            $offset = ($page - 1) * $per_page;
        }

        if (!empty($filtersArray['content'])) {
            $this->db->where(appUtils::strLower($this->m_cms_item_table.'.content',false,false).' like '.appUtils::strLower($filtersArray['content'],true,true),'',false);
        }

        if (!empty($filtersArray['title'])) {
            if ( empty($filtersArray['in_description']) ) {
                $this->db->where(appUtils::strLower($this->m_cms_item_table.'.title',false,false).' like '.appUtils::strLower($filtersArray['title'],true,true),'',false);
            } else {
                $this->db->where( '( '.
                    appUtils::strLower($this->m_cms_item_table.'.title', false, false) . ' like '. appUtils::strLower($filtersArray['title'], true, true) . ' OR ' .
                    appUtils::strLower($this->m_cms_item_table.'.alias', false, false) . ' like '. appUtils::strLower($filtersArray['title'], true, true) .' OR ' .
                    appUtils::strLower($this->m_cms_item_table.'.content', false, false) . ' like '. appUtils::strLower($filtersArray['title'], true, true) .' OR ' .
                    appUtils::strLower($this->m_cms_item_table.'.short_descr', false, false) . ' like '. appUtils::strLower($filtersArray['title'], true, true) .
                    ' )' , '', false );
            }
        }

        if (!empty($filtersArray['page_type'])) {
            if ( is_array($filtersArray['page_type']) ) {
                $this->db->where_in($this->m_cms_item_table . '.page_type', $filtersArray['page_type']);
            } else {
                $this->db->where($this->m_cms_item_table . '.page_type', $filtersArray['page_type']);
            }
        }
        if (!empty($filtersArray['published'])) {
            $this->db->where($this->m_cms_item_table . '.published', $filtersArray['published']);
        }
        if (!empty($filtersArray['created_at_from'])) {
            $this->db->where($this->m_cms_item_table.'.created_at >= ' . "'" . $filtersArray['created_at_from'] . "'");
        }
        if (!empty($filtersArray['created_at_till'])) {
            $this->db->where($this->m_cms_item_table.'.created_at <= ' . "'" . $filtersArray['created_at_till'] . " 23:59:59'");
        }
        if ( !empty($filtersArray['ids']) and is_array($filtersArray['ids']) ) {
            $this->db->where_in($this->m_cms_item_table . '.id', $filtersArray['ids']);
        }

        $additive_fields_for_select= "";
        $fields_for_select= $this->m_cms_item_table.".*";

        if ( ( !empty($limit) and appUtils::is_positive_integer($limit) ) and ( !empty($offset) and appUtils::is_positive_integer($offset) ) ) {
            $this->db->limit($limit, $offset);
        }

        if ( ( !empty($limit) and appUtils::is_positive_integer($limit) ) ) {
            $this->db->limit($limit);
        }


        if ( !empty($filtersArray['show_username'])  ) {
            $this->db->join( $this->m_users_table, $this->m_users_table . '.id = ' . $this->m_cms_item_table . '.user_id', 'left' );
            $additive_fields_for_select= ', '.$this->m_users_table.'.username as username, '.$this->m_users_table.'.active_status as  user_active_status';
        }
        $fields_for_select.= ' ' . $additive_fields_for_select;

        if (!empty($sorting)) {
            $this->db->order_by($sorting, ((strtolower($sort_direction) == 'desc' or strtolower($sort_direction) == 'asc') ? $sort_direction : ''));
        }

        if ($output_return_count) {
            return $this->db->count_all_results($this->m_cms_item_table);
        } else {
            $query = $this->db->from($this->m_cms_item_table);
            if (strlen(trim($fields_for_select)) > 0) {
                $query->select($fields_for_select);
            }
            $ci = & get_instance();
            $retArray= $query->get()->result('array');

            $price_list_default_product_image_path = $this->m_appConfig['document_root'] . $this->m_appConfig['uploads_settings_dir'] . $ci->getSettings('price_list_default_product_image');
            $default_product_image = $ci->getSettings('price_list_default_product_image');
            $price_list_default_product_image_url = $this->m_appConfig['base_url'] . '/' . $this->m_appConfig['uploads_settings_dir'] . $ci->getSettings('price_list_default_product_image');

            $max_width=200;
            $max_height= 300;
            $a= appUtils::getImageShowSize($price_list_default_product_image_path, $max_width, $max_height );
            $default_width= $a['Width'];
            $default_height= $a['Height'];

            foreach( $retArray as $next_key=>$next_Cms_Item ) {
                $retArray[$next_key]= $ci->makeRetreivingFieldsMapping( $next_Cms_Item, $this->m_fieldsMappingArray );
                if ( !empty( $filtersArray['get_file_info'] ) ) {
                    $cms_item_img = $this->getCms_ItemDir($next_Cms_Item['id']) . $next_Cms_Item['img'];
                    $retArray[$next_key]['file_info'] = '';
                    if ( file_exists($cms_item_img) and !is_dir($cms_item_img) ) {
                        $file_info = $next_Cms_Item['img'];
                        $file_info .= ', ' . appUtils::getFileSizeAsString(filesize($cms_item_img));
                        $fileArray = @getimagesize($cms_item_img);
                        if (!empty($fileArray)) {
                            $file_info .= ', ' . $fileArray[0] . 'x' . $fileArray[1];
                        }
                        $retArray[$next_key]['file_info'] = $file_info;
                        $cms_item_img_url = $this->getCms_ItemImageUrl($next_Cms_Item['id'], $next_Cms_Item['img']);
                        $retArray[$next_key]['img_url'] = $cms_item_img_url;
                        $retArray[$next_key]['image'] = $default_product_image;
                        $a= appUtils::getImageShowSize($cms_item_img, $max_width, $max_height );
                        $retArray[$next_key]['width']= $a['Width'];
                        $retArray[$next_key]['height']= $a['Height'];
                    }
                    else {
                        $retArray[$next_key]['img_url'] = $price_list_default_product_image_url;
                        $retArray[$next_key]['width']= $default_width;
                        $retArray[$next_key]['height']= $default_height;
                    }
                }
            }
            return $retArray;
        }
    }

    public function getSimilarByTitle(string $title, int $cms_item_id=0)
    {
        $fields_for_select=$this->m_cms_item_table.".*";
        $this->db->where(appUtils::strLower($this->m_cms_item_table.'.title',false,false).' =  TRIM( ' . appUtils::strLower($title,true,false).' ) ','',false);
        if (!empty($cms_item_id)) {
            $this->db->where($this->m_cms_item_table.'.id != ' . $cms_item_id);
        }
        $query = $this->db->from($this->m_cms_item_table);
        $query->select($fields_for_select);
        $ret= $query->get()->result('array');
        if (empty($ret[0])) return false;
        $ci = & get_instance();
        return $ci->makeRetreivingFieldsMapping( $ret[0], $this->m_fieldsMappingArray );
    }

    public function getSimilarByAlias(string $alias, int $cms_item_id=0)
    {
        $fields_for_select=$this->m_cms_item_table.".*";
        $this->db->where(appUtils::strLower($this->m_cms_item_table.'.alias',false,false).' =  TRIM( ' . appUtils::strLower($alias,true,false).' ) ','',false);
        if (!empty($cms_item_id)) {
            $this->db->where($this->m_cms_item_table.'.id != ' . $cms_item_id);
        }
        $query = $this->db->from($this->m_cms_item_table);
        $query->select($fields_for_select);
        $ret= $query->get()->result('array');
        if (empty($ret[0])) return false;
        $ci = & get_instance();
        return $ci->makeRetreivingFieldsMapping( $ret[0], $this->m_fieldsMappingArray );
    }

    public function getRowById( int $id, array $additiveParams= array() )
    {
        $additive_fields_for_select= '';
        $fields_for_select= $this->m_cms_item_table . '.*';
        if (!empty( $additiveParams['fields_for_select'] )) {
            $fields_for_select= $additiveParams['fields_for_select'];
        }
        if ( !empty($additiveParams['show_username']) ) {
            $this->db->join( $this->m_users_table, $this->m_users_table . '.id = ' . $this->m_cms_item_table . '.user_id', 'left' );
            $additive_fields_for_select= ', '.$this->m_users_table.'.username as username, '.$this->m_users_table.'.active_status as  user_active_status';
        }

        $fields_for_select.= $additive_fields_for_select;
        $this->db->where( $this->m_cms_item_table . '.id', $id);

        $this->db->select( $fields_for_select );

        $ci = & get_instance();
        $query = $this->db->from($this->m_cms_item_table);
        $resultRows = $query->get()->result('array');
        if ( !empty($resultRows[0]) ) {
            $cms_itemRow= $resultRows[0];
            if (!empty($additiveParams['show_file_info']) and !empty($cms_itemRow['img'])) {
                $cms_item_img = $this->getCms_ItemDir($id) . $cms_itemRow['img'];
                $cms_itemRow['file_info'] = '';
                if ( file_exists($cms_item_img) ) {
                    $file_info= $cms_itemRow['img'];
                    $file_info.= ', '.appUtils::getFileSizeAsString( filesize($cms_item_img) );
                    $fileArray = @getimagesize($cms_item_img);
                    if (!empty($fileArray)) {
                        $file_info.= ', '.$fileArray[0].'x'.$fileArray[1];
                    }
                    $cms_itemRow['file_info'] = $file_info;
                }
            }
            if (!empty($additiveParams['show_size_restricted_image'])) {
                if (  !empty($cms_itemRow['img']) and !empty($ci->m_appConfig['admin_images_size_restrictions']) and is_array($ci->m_appConfig['admin_images_size_restrictions'])  ) {
                    $cms_item_img = $this->getCms_ItemDir($id) . $cms_itemRow['img'];
                    $cms_item_img_url = $this->getCms_ItemImageUrl($id, $cms_itemRow['img']);
                    $imageSizesRestrictions= appUtils::getImageRestrictionSizes( $cms_item_img, $ci->m_appConfig['admin_images_size_restrictions'] );
                    $cms_itemRow['imageSizesRestrictions']= $imageSizesRestrictions;
                    $cms_itemRow['img_path']= $cms_item_img;
                    $cms_itemRow['img_url']= $cms_item_img_url;
                }

            }
            return $ci->makeRetreivingFieldsMapping($cms_itemRow, $this->m_fieldsMappingArray);
        }
        return false;
    }

    public function updateCms_Item(int $id, array $dataArray) : int
    {
        $ci = & get_instance();
        if (empty($id)) {
            $id= 0;
        }
        $funcname= $this->db->dbprefix."update_cms_item";
        $dataArray= $ci->makeSavingFieldsMapping( $dataArray, $this->m_fieldsMappingArray );
        if ( empty($dataArray['alias']) ) {
            $slug_config = array(
                'field' => 'alias',
                'title' => 'title',
                'table' => $this->m_cms_item_table,
                'id' => 'id',
            );

            get_instance()->load->library( 'slug', $slug_config );
            $generated_alias = get_instance()->slug->create_uri( array( 'title' => $dataArray['title'] ) );
            $dataArray['alias']= $generated_alias;
        }

        $user_id= $ci->muser->getLoggedUser(true);
        $updateValuesArray= [
            [ 'field_name'=> 'p_id', 'value'=> $id, 'type'=> 'N', 'is_array'=> false ],
            [ 'field_name'=> 'p_title', 'value'=> appUtils::pgEscape($dataArray['title']), 'type'=> 'S', 'is_array'=> false ],
            [ 'field_name'=> 'p_alias', 'value'=> appUtils::pgEscape($dataArray['alias']), 'type'=> 'S', 'is_array'=> false ],
            [ 'field_name'=> 'p_page_type', 'value'=> $dataArray['page_type'], 'type'=> 'S', 'is_array'=> false ],

            [ 'field_name'=> 'p_short_descr', 'value'=> appUtils::pgEscape($dataArray['short_descr']), 'type'=> 'S', 'is_array'=> false ],
            [ 'field_name'=> 'p_content', 'value'=> appUtils::pgEscape($dataArray['content']), 'type'=> 'S', 'is_array'=> false ],
            [ 'field_name'=> 'p_img', 'value'=> appUtils::pgEscape($dataArray['img']), 'type'=> 'S', 'is_array'=> false ],
            [ 'field_name'=> 'p_user_id', 'value'=> $user_id, 'type'=> 'N', 'is_array'=> false ],

            [ 'field_name'=> 'p_published', 'value'=> $dataArray['published'], 'type'=> 'B', 'is_array'=> false ],
        ];

        $sql_params_str= $ci->updateValuesArrayRoString($updateValuesArray);
        $sql= " select * from ".$funcname."( " . $sql_params_str . " )";
        $func_ret = $ci->dbquery(   $sql   );
        if (empty($func_ret[0][$funcname])) return false;
        return $func_ret[0][$funcname];
    }

    public function deleteCms_Item(int $id) : bool
    {
        if (!empty($id)) {
            $ci = &get_instance();
            if (empty($id)) {
                $id = 0;
            }
            $funcname = $this->db->dbprefix . "delete_cms_item";
            $sql = " select * from " . $funcname . "( " . $id . " )";
            $func_ret = $ci->dbquery($sql);
            if (empty($func_ret[0][$funcname])) return false;
            return $func_ret[0] [$funcname];
        }
    }

    /* get Body of content by title of Content*/
    public function getBodyContentByAlias($pAlias, $ConstantsArray = array(), $is_content = true)
    {
        $query = $this->db->get_where('cms_item', array('alias' => $pAlias), 1, 0);
        $ResultRow = $query->result('array');
        if (!empty($ResultRow[0]['content'])) {

            $Body = $is_content ? $ResultRow[0]['content'] : $ResultRow[0]['title'];
            foreach ($ConstantsArray as $Key => $Value) {
                $Pattern = '/\[([\s]*' . $Key . '[\s]*)\]/xsi';
                $Body = preg_replace($Pattern, $Value, $Body);
            }
            return $Body;
        }
        return '';
    }

    public function getCmsItemIdByAlias($pAlias)
    {
        $query = $this->db->get_where('cms_item', array('alias' => $pAlias), 1, 0);
        $ResultRow = $query->result('array');
        if (!empty($ResultRow[0]['id'])) {
            return $ResultRow[0]['id'];
        }
        return '';
    }

    public function getCms_ItemsDir() : string
    {
        $ci = & get_instance();
        return $ci->m_appConfig['document_root'] . $ci->m_appConfig['uploads_cms_items_dir'];
    }

    public function getCms_ItemDir($cms_item_id= '') : string
    {
        $ci = & get_instance();
        return $ci->m_appConfig['document_root'] . $ci->m_appConfig['uploads_cms_items_dir'].'-cms_item-' . $cms_item_id.'/';
    }

    public function getCms_ItemImageUrl($cms_item_id, $img) : string
    {
        $ci = & get_instance();
        return $ci->m_appConfig['base_url'] .'/'. $ci->m_appConfig['uploads_cms_items_dir'].'-cms_item-' . $cms_item_id.'/' . $img;
    }

    public function getCms_ItemImagePath($cms_item_id, $img) : string
    {
        $ci = & get_instance();
        return $ci->m_appConfig['document_root'] . $ci->m_appConfig['uploads_cms_items_dir'].'-cms_item-' . $cms_item_id.'/' . $img;
    }

}