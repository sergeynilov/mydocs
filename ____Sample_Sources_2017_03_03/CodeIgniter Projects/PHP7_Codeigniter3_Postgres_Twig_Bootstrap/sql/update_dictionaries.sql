
CREATE OR REPLACE FUNCTION public.pd_update_settings(p_name character varying, p_value character varying)
 RETURNS integer
 LANGUAGE plpgsql
AS $function$
  DECLARE existing_settings_id int2;

  begin

       SELECT id INTO existing_settings_id FROM pd_settings WHERE lower("name") = lower(p_name) LIMIT 1;

       IF ( existing_settings_id IS NULL ) THEN


          INSERT INTO pd_settings ( "name", "value" ) VALUES(  p_name, p_value );
          existing_settings_id= lastval();

       ELSE

          UPDATE pd_settings  SET "value" = p_value WHERE id = existing_settings_id;

       END IF;


       RETURN existing_settings_id;

  end
$function$



CREATE OR REPLACE FUNCTION public.pd_delete_attribute(p_id integer)
 RETURNS integer
 LANGUAGE plpgsql
AS $function$
  begin

      DELETE FROM pd_attribute_item where attribute_id= p_id;

      DELETE FROM pd_attribute where id= p_id;

      RETURN p_id;

  end
$function$


CREATE OR REPLACE FUNCTION public.pd_update_cms_item(p_id integer, p_title character varying, p_alias character varying, p_page_type type_cms_item_page_type, p_short_descr character varying, p_content text, p_img character varying, p_user_id integer, p_published boolean, p_created_at timestamp without time zone DEFAULT NULL::timestamp without time zone)
 RETURNS integer
 LANGUAGE plpgsql
AS $function$

  begin
    IF p_id <= 0 OR p_id IS NULL THEN

      INSERT INTO pd_cms_item ( title, alias, page_type, short_descr, content, img, user_id, published )
        VALUES( p_title, p_alias, p_page_type, p_short_descr, p_content, p_img, p_user_id, p_published );

      p_id= lastval();

      IF (p_created_at IS NOT NULL ) THEN
        UPDATE pd_cms_item SET created_at= p_created_at
          where id= p_id;
      END IF;

    ELSE

      UPDATE pd_cms_item SET title= p_title, alias= p_alias, page_type= p_page_type, short_descr= p_short_descr, content= p_content, img= p_img, user_id= p_user_id, published= p_published, updated_at= now()
        where id= p_id;

    END IF;

    RETURN p_id;
    
  end
$function$
