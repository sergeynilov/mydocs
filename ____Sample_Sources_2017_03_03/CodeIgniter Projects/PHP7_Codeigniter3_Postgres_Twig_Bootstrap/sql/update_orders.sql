

CREATE OR REPLACE FUNCTION public.pd_update_order(p_id integer, p_user_id integer, p_billing_first_name character varying, p_billing_last_name character varying, p_billing_company character varying, p_billing_phone character varying, p_billing_email character varying, p_billing_country character varying, p_billing_address character varying, p_billing_address2 character varying, p_billing_city character varying, p_billing_state character varying, p_billing_postcode character varying, p_info text, p_total type_money, p_total_items integer, p_payment character varying, p_mode type_order_mode, p_status type_order_status, p_other_shipping boolean, p_shipping_first_name character varying, p_shipping_last_name character varying, p_shipping_company character varying, p_shipping_phone character varying, p_shipping_email character varying, p_shipping_country character varying, p_shipping_address character varying, p_shipping_address2 character varying, p_shipping_city character varying, p_shipping_state character varying, p_shipping_postcode character varying, p_order_items_list jsonb DEFAULT NULL::jsonb, p_ip_address character varying DEFAULT NULL::character varying, p_created_at timestamp without time zone DEFAULT NULL::timestamp without time zone)
 RETURNS integer
 LANGUAGE plpgsql
AS $function$

DECLARE p_order_number character varying;
DECLARE items_count integer;
DECLARE item_data jsonb;
  begin


    IF p_id <= 0 OR p_id IS NULL THEN

      SELECT pd_get_ordernumber(p_user_id) INTO p_order_number;

      INSERT INTO pd_order ( user_id, billing_first_name, billing_last_name, billing_company, billing_phone, billing_email, billing_country,
billing_address, billing_address2, billing_city, billing_state,	billing_postcode, info, total, total_items,	payment, mode, status,	order_number,  other_shipping
      )
        VALUES(  p_user_id, p_billing_first_name, p_billing_last_name, p_billing_company, p_billing_phone, p_billing_email, p_billing_country, p_billing_address, p_billing_address2, p_billing_city, p_billing_state,	 p_billing_postcode, p_info, p_total, p_total_items, p_payment, p_mode, p_status, p_order_number, p_other_shipping );
      p_id= lastval();


      IF (p_created_at IS NOT NULL ) THEN
        UPDATE pd_order SET created_at= p_created_at
          where id= p_id;
      END IF;


      IF ( p_other_shipping ) THEN
          PERFORM pd_update_order_shipping(  p_id:= p_id, p_shipping_first_name:= p_shipping_first_name, p_shipping_last_name:= p_shipping_last_name, p_shipping_company:= p_shipping_company, p_shipping_phone:= p_shipping_phone, p_shipping_email:= p_shipping_email, p_shipping_country:= p_shipping_country, p_shipping_address:= p_shipping_address, p_shipping_address2:= p_shipping_address2, p_shipping_city:= p_shipping_city, p_shipping_state:= p_shipping_state, p_shipping_postcode:= p_shipping_postcode  );
      END IF;


      PERFORM pd_update_order_operation( 0, p_id, p_user_id, 'ORDER_MADE',	NULL, p_status, p_info, '' , p_ip_address );

      items_count=  jsonb_array_length(p_order_items_list);

      items_count = jsonb_array_length(p_order_items_list);
      FOR next_order_item IN 0..(items_count-1) LOOP
           item_data= p_order_items_list->next_order_item;

           PERFORM pd_update_order_item( 0, p_id, ( item_data->>'product_id' )::bigint, (item_data->>'qty' )::type_qty,	( item_data->>'price' )::type_money ,	( item_data->>'back_order' )::boolean );
      END LOOP;

    ELSE

      UPDATE pd_order set user_id= p_user_id, billing_last_name= p_billing_last_name, billing_company= p_billing_company, billing_phone= p_billing_phone, billing_email= p_billing_email, billing_country= p_billing_country, billing_address= p_billing_address, billing_address2= p_billing_address2, billing_city= p_billing_city, billing_postcode= p_billing_postcode, info= p_info, total= p_total, total_items= p_total_items, payment= p_payment, mode= p_mode, status= p_status, other_shipping= p_other_shipping, updated_at= NOW()
        where id= p_id;

      IF ( p_other_shipping ) THEN
         PERFORM pd_update_order_shipping(  p_id:= p_id, p_shipping_first_name:= p_shipping_first_name, p_shipping_last_name:= p_shipping_last_name, p_shipping_company:= p_shipping_company, p_shipping_phone:= p_shipping_phone, p_shipping_email:= p_shipping_email, p_shipping_country:= p_shipping_country, p_shipping_address:= p_shipping_address, p_shipping_address2:= p_shipping_address2, p_shipping_city:= p_shipping_city, p_shipping_state:= p_shipping_state, p_shipping_postcode:= p_shipping_postcode  );

      END IF;

      PERFORM pd_update_order_operation( 0, p_id, p_user_id, 'ORDER_UPDATED',	NULL, p_status, p_info, '' , p_ip_address );

    END IF;

    RETURN p_id;

  end
$function$




CREATE OR REPLACE FUNCTION public.pd_recalc_order_total_sum(p_order_id bigint)
 RETURNS type_money
 LANGUAGE plpgsql
AS $function$

  DECLARE sold_sum type_money;
  DECLARE qty_sum integer;

begin

    SELECT  COALESCE( SUM( oi.qty*oi.price ), 0 ),   COALESCE( SUM( oi.qty ), 0 )  INTO sold_sum, qty_sum
      FROM pd_order_item as oi
      WHERE
          oi.order_id = p_order_id;

    UPDATE pd_order SET total = sold_sum, total_items = qty_sum WHERE id = p_order_id;

    RETURN sold_sum;

 end
$function$



CREATE OR REPLACE FUNCTION public.pd_get_ordernumber(p_user_id integer)
 RETURNS character varying
 LANGUAGE plpgsql
AS $function$
DECLARE ret character varying;
DECLARE next_code character varying;
DECLARE i bigint;
DECLARE order_number_exists integer;
DECLARE ret_non_exists_roder integer;

  begin

    ret= concat( lpad( cast( p_user_id AS character varying ), 8, '0'), to_char(current_timestamp,  'YYYYMMDD') );
    ret_non_exists_roder= 0;
    i = 1;
    order_number_exists= 1;
    WHILE ( i< 9999 AND ret_non_exists_roder = 0 ) LOOP

       next_code= concat( ret, lpad( cast( i AS character varying ), 4, '0') );

       SELECT count("id") INTO order_number_exists FROM pd_order WHERE order_number = next_code LIMIT 1;

       IF order_number_exists = 0 THEN
          ret_non_exists_roder= i;
       END IF;

       i = i+1;

    END LOOP;

    IF ret_non_exists_roder > 0 THEN
      RETURN concat( ret, lpad( cast( ret_non_exists_roder AS character varying ), 4, '0') );
    END IF;

    RETURN '';
  end
$function$



CREATE OR REPLACE FUNCTION public.pd_delete_order(p_id integer)
 RETURNS integer
 LANGUAGE plpgsql
AS $function$
  begin

      DELETE FROM pd_order_item where order_id= p_id;

      DELETE FROM pd_order_operation where order_id= p_id;

      DELETE FROM pd_order_shipping where id= p_id;

      DELETE FROM pd_order where id= p_id;

      RETURN p_id;

  end
$function$


CREATE OR REPLACE FUNCTION public.pd_update_order_shipping(p_id bigint, p_shipping_first_name character varying, p_shipping_last_name character varying, p_shipping_company character varying, p_shipping_phone character varying, p_shipping_email character varying, p_shipping_country character varying, p_shipping_address character varying, p_shipping_address2 character varying, p_shipping_city character varying, p_shipping_state character varying, p_shipping_postcode character varying, p_created_at timestamp without time zone DEFAULT NULL::timestamp without time zone)
 RETURNS integer
 LANGUAGE plpgsql
AS $function$

  DECLARE update_order_shipping_id bigint;
  begin

      SELECT id FROM pd_order_shipping INTO update_order_shipping_id WHERE id = p_id;

      IF update_order_shipping_id IS NULL THEN

        INSERT INTO pd_order_shipping ( id, shipping_first_name,	shipping_last_name,	shipping_company, shipping_phone, shipping_email, shipping_country, shipping_address, shipping_address2, shipping_city, shipping_state, shipping_postcode )
          VALUES( p_id, p_shipping_first_name, p_shipping_last_name, p_shipping_company,	p_shipping_phone, p_shipping_email, p_shipping_country, p_shipping_address, p_shipping_address2, p_shipping_city, p_shipping_state, p_shipping_postcode  );

        p_id= lastval();

        IF (p_created_at IS NOT NULL ) THEN
          UPDATE pd_order_shipping SET created_at= p_created_at
            where id= p_id;
        END IF;

      ELSE

        UPDATE pd_order_shipping set shipping_first_name= p_shipping_first_name, shipping_last_name= p_shipping_last_name, shipping_company= p_shipping_company, shipping_phone= p_shipping_phone, shipping_email= p_shipping_email, shipping_country= p_shipping_country, shipping_address= p_shipping_address, shipping_address2= p_shipping_address2, shipping_city= p_shipping_city, shipping_state= p_shipping_state, shipping_postcode= p_shipping_postcode,  updated_at= NOW()
          where id= p_id;

    END IF;

    RETURN p_id;

  end
$function$
