CREATE OR REPLACE FUNCTION public.pd_update_product(p_id integer, p_title character varying, p_status type_productstatus, p_slug character varying, p_sku character varying, p_user_id integer, p_regular_price type_money, p_sale_price type_money, p_in_stock boolean, p_is_homepage boolean, p_is_featured boolean, p_short_description character varying, p_description text, p_has_attributes boolean, p_downloadable boolean, p_virtual boolean, p_category_list integer[] DEFAULT NULL::integer[], p_tag_list integer[] DEFAULT NULL::integer[], p_attribute_list jsonb DEFAULT NULL::jsonb, p_published_at timestamp without time zone DEFAULT NULL::timestamp without time zone, p_created_at timestamp without time zone DEFAULT NULL::timestamp without time zone)
 RETURNS integer
 LANGUAGE plpgsql
AS $function$

DECLARE product_published_at timestamp;

  begin

    product_published_at= NULL;
    IF p_status = 'A' THEN  -- Set published_at only for active Product ( status == 'A' )
      IF p_published_at IS NULL THEN
        product_published_at= now();
      ELSE
        product_published_at= p_published_at;
      END IF;
    END IF;

    IF p_id <= 0 OR p_id IS NULL THEN

      INSERT INTO pd_product ( title, status, slug, sku, user_id, regular_price, sale_price, in_stock, is_homepage, is_featured, short_description, description, has_attributes, downloadable, virtual, published_at )
        VALUES(  p_title, p_status, p_slug, p_sku, p_user_id, p_regular_price, p_sale_price, p_in_stock, p_is_homepage, p_is_featured, p_short_description, p_description, p_has_attributes, p_downloadable, p_virtual, product_published_at );
      p_id= lastval();

      IF (p_created_at IS NOT NULL ) THEN
        UPDATE pd_product SET created_at= p_created_at
          where id= p_id;
      END IF;

    ELSE

      UPDATE pd_product set title= p_title, status= p_status, slug= p_slug, sku= p_sku, user_id= p_user_id, regular_price= p_regular_price, sale_price= p_sale_price, in_stock= p_in_stock, is_homepage= p_is_homepage, is_featured= p_is_featured, short_description= p_short_description, description= p_description, has_attributes= p_has_attributes, downloadable= p_downloadable, virtual= p_virtual, updated_at= now(), published_at= product_published_at
        where id= p_id;

        IF p_downloadable = false THEN
          DELETE FROM pd_product_downloadable where product_id= p_id;
        END IF;

        IF p_has_attributes = false THEN
          DELETE FROM pd_product_attribute where product_id= p_id;
        END IF;

    END IF;

    PERFORM pd_update_product_category(p_id, p_category_list);

    PERFORM pd_update_product_tag(p_id, p_tag_list);

    PERFORM pd_update_product_attribute(p_id, p_attribute_list);

    RETURN p_id;

  end
$function$



CREATE OR REPLACE FUNCTION public.pd_delete_product(p_id integer)
 RETURNS integer
 LANGUAGE plpgsql
AS $function$
  begin

      DELETE FROM pd_product_attribute where product_id= p_id;

      DELETE FROM pd_product_category where product_id= p_id;

      DELETE FROM pd_product_comment where product_id= p_id;

      DELETE FROM pd_product_downloadable where product_id= p_id;

      DELETE FROM pd_product_image where product_id= p_id;

      DELETE FROM pd_product_tag where product_id= p_id;

      DELETE FROM pd_product_affiliated_user where product_id= p_id;

      DELETE FROM pd_product_shipping where product_id= p_id;

      DELETE FROM pd_product_filter where product_id= p_id;

      DELETE FROM pd_product where id= p_id;

      RETURN p_id;

  end
$function$
