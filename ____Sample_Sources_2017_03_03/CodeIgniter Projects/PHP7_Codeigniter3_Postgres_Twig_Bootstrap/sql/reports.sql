
CREATE OR REPLACE FUNCTION public.pd_get_most_rating_categories(p_limit integer)
 RETURNS TABLE(category_name character varying, category_slug character varying, category_id smallint, products_count bigint, rating numeric)
 LANGUAGE sql
AS $function$

SELECT c.name as category_name, c.slug as category_slug, c.id as category_id,

  count(p.id) as products_count,

  CAST( avg(  p.rating_summary / NULLIF(p.rating_count,0)  )  AS decimal(5,2) ) as rating

  from pd_product as p join pd_product_category as pc on pc.product_id = p.id join pd_category as c on c.id = pc.category_id

  group by c.id

  order by rating desc, products_count asc, c.name asc

  LIMIT p_limit ;

$function$




CREATE OR REPLACE FUNCTION public.pd_report_orders_sum_by_categories(p_category_list integer[], p_last_operation_date_from timestamp without time zone, p_last_operation_date_till timestamp without time zone, p_status type_order_status, p_sort_categories character varying DEFAULT 'by_sum_asc'::character varying, p_limit integer DEFAULT NULL::integer)
 RETURNS TABLE(category_name character varying, category_slug character varying, category_id smallint, sold_sum numeric, qty_sum bigint)
 LANGUAGE sql
AS $function$

SELECT c.name as category_name,

   c.slug as category_slug,

   pc.category_id,

   sum( oi.qty*oi.price ) AS sold_sum,

   sum( oi.qty ) AS qty_sum

  from pd_order_item as oi join

      pd_product_category as pc on pc.product_id = oi.product_id join

      pd_order as o on o.id = oi.order_id join

      pd_category as c on c.id = pc.category_id

  WHERE ( CASE when p_category_list IS NOT NULL THEN pc.category_id = ANY (p_category_list) else true END ) AND 
       
       ( o.last_operation_date BETWEEN coalesce(p_last_operation_date_from,pd_f_min_timestamp()) AND coalesce(p_last_operation_date_till,pd_f_max_timestamp()) ) AND

       ( CASE when p_status IS NOT NULL THEN o.status = p_status else true END )

  group by pc.category_id, category_name, category_slug

  ORDER BY

    	CASE WHEN p_sort_categories = 'by_sum_asc' THEN

		    sum( oi.qty*oi.price )

        end ASC,

    	CASE WHEN p_sort_categories = 'by_sum_desc' THEN

		    sum( oi.qty*oi.price )

        end DESC,

    	CASE WHEN p_sort_categories = 'by_sum_category_name' THEN

		    c.name

		end ASC

  LIMIT p_limit ;

$function$



CREATE OR REPLACE FUNCTION public.pd_get_product_prices(p_limit integer, p_offset integer, p_sort character varying DEFAULT 'rating'::character varying, p_sort_direction character varying DEFAULT 'asc'::character varying, p_title character varying DEFAULT NULL::character varying, p_in_description boolean DEFAULT NULL::boolean, p_status type_productstatus DEFAULT NULL::type_productstatus, p_in_stock boolean DEFAULT NULL::boolean, p_downloadable boolean DEFAULT NULL::boolean, p_virtual boolean DEFAULT NULL::boolean, p_sku character varying DEFAULT NULL::character varying, p_sale_price_from type_money DEFAULT NULL::numeric, p_sale_price_till type_money DEFAULT NULL::numeric, p_rating_from integer DEFAULT NULL::integer, p_rating_till integer DEFAULT NULL::integer, p_category_list integer[] DEFAULT NULL::integer[])
 RETURNS TABLE(id bigint, title character varying, status type_productstatus, slug character varying, sku character varying, user_id integer, regular_price type_money, sale_price type_money, in_stock boolean, short_description character varying, downloadable boolean, virtual boolean, rating_count integer, rating_summary integer, rating integer, published_at timestamp without time zone, created_at timestamp without time zone, main_image character varying, product_categories character varying[], product_categories_slug character varying[], product_tags character varying[], product_tags_id smallint[], bookmarks_count bigint, product_attributes jsonb)
 LANGUAGE sql
AS $function$

select * from ( SELECT p.id, p.title, p.status, p.slug, p.sku, p.user_id, p.regular_price, p.sale_price, p.in_stock, p.short_description, p.downloadable, p.virtual, p.rating_count, p.rating_summary,

COALESCE( CAST( floor( p.rating_summary / NULLIF(p.rating_count,0)  ) AS INTEGER ), 0 ) as rating,

p.published_at, p.created_at,

(select pi.image from pd_product_image as pi where pi.product_id = p.id and pi.is_main = true ) as main_image,

( select array_agg(c.name) from pd_category as c, pd_product_category as pd where c.id= pd.category_id and pd.product_id = p.id) as product_categories,

( select array_agg(c.slug) from pd_category as c, pd_product_category as pd where c.id= pd.category_id and pd.product_id = p.id) as product_categories_slug,

( select array_agg(t.name) from pd_tag as t, pd_product_tag as pt where t.id= pt.tag_id and pt.product_id = p.id) as product_tags,

( select array_agg(t.id) from pd_tag as t, pd_product_tag as pt where t.id= pt.tag_id and pt.product_id = p.id) as product_tags_id,

( select count(*) from pd_product_bookmark where pd_product_bookmark.product_id = p.id and pd_product_bookmark.user_id = p.user_id ) as bookmarks_count,

( select pa.attributes_data from pd_product_attribute as pa where pa.product_id = p.id) as product_attributes

FROM pd_product AS p LEFT JOIN  pd_product_category AS pc ON pc.product_id = p.id

      WHERE

           ( CASE when p_status IS NOT NULL THEN p.status = p_status else true END ) AND

           ( CASE when p_in_stock IS NOT NULL THEN p.in_stock = p_in_stock else true END ) AND

           ( CASE when p_downloadable IS NOT NULL THEN p.downloadable = p_downloadable else true END ) AND

           ( CASE when p_virtual IS NOT NULL THEN p.virtual = p_virtual else true END ) AND

           ( CASE when p_sku IS NOT NULL THEN LOWER(p.sku) like LOWER(p_sku) else true END ) AND


           (
             p_title IS NULL OR LOWER(p.title) LIKE LOWER(p_title) OR (
               p_in_description AND (  LOWER(p.description) LIKE LOWER(p_title) OR LOWER(p.short_description) LIKE LOWER(p_title) )
             )
           )  AND


           ( p.sale_price BETWEEN coalesce(p_sale_price_from, pd_f_min_numeric() ) AND coalesce(p_sale_price_till, pd_f_max_numeric() )
 )  AND

           ( CASE when p_category_list IS NOT NULL THEN pc.category_id = ANY (p_category_list) else true END )

      GROUP BY p.id

) as rows

      WHERE
       ( CASE when rows.rating IS NOT NULL THEN   rows.rating BETWEEN coalesce(p_rating_from, 0) AND coalesce( p_rating_till, 5 )  else true END  )


      ORDER BY

      CASE WHEN p_sort_direction = 'asc' THEN

          CASE p_sort

              -- sort by numeric fields

              WHEN 'rating' THEN rating

              WHEN 'sale_price' THEN sale_price

              ELSE NULL

          END

      ELSE

          NULL

      END

      ASC,

      CASE WHEN p_sort_direction = 'desc' THEN

          CASE p_sort -- sort by numeric fields

              WHEN 'rating' THEN rating

              WHEN 'sale_price' THEN sale_price

              ELSE NULL

          END

      ELSE

          NULL

      END

      DESC,

      CASE WHEN p_sort_direction = 'asc' THEN

          CASE p_sort   -- sort by string fields

              WHEN 'title' THEN title

              WHEN 'status' THEN CAST(status AS character varying )

              WHEN 'downloadable' THEN CAST(downloadable AS character varying )

              WHEN 'virtual' THEN CAST(virtual AS character varying )

              WHEN 'in_stock' THEN CAST(in_stock AS character varying )

              ELSE NULL

          END

      ELSE

          NULL

      END

      ASC,

      CASE WHEN p_sort_direction = 'desc' THEN

          CASE p_sort  -- sort by string fields

              WHEN 'title' THEN title

              WHEN 'status' THEN CAST(status AS character varying )

              WHEN 'downloadable' THEN CAST(downloadable AS character varying )

              WHEN 'virtual' THEN CAST(virtual AS character varying )

              WHEN 'in_stock' THEN CAST(in_stock AS character varying )

              ELSE NULL

          END

      ELSE

          NULL

      END

      DESC,

      CASE WHEN p_sort_direction = 'asc' THEN

          CASE p_sort

              -- sort by timestamp fields

              WHEN 'published_at' THEN published_at

              WHEN 'created_at' THEN created_at

              ELSE NULL

          END

      ELSE

          NULL

      END

      ASC,

      CASE WHEN p_sort_direction = 'desc' THEN

          CASE p_sort -- sort by timestamp fields

              WHEN 'published_at' THEN published_at

              WHEN 'created_at' THEN created_at

              ELSE NULL

          END

      ELSE

          NULL

      END

      DESC

      LIMIT p_limit  OFFSET p_offset;

$function$


CREATE OR REPLACE FUNCTION public.pd_get_product_prices_rows_count(p_title character varying DEFAULT NULL::character varying, p_in_description boolean DEFAULT NULL::boolean, p_status type_productstatus DEFAULT NULL::type_productstatus, p_in_stock boolean DEFAULT NULL::boolean, p_downloadable boolean DEFAULT NULL::boolean, p_virtual boolean DEFAULT NULL::boolean, p_sku character varying DEFAULT NULL::character varying, p_sale_price_from type_money DEFAULT NULL::numeric, p_sale_price_till type_money DEFAULT NULL::numeric, p_rating_from integer DEFAULT NULL::integer, p_rating_till integer DEFAULT NULL::integer, p_category_list integer[] DEFAULT NULL::integer[])
 RETURNS bigint
 LANGUAGE sql
AS $function$

select count(*) AS rows_count from ( SELECT CAST( floor( p.rating_summary / NULLIF(p.rating_count,0)  ) AS INTEGER ) as rating

FROM pd_product AS p LEFT JOIN  pd_product_category AS pc ON pc.product_id = p.id

      WHERE

           ( CASE when p_status IS NOT NULL THEN p.status = p_status else true END ) AND

           ( CASE when p_in_stock IS NOT NULL THEN p.in_stock = p_in_stock else true END ) AND

           ( CASE when p_downloadable IS NOT NULL THEN p.downloadable = p_downloadable else true END ) AND


           ( CASE when p_virtual IS NOT NULL THEN p.virtual = p_virtual else true END ) AND

           ( CASE when p_sku IS NOT NULL THEN LOWER(p.sku) like LOWER(p_sku) else true END ) AND

           (
              p_title IS NULL OR LOWER(p.title) LIKE LOWER(p_title) OR (
                p_in_description AND (  LOWER(p.description) LIKE LOWER(p_title) OR LOWER(p.short_description) LIKE LOWER(p_title) )
              )
           )  AND

           ( p.sale_price BETWEEN coalesce(p_sale_price_from, pd_f_min_numeric() ) AND coalesce(p_sale_price_till,pd_f_max_numeric() )
 )  AND

           ( CASE when p_category_list IS NOT NULL THEN pc.category_id = ANY (p_category_list) else true END )


      GROUP BY p.id

) as rows

      WHERE 
       ( CASE when rows.rating IS NOT NULL THEN   rows.rating BETWEEN coalesce(p_rating_from, 1) AND coalesce( p_rating_till, 5 )  else true END  );

$function$
