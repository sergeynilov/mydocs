<?php
include_once ( APPPATH . "/third_party/phpass-0.3/PasswordHash.php" );
class Admin extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('muser', '', true);
		...
	}




	public function run_import()
	{
		if ( !$this->ion_auth->logged_in() or !$this->m_has_settings_editor_access or empty($this->session->userdata['is_logged']) ) {
			redirect(base_url() . 'admin/admin/msg/type/warning/icon/exclamation-sign/msg/' . urlencode ( 'You have no access to this page !').'/action/show-login' );
		}

		$this->load->database();
		$this->load->model('muser');
		$this->load->model('muser_bank_account');
		$this->load->model('mproduct_affiliated_user');
		$this->load->model('mattribute');
		$this->load->model('mcategory');
		$this->load->model('mshipping_class');
		$this->load->model('mtag');
		$this->load->model('mattribute_item');
		$this->load->model('mproduct');
		$this->load->model('mproduct_image');
		$this->load->model('mproduct_comment');
		$this->load->model('mproduct_downloadable');
		$this->load->model('mproduct_attribute');
		$this->load->model('mproduct_tag');
		$this->load->model('mtemplate');
		$this->load->model('msettings');

		$this->load->library('Wprods_reader');
		$src_filename= '/_wwwroot/wprods/FOR_IMPORT/wooexim_export_2017_02_20_15_12_50.csv';
		$src_dir= '/_wwwroot/wprods/FOR_IMPORT/';
		$this->wprods_reader->setImportComments(true);
		$this->wprods_reader->setDebuggingData(true);
		$this->wprods_reader->setNumberOfCopies(1); // with setDebuggingData = true makes inserting of products in circle for many rows testing
		$this->wprods_reader->setItemsSplitter('|');
		$this->wprods_reader->run($src_filename, $src_dir, $this->m_appConfig);
		$results_text= $this->wprods_reader->showResultsText();
		echo $results_text;
	}

}


class Wprods_reader {
	private $m_source_file_to_upload;
...
	private $m_users_added_count;
	private $m_products_added_count;
	private $m_products_skipped_as_existing_count;
	private $m_products_skipped_as_wrong_format_count;
	private $m_attributes_added_count;
	private $m_attribute_items_added_count;
	private $m_categories_added_count;
	private $m_categories_adding_error_count;
	private $m_shipping_classes_added_count;
	private $m_shipping_classes_adding_error_count;
	private $m_tags_added_count;
	private $m_tags_adding_error_count= 0;
	private $m_attributes_notfound_error_count;
	private $m_product_images_added_count;
	private $m_product_images_adding_skipped_as_existing_count;
	private $m_product_images_adding_error_count;
	public function __construct()
	{
		$this->m_ci = &get_instance();

		if ( !$this->m_ci->ion_auth->logged_in() or !$this->m_ci->ion_auth->in_group( array( SUPERADMIN_GROUP_LABEL, ADMIN_GROUP_LABEL ) ) or empty($this->m_ci->session->userdata['is_logged'] ) ) {
			die("Invalid access");
		}

		$this->m_config_object = $this->m_ci->config;

		$this->m_is_homepage_rows_inserted= 0;
		$this->m_products_added_count= 0;
		$this->m_products_skipped_as_existing_count= 0;
		$this->m_products_skipped_as_wrong_format_count= 0;

		$this->m_categories_added_count= 0;
		$this->m_categories_adding_error_count= 0;
		$this->m_shipping_classes_added_count= 0;
		$this->m_shipping_classes_adding_error_count= 0;
		$this->m_tags_added_count= 0;
		$this->m_tags_adding_error_count= 0;
		$this->m_categories_adding_error_count= 0;
		$this->m_attributes_notfound_error_count= 0;
		$this->m_product_images_added_count= 0;
		$this->m_product_images_adding_skipped_as_existing_count= 0;
		$this->m_product_images_adding_error_count= 0;
		$LoggedUserData = $this->m_ci->muser->getLoggedUserData();
		if (empty($LoggedUserData['logged_user_id'])) {
			die("Must log into the system!");
		}
		$this->m_logged_user_id = $LoggedUserData['logged_user_id'];
		$this->m_userRoleValueArray = Array( // list of all user group in system
			$this->m_ci->muser->getGroupIdByname(SUPERADMIN_GROUP_LABEL),
			$this->m_ci->muser->getGroupIdByname(ADMIN_GROUP_LABEL),
			$this->m_ci->muser->getGroupIdByname(EDITOR_GROUP_LABEL),
			$this->m_ci->muser->getGroupIdByname(AUTHOR_GROUP_LABEL),
			$this->m_ci->muser->getGroupIdByname(CONTRIBUTOR_GROUP_LABEL),
			$this->m_ci->muser->getGroupIdByname(AFFILIATED_GROUP_LABEL),
			$this->m_ci->muser->getGroupIdByname(SUBSCRIBER_GROUP_LABEL),
			$this->m_ci->muser->getGroupIdByname(MEMBERS_GROUP_LABEL),
		);
	}
	public function run($p_file_to_upload, $p_source_dir, $p_m_appConfig)
	{
		$this->create_testing_users_text= $this->create_testing_users();
		$this->m_source_file_to_upload= $p_file_to_upload;
		$this->m_source_dir= $p_source_dir;
		$this->m_appConfig= $p_m_appConfig;
		$ci =& get_instance();
		$ci->load->library('CSVReader');
		$this->insertDefaultSettings();
		$productsList = $ci->csvreader->parse_file($this->m_source_file_to_upload); //path to csv file

		$row_index= 0;
		$this->m_products_added_count= 0;
		$this->m_users_added_count= 0;
		$this->m_attributes_added_count= 0;
		$this->m_attribute_items_added_count= 0;
		...
				$product_data = true;
				if ($nextProduct['Name'] == 'shipping_class_list') {
					$shippingClassData = unserialize($nextProduct['Description']);
					if ($shippingClassData) {
						$this->fillNextShippingClassData($shippingClassData, $row_index);
					}
					$product_data = false;
				}

				if ($nextProduct['Name'] == 'attributes_list') {
					$nextAttributesData = unserialize($nextProduct['Description']);
					if ($nextAttributesData) {
						$this->fillNextAttributesData($nextAttributesData, $row_index);
					}
					$product_data = false;
				}

				if ($nextProduct['Name'] == 'users_list') {
					$next_users_data = unserialize($nextProduct['Description']);
					if ($next_users_data) {
						$this->fillNextUsersData($next_users_data, $row_index);
					}
					$product_data = false;
				}

				if ($product_data) {
					$this->m_current_product_id = '';
					$this->fillNextProductsData($nextProduct, $row_index, $number_copy);
				}
		...
		if (!$this->m_debugging) {
			if ($this->m_ci->db->trans_status() === FALSE) {
				$this->m_ci->db->trans_rollback();
			} else {
				$this->m_ci->db->trans_commit();
			}
		}
	}

	private function fillNextProductsData($Product, $row_index, $number_copy)
	{
		...
		if ( !empty($Product['Categories']) ) { // DONE
			$categories_ids_list= $this->getCategoriesIdsList($Product['Categories'], 'string');
		}
		...

		if (empty($Product['SKU'])) {
			$this->m_products_skipped_as_wrong_format_count++;
			$this->m_current_product_id= '';
			return false;

		}
		$similarProduct = $this->m_ci->mproduct->getSimilarBySku($Product['SKU'],  0);

		if (!empty($similarProduct)) {
			$this->m_products_skipped_as_existing_count++;
			$this->m_current_product_id= $similarProduct['id'];
			return false;
		}

		$is_homepage= 'N';
		$is_featured= 'N';
		$status= $this->getProductStatusValue($Product['Product Status']);
		echo '<pre>Add Product $status::'.print_r($status,true).'</pre>';
		$updateDataArray = array( 'title' => $Product['Name'], 'status' => $status, 'sku' => $Product['SKU'],'slug' => $Product['SKU'], 'user_id' => $this->m_logged_user_id, 'regular_price' => ( !empty($Product['Regular Price']) ? $Product['Regular Price'] : 0 ), 'sale_price' => ( !empty($Product['Price']) ? $Product['Price'] : 0 ), 'in_stock' => $this->get_product_stock_status_value($Product['Stock Status']), 'short_description' => $Product['Purchase Note'], 'description' => $Product['Description'], 'has_attributes' => ( strlen($product_attributes_ids_list) > 0 ? "Y" : "N" ), 'downloadable' => $this->get_product_downloadable_value($Product['Downloadable']), 'virtual' => $this->getProductVirtualValue($Product['Virtual']), 'category_list' => $categories_ids_list, 'tag_list'=> $tags_ids_list, 'attribute_list'=> $product_attributes_ids_list, 'created_at'=> $Product['post_date'], 'is_homepage'=> $is_homepage, 'is_featured'=> $is_featured );  // similar Product(by sku) not found we need to create it!
		if ( $this->getProductStatusValue( $Product['Product Status']) == 'A' and !empty($Product['post_date']) ) {
			$updateDataArray['published_at']= $Product['post_date'];
		}

		if ($this->m_insert_debugging_data ) { // for debugging purpose set datas withing -1 year - + 1 year
			$generated_date= $this->generateDate();
			$updateDataArray['published_at'] = $generated_date;
			$updateDataArray['created_at'] = $generated_date;
		}


		if (  (!empty($Product['Shipping Class']) or !empty($Product['Weight'])  or  !empty($Product['Length'] or !empty( $Product['Width']) or !empty($Product['Height'])) )  ) { // shipping info if defined
			$similarShippingClass= $this->m_ci->mshipping_class->getSimilarByName($Product['Shipping Class']);
			if (!empty($similarShippingClass)) {
				$updateDataArray['shipping_class_id']= $similarShippingClass['id'];
				$updateDataArray['weight']= $Product['Weight'];
				$updateDataArray['length']= $Product['Length'];
				$updateDataArray['width']= $Product['Width'];
				$updateDataArray['height']= $Product['Height'];
			}
		}

		$product_id = $this->m_ci->mproduct->updateProduct(0, $updateDataArray);
		if ($product_id) {
			$this->m_products_added_count++;
			$this->m_current_product_id= $product_id;
		}
		if (in_array($product_id,[1,7,3,8])) {
			$r_user_id= rand(1,3);
			$percent= rand(500,2500)/1000;
			$product_affiliated_user_id= $this->m_ci->mproduct_affiliated_user->updateProduct_AffiliatedUser(0, [	'product_id'=> $product_id,	'user_id'=> $r_user_id,'status'=>'A',  'percent'=> $percent,  'status'=> 'A', 'info'=> 'product affiliated user info' ] );
		}


		if ( !empty($Product['result_comments_list']) and $this->m_import_comments ) {
			$result_comments_list= @unserialize($Product['result_comments_list']);
			if ($result_comments_list and is_array($result_comments_list) and count($result_comments_list) > 0 ) {
				$product_comments_ret = $this->addProductComments($result_comments_list, 0, 0, $Product['Name']);
			}
		}
		$this->m_ci->mproduct_comment->productRatingRecalc( $this->m_current_product_id ); // run on server function for setting of calculated fields
		return $product_id;
	}


	private function get_product_stock_status_value($stock_status)
	{
		if ( strtolower($stock_status) == 'instock' ) return 'Y';
		return 'N';
	}

	private function get_product_downloadable_value($downloadable)
	{
		if ( strtolower($downloadable) == 'yes' ) return 'Y';
		return 'N';
	}

	private function getProductVirtualValue($virtual)
	{
		if ( strtolower($virtual) == 'yes' ) return 'Y';
		return 'N';
	}

	private function getProductStatusValue($status) {
		if ( strtolower($status) == 'publish') return 'A';
		if ( strtolower($status) == 'future') return 'I';
		if ( strtolower($status) == 'draft') return 'D';
		if ( strtolower($status) == 'pending') return 'P';
		if ( strtolower($status) == 'private') return 'I';
		if ( strtolower($status) == 'trash') return 'I';
		if ( strtolower($status) == 'auto-draft') return 'D';
		if ( strtolower($status) == 'inherit') return 'I';
	}

	private function fillNextUsersData($users_data, $row_index)
	{
		foreach ($users_data as $next_key => $next_user) {
			$similarUser = $this->m_ci->muser->getSimilarUsername($next_user['user_login'], 0);
			if ( empty($similarUser) ) { // similar user not found we need to create it!
				...
				$a= appUtils::pregSplit( "/\|/", $next_user['roles'] );
				$user_group= EDITOR_GROUP_LABEL;
				if (!empty($a[0])) {
					$user_group = $a[0];
				}
				$user_group= $this->checkValidUserRole( $user_group );

				$additional_data = array('active_status' => $active_status, 'first_name' => $first_name, 'last_name' => $last_name, 'company' => '', 'phone' => '',
				                         'user_url'=> $next_user['user_url'], 'created_at'=>$next_user['user_registered'],
				                         'backend_template_id'=> (!empty($backendDefaultTemplate[0]['id']) ? $backendDefaultTemplate[0]['id'] : ''),
				                         'frontend_template_id'=> ( !empty($frontendDefaultTemplate[0]['id']) ? $frontendDefaultTemplate[0]['id'] : '' ) );

				$user_id = $this->m_ci->ion_auth->register( $next_user['user_login'], $next_user['user_pass'], $next_user['user_email'], $additional_data,   array( $this->m_ci->muser->getUserRoleKeyBylabel( $user_group ) ) );
				$this->m_ci->muser->updateUser( $user_id, array('active_status'=> $active_status, 'password'=> $next_user['user_pass'], 'hasher_login'=> 'Y' ) );

				if ($user_id) {
					$this->m_users_added_count++;
				}
			}

		}

	}


	private function fillNextAttributesData($attributes_data, $row_index, $output= 'array'){
		foreach( $attributes_data as $next_key=>$next_attribute ) {
			$similarAttribute= $this->m_ci->mattribute->getSimilarByName( $next_attribute['attribute_name'], 0 );
			if ( empty($similarAttribute) ) { // similar Attribute not found we need to create it!

				$updateDataArray= array( 'name' => $next_attribute['attribute_name'], 'label' => $next_attribute['attribute_label'], 'type' => ( strtolower($next_attribute['attribute_type']) == 'select' ? 'S' : 'I' ), 'ordering' => $attribute_row_index );
				$attribute_id = $this->m_ci->mattribute->updateAttribute(0, $updateDataArray );
				if ($attribute_id) {
					...
					$this->m_attributes_added_count++;
					if (!empty($next_attribute['attrs']) and is_array($next_attribute['attrs'])) {
						foreach( $next_attribute['attrs'] as $next_attribute_item ) {
							$similarAttributeItem= $this->m_ci->mattribute_item->getSimilarByItemValue( $next_attribute_item['slug'], $attribute_id, 0 );
							if ( empty($similarAttributeItem) ) { // similar Attribute Item not found we need to create it!
								$attribute_item_id = $this->m_ci->mattribute_item->updateAttribute_Item( 0, array( 'attribute_id'=> $attribute_id, 'item_value'=> $next_attribute_item['slug'], 'item_label'=> $next_attribute_item['name'] ) );
								if ( $attribute_item_id > 0 ) {
									$this->m_attribute_items_added_count++;
									...
								}
							}
						}

					}
				}

			}
			$attribute_row_index++;
		}
	}

	private function addProductComments( $product_comments_list, $source_parent_comment_id, $dest_parent_comment_id, $product_name, $level_count= 0 )
	{
		if ( empty($this->m_current_product_id) ) return false;
		...
		foreach( $product_comments_list as $next_key=>$nextProduct_comment ) {
			if ( (int)$source_parent_comment_id != (int)$nextProduct_comment['comment_parent'] ) continue;// to insert only comments of $source_parent_comment_id

			$this->comment_user_id= $this->getUserIdByData( $nextProduct_comment['comment_author'],  $nextProduct_comment['comment_author_email'],  $nextProduct_comment['comment_author_IP'],  $nextProduct_comment['comment_author_url'] );
			$approved= ( $nextProduct_comment['comment_approved']?"A":"N" );
			$is_inappropriate= 'N';
			$rating= $nextProduct_comment['rating'];

			if (empty($dest_parent_comment_id) or $dest_parent_comment_id< 0) $dest_parent_comment_id= null;
			$productComment= array( 'product_id'=> $this->m_current_product_id, 'parent_product_comment_id'=> $dest_parent_comment_id, 'comment_text'=> $nextProduct_comment['comment_content'], 'approved_status'=> $approved, 'is_inappropriate'=> $is_inappropriate, 'rating'=> $rating, 'user_id'=> $this->comment_user_id, 'comment_date'=> $nextProduct_comment['comment_date'] );
			...
			$new_dest_parent_comment_id = $this->m_ci->mproduct_comment->updateProduct_Comment( 0, $productComment );
			$level_count++;
			$product_comments_ret = $this->addProductComments($product_comments_list, $nextProduct_comment['comment_ID'], $new_dest_parent_comment_id, $product_name, $level_count);
		}
	}




	private function getAttributesIdsList( $attributes_text, $output= 'array' )
	{
		$attributes_list = unserialize($attributes_text);
		$ret_attributes_list = [];
		foreach ($attributes_list as $next_attributes_name => $next_attributes_value) {
			...
		}   // attributes_list::S=9:114,S=11:154,I=10: wwwwwwwwww,
		if ($output == 'array') {
			return $ret_attributes_list;
		}
		if ($output == 'string') { // string implode ( string $glue , array $pieces )
			return implode(',', $ret_attributes_list);
		}
		if ($output == 'arrays_string') { // string implode ( string $glue , array $pieces )
			$ret = '';
			foreach ($ret_attributes_list as $next_attribute_key => $next_attribute_value) {
				$ret .= "" . $next_attribute_key . ":" . $next_attribute_value . ", ";
			}
			return $ret;
		}
	}


	private function insertDefaultSettings(  )
	{
		$ci =& get_instance();
		$default_settings= [ 'contact_us_email' => 'contact_us@productscatalog.com',
		                     'currency'=> 'USD dollar', 'currency_left'=> 'Y',  'owner_phone'=> '1234567890', 'owner_email'=> 'owner_email@productscatalog.com',
		                     'small_icon'=> 'site-icon-small.png', 'big_icon'=> 'site-icon-big.png', 'price_list_default_product_image'=> 'empty_product_image.png',
		                     ...

		                     'blog_items_per_page'=> '10', 'max_chars_in_blog_header'=> '250', 'show_alert_popup'=> 'N', 'product_weight_unit'=> 'kg', 'product_dimension_unit'=> 'cm' ];


	}

	public function showResultsText() {
		$ret_str= "<b>".$this->m_users_added_count.'</b> users added, <b>'.
		          $this->m_attributes_added_count . '</b> attributes added, <b>'.
		          $this->m_attribute_items_added_count . '</b> attribute items added,<br><b>' . $this->m_categories_added_count.'</b> categories added, <span style="color:red;font-weight:bold">' .  $this->m_categories_adding_error_count .'</span> categories failed to add,<br><b>' .
		          $this->m_tags_added_count.'</b> tags added, <span style="color:red;font-weight:bold">' .  $this->m_tags_adding_error_count .'</span> tags failed to add., <br><b>' .
		          $this->m_shipping_classes_added_count.'</b> shipping classes added, <span style="color:red;font-weight:bold">' .  $this->m_shipping_classes_adding_error_count .'</span> shipping classes failed to add, <br><span style="color:red;font-weight:bold">' .
		          $this->m_attributes_notfound_error_count . '</span> attributes not found.<br><b>'.
		          $this->m_products_added_count.  '</b> products added, <span style="color:red;font-weight:bold">' .  $this->m_products_skipped_as_existing_count . '</span> products skipped as existing(by sku), <span style="color:red;font-weight:bold">' . $this->m_products_skipped_as_wrong_format_count . '</span> products skipped as wrong format, <br><span style="color:red;font-weight:bold">' .
		          $this->m_product_images_added_count.  '</span> product images added, <span style="color:red;font-weight:bold">' .  $this->m_product_images_adding_error_count . '</span> product images adding errors, <span style="color:red;font-weight:bold">'.$this->m_product_images_adding_skipped_as_existing_count.' </span>adding skipped as existing<br>' . $this->create_testing_users_text ;


}



