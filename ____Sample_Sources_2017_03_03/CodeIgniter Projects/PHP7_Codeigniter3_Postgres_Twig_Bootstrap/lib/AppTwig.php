<?php
class appTwig
{
    private $m_twig_Environment;
    public function __construct()
    {
    }

    public function setParemeters($twig_Environment) {
        $this->m_twig_Environment= $twig_Environment;

    }


    public function registerUserFunctions()
    {

	    $function = new Twig_SimpleFunction('show_stripe_button', function ($order) {
		    return appCart::getStripeButtonCode($order);
        });
        $this->m_twig_Environment->addFunction($function);


	    $function = new Twig_SimpleFunction('show_stripe_custom_button', function ($order) {
		    return appCart::getStripeCustomButtonCode($order);
        });
        $this->m_twig_Environment->addFunction($function);


        $function = new Twig_SimpleFunction('concat_array', function ($arr, $splitter = ',', $skip_empty = true, $skip_last_delimiter = true) {
            return appUtils::concatArray( $arr, $splitter, $skip_empty, $skip_last_delimiter );
        });
        $this->m_twig_Environment->addFunction($function);



        $filter = new Twig_SimpleFilter('get_attribute_type_label', function ( $type ) {
            $ci = & get_instance();
            $label= $ci->mattribute->getAttributeTypeLabel($type);
            return $label;
        });
        $this->m_twig_Environment->addFilter($filter);


        $filter = new Twig_SimpleFilter('select_searched_text', function ( $content, $search_text ) { // select_searched_text (search_text)
	        if (empty($content)) return $content;
       	    $ret= appUtils::selectSearchedText( $content, $search_text, 'searched_text');
            return $ret;
        });
        $this->m_twig_Environment->addFilter($filter);



        $filter = new Twig_SimpleFilter('concat_str', function ( $str, $max_len, $add_str = '...', $show_help = false, $strip_tags = true, $additive_code= '' ) {
            return appUtils::concatStr( $str, $max_len, $add_str, $show_help, $strip_tags, $additive_code );
        });
        $this->m_twig_Environment->addFilter($filter);


        $filter = new Twig_SimpleFilter('get_formatted_currency', function ( $value, $show_zero_value= true ) {
            if (!$show_zero_value and empty($value)) return '';
            return appUtils::getFormattedCurrency( $value );
        });
        $this->m_twig_Environment->addFilter($filter);


        $function = new Twig_SimpleFunction('get_currency_symbol', function () {
            $ci = & get_instance();
            return $ci->getSettings('currency_short', '$');
        });
        $this->m_twig_Environment->addFunction($function);


        $function = new Twig_SimpleFunction('get_currency_symbol_left', function () {
            $ci = & get_instance();
            return $ci->getSettings('currency_left','Y') == 'Y';
        });
        $this->m_twig_Environment->addFunction($function);


        $filter = new Twig_SimpleFilter('d', function ($v, $t='') {
            echo '<pre>'.(!empty($t)? $t.'::':'').print_r($v,true).'</pre>';
        });
        $this->m_twig_Environment->addFilter($filter);



        $function = new Twig_SimpleFunction('get_user_group_id', function ($label) {
            $ci = & get_instance();
            $group_id= $ci->muser->getGroupIdByName( $label );
            return $group_id;
        });
        $this->m_twig_Environment->addFunction($function);




        $function = new Twig_SimpleFunction('file_exists', function ($file_name) {
            return file_exists($file_name) and !is_dir($file_name);
        });
        $this->m_twig_Environment->addFunction($function);



        $function = new Twig_SimpleFunction('set_field_error_tag', function ($fieldname, $attr) {
            if (empty($attr)) $attr = ' class="has-error" ';
            $fieldvalue = strip_tags( form_error($fieldname) );
            if (!empty($fieldvalue)) {
                return $attr;
            }
        });
        $this->m_twig_Environment->addFunction($function);


        $function = new Twig_SimpleFunction('showListHeaderItem', function ($url, $filters_str, $field_title, $fieldname, $sort_direction, $sort ) {
            if (empty($field_title)) {
                $field_title = ucwords($fieldname);
            }
            $field_title = str_replace('', '&nbsp;', $field_title);
            $R = tplSortDirection( $fieldname, $sort_direction, $sort );
            $isFirstLetter = true;
            $filters_str .= ($isFirstLetter ? '/' : '/') . 'sort_direction/' . $R;
            $filters_str .= '/sort/' . urlencode($fieldname);
            $filters_str .= '/fieldname/' . urlencode($fieldname);
            $imgHtlm = tplListSortingImage($fieldname, $sort, $sort_direction);
            $res_url = '<a href="' . $url . $filters_str . '"><span>' . $field_title . $imgHtlm . '</span></a>';
            return $res_url;
        });
        $this->m_twig_Environment->addFunction($function);


        $function = new Twig_SimpleFunction('prepare_chosen_select', function ($data_list, $id, $current_value='', $width='', $multiple='', $additive_class= '') {
            $Res = '<select data-placeholder="Select Region" style="width:' . (!empty($width) ? (string)$width : '350') . ' '  . ' " class="form-control chosen-select ' . $additive_class . '" ' . ($multiple ? 'multiple' : '') . ' tabindex="6" id="' . $id . '">
	<option value=""></option> ';
            $current_value= !empty($current_value) ? $current_value : '';
            foreach ($data_list as $data_item) {
                $Res .= '<optgroup label="' . $data_item['state_name'] . '">';
                foreach ($data_item['regions_sub_array'] as $region_id => $region_name) {
                    $Res .= '<option value="' . $region_id . '" '.( (int)$current_value== (int)$region_id ? 'selected' : '').' >' . $region_name . '</option>';
                }
            }
            $Res .= '</select>';
            return $Res;
        } , array('is_safe' => array('html') ) );
        $this->m_twig_Environment->addFunction($function);

	    ...

        $filter = new Twig_SimpleFilter('get_user_active_label', function ( $active ) {
            return replaceSpaces(get_instance()->muser->getUserActiveTitle($active));
        });
        $this->m_twig_Environment->addFilter($filter);
	    ...

        $filter = new Twig_SimpleFilter('get_user_group_label', function ($user_id) {
            $ret= get_instance()->muser->getGroupLabelById($user_id);
            return $ret;
        });
        $this->m_twig_Environment->addFilter($filter);




        $filter = new Twig_SimpleFilter('get_user_username', function ($user_id) {
        	if (empty($user_id)) return '';
            $userRow= get_instance()->muser->getRowById($user_id);
            return !empty($userRow['username']) ? $userRow['username'] : '';
        });
        $this->m_twig_Environment->addFilter($filter);




        $filter = new Twig_SimpleFilter('date_format', function ($value, $format_time) {
            return replaceSpaces( appUtils::showFormattedDateTime($value, $format_time) );
        });
        $this->m_twig_Environment->addFilter($filter);



        $filter = new Twig_SimpleFilter('conditional_label', function ($value, $conditional_array, $default_value= "") {
	        foreach( $conditional_array as $next_key=>$next_value ) {
	            if ( $next_key == $value ) {
		            return $next_value;
	            }
	        }
            return replaceSpaces( $default_value );
        });
        $this->m_twig_Environment->addFilter($filter);


        $filter = new Twig_SimpleFilter('get_yes_no_label', function ($value,$show_zero_value=true) {
            if( !$show_zero_value and empty($value) ) return '';
	        $ret= replaceSpaces( appUtils::showYesNoLabel($value) );
            return $ret;
        });
        $this->m_twig_Environment->addFilter($filter);


//        /* ORDERS BLOCK START */
        $filter = new Twig_SimpleFilter('get_order_status_label', function ($status) {
	        $ci = & get_instance();
	        return $ci->morder->getOrderStatusLabel($status);
        });
        $this->m_twig_Environment->addFilter($filter);

        $filter = new Twig_SimpleFilter('get_order_payment_label', function ($payment) {
	        return appCart::getPaymentLabel($payment);
        });
        $this->m_twig_Environment->addFilter($filter);


        $filter = new Twig_SimpleFilter('get_order_mode_label', function ($mode) {
	        $ci = & get_instance();
	        return $ci->morder->getOrderModeLabel($mode);
        });
        $this->m_twig_Environment->addFilter($filter);


//        /* ORDERS BLOCK START */


//        /* PRODUCTS BLOCK START */
        $filter = new Twig_SimpleFilter('get_product_affiliated_user_status_label', function ($status) {
            $ci = & get_instance();
	        $status_label= $ci->mproduct_affiliated_user->getProductAffiliatedUserStatusLabel($status);
	        if ( !empty($status) and $status != 'Y' ) return 'Has ' . $status_label;
	        if ( $status == 'Y' ) {
		        return 'Has any ';
	        }
            return '';
        });
        $this->m_twig_Environment->addFilter($filter);


        $filter = new Twig_SimpleFilter('get_product_category_label', function ($category) {
            $categories_list= appUtils::pregSplit('/,/',$category);
            $ci = & get_instance();
            $categories_title= '';
            foreach( $categories_list as $next_key=>$next_category_id ) {
                $nextCategory= $ci->mcategory->getRowById($next_category_id);
                if ( !empty($nextCategory) ) {
                    $categories_title.= $nextCategory['name'].', ';
                }
            }
            return appUtils::trimRightSubString( $categories_title,',' );
        });
        $this->m_twig_Environment->addFilter($filter);

        $filter = new Twig_SimpleFilter('get_filter_shipping_class_label', function ($shipping_class_id,$show_zero_value=true) {
            if(!$show_zero_value and empty($in_stock)) return '';
            $ci = & get_instance();
            $shipping_Class= $ci->mshipping_class->getRowById($shipping_class_id);
	        return ( !empty($shipping_Class['name']) ? $shipping_Class['name'] : '' );
        });
        $this->m_twig_Environment->addFilter($filter);



        $filter = new Twig_SimpleFilter('get_product_in_stock_label', function ($in_stock,$show_zero_value=true) {
            if(!$show_zero_value and empty($in_stock)) return '';
            $ci = & get_instance();
            return $ci->mproduct->getProductInStockLabel($in_stock);
        });
        $this->m_twig_Environment->addFilter($filter);

...

        $filter = new Twig_SimpleFilter('get_product_comment_approved_status_label', function ($approved_status) {
            $ci = & get_instance();
            return $ci->mproduct_comment->getProductCommentApprovedStatusLabel($approved_status);
        });
        $this->m_twig_Environment->addFilter($filter);

        $filter = new Twig_SimpleFilter('get_product_comment_review_stars_rating_type_label', function ( $stars_rating ) {
            return replaceSpaces(get_instance()->mproduct_comment->getproduct_comment_Review_Stars_Rating_TypeLabel($stars_rating) );
        });
        $this->m_twig_Environment->addFilter($filter);

        $filter = new Twig_SimpleFilter('get_product_comment_approved_status_label', function ($approved_status) {
            $ci = & get_instance();
            return $ci->mproduct_comment->getProductCommentApprovedStatusLabel($approved_status);
        });
        $this->m_twig_Environment->addFilter($filter);


        $filter = new Twig_SimpleFilter('get_user_active_status_label', function ($active_status) {
            $ci = & get_instance();
            return $ci->muser->getUserActiveStatusLabel($active_status);
        });
        $this->m_twig_Environment->addFilter($filter);


	    $filter = new Twig_SimpleFilter('get_product_filter_is_default_label', function ($is_default) {
		    $ci = & get_instance();
		    return $ci->mproduct_filter->getProductFilterIsDefaultLabel($is_default);
	    });
	    $this->m_twig_Environment->addFilter($filter);


	    $filter = new Twig_SimpleFilter('get_product_filter_filters_link', function ($filtersArray) {
		    $json_b = json_decode($filtersArray, true);
		    if ( empty($json_b) or !is_array($json_b) ) return 'EMPTY';
		    $ret_str= '';
		    foreach( $json_b as $next_key=>$next_value ) {
		    	if ( in_array($next_key, ['sort', 'sort_direction']) ) {
				    $ret_str .= $next_key . '/' . $next_value . '/';
			    } else {
				    $ret_str .= 'filter_' . $next_key . '/' . $next_value . '/';
			    }
		    }
		    return $ret_str;
	    });
	    $this->m_twig_Environment->addFilter($filter);
//        /* PRODUCTS BLOCK END */


        /* CMS ITEMS BLOCK START */
        $filter = new Twig_SimpleFilter('get_cms_item_page_type', function ($active_status) {
            $ci = & get_instance();
            return $ci->mcms_item->getPage_TypeLabel($active_status);
        });
        $this->m_twig_Environment->addFilter($filter);


        $filter = new Twig_SimpleFilter('get_cms_item_published_status_label', function ($published) {
            $ci = & get_instance();
            return $ci->mcms_item->getCms_Item_PublishedLabel($published);
        });
        $this->m_twig_Environment->addFilter($filter);


        $filter = new Twig_SimpleFilter('get_cms_item_icon', function ($img, $id) {
            $ci = & get_instance();
            $ret= $ci->mcms_item->getCms_ItemImageUrl($id, $img);
            return $ret;

        });
        $this->m_twig_Environment->addFilter($filter);
        /* CMS ITEMS BLOCK END */


        /* CATEGORIES BLOCK START */
        $filter = new Twig_SimpleFilter('get_category_icon', function ($img, $id) {
            $ci = & get_instance();
            $url= $ci->mcategory->getCategoryImageUrl($id, $img);
            return $url;
        });
        $this->m_twig_Environment->addFilter($filter);
        /* CATEGORIES BLOCK END */
    }

} // class appTwig

function replaceSpaces($S)
{
    $Pattern = '/([\s])/xsi';
    $S = preg_replace($Pattern, '&nbsp;', $S);
    return $S;
}

function tplSortDirection($fieldname, $sort_direction, $sort)
{
    return (($sort == $fieldname and $sort_direction == 'asc') ? "desc" : "asc");
}

function tplListSortingImage($fieldname, $sort, $sort_direction)
{
    $ci = & get_instance();
    $config_object = $ci->config;
    $configArray = $config_object->config;
    if ($sort == $fieldname and strtolower($sort_direction) == 'asc') {
        return '<i class="glyphicon glyphicon-arrow-down" style="display: inline-block;" ></i>';
    }
    if ($sort == $fieldname and strtolower($sort_direction) == 'desc') {
        return '<i class="glyphicon glyphicon-arrow-up" style="display: inline-block;" ></i>';
    }
}