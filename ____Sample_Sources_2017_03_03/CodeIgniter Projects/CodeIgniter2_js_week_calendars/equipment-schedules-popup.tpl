<link rel="stylesheet" type="text/css" href="/static/css/jquery-ui/themes/base/jquery-ui.css" />
<link rel="stylesheet" type="text/css" href="/static/css/jquery-ui/themes/base/jquery.ui.datepicker.css" />
<link rel="stylesheet" type="text/css" href="/static/css/jquery.weekcalendar.css" />
<script src="/static/js/jquery.weekcalendar.js"></script>

<script>
    var $scheduleSetType = "unavailable";

    $(document).ready(function(){
        $("form#holiday-add input[type='text']").datepicker();
        $("#equipmentAvailableScheduleContainer").show();
        loadEquipmentScheduleCalendar($("#equipmentAvailableSchedule"), {$Equipment->id});
    });

    function loadEquipmentScheduleCalendar($Target, $EquipmentID) {
        $Target.weekCalendar({
            timeslotsPerHour: 4,
            timeslotHeight: 10,
            daysToShow: 7,
            firstDayOfWeek: 1,
            displayOddEven: true,
            hourLine: true,
            allowEventDelete: true,
            dateFormat: "",
            showHeader: false,
            date: new Date("2014-03-10"),
            height: function ($calendar) {
                return 300;
            },
            eventRender: function ($calEvent, $element) {
                $calEvent.title = "";
                $calEvent.id = Math.floor(Math.random() * 65535);
                return $element;
            },

            eventNew: function (calEvent, $event) {
                var src_weekday= calEvent.start.getDay()
                var dst_weekday= calEvent.start.getDay()
                if ( src_weekday == 0 ) src_weekday= 7;
                if ( dst_weekday == 0 ) dst_weekday= 7;
                $( "#day_selection_start_" + src_weekday ).val(-1)
                $( "#day_selection_end_" + src_weekday ).val(-1)
                var start_hours= calEvent.start.getHours()
                var start_minutes= calEvent.start.getMinutes()
                var end_hours= calEvent.end.getHours()
                var end_minutes= calEvent.end.getMinutes()
                $( "#day_selection_start_" + dst_weekday ).val( start_hours * 60 + start_minutes )
                $( "#day_selection_end_" + dst_weekday ).val( end_hours * 60 + end_minutes )
            },

            eventDrop: function (calEvent, $event) {
                var src_weekday= $event.start.getDay()
                var dst_weekday= calEvent.start.getDay()
                if ( src_weekday == 0 ) src_weekday= 7;
                if ( dst_weekday == 0 ) dst_weekday= 7;
                $( "#day_selection_start_" + src_weekday ).val(-1)
                $( "#day_selection_end_" + src_weekday ).val(-1)
                var start_hours= calEvent.start.getHours()
                var start_minutes= calEvent.start.getMinutes()
                var end_hours= calEvent.end.getHours()
                var end_minutes= calEvent.end.getMinutes()
                $( "#day_selection_start_" + dst_weekday ).val( start_hours * 60 + start_minutes )
                $( "#day_selection_end_" + dst_weekday ).val( end_hours * 60 + end_minutes )
            },

            eventResize: function (calEvent, $event) {
                var src_weekday= calEvent.start.getDay()
                var dst_weekday= calEvent.start.getDay()
                if ( src_weekday == 0 ) src_weekday= 7;
                if ( dst_weekday == 0 ) dst_weekday= 7;
                $( "#day_selection_start_" + src_weekday ).val(-1)
                $( "#day_selection_end_" + src_weekday ).val(-1)
                var start_hours= calEvent.start.getHours()
                var start_minutes= calEvent.start.getMinutes()
                var end_hours= calEvent.end.getHours()
                var end_minutes= calEvent.end.getMinutes()
                $( "#day_selection_start_" + dst_weekday ).val( start_hours * 60 + start_minutes )
                $( "#day_selection_end_" + dst_weekday ).val( end_hours * 60 + end_minutes )
            },

            eventClick: function(calEvent, element, dayFreeBusyManager, calendar, clickEvent) {
                //alert( "eventClick calEvent::"+var_dump(calEvent)+"  element::"+var_dump(element) + "  clickEvent::"+var_dump(clickEvent))
                var src_weekday= calEvent.start.getDay()
                var dst_weekday= calEvent.start.getDay()
                if ( src_weekday == 0 ) src_weekday= 7;
                if ( dst_weekday == 0 ) dst_weekday= 7;
                $( "#day_selection_start_" + src_weekday ).val(-1)
                $( "#day_selection_end_" + src_weekday ).val(-1)
                var start_hours= calEvent.start.getHours()
                var start_minutes= calEvent.start.getMinutes()
                var end_hours= calEvent.end.getHours()
                var end_minutes= calEvent.end.getMinutes()
                $( "#day_selection_start_" + dst_weekday ).val( start_hours * 60 + start_minutes )
                $( "#day_selection_end_" + dst_weekday ).val( end_hours * 60 + end_minutes )
            },

            eventDelete: function (calEvent, element, dayFreeBusyManager, calendar, clickEvent) {
                calendar.weekCalendar('removeEvent', calEvent.id);
            }
        });


        $.ajax({
            timeout: 10000,
            url: $ajaxurl + "/businessEquipmentGetSchedule",
            type: "POST",
            dataType: "json",
            data: { "equipment_id": $EquipmentID }
        }).done(function (data) {
            if (!data) {
                alert("Data loading error");
            } else {
                var $wdToDates = {  'year': 2014, 'month' : 2/*month zero based*/, 'day' : 10  }
                $Target.weekCalendar("clear");

                data.data.forEach(function (obj) {
                    var $calEvent = new Object();
                    var start_hour= getTimeParamater( obj.time_start, 'hour', ':' )
                    var start_minute= getTimeParamater( obj.time_start, 'minute', ':' )
                    $calEvent.start = new Date( $wdToDates['year']/*year*/, $wdToDates['month']/*month*/ , parseInt($wdToDates['day'])+parseInt(obj.weekday) -1 /*day*/,
                            start_hour /*hours*/, start_minute /*minutes*/ /*seconds*/ /*ms*/ );

                    var end_hour= getTimeParamater( obj.time_end, 'hour', ':' )
                    var end_minute= getTimeParamater( obj.time_end, 'minute', ':' )
                    $calEvent.end = new Date( $wdToDates['year']/*year*/, $wdToDates['month']/*month*/ , parseInt($wdToDates['day'])+parseInt(obj.weekday) - 1 /*day*/,
                            end_hour /*hours*/, end_minute /*minutes*/ /*seconds*/ /*ms*/ );

                    $calEvent.id = Math.floor(Math.random() * 65535);
                    $calEvent.title = "";

                    try {
                        $Target.weekCalendar("updateEvent", $calEvent)
                    } catch (e) {
                        //alert( "$calEvent::"+var_dump($calEvent)+"  e::"+var_dump(e));
                    }

                    var start_hours= $calEvent.start.getHours()
                    var start_minutes= $calEvent.start.getMinutes()
                    var end_hours= $calEvent.end.getHours()
                    var end_minutes= $calEvent.end.getMinutes()
                    var dst_weekday= $calEvent.start.getDay()
                    $( "#day_selection_start_" + dst_weekday ).val( start_hours * 60 + start_minutes )
                    $( "#day_selection_end_" + dst_weekday ).val( end_hours * 60 + end_minutes )
                });
            }
        }).error(function (data) {
            alert("Data saving error");
        });
    }


    function MakeEquipmentScheduleSave() {
        err("", "class-success");
        err("", "class");
        $Target= $("#equipmentAvailableSchedule")
        $.ajax({
            timeout: 10000,
            url: $ajaxurl + "/businessEquipmentUpdateSchedule",
            type: "POST",
            dataType: "json",
            data: { "data": $Target.weekCalendar("serializeEvents"), "equipment": '{$Equipment->id}' }
        }).done(function (data) {
            if (!data) {
                alert("Data saving error");
            } else {
                $("#span-box-modal-alert").html("{"Equipment's Schedule saved"|translate} ! ")
                $("#div-box-modal-alert").css("display", "block")
                $('#div-box-modal-alert').fadeOut(parseInt('{$messageDisappearingTime}'))
                loadEquipmentsSchedulesData('{$Equipment->id}')
            }
        }).error(function (data) {
            alert("Data saving error");
        });

    }


    function time_selection_onchange( modifiedWeekday, type ) {
        err("", "class-success");
        err("", "class");
        var $wdToDates = {  'year': 2014, 'month': 2, 'day': 10  }
        var start_value= parseInt( $( "#day_selection_start_" + modifiedWeekday ).val() )
        var end_value= parseInt( $( "#day_selection_end_" + modifiedWeekday ).val() )

        if ( start_value != -1 && end_value != -1 && start_value >= end_value ) {
            err("{"Wrong time selected"|translate}!", "class")
            $( "#day_selection_start_" + modifiedWeekday ).focus()
            return;
        }
        $Target= $("#equipmentAvailableSchedule")
        var scheduleData= []
        weekday= modifiedWeekday
            var start_value= $( "#day_selection_start_" + weekday ).val()

            var end_value= $( "#day_selection_end_" + weekday ).val()  // 120  day_selection_end_1
            if ( start_value == -1 || end_value == -1  ) return;

            var scheduleItem = []
            scheduleItem['weekday'] = weekday
            time_start = new Date($wdToDates['year'], $wdToDates['month'], parseInt($wdToDates['day']) + parseInt(weekday) - 1, 0, start_value);
            time_end = new Date($wdToDates['year'], $wdToDates['month'], parseInt($wdToDates['day']) + parseInt(weekday) - 1, 0, end_value);

            scheduleItem['time_start'] = time_start
            scheduleItem['time_end'] = time_end
            scheduleData[scheduleData.length] = scheduleItem

            if (scheduleData) {
                scheduleData.forEach(function (obj) {
                    var calEvent = new Object();
                    calEvent.start = obj.time_start;
                    calEvent.end = obj.time_end;
                    calEvent.id = Math.floor(Math.random() * 65535);
                    calEvent.title = "";
                    try {
                        $Target.weekCalendar("updateEvent", calEvent)
                    } catch (e) {
                        //alert( "$calEvent::"+var_dump($calEvent)+"  e::"+var_dump(e));
                    }
                });
            }
        // } // for( weekday = 1; weekday <= 7; weekday++ ) {
    }

</script>


<h4>{"Please select a time ranges when Equipment is AVAILABLE"|translate}:</h4>

<div class="alert alert-danger alert-class" style="display: none;">
    <button type="button" class="close">&times;</button>
    <span class="message">{if isset($Error)}{$Error}{/if}</span>
</div>

<div class="alert alert-success alert-class-success" style="display: none;">
    <button type="button" class="close">&times;</button>
    <span class="message">{if isset($Error)}{$Error}{/if}</span>
</div>

<div class="form-inline" style=" max-width: 800px;" id="div-add-new-schedule">

<table style="max-width: 800px; border: none;" cellpadding="0" cellspacing="0">
    <tr>
        <td style="padding-right: 40px; padding-top: 5px;">
            &nbsp;
        </td>
        <td style="padding-top: 5px;">
            <small>{"Monday"|translate}</small>
        </td>
        <td style="padding-top: 5px;">
            <small>{"Tuesday"|translate}</small>
        </td>
        <td style="padding-top: 5px;">
            <small>{"Wednesday"|translate}</small>
        </td>
        <td style="padding-top: 5px;">
            <small>{"Thursday"|translate}</small>
        </td>
        <td style="padding-top: 5px;">
            <small>{"Friday"|translate}</small>
        </td>
        <td style="padding-top: 5px;">
            <small>{"Saturday"|translate}</small>
        </td>
        <td style="padding-top: 5px;">
            <small>{"Sunday"|translate}</small>
        </td>
    </tr>

    <tr style="padding-top: 5px;">
        <td style="width: 40px; padding-top: 5px;">
            &nbsp;
        </td>
        <td>
            {include '../../lib/input_time_selection.tpl' class="form-control" id="day_selection_start_1" weekday=1 type="start" value="" width=106}
        </td>
        <td>
            {include '../../lib/input_time_selection.tpl' class="form-control" id="day_selection_start_2" weekday=2 type="start" value="" width=106}
        </td>
        <td>
            {include '../../lib/input_time_selection.tpl' class="form-control" id="day_selection_start_3" weekday=3 type="start" value="" width=106}
        </td>
        <td>
            {include '../../lib/input_time_selection.tpl' class="form-control" id="day_selection_start_4" weekday=4 type="start" value="" width=106}
        </td>
        <td>
            {include '../../lib/input_time_selection.tpl' class="form-control" id="day_selection_start_5" weekday=5 type="start" value="" width=106}
        </td>
        <td>
            {include '../../lib/input_time_selection.tpl' class="form-control" id="day_selection_start_6" weekday=6 type="start" value="" width=106}
        </td>
        <td>
            {include '../../lib/input_time_selection.tpl' class="form-control" id="day_selection_start_7" weekday=7 type="start" value="" width=106}
        </td>
    </tr>

    <tr>
        <td style="width: 40px; padding-top: 5px;">
            &nbsp;
        </td>
        <td style="padding-top: 5px;">
            {include '../../lib/input_time_selection.tpl' class="form-control" id="day_selection_end_1" weekday=1 type="end" value="" width=106}
        </td>
        <td style="padding-top: 5px;">
            {include '../../lib/input_time_selection.tpl' class="form-control" id="day_selection_end_2" weekday=2 type="end" value="" width=106}
        </td>
        <td style="padding-top: 5px;">
            {include '../../lib/input_time_selection.tpl' class="form-control" id="day_selection_end_3" weekday=3 type="end" value="" width=106}
        </td>
        <td style="padding-top: 5px;">
            {include '../../lib/input_time_selection.tpl' class="form-control" id="day_selection_end_4" weekday=4 type="end" value="" width=106}
        </td>
        <td style="padding-top: 5px;">
            {include '../../lib/input_time_selection.tpl' class="form-control" id="day_selection_end_5" weekday=5 type="end" value="" width=106}
        </td>
        <td style="padding-top: 5px;">
            {include '../../lib/input_time_selection.tpl' class="form-control" id="day_selection_end_6" weekday=6 type="end" value="" width=106}
        </td>
        <td style="padding-top: 5px;">
            {include '../../lib/input_time_selection.tpl' class="form-control" id="day_selection_end_7" weekday=7 type="end" value="" width=106}
        </td>
    </tr>
</table>

<div id="equipmentAvailableScheduleContainer" style="display: none;">
    <div id="equipmentAvailableSchedule"></div><br/>

    <div class="widget-body" style="border-top: solid 1px #CCCCCC; padding-top: 5px; ">
        <div class="form-group">
            <div class="col-sm-6">
            </div>
            <div class="col-sm-4" >
                <div class="form-group">
                    <div class="col-sm-8" style="padding-right: 40px">
                        <a target="_blank" class="btn-primary btn" onclick="javascript:MakeEquipmentScheduleSave()">{"Save"|translate}</a>&nbsp;
                    </div>
                    <div class="col-sm-4" >
                        <a class="btn-primary btn modalclose arcticmodal-close" href="#">{"Cancel"|translate}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
</div>
