 {extends file="includes/website.tpl"}

{block name="body"}


    <script src="/static/js/jquery-ui/jquery.ui.datepicker.min.js"></script>
    <script src="/static/js/jquery-ui-timepicker-addon.js"></script>


    <script type="text/javascript" src="/static/js/jquery-ui/jquery.ui.autocomplete.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/static/css/jquery-ui/themes/base/jquery.ui.autocomplete.css" />

    <link rel="stylesheet" type="text/css" href="/static/css/fullcalendar.css" />
    {*    <link rel="stylesheet" type="text/css" href="/static/js/fullcalendar-lang/{$language_code}.js" />  *}
    <script language="javascript" src="/static/js/moment.min.js"></script>

    <script language="javascript" src="/static/assets/components/common/forms/elements/bootstrap-datepicker/assets/lib/js/bootstrap-datepicker.js"></script>

    <script language="javascript" src="/static/assets/components/common/forms/elements/bootstrap-datepicker/assets/custom/js/bootstrap-datepicker.init.js"></script>



    <script language="javascript" src="/static/js/fullcalendar.js"></script>
    {* <script language="javascript" src="/static/js/fullcalendar-lang/en-gb.js"></script>    *}

    <link href="/static/css/select2.css" rel="stylesheet"/>
    <script src="/static/js/select2.js"></script>

    <h3>{"Your calendar"|translate}</h3>
    {if $Message}
        <div class="alert {if $MessageType == "error"}alert-danger{else}alert-success{/if} alert-user-register">
            <button type="button" class="close">&times;</button>
            <span class="message">{$Message}</span>
        </div>
    {/if}

    <script>

    var $evtUrl;
    var select_scheduling_mode = '';
    var calendarSelectedDate= new Date()

    function getOnlyProvider() {
        var retProviderId = ''
        var providersCount = 0
        if (select_scheduling_mode == "appointments") {
            var sched_filter_staffs_selected = '';
            var continueWorking= true;
            $('#appointments_sched_filter_staffs option:selected').each(function () {
                if ($(this).val() == "all") {
                    continueWorking= false
                    return '';
                }
                if ($(this).val() != "all" && continueWorking ) {
                    providersCount = providersCount + 1
                    retProviderId = $(this).val()
                }
            });
            if ( providersCount == 1 ) return retProviderId;
        }
        return '';
    } // function getOnlyProvider() {

    function getMultyProviders() {
        var retMultyProvidersId = ''
        if (select_scheduling_mode == "appointments") {
            var sched_filter_staffs_selected = '';
            $('#appointments_sched_filter_staffs option:selected').each(function () {
                if ($(this).val() != "all") {
                    retMultyProvidersId = retMultyProvidersId + $(this).val() + ","
                }
            });
        }
        return retMultyProvidersId;
    } // function getMultyProviders() {

    function appointments_sched_filter_staffsonChange()
    {
        var onlyProviderId= getOnlyProvider()
        $("#span_showAddPersonalTime").css("display","inline");
    }
    ////////////////// getOnlyLocation //
    function getOnlyLocation() {
        var retLocationId = ''
        var LocationsCount = 0
        if (select_scheduling_mode == "appointments") {
            var sched_filter_staffs_selected = '';
            var continueWorking= true;
            $('#appointments_sched_filter_locations option:selected').each(function () {
                if ($(this).val() == "all") {
                    continueWorking= false
                    return '';
                }
                if ($(this).val() != "all" && continueWorking ) {
                    LocationsCount = LocationsCount + 1
                    retLocationId = $(this).val()
                }
            });
            if ( LocationsCount == 1 ) return retLocationId;
        }
        return '';
    } // function getOnlyLocation() {

    function getMultyLocations() {
        var retMultyLocationsId = ''
        if (select_scheduling_mode == "appointments") {
            var sched_filter_staffs_selected = '';
            $('#appointments_sched_filter_locations option:selected').each(function () {
                if ($(this).val() != "all") {
                    retMultyLocationsId = retMultyLocationsId + $(this).val() + ","
                }
            });
        }
        return retMultyLocationsId;
    } // function getOnlyLocation() {

    function select_scheduling_modeChange() {
        select_scheduling_mode = $("#select_scheduling_mode").val();
        // alert( "select_scheduling_mode  select_scheduling_mode::" + select_scheduling_mode )
        if ( select_scheduling_mode == "appointments" ) { // "Appointments calendar"

            if (select_scheduling_mode == "" || typeof select_scheduling_mode == "undefined") select_scheduling_mode = "A";
            $("#appointments_sched_filter_staffs").val("all");
            $("#appointments_sched_filter_services").val("all");
            $("#appointments_sched_filter_locations").val("all");
            $("#appointments_sched_filter_statuses").val("all");
            $("#div_appointment_scheduling").css("display","block");
            $("#div_resource_scheduling").css("display","none");

            var onlyProviderId= getOnlyProvider()
            $("#span_showAddPersonalTime").css("display","inline");
            /*if ( onlyProviderId != ""  ) { // there is only 1 Provider selected
                $("#div_showAddPersonalTime").css("display","block");
            } else {
                $("#div_showAddPersonalTime").css("display","none");
            }                              */

        }
        if ( select_scheduling_mode == "resource" ) { // "Classes/Activities calendar"
            $("#resource_sched_filter_staffs").val("all");
            $("#resource_sched_filter_services").val("all");
            $("#resource_sched_filter_locations").val("all");
            $("#div_appointment_scheduling").css("display","none");
            $("#div_resource_scheduling").css("display","block");
            $("#span_showAddPersonalTime").css("display","none");
        }
        runFilter();

    }

    function showAddPersonalTime() {
        var onlyProviderId= getOnlyProvider()
        var multyProvidersId= ''
        if (onlyProviderId == '') { // only Provider is not selected!
            multyProvidersId= getMultyProviders()
        }
        $.ajax({
            timeout: 10000,
            url: $ajaxurl_scheduler + "/editPersonalTime/",
            type: "POST",
            dataType: "json",
            data: { 'onlyProviderId' : onlyProviderId, "multyProvidersId" : multyProvidersId, "mode" : "add" }
        }).done(function (data) {
            if (data.error) {
                err(data.error, "login");
            } else if (data.html) {
                $("#popupwnd div.popupData").html(data.html);
                $("#popupwnd").arcticmodal();
            } else {
                alert("{"Please try again later"|translate}.");
            }
        }).error(function (data) {
            alert("{"Please try again later"|translate}.");
        });
        return false;
    }

    function runFilter() {
        var ret= startAndEndOfWeek( calendarSelectedDate );
        loadCalendar('agendaWeek', calendarSelectedDate, ret[0], ret[1]);
    }

    $(document).ready(function(){
        $("#appointments_sched_filter_staffs").select2();
        $("#appointments_sched_filter_services").select2();
        $("#appointments_sched_filter_locations").select2();
        $("#appointments_sched_filter_statuses").select2();
        $("#resource_sched_filter_staffs").select2();
        $("#resource_sched_filter_services").select2();
        $("#resource_sched_filter_locations").select2();
        var ret= startAndEndOfWeek( calendarSelectedDate );
        loadCalendar('agendaWeek', calendarSelectedDate, ret[0], ret[1]);

        $('#datepicker-inline').bind('changeDate', onDateChange);
    });


    function onDateChange(dat) {
        calendarSelectedDate= dat.date
        var ret= startAndEndOfWeek( calendarSelectedDate );
        loadCalendar('agendaWeek', calendarSelectedDate, ret[0], ret[1]);
    }

    function editPersonalTime( $formatted_datetime, mode, appointments_id, event_title ) {      // appointments_id
        $.ajax({
            timeout: 10000,
            url: $ajaxurl_scheduler + "/editPersonalTime/",
            type: "POST",
            dataType: "json",
            data: { "formatted_datetime": $formatted_datetime, "mode" : mode, 'appointments_id' : appointments_id }
        }).done(function(data){
            // alert( "data::"+var_dump(data))
            if (data.error) {
                err(data.error, "login");
            } else if (data.html) {
                $("#popupwnd div.popupData").html(data.html);
                $("#popupwnd").arcticmodal();
            } else {
                alert("{"Please try again later"|translate}.");
            }
        }).error(function(data){
            alert("{"Please try again later"|translate}.");
        });
    }

    function editCalendarScheduleClass( $formatted_datetime, mode, appointments_id, event_title ) {      // appointments_id
        $.ajax({
            timeout: 10000,
            url: $ajaxurl_scheduler + "/editCalendarScheduleClass/",  // addCalendarScheduleItemSelector
            type: "POST",
            dataType: "json",
            data: { "formatted_datetime": $formatted_datetime, "mode" : mode, 'appointments_id' : appointments_id }
        }).done(function(data){
            // alert( "data::"+var_dump(data))
            if (data.error) {
                err(data.error, "login");
            } else if (data.html) {
                $("#popupwnd div.popupData").html(data.html);
                $("#popupwnd").arcticmodal();
            } else {
                alert("{"Please try again later"|translate}.");
            }
        }).error(function(data){
            alert("{"Please try again later"|translate}.");
        });
    }

    function editCalendarScheduleService( $formatted_datetime, mode, onlyProviderId, multyProvidersId, onlyLocationId, multyLocationsId, appointments_id, event_title ) {      //
        $.ajax({
            timeout: 10000,
            url: $ajaxurl_scheduler + "/editCalendarScheduleService/",
            type: "POST",
            dataType: "json",
            data: { "formatted_datetime": $formatted_datetime, "mode" : mode, 'appointments_id' : appointments_id, 'onlyProviderId' : onlyProviderId, 'multyProvidersId' : multyProvidersId, 'onlyLocationId' : onlyLocationId, 'multyLocationsId' : multyLocationsId }
        }).done(function(data){
            // alert( "data::"+var_dump(data))
            if (data.error) {
                err(data.error, "login");
            } else if (data.html) {
                $("#popupwnd div.popupData").html(data.html);
                $("#popupwnd").arcticmodal();
            } else {
                alert("{"Please try again later"|translate}.");
            }
        }).error(function(data){
            alert("{"Please try again later"|translate}.");
        });
    }

    function loadCalendar(agendaType, selectedDate, dateStart, dateEnd) {
        if ( typeof selectedDate == "undefined" ) {
            selectedDate= new Date()
            var ret= startAndEndOfWeek( selectedDate );
            // alert(  " selectedDate::" + selectedDate + "  ret::"+var_dump(ret) )
            dateStart=ret[0]
            dateEnd= ret[1]
        }
        if ( agendaType=='' || typeof agendaType == "undefined" ) {
            agendaType= 'agendaWeek'
        }
        select_scheduling_mode = $("#select_scheduling_mode").val();
        var $evtUrl= $ajaxurl_scheduler + "/schedulerCalendar/?";
        if ( select_scheduling_mode == "resource" ) {
            if (select_scheduling_mode == "" || typeof select_scheduling_mode == "undefined") select_scheduling_mode = "A";
            var sched_filter_staffs_selected = '';
            $('#resource_sched_filter_staffs option:selected').each(function () {
                sched_filter_staffs_selected = sched_filter_staffs_selected + $(this).val() + ",";
            });

            var sched_filter_services_selected = '';
            $('#resource_sched_filter_services option:selected').each(function () {
                sched_filter_services_selected = sched_filter_services_selected + $(this).val() + ",";
            });

            var sched_filter_locations_selected = $("#resource_sched_filter_locations").val();
            //alert( "sched_filter_staffs_selected::"+sched_filter_staffs_selected)
            $evtUrl += 'request_type=' + select_scheduling_mode + '&sched_filter_staffs=' + sched_filter_staffs_selected + "&sched_filter_services=" + sched_filter_services_selected + "&sched_filter_locations=" + sched_filter_locations_selected
        }
        if ( select_scheduling_mode == "appointments" ) {
            if (select_scheduling_mode == "" || typeof select_scheduling_mode == "undefined") select_scheduling_mode = "A";
            var sched_filter_staffs_selected = '';
            $('#appointments_sched_filter_staffs option:selected').each(function () {
                sched_filter_staffs_selected = sched_filter_staffs_selected + $(this).val() + ",";
            });

            var sched_filter_services_selected = '';
            $('#appointments_sched_filter_services option:selected').each(function () {
                sched_filter_services_selected = sched_filter_services_selected + $(this).val() + ",";
            });

            var sched_filter_locations_selected = '';
            $('#appointments_sched_filter_locations option:selected').each(function () {
                sched_filter_locations_selected = sched_filter_locations_selected + $(this).val() + ",";
            });

            var sched_filter_statuses_selected = '';
            $('#appointments_sched_filter_statuses option:selected').each(function () {
                sched_filter_statuses_selected = sched_filter_statuses_selected + $(this).val() + ",";
            });

            $evtUrl += 'request_type=' + select_scheduling_mode + '&sched_filter_staffs=' + sched_filter_staffs_selected + "&sched_filter_services=" + sched_filter_services_selected + "&sched_filter_locations=" + sched_filter_locations_selected+ "&sched_filter_statuses=" + sched_filter_statuses_selected
        }

        var $oldEvtUrl = $evtUrl;
        $('#calendar').fullCalendar('destroy');
        var slotDurationValue= '00:15:00'

        $('#calendar').fullCalendar({
            'allDaySlot': false,        // Determines if the "all-day" slot is displayed at the top of the calendar.  When hidden with false, all-day events will not be displayed in agenda views.
            'slotEventOverlap' : true,
            'slotDuration': slotDurationValue,
            'height': 870,
            editable: select_scheduling_mode == "appointments",
            contentHeight: 800,
            titleFormat: {
                month: 'MMMM YYYY', // like "September 1986". each language will override this
                week: 'LL', //'ll', // like "Sep 4 1986"
                //day: 'LL' // like "September 4 1986"
                day: 'dddd, MMMM D, YYYY'   //whatever date format you want here
            },

            now         : selectedDate,
            defaultDate : selectedDate,
            today       : dateStart,
            intervalStart   : dateStart,
            intervalEnd     : dateEnd,
            start   : dateStart,
            end     : dateEnd,
            'eventSources': [$evtUrl],

            dayClick: function (date, allDay, jsEvent, view) {
                var select_scheduling_mode = $("#select_scheduling_mode").val();

                var onlyProviderId= getOnlyProvider()
                var multyProvidersId= ''
                if (onlyProviderId == '') { // only Provider is not selected!
                    multyProvidersId= getMultyProviders()
                }

                var onlyLocationId= getOnlyLocation()
                var multyLocationsId= ''
                if (onlyLocationId == '') { // only Location is not selected!
                    multyLocationsId= getMultyLocations()
                }

                if ( select_scheduling_mode == "appointments" ) { // "Appointments calendar"
                    var unTime= convertJSDateTimeToUnix(date._d)
                    editCalendarScheduleService(unTime, 'add', onlyProviderId, multyProvidersId, onlyLocationId, multyLocationsId );
                    return;
                }

                $.ajax({
                    timeout: 10000,
                    url: $ajaxurl_scheduler + "/addCalendarScheduleItemSelector/",
                    type: "POST",
                    dataType: "json",
                    data: { "year": date._a[0], "month": parseInt(date._a[1]+1), "day": date._a[2], "hour": date._a[3], "minute": date._a[4], 'onlyProviderId' : onlyProviderId, "multyProvidersId" : multyProvidersId, 'onlyLocationId' : onlyLocationId, "multyLocationsId" : multyLocationsId  }
                }).done(function (data) {
                    if (data.error) {
                        err(data.error, "login");
                    } else if (data.html) {
                        $("#popupwnd div.popupData").html(data.html);
                        $("#popupwnd").arcticmodal();
                    } else {
                        alert("{"Please try again later"|translate}.");
                    }
                }).error(function (data) {
                    alert("{"Please try again later"|translate}.");
                });
                return false;

            },

            eventDrop: function(event, dayDelta, minuteDelta, allDay, revertFunc) {

                var mysql_start= $('#calendar').fullCalendar('formatDate', event.start, "YYYY-MM-D H:mm");

                var mysql_end;

                if (event.end != null||event.end != undefined) {
                    mysql_end = $('#calendar').fullCalendar('formatDate', event.end, "YYYY-MM-D H:mm");
                }

                $.ajax({
                    timeout: 10000,
                    url: $ajaxurl_scheduler + "/updateAppointmentTime/",
                    type: "POST",
                    dataType: "json",
                    data: { "mysql_start" : mysql_start, "mysql_end" : mysql_end, "appointment_type" : event.type, "appointments_id" : event.appointments_id }
                    //data: { "start_unix" : convertJSDateTimeToUnix(event.start.format()), "end_unix" : convertJSDateTimeToUnix(event.end.format()), "appointment_type" : event.type, "appointments_id" : event.appointments_id }
                }).done(function (data) {
                    //alert( "data::"+var_dump(data) )
                    if (data.error) {
                        err(data.error, "");
                    } else {
                       // alert("{"Please try again later"|translate}.");
                    }
                }).error(function (data) {
                    alert("{"-1 Please try again later"|translate}.");
                });

            },

            eventClick: function(calEvent, jsEvent, view) {   // Triggered when the user clicks an event. http://fullcalendar.io/docs/mouse/eventClick/
                // alert( "select_scheduling_mode::" + select_scheduling_mode + "  calEvent::"+var_dump(calEvent) )
                if ( calEvent.type == 'service' && select_scheduling_mode == "appointments" ) {
                    var onlyProviderId= getOnlyProvider()
                    var multyProvidersId= ''
                    if (onlyProviderId == '') { // only Provider is not selected!
                        multyProvidersId= getMultyProviders()
                    }
                    var onlyLocationId= getOnlyLocation()
                    var multyLocationsId= ''
                    if (onlyLocationId == '') { // only Location is not selected!
                        multyLocationsId= getMultyLocations()
                    }
                    editCalendarScheduleService( calEvent.start_unix, "edit", onlyProviderId, multyProvidersId, onlyLocationId, multyLocationsId, calEvent.appointments_id, calEvent.title )
                }
                if ( calEvent.type == 'class' && select_scheduling_mode == "appointments" ) {
                    editCalendarScheduleClass( calEvent.start_unix, "edit", calEvent.appointments_id, calEvent.title )
                }
                if ( calEvent.type == 'personal_time' && select_scheduling_mode == "appointments" ) {
                    editPersonalTime( calEvent.start_unix, "edit", calEvent.appointments_id, calEvent.title )
                }
            },

            eventMouseover: function(event, jsEvent, view) {
                // alert( "select_scheduling_mode::" + select_scheduling_mode + "eventMouseover event::"+var_dump(event))
                if ( select_scheduling_mode == "resource" ) { // "Classes/Activities calendar"  businessname
                    var hintStr= event.appointments_id+" : ";
                    if ( event.title!= '' && typeof event.title != "undefined" ) {
                        hintStr= hintStr + "<b>{"Title"|translate}: </b>" + event.title + "<br>";
                    }
                    if ( event.slots_busy!= '' && typeof event.slots_busy != "undefined" ) {
                        hintStr= hintStr + "<b>{"Number of slots reserved"|translate}: </b>" + event.slots_busy + "<br>";
                    }
                    if ( event.slots_count!= '' && typeof event.slots_count != "undefined" ) {
                        hintStr= hintStr + "<b>{"Total number of slot"|translate}: </b>" + event.slots_count + "<br>";
                    }

                    if ( event.provider_name!= '' && typeof event.provider_name != "undefined" ) {
                        hintStr= hintStr + "<b>{"Provider"|translate}: </b>"+event.provider_name + "<br>";
                    }

                    if ( event.status!= '' && typeof event.status != "undefined" ) {
                        hintStr= hintStr + "<b>{"Status"|translate}: </b>"+event.status + "<br>";
                    }

                    if ( event.businessname!= '' && typeof event.businessname != "undefined" ) {
                        hintStr= hintStr + "<b>{"Company"|translate}: </b>"+event.businessname + "<br>";
                    }
                    if ( event.type!= '' && typeof event.type != "undefined" ) {
                        hintStr= hintStr + "<b>{"Type"|translate}: </b>"+capitalize(event.type, true) + "<br>";
                    }
                    if ( event.sched_type!= '' && typeof event.sched_type != "undefined" ) {
                        hintStr= hintStr + "<b>{"Schedule"|translate}: </b>"+capitalize(event.sched_type, true) + "<br>";
                    }
                    if ( event.price!= '' && typeof event.price != "undefined" && event.price != null ) {
                        hintStr= hintStr + "<b>{"Price"|translate}: </b>" + event.iso_code + " " + formatMoney(event.price)  + "<br>";
                    }
                    if ( event.is_for_yourself != 1 && $.trim(event.client_name) != "" && event.client_name != null ) {
                        hintStr= hintStr + "<b>{"Scheduling for"|translate}: </b>"+event.client_name + "<br>";
                    }


                    if ( event.travel_time > 0  && event.travel_time != null ) {
                        hintStr= hintStr + "<b>{"Travel Time (mins)"|translate}: </b>"+event.travel_time + "<br>";
                    }
                    if ( event.use_outcall_address == 1  && event.outcall_address != null && $.trim(event.outcall_address) != "" ) {
                        hintStr= hintStr + "<b>{"Outcall Address"|translate}: </b>"+event.outcall_address + "<br>";
                    }


                    var tooltip = '<div class="tooltipevent" style="background:#C0FEFE;position:absolute;z-index:10001; font-size: 11px; border:1px solid #000; padding: 5px;">' + hintStr + "<b>{"Time"|translate}: </b>"+ event.start_label + "-" + event.end_label + '</div>';
                    $("body").append(tooltip);
                    $(this).mouseover(function(e) {
                        $(this).css('z-index', 10000);
                        $('.tooltipevent').fadeIn('500');
                        $('.tooltipevent').fadeTo('10', 1.9);
                    }).mousemove(function(e) {
                        $('.tooltipevent').css('top', e.pageY + 10);
                        $('.tooltipevent').css('left', e.pageX + 20);
                    });
                }
                if ( select_scheduling_mode == "appointments" ) { // "Appointments calendar"
                    var hintStr= event.appointments_id+" : ";
                    if ( event.title!= '' && typeof event.title != "undefined" ) {
                        hintStr= hintStr + "<b>{"Title"|translate}: </b>"+event.title + "<br>";
                    }

                    if ( event.provider_name!= '' && typeof event.provider_name != "undefined" ) {
                        hintStr= hintStr + "<b>{"Provider"|translate}: </b>"+event.provider_name + "<br>";
                    }

                    if ( event.status!= '' && typeof event.status != "undefined" ) {
                        hintStr= hintStr + "<b>{"Status"|translate}: </b>"+event.status + "<br>";
                    }

                    if ( event.confirmation_number!= '' && typeof event.confirmation_number != "undefined" ) {
                        hintStr= hintStr + "<b>{"Confirmation Number"|translate}: </b>"+event.confirmation_number + "<br>";
                    }

                    if ( event.businessname!= '' && typeof event.businessname != "undefined" ) {
                        hintStr= hintStr + "<b>{"Company"|translate}: </b>"+event.businessname + "<br>";
                    }
                    if ( event.type!= '' && typeof event.type != "undefined" ) {
                        hintStr= hintStr + "<b>{"Type"|translate}: </b>"+capitalize(event.type, true) + "<br>";
                    }
                    if ( event.sched_type!= '' && typeof event.sched_type != "undefined" ) {
                        hintStr= hintStr + "<b>{"Schedule"|translate}: </b>"+capitalize(event.sched_type, true) + "<br>";
                    }
                    if ( event.price!= '' && typeof event.price != "undefined" && event.price != null ) {
                        hintStr= hintStr + "<b>{"Price"|translate}: </b>" + event.iso_code + " " + formatMoney(event.price) + "<br>";
                    }
                    if ( event.is_for_yourself != 1 && $.trim(event.client_name) != "" && event.client_name != null ) {
                        hintStr= hintStr + "<b>{"Scheduling for"|translate}: </b>"+event.client_name + "<br>";
                    }

                    if ( event.travel_time > 0  && event.travel_time != null ) {
                        hintStr= hintStr + "<b>{"Travel Time (mins)"|translate}: </b>"+event.travel_time + "<br>";
                    }
                    if ( event.use_outcall_address == 1  && event.outcall_address != null && $.trim(event.outcall_address) != "" ) {
                        hintStr= hintStr + "<b>{"Outcall Address"|translate}: </b>"+event.outcall_address + "<br>";
                    }

                    var tooltip = '<div class="tooltipevent" style="background:#C0FEFE;position:absolute;z-index:10001; font-size: 11px; border:1px solid #000; padding: 5px;">' + hintStr + "<b>{"Time"|translate}: </b>"+ event.start_label + "-" + event.end_label + '</div>';
                    $("body").append(tooltip);
                    $(this).mouseover(function(e) {
                        $(this).css('z-index', 10000);
                        $('.tooltipevent').fadeIn('500');
                        $('.tooltipevent').fadeTo('10', 1.9);
                    }).mousemove(function(e) {
                        $('.tooltipevent').css('top', e.pageY + 10);
                        $('.tooltipevent').css('left', e.pageX + 20);
                    });
                }
            },

            eventMouseout: function(event, jsEvent, view) {
                $(this).css('z-index', 8);
                $('.tooltipevent').remove();
            }
        }).fullCalendar('changeView', agendaType /*'agendaWeek' */);   // $('#calendar').fullCalendar({


        $(window).resize();
    }

    $(window).resize(function(){
        var $h = $(window).height();
        var $cal_top = $('#calendar').offset().top;
        var $ftr = $("#footer").height();
        var $calc = $h - $cal_top - $ftr - 120;
        $('#calendar').fullCalendar('option', 'height', $calc);
    })
    </script>


    <div class="widget widget-body-white" style="border: 0px solid #ff0000; width: 1400px;">
        <div class="widget-head">
            <h4 class="heading glyphicons settings"><i></i> {"Filter your calendar"|translate}</h4>
        </div>

        <div class="widget-body" style="height: 80px; padding-left: 0; padding-top: 0px; padding-bottom: 0px;" >
            <div class="" style=" padding-left: 0; padding-top: 0px; padding-bottom: 10px;">


                <div class="form-group">
                    <div class="col-sm-12">
                        <div class="col-sm-4" style="padding-left: 0; padding-top: 0px; padding-bottom: 10px; ">
                            <select name="select_scheduling_mode" id="select_scheduling_mode" class="form-control" onchange="javascript:return select_scheduling_modeChange();">
                                <option value="appointments">{"Appointments calendar"|translate}</option>
                                <option value="resource">{"Classes/Activities calendar"|translate}</option>
                            </select>
                        </div>

                        <div class="col-sm-8">
                            &nbsp;
                        </div>

                    </div>
                </div>

                <div id= "div_appointment_scheduling" style="border:0px dotted red; padding-top: 0px; padding-bottom: 0px;" >

                    <div class="form-group" >
                        <div class="col-sm-3">
                            <select id="appointments_sched_filter_staffs" name="appointments_sched_filter_staffs" multiple class="populate" style="min-width: 150px;" onchange="appointments_sched_filter_staffsonChange()">
                                <option {if $UserPrefFilter_Staffs == 'all' || $UserPrefFilter_Staffs == ''}selected{/if} value="all">{"View all service providers"|translate}</option>
                                {foreach from=$providersList item=Item}
                                    <option {if $UserPrefFilter_Staffs == $Item->id}selected{/if} value="{$Item->id}">{$Item->firstname} {$Item->lastname}</option>
                                {/foreach}
                            </select>
                        </div>
                        <div class="col-sm-3">
                            <select name="appointments_sched_filter_services" id="appointments_sched_filter_services" multiple class="populate" style="min-width: 150px;" >
                                <option {if $UserPrefFilter_Services == 'all' || $UserPrefFilter_Services == ''}selected{/if} value="all">{"View all services"|translate}</option>
                                {foreach from=$servicesList item=Item}
                                    <option {if $UserPrefFilter_Services == $Item->id}selected{/if} value="{$Item->id}">{$Item->name}</option>
                                {/foreach}
                            </select>
                        </div>

                        <div class="col-sm-3">
                            <select name="appointments_sched_filter_locations" id="appointments_sched_filter_locations" multiple class="populate" style="min-width: 150px;" >
                                <option {if $UserPrefFilter_Locations == 'all' || $UserPrefFilter_Locations == ''}selected{/if} value="all">{"View all locations"|translate}</option>
                                {foreach from=$branchesList item=Item}
                                    <option {if $UserPrefFilter_Locations == $Item->id}selected{/if} value="{$Item->id}">{$Item->Country->title}, {$Item->City->title}, {$Item->address_1}</option>
                                {/foreach}
                                <option value="outcall_locations">{"Outcall locations"|translate}</option>
                            </select>
                        </div>

                        <div class="col-sm-2">
                            <select name="appointments_sched_filter_statuses" id="appointments_sched_filter_statuses" multiple class="populate" style="min-width: 150px;" >
                                <option {if $UserPrefFilter_Statuses == 'all' || $UserPrefFilter_Statuses == ''}selected{/if} value="all">{"View all statuses"|translate}</option>
                                    <option {if $UserPrefFilter_Statuses == 'Confirmed'}selected{/if} value="Confirmed">{"Confirmed"|translate}</option>
                                    <option {if $UserPrefFilter_Statuses == 'Cancelled'}selected{/if} value="Cancelled">{"Cancelled"|translate}</option>
                                    <option {if $UserPrefFilter_Statuses == 'No-show'}selected{/if} value="No-show">{"No-show"|translate}</option>
                                    <option {if $UserPrefFilter_Statuses == 'Completed'}selected{/if} value="Completed">{"Completed"|translate}</option>
                            </select> {*'Confirmed', 'Cancelled', 'No-show', 'Completed'*}
                        </div>

                        <div class="col-sm-1">
                            <button onclick="javascript:runFilter(); return false;" class="btn btn-primary">{"Filter"|translate}</button>
                        </div>
                    </div> <!-- form-group -->
                </div> <!-- div_appointment_scheduling -->


                <div id="div_resource_scheduling" style="border: 0px dotted green; display: none; padding-top: 10px; padding-bottom: 10px;" >
                    <div class="form-group">
                        <div class="col-sm-3">
                            <select name="resource_sched_filter_staffs" id="resource_sched_filter_staffs" multiple class="populate" style="min-width: 150px;" >
                                <option {if $UserPrefFilter_Staffs == 'all' || $UserPrefFilter_Staffs == ''}selected{/if} value="all">{"View all service providers"|translate}</option>
                                {foreach from=$providersList item=Item}
                                    <option {if $UserPrefFilter_Staffs == $Item->id}selected{/if} value="{$Item->id}">{$Item->firstname} {$Item->lastname}</option>
                                {/foreach}
                            </select>
                        </div>

                        <div class="col-sm-5">
                            <select name="resource_sched_filter_locations" id="resource_sched_filter_locations"  multiple class="populate" style="min-width: 150px;" >
                                <option {if $UserPrefFilter_Locations == 'all' || $UserPrefFilter_Locations == ''}selected{/if} value="all">{"View all locations"|translate}</option>
                                {foreach from=$branchesList item=Item}
                                    <option {if $UserPrefFilter_Locations == $Item->id}selected{/if} value="{$Item->id}">{$Item->Country->title}, {$Item->City->title}, {$Item->address_1}</option>
                                {/foreach}
                                <option value="outcall_locations">{"Outcall locations"|translate}</option>
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <button onclick="javascript:runFilter(); return false;" class="btn btn-primary">{"Filter"|translate}</button>
                        </div>
                    </div> <!-- form-group -->
                </div>


            </div>
        </div>
    </div>


    <div class="col-sm-12" style="border: 0px dotted green; width: 1420px;">

        <div class="col-sm-2" style="border: 0px dotted red">
            <div id="datepicker-inline" style="margin-left: -22px"></div>
            {if $CurrentUser->type == Users::USER_TYPE_PROVIDER}
            <span id="span_showAddPersonalTime" style="margin-left: -18px" >
                <button onclick="javascript:showAddPersonalTime(); return false;" class="btn btn-primary">{"Add personal/off time"|translate}</button>&nbsp;&nbsp;&nbsp;
            </span>
            {/if}
        </div>

        <div class="col-sm-10" style="border: 0px dotted blue">

            <div class="widget widget-heading-simple widget-body-white">
                <div class="widget-body">
                    <a href="#" class="sharecalendar">{"Share this calendar with your organizer software/devices"|translate}</a>
                    <span style="border: 0px dotted gray; margin-right: 1px; margin-top: 10px; text-align: right; position: absolute;top: 28px; left: 890px">
                        <button onclick="javascript:loadCalendar('agendaWeek'); return false;" class="btn btn-primary">{"Weekly View"|translate}</button>&nbsp;&nbsp;&nbsp;
                        <button onclick="javascript:loadCalendar('agendaDay'); return false;" class="btn btn-primary">{"Daily View"|translate}</button>
                    </span>

                    <div id="calendar"></div>

                </div>

            </div>
        </div>
    </div>

{/block}
