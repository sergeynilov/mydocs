<?php
class Ajax_scheduler_Controller extends CI_Controller
{
    public  $CurrentUser;

    public function __construct()
    {
        parent::__construct();
        $this->load->model("companies");
        $this->load->model("users");
        $this->load->model("notifications");
        $this->load->model("location");
        $this->load->model("companypolicies");
        $this->load->model("touractivities");
        $this->load->model("branches");
        $this->CurrentUser = $this->users->IsAuthenticated();
        header("Content-Type: application/json; charset=utf-8");
    }

    private function isOnlyProvider($array) {
        if (!is_array($array) ) return false;
        $providersCount = 0;
        foreach( $array as $key=>$provider_id ) {
            if ( $provider_id== 'all' ) return false;
            if ( $providersCount > 0 ) return false;
            $providersCount++;
        }
        return true;
    }

    public function schedulerCalendar()
    {
        $this->load->model("classes");
        $this->load->model("appointments");
        $CCalendar = new BigCalendar();

        $dStart = strtotime($this->input->get("start"));
        $dEnd = strtotime($this->input->get("end"));

        if ($dEnd < $dStart || ($dEnd - $dStart) > 86400 * 90) {
            return false;
        }

        $filterUserIDArray = AppUtils::getArrayPairValues( $this->input->get_post("sched_filter_staffs"), ',', array('all'=>0) );
        $FilterServiceIDArray = AppUtils::getArrayPairValues( $this->input->get_post("sched_filter_services"), ',', array('all'=>0) );
        $FilterLocationIDArray = AppUtils::getArrayPairValues( $this->input->get_post("sched_filter_locations"), ',', array('all'=>0) );
        $FilterStatusArray = AppUtils::getArrayPairValues( $this->input->get_post("sched_filter_statuses"), ',', array('all'=>0) );
        $request_type = $this->input->get_post("request_type");

        if ( $request_type=='resource' ) {
            $ret = $CCalendar->GetEvents($this->input->get_post("start"), $this->input->get_post("end"), $filterUserIDArray, null, null, $request_type, $FilterServiceIDArray, $FilterLocationIDArray, $FilterStatusArray);
        }
        if ( $request_type=='appointments' ) {
            $ret = $CCalendar->GetEvents($this->input->get_post("start"), $this->input->get_post("end"), $filterUserIDArray, null, $this->CurrentUser, $request_type, $FilterServiceIDArray, $FilterLocationIDArray, $FilterStatusArray);
        }
        $isOnlyProvider= $this->isOnlyProvider($filterUserIDArray);
        if (!is_array($ret)) {
            return false;
        }

        $ret2= [];
        foreach ($ret as $Item) {
            if ($Item->type=='service') {
                $className= "fc-event-custom-" . $Item->type . '-' . $Item->status;
                $use_outcall_address= $Item->use_outcall_address;
                if ($Item->use_outcall_address) {
                    $className .= " fc-event-custom-" . $Item->type . '-' . $Item->status . '-use_outcall_address';
                }

            } else {
                $className= "fc-event-custom-" . $Item->type;
            }
            $Event = [
                'travel_time' => $Item->travel_time,
                'start_label' => substr($Item->time_start,0,5),
                'end_label' => AppUtils::timeFormat($Item->time_end,false),
                'start_unix' => strtotime($Item->date_start . " " . $Item->time_start),
                'end_unix' => strtotime($Item->date_end . " " . $Item->time_end),
                'start' => date("c", strtotime($Item->date_start . " " . $Item->time_start)),
                'end' => date("c", strtotime($Item->date_end . " " . $Item->time_end)),
                'is_for_yourself' => $Item->is_for_yourself,
                'other_client_name' => $Item->other_client_name,
                'use_outcall_address' => $Item->use_outcall_address,
                'outcall_address' => $Item->outcall_address,
                'businessname' => $Item->businessname,
                'className' => $className,
            ];
            switch ($Item->type) {
                case BigCalendar::CALENDAR_EVENT_TYPE_CLASS:
                    $Event['id'] = BigCalendar::CALENDAR_EVENT_TYPE_CLASS . '-' . $Item->class_id;
                    $Event['appointments_id'] = $Item->class_id;
                    $Event['class_schedule_dates_id'] = $Item->Slot()->id;
                    $classItem= $Item->Slot()->Schedule()->ClassItem();
                    if ( !empty($classItem) ) {
                        if ( $request_type=='resource' ) { //  2/20 (2= number of slots reserved, 20= total number of slot .
                            $slots_busy = $CCalendar->getSlotsBusy( $Item->Slot()->Info()->slot_id );
                            $slots_count= $Item->Slot()->Info()->slots_count;
                            $Event['title'] = "{$classItem->name} (".($slots_busy . "/" . $slots_count ).")";
                            $Event['slots_busy'] = (string)$slots_busy;
                            $Event['slots_count'] = (string)( empty($slots_count) ? "0" : $slots_count );
                        }
                        if ( $request_type=='appointments' ) {
                            $Event['title'] = "{$classItem->name}";  // ({$Item->Slot()->Info()->slots_busy}/{$slots_count})"; // \n
                        }
                    }
                    $Event['provider_name'] = $Item->provider_name;
                    $Event['type'] = $Item->type;
                    $Event['status'] = $Item->status;
                    $Event['sched_type'] = $Item->Slot()->Info()->sched_type;
                    $Event['price'] = $Item->price;
                    $ItemProvider= $this->users->GetById( $Item->provider_id );
                    $iso_code= '';
                    if ( !empty($ItemProvider) ) {
                        $iso_code= $ItemProvider->Company()->Currency()->iso_code;
                    }
                    $Event['iso_code'] = $iso_code;

                    break;

                case BigCalendar::CALENDAR_EVENT_TYPE_PERSONALTIME:
                    $Event['id'] = BigCalendar::CALENDAR_EVENT_TYPE_PERSONALTIME . '-' . $Item->appointments_id;
                    $Event['appointments_id'] = $Item->appointments_id;
                    if ( !$isOnlyProvider ) {
                        $Event['title'] = "Personal time for ". $Item->provider_name;
                    } else {
                        $Event['title'] = "Personal time";
                    }
                    $Event['type'] = $Item->type;
                    $Event['status'] = $Item->status;
                    $Event['provider_name'] = $Item->provider_name;
                    break;

                case BigCalendar::CALENDAR_EVENT_TYPE_SERVICE:
                    $Event['id'] = BigCalendar::CALENDAR_EVENT_TYPE_SERVICE . '-' . $Item->appointments_id;
                    $Event['appointments_id'] = $Item->appointments_id;
                    $Event['title'] = "{$Item->Service()->name} " . AppUtils::getTranslate("for") . " {$Item->Client()->firstname} {$Item->Client()->lastname}";
                    $Event['type'] = $Item->type;
                    $Event['status'] = $Item->status;
                    $Event['provider_name'] = $Item->provider_name;
                    $Event['price'] = $Item->price;
                    $ItemProvider= $this->users->GetById( $Item->provider_id );
                    $iso_code= '';
                    if ( !empty($ItemProvider) ) {
                        $iso_code= $ItemProvider->Company()->Currency()->iso_code;
                    }
                    $Event['iso_code'] = $iso_code;
                    break;
            }
            $ret2[] = $Event;
        }
        header("Content-Type: application/json; charset=utf-8");
        print json_encode($ret2);
    }




    public function deleteClassAppointment()  //
    {
        $appointments_id= $this->input->post('appointments_id');
        $Appointment= $this->appointments->GetById($appointments_id);
        if ( empty($Appointment) ) {
            print json_encode(["error" => AppUtils::getTranslate("Appointment not found") ." !"]);
            return;
        }
        $this->appointments->delete($Appointment);
        print json_encode(["success" => 1]);
    }   // public function deleteClassAppointment()  //

    public function saveClassAppointment()
    {
        $mode= $this->input->post('mode');
        $appointments_id= $this->input->post('appointments_id');
        $selected_class_id= $this->input->post('selected_class');
        $selected_classes_schedule_id= $this->input->post('selected_classes_schedule');
        $appointment_time_start= $this->input->post('appointment_time_start');
        $appointment_time_end= $this->input->post('appointment_time_end');
        $formatted_datetime= $this->input->post('formatted_datetime');
        $is_for_yourself= ($this->input->post("is_for_yourself") ? true : false);
        $other_client_name= $this->input->post('other_client_name');
        $app_notes= $this->input->post('app_notes');

        $appointment_date = date('Y-m-d', $formatted_datetime);          // 2014-11-21
        $appointment_time = date('H:i:s A', $formatted_datetime);
        $duration_class= $this->input->post('duration_class');

        $SelectedClass = $this->classes->GetById($selected_class_id);
        if ( empty($SelectedClass) ) {
            print json_encode(["error_1" => 1 ]);
            return;
        }

        $SelectedScheduleClass = $this->classes->GetScheduleById($selected_classes_schedule_id);
        $Slot = $this->classes->GetScheduleSlotById($selected_classes_schedule_id);
        if ( empty($SelectedScheduleClass) ) {
            print json_encode(["error" => 1 ]);
            return;
        }

        $selectedClassProvider= $SelectedClass->getProvider(true);
        $personalTimeAppsList = $this->appointments->getAppointmentsListByFilters( array('type'=>'personal_time', 'provider_id'=>$selectedClassProvider->id,
            'appointment_end'=> $appointment_date . ' ' . $appointment_time_end, 'appointment_start'=> $appointment_date . ' ' . $appointment_time_start ) );
        if ( count($personalTimeAppsList) > 0 ) {
            print json_encode(["error" => AppUtils::getTranslate("Provider set selected time as personal time")."!"]);
            return;
        }

        $ScheduleDatesList = $SelectedScheduleClass->GetScheduleDates(true);
        $class_schedule_date_id= '';
        foreach( $ScheduleDatesList as $ScheduleDate ) {
            if ( strtotime($ScheduleDate->date) == strtotime($appointment_date) ) {
                $class_schedule_date_id= $ScheduleDate->id;
            }
        }
        if ( empty($class_schedule_date_id) ) {
            print json_encode(["error" => AppUtils::getTranslate("class schedule date not found : verify selected day and hours")."!"]);
            return;
        }

        if ($mode == 'add') {
            $App = new Appointment();
            $App->type = Appointments::APPOINTMENT_TYPE_CLASS;
            $App->users_id = $this->CurrentUser->id;
            $App->provider_id = $selectedClassProvider->id;
            $App->ipaddress = $_SERVER['REMOTE_ADDR'];
            $App->appointment_start = $appointment_date . ' ' . $appointment_time_start;
            $App->appointment_end = $appointment_date . ' ' . $appointment_time_end;
            $App->price = $SelectedClass->ovr_price;
            $App->is_for_yourself = $is_for_yourself;
            $App->other_client_name = $other_client_name;
            $App->app_notes = $app_notes;
            $App->confirmation_number = $this->appointments->prepareConfirmationNumber();
            $App->created = date("Y-m-d H:i:s");
            $App->updated = date("Y-m-d H:i:s");
        } else {
            $App = $this->appointments->GetById($appointments_id);
        }

        if ($mode == 'add') {
            $appointments_id = '';
            if ($this->db->insert(Appointments::table, $App)) {
                $appointments_id = $this->db->insert_id();
                $Item = new AppointmentsSlotSetsAssocItem();
                $Item->appointments_id = $appointments_id;
                $Item->class_schedule_dates_id = $class_schedule_date_id;
                if ($this->db->insert(AppointmentsSlotSetsAssoc::table, $Item)) {
                }
                print json_encode(["success"=>1, "appointments_id" => $appointments_id]);
                return;
            }
        }

        if ($mode == 'edit') {
            $App->appointment_start = $appointment_date . ' ' . $appointment_time_start;
            $App->appointment_end = $appointment_date . ' ' . $appointment_time_end;
            $App->is_for_yourself = $is_for_yourself;
            $App->other_client_name = $other_client_name;
            $App->app_notes = $app_notes;
            $App->updated = date("Y-m-d H:i:s");
            if ($this->db->update(Appointment::table, $App, array('id' => $appointments_id) )) {
                $AppointmentAssoc = (new AppointmentsSlotSetsAssoc())->GetByAppointment($App);
                if (empty($AppointmentAssoc[0])) {
                    print json_encode(["error" => 1]);
                    return;
                }
                if ($this->db->update(AppointmentsSlotSetsAssoc::table, $AppointmentAssoc[0], array('appointments_id' => $appointments_id))) {
                }
            }
        }
        print json_encode(["success"=>1, "appointments_id" => $appointments_id]);
    }   // public function saveClassAppointment()  //

    public function addCalendarScheduleItemSelector()
    {
        $year= $this->input->post('year');
        $month= $this->input->post('month');
        $day= $this->input->post('day');
        $hour= $this->input->post('hour');
        $minute= $this->input->post('minute');
        $onlyProviderId= $this->input->post('onlyProviderId');
        $multyProvidersId= $this->input->post('multyProvidersId');

        $onlyLocationId= $this->input->post('onlyLocationId');
        $multyLocationsId= $this->input->post('multyLocationsId');
        $formatted_datetime= mktime($hour, $minute, 0, $month, $day, $year);
        $html = $this->load->view("business/new-calendar-schedule-selector-popup.tpl", [ 'CurrentUser' => $this->CurrentUser, 'formatted_datetime'=> $formatted_datetime, 'onlyProviderId'=> $onlyProviderId, 'multyProvidersId'=> $multyProvidersId, 'onlyLocationId'=> $onlyLocationId, 'multyLocationsId'=> $multyLocationsId ], TRUE);
        print json_encode(["html" => $html]);
    }


    public function savePersonalTimeAppointment()
    {
        $mode= $this->input->post('mode');
        $appointments_id= $this->input->post('appointments_id');
        $appointment_start= $this->input->post('appointment_start');
        $appointment_end= $this->input->post('appointment_end');

        $personal_time_app_notes= $this->input->post('personal_time_app_notes');
        $provider_id= $this->input->post('provider_id');

        if ($mode == 'add') {
            $App = new Appointment();
            $App->type = Appointments::CALENDAR_EVENT_TYPE_PERSONALTIME;
            $App->users_id = $this->CurrentUser->id;
            $App->provider_id = $provider_id;//$this->CurrentUser->id;
            $App->ipaddress = $_SERVER['REMOTE_ADDR'];
            $App->status = '';
            $App->appointment_start = $appointment_start;
            $App->appointment_end = $appointment_end; // !!!
            $App->app_notes = $personal_time_app_notes;
            $App->confirmation_number = $this->appointments->prepareConfirmationNumber();
            $App->created = date("Y-m-d H:i:s");
            $App->updated = date("Y-m-d H:i:s");
        } else {
            $App = $this->appointments->GetById($appointments_id);
        }

        if ($mode == 'add') {
            $appointments_id = '';
            if ($this->db->insert(Appointments::table, $App)) {
                $appointments_id = $this->db->insert_id();
                print json_encode(["success"=>1, "appointments_id" => $appointments_id]);
                return;
            }
        }

        if ($mode == 'edit') {
            $App->appointment_start = $appointment_start;
            $App->appointment_end = $appointment_end;
            $App->app_notes = $personal_time_app_notes;
            $App->updated = date("Y-m-d H:i:s");
            if ($this->db->update(Appointment::table, $App, array('id' => $appointments_id) )) {
                print json_encode(["success"=>1, "appointments_id" => $appointments_id]);
                return;
            }
        }
        print json_encode(["success"=>1, "appointments_id" => $appointments_id]);
    }   // public function savePersonalTimeAppointment()  //


    public function updateAppointmentTime() {
        $appointment_type= $this->input->post('appointment_type');
        $appointments_id= $this->input->post('appointments_id');
        $mysql_start= $this->input->post('mysql_start');
        $mysql_end= $this->input->post('mysql_end');
        if ( $appointment_type == 'personal_time' or $appointment_type == 'class' or $appointment_type == 'service' ) {
            $Appointment= $this->appointments->GetById($appointments_id);
            if ( empty($Appointment) ) {
                print json_encode(["error" => AppUtils::getTranslate("Appointment not found") ." !"]);
                return;
            }
            if ($this->db->update(Appointment::table, array( 'appointment_start' => $mysql_start, 'appointment_end' => $mysql_end ), array('id' => $appointments_id) )) {
                print json_encode(["success"=>1, "appointments_id" => $appointments_id]);
                return;
            }
            print json_encode(["error" => AppUtils::getTranslate("Error saving Appointment") . "!" ]);
        }
    }

    public function addTourActivityScheduleTypeSelector()
    {
        $tour_activity_id= $this->input->post("tour_activity_id");
        $html = $this->load->view("business/new-tour-activity-schedule-selector-popup.tpl", ['User' => $this->CurrentUser, 'tour_activity_id'=> $tour_activity_id], TRUE);
        print json_encode(["html" => $html]);
    }

    public function editPersonalTime()
    {
        if ( !Helpers::ControlUserType($this->CurrentUser, [Users::USER_TYPE_PROVIDER], [], true) ) {
            print json_encode(["html" => "<b>Have no access</b>"]);
            return;
        }

        $formatted_datetime= $this->input->post('formatted_datetime');
        $mode= $this->input->post('mode');
        $appointments_id= $this->input->post('appointments_id');
        $onlyProviderId= $this->input->post('onlyProviderId');

        $default_time = '';
        if ( !empty($formatted_datetime) ) {
            $default_time = AppUtils::getTimeFormat($formatted_datetime, 'short');
        }
        $CompanyPolicy = $this->CurrentUser->Company()->Policy();
        $Company = $this->CurrentUser->Company();
        $iso_code= $this->CurrentUser->Company()->Currency()->iso_code;
        $time_end= '';
        $app_notes= '';
        $provider_id= '';
        $duration= '0';
        if ($mode == "edit") {
            $Appointment = $this->appointments->GetById($appointments_id);
            if (!empty($Appointment)) {
                $default_time= AppUtils::getTimeFormat(strtotime($Appointment->appointment_start),'Short');
                $time_end= AppUtils::getTimeFormat(strtotime($Appointment->appointment_end),'Short');
                $app_notes= $Appointment->app_notes;
                $duration= (int) ( ( strtotime($Appointment->appointment_end) - strtotime($Appointment->appointment_start) ) / 60 );
                $provider_id= $Appointment->provider_id;
            }
        }
        $appointment_stepMinute= $this->appointments->getappointment_stepMinute();
        $dateTimePickerFormat= $this->companies->getJSDateFormatbyDateFormatArray($Company->date_format);
        $ProvidersList = $this->users->getUsersListByType( 'provider', array('show_related_services_count'=>0, 'company_id'=> $this->CurrentUser->Company()->id ) );

        $html = $this->load->view("business/edit-calendar-schedule-personal-time-popup.tpl", [ 'iso_code' => $iso_code, 'onlyProviderId'=> $onlyProviderId, 'formatted_datetime'=>$formatted_datetime, 'mode'=> $mode, 'default_time'=> $default_time, 'time_end'=>$time_end, 'CompanyPolicy'=>$CompanyPolicy, 'app_notes'=> $app_notes, 'duration'=>$duration, 'provider_id'=>$provider_id, 'appointments_id'=> $appointments_id, 'appointment_stepMinute'=> $appointment_stepMinute, 'date_format'=> $Company->date_format, 'dateTimePickerFormat' => $dateTimePickerFormat, 'ProvidersList'=> $ProvidersList, 'messageDisappearingTime'=> AppUtils::getParameter('messageDisappearingTime') ], TRUE);
        print json_encode(["html" => $html]);
    } // public function editPersonalTime()

    public function deletePersonalTime()
    {
        $appointments_id= $this->input->post('appointments_id');
        $Appointment= $this->appointments->GetById($appointments_id);
        if ( empty($Appointment) ) {
            print json_encode(["error" => AppUtils::getTranslate("Appointment not found") ." !"]);
            return;
        }
        $this->appointments->delete($Appointment);
        print json_encode(["success" => 1]);
    }   // public function deletePersonalTime()

    public function editCalendarScheduleClass()
    {
        global $args;
        $ClassesList = $this->classes->GetClassesList();
        $formatted_datetime= $this->input->post('formatted_datetime');
        $mode= $this->input->post('mode');
        $appointments_id= $this->input->post('appointments_id');

        $default_time = '';
        if ( !empty($formatted_datetime) ) {
            $default_time = AppUtils::getTimeFormat($formatted_datetime, 'short');
        }
        $CompanyPolicy = $this->CurrentUser->Company()->Policy();
        $iso_code= $this->CurrentUser->Company()->Currency()->iso_code;
        $classes_schedule_id= '';
        $classes_id= '';
        $time_end= '';
        $is_for_yourself= "1";
        $class_name= '';
        $other_client_name= '';
        $app_notes= '';
        if ($mode == "edit") {
            $Appointment = $this->appointments->GetById($appointments_id);
            if (!empty($Appointment)) {
                $default_time= AppUtils::getTimeFormat(strtotime($Appointment->appointment_start),'Short');
                $time_end= AppUtils::getTimeFormat(strtotime($Appointment->appointment_end),'Short');
                $is_for_yourself= $Appointment->is_for_yourself;
                $other_client_name= $Appointment->other_client_name;
                $app_notes= $Appointment->app_notes;
                $AppointmentAssoc = (new AppointmentsSlotSetsAssoc())->GetByAppointment($Appointment);
                if (!empty($AppointmentAssoc[0])) {
                    $Slot = $this->classes->GetScheduleSlotById($AppointmentAssoc[0]->class_schedule_dates_id);
                }
            }
            if (empty($Slot)) {
                $mode= "add";
            } else {
                $classes_schedule_id = $Slot->classes_schedule_id;
                $SlotInfoEx = $Slot->Info();
                $classes_id= $SlotInfoEx->class_id;
                $SelectedClass = $this->classes->GetById($classes_id);
                if( !empty($SelectedClass) ) {
                    $class_name= $SelectedClass->name;
                }
            }
        }
        $default_location_name= '';
        if ($mode=='edit' and !empty($classes_schedule_id)) {
            $classSchedule = $this->classes->GetScheduleById($classes_schedule_id);
            if ( $classSchedule->is_outcall ) {
                $default_location_name= $classSchedule->outcall_class_location;
            } else {
                $classScheduleBranch= $classSchedule->Branch;
                $default_location_name= $classScheduleBranch->City()->title.', '. $classScheduleBranch->address_1 .' '.$classScheduleBranch->address_2;
            }
        }
        $class_stepMinute= $this->classes->getclass_stepMinute();
        $html = $this->load->view("business/edit-calendar-schedule-class-popup.tpl", [ 'iso_code' => $iso_code, 'ClassesList'=>$ClassesList, 'formatted_datetime'=>$formatted_datetime, 'mode'=> $mode, 'classes_id'=> $classes_id, 'class_name'=> $class_name, 'classes_schedule_id'=> $classes_schedule_id, 'default_time'=> $default_time, 'time_end'=>$time_end, 'CompanyPolicy'=>$CompanyPolicy, 'is_for_yourself'=> $is_for_yourself, 'other_client_name'=> $other_client_name, 'app_notes'=> $app_notes, 'appointments_id'=> $appointments_id, 'default_location_name'=> $default_location_name, 'class_stepMinute'=> $class_stepMinute, 'messageDisappearingTime'=> AppUtils::getParameter('messageDisappearingTime') ], TRUE);
        print json_encode(["html" => $html]);
    } // public function editCalendarScheduleClass()

    public function editCalendarScheduleService()
    {
        global  $args;
        if ( !Helpers::ControlUserType($this->CurrentUser, [Users::USER_TYPE_PROVIDER], [], true) ) {
            print json_encode(["html" => "<b>Have no access</b>"]);
            return;
        }

        $formatted_datetime= $this->input->post('formatted_datetime');
        $onlyProviderId= $this->input->post('onlyProviderId');
        $multyProvidersId= $this->input->post('multyProvidersId');
        $onlyLocationId= $this->input->post('onlyLocationId');
        $multyLocationsId= $this->input->post('multyLocationsId');
        $mode= $this->input->post('mode');
        $appointments_id= $this->input->post('appointments_id');

        $onlyProviderCompanyId= '';
        $onlyProviderName= '';
        if ( !empty($onlyProviderId) ) {
            $userProvider= $this->users->GetById($onlyProviderId);
            if ( !empty($userProvider) ) {
                $onlyProviderName= $userProvider->firstname .' '. $userProvider->lastname;
                $onlyProviderCompanyId= $userProvider->company_id;
            }
        }

        $ClientsList = $this->users->getUsersListByType('user');
        $ServicesList = array();
        $ServicesArray = $this->services->GetList(array('filter_company_id'=> $onlyProviderCompanyId, 'filter_branch_id'=>$onlyLocationId));
        foreach( $ServicesArray as $key => $nextService ) {
            $providerName = '';
            $nextProvider = $nextService->getProvider();
            if (!empty($nextProvider[0])) {
                $providerName = $nextProvider[0]->firstname .' '. $nextProvider[0]->lastname;
            }
            $ServicesList[$key] = $nextService;
            $ServicesList[$key]->providerName = $providerName;
            $ServicesList[$key]->relatedCategories = $nextService->Categories();
        }

        $servicesCategorieslist= (new ServiceCategories())->getServiceCategoriesListByFilters(array('show_related_services_categories_count'=>1, 'hide_zero_counts'=>1));
        $servicesListWithoutAssocRefs= $this->services->getServicesListWithoutAssocRefs();
        if ( count($servicesListWithoutAssocRefs) > 0 ) {
            $newServiceCategory= new ServiceCategory();
            $newServiceCategory->id= -1;
            $newServiceCategory->company_id= '';
            $newServiceCategory->title= AppUtils::getTranslate("No category");
            $newServiceCategory->related_services_categories_count= count($servicesListWithoutAssocRefs);
            $servicesCategorieslist[]= $newServiceCategory;
        }

        $ProvidersList= array();
        $ProvidersArray = $this->users->getUsersListByType( 'provider', array('show_related_services_count'=>1) );

        if ( !empty($multyProvidersId) ) {
            $A= AppUtils::getSplittedArray($multyProvidersId,',');
            foreach( $ProvidersArray as $nextProvider ) {
                if ( in_array($nextProvider->id,$A) and $nextProvider->related_services_count > 0 ) {
                    $ProvidersList[]= $nextProvider;
                }
            }
        } else {
            if ( !empty($onlyProviderCompanyId) ) {
                foreach( $ProvidersArray as $nextProvider ) {
                    if ( $nextProvider->id == $onlyProviderCompanyId and $nextProvider->related_services_count > 0) {
                        $ProvidersList[]= $nextProvider;
                    }
                }
            } else {
                //$ProvidersList = $ProvidersArray;
                foreach( $ProvidersArray as $nextProvider ) {
                    if ( $nextProvider->related_services_count > 0) {
                        $ProvidersList[]= $nextProvider;
                    }
                }
            }
        }

        ////////////////////////////////// Location //////////////////////
        $onlyLocationName= '';
        if ( !empty($onlyLocationId) ) {
            $locationObj= $this->branches->GetById($onlyLocationId);
            if ( !empty($locationObj) ) {
                $onlyLocationName= $locationObj->Country()->title .', '. $locationObj->City()->title .', '. $locationObj->address_1;
            }
        }

        $selected_year= strftime("%Y",$formatted_datetime);
        $selected_month= strftime("%m",$formatted_datetime);
        $selected_day= strftime("%d",$formatted_datetime);

        $default_time = '';
        if ( !empty($formatted_datetime) ) {
            $default_time = AppUtils::getTimeFormat($formatted_datetime, 'short');
        }
        $author_name= $this->CurrentUser->firstname . ' ' . $this->CurrentUser->lastname;
        $CompanyPolicy = $this->CurrentUser->Company()->Policy();
        $provider_iso_code= '';
        $provider_id= '';
        $status= '';
        $confirmation_number= '';
        $service_id= '';
        $service_name= '';
        $branch_id= '';
        $location_id= '';
        $available_online= '0';
        $time_end= '';
        $is_for_yourself= "1";
        $client_id= '';
        $client_name= '';
        $client_email= '';
        $client_phone= '';
        $client_address= '';
        $client_reliability_rating= '';
        $clientPicture= '';
        $price= '';
        $travel_time= $CompanyPolicy->app_travel_time;
        $duration= 0;
        $app_notes= '';
        $is_for_yourself= 1;
        $other_client_name= '';
        $selectedService = null;
        $service_is_outcall= '';
        $serviceIsOutcall= '';
        $provider_name= '';
        $location_name= '';
        if ($mode == "edit") {
            $Appointment = $this->appointments->GetById($appointments_id);
            $appointment_at_the_same_day= AppUtils::compareDaysDiff($Appointment->appointment_start, $Appointment->appointment_end, 'mysql');
            $appointmentClient= $Appointment->Client();
            $client_name= $appointmentClient->firstname . ' ' . $appointmentClient->lastname;
            $client_email= $appointmentClient->email;
            $client_phone= $appointmentClient->phone;
            $client_address= $appointmentClient->address;
            $client_reliability_rating= $appointmentClient->reliability_rating;

            $clientPicture= $appointmentClient->Picture();
            $time_end = AppUtils::timeFormat( $Appointment->appointment_end, false );
            $time_start = AppUtils::timeFormat( $Appointment->appointment_start, false );
            $travel_time = $Appointment->travel_time;


            $selectedService = $this->services->GetById($Appointment->service_id);
            $service_id = $Appointment->service_id;
            $branch_id = $Appointment->branch_id;
            $AppointmentBranch= $this->branches->GetById($branch_id);
            if (!empty($AppointmentBranch)) {
                $location_name = $AppointmentBranch->City()->title.', '. $AppointmentBranch->address_1 .' '.$AppointmentBranch->address_2;
            }

            $client_id = $Appointment->client_id;
            $appointmentClient= $this->users->GetById( $Appointment->client_id );
            if ( !empty($appointmentClient) ) {
                $client_name = $appointmentClient->firstname .' '.$appointmentClient->lastname;
            }
            $app_notes= $Appointment->app_notes;
            $is_for_yourself= $Appointment->is_for_yourself;
            $other_client_name= $Appointment->other_client_name;
            $status = $Appointment->status;
            $confirmation_number = $Appointment->confirmation_number;
            $price = $Appointment->price;
            $duration= (int) ( ( strtotime($time_end) - strtotime($time_start) ) / 60 );

            $appointmentProvider = $this->users->GetById( $Appointment->provider_id );
            if (!empty($appointmentProvider)) {
                $provider_name = $appointmentProvider->firstname . ' ' . $appointmentProvider->lastname;
                $provider_id = $appointmentProvider->id;
            }
            if ( !empty($selectedService) ) {
                $service_name= $selectedService->name;
                $available_online= $selectedService->available_online;
                $provider_iso_code= $selectedService->Company()->Currency()->iso_code;
                $servicePicture = $selectedService->Picture();
                $service_is_outcall= $selectedService->is_outcall;
            }
        }
        $Company = $this->CurrentUser->Company();

        $dateTimePickerFormat= $this->companies->getJSDateFormatbyDateFormatArray($Company->date_format);
        $service_stepMinute= $this->services->getservice_stepMinute();
        $paymentGatewayAccessible= 1;
        $payment_name_prefix= 'serv_app_';
        $outcall_address_name_prefix= 'outcall_address_serv_app_';
        $html = $this->load->view("business/edit-calendar-schedule-service-popup.tpl", [ 'provider_iso_code' => $provider_iso_code, 'Appointment'=> $Appointment, 'selectedService'=>$selectedService, 'servicePicture'=>$servicePicture, 'service_is_outcall'=> $service_is_outcall,  'ServicesList'=>$ServicesList, 'servicesCategorieslist'=> $servicesCategorieslist, 'formatted_datetime'=>$formatted_datetime, 'mode'=> $mode, 'default_time'=> $default_time, 'time_end'=>$time_end, 'CompanyPolicy'=>$CompanyPolicy, 'is_for_yourself'=> $is_for_yourself, 'client_name'=> $client_name, 'client_email'=>$client_email, 'client_phone'=> $client_phone, 'client_address'=> $client_address, 'clientPicture'=> $clientPicture, 'client_reliability_rating'=> $client_reliability_rating, 'app_notes'=> $app_notes, 'is_for_yourself'=>$is_for_yourself, 'other_client_name'=>$other_client_name, 'appointments_id'=> $appointments_id, 'location_name'=> $location_name, 'provider_id'=> $provider_id, 'status'=> $status, 'confirmation_number'=>$confirmation_number, 'provider_name'=> $provider_name, 'service_id'=> $service_id, 'service_name'=> $service_name, 'branch_id'=> $branch_id, 'available_online'=>$available_online, 'client_id'=> $client_id, 'service_stepMinute'=> $service_stepMinute, 'ClientsList'=> $ClientsList, 'selected_year'=>$selected_year, 'selected_month'=>$selected_month, 'selected_day'=>$selected_day, 'location_id'=> $location_id, 'onlyProviderId'=> $onlyProviderId, 'onlyProviderName'=> $onlyProviderName, 'travel_time'=>$travel_time,  'duration'=> $duration, 'price'=> $price, 'multyProvidersId'=> $multyProvidersId, 'ProvidersList'=> $ProvidersList,  'onlyLocationId'=> $onlyLocationId, 'onlyLocationName'=> $onlyLocationName, 'multyLocationsId'=> $multyLocationsId, /* 'LocationsList'=> $LocationsList, */ 'provider_id'=> $provider_id,          'JS_DAY_OF_WEEK_FORMAT'=> AppUtils::getFormat('JS_DAY_OF_WEEK_FORMAT'), 'JS_DATETIME_COMMON_FORMAT'=> AppUtils::getFormat('JS_DATETIME_COMMON_FORMAT'), 'JS_DATE_COMMON_FORMAT'=> AppUtils::getFormat('JS_DATE_COMMON_FORMAT'),'JS_TIME_COMMON_FORMAT'=> AppUtils::getFormat('JS_TIME_COMMON_FORMAT'), 'dateTimePickerFormat' => $dateTimePickerFormat, 'date_format'=> $Company->date_format,     'messageDisappearingTime'=> AppUtils::getParameter('messageDisappearingTime'),              'Countries' => (new LocationCountries())->GetAll(), 'paymentGatewayAccessible'=> $paymentGatewayAccessible, 'payment_name_prefix'=> $payment_name_prefix, 'outcall_address_name_prefix'=> $outcall_address_name_prefix, 'appointment_at_the_same_day'=> ( $appointment_at_the_same_day ? 1 : 0 ), 'author_name'=> $author_name
        ], TRUE);
        print json_encode(["html" => $html]);
    } // public function editCalendarScheduleService()   // appointments_id



    public function verifyClientisScheduledAtTime()  //
    { $mode= $this->input->post('mode');
        $appointments_id= $this->input->post('appointments_id');
        $selected_client_id= $this->input->post('selected_client_id');
        $start_date_time= $this->input->post('start_date_time');
        $end_date_time= $this->input->post('end_date_time');
        $App = $this->appointments->getSimilarAppointment( $appointments_id,  $selected_client_id, $start_date_time, $end_date_time );
        if ( empty($App) ) {
            print json_encode(["success" => 1, "appointments_id" => $appointments_id]);
            return;
        }
        print json_encode(["error" => AppUtils::getTranslate("There is Appointment for this client at scheduled time") ."!"]);
    } // public function verifyClientisScheduledAtTime()  //

    public function saveServiceAppointment()
    {
        $mode= $this->input->post('mode');
        $isAddNewClient = (bool)$this->input->get_post("isAddNewClient");

        $appointments_id= $this->input->post('appointments_id');
        $selected_provider_id= $this->input->post('selected_provider_id');
        $selected_service_id= $this->input->post('selected_service_id');
        $appointment_scheduled_online= $this->input->post('appointment_scheduled_online');
        $selected_location_id= $this->input->post('selected_location_id');
        $appointment_date= $this->companies->setTimebyDateFormat( $this->input->post('appointment_date'), $this->CurrentUser, 'mysql' );

        $appointment_time_start= $this->input->post('appointment_time_start');
        $pastDate= (int)$this->input->post('pastDate');

        $appointment_time_end= $this->input->post('appointment_time_end');
        $appointment_travel_time= $this->input->post('appointment_travel_time');

        $appointment_price= $this->input->post('appointment_price');
        $appointment_client_id= $this->input->post('appointment_client_id');
        $is_for_yourself= ($this->input->post("is_for_yourself") ? true : false);
        $app_notes= $this->input->post('app_notes');
        $is_for_yourself= $this->input->post('is_for_yourself');
        $other_client_name= $this->input->post('other_client_name');

        $payment_name_prefix= '';
        $outcall_address_name_prefix= 'client_outcall_';
        $client_use_outcall= $this->input->post('client_use_outcall');
        $client_outcall_address= $this->input->post($outcall_address_name_prefix.'address');
        $client_outcall_city_id= $this->input->post($outcall_address_name_prefix.'city_id');
        $client_outcall_state_id= $this->input->post($outcall_address_name_prefix.'state_id');
        $client_outcall_postal_code= $this->input->post($outcall_address_name_prefix.'postal_code');
        $client_outcall_country_id= $this->input->post($outcall_address_name_prefix.'country_id');

        $payment_action= $this->input->post($payment_name_prefix.'payment_action'); // TODO PAYMENT
        $payment_amount_to_bill= $this->input->post($payment_name_prefix.'payment_amount_to_bill');
        $payment_credit_card= $this->input->post($payment_name_prefix.'payment_credit_card');
        $payment_cardholder_name= $this->input->post($payment_name_prefix.'payment_cardholder_name');
        $payment_card_number= $this->input->post($payment_name_prefix.'payment_card_number');
        $payment_expiry_date_month= $this->input->post($payment_name_prefix.'payment_expiry_date_month');
        $payment_expiry_date_year= $this->input->post($payment_name_prefix.'payment_expiry_date_year');
        $payment_cvv_code= $this->input->post($payment_name_prefix.'payment_cvv_code');

        $notify_service_provider= (bool)$this->input->post('notify_service_provider');
        $notify_client= (bool)$this->input->post('notify_client');
        $SelectedService = $this->services->GetById($selected_service_id);
        if ( empty($SelectedService) ) {
            print json_encode(["error" => AppUtils::getTranslate("Selected Service not found") . "!" ]);
            return;
        }

        $SelectedBranch = $this->branches->GetById($selected_location_id);
        if ( empty($SelectedBranch) ) {
            print json_encode(["error" => AppUtils::getTranslate("Selected Branch not found") . "!" ]);
            return;
        }
        $selectedServiceProvider= $this->users->GetById($selected_provider_id); //$SelectedService->getProvider(true);
        if ( empty($selectedServiceProvider) ) {
            print json_encode(["error" => AppUtils::getTranslate("Selected Service Provider not found") . "!" ]);
            return;
        }

        if ($isAddNewClient) {
            $appointmentClientUser= $this->createUserForAppointment();
            $appointment_client_id = $appointmentClientUser->id;
        } //if ($isAddNewClient) {
        else {
            $appointmentClientUser= $this->users->GetById($appointment_client_id);
        }

        if ($mode == 'add') {
            $this->updateProfileData($appointmentClientUser, $outcall_address_name_prefix);
            $App = new Appointment();
            $App->confirmation_number = $this->appointments->prepareConfirmationNumber();

            $App->provider_id = $selected_provider_id;
            $App->status = 'Confirmed';
            $App->service_id = $selected_service_id;
            $App->scheduled_online = $appointment_scheduled_online;
            $App->branch_id = $selected_location_id;
            $App->type = Appointments::APPOINTMENT_TYPE_SERVICE;
            $App->users_id = $this->CurrentUser->id;
            $App->ipaddress = $_SERVER['REMOTE_ADDR'];
            $App->travel_time = $appointment_travel_time;
            $App->appointment_start = $appointment_date . ' ' . $appointment_time_start;
            $App->appointment_end = $appointment_date . ' ' . $appointment_time_end;
            $App->price = $appointment_price;
            $App->client_id = $appointment_client_id;
            $App->is_for_yourself = $is_for_yourself;
            $App->other_client_name = $other_client_name;
            $App->app_notes = $app_notes;
            $App->created = date("Y-m-d H:i:s");
            $App->updated = date("Y-m-d H:i:s");
            if ( $client_use_outcall ) {
                $App->use_outcall_address = 1;
                $App->outcall_countries_id = ( !empty($client_outcall_country_id) ? $client_outcall_country_id : null );
                $App->outcall_states_id = ( !empty($client_outcall_state_id) ? $client_outcall_state_id : null );
                $App->outcall_cities_id = ( !empty($client_outcall_city_id) ? $client_outcall_city_id : null );
                $App->outcall_postal_code = $client_outcall_postal_code;
                $App->outcall_address = $client_outcall_address;
            }
        } else {
            $App = $this->appointments->GetById($appointments_id);
        }

        if ($mode == 'add') {
            $appointments_id = '';
            if ($this->db->insert(Appointments::table, $App)) {
                $appointments_id = $this->db->insert_id();
                $App= $this->appointments->GetById($appointments_id);
                if ( $pastDate!= 1 and $notify_client ) {
                    $appointmentClient= $this->users->GetById($App->client_id);
                    $this->notifications->Compose(Notifications::TEMPLATE_USER_NEW_APPOINTMENT, $appointmentClient->email,
                        ['AppointmentClient' => $appointmentClient, 'Appointment' => $App, 'Service'=>$SelectedService, 'SelectedServiceProvider'=> $selectedServiceProvider]);
                    Events::Add(Event::EVENT_TYPE_APPOINTMENT_NEW, $App->Provider()->Company(), null, $App);
                    $smsRet= $this->notifications->sendSmsbyAppointment(Notifications::TEMPLATE_USER_NEW_APPOINTMENT, ['smsDestinationUser' => $appointmentClient, 'Appointment' => $App,'Service'=>$SelectedService]);
                }
                if ( $pastDate!= 1 and $notify_service_provider ) {
                    $appointmentClient= $this->users->GetById($App->client_id);
                    $this->notifications->Compose(Notifications::TEMPLATE_USER_NEW_APPOINTMENT_TO_SERVICE_PROVIDER, $selectedServiceProvider->email,
                        ['AppointmentClient' => $appointmentClient, 'Appointment' => $App, 'Service'=>$SelectedService, 'SelectedServiceProvider'=> $selectedServiceProvider]);
                }
                $this->updateClientsNotes($appointmentClientUser);
                print json_encode(["success"=>1, "appointments_id" => $appointments_id]);
                return;
            }
        }

        if ($mode == 'edit') {
            $App= $this->appointments->GetById($appointments_id);
            if ( empty($App) ) {
                print json_encode(["error" => "Appointment not found!" ]);
                return;
            }
            $this->updateProfileData($appointmentClientUser, $outcall_address_name_prefix);
            $App->provider_id = $selected_provider_id;
            $App->service_id = $selected_service_id;
            $App->scheduled_online = $appointment_scheduled_online;
            $App->branch_id = $selected_location_id;
            $App->appointment_start = $appointment_date . ' ' . $appointment_time_start;
            $App->appointment_end = $appointment_date . ' ' . $appointment_time_end;
            $App->price = $appointment_price;
            $App->client_id = $appointment_client_id;
            $App->is_for_yourself = $is_for_yourself;
            $App->other_client_name = $other_client_name;
            $App->app_notes = $app_notes;
            $App->updated = date("Y-m-d H:i:s");
            if ( $client_use_outcall ) {
                $App->use_outcall_address = 1;
                $App->outcall_countries_id = ( !empty($client_outcall_country_id) ? $client_outcall_country_id : null );
                $App->outcall_states_id = ( !empty($client_outcall_state_id) ? $client_outcall_state_id : null );
                $App->outcall_cities_id = ( !empty($client_outcall_city_id) ? $client_outcall_city_id : null );
                $App->outcall_postal_code = $client_outcall_postal_code;
                $App->outcall_address = $client_outcall_address;
            }

            if ($this->db->update(Appointment::table, $App, array('id' => $appointments_id) )) {
                if ( $pastDate!= 1 and $notify_client ) {
                    $appointmentClient= $this->users->GetById($App->client_id);
                    $this->notifications->Compose(Notifications::TEMPLATE_USER_NEW_APPOINTMENT, $appointmentClient->email,
                        ['AppointmentClient' => $appointmentClient, 'Appointment' => $App, 'Service'=>$SelectedService, 'SelectedServiceProvider'=> $selectedServiceProvider]);
                    Events::Add(Event::EVENT_TYPE_APPOINTMENT_NEW, $App->Provider()->Company(), null, $App);
                }
                if ( $pastDate!= 1 and $notify_service_provider ) {
                    $appointmentClient= $this->users->GetById($App->client_id);
                    $this->notifications->Compose(Notifications::TEMPLATE_USER_NEW_APPOINTMENT_TO_SERVICE_PROVIDER, $selectedServiceProvider->email,
                        ['AppointmentClient' => $appointmentClient, 'Appointment' => $App, 'Service'=>$SelectedService, 'SelectedServiceProvider'=> $selectedServiceProvider]);
                }
            }
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();
            }
        }
        print json_encode(["success"=>1, "appointments_id" => $appointments_id]);
    }   // public function saveServiceAppointment()

    private function updateClientsNotes($appointmentClientUser) {
        $clients_notes= $this->input->post('clients_notes');
        if ( !empty($clients_notes) ) {
            $Notes = new UserNotes();
            $Notes->AddNote($appointmentClientUser, $this->CurrentUser, $clients_notes);
        }
    }

    private function updateProfileData($appointmentClientUser, $outcall_address_name_prefix)
    {
        $client_use_outcall= $this->input->post('client_use_outcall');
        $client_outcall_address= $this->input->post($outcall_address_name_prefix.'address');
        $client_outcall_city_id= $this->input->post($outcall_address_name_prefix.'city_id');
        $client_outcall_state_id= $this->input->post($outcall_address_name_prefix.'state_id');
        $client_outcall_postal_code= $this->input->post($outcall_address_name_prefix.'postal_code');
        $client_outcall_country_id= $this->input->post($outcall_address_name_prefix.'country_id');
        $client_use_this_address_for_future= (bool)$this->input->post('client_use_this_address_for_future');
        if ( $client_use_outcall ) {
            $Profile = $appointmentClientUser->Profile();
            $Profile->title = '';
            $Profile->location_address = $client_outcall_address;
            if ($client_outcall_city_id != '') {
                $Profile->location_cities_id = $client_outcall_city_id;
            }
            if ($client_outcall_country_id != '') {
                $Profile->location_countries_id = $client_outcall_country_id;
            }
            if ($client_outcall_state_id != '') {
                $Profile->location_states_id = $client_outcall_state_id;
            }
            $Profile->postal_code = $client_outcall_postal_code;
            $Profile->use_address_as_outcall = ($client_use_this_address_for_future ? 1 : 0);
            $this->users->ProfileData()->Update($Profile);
        } // if ( $client_use_outcall ) {

    }
    private function createUserForAppointment()  {
        $qaFirstname = $this->input->get_post("firstname");
        $qaLastname = $this->input->get_post("lastname");
        $qaEmail = $this->input->get_post("email");
        $qaPhone = $this->input->get_post("phone");
        $qaAddress = $this->input->get_post("address");

        $default_reliability_rating = ClientReliabilityRating::getDefaultReliabilityRating();
        if (!$User = $this->users->GetByEmail($qaEmail)) { // Create new user

            $User = $this->users->register(
                $qaEmail, null, Users::USER_TYPE_USER, [
                'firstname' => $qaFirstname,
                'lastname' => $qaLastname,
                'address' => $qaAddress,
                'phone' => $qaPhone,
                'language_id' => $this->CurrentUser->Company()->Language()->id,
                'allow_sign_in' => 1,
                'reliability_rating' => $default_reliability_rating,
                'company_id' => $this->CurrentUser->Company()->id,
            ], false
            );
            if ($User) {
                $this->users->ClientCompany()->AssociateUserWithCompany($User, $this->CurrentUser->Company());
                $User->SendRegistrationInvitation($this->CurrentUser->Company());
            }
        } else {  // have existing user
            $this->users->ClientCompany()->AssociateUserWithCompany($User, $this->CurrentUser->Company());
        }
        return $User;
    } // private function createUserForAppointment()  {


    public function deleteServiceAppointment()
    {
        $appointments_id= $this->input->post('appointments_id');
        $Appointment= $this->appointments->GetById($appointments_id);
        if ( empty($Appointment) ) {
            print json_encode(["error" => "Appointment not found !"]);
            return;
        }
        $this->appointments->delete($Appointment);
        print json_encode(["success" => 1]);
    }   // public function deleteServiceAppointment()  //

    public function cancelServiceAppointment()
    {
        $appointments_id= $this->input->post('appointments_id');
        $cancel_apply_noshow_notify_client= (bool)$this->input->post('cancel_apply_noshow_notify_client');
        $cancel_apply_noshow_notify_service_provider= (bool)$this->input->post('cancel_apply_noshow_notify_service_provider');
        $Appointment= $this->appointments->GetById($appointments_id);
        if ( empty($Appointment) ) {
            print json_encode(["error" => "Appointment not found !"]);
            return;
        }

        if ($this->db->update(Appointment::table, array( 'status' => 'Cancelled' ), array('id' => $appointments_id) )) {

            $appointmentClient= $Appointment->Client();
            $selectedService = $this->services->GetById($Appointment->service_id);

            $selectedServiceProvider= $this->users->GetById($Appointment->provider_id); //$SelectedService->getProvider(true);
            if ( $cancel_apply_noshow_notify_client ) {
                $this->notifications->Compose(Notifications::TEMPLATE_TO_CLIENT_APPOINTMENT_WAS_CANCELLED, $appointmentClient->email,
                    ['AppointmentClient' => $appointmentClient, 'Appointment' => $Appointment, 'Service'=>$selectedService, 'SelectedServiceProvider'=> $selectedServiceProvider]);
                Events::Add(Event::EVENT_TYPE_APPOINTMENT_CANCEL, $Appointment->Provider()->Company(), null, $Appointment);
                $smsRet= $this->notifications->sendSmsbyAppointment(Notifications::TEMPLATE_TO_CLIENT_APPOINTMENT_WAS_CANCELLED, ['smsDestinationUser' => $appointmentClient, 'Appointment' => $Appointment,'Service'=>$selectedService]);
            }
            if ( $cancel_apply_noshow_notify_service_provider ) {
                $this->notifications->Compose(Notifications::TEMPLATE_TO_SERVICE_PROVIDER_APPOINTMENT_WAS_CANCELLED, $selectedServiceProvider->email,
                    ['AppointmentClient' => $appointmentClient, 'Appointment' => $Appointment, 'Service'=>$selectedService, 'SelectedServiceProvider'=> $selectedServiceProvider]);
            }
            print json_encode(["success"=>1, "appointments_id" => $appointments_id]);
            return;
        }

        print json_encode(["error" => 1]);
    }   // public function cancelServiceAppointment()  //

    public function noshowServiceAppointment()
    {
        $appointments_id= $this->input->post('appointments_id');
        $Appointment= $this->appointments->GetById($appointments_id);
        $cancel_apply_noshow_notify_client= (bool)$this->input->post('cancel_apply_noshow_notify_client');
        $cancel_apply_noshow_notify_service_provider= (bool)$this->input->post('cancel_apply_noshow_notify_service_provider');

        if ( empty($Appointment) ) {
            print json_encode(["error" => "Appointment not found !"]);
            return;
        }
        if ($this->db->update(Appointment::table, array( 'status' => 'No-show' ), array('id' => $appointments_id) )) {

            $appointmentClient= $Appointment->Client();
            $this->users->decreaseReliabilityRating( $appointmentClient );

            $selectedService = $this->services->GetById($Appointment->service_id);

            $selectedServiceProvider= $this->users->GetById($Appointment->provider_id); //$SelectedService->getProvider(true);
            if ( $cancel_apply_noshow_notify_client ) {
                $this->notifications->Compose(Notifications::TEMPLATE_TO_CLIENT_APPOINTMENT_WAS_SET_NO_SHOW, $appointmentClient->email,
                    ['AppointmentClient' => $appointmentClient, 'Appointment' => $Appointment, 'Service'=>$selectedService, 'SelectedServiceProvider'=> $selectedServiceProvider]);
                Events::Add(Event::EVENT_TYPE_APPOINTMENT_WAS_SET_NO_SHOW, $Appointment->Provider()->Company(), null, $Appointment);
                $smsRet= $this->notifications->sendSmsbyAppointment(Notifications::TEMPLATE_TO_CLIENT_APPOINTMENT_WAS_SET_NO_SHOW, ['smsDestinationUser' => $appointmentClient, 'Appointment' => $Appointment,'Service'=>$selectedService]);
            }
            if ( $cancel_apply_noshow_notify_service_provider ) {
                $this->notifications->Compose(Notifications::TEMPLATE_TO_SERVICE_PROVIDER_APPOINTMENT_WAS_SET_NO_SHOW, $selectedServiceProvider->email,
                    ['AppointmentClient' => $appointmentClient, 'Appointment' => $Appointment, 'Service'=>$selectedService, 'SelectedServiceProvider'=> $selectedServiceProvider]);
            }

            print json_encode(["success"=>1, "appointments_id" => $appointments_id]);
            return;
        }
        print json_encode(["success" => 1]);
    }   // public function noshowServiceAppointment()  //


    public function UndoNoshowServiceAppointment()
    {
        $appointments_id= $this->input->post('appointments_id');
        $Appointment= $this->appointments->GetById($appointments_id);
        $cancel_apply_noshow_notify_client= (bool)$this->input->post('cancel_apply_noshow_notify_client');
        $cancel_apply_noshow_notify_service_provider= (bool)$this->input->post('cancel_apply_noshow_notify_service_provider');

        if ( empty($Appointment) ) {
            print json_encode(["error" => "Appointment not found !"]);
            return;
        }
        if ($this->db->update(Appointment::table, array( 'status' => 'Confirmed' ), array('id' => $appointments_id) )) {

            $appointmentClient= $Appointment->Client();
            $this->users->encreaseReliabilityRating( $appointmentClient );
            $selectedService = $this->services->GetById($Appointment->service_id);

            print json_encode(["success"=>1, "appointments_id" => $appointments_id]);
            return;
        }
        print json_encode(["success" => 1]);
    }   // public function UndoNoshowServiceAppointment()  //


    public function getServiceInfoByServiceId()
    {
        $selected_service_id= $this->input->post('selected_service');
        $selected_provider_id= $this->input->post('selected_provider_id');
        $mode= $this->input->post('mode');
        $retArray= [];
        $SelectedService = $this->services->GetById($selected_service_id);
        if ( empty($SelectedService) ) {
            print json_encode(["error" => $retArray, "retArrayLength" => count($retArray) ]);
            return;
        }
        $location_name= '';
        $SelectedBranch= $this->branches->GetByID((int)$SelectedService->branch_id);
        if ( !empty($SelectedBranch) ) {
            $location_name = $SelectedBranch->City()->title.', '. $SelectedBranch->address_1 .' '.$SelectedBranch->address_2;;
        }
        $SelectedServiceArray= array(
            'name'=> $SelectedService->name,
            'description'=> AppUtils::getMemoTextToMultyLine($SelectedService->description),
            'enable_waiting_list'=> $SelectedService->enable_waiting_list,
            'available_online'=> $SelectedService->available_online,
            'available_online_label'=> ( $SelectedService->available_online ? AppUtils::getTranslate("Yes"):AppUtils::getTranslate("No") ),
            'start_time'=> $SelectedService->start_time,
            'start_time_label'=> AppUtils::ShowFormattedDateTime( strtotime($SelectedService->start_time), "Common" ),
            'duration_time'=> $SelectedService->duration_time,
            'end_time'=> $SelectedService->end_time,
            'end_time_label'=> AppUtils::ShowFormattedDateTime( strtotime($SelectedService->end_time), "Common" ),
            'price'=> $SelectedService->price,
            'picture'=> $SelectedService->Picture(),
        );

        $selectedServiceProvider= $this->users->GetById($selected_provider_id); //$SelectedService->getProvider(true);
        $provider_name= '';
        if (!empty($selectedServiceProvider)) {
            $provider_name = $selectedServiceProvider->firstname . ' ' . $selectedServiceProvider->lastname;
        }
        print json_encode( [ 'SelectedService'=> $SelectedServiceArray, 'provider_name'=> $provider_name, 'location_name'=> $location_name ] );
    }   // public function getServiceInfoByServiceId()

    public function getClientInfoByClientId()
    {
        $selected_client_id= $this->input->post('selected_client');
        $mode= $this->input->post('mode');
        $retArray= [];
        $SelectedClient = $this->users->GetById($selected_client_id);
        if ( empty($SelectedClient) ) {
            print json_encode(["error" => $retArray, "retArrayLength" => count($retArray) ]);
            return;
        }
        $selectedClientNotes= $SelectedClient->Notes();
        $use_address_as_outcall= '';
        $location_countries_id= '';
        $location_states_id= '';
        $location_address= '';
        $location_cities_id= '';
        $postal_code= '';
        $SelectedClientProfileData= $SelectedClient->Profile();
        if ( !empty($SelectedClientProfileData) )   {
            $use_address_as_outcall= $SelectedClientProfileData->use_address_as_outcall;
            $location_countries_id= $SelectedClientProfileData->location_countries_id;
            $location_states_id= $SelectedClientProfileData->location_states_id;
            $location_address= $SelectedClientProfileData->location_address;
            $location_cities_id= $SelectedClientProfileData->location_cities_id;
            $postal_code= $SelectedClientProfileData->postal_code;
        }
        $SelectedClientArray= array(
            'client_id'=> $SelectedClient->id,
            'name'=> $SelectedClient->firstname.' '.$SelectedClient->lastname,
            'email'=> $SelectedClient->email,
            'phone'=> $SelectedClient->phone,
            'address'=> $SelectedClient->address,
            'picture'=> $SelectedClient->Picture(),
            'use_address_as_outcall'=> $use_address_as_outcall,
            'location_countries_id'=> $location_countries_id,
            'location_states_id'=> $location_states_id,
            'location_cities_id'=> $location_cities_id,
            'location_address'=> $location_address,
            'postal_code'=> $postal_code,
            'selectedClientNotes'=> $selectedClientNotes,
            'selectedClientNotesCount'=> count($selectedClientNotes),
        );
        print json_encode( [ 'succes'=> 1, 'SelectedClient'=> $SelectedClientArray ] );
    }   // public function getClientInfoByClientId()

    public function readClientsNotesList() {
        $client_id= $this->input->post('client_id');
        $SelectedClient = $this->users->GetById($client_id);
        if ( empty($SelectedClient) ) {
            print AppUtils::getTranslate("Selected client not found")."!";
            return;
        }
        $selectedClientNotes= $SelectedClient->Notes();
        $html = $this->load->view("business/clients-notes-list.tpl", [ 'CurrentUser' => $this->CurrentUser, 'selectedClientNotes'=> $selectedClientNotes ], TRUE);
        print $html;
    }
}

