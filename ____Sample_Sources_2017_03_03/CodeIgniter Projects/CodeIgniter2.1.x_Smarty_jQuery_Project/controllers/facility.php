<?php

class Facility extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library("AppAuth");
	}

	public function index()
	{
		AppAuth::IsLogged($this, array('Admin'));
		$this->load->model('mfacility', '', true);
		$this->load->helper('text');
		$UriArray = $this->uri->uri_to_assoc(4);
		$editor_message = $this->session->flashdata('editor_message');
		$post_array = $this->input->post();

		$page = AppUtils::getParameter($this, $UriArray, $post_array, 'page', 1);
		$filter_name = AppUtils::getParameter($this, $UriArray, $post_array, 'filter_name');
		$sorting = AppUtils::getParameter($this, $UriArray, $post_array, 'sorting');
		$sort = AppUtils::getParameter($this, $UriArray, $post_array, 'sort');
		$PageParametersWithSort = $this->PreparePageParameters($UriArray, $post_array, true, true);
		$PageParametersWithoutSort = $this->PreparePageParameters($UriArray, $post_array, true, false);

		$this->load->library('pagination');
		$config_array = $this->config->config;
		$config_array['base_url'] = $this->config->base_url() . 'admin/facility/index/page/';
		$RowsInTable = $this->mfacility->getFacilitysList(true, $page, $filter_name, $sorting, $sort);
		$config_array['total_rows'] = $RowsInTable;
		$this->pagination->initialize($config_array);
		$FacilitysList = array();
		if ($RowsInTable > 0) {
			$FacilitysList = $this->mfacility->getFacilitysList(false, $page, $filter_name, $sorting, $sort, '', false);
		}

		$this->pagination->suffix = $this->PreparePageParameters($UriArray, $post_array, false, true);
		$pagination_links = $this->pagination->create_links();
		$data = array('FacilitysList' => $FacilitysList, 'RowsInTable' => $RowsInTable, 'page' => $page, 'PageParametersWithSort' => $PageParametersWithSort, 'PageParametersWithoutSort' => $PageParametersWithoutSort,
			'filter_name' => $filter_name, 'sort' => $sort, 'sorting' => $sorting, 'pagination_links' => $pagination_links, 'editor_message' => $editor_message,
			'ControllerRef' => $this);
		$data = $this->appsmarty->AddSystemParameters($data, $this, $this->config, true);
		// AppUtils::deb( $data, '$data::');
		$this->appsmarty->RunSmartyTemplate('admin/facilitys_list.tpl', $data, $this->config, true, true, true);
	}

	public function edit($id = 0)
	{
		AppAuth::IsLogged($this, array('Admin'));
		$post_array = $this->input->post();
		$IsInsert = empty($id);
		$editor_message = $this->session->flashdata('editor_message');
		$this->edit_load($this);
		$config_array = $this->config->config;

		$config_array['base_url'] = $this->config->base_url() . 'admin/hostel/index/page/';
		$LoggedUserData = AppAuth::getLoggedUserData($this);
		$logged_user_id = $LoggedUserData['logged_user_id'];


		$UriArray = $this->uri->uri_to_assoc(5);
		$PageParametersWithSort = $this->PreparePageParameters($UriArray, $post_array, true, true);
		$Facility = null;
		$PageParametersWithSort = $this->PreparePageParameters($UriArray, null, true, true);
		$this->edit_form_validation($this, $IsInsert, $id);

		$RedirectUrl = $this->config->base_url() . 'admin/facility/index' . $PageParametersWithSort;
		if (!$IsInsert) {
			$Facility = $this->mfacility->getRowById($id);
			if (empty($Facility)) {
				$this->session->set_flashdata('editor_message', "facility '" . $id . "' not found");
				redirect($RedirectUrl);
				return;
			}
		}
		$form_status = 'edit';

		if (!empty($_POST)) { // form was submitted
			$IsInsert = empty($post_array['id']);
			$is_reopen = $this->input->post('is_reopen');
			$validation_status = $this->form_validation->run();
			if ($validation_status != FALSE) {
				if (!$IsInsert) { // verify if in edit status Row was changed.
					$WasFieldChanged = AppUtils::WasFieldChanged($Facility, $this->input->post(), array('id', 'created_at'));
					if ($WasFieldChanged == ''
					) {
						if ($is_reopen)
							$RedirectUrl = $this->config->base_url() . 'admin/facility/edit/' . $post_array['id'] . $PageParametersWithSort;
						redirect($RedirectUrl);
						return;
					}
				}
				$this->db->trans_begin();
				$OkResult = $this->mfacility->UpdateFacility($this->input->post('id'), Array(
					'name' => $this->input->post('name'), 'descr' => $this->input->post('descr')));
				if ($is_reopen) {
					$RedirectUrl = $this->config->base_url() . 'admin/facility/edit/' . $OkResult . $PageParametersWithSort;
				}
				if ($OkResult) {
					$this->session->set_flashdata('editor_message', "Facility '" . $this->input->post('name') . "' was " . ($IsInsert ? "inserted" : "updated"));
					if ($this->db->trans_status() === FALSE) {
						$this->db->trans_rollback();
					} else {
						$this->db->trans_commit();
					}
					redirect($RedirectUrl);
					return;
				}
			} else {
				$form_status = 'invalid';
				$Facility['id'] = $id;
				$Facility['name'] = set_value('name');
				$Facility['descr'] = set_value('descr');
			}
		} // if (!empty($_POST)) { // form was submitted
		$data = array('Facility' => AppUtils::SaveFormHTML($Facility), 'id' => $id, 'IsInsert' => $IsInsert, 'PageParametersWithSort' => $PageParametersWithSort, 'form_status' => $form_status,
			'editor_message' => $editor_message, 'validation_errors_text' => validation_errors($config_array['backend_error_icon_start'], $config_array['backend_error_icon_end']),
			'ControllerRef' => $this);
		$data = $this->appsmarty->AddSystemParameters($data, $this, $this->config, true);
		$this->appsmarty->RunSmartyTemplate('admin/facility_edit.tpl', $data, $this->config, true, true, true);
	}

	private function edit_load($ControllerRef)
	{
		$ControllerRef->load->model('mfacility', '', true);
		$ControllerRef->load->library('form_validation');
	}

	private function edit_form_validation($ControllerRef, $IsInsert, $id)
	{
		if ($IsInsert) {
			$ControllerRef->form_validation->set_rules('name', 'Name', 'required|is_unique[facility.name]');
		} else {
			$ControllerRef->form_validation->set_rules('name', 'Name', 'required|is_unique[facility.name.id.' . $id . ']');
		}
		$ControllerRef->form_validation->set_rules('descr', 'Description', 'required');
	}

	public function delete($id = 0)
	{
		AppAuth::IsLogged($this, array('Admin'));
		$this->load->model('mfacility', '', true);
		$this->load->model('mhostel_facility', '', true);

		$config_array = $this->config->config;
		if (!empty($id)) {
			$UriArray = $this->uri->uri_to_assoc(5);
			$PageParametersWithSort = $this->PreparePageParameters($UriArray, null, false, true);
			$RedirectUrl = $this->config->base_url() . 'admin/facility/index' . $PageParametersWithSort;

			$Facility = $this->mfacility->getRowById($id);
			if (empty($Facility)) {
				$this->session->set_flashdata('editor_message', "facility '" . $id . "' not found");
				redirect($RedirectUrl);
				return;
			}
			$FacilityName = $Facility['name'];

			$Hostel_FacilitysCount = $this->mhostel_facility->getHostel_FacilitysList(true, '', $id, '', '', '', '');
			if ($Hostel_FacilitysCount > 0) {
				$this->session->set_flashdata('editor_message', "Facility '" . $FacilityName . "' can not be deleted, as " . $Hostel_FacilitysCount .
					' Hostel Facilit' . ($Hostel_FacilitysCount > 1 ? 'ies' : 'y') . ' use this Facility.');
				redirect($RedirectUrl);
				return;
			}

			$this->db->trans_begin();
			$OkResult = $this->mfacility->DeleteFacility($id);
			if ($this->db->trans_status() === FALSE) {
				$this->db->trans_rollback();
			} else {
				$this->session->set_flashdata('editor_message', "Facility '" . $FacilityName . "' was deleted");
				$this->db->trans_commit();
			}
			if ($OkResult) {
				$this->session->set_flashdata('editor_message', "Facility '" . $FacilityName . "' was deleted");
				redirect($RedirectUrl);
				return;
			}
		}
	}

	private function PreparePageParameters($UriArray, $_post_array, $WithPage, $WithSort)
	{
		return '';
		$ResStr = '';
		if (!empty($_post_array)) { // form was submitted
			if ($WithPage) {
				$page = $this->input->post('page');
				$ResStr .= !empty($page) ? 'page/' . $page . '/' : 'page/1/';
			}
			$filter_name = $this->input->post('filter_name');
			$ResStr .= !empty($filter_name) ? 'filter_name/' . $filter_name . '/' : '';
			$filter_user_role = $this->input->post('filter_user_role');
			$ResStr .= !empty($filter_user_role) ? 'filter_user_role/' . $filter_user_role . '/' : '';
			if ($WithSort) {
				$sorting = $this->input->post('sorting');
				$ResStr .= !empty($sorting) ? 'sorting/' . $sorting . '/' : '';
				$sort = $this->input->post('sort');
				$ResStr .= !empty($sort) ? 'sort/' . $sort . '/' : '';
			}
		} else {
			if ($WithPage) {
				$ResStr .= !empty($UriArray['page']) ? 'page/' . $UriArray['page'] . '/' : 'page/1/';
			}
			$ResStr .= !empty($UriArray['filter_name']) ? 'filter_name/' . $UriArray['filter_name'] . '/' : '';
			$ResStr .= !empty($UriArray['filter_user_role']) ? 'filter_user_role/' . $UriArray['filter_user_role'] . '/' : '';
			if ($WithSort) {
				$ResStr .= !empty($UriArray['sorting']) ? 'sorting/' . $UriArray['sorting'] . '/' : '';
				$ResStr .= !empty($UriArray['sort']) ? 'sort/' . $UriArray['sort'] . '/' : '';
			}
		}
		if (substr($ResStr, strlen($ResStr) - 1, 1) == '/') {
			$ResStr = substr($ResStr, 0, strlen($ResStr) - 1);
		}
		return '/' . $ResStr;
	}

}

