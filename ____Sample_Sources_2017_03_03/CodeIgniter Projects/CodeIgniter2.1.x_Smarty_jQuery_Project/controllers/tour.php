<?php

class Tour extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library("AppAuth");
	}

	public function index()
	{
		AppAuth::IsLogged($this, array('Admin', 'Operator'));
		$this->load->model('muser', '', true);
		$this->load->model('mtour', '', true);
		$this->load->model('mregion', '', true);
		$this->load->model('mstate', '', true);
		$UriArray = $this->uri->uri_to_assoc(4);
		if (!empty($UriArray['clearsteps']) and (int)$UriArray['clearsteps'] == 1) {
			$this->ClearStepsData();
		}
		$editor_message = $this->session->flashdata('editor_message');
		$post_array = $this->input->post();

		$page = AppUtils::getParameter($this, $UriArray, $post_array, 'page', 1);
		$filter_login = AppUtils::getParameter($this, $UriArray, $post_array, 'filter_login');
		$filter_feature = '';
		$filter_status = '';
		$sorting = AppUtils::getParameter($this, $UriArray, $post_array, 'sorting');
		$sort = AppUtils::getParameter($this, $UriArray, $post_array, 'sort');
		$PageParametersWithSort = $this->PreparePageParameters($UriArray, $post_array, true, true);
		$PageParametersWithoutSort = $this->PreparePageParameters($UriArray, $post_array, true, false);

		$this->load->library('pagination');
		$config_array = $this->config->config;
		$config_array['base_url'] = $this->config->base_url() . 'admin/tour/index/page/';
		$LoggedUserData = AppAuth::getLoggedUserData($this);
		$admin_watch_operator_id = -1;
		if (!empty($LoggedUserData['user_role']) and $LoggedUserData['user_role'] == 'Admin') {
			if (!empty($LoggedUserData['admin_watch_operator_id'])) {
				$admin_watch_operator_id = $LoggedUserData['admin_watch_operator_id'];
			}
			$logged_user_id = $LoggedUserData['logged_user_id'];
		}
		if (!empty($LoggedUserData['user_role']) and $LoggedUserData['user_role'] == 'Operator') {
			$admin_watch_operator_id = $LoggedUserData['logged_user_id'];
			$logged_user_id = $LoggedUserData['logged_user_id'];
		}

		$RowsInTable = $this->mtour->getToursList(true, $page, array('filter_user_id' => $admin_watch_operator_id, 'filter_feature' => $filter_feature, 'filter_status' => $filter_status), $sorting, $sort);
		$config_array['total_rows'] = $RowsInTable;
		$this->pagination->initialize($config_array);
		$ToursList = array();
		if ($RowsInTable > 0) {
			$ToursList = $this->mtour->getToursList(false, $page, array('filter_user_id' => $admin_watch_operator_id, 'filter_feature' => $filter_feature, 'filter_status' => $filter_status), $sorting, $sort);
		}
		$this->pagination->suffix = $this->PreparePageParameters($UriArray, $post_array, false, true);
		$pagination_links = $this->pagination->create_links();
		$data = array('ToursList' => $ToursList, 'RowsInTable' => $RowsInTable, 'page' => $page, 'PageParametersWithSort' => $PageParametersWithSort, 'PageParametersWithoutSort' => $PageParametersWithoutSort,
			'logged_user_id' => $logged_user_id, 'admin_watch_operator_id' => $admin_watch_operator_id, 'filter_login' => $filter_login, 'filter_feature' => $filter_feature, 'sort' => $sort, 'sorting' => $sorting, 'pagination_links' => $pagination_links, 'editor_message' => $editor_message,
			'ControllerRef' => $this);
		$data = $this->appsmarty->AddSystemParameters($data, $this, $this->config, true);
		$this->appsmarty->RunSmartyTemplate('admin/tours_list.tpl', $data, $this->config, true, true, true);
	}


	public function tourstep1($id = 0)
	{
		AppAuth::IsLogged($this, array('Admin', 'Operator'));
		$this->load->helper('file');
		$this->load->model('muser', '', true);
		$this->load->model('mtour', '', true);
		$this->load->model('mregion', '', true);
		$this->load->model('msubregion', '', true);
		$this->load->model('mstate', '', true);
		$this->load->library('form_validation');

		$post_array = $this->input->post();
		$IsInsert = empty($id);
		$UriArray = $this->uri->uri_to_assoc(3);
		if ($IsInsert) {
			$this->form_validation->set_rules('name', 'Name', 'required|is_unique[tour.name]');
		} else {
			$this->form_validation->set_rules('name', 'Name', 'required|is_unique[tour.name.id.' . $id . ']');
		}
		$this->form_validation->set_rules('region_id', 'Region', 'required');
		$this->form_validation->set_rules('subregion_id', 'Area', 'required');
		$this->form_validation->set_rules('state_id', 'State', 'required');
		$this->form_validation->set_rules('slug', 'Slug', '');

		$TourStatusValueArray = AppUtils::SetArrayHeader(array(array('key' => '', 'value' => ' -Select Status- ')), $this->mtour->getTourStatusValueArray());
		$RegionValueArray = AppUtils::SetArrayHeader(array(array('key' => '', 'value' => ' -Select Region- ')), $this->mregion->getRegionsSelectionList());
		$SubregionValueArray = AppUtils::SetArrayHeader(array(array('key' => '', 'value' => ' -Select Area- ')), $this->msubregion->getSubregionsSelectionList());
		$StateValueArray = AppUtils::SetArrayHeader(array(array('key' => '', 'value' => ' -Select State- ')), $this->mstate->getStatesSelectionList());
		$TourFeatureValueArray = AppUtils::SetArrayHeader(array(array('key' => '', 'value' => ' -Select Package- ')), $this->mtour->getTourFeatureValueArray());
		session_start();

		$editor_message = $this->session->flashdata('editor_message');
		$config_array = $this->config->config;
		$PageParametersWithSort = $this->PreparePageParameters($UriArray, null, true, true);
		$RedirectUrl = $this->config->base_url() . 'admin/tour/index' . $PageParametersWithSort;
		$Tour = null;
		$form_status = 'edit';
		if (!empty($_POST)) { // form was submitted
			$IsInsert = empty($post_array['id']);
			$validation_status = $this->form_validation->run();
			if ($validation_status != FALSE) { //
				$unique_session_id = $this->session->userdata['session_id'];
				$DataArray = array('name' => $post_array['name'], 'state_id' => $post_array['state_id'],
					'region_id' => $post_array['region_id'], 'subregion_id' => $post_array['subregion_id']);
				if (empty($DataArray['tmp_tour_dir_path'])) {
					$tmp_tour_dir_path = $config_array['document_root'] . $config_array['images_dir'] . 'tmp/-tmp_tour-' . $unique_session_id . DIRECTORY_SEPARATOR;
					if (!is_dir($tmp_tour_dir_path)) {
						mkdir($tmp_tour_dir_path, 0777);
					}
					$DataArray['unique_session_id'] = $unique_session_id;
					$DataArray['tmp_tour_dir_path'] = $tmp_tour_dir_path;
				}
				$this->session->set_userdata(array("current_tour_step_1" => $DataArray));
				$RedirectUrl = $this->config->base_url() . 'admin/tour/tourstep2';
				redirect($RedirectUrl);
			} else {
				$form_status = 'invalid';
				$Tour['id'] = $id;
				$Tour['uid'] = set_value('uid');
				$Tour['status'] = set_value('status');
				$Tour['region_id'] = set_value('region_id');
				$Tour['subregion_id'] = set_value('subregion_id');
				$Tour['state_id'] = set_value('state_id');
				$Tour['name'] = set_value('name');
				$Tour['slug'] = set_value('slug');

			}
		} // if (!empty($_POST)) { // form was submitted
		else {
			$current_tour_step_1 = $this->session->userdata('current_tour_step_1');
			$Tour['id'] = (!empty($current_tour_step_1['id']) ? $current_tour_step_1['id'] : "");
			$Tour['uid'] = (!empty($current_tour_step_1['uid']) ? $current_tour_step_1['uid'] : "");
			$Tour['status'] = (!empty($current_tour_step_1['status']) ? $current_tour_step_1['status'] : "");
			$Tour['region_id'] = (!empty($current_tour_step_1['region_id']) ? $current_tour_step_1['region_id'] : "");
			$Tour['subregion_id'] = (!empty($current_tour_step_1['subregion_id']) ? $current_tour_step_1['subregion_id'] : "");
			$Tour['state_id'] = (!empty($current_tour_step_1['state_id']) ? $current_tour_step_1['state_id'] : "");
			$Tour['name'] = (!empty($current_tour_step_1['name']) ? $current_tour_step_1['name'] : "");
			$Tour['slug'] = (!empty($current_tour_step_1['slug']) ? $current_tour_step_1['slug'] : "");
		}
		$data = array('Tour' => AppUtils::SaveFormHTML($Tour), 'id' => $id, 'IsInsert' => $IsInsert, 'PageParametersWithSort' => $PageParametersWithSort, 'form_status' => $form_status,
			'editor_message' => $editor_message, 'TourStatusValueArray' => $TourStatusValueArray, 'SubregionValueArray' => $SubregionValueArray,
			'RegionValueArray' => $RegionValueArray, 'StateValueArray' => $StateValueArray,
			'TourFeatureValueArray' => $TourFeatureValueArray, 'validation_errors_text' => validation_errors($config_array['backend_error_icon_start'], $config_array['backend_error_icon_end']),
			'ControllerRef' => $this);
		$data = $this->appsmarty->AddSystemParameters($data, $this, $this->config, true);
		$this->appsmarty->RunSmartyTemplate('admin/tourstep1.tpl', $data, $this->config, true, true, true);
	} // public function tourstep1($id = 0) {  // local-backpackers.com/admin/tour/tourstep1


	public function tourstep2($id = 0)
	{
		AppAuth::IsLogged($this, array('Admin', 'Operator'));
		$this->load->helper('file');
		$this->load->model('muser', '', true);
		$this->load->model('mtour', '', true);
		$this->load->model('mpricing', '', true);
		$this->load->library('form_validation');
		$post_array = $this->input->post();
		$IsInsert = empty($id);
		$UriArray = $this->uri->uri_to_assoc(3);
		$this->form_validation->set_rules('feature', 'Package', 'required');
		$TourFeatureValueArray = AppUtils::SetArrayHeader(array(array('key' => '', 'value' => ' -Select Package- ')), $this->mtour->getTourFeatureValueArray());
		$current_tour_step_1 = $this->session->userdata('current_tour_step_1');
		if ( empty($current_tour_step_1['unique_session_id']) ) {
			redirect( $this->config->base_url() . 'admin/tour/tourstep1' );
		}

		$editor_message = $this->session->flashdata('editor_message');
		$config_array = $this->config->config;
		$PageParametersWithSort = $this->PreparePageParameters($UriArray, null, true, true);
		$RedirectUrl = $this->config->base_url() . 'admin/tour/index' . $PageParametersWithSort;
		$Tour = null;
		$form_status = 'edit';
		if (!empty($_POST)) { // form was submitted
			$IsInsert = empty($post_array['id']);
			$validation_status = $this->form_validation->run();
			if ($validation_status != FALSE) { //
				$DataArray = array('feature' => $post_array['feature']);
				$this->session->set_userdata(array("current_tour_step_2" => $DataArray));
				$RedirectUrl = $this->config->base_url() . 'admin/tour/tourstep3';
				redirect($RedirectUrl);
			} else {
				$form_status = 'invalid';
				$Tour['feature'] = set_value('feature');
			}
		} // if (!empty($_POST)) { // form was submitted
		else {
			$current_tour_step_2 = $this->session->userdata('current_tour_step_2');
			$Tour['feature'] = (!empty($current_tour_step_2['feature']) ? $current_tour_step_2['feature'] : "");
		}
		$Pricing = $this->mpricing->getRow();
		if (!empty($Pricing)) {
			$amount_per_month_standard = $Pricing['tour_standard'];
			$amount_per_month_featured = $Pricing['tour_featured'];
		}

		$data = array('Tour' => AppUtils::SaveFormHTML($Tour), 'id' => $id, 'IsInsert' => $IsInsert, 'PageParametersWithSort' => $PageParametersWithSort, 'form_status' => $form_status,
			'editor_message' => $editor_message,
			'amount_per_month_standard' => $amount_per_month_standard, 'amount_per_month_featured' => $amount_per_month_featured,
			'TourFeatureValueArray' => $TourFeatureValueArray, 'validation_errors_text' => validation_errors($config_array['backend_error_icon_start'], $config_array['backend_error_icon_end']));

		$data = $this->appsmarty->AddSystemParameters($data, $this, $this->config, true);
		$this->appsmarty->RunSmartyTemplate('admin/tourstep2.tpl', $data, $this->config, true, true, true);
	} // public function tourstep2($id = 0) {  // local-backpackers.com/admin/tour/tourstep2


	private function ClearStepsData()
	{
		$this->load->model('msession_data', '', true);
		$temp_current_hostel_step_3 = $this->session->userdata('current_hostel_step_3');
		if (!empty($temp_current_hostel_step_3['unique_session_id'])) {
			$unique_session_id = $temp_current_hostel_step_3['unique_session_id'];
			$this->msession_data->DeleteSession_DataBySession_id($unique_session_id);
		}

		$this->session->set_userdata(array("current_tour_step_1" => Array()));
		$this->session->set_userdata(array("current_tour_step_2" => Array()));
		$this->session->set_userdata(array("current_tour_step_3" => Array()));
		$this->session->set_userdata(array("current_tour_step_4" => Array()));
		$this->session->set_userdata(array("current_tour_step_5" => Array()));
	}
	private function get_tour_step_3_data() {
		$this->load->model('msession_data', '', true);
		$temp_current_tour_step_3 = $this->session->userdata('current_tour_step_3');
		$current_tour_step_3= '';
		if ( !empty($temp_current_tour_step_3['unique_session_id']) ) {
			$unique_session_id= $temp_current_tour_step_3['unique_session_id'];
			$session_data= $this->msession_data->getRowBySessionId( $unique_session_id );
			if ( !empty($session_data['session_data']) ) {
				$current_tour_step_3= unserialize( $session_data['session_data'] ) ;
			}
			return $current_tour_step_3;
		}
	}


	public function tourstep3($id = 0)
	{
		AppAuth::IsLogged($this, array('Admin', 'Operator'));
		$this->load->helper('file');
		$this->load->model('muser', '', true);
		$this->load->model('mtour', '', true);
		$this->load->model('mcategory', '', true);
		$this->load->model('msession_data', '', true);
		$this->load->library('form_validation');
		$config_array = $this->config->config;
		$post_array = $this->input->post();
		$IsInsert = empty($id);
		$current_tour_step_1 = $this->session->userdata('current_tour_step_1');
		if ( empty($current_tour_step_1['unique_session_id']) ) {
			redirect( $this->config->base_url() . 'admin/tour/tourstep1' );
		}
		$current_tour_step_2 = $this->session->userdata('current_tour_step_2');
		$current_feature = 'S';
		if (!empty($current_tour_step_2['feature'])) $current_feature = $current_tour_step_2['feature'];

		$UriArray = $this->uri->uri_to_assoc(3);
		if ($IsInsert) {
			$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[tour.email]');
		} else {
			$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[tour.email.id.' . $id . ']');
		}
		$this->form_validation->set_rules('category_id', 'Category', 'required');
		$this->form_validation->set_rules('price', 'Price', 'required|numeric');

		$this->form_validation->set_rules('short_descr', 'Short Description', 'required');
		$this->form_validation->set_rules('descr', 'Description', 'required|callback_verify_max_words_descr');
		$this->form_validation->set_rules('website', 'Website', ($current_feature == 'F' ? 'required' : ''));
		$this->form_validation->set_rules('about_us', 'About Us', 'callback_verify_max_words_about_us');
		$this->form_validation->set_rules('schedule_pricing', 'Schedule Pricing', 'callback_verify_max_words_schedule_pricing');
		$this->form_validation->set_rules('additional_info', 'Additional Info', 'callback_verify_max_words_additional_info');
		$CategoryValueArray = AppUtils::SetArrayHeader(array(array('key' => '', 'value' => ' -Select Category- ')), $this->mcategory->getCategorysSelectionList());

		$editor_message = $this->session->flashdata('editor_message');
		$PageParametersWithSort = $this->PreparePageParameters($UriArray, null, true, true);
		$Tour = null;
		$form_status = 'edit';
		if (!empty($_POST)) { // form was submitted
			$IsInsert = empty($post_array['id']);
			$validation_status = $this->form_validation->run();
			if ($validation_status != FALSE) { //
				$current_tour_step_3= $this->get_tour_step_3_data();
				$highlights_list = array();
				if (!empty($current_tour_step_3['highlights_list']) and is_array($current_tour_step_3['highlights_list'])) {
					$highlights_list = $current_tour_step_3['highlights_list'];
				}
				$DataArray = array('email' => $post_array['email'], 'category_id' => $post_array['category_id'],
					'price' => $post_array['price'], 'short_descr' => $post_array['short_descr'], 'descr' => $post_array['descr'],
					'website' => (!empty($post_array['website']) ? $post_array['website'] : ''),
					'about_us' => $post_array['about_us'], 'schedule_pricing' => $post_array['schedule_pricing'], 'additional_info' => $post_array['additional_info'],
					'highlights_list' => $highlights_list);

				$current_tour_step_1= $this->session->userdata("current_tour_step_1");
				$unique_session_id= 'tour_wizard_' . $current_tour_step_1['unique_session_id'];
				$Res= $this->msession_data->UpdateSession_Data($unique_session_id, $DataArray);
				$this->session->set_userdata( "current_tour_step_3", array( 'unique_session_id'=> $unique_session_id ) );

				$RedirectUrl = $this->config->base_url() . 'admin/tour/tourstep4';
				redirect($RedirectUrl);
			} else {
				$form_status = 'invalid';
				$Tour['email'] = set_value('email');
				$Tour['category_id'] = set_value('category_id');
				$Tour['price'] = set_value('price');
				$Tour['short_descr'] = set_value('short_descr');
				$Tour['descr'] = set_value('descr');
				$Tour['website'] = set_value('website');
				$Tour['about_us'] = set_value('about_us');
				$Tour['schedule_pricing'] = set_value('schedule_pricing');
				$Tour['additional_info'] = set_value('additional_info');
			}
		} // if (!empty($_POST)) { // form was submitted
		else {
			$current_tour_step_3= $this->get_tour_step_3_data();
			$Tour['email'] = (!empty($current_tour_step_3['email']) ? $current_tour_step_3['email'] : "");
			$Tour['category_id'] = (!empty($current_tour_step_3['category_id']) ? $current_tour_step_3['category_id'] : "");
			$Tour['price'] = (!empty($current_tour_step_3['price']) ? $current_tour_step_3['price'] : "");
			$Tour['short_descr'] = (!empty($current_tour_step_3['short_descr']) ? $current_tour_step_3['short_descr'] : "");
			$Tour['descr'] = (!empty($current_tour_step_3['descr']) ? $current_tour_step_3['descr'] : "");
			$Tour['website'] = (!empty($current_tour_step_3['website']) ? $current_tour_step_3['website'] : "");
			$Tour['about_us'] = (!empty($current_tour_step_3['about_us']) ? $current_tour_step_3['about_us'] : "");
			$Tour['schedule_pricing'] = (!empty($current_tour_step_3['schedule_pricing']) ? $current_tour_step_3['schedule_pricing'] : "");
			$Tour['additional_info'] = (!empty($current_tour_step_3['additional_info']) ? $current_tour_step_3['additional_info'] : "");
		}

		$data = array('Tour' => AppUtils::SaveFormHTML($Tour), 'id' => $id, 'IsInsert' => $IsInsert, 'PageParametersWithSort' => $PageParametersWithSort,
			'form_status' => $form_status, 'current_feature' => $current_feature,
			'editor_message' => $editor_message, 'CategoryValueArray' => $CategoryValueArray,
			'validation_errors_text' => validation_errors($config_array['backend_error_icon_start'], $config_array['backend_error_icon_end']),
			'ControllerRef' => $this);
		$data = $this->appsmarty->AddSystemParameters($data, $this, $this->config, true);
		$this->appsmarty->RunSmartyTemplate('admin/tourstep3.tpl', $data, $this->config, true, true, true);
	} // public function tourstep3($id = 0) {  // local-backpackers.com/admin/tour/tourstep4


	public function tourstep4($id = 0)
	{
		AppAuth::IsLogged($this, array('Admin', 'Operator'));
		$this->load->helper('file');
		$this->load->model('muser', '', true);
		$this->load->model('mtour', '', true);
		$this->load->library('form_validation');
		$post_array = $this->input->post();
		$IsInsert = empty($id);
		$UriArray = $this->uri->uri_to_assoc(3);
		$current_tour_step_1 = $this->session->userdata('current_tour_step_1');
		if ( empty($current_tour_step_1['unique_session_id']) ) {
			redirect( $this->config->base_url() . 'admin/tour/tourstep1' );
		}

		$editor_message = $this->session->flashdata('editor_message');
		$config_array = $this->config->config;
		$PageParametersWithSort = $this->PreparePageParameters($UriArray, null, true, true);
		$images_upload_display_errors = '';

		$form_status = 'edit';
		if (!empty($_POST) or !empty($_FILES)) { // form was submitted
			$hidden_next_page = $post_array['hidden_next_page'];
			$current_tour_step_1 = $this->session->userdata('current_tour_step_1');
			$dir_path = $config_array['document_root'] . $config_array['images_dir'] . 'tmp/-tmp_tour-' . $current_tour_step_1['unique_session_id'] . DIRECTORY_SEPARATOR;
			if (!is_dir($dir_path)) {
				mkdir($dir_path, 0777);
			}
			$upload_config['upload_path'] = $dir_path;
			if (!empty($config_array['uploads_images_allowed_types'])) $upload_config['allowed_types'] = $config_array['uploads_images_allowed_types'];
			if (!empty($config_array['uploads_images_max_size'])) $upload_config['max_size'] = $config_array['uploads_images_max_size'];
			if (!empty($config_array['uploads_images_max_width'])) $upload_config['max_width'] = $config_array['uploads_images_max_width'];
			if (!empty($config_array['uploads_images_max_height'])) $upload_config['max_height'] = $config_array['uploads_images_max_height'];

			$this->load->library('upload', $upload_config);

			if (!empty($_FILES['add_image']['name'])) {
				$current_tour_step_2 = $this->session->userdata('current_tour_step_2');
				if ( !empty($current_tour_step_2['feature']) ) $current_feature= $current_tour_step_2['feature'];
				$TourImagesCount = count(get_filenames($dir_path));
				if ( $current_feature == "S" ) {
					$tour_max_photos= $config_array['tour_max_photos_standart'];
				} else {
					$tour_max_photos= $config_array['tour_max_photos_featured'];
				}
				if ( $tour_max_photos < $TourImagesCount + 1 ) {
					$this->session->set_flashdata('editor_message', 'You can not upload more '.$tour_max_photos.' images for Tour.');
					redirect( $this->config->base_url() . 'admin/tour/tourstep4' );
				}
				$this->upload->do_upload('add_image');
				$add_image = $_FILES['add_image']['name'];
			}

			if ((int)$hidden_next_page == 1) {
				$TourImages = get_filenames($dir_path);
				AppUtils::deb($TourImages, '$TourImages::');
				if (empty($TourImages) or !is_array($TourImages)) {
					$RedirectUrl = $this->config->base_url() . 'admin/tour/tourstep4';
					$this->session->set_flashdata('editor_message', 'At least 1 image must be uploaded for Tour.');
				} else {
					$RedirectUrl = $this->config->base_url() . 'admin/tour/tourstep5';
				}

			} else {
				$RedirectUrl = $this->config->base_url() . 'admin/tour/tourstep4';
			}
			redirect($RedirectUrl);
		} // if (!empty($_POST)) { // form was submitted
		$data = array('id' => $id, 'IsInsert' => $IsInsert, 'PageParametersWithSort' => $PageParametersWithSort, 'form_status' => $form_status,
			'editor_message' => $editor_message,
			'validation_errors_text' => validation_errors($config_array['backend_error_icon_start'], $config_array['backend_error_icon_end']),
			'ControllerRef' => $this, 'is_developer_comp' => 0, 'tmp_url_directory' => '');
		$data = $this->appsmarty->AddSystemParameters($data, $this, $this->config, true);
		$this->appsmarty->RunSmartyTemplate('admin/tourstep4.tpl', $data, $this->config, true, true, true);
	} // public function tourstep4($id = 0) {  // local-backpackers.com/admin/tour/tourstep4


	public function tourstep5($id = 0)
	{
		AppAuth::IsLogged($this, array('Admin', 'Operator'));
		$this->load->helper('file');
		$this->load->model('muser', '', true);
		$this->load->model('mtour', '', true);
		$this->load->model('mtour_highlight', '', true);
		$this->load->model('mstate', '', true);
		$this->load->model('mregion', '', true);
		$this->load->model('msubregion', '', true);
		$this->load->model('mcategory', '', true);
		$this->load->model('msession_data', '', true);
		$this->load->library('form_validation');
		$post_array = $this->input->post();
		$IsInsert = empty($id);
		$UriArray = $this->uri->uri_to_assoc(3);

		$editor_message = $this->session->flashdata('editor_message');
		$config_array = $this->config->config;
		$PageParametersWithSort = $this->PreparePageParameters($UriArray, null, true, true);

		$form_status = 'edit';

		$current_tour_step_1 = $this->session->userdata('current_tour_step_1');
		if ( empty($current_tour_step_1['unique_session_id']) ) {
			redirect( $this->config->base_url() . 'admin/tour/tourstep1' );
		}

		$StateRow = $this->mstate->getRowById($current_tour_step_1['state_id']);
		$state_name = '';
		if (!empty($StateRow['name'])) $state_name = $StateRow['name'];

		$RegionRow = $this->mregion->getRowById($current_tour_step_1['region_id']);
		$region_name = '';
		if (!empty($RegionRow['name'])) $region_name = $RegionRow['name'];

		$SubregionRow = $this->msubregion->getRowById($current_tour_step_1['subregion_id']);
		$subregion_name = '';
		if (!empty($SubregionRow['name'])) $subregion_name = $SubregionRow['name'];


		$current_tour_step_2 = $this->session->userdata('current_tour_step_2');
		$feature_name = '';
		if (!empty($current_tour_step_2['feature']) and $current_tour_step_2['feature'] == 'F') $feature_name = 'Featured';
		if (!empty($current_tour_step_2['feature']) and $current_tour_step_2['feature'] == 'S') $feature_name = 'Standart';
		$current_feature = 'S';
		if (!empty($current_tour_step_2['feature'])) $current_feature = $current_tour_step_2['feature'];

		$current_tour_step_3= $this->get_tour_step_3_data();

		$CategoryRow = $this->mcategory->getRowById($current_tour_step_3['category_id']);
		$category_name = '';
		if (!empty($CategoryRow['name'])) $category_name = $CategoryRow['name'];
		$highlights_list = array();
		if (!empty($current_tour_step_3['highlights_list'])) {
			foreach ($current_tour_step_3['highlights_list'] as $key => $highlight) {
				$highlights_list[] = array('key' => $key, 'name' => $highlight);
			}
		}
		$current_tour_step_4 = $this->session->userdata('current_tour_step_4');

		$ImagesGallery = $this->ShowImagesGalleryFromTourStep4($config_array, false, false);
		if (!empty($_POST) or !empty($_FILES)) { // form was submitted
			$TourId = $this->MakeSaveTourStep5($this, $config_array, $current_tour_step_1, $current_tour_step_2, $current_tour_step_3, $highlights_list, $current_tour_step_4);
			$this->ClearStepsData();
			redirect($this->config->base_url() . 'admin/tour/tourstep_final/tour_id/' . $TourId);
		} // if (!empty($_POST)) { // form was submitted

		$data = array('id' => $id, 'IsInsert' => $IsInsert, 'PageParametersWithSort' => $PageParametersWithSort, 'form_status' => $form_status,
			'editor_message' => $editor_message,
			'validation_errors_text' => validation_errors($config_array['backend_error_icon_start'], $config_array['backend_error_icon_end']),
			'ControllerRef' => $this, 'is_developer_comp' => 0, 'tmp_url_directory' => '', 'current_tour_step_1' => $current_tour_step_1, 'state_name' => $state_name,
			'region_name' => $region_name, 'subregion_name' => $subregion_name,
			'current_tour_step_2' => $current_tour_step_2, 'feature_name' => $feature_name,
			'current_tour_step_3' => $current_tour_step_3, 'highlights_list' => $highlights_list,
			'category_name' => $category_name,
			'current_tour_step_4' => $current_tour_step_4, 'ImagesGallery' => $ImagesGallery, 'current_feature' => $current_feature);
		$data = $this->appsmarty->AddSystemParameters($data, $this, $this->config, true);
		$this->appsmarty->RunSmartyTemplate('admin/tourstep5.tpl', $data, $this->config, true, true, true);
	} // public function tourstep5($id = 0) {  // local-backpackers.com/admin/tour/tourstep4


	private function MakeSaveTourStep5($ControllerRef, $config_array, $current_tour_step_1, $current_tour_step_2, $current_tour_step_3, $highlights_list, $current_tour_step_4)
	{
		$ControllerRef->db->trans_begin();
		$LoggedUserData = AppAuth::getLoggedUserData($ControllerRef);
		$admin_watch_operator_id = -1;
		if (!empty($LoggedUserData['user_role']) and $LoggedUserData['user_role'] == 'Admin') {
			if (!empty($LoggedUserData['admin_watch_operator_id'])) {
				$admin_watch_operator_id = $LoggedUserData['admin_watch_operator_id'];
			}
			$logged_user_id = $LoggedUserData['logged_user_id'];
		}
		if (!empty($LoggedUserData['user_role']) and $LoggedUserData['user_role'] == 'Operator') {
			$admin_watch_operator_id = $LoggedUserData['logged_user_id'];
			$logged_user_id = $LoggedUserData['logged_user_id'];
		}

		$slug_config = array(
			'field' => 'slug',
			'title' => 'name',
			'table' => $ControllerRef->mtour->db->dbprefix . 'tour',
			'id' => 'id',
		);
		$ControllerRef->load->library('slug', $slug_config);
		$generated_slug = $ControllerRef->slug->create_uri(array('name' => $current_tour_step_1['name']));
		$TourId = $ControllerRef->mtour->UpdateTour('', Array(
			'user_id' => ($LoggedUserData['user_role'] == 'Admin' ? $admin_watch_operator_id : $logged_user_id), 'uid' => '',
			'region_id' => $current_tour_step_1['region_id'], 'subregion_id' => $current_tour_step_1['subregion_id'], 'state_id' => $current_tour_step_1['state_id'],
			'category_id' => $current_tour_step_3['category_id'], 'price' => $current_tour_step_3['price'], 'name' => $current_tour_step_1['name'],
			'email' => $current_tour_step_3['email'],
			'feature' => $current_tour_step_2['feature'], 'slug' => $generated_slug,
			'short_descr' => $current_tour_step_3['short_descr'], 'descr' => $current_tour_step_3['descr'],
			'distance' => '',
			'website' => $current_tour_step_3['website'], 'payment_log_id' => '',
			'payment_expired_at' => '',
			'about_us' => $current_tour_step_3['about_us'],
			'schedule_pricing' => $current_tour_step_3['schedule_pricing'], 'additional_info' => $current_tour_step_3['additional_info'],
			'modified_by' => ($LoggedUserData['user_role'] == 'Admin' ? $admin_watch_operator_id : $logged_user_id)));

		if ($TourId) {
			foreach ($highlights_list as $highlight) {
				$Res = $ControllerRef->mtour_highlight->UpdateTour_Highlight(null, array('tour_id' => $TourId, 'name' => $highlight['name']));
			}
		}

		if ($TourId) {
			if ($ControllerRef->db->trans_status() === FALSE) {
				$ControllerRef->db->trans_rollback();
			} else {
				if ($TourId) {
					$tmp_tour_dir_path = $current_tour_step_1['tmp_tour_dir_path'];

					$TourImages = get_filenames($tmp_tour_dir_path);
					if (empty($TourImages) or !is_array($TourImages))
						return '';
					$dest_dir_path = $config_array['document_root'] . $config_array['uploads_tours_dir'] . '-tour-' . $TourId . DIRECTORY_SEPARATOR;
					if (!is_dir($dest_dir_path)) {
						mkdir($dest_dir_path, 0777);
					}
					foreach ($TourImages as $TourImage) {
						copy($tmp_tour_dir_path . DIRECTORY_SEPARATOR . $TourImage, $dest_dir_path . DIRECTORY_SEPARATOR . $TourImage);
					}
					AppUtils::DeleteDirectory($tmp_tour_dir_path);
				}
				$ControllerRef->db->trans_commit();
			}
			return $TourId;
		}
	} // private function MakeSaveTourStep5($ControllerRef) {


	public function tourstep_final($id = 0)
	{
		AppAuth::IsLogged($this, array('Admin', 'Operator'));
		$UriArray = $this->uri->uri_to_assoc(2);
		$data = array('tour_id' => $UriArray['tour_id']);
		$data = $this->appsmarty->AddSystemParameters($data, $this, $this->config, true);
		$this->appsmarty->RunSmartyTemplate('admin/tourstep_final.tpl', $data, $this->config, true, true, true);
	}


	public function edit($id = 0)
	{
		AppAuth::IsLogged($this, array('Admin', 'Operator'));
		$post_array = $this->input->post();
		$IsInsert = empty($id);
		$editor_message = $this->session->flashdata('editor_message');
		$this->edit_load($this);
		$config_array = $this->config->config;

		$LoggedUserData = AppAuth::getLoggedUserData($this);
		$admin_watch_operator_id = -1;
		if (!empty($LoggedUserData['user_role']) and $LoggedUserData['user_role'] == 'Admin') {
			if (!empty($LoggedUserData['admin_watch_operator_id'])) {
				$admin_watch_operator_id = $LoggedUserData['admin_watch_operator_id'];
			}
			$logged_user_id = $LoggedUserData['logged_user_id'];
		}
		if (!empty($LoggedUserData['user_role']) and $LoggedUserData['user_role'] == 'Operator') {
			$admin_watch_operator_id = $LoggedUserData['logged_user_id'];
			$logged_user_id = $LoggedUserData['logged_user_id'];
		}

		$UriArray = $this->uri->uri_to_assoc(5);
		$active_tab = AppUtils::getParameter($this, $UriArray, $post_array, 'active_tab');

		$PageParametersWithSort = $this->PreparePageParameters($UriArray, $post_array, true, true);
		$Tour = null;
		$TourStatusValueArray = AppUtils::SetArrayHeader(array(array('key' => '', 'value' => ' -Select Status- ')), $this->mtour->getTourStatusValueArray());
		$RegionValueArray = AppUtils::SetArrayHeader(array(array('key' => '', 'value' => ' -Select Region- ')), $this->mregion->getRegionsSelectionList());
		$SubregionValueArray = AppUtils::SetArrayHeader(array(array('key' => '', 'value' => ' -Select Area- ')), $this->msubregion->getSubregionsSelectionList());
		$StateValueArray = AppUtils::SetArrayHeader(array(array('key' => '', 'value' => ' -Select State- ')), $this->mstate->getStatesSelectionList());
		$CategoryValueArray = AppUtils::SetArrayHeader(array(array('key' => '', 'value' => ' -Select Category- ')), $this->mcategory->getCategorysSelectionList());
		$TourFeatureValueArray = AppUtils::SetArrayHeader(array(array('key' => '', 'value' => ' -Select Package- ')), $this->mtour->getTourFeatureValueArray());

		$Tour_InqueryRequest_CallbackValueArray = AppUtils::SetArrayHeader(array(array('key' => '', 'value' => ' -Select Request Callback- ')), $this->mtour_inquery->getTour_InqueryRequest_CallbackValueArray());
		$Tour_InqueryStatusValueArray = AppUtils::SetArrayHeader(array(array('key' => '', 'value' => ' -Select Status- ')), $this->mtour_inquery->getTour_InqueryStatusValueArray());
		$Tour_Review_Flag_StatusValueArray = AppUtils::SetArrayHeader(array(array('key' => '', 'value' => ' -Select Flag Status- ')), $this->mtour_review->getTour_Review_Flag_StatusValueArray());
		$Tour_Review_Stars_Rating_TypeArray = AppUtils::SetArrayHeader(array(array('key' => '', 'value' => ' -Select Review Stars Rating- ')), $this->mtour_review->getTour_Review_Stars_Rating_TypeValueArray());


		$PageParametersWithSort = $this->PreparePageParameters($UriArray, null, true, true);
		$RedirectUrl = $this->config->base_url() . 'admin/tour/index' . $PageParametersWithSort;
		$ImagesGallery = '';
		if (!$IsInsert) {
			$Tour = $this->mtour->getRowById($id);
			if (empty($Tour)) {
				$this->session->set_flashdata('editor_message', "Tour '" . $id . "' not found");
				redirect($RedirectUrl);
				return;
			}
			$ImagesGallery = $this->ShowImagesGallery($id, $config_array, false);

			if ($LoggedUserData['user_role'] == 'Operator' and $Tour['user_id'] != $logged_user_id) {
				$this->session->set_flashdata('editor_message', "Tour '" . $id . "' is of wrong Operator");
				redirect($RedirectUrl);
				return;
			}
			if ($LoggedUserData['user_role'] == 'Admin' and $Tour['user_id'] != $admin_watch_operator_id) {
				$this->session->set_flashdata('editor_message', "Tour '" . $id . "' is of wrong Admin/Operator");
				redirect($RedirectUrl);
				return;
			}
		}
		$form_status = 'edit';
		$images_upload_display_errors = '';
		$TabTour_DetailsHasError = false;
		$TabChoose_PackageHasError = false;
		$TabTour_SetupHasError = false;
		$TabTour_GalleryHasError = false;
		$CheckedHighlightsArray = array();

		if (!empty($_POST)) { // form was submitted
			$IsInsert = empty($post_array['id']);
			$is_reopen = $this->input->post('is_reopen');
			$IsImageUploaded = !empty($_FILES['add_image']['name']);
			$TourHighlightsList = $this->mtour_highlight->getTour_HighlightsList(false, $id);
			$WasFieldChanged = false;
			$IsTourHighlightsListChanged = false;
			$this->edit_form_validation($this, $IsInsert, $id, $LoggedUserData, $post_array);
			$validation_status = $this->form_validation->run();

			if ($validation_status != FALSE) { //

				if (!$IsInsert) {

					$WasFieldChanged = AppUtils::WasFieldChanged($Tour, $this->input->post(), array('id', 'updated_at', 'created_at'));
					if ($WasFieldChanged == '' and !$IsImageUploaded and !$IsTourHighlightsListChanged
					) {
						if ($is_reopen)
							$RedirectUrl = $this->config->base_url() . 'admin/tour/edit/' . $post_array['id'] . $PageParametersWithSort;
						redirect($RedirectUrl);
						return;
					}

				} // if (!$IsInsert) {
				else {
					$WasFieldChanged = true;
				}

				$this->edit_makesave($this, $IsInsert, $id, $is_reopen, $RedirectUrl, $LoggedUserData, $logged_user_id, $admin_watch_operator_id, $PageParametersWithSort, $post_array, $config_array,
					$WasFieldChanged, $IsImageUploaded, $IsTourHighlightsListChanged/*(, $CheckedHighlightsArray*/);
			} else {
				$form_status = 'invalid';
				$Tour = $this->edit_fill_current_data($this, $Tour, $IsInsert, $id, $LoggedUserData, $logged_user_id);
				$validation_error_fields_array = array('name' => strip_tags(form_error('name')), 'state_id' => strip_tags(form_error('state_id')),
					'region_id' => strip_tags(form_error('region_id')), 'subregion_id' => strip_tags(form_error('subregion_id')),
					'feature' => strip_tags(form_error('feature')),
					'email' => strip_tags(form_error('email')), 'category_id' => strip_tags(form_error('category_id')),
					'price' => strip_tags(form_error('price')), 'short_descr' => strip_tags(form_error('short_descr')), 'descr' => strip_tags(form_error('descr')),
					'about_us' => strip_tags(form_error('about_us')), 'schedule_pricing' => strip_tags(form_error('schedule_pricing')), 'additional_info' => strip_tags(form_error('additional_info')), 'add_image' => strip_tags(form_error('add_image'))
				);

				$TabTour_DetailsHasError = $this->getHasNonEmptyArrayItems($validation_error_fields_array, array('name', 'state_id', 'region_id', 'subregion_id'));
				$TabChoose_PackageHasError = $this->getHasNonEmptyArrayItems($validation_error_fields_array, array('feature'));
				$TabTour_SetupHasError = $this->getHasNonEmptyArrayItems($validation_error_fields_array, array('email', 'category_id', 'price', 'short_descr', 'descr', 'about_us', 'schedule_pricing', 'additional_info'));
				$TabTour_GalleryHasError = $this->getHasNonEmptyArrayItems($validation_error_fields_array, array('add_image'));
			}
		} // if (!empty($_POST)) { // form was submitted
		else {
		}

		$Pricing = $this->mpricing->getRow();
		if (!empty($Pricing)) {
			$amount_per_month_standard = $Pricing['tour_standard'];
			$amount_per_month_featured = $Pricing['tour_featured'];
		}

		$data = array('Tour' => AppUtils::SaveFormHTML($Tour), 'id' => $id, 'IsInsert' => $IsInsert, 'PageParametersWithSort' => $PageParametersWithSort, 'form_status' => $form_status,
			'editor_message' => $editor_message, 'TourStatusValueArray' => $TourStatusValueArray, 'SubregionValueArray' => $SubregionValueArray,
			'RegionValueArray' => $RegionValueArray, 'StateValueArray' => $StateValueArray,
			'CategoryValueArray' => $CategoryValueArray, /* 'CheckedHighlightsArray' => urlencode(json_encode($CheckedHighlightsArray)), */
			'TourFeatureValueArray' => $TourFeatureValueArray, 'Tour_InqueryStatusValueArray' => $Tour_InqueryStatusValueArray, 'Tour_Review_Flag_StatusValueArray' => $Tour_Review_Flag_StatusValueArray,
			'Tour_Review_Stars_Rating_TypeArray' => $Tour_Review_Stars_Rating_TypeArray, 'active_tab' => $active_tab,
			'images_upload_display_errors' => $images_upload_display_errors, 'validation_errors_text' => validation_errors($config_array['backend_error_icon_start'], $config_array['backend_error_icon_end']), 'TabTour_DetailsHasError' => $TabTour_DetailsHasError,
			'TabChoose_PackageHasError' => $TabChoose_PackageHasError, 'TabTour_SetupHasError' => $TabTour_SetupHasError,
			'TabTour_GalleryHasError'=>$TabTour_GalleryHasError,
			'ControllerRef' => $this, 'ImagesGallery' => $ImagesGallery, 'is_developer_comp' => 0, 'tmp_url_directory' => '',
			'amount_per_month_standard' => $amount_per_month_standard, 'amount_per_month_featured' => $amount_per_month_featured
		);

		$data = $this->appsmarty->AddSystemParameters($data, $this, $this->config, true);
		$this->appsmarty->RunSmartyTemplate('admin/tour_edit.tpl', $data, $this->config, true, true, true);
	}

	private function edit_load($ControllerRef)
	{
		$ControllerRef->load->helper('file');
		$ControllerRef->load->model('muser', '', true);
		$ControllerRef->load->model('mtour', '', true);
		$ControllerRef->load->model('mtour_inquery', '', true);
		$ControllerRef->load->model('mtour_review', '', true);
		$ControllerRef->load->model('mtour_highlight', '', true);
		$ControllerRef->load->model('mregion', '', true);
		$ControllerRef->load->model('msubregion', '', true);
		$ControllerRef->load->model('mstate', '', true);
		$ControllerRef->load->model('mcategory', '', true);
		$ControllerRef->load->model('mpricing', '', true);
		$ControllerRef->load->library('form_validation');
	}

	private function edit_form_validation($ControllerRef, $IsInsert, $id, $LoggedUserData, $post_array)
	{
		if ($IsInsert) {
			$ControllerRef->form_validation->set_rules('name', 'Name', 'required|is_unique[tour.name]');
			$ControllerRef->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[tour.email]');
		} else {
			$ControllerRef->form_validation->set_rules('name', 'Name', 'required|is_unique[tour.name.id.' . $id . ']');
			$ControllerRef->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[tour.email.id.' . $id . ']');
		}
		$ControllerRef->form_validation->set_rules('region_id', 'Region', 'required');
		$ControllerRef->form_validation->set_rules('subregion_id', 'Area', 'required');
		$ControllerRef->form_validation->set_rules('state_id', 'State', 'required');

		$ControllerRef->form_validation->set_rules('feature', 'Package', 'required');
		$ControllerRef->form_validation->set_rules('slug', 'Slug', '');
		$ControllerRef->form_validation->set_rules('status', 'status', '');

		$ControllerRef->form_validation->set_rules('category_id', 'Category', 'required');
		$ControllerRef->form_validation->set_rules('price', 'Price', 'required|numeric');

		$ControllerRef->form_validation->set_rules('town', 'Town', '');

		$ControllerRef->form_validation->set_rules('short_descr', 'Short Description', 'required');
		$ControllerRef->form_validation->set_rules('descr', 'Description', 'required|callback_verify_max_words_descr');

		if (!empty($post_array['feature']) and $post_array['feature'] == 'F' ) {
		  $ControllerRef->form_validation->set_rules('website', 'Website',  'required' );
		} else {
			$ControllerRef->form_validation->set_rules('website', 'Website',  '' );
		}
		$ControllerRef->form_validation->set_rules('about_us', 'About Us', 'callback_verify_max_words_about_us');
		$ControllerRef->form_validation->set_rules('schedule_pricing', 'Schedule Pricing', 'callback_verify_max_words_schedule_pricing');
		$ControllerRef->form_validation->set_rules('additional_info', 'Additional Info', 'callback_verify_max_words_additional_info');
		$ControllerRef->form_validation->set_rules('add_image', 'Add Image', 'callback_verify_add_image');
	}

	public function verify_add_image($str) {
		$config_array = $this->config->config;
		$UriArray = $this->uri->uri_to_assoc(3);
		if (empty($UriArray['edit'])) return true;
		$tour_id= $UriArray['edit'];

		$dir_path =  $config_array['document_root'] . $config_array['uploads_tours_dir'] . '-tour-' . $tour_id . DIRECTORY_SEPARATOR;
		$TourImagesCount = count(get_filenames($dir_path));
		$current_feature= $this->input->post('feature');
		if ( $current_feature == "S" ) {
			$tour_max_photos= $config_array['tour_max_photos_standart'];
		} else {
			$tour_max_photos= $config_array['tour_max_photos_featured'];
		}
		if ( $tour_max_photos < $TourImagesCount + ( !empty($_FILES['add_image']['name']) ? 1 : 0 ) ) {
			$this->form_validation->set_message('verify_add_image', 'You can not upload more '.$tour_max_photos.' images for Tour.' );
			return false;
		}
		if ( $TourImagesCount + ( !empty($_FILES['add_image']['name']) ? 1 : 0 ) == 0 ) {
			$this->form_validation->set_message('verify_add_image', 'You must upload at least 1 image for Tour.' );
			return false;
		}
		return TRUE;
	}


	public function verify_max_words_additional_info($str)
	{
		if (strlen($str) > 2000) {
			$this->form_validation->set_message('verify_max_words_additional_info', "The %s field can not be more 2000 characters ! ");
			return FALSE;
		}
		return TRUE;
	}

	public function verify_max_words_schedule_pricing($str)
	{
		if (strlen($str) > 2000) {
			$this->form_validation->set_message('verify_max_words_schedule_pricing', "The %s field can not be more 2000 characters ! ");
			return FALSE;
		}
		return TRUE;
	}

	public function verify_max_words_about_us($str)
	{
		if (strlen($str) > 2000) {
			$this->form_validation->set_message('verify_max_words_about_us', "The %s field can not be more 2000 characters ! ");
			return FALSE;
		}
		return TRUE;
	}

	public function verify_max_words_descr($str)
	{
		if (strlen($str) > 500) {
			$this->form_validation->set_message('verify_max_words_descr', "The %s field can not be more 500 characters ! ");
			return FALSE;
		}
		return TRUE;
	}


	private function edit_makesave($ControllerRef, $IsInsert, $id, $is_reopen, $RedirectUrl, $LoggedUserData, $logged_user_id, $admin_watch_operator_id, $PageParametersWithSort, $post_array, $config_array,
																 $WasFieldChanged, $IsImageUploaded, $IsTourHighlightsListChanged /*, $CheckedHighlightsArray */)
	{
		$ControllerRef->db->trans_begin();
		$slug_config = array(
			'field' => 'slug',
			'title' => 'name',
			'table' => $ControllerRef->mtour->db->dbprefix . 'tour',
			'id' => 'id',
		);
		$ControllerRef->load->library('slug', $slug_config);
		$generated_slug = $ControllerRef->slug->create_uri(array('name' => $ControllerRef->input->post('name')) /* , $ControllerRef->input->post('id') */);
		if ($WasFieldChanged) {
			$OkResult = $ControllerRef->mtour->UpdateTour($ControllerRef->input->post('id'), Array(
				'user_id' => ($LoggedUserData['user_role'] == 'Admin' ? $admin_watch_operator_id : $logged_user_id), 'uid' => $ControllerRef->input->post('uid'),
				'region_id' => $ControllerRef->input->post('region_id'), 'subregion_id' => $ControllerRef->input->post('subregion_id'), 'state_id' => $ControllerRef->input->post('state_id'),
				'category_id' => $ControllerRef->input->post('category_id'), 'price' => $ControllerRef->input->post('price'), 'name' => $ControllerRef->input->post('name'),
				'email' => $ControllerRef->input->post('email'),
				'feature' => $ControllerRef->input->post('feature'), 'slug' => $generated_slug,
				'short_descr' => $ControllerRef->input->post('short_descr'), 'descr' => $ControllerRef->input->post('descr'),
				'distance' => $ControllerRef->input->post('distance'),
				'website' => $ControllerRef->input->post('website'), 'payment_log_id' => $ControllerRef->input->post('payment_log_id'),
				'payment_expired_at' => $ControllerRef->input->post('payment_expired_at'), // 'view_count' => $ControllerRef->input->post('view_count'),
				'about_us' => $ControllerRef->input->post('about_us'),
				'schedule_pricing' => $ControllerRef->input->post('schedule_pricing'), 'additional_info' => $ControllerRef->input->post('additional_info'),
				'modified_by' => ($LoggedUserData['user_role'] == 'Admin' ? $admin_watch_operator_id : $logged_user_id)));
		} else { // if ( $WasFieldChanged ) {
			$OkResult = $id;
		}

		if ($is_reopen) {
			$RedirectUrl = $ControllerRef->config->base_url() . 'admin/tour/edit/' . $OkResult . $PageParametersWithSort;
		}
		if ($OkResult and empty($images_upload_display_errors)) {
			$ControllerRef->session->set_flashdata('editor_message', "Tour '" . $ControllerRef->input->post('name') . "' was " . ($IsInsert ? "inserted" : "updated"));
			if ($ControllerRef->db->trans_status() === FALSE) {
				$ControllerRef->db->trans_rollback();
			} else {
				if ($OkResult and $IsImageUploaded) {
					$images_upload_display_errors = $ControllerRef->mtour->UploadTourImage($ControllerRef, $OkResult, $_FILES, $post_array, $config_array);
				}
				$ControllerRef->db->trans_commit();
			}
			redirect($RedirectUrl);
			return $OkResult;
		}
	}

	private function edit_fill_current_data($ControllerRef, $Tour, $IsInsert, $id, $LoggedUserData, $logged_user_id)
	{
		$Tour['id'] = $id;
		$Tour['uid'] = set_value('uid');
		$Tour['status'] = set_value('status');
		$Tour['region_id'] = set_value('region_id');
		$Tour['subregion_id'] = set_value('subregion_id');
		$Tour['state_id'] = set_value('state_id');
		$Tour['category_id'] = set_value('category_id');
		$Tour['price'] = set_value('price');
		$Tour['email'] = set_value('email');
		$Tour['name'] = set_value('name');
		$Tour['feature'] = set_value('feature');
		$Tour['slug'] = set_value('slug');
		$Tour['short_descr'] = set_value('short_descr');
		$Tour['descr'] = set_value('descr');
		$Tour['distance'] = set_value('distance');
		$Tour['website'] = set_value('website');
		//$Tour['rating'] = set_value('rating');
		//$Tour['view_count'] = set_value('view_count');
		$Tour['payment_log_id'] = set_value('payment_log_id');
		$Tour['payment_expired_at'] = set_value('payment_expired_at');
		$Tour['about_us'] = set_value('about_us');
		$Tour['schedule_pricing'] = set_value('schedule_pricing');
		$Tour['additional_info'] = set_value('additional_info');
		return $Tour;
	}


	public function delete($id = 0)
	{
		AppAuth::IsLogged($this, array('Admin', 'Operator'));
		$this->load->model('mtour_inquery', '', true);
		$this->load->model('mtour', '', true);
		$this->load->model('mtour_highlight', '', true);
		$this->load->model('mtour_review', '', true);
		$config_array = $this->config->config;
		if (!empty($id)) {
			$UriArray = $this->uri->uri_to_assoc(5);
			$PageParametersWithSort = $this->PreparePageParameters($UriArray, null, false, true);
			$RedirectUrl = $this->config->base_url() . 'admin/tour/index' . $PageParametersWithSort;

			$Tour = $this->mtour->getRowById($id);
			if (empty($Tour)) {
				$this->session->set_flashdata('editor_message', "Tour '" . $id . "' not found");
				redirect($RedirectUrl);
				return;
			}
			$TourName = $Tour['name'];
			$this->db->trans_begin();
			$Res = $this->mtour_inquery->DeleteTour_InquerysByTourId($id);
			$Res = $this->mtour_highlight->DeleteTour_HighlightsByTourId($id);
			$Res = $this->mtour_review->DeleteTour_ReviewsByTourId($id);
			if ($Res) {
				$OkResult = $this->mtour->DeleteTour($id);
				if ($this->db->trans_status() === FALSE) {
					$this->db->trans_rollback();
				} else {
					$this->session->set_flashdata('editor_message', "Tour '" . $TourName . "' was deleted");
					$this->db->trans_commit();
					$dir_path = $config_array['document_root'] . DIRECTORY_SEPARATOR . $config_array['uploads_tours_dir'] . '-tour-' . $id . DIRECTORY_SEPARATOR;
					AppUtils::DeleteDirectory($dir_path);
				}
				if ($OkResult) {
					$this->session->set_flashdata('editor_message', "Tour '" . $TourName . "' was deleted");
					redirect($RedirectUrl);
					return;
				}
			}
		}
	}

	public function set_to_active()
	{
		AppAuth::IsLogged($this, array('Admin'));
		$this->load->model('muser', '', true);
		$this->load->model('mtour', '', true);
		$this->load->model('mcms_item', '', true);
		$this->load->helper('email');
		$UriArray = $this->uri->uri_to_assoc(4);
		$config_array = $this->config->config;
		$id = $UriArray['id'];
		if (!empty($id)) {
			$Tour = $this->mtour->getRowById($id);
			if (!empty($Tour)) {
				$OkResult = $this->mtour->UpdateTour($id, Array('status' => 'A'));
				$TourOperator = $this->muser->getRowById($Tour['user_id']);
				$Title = $this->mcms_item->getBodyContentByAlias('tour_set_to_active',
					array('user_name' => $TourOperator['first_name'] . ' ' . $TourOperator['last_name'],
						'first_name' => $TourOperator['first_name'],
						'last_name' => $TourOperator['last_name'],
						'site_name' => $config_array['site_name'],
						'support_signature' => $config_array['support_signature'],
						'site_url' => $config_array['base_root_url'],
						'tour_url' => $config_array['base_url'] . '/admin/tour/edit/' . $id
					), false);
				$Content = $this->mcms_item->getBodyContentByAlias('tour_set_to_active',
					array('user_name' => $TourOperator['first_name'] . ' ' . $TourOperator['last_name'],
						'site_name' => $config_array['site_name'],
						'support_signature' => $config_array['support_signature'],
						'site_url' => $config_array['base_root_url'],
						'tour_url' => $config_array['base_url'] . '/admin/tour/edit/' . $id
					), true);
				$EmailOutput = AppUtils::SendEmail($TourOperator['email'], $Title, $Content, $this, $config_array, true, $this->mcms_item->getCmsItemIdByAlias('tour_set_to_active'));
				$this->output->set_content_type('application/json')->set_output(json_encode(array('ErrorMessage' => '', 'ErrorCode' => 0, 'Title' => $Title, 'Content' => $Content, 'id' => $OkResult, 'EmailOutput' => $EmailOutput)));
				return;
			}
		}
		$this->output->set_content_type('application/json')->set_output(json_encode(array('ErrorMessage' => 'Invalid Parameters', 'ErrorCode' => 1, 'id' => 0)));
	}

	public function set_to_inactive()
	{
		AppAuth::IsLogged($this, array('Admin'));
		$this->load->model('muser', '', true);
		$this->load->model('mtour', '', true);
		$this->load->model('mcms_item', '', true);
		$this->load->helper('email');
		$UriArray = $this->uri->uri_to_assoc(4);
		$config_array = $this->config->config;
		$id = $UriArray['id'];
		if (!empty($id)) {
			$Tour = $this->mtour->getRowById($id);
			if (!empty($Tour)) {
				$OkResult = $this->mtour->UpdateTour($id, Array('status' => 'I'));
				$TourOperator = $this->muser->getRowById($Tour['user_id']);
				$Title = $this->mcms_item->getBodyContentByAlias('tour_set_to_inactive',
					array('user_name' => $TourOperator['first_name'] . ' ' . $TourOperator['last_name'],
						'first_name' => $TourOperator['first_name'],
						'last_name' => $TourOperator['last_name'],
						'first_name' => $TourOperator['first_name'],
						'last_name' => $TourOperator['last_name'],
						'site_name' => $config_array['site_name'],
						'support_signature' => $config_array['support_signature'],
						'site_url' => $config_array['base_root_url'],
						'tour_url' => $config_array['base_url'] . '/admin/tour/edit/' . $id
					), false);
				$Content = $this->mcms_item->getBodyContentByAlias('tour_set_to_inactive',
					array('user_name' => $TourOperator['first_name'] . ' ' . $TourOperator['last_name'],
						'first_name' => $TourOperator['first_name'],
						'last_name' => $TourOperator['last_name'],
						'site_name' => $config_array['site_name'],
						'support_signature' => $config_array['support_signature'],
						'site_url' => $config_array['base_root_url'],
						'tour_url' => $config_array['base_url'] . '/admin/tour/edit/' . $id
					), true);

				$EmailOutput = AppUtils::SendEmail($TourOperator['email'], $Title, $Content, $this, $config_array, true, $this->mcms_item->getCmsItemIdByAlias('tour_set_to_inactive'));
				$this->output->set_content_type('application/json')->set_output(json_encode(array('ErrorMessage' => '', 'ErrorCode' => 0, 'Title' => $Title, 'Content' => $Content, 'id' => $OkResult, 'EmailOutput' => $EmailOutput)));
				return;
			}
		}
		$this->output->set_content_type('application/json')->set_output(json_encode(array('ErrorMessage' => 'Invalid Parameters', 'ErrorCode' => 1, 'id' => 0)));
	}

	private function PreparePageParameters($UriArray, $_post_array, $WithPage, $WithSort)
	{
		return '';
		$ResStr = '';
		if (!empty($_post_array)) { // form was submitted
			if ($WithPage) {
				$page = $this->input->post('page');
				$ResStr .= !empty($page) ? 'page/' . $page . '/' : 'page/1/';
			}
			$filter_login = $this->input->post('filter_login');
			$ResStr .= !empty($filter_login) ? 'filter_login/' . $filter_login . '/' : '';
			$filter_feature = $this->input->post('filter_feature');
			$ResStr .= !empty($filter_feature) ? 'filter_feature/' . $filter_feature . '/' : '';
			if ($WithSort) {
				$sorting = $this->input->post('sorting');
				$ResStr .= !empty($sorting) ? 'sorting/' . $sorting . '/' : '';
				$sort = $this->input->post('sort');
				$ResStr .= !empty($sort) ? 'sort/' . $sort . '/' : '';
			}
		} else {
			if ($WithPage) {
				$ResStr .= !empty($UriArray['page']) ? 'page/' . $UriArray['page'] . '/' : 'page/1/';
			}
			$ResStr .= !empty($UriArray['filter_login']) ? 'filter_login/' . $UriArray['filter_login'] . '/' : '';
			$ResStr .= !empty($UriArray['filter_feature']) ? 'filter_feature/' . $UriArray['filter_feature'] . '/' : '';
			if ($WithSort) {
				$ResStr .= !empty($UriArray['sorting']) ? 'sorting/' . $UriArray['sorting'] . '/' : '';
				$ResStr .= !empty($UriArray['sort']) ? 'sort/' . $UriArray['sort'] . '/' : '';
			}
		}
		if (substr($ResStr, strlen($ResStr) - 1, 1) == '/') {
			$ResStr = substr($ResStr, 0, strlen($ResStr) - 1);
		}
		return '/' . $ResStr;
	}

	private function ShowImagesGallery($id, $config_array, $show_crop = false)
	{
		$ImagesGalleryHTML = '<ul class="images_gallery">';
		$filename_path = $config_array['document_root'] . $config_array['uploads_tours_dir'] . '-tour-' . $id . DIRECTORY_SEPARATOR;
		$cropping_valid_extentions = $config_array['cropping_valid_extentions'];

		$TourImages = get_filenames($filename_path);
		if (empty($TourImages) or !is_array($TourImages))
			return '';
		$Counter = 0;
		$orig_width = 100;
		$orig_height = 80;
		foreach ($TourImages as $TourImage) {
			$filename_path = $config_array['document_root'] . DIRECTORY_SEPARATOR . $config_array['uploads_tours_dir'] . '-tour-' . $id . DIRECTORY_SEPARATOR . $TourImage;
			$filename_url = $config_array['base_root_url'] . DIRECTORY_SEPARATOR . $config_array['uploads_tours_dir'] . '-tour-' . $id . DIRECTORY_SEPARATOR . $TourImage;
			$orig_filename_path = $filename_url;
			if (file_exists($filename_path)) {
				$image_ext = AppUtils::GetFileNameExt($filename_path);
				$Filesize = filesize($filename_path);
				$FilenameInfo = AppUtils::GetImageShowSize($filename_path, $orig_width, $orig_height);
				$filename_url = AppUtils::tbUrlEncode($filename_url);
				$ImagesGalleryHTML .= '<li>
				<div>
				<a class="fancybox-button" rel="fancybox-button" title="" href="' . $orig_filename_path . '" style="background-image: url(' . $orig_filename_path . ');"/></a>' .
					'<img  style="cursor:pointer;" src="' . $config_array['base_root_url'] . DIRECTORY_SEPARATOR . $config_array['images_dir'] . 'delete.png" alt="Remove Image" onclick="javascript:DeleteTourImage(\'' . $TourImage .
					"', '" . $TourImage . "')\" > " .
					(($show_crop and in_array(strtolower($image_ext), $cropping_valid_extentions)) ?
						'<a style="cursor:pointer;" onclick="javascript:CropImage(\' ' . $orig_filename_path . '\', \'' . $TourImage . '\', '
						. $FilenameInfo['OriginalWidth'] . ', ' . $FilenameInfo['OriginalHeight'] . ' ) " title="Crop Image" >Crop</a>' : '') .
					"</div><span>" . $TourImage . '</span></li>';
			}
		}
		$ImagesGalleryHTML .= '</ul>';
		return $ImagesGalleryHTML;
	}


	private function ShowImagesGalleryFromTourStep4($config_array, $show_crop = false, $show_delete = true)
	{
		$ImagesGalleryHTML = '<ul class="images_gallery">';
		$current_tour_step_1 = $this->session->userdata("current_tour_step_1");

		$filename_path = $config_array['document_root'] . $config_array['images_dir'] . 'tmp/-tmp_tour-' . $current_tour_step_1['unique_session_id'] . DIRECTORY_SEPARATOR;
		$cropping_valid_extentions = $config_array['cropping_valid_extentions'];

		$TourImages = get_filenames($filename_path);
		if (empty($TourImages) or !is_array($TourImages))
			return '';
		$Counter = 0;
		$orig_width = 100;
		$orig_height = 80;
		foreach ($TourImages as $TourImage) {
			$filename_path = $config_array['document_root'] . DIRECTORY_SEPARATOR . $config_array['images_dir'] . 'tmp/-tmp_tour-' . $current_tour_step_1['unique_session_id'] . DIRECTORY_SEPARATOR . $TourImage;
			$filename_url = $config_array['base_root_url'] . DIRECTORY_SEPARATOR . $config_array['images_dir'] . 'tmp/-tmp_tour-' . $current_tour_step_1['unique_session_id'] . DIRECTORY_SEPARATOR . $TourImage;
			$orig_filename_path = $filename_url;
			if (file_exists($filename_path)) {
				$image_ext = AppUtils::GetFileNameExt($filename_path);
				$Filesize = filesize($filename_path);
				$FilenameInfo = AppUtils::GetImageShowSize($filename_path, $orig_width, $orig_height);
				$filename_url = AppUtils::tbUrlEncode($filename_url);
				$ImagesGalleryHTML .= '<li>
				<div>
				<a class="fancybox-button" rel="fancybox-button" title="" href="' . $orig_filename_path . '" style="background-image: url(' . $orig_filename_path . ');"></a>' .

					($show_delete ? '<img  style="cursor:pointer;" src="' . $config_array['base_root_url'] . DIRECTORY_SEPARATOR . $config_array['images_dir'] .
						'delete.png" alt="Remove Image" onclick="javascript:DeleteTourImage(\'' . $TourImage .
						"', '" . $TourImage . "')\" >" : '') .
					(($show_crop and in_array(strtolower($image_ext), $cropping_valid_extentions)) ? '
					<a style="cursor:pointer;" onclick="javascript:CropImage(\' ' . $orig_filename_path . '\', \'' . $TourImage . '\',
					' . $FilenameInfo['OriginalWidth'] . ', ' . $FilenameInfo['OriginalHeight'] . ' ) " title="Crop Image" >Crop</a>' : '') .
					'</div>' .
					'<span>' . $TourImage . '</span></li>';
			}
		}
		$ImagesGalleryHTML .= '</ul>';
		return $ImagesGalleryHTML;
	}


	public function upload_image_to_tour()
	{
		$UriArray = $this->uri->uri_to_assoc(4);
		$config_array = $this->config->config;
		$tour_id = $UriArray['id'];
		$img = AppUtils::tbUrlDecode($UriArray['img']);
		$img_basename = basename($img);
		$tmp_directory = $this->config->config['document_root'] . $this->config->config['images_dir'] . 'tmp/-tour-' . $tour_id;
		$src_filename = $tmp_directory . DIRECTORY_SEPARATOR . $img_basename;
		$dest_filename = $config_array['document_root'] . $config_array['uploads_tours_dir'] . '-tour-' . $tour_id . DIRECTORY_SEPARATOR . $img_basename;
		$uploaded_file_return = copy($src_filename, $dest_filename);
		if ($uploaded_file_return) {
			$this->output->set_content_type('application/json')->set_output(json_encode(array('ErrorMessage' => '', 'ErrorCode' => 0, 'dest_filename' => $dest_filename, 'src_filename' => $src_filename)));
		} else {
			$this->output->set_content_type('application/json')->set_output(json_encode(array('ErrorMessage' => 'Error Uploading', 'ErrorCode' => 1)));
		}
	}


	public function remove_images_tmp_dir()
	{
		$UriArray = $this->uri->uri_to_assoc(4);
		$tour_id = $UriArray['id'];
		$tmp_directory = $this->config->config['document_root'] . $this->config->config['images_dir'] . 'tmp/-tour-' . $tour_id;
		AppUtils::DeleteDirectory($tmp_directory);
		$this->output->set_content_type('application/json')->set_output(json_encode(array('ErrorMessage' => '', 'ErrorCode' => 0, 'tmp_directory' => $tmp_directory)));
	}

	public function create_cropped_image()
	{
		AppAuth::IsLogged($this, array('Admin', 'Operator'));
		$this->load->model('mtour', '', true);
		$UriArray = $this->uri->uri_to_assoc(4);
		$config_array = $this->config->config;
		$tour_id = $UriArray['tour_id'];
		$imagename = urldecode($UriArray['imagename']);

		$x = $UriArray['x'];
		$y = $UriArray['y'];
		$x2 = $UriArray['x2'];
		$y2 = $UriArray['y2'];
		$w = $UriArray['w'];
		$h = $UriArray['h'];

		if (!empty($tour_id) and !empty($imagename)) {

			$imagename_ext = AppUtils::GetFileNameExt($imagename);
			$imagename_base = AppUtils::GetFileNameBase(basename($imagename, true));
			$jpeg_quality = 100;

			$full_imagename = $config_array['base_url'] . DIRECTORY_SEPARATOR . $config_array['uploads_tours_dir'] . '-tour-' . $tour_id . DIRECTORY_SEPARATOR . $imagename;

			$known_format = false;
			if (strtolower($imagename_ext) == 'jpg' or strtolower($imagename_ext) == 'jpeg') {
				$img_r = imagecreatefromjpeg($full_imagename);
				$known_format = true;
			}
			if (strtolower($imagename_ext) == 'gif') {
				$img_r = imagecreatefromgif($full_imagename);
				$known_format = true;
			}
			if (strtolower($imagename_ext) == 'png') {
				$img_r = imagecreatefrompng($full_imagename);
				$known_format = true;
			}
			//AppUtils::deb($img_r, '$img_r::');

			$dst_r = ImageCreateTrueColor($w, $h);

			imagecopyresampled($dst_r, $img_r, 0, 0, $x, $y, 0, 0, $w, $h);
			$tmp_image = $config_array['document_root'] . DIRECTORY_SEPARATOR . 'images/tmp' . DIRECTORY_SEPARATOR . $imagename_base . '-' . time() . '.' . $imagename_ext;
			$tmp_image_url = $config_array['base_url'] . DIRECTORY_SEPARATOR . 'images/tmp' . DIRECTORY_SEPARATOR . $imagename_base . '-' . time() . '.' . $imagename_ext;
			//AppUtils::deb($tmp_image, '$tmp_image::');

			$img = imagecreatetruecolor($w, $h);
			imagecopy($img, $img_r, 0, 0, $x, $y, $w, $h);
			if (strtolower($imagename_ext) == 'jpg' or strtolower($imagename_ext) == 'jpeg') {
				imagejpeg($img, $tmp_image);
			}
			if (strtolower($imagename_ext) == 'gif') {
				imagegif($img, $tmp_image);
			}
			if (strtolower($imagename_ext) == 'png') {
				imagepng($img, $tmp_image);
			}
			if ($known_format) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('ErrorMessage' => '', 'ErrorCode' => 0, 'tmp_image' => $tmp_image, 'tmp_image_url' => $tmp_image_url, 'tmp_image_encoded' => AppUtils::tbUrlEncode($tmp_image), 'ext' => $imagename_ext, 'w' => $w, 'h' => $h)));

			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('ErrorMessage' => 'Unknown extention ' . $imagename_ext, 'ErrorCode' => 1, 'tmp_image' => '',
					'tmp_image_url' => '', 'ext' => $imagename_ext)));
			}
		}
	}

	public function upload_cropped_image()
	{
		AppAuth::IsLogged($this, array('Admin', 'Operator'));
		$this->load->model('mtour', '', true);
		$UriArray = $this->uri->uri_to_assoc(4);
		$config_array = $this->config->config;
		$tour_id = $UriArray['tour_id'];
		$uploaded_filename = $UriArray['uploaded_filename'];

		$cropped_image_toupload = AppUtils::tbUrlDecode($UriArray['cropped_image_toupload']);
		if (!empty($tour_id) and !empty($cropped_image_toupload)) {
			$base_filename = basename($cropped_image_toupload);
			$ext = AppUtils::GetFileNameExt($cropped_image_toupload);
			$dst_uploaded_filename = $uploaded_filename . '.' . $ext;
			$dst_imagename = $config_array['document_root'] . $config_array['uploads_tours_dir'] . '-tour-' . $tour_id . DIRECTORY_SEPARATOR . $dst_uploaded_filename;

			if (file_exists($dst_imagename)) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('ErrorMessage' => 'This tour already has "' . $dst_uploaded_filename . '" uploaded image !', 'ErrorCode' => 1, 'cropped_image_toupload' => $cropped_image_toupload)));
				return;
			}
			$Res = rename($cropped_image_toupload, $dst_imagename);
			if ($Res) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('ErrorMessage' => '', 'ErrorCode' => 0, 'cropped_image_toupload' => $cropped_image_toupload)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('ErrorMessage' => 'Error Uploading', 'ErrorCode' => 1)));
			}
		}
	}

	public function load_images_gallery()
	{
		AppAuth::IsLogged($this, array('Admin', 'Operator'));
		$this->load->model('mtour', '', true);
		$UriArray = $this->uri->uri_to_assoc(4);
		$config_array = $this->config->config;
		$tour_id = $UriArray['tour_id'];
		$ImagesGallery = $this->ShowImagesGallery($tour_id, $config_array, false);
		$this->output->set_content_type('text/html')->set_output($ImagesGallery);
	}

	public function load_images_gallery_tourstep4()
	{
		AppAuth::IsLogged($this, array('Admin', 'Operator'));
		$this->load->model('mtour', '', true);
		$UriArray = $this->uri->uri_to_assoc(4);
		$config_array = $this->config->config;
		$ImagesGallery = $this->ShowImagesGalleryFromTourStep4($config_array, false, true);
		$this->output->set_content_type('text/html')->set_output($ImagesGallery);
	}

	public function delete_tour_image($tour_id, $image)
	{
		AppAuth::IsLogged($this, array('Admin', 'Operator'));
		$this->load->model('mtour', '', true);
		$UriArray = $this->uri->uri_to_assoc(4);
		$config_array = $this->config->config;
		$tour_id = $UriArray['tour_id'];
		$image = $UriArray['image'];

		if (!empty($tour_id) and !empty($image)) {
			$RedirectUrl = $this->config->base_url() . 'admin/tour/edit/' . $tour_id; //$PageParametersWithSort;
			$dir_path = $config_array['document_root'] . DIRECTORY_SEPARATOR . $config_array['uploads_tours_dir'] . '-tour-' . $tour_id . DIRECTORY_SEPARATOR;
			unlink($dir_path . $image);
			$this->session->set_flashdata('editor_message', "Tour Image'" . $image . "' was deleted");
			redirect($RedirectUrl);
		}
	}

	public function delete_tour_image_tourstep4()
	{
		AppAuth::IsLogged($this, array('Admin', 'Operator'));
		$UriArray = $this->uri->uri_to_assoc(4);
		$config_array = $this->config->config;
		$image = $UriArray['image'];

		if (!empty($image)) {
			$current_tour_step_1 = $this->session->userdata("current_tour_step_1");
			$filename_path = $config_array['document_root'] . $config_array['images_dir'] . 'tmp/-tmp_tour-' . $current_tour_step_1['unique_session_id'] . DIRECTORY_SEPARATOR;
			if (file_exists($filename_path . $image)) {
				unlink($filename_path . $image);
				$this->output->set_content_type('application/json')->set_output(json_encode(array('ErrorMessage' => '', 'ErrorCode' => 0, 'image' => $image)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('ErrorMessage' => 'File not found', 'ErrorCode' => 1, 'image' => $image)));
			}
			return;
		}
		$this->output->set_content_type('application/json')->set_output(json_encode(array('ErrorMessage' => 'Image File is not given', 'ErrorCode' => 1, 'image' => $image)));
	}


	public function load_tour_inquerys()
	{
		AppAuth::IsLogged($this, array('Admin', 'Operator'));
		$this->load->model('mtour', '', true);
		$this->load->model('mtour_inquery', '', true);
		$this->load->helper('text');
		$UriArray = $this->uri->uri_to_assoc(4);
		$tour_id = $UriArray['tour_id'];
		$Tour_InquerysList = $this->mtour_inquery->getTour_InquerysList(false, '', '', $tour_id, '', '', '');
		$data = array('Tour_InquerysList' => $Tour_InquerysList, 'ControllerRef' => $this);
		$data = $this->appsmarty->AddSystemParameters($data, $this, $this->config, true);
		$this->appsmarty->RunSmartyTemplate('admin/loaded_tour_inquerys_list.tpl', $data, $this->config, true, false, false);
	}

	public function get_tour_inquery()
	{
		AppAuth::IsLogged($this, array('Admin', 'Operator'));
		$this->load->model('mtour', '', true);
		$this->load->model('mtour_inquery', '', true);
		$this->load->helper('text');
		$UriArray = $this->uri->uri_to_assoc(4);
		$tour_inquery_id = $UriArray['id'];
		if (!empty($tour_inquery_id)) {
			$TourInquery = $this->mtour_inquery->getRowById($tour_inquery_id);
			$TourInquery['request_callback'] = $this->mtour_inquery->getTour_InqueryRequest_CallbackLabel($TourInquery['request_callback']);
			$TourInquery['start_date'] = AppUtils::ShowFormattedDateTime($TourInquery['start_date'], "DATEASTEXT");
			$TourInquery['end_date'] = AppUtils::ShowFormattedDateTime($TourInquery['start_date'], "DATEASTEXT");
			$TourInquery['created_at'] = AppUtils::ShowFormattedDateTime($TourInquery['created_at'], "ASTEXT");
			if ($TourInquery) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('ErrorMessage' => '', 'ErrorCode' => 0, 'id' => $tour_inquery_id, 'TourInquery' => $TourInquery)));
				return;
			}
		}
		$this->output->set_content_type('application/json')->set_output(json_encode(array('ErrorMessage' => 'Invalid Parameters', 'ErrorCode' => 1, 'id' => 0)));
	}

	public function change_tour_inquery()
	{
		AppAuth::IsLogged($this, array('Admin', 'Operator'));
		$this->load->model('mtour', '', true);
		$this->load->model('mtour_inquery', '', true);
		$UriArray = $this->uri->uri_to_assoc(4);
		$post_array = $this->input->post();
		$tour_inquery_id = AppUtils::getParameter($this, $UriArray, $post_array, 'tour_inquery_id');
		$status = AppUtils::getParameter($this, $UriArray, $post_array, 'status');
		$extro_info = AppUtils::getParameter($this, $UriArray, $post_array, 'extro_info');

		$TourInquery = $this->mtour_inquery->getRowById($tour_inquery_id);
		if (!empty($tour_inquery_id) and !empty($status) and !empty($TourInquery)) {
			$OkResult = $this->mtour_inquery->UpdateTour_Inquery($tour_inquery_id, Array(
				'status' => $status, 'extro_info' => $extro_info));

			$EmailOutput = '';
			if ($OkResult) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('ErrorMessage' => '', 'ErrorCode' => 0, 'id' => $tour_inquery_id, 'status' => $status, 'EmailOutput' => $EmailOutput)));
				return;
			}
		}
		$this->output->set_content_type('application/json')->set_output(json_encode(array('ErrorMessage' => 'Invalid Parameters', 'ErrorCode' => 1, 'id' => 0)));
	}

	public function delete_tour_inquery($id = 0)
	{
		AppAuth::IsLogged($this, array('Admin', 'Operator'));
		$this->load->model('mtour_inquery', '', true);
		$config_array = $this->config->config;
		if (!empty($id)) {
			$UriArray = $this->uri->uri_to_assoc(5);
			$PageParametersWithSort = $this->PreparePageParameters($UriArray, null, false, true);

			$OkResult = $this->mtour_inquery->DeleteTour_Inquery($id);
			if ($OkResult) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('ErrorMessage' => '', 'ErrorCode' => 0, 'id' => $id)));
				return;
			}
		}
		$this->output->set_content_type('application/json')->set_output(json_encode(array('ErrorMessage' => 'Invalid Parameters', 'ErrorCode' => 1, 'id' => 0)));
	}

	////////////////////////////////////////
	public function load_tour_highlights()
	{
		AppAuth::IsLogged($this, array('Admin', 'Operator'));
		$this->load->model('mtour_highlight', '', true);
		$this->load->helper('text');
		$UriArray = $this->uri->uri_to_assoc(4);
		$form_status = '';
		if (!empty($UriArray['form_status']))
			$form_status = $UriArray['form_status'];
		$CheckedHighlightsArray = array();
		if (!empty($UriArray['CheckedHighlightsArray'])) {
			$CheckedHighlightsArray = json_decode(urldecode($UriArray['CheckedHighlightsArray']));
		}

		$tour_id = $UriArray['tour_id'];
		$Tour_HighlightsList = $this->mtour_highlight->getTour_HighlightsList(false, '', '', $tour_id, '', '', '');
		$data = array( 
			'ControllerRef' => $this, 'Tour_HighlightsList' => $Tour_HighlightsList, 'tour_id' => $tour_id);
		$data = $this->appsmarty->AddSystemParameters($data, $this, $this->config, true);
		ob_start();
		$this->appsmarty->RunSmartyTemplate('admin/loaded_tour_highlights_list.tpl', $data, $this->config, true, false, false);
		$data = ob_get_contents();
		ob_end_clean();
		$this->output->set_content_type('application/json')->set_output(json_encode(array('ErrorMessage' => '', 'ErrorCode' => 0, 'rows_count' => count($Tour_HighlightsList), 'data' => $data)));
	}


	////////////////////////////////////////
	public function load_tour_highlights_tourstep3()
	{
		AppAuth::IsLogged($this, array('Admin', 'Operator'));
		$this->load->model('msession_data', '', true);
		$this->load->model('mtour_highlight', '', true);
		$this->load->helper('text');
		$UriArray = $this->uri->uri_to_assoc(4);
		$form_status = '';
		if (!empty($UriArray['form_status']))
			$form_status = $UriArray['form_status'];
		$current_tour_step_3= $this->get_tour_step_3_data();
		$Tour_HighlightsList = array();
		if (!empty($current_tour_step_3['highlights_list']) and is_array($current_tour_step_3['highlights_list'])) {
			$Tour_HighlightsList = $current_tour_step_3['highlights_list'];
		}
		$data = array(
			'ControllerRef' => $this, 'Tour_HighlightsList' => $Tour_HighlightsList);
		$data = $this->appsmarty->AddSystemParameters($data, $this, $this->config, true);
		ob_start();
		$this->appsmarty->RunSmartyTemplate('admin/loaded_tour_highlights_tourstep3_list.tpl', $data, $this->config, true, false, false);
		$data = ob_get_contents();
		ob_end_clean();
		$this->output->set_content_type('application/json')->set_output(json_encode(array('ErrorMessage' => '', 'ErrorCode' => 0, 'rows_count' => count($Tour_HighlightsList), 'data' => $data)));
	}

	public function get_tour_highlight_tourstep4()
	{
		AppAuth::IsLogged($this, array('Admin', 'Operator'));
		$this->load->helper('text');
		$UriArray = $this->uri->uri_to_assoc(4);
		$tour_highlight_id = $UriArray['id'];
		if ($tour_highlight_id == "new") {
			$this->output->set_content_type('application/json')->set_output(json_encode(array('ErrorMessage' => 'new', 'ErrorCode' => 0, 'id' => 0)));
			return;
		}
		if (!empty($tour_highlight_id)) {
			$TourHighlight = $this->mtour_highlight->getRowById($tour_highlight_id);
			$TourHighlight['name'] = $TourHighlight['name'];
			$TourHighlight['created_at'] = AppUtils::ShowFormattedDateTime($TourHighlight['created_at'], "ASTEXT");
			if ($TourHighlight) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('ErrorMessage' => '', 'ErrorCode' => 0, 'id' => $tour_highlight_id, 'TourHighlight' => $TourHighlight)));
				return;
			}
		}
		$this->output->set_content_type('application/json')->set_output(json_encode(array('ErrorMessage' => 'Invalid Parameters', 'ErrorCode' => 1, 'id' => 0)));
	}


	public function get_tour_highlight()
	{
		AppAuth::IsLogged($this, array('Admin', 'Operator'));
		$this->load->model('mtour', '', true);
		$this->load->model('mtour_highlight', '', true);
		$this->load->helper('text');
		$UriArray = $this->uri->uri_to_assoc(4);
		$tour_highlight_id = $UriArray['id'];
		if ($tour_highlight_id == "new") {
			$this->output->set_content_type('application/json')->set_output(json_encode(array('ErrorMessage' => 'new', 'ErrorCode' => 0, 'id' => 0)));
			return;
		}
		if (!empty($tour_highlight_id)) {
			$TourHighlight = $this->mtour_highlight->getRowById($tour_highlight_id);
			$TourHighlight['name'] = $TourHighlight['name'];
			$TourHighlight['created_at'] = AppUtils::ShowFormattedDateTime($TourHighlight['created_at'], "ASTEXT");
			if ($TourHighlight) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('ErrorMessage' => '', 'ErrorCode' => 0, 'id' => $tour_highlight_id, 'TourHighlight' => $TourHighlight)));
				return;
			}
		}
		$this->output->set_content_type('application/json')->set_output(json_encode(array('ErrorMessage' => 'Invalid Parameters', 'ErrorCode' => 1, 'id' => 0)));
	}

	public function change_tour_highlight()
	{
		AppAuth::IsLogged($this, array('Admin', 'Operator'));
		$this->load->model('mtour', '', true);
		$this->load->model('mtour_highlight', '', true);
		$UriArray = $this->uri->uri_to_assoc(4);
		$post_array = $this->input->post();
		$tour_highlight_id = AppUtils::getParameter($this, $UriArray, $post_array, 'tour_highlight_id');
		$name = AppUtils::getParameter($this, $UriArray, $post_array, 'name');
		$tour_id = AppUtils::getParameter($this, $UriArray, $post_array, 'tour_id');

		if (!empty($name)) {
			$OkResult = $this->mtour_highlight->UpdateTour_Highlight($tour_highlight_id, Array('name' => $name, 'tour_id' => $tour_id));
			if ($OkResult) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('ErrorMessage' => '', 'ErrorCode' => 0, 'id' => $tour_highlight_id, 'name' => $name)));
				return;
			}
		}
		$this->output->set_content_type('application/json')->set_output(json_encode(array('ErrorMessage' => 'Invalid Parameters', 'ErrorCode' => 1, 'id' => 0)));
	}

	public function change_tour_highlight_tourstep3()
	{
		AppAuth::IsLogged($this, array('Admin', 'Operator'));
		$this->load->model('msession_data', '', true);
		$UriArray = $this->uri->uri_to_assoc(4);
		$post_array = $this->input->post();
		$tour_highlight_id = AppUtils::getParameter($this, $UriArray, $post_array, 'tour_highlight_id');
		$name = AppUtils::getParameter($this, $UriArray, $post_array, 'name');
		$old_name_value = AppUtils::getParameter($this, $UriArray, $post_array, 'old_name_value');

		if (!empty($name)) {
			$current_tour_step_3= $this->get_tour_step_3_data();
			if (empty($current_tour_step_3)) $current_tour_step_3 = array();
			$highlights_list = array();
			if (!empty($current_tour_step_3['highlights_list'])) {
				$highlights_list = $current_tour_step_3['highlights_list'];
			}
			$KeyFound = false;
			foreach ($highlights_list as $key => $value) {
				if ($value == $old_name_value) {
					$highlights_list[$key] = $name;
					$KeyFound = true;
				}
			}
			if (!$KeyFound) $highlights_list[] = $name;
			$current_tour_step_1= $this->session->userdata("current_tour_step_1");
			$unique_session_id= 'tour_wizard_' . $current_tour_step_1['unique_session_id'];
			$DataArray= array( 'highlights_list'=> $highlights_list );
			$this->msession_data->UpdateSession_Data($unique_session_id, $DataArray);
			$this->session->set_userdata( "current_tour_step_3", array( 'unique_session_id'=> $unique_session_id ) );

			$this->output->set_content_type('application/json')->set_output(json_encode(array('ErrorMessage' => '', 'ErrorCode' => 0, 'id' => $tour_highlight_id, 'name' => $name)));
		}
	}


	public function delete_tour_highlight($id = 0)
	{
		AppAuth::IsLogged($this, array('Admin', 'Operator'));
		$this->load->model('mtour_highlight', '', true);
		$config_array = $this->config->config;
		if (!empty($id)) {
			$UriArray = $this->uri->uri_to_assoc(5);
			$PageParametersWithSort = $this->PreparePageParameters($UriArray, null, false, true);

			$OkResult = $this->mtour_highlight->DeleteTour_Highlight($id);
			if ($OkResult) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('ErrorMessage' => '', 'ErrorCode' => 0, 'id' => $id)));
				return;
			}
		}
		$this->output->set_content_type('application/json')->set_output(json_encode(array('ErrorMessage' => 'Invalid Parameters', 'ErrorCode' => 1, 'id' => 0)));
	}

	public function delete_tour_highlight_tourstep3($id = 0)
	{
		AppAuth::IsLogged($this, array('Admin', 'Operator'));
		$this->load->model('msession_data', '', true);
		$config_array = $this->config->config;
		$UriArray = $this->uri->uri_to_assoc(3);
		if (!empty($id)) {
			$current_tour_step_3= $this->get_tour_step_3_data();

			if (empty($current_tour_step_3)) $current_tour_step_3 = array();
			$Arr = array();
			if (!empty($current_tour_step_3['highlights_list'])) {
				$Arr = $current_tour_step_3['highlights_list'];
			}
			$highlights_list = array();
			foreach ($Arr as $key => $value) {
				if ($value == $id) continue;
				$highlights_list[] = $value;
			}
			$current_tour_step_3['highlights_list'] = $highlights_list;
			$current_tour_step_1= $this->session->userdata("current_tour_step_1");
			$unique_session_id= 'tour_wizard_' . $current_tour_step_1['unique_session_id'];
			$DataArray= array( 'highlights_list'=> $highlights_list );
			$Res= $this->msession_data->UpdateSession_Data($unique_session_id, $DataArray);
		}
		$this->output->set_content_type('application/json')->set_output(json_encode(array('ErrorMessage' => '', 'ErrorCode' => 0, 'id' => $id)));
	}


	public function load_tour_reviews()
	{
		AppAuth::IsLogged($this, array('Admin', 'Operator'));
		$this->load->model('mtour', '', true);
		$this->load->model('mtour_review', '', true);
		$this->load->helper('text');
		$UriArray = $this->uri->uri_to_assoc(4);
		$tour_id = $UriArray['tour_id'];
		$Tour_ReviewsList = $this->mtour_review->getTour_ReviewsList(false, '', '', $tour_id, '', '', '');
		$data = array('Tour_ReviewsList' => $Tour_ReviewsList, 'ControllerRef' => $this);
		$data = $this->appsmarty->AddSystemParameters($data, $this, $this->config, true);
		$this->appsmarty->RunSmartyTemplate('admin/loaded_tour_reviews_list.tpl', $data, $this->config, true, false, false);
	}

	public function get_tour_review()
	{
		AppAuth::IsLogged($this, array('Admin', 'Operator'));
		$this->load->model('mtour', '', true);
		$this->load->model('mtour_review', '', true);
		$this->load->helper('text');
		$UriArray = $this->uri->uri_to_assoc(4);
		$tour_review_id = $UriArray['id'];
		if (!empty($tour_review_id)) {
			$TourReview = $this->mtour_review->getRowById($tour_review_id);
			$TourReview['created_at'] = AppUtils::ShowFormattedDateTime($TourReview['created_at'], "ASTEXT");
			if ($TourReview) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('ErrorMessage' => '', 'ErrorCode' => 0, 'id' => $tour_review_id, 'TourReview' => $TourReview)));
				return;
			}
		}
		$this->output->set_content_type('application/json')->set_output(json_encode(array('ErrorMessage' => 'Invalid Parameters', 'ErrorCode' => 1, 'id' => 0)));
	}

	public function delete_tour_review($id = 0)
	{
		AppAuth::IsLogged($this, array('Admin' /* , 'Operator' */));
		$this->load->model('mtour_review', '', true);
		$config_array = $this->config->config;
		if (!empty($id)) {
			$UriArray = $this->uri->uri_to_assoc(5);
			$PageParametersWithSort = $this->PreparePageParameters($UriArray, null, false, true);

			$OkResult = $this->mtour_review->DeleteTour_Review($id);
			if ($OkResult) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('ErrorMessage' => '', 'ErrorCode' => 0, 'id' => $id)));
				return;
			}
		}
		$this->output->set_content_type('application/json')->set_output(json_encode(array('ErrorMessage' => 'Invalid Parameters', 'ErrorCode' => 1, 'id' => 0)));
	}


	public function change_tour_review()
	{
		AppAuth::IsLogged($this, array('Admin', 'Operator'));
		$this->load->model('mtour', '', true);
		$this->load->model('mtour_review', '', true);
		$this->load->model('mcms_item', '', true);
		$UriArray = $this->uri->uri_to_assoc(4);
		$post_array = $this->input->post();
		$config_array = $this->config->config;
		$tour_review_id = AppUtils::getParameter($this, $UriArray, $post_array, 'tour_review_id');
		$flag_status = AppUtils::getParameter($this, $UriArray, $post_array, 'flag_status');
		$review = AppUtils::getParameter($this, $UriArray, $post_array, 'review');
		$stars_rating_type_id = AppUtils::getParameter($this, $UriArray, $post_array, 'stars_rating_type_id');
		$LoggedUserData = AppAuth::getLoggedUserData($this);

		$TourReview = $this->mtour_review->getRowById($tour_review_id);

		if (!empty($tour_review_id) and !empty($flag_status) and !empty($TourReview)) {
			$Tour = $this->mtour->getRowById($TourReview['tour_id']);
			if ($LoggedUserData['user_role'] == 'Operator') {
				$OkResult = $this->mtour_review->UpdateTour_Review($tour_review_id, Array(
					'flag_status' => $flag_status));
			}
			if ($LoggedUserData['user_role'] == 'Admin') {
				$OkResult = $this->mtour_review->UpdateTour_Review($tour_review_id, Array(
					'flag_status' => $flag_status, 'review' => $review, 'stars_rating_type_id' => $stars_rating_type_id));
			}
			$EmailOutput = '';
			if ($OkResult) {
				if ($flag_status != '' and $flag_status != 'N' and $LoggedUserData['user_role'] == 'Operator') {
					$Title = $this->mcms_item->getBodyContentByAlias('operator_change_tour_review_flag_status',
						array('user_name' => $TourReview['full_name'],
							'flag_status' => $this->mtour_review->getTour_Review_Flag_StatusLabel($flag_status),
							'site_name' => $config_array['site_name'],
							'support_signature' => $config_array['support_signature'],
							'site_url' => $config_array['base_root_url'],
							'tour_name' => $Tour['name'],
							'tour_url' => $config_array['base_url'] . '/admin/tour/edit/' . $TourReview['tour_id']
						), false);
					$Content = $this->mcms_item->getBodyContentByAlias('operator_change_tour_review_flag_status',
						array('user_name' => $TourReview['full_name'],
							'flag_status' => $this->mtour_review->getTour_Review_Flag_StatusLabel($flag_status),
							'site_name' => $config_array['site_name'],
							'support_signature' => $config_array['support_signature'],
							'site_url' => $config_array['base_root_url'],
							'tour_name' => $Tour['name'],
							'tour_url' => $config_array['base_url'] . '/admin/tour/edit/' . $TourReview['tour_id']
						), true);
					$EmailOutput = AppUtils::SendEmail($config_array['admin_email'], $Title, $Content, $this, $config_array, true, $this->mcms_item->getCmsItemIdByAlias('operator_change_tour_review_flag_status'));
				}
				//  AppUtils::DebToFile(  json_encode(array('ErrorMessage' => '', 'ErrorCode' => 0, 'id' => $tour_review_id, 'flag_status' => $flag_status, 'EmailOutput' => $EmailOutput))   , false);
				$this->output->set_content_type('application/json')->set_output(json_encode(array('ErrorMessage' => '', 'ErrorCode' => 0, 'id' => $tour_review_id, 'flag_status' => $flag_status, 'EmailOutput' => $EmailOutput)));
				return;
			}
		}
		$this->output->set_content_type('application/json')->set_output(json_encode(array('ErrorMessage' => 'Invalid Parameters', 'ErrorCode' => 1, 'id' => 0)));
	}

	private function getHasNonEmptyArrayItems($validation_error_fields_array, $key_fields_array)
	{
		foreach ($key_fields_array as $key_field) {
			reset($validation_error_fields_array);
			foreach ($validation_error_fields_array as $validation_error_field_key => $validation_error_field_value) {
				if ($key_field == $validation_error_field_key) {
					if (!empty($validation_error_fields_array[$validation_error_field_key])) {
						return true;
					}
				}
			}
		}
		return false;
	}

}