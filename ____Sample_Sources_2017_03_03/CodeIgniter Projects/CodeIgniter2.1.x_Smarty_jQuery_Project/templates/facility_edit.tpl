<script type="text/javascript">

    jQuery(document).ready(function ($) {
        $("#name").focus()
    });


    function onSubmit(IsReopen) {
        var theForm = $("#form_facility_edit");
        $("#is_reopen").val(IsReopen)
        theForm.submit();
    }
</script>


<form action="{$base_url}admin/facility/edit/{$id}{$PageParametersWithSort}" method="post" accept-charset="utf-8" id="form_facility_edit" name="form_facility_edit" enctype="multipart/form-data">
    {$csrf_token_name_hidden}
    <input type="hidden" id="is_reopen" name="is_reopen" value="">

    <h3>{if $IsInsert ne 1}Edit{else}New{/if}&nbsp;Facility</h3>

    {if $validation_errors_text neq ''}
        <div class="error">{$validation_errors_text}
        </div>
    {/if}
    {if $editor_message neq ''}
        <div class="flash_message">
            {$editor_message}
        </div>
    {/if}

    <div style="text-align: center;margin-top: 50px;">
        <ul class="form" style="margin: auto;">
            {if $Facility neq "" and $IsInsert neq 1 }
                <li>
                    <span>Id</span>
                    <input type="text" id="id" name="id" value="{if $Facility neq ""}{$Facility.id}{/if}" size="8" readonly tabindex="-1">
                </li>
            {/if}
            <li>
                <span>Name</span>
                <input type="text" id="name" name="name" value="{if $Facility neq ""}{$Facility.name}{/if}" size="30" maxlength="50">
            </li>
            <li>
                <span>Description</span>
                <textarea id="descr" name="descr" rows="8" cols="60">{if $Facility neq ""}{$Facility.descr}{/if}</textarea>
            </li>
            {if $Facility neq "" and $IsInsert neq 1 }
                <li>
                    <span>Created At</span>
                    <input type="text" id="created_at" name="created_at" value="{show_formatted_datetime datetime_label=$Facility.created_at format= 'AsText'}" size="30" readonly tabindex="-1">
                </li>
            {/if}
            <li>
                <div>
                    <input class="grey" type="button" id="btn_submit_reopen" name="btn_submit_reopen" onclick="javascript:onSubmit(1);" value="{if $IsInsert ne 1}Update & Reopen{else}Add & Reopen{/if}">
                    <input class="grey" type="button" id="btn_submit_list" name="btn_submit_list" onclick="javascript:onSubmit(0);" value="{if $IsInsert ne 1}Update & List{else}Add & List{/if}">
                    <input class="grey" type="button" id="btn_cancel" name="btn_cancel" value="Cancel" onclick="javascript:document.location='{$base_url}admin/facility/index{$PageParametersWithSort}'">
                </div>
            </li>
        </ul>
    </div>
</form>
