<link rel="stylesheet" href="{$base_root_url}/{$css_dir}commonHT.css" type="text/css" media="all"/>
<link rel="stylesheet" href="{$base_root_url}/{$css_dir}tours.css" type="text/css" media="all"/>
<script type="text/javascript" language="JavaScript" src="{$base_root_url}/{$js_dir}jquery/jquery.fancybox.pack.js"></script>
<link rel="stylesheet" href="{$base_root_url}/{$css_dir}jquery.fancybox.css" type="text/css" media="all"/>
<script type="text/javascript" src="{$base_root_url}/{$js_dir}jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="{$base_root_url}/{$js_dir}jquery/jquery.raty.min.js"></script>

<script type="text/javascript">

jQuery(document).ready(function ($) {
    $(".fancybox-button").fancybox({
        prevEffect: 'none',
        nextEffect: 'none',
        closeBtn: true,
        openEffect: 'none',
        closeEffect: 'none',
        helpers: {
            title: { type: 'inside' },
            buttons: {}
        }
    });

    $(".contentCurent>div:first-child>ul>li:first-child>ul>li>ul>li>div").click(
            function () {
                var s = $(this).css("background-image");
                $(this).css("background-image", $(".contentCurent>div:first-child>ul>li>ul>li:first-child>a").css("background-image"));
                $(".contentCurent>div:first-child>ul>li>ul>li:first-child>a").css("background-image", s);
                $(".contentCurent>div:first-child>ul>li>ul>li:first-child>a").attr("href", s.replace('url(', '').replace(')', '').replace('"', '').replace("'", '').replace('"', '').replace("'", ''));

            }
    );
    $("#tabs").tabs();
    $('#stars_rating_type_id').raty({
        score: 0,
        starOff: '{$base_root_url}/{$images_dir}star-off.png',
        starOn: '{$base_root_url}/{$images_dir}star-on.png',
        cancel: true,
        cancelPlace: 'right',
        cancelOff: '{$base_root_url}/{$images_dir}cancel-off.png',
        cancelOn: '{$base_root_url}/{$images_dir}cancel-on.png'
    });

    load_customer_reviews()
});

function load_customer_reviews() {
    var HRef = "{$base_url}tours/load_customer_reviews/tour_id/{$Tour.id}"
    $.get(HRef, function (data) {
        $("#div_load_customer_reviews").html(data);
    });
}

function NewReview(tour_id) {
    $("#tour_new_review_full_name").val("")
    $("#tour_new_review_email_inquired").val("")
    $("#tour_new_review_review").val("")
    $('#stars_rating_type_id').raty('score', 0);

    $("#tour_new_review_dialog").dialog({
        buttons: {
            "Submit": function () {
                var tour_new_review_full_name = $("#tour_new_review_full_name").val()
                if (tour_new_review_full_name == "") {
                    alert("Enter your full name !")
                    $('#tour_new_review_full_name').focus();
                    return;
                }

                var tour_new_review_email_inquired = $("#tour_new_review_email_inquired").val()
                if (tour_new_review_email_inquired == "") {
                    alert("Enter your email!")
                    $('#tour_new_review_email_inquired').focus();
                    return;
                }

                if (!CheckEmail(tour_new_review_email_inquired)) {
                    alert("Enter valid email!")
                    $('#tour_new_review_email_inquired').focus();
                    return;
                }

                var tour_new_review_review = $("#tour_new_review_review").val()

                if (tour_new_review_review == "") {
                    alert("Enter your Review !")
                    $('#tour_new_review_review').focus();
                    return;
                }
                var stars_rating_type_id = $('#stars_rating_type_id').raty('score');

                var HRef = "{$base_url}tours/insert_tour_review"
                var DataArray = {
                    "tour_id": tour_id,
                    "email_inquired": encodeURIComponent(tour_new_review_email_inquired),
                    "full_name": encodeURIComponent(tour_new_review_full_name),
                    "review": encodeURIComponent(tour_new_review_review),
                    "stars_rating_type_id": encodeURIComponent(stars_rating_type_id),
                    'csrf_test_name': $.cookie('csrf_cookie_name')
                };
                jQuery.ajax({
                    url: HRef,
                    type: "POST",
                    data: DataArray,
                    success: onInsertedTourReview,
                    dataType: "json"
                });
            }, // "Submit": function() {
            "Close": function () {
                $(this).dialog("close");
            }
        },
        modal: true,
    }).dialog("open");
}
function onInsertedTourReview(data) {
    if (data.ErrorCode != 0) {
        alert(data.ErrorMessage)
        return
    }
    $("#tour_new_review_dialog").dialog('destroy');
    load_customer_reviews()
}

/////////////////////////////
function NewTourBookingEnquiry(tour_id) {
    $("#tour_new_enquiry_full_name").val("")
    $("#tour_new_enquiry_email").val("")
    $("#tour_new_enquiry_extro_info").val("")
    $("#tour_new_enquiry_phone").val("")
    $("#tour_new_enquiry_start_date").val("")
    $("#tour_new_enquiry_end_date").val("")
    $("#tour_new_enquiry_request_callback").prop('checked', false);

    jQuery("#tour_new_enquiry_start_date").datepicker({ // http://docs.jquery.com/UI/API/1.8/Datepicker#options
        dateFormat: '{$DatePickerSelectionFormat}',
        minDate: new Date(2012, 1, 1),
        maxDate: new Date(2028, 12, 31)
    });  // $( ".selector" ).datepicker( "option", "changeMonth", true );
    $("#tour_new_enquiry_start_date").datepicker("option", "changeMonth", true);
    $("#tour_new_enquiry_start_date").datepicker("option", "changeYear", true);
    $("#tour_new_enquiry_start_date").datepicker("option", "closeText", "Close");
    $("#tour_new_enquiry_start_date").datepicker("option", "constrainInput", true);


    jQuery("#tour_new_enquiry_end_date").datepicker({ // http://docs.jquery.com/UI/API/1.8/Datepicker#options
        dateFormat: '{$DatePickerSelectionFormat}',
        minDate: new Date(2012, 1, 1),
        maxDate: new Date(2028, 12, 31)
    });  // $( ".selector" ).datepicker( "option", "changeMonth", true );
    $("#tour_new_enquiry_end_date").datepicker("option", "changeMonth", true);
    $("#tour_new_enquiry_end_date").datepicker("option", "changeYear", true);
    $("#tour_new_enquiry_end_date").datepicker("option", "closeText", "Close");
    $("#tour_new_enquiry_end_date").datepicker("option", "constrainInput", true);

    $("#tour_new_enquiry_dialog").dialog({
        buttons: {
            "Submit": function () {
                var tour_new_enquiry_full_name = $("#tour_new_enquiry_full_name").val()
                if (tour_new_enquiry_full_name == "") {
                    alert("Enter your full name !")
                    $('#tour_new_enquiry_full_name').focus();
                    return;
                }

                var tour_new_enquiry_email = $("#tour_new_enquiry_email").val()
                if (tour_new_enquiry_email == "") {
                    alert("Enter your email!")
                    $('#tour_new_enquiry_email').focus();
                    return;
                }

                if (!CheckEmail(tour_new_enquiry_email)) {
                    alert("Enter valid email!")
                    $('#tour_new_enquiry_email').focus();
                    return;
                }

                var tour_new_enquiry_extro_info = $("#tour_new_enquiry_extro_info").val()
                var tour_new_enquiry_phone = $("#tour_new_enquiry_phone").val()

                var tour_new_enquiry_start_date = $("#tour_new_enquiry_start_date").val()
                if (tour_new_enquiry_start_date == "") {
                    alert("Enter your Check-In Date !")
                    $('#tour_new_enquiry_start_date').focus();
                    return;
                }

                var tour_new_enquiry_end_date = $("#tour_new_enquiry_end_date").val()
                if (tour_new_enquiry_end_date == "") {
                    alert("Enter your Check-Out Date !")
                    $('#tour_new_enquiry_end_date').focus();
                    return;
                }

                var tour_new_enquiry_request_callback = $("#tour_new_enquiry_request_callback").is(':checked')

                var HRef = "{$base_url}tours/insert_tour_inquery"
                var DataArray = {
                    "tour_id": tour_id,
                    "email": encodeURIComponent(tour_new_enquiry_email),
                    "phone": encodeURIComponent(tour_new_enquiry_phone),
                    "full_name": encodeURIComponent(tour_new_enquiry_full_name),
                    "start_date": encodeURIComponent(tour_new_enquiry_start_date),
                    "end_date": encodeURIComponent(tour_new_enquiry_end_date),
                    "extro_info": encodeURIComponent(tour_new_enquiry_extro_info),
                    "request_callback": tour_new_enquiry_request_callback,
                    'csrf_test_name': $.cookie('csrf_cookie_name')
                };
                jQuery.ajax({
                    url: HRef,
                    type: "POST",
                    data: DataArray,
                    success: onInsertedTourInquery,
                    dataType: "json"
                });
            }, // "Submit": function() {
            "Close": function () {
                $(this).dialog("close");
            }
        },
        modal: true,
    }).dialog("open");
}

function onInsertedTourInquery(data) {
    if (data.ErrorCode != 0) {
        alert(data.ErrorMessage)
        return
    }
    alert("Your Booking Enquiry was created and email was sent to operator of this Tour !")
    $("#tour_new_enquiry_dialog").dialog('destroy');
}

function FieldOnFocus(FieldName) {
    FieldValue = "Search for a State, City...";
    var S = $("#" + FieldName).val();
    if ($.trim(S) == FieldValue) {
        $("#" + FieldName).val("");
        jQuery('#ml_email').css('color', '#000000');
    }
}

function FieldOnBlur(FieldName) {
    FieldValue = "Search for a State, City...";
    var S = $("#" + FieldName).val();
    if ($.trim(S) == "") {
        $("#" + FieldName).val(FieldValue);
    }
}

function onChange_select_search_state() {
    var state_id = $("#select_search_state option:selected").val()
    var HRef = "{$base_url}main/get_regions_list_by_state_id/state-id/" + state_id + "/format/html";
    $.getJSON(HRef, function (data) {
        if (data.ErrorCode == 0) {
            $("#select_search_region").html(data.html)
        }
    });
}

function SearchOnClick() {
    var state = $.trim($("#search_tour_field").val())
    if (state != "" && state != "Search for a State, City...") {
        document.location = "{$base_url}tours/search_tours_by_state/state/" + state
    } else {
        var state_id = $("#select_search_state option:selected").val()
        var region_id = $("#select_search_region option:selected").val()
        var category_id = $("#select_search_category option:selected").val()

        if (state_id == '' || region_id == '') {
            if (category_id == '') {
                alert("Select state and region or category !")
                $("#select_search_state").focus()
                return;
            }
            //var HRef = "
            {$base_url}tours/search_tours_by_state/category_id/"+category_id+ "/category_name/" + $("#select_search_category option:selected").text()
            document.location = "{$base_url}tours/search_tours_by_state/category-id/" + category_id
        } else {
            var HRef_1 = "{$base_url}tours/get_state_region_slugs/state-id/" + state_id + "/region-id/" + region_id;
            $.getJSON(HRef_1, function (data) {
                if (data.ErrorCode == 0) {
                    var HRef = "{$base_url}tours/search_tours_by_region" + '/state-name/' + data.state_slug + "/state-id/" + state_id + '/region-name/' + data.region_slug + "/region-id/" + region_id
                    document.location = HRef
                }
            });

        }
    }
}


</script>

<section class="breadcrumbsBar">
    <div>
        <ul>
            <li><a href="{$base_url}">Home</a><span>\</span></li>
            <li><a href="{$base_url}tours/search_tours_by_state">Tours</a><span>\</span></li>
            <li><a>{$Tour.name}</a></li>
        </ul>
    </div>
</section>


<!-- END breadcrumbs bar -->
<section class="root">
    <ul>
        <li class="searchBox">
            {include 'search_tours_left_box_forsearch.tpl' base_url=$base_url RegionValueArray=$RegionValueArray StateValueArray=$StateValueArray state=$state region_id=$region_id state_id=$state_id state_title="" category_id=$category_id  category_name=$category_name}
        </li>
        <!-- END Tours search box -->
        <li class="contentCurent">
            <div>
                <ul>
                    <li>
                        <ul>
                            <li>
                                <a class="fancybox-button" rel="fancybox-button" href="{$Tour.default_image}" style="background-image: url('{$Tour.default_image}')"></a>
                                <ul>
                                    {assign var="images_list" value=$Tour.images_list}
                                    {section name=row loop=$images_list}
                                        <li>
                                            <div style="background-image: url('{$images_list[row]}')"></div>
                                        </li>
                                    {/section}
                                </ul>
                            </li>
                            <!-- END images_temp -->
                            <li>
                                <dl>
                                    <dt>{$Tour.name}</dt>
                                    <dd>
                                        <div>{if $Tour.state_name neq ""}{$Tour.state_name},{/if} {if $Tour.region_name neq ""}{$Tour.region_name},{/if} {if $Tour.subregion_name neq ""}{$Tour.subregion_name},{/if} Australia</div>
                                        {if $Tour.website neq "" and $Tour.feature eq 'F'}<a href="{url_with_prefix url=$Tour.website}">{$Tour.website}</a>{/if}
                                        {$Tour.descr|nl2br|stripslashes}
                                    </dd>
                                </dl>
                                <a class="dark" href="#tabs">+ Read more</a>
                            </li>
                            <!-- END text box -->
                    </li>
                </ul>

                <li class="ratingBox">
                    <div>
                        <dl>
                            <dt>Rating</dt>
                            <dd>
                                <img src="{$base_root_url}/{$images_dir}css/img/stars/stars{$Tour.reviews_avg_rating}.png">
                            </dd>
                            <dd>from {$Tour.reviews_count} review{if $Tour.reviews_count eq 1}{else}s{/if}</dd>
                        </dl>
                        <dl>
                            <dd class="tour">From AUD</dd>
                            <dt class="tour"><span>$</span>{$Tour.price}</dt>
                        </dl>
                    </div>
                    <input class="grey" type="button" value="Booking enquiry" onclick="javascript:NewTourBookingEnquiry({$Tour.id})">
                </li>
    </ul>
    </div>
    <!-- END rating and price box -->
    {if $Tour.feature eq 'F'}
    <div class="tourHighlights">
        <dl>
            <dt>Highlights</dt>
            <dd>
                <ul>
                    <li>
                        {section name=row loop=$Tour_HighlightsList}
                            <div>
                                • {$Tour_HighlightsList[row].name}
                            </div>
                        {/section}
                    </li>
                    <li>
                        {section name=row loop=$Tour_HighlightsList_2}
                            <div>
                                • {$Tour_HighlightsList_2[row].name}
                            </div>
                        {/section}
                    </li>
                </ul>
            </dd>
        </dl>
    </div>
    {/if}
    <!-- END Facilities -->
    <div class="usefulInformation">
        <dl>
            <dt>Useful Information</dt>
            <dd>
                <div id="tabs">
                    <ul>
                        <li><a href="#tabs-1">About Us</a></li>
                        <li><a href="#tabs-2">Schedule & Pricing</a></li>
                        <li><a href="#tabs-3">Additional Info</a></li>
                    </ul>
                    <div id="tabs-1">
                        {$Tour.about_us|nl2br|stripslashes}
                    </div>
                    <div id="tabs-2">
                        {$Tour.schedule_pricing|nl2br|stripslashes}
                    </div>
                    <div id="tabs-3">
                        {$Tour.additional_info|nl2br|stripslashes}
                    </div>
                </div>
            </dd>
        </dl>
    </div>
    <!-- END Useful Information -->

    <div class="customerReviews">
        <dl>
            <dt>Customer Reviews</dt>
            <dd id="div_load_customer_reviews"></dd>

            <dd>
                <div>
                    <a href="javascript:NewReview({$Tour['id']})">Submit a review</a>
                </div>
            </dd>
        </dl>
    </div>
    <!-- END Customer Reviews -->
    </li>
    <!-- END tours content -->
    </ul>
</section>

{include 'tour_new_enquiry.tpl' base_url=$base_url Tour=$Tour}
{include 'tour_new_review.tpl' base_url=$base_url Tour=$Tour}
