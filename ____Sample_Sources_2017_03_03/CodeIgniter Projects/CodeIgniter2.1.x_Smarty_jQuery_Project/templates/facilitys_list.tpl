<script type="text/javascript" language="JavaScript">
    <!--
    function DeleteFacility(id, facility_name) {
        if (!confirm("Do you want to delete '" + facility_name + "' Facility ?")) return;
        document.location = "{$base_url}admin/facility/delete/" + id
    }
    //-->
</script>

<span class="flash_message">
  {$editor_message}
</span>
<h3>Facilities List</h3>

<form action="{$base_url}admin/facility/index/page/1{$PageParametersWithSort}" method="post" accept-charset="utf-8" id="form_facilitys" name="form_facilitys" enctype="multipart/form-data">
    {$csrf_token_name_hidden}

    <input type="hidden" id="page" name="page" value="{$page}">
    <input type="hidden" id="sorting" name="sorting" value="{$sorting}">
    <input type="hidden" id="sort" name="sort" value="{$sort}">

    {if count($FacilitysList) eq 0}
        <div>No Data Found</div>
        <a href="{$base_url}admin/facility/edit/0">New Facility</a>
        {return}
    {/if}
    <div class="table_info">
        {$FacilitysList|count}&nbsp;Row{if count($FacilitysList) gt 0}s{/if}&nbsp;of&nbsp;{$RowsInTable}&nbsp;(Page # <b>{$page}</b>)
    </div>
    <div style="display: inline-block;">
        <table class="Table_List" cellspacing="0">
            <tr>
                <th>
                    Name
                </th>
                <th>
                    Description
                </th>

                <th>
                    Created At
                </th>

                <th colspan="2">&nbsp;</th>
            </tr>

            {section name=row loop=$FacilitysList}
                {cycle values='admin_row_even,admin_row_odd' assign=RowCSS}
                <tr class="{$RowCSS}">
                    <td style="min-width: 200px;">
                        {$FacilitysList[row].name}
                    </td>
                    <td style="min-width: 200px;">
                        {concat_str str=$FacilitysList[row].descr len=60}
                    </td>
                    <td style="min-width: 200px;">
                        {show_formatted_datetime datetime_label=$FacilitysList[row].created_at format= 'AsText'}
                    </td>
                    <td>
                        <a class="edit" href="{$base_url}admin/facility/edit/{$FacilitysList[row].id}{$PageParametersWithSort}">Edit</a>
                    </td>
                    <td>
                        <a class="delete" href="javascript:DeleteFacility( '{$FacilitysList[row].id}{$PageParametersWithSort}', '{$FacilitysList[row].name|addslashes}' )">Delete</a>
                    </td>
                </tr>
            {/section}
        </table>
        <ul class="table_pagination">
            <li>
                {$pagination_links}
            </li>
            <li>
                <a class="grey" href="{$base_url}admin/facility/edit/0{$PageParametersWithSort}">New Facility</a>
            </li>
        </ul>
    </div>
</form>