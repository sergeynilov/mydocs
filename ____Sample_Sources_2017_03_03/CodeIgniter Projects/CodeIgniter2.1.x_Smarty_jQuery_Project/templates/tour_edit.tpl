<script type="text/javascript" src="{$base_root_url}/{$js_dir}jquery/jquery.Jcrop.js"></script>
<script src="{$base_root_url}/{$js_dir}jquery/jQuery-File-Upload/vendor/jquery.ui.widget.js"></script>
<script src="{$base_root_url}/{$js_dir}jquery/jQuery-File-Upload/jquery.iframe-transport.js"></script>
<script src="{$base_root_url}/{$js_dir}jquery/jQuery-File-Upload/jquery.fileupload.js"></script>

<script type="text/javascript">
var ImagesPreviewCount = 0
var ImagesPreviewToUploadCommonCount = 0
var ImagesPreviewUploadedCount = 0


function MakeMultiImagesUpload() {
  {if $is_developer_comp eq 1}
  $('#fileupload').fileupload({ // https://github.com/blueimp/jQuery-File-Upload/wiki/Basic-plugin

    dataType: 'json', // demo : http://blueimp.github.io/jQuery-File-Upload/index.html
    done: function (e, data) {
      // alert( "done.result::"+var_dump(data.result) )
      ImagesPreviewCount = ImagesPreviewCount + 1
      var DivHTML = '<table style="border:0px dotted green;">';
      DivHTML = DivHTML + '<tr id="tr_image_preview_' + ImagesPreviewCount + '" >' +
        '<td style="text-align: left; vertical-align:top; width: ' + data.result['image_preview_max_width'] + 'px; "><img src="' + data.result['uploaded_file'] + '" width="' + data.result['thumbnail_width'] +
        '" height="' + data.result['thumbnail_height'] + '"  id="image_preview_' + ImagesPreviewCount + '" >&nbsp;&nbsp;</td>' +


        '<td style="text-align: left; vertical-align:top; "><small>&nbsp;&nbsp;' + data.result['name'] + '<br>&nbsp;&nbsp;' +
        '' + data.result['type'] + ',  ' +
        '' + data.result['width'] + ' * ' + data.result['height'] + ',  ' +
        '' + data.result['size'] + '</small></td>  ' +
        '<td style=" text-align: left; vertical-align:top;"><a style="cursor:pointer;" onclick="javascript:RemoveImage(' + ImagesPreviewCount + ') " >&nbsp;&nbsp;<small>Remove</small></a></td>' +
        '</tr>';
      //  alert(DivHTML)
      DivHTML = DivHTML + '</table>';
      $('#div_upload_images_preview').append(DivHTML);

    },
    progressall: function (e, data) {
      //alert( "progressall::"+var_dump(data) )
      var progress = parseInt(data.loaded / data.total * 100, 10);
      $('#progress .bar').css(
        'width',
        progress + '%'
      );
    }

  });
  //alert("AFTER UPLOAD INIT")
  {/if}
} // function MakeMultiImagesUpload() {

function ShowTourMultiImagesUpload() {

  $("#tour_images_upload_preview_dialog").dialog({
    buttons: {
      "Upload": function () {

        for (I = 1; I <= ImagesPreviewCount; I++) {
          var TrStyle = $("#tr_image_preview_" + I).css("display")
          if (TrStyle == "none") continue;
          ImagesPreviewToUploadCommonCount = ImagesPreviewToUploadCommonCount + 1
        }
        for (I = 1; I <= ImagesPreviewCount; I++) {
          var TrStyle = $("#tr_image_preview_" + I).css("display")
          if (TrStyle == "none") continue;
          var Img = $("#image_preview_" + I).attr("src")
          var HRef = "{$base_url}admin/tour/upload_image_to_tour/id/{$id}/img/" + tbUrlDecode(Img)
          $.getJSON(HRef, function (data) {
            if (data.ErrorCode == 0) {
              ImagesPreviewUploadedCount = ImagesPreviewUploadedCount + 1
              if (ImagesPreviewUploadedCount >= ImagesPreviewToUploadCommonCount) {
                $('#div_upload_images_preview').html('');
                $("#tour_images_upload_preview_dialog").dialog("close");
                ReloadImagesGallery()
                var HRef = "{$base_url}admin/tour/remove_images_tmp_dir/id/{$id}"
                $.getJSON(HRef, function (data) {
                });
              }
            }
          });

        }
      },
      "Close": function () {
        $('#div_upload_images_preview').html('');
        $(this).dialog("close");
      }
    },
    //hide: 'slide',
    //show: 'slide',
    modal: true,
    height: 620,
    width: 720
  }).dialog("open");

}

function RemoveImage(FileIndex) {
  //alert("RemoveImage(FileIndex::"+FileIndex)
  $("#tr_image_preview_" + FileIndex).css("display", "none")
}

var CroppedImageName = '';
var CroppedImageToUpload = '';
jQuery(document).ready(function ($) {
    $(".error_f").bind('change keypress', function(){
        $(this).removeClass("error_f");
    });

    {if $Tour neq "" and $IsInsert neq 1 }
  {if $is_logged eq 1 and $user_role eq "Admin" and $Tour.status eq "A" }
  $("#span_status_activate_button_label").css("display", "none")
  {/if}
  {if $is_logged eq 1 and $user_role eq "Admin" and $Tour.status eq "I" }
  $("#span_status_deactivate_button_label").css("display", "none")
  {/if}
  TourInquerysLoad()

  {if $Tour neq "" and $Tour.feature eq "F"}
  TourHighlightsLoad()
  {/if}

  TourReviewsLoad()
  {/if}
  {if $IsInsert eq 1 }
  $("#span_status_activate_button_label").css("display", "none")
  $("#span_status_deactivate_button_label").css("display", "none")
  {/if}
  $(".fancybox-button").fancybox({
    prevEffect: 'none',
    nextEffect: 'none',
    closeBtn: true,
    openEffect: 'none',
    closeEffect: 'none',
    helpers: {
      title: { type: 'inside' },
      buttons: {}
    }
  });

  var active_tab = 0;
  {if $active_tab neq "" }
  active_tab = {$active_tab}
    {/if}
    $("#tabs").tabs({  active: active_tab  });  // http://jqueryui.com/tabs/

  $("#name").focus()
  onChangeFeature()
  {if $Tour neq "" and $IsInsert neq 1 }
  onChange_state_id( '{$Tour.region_id}', '{$Tour.subregion_id}' )
  //onChange_region_id('{$Tour.subregion_id}')
  MakeMultiImagesUpload()
  {/if}
});

function onChangeFeature() {
  var feature = $('#feature').val()
  // alert( "onChangeFeature()feature::" + feature)


  if (feature == 'S') {
    $("#website").attr("readonly", "readonly");
    $("#website").css("border", "1px dotted gray")
    $("#div_LoadedHighlights").css("display", "none")
    $("#website").val("")
  }
  if (feature == 'F') {
    $("#website").removeAttr('readonly');
    $("#website").css("border", "")
    $("#div_LoadedHighlights").css("display", "block")
    $("#website").removeAttr('readonly');
    TourHighlightsLoad()
  }

}
function CropImage(ImageUrl, ImageName, ImageWidth, ImageHeight) {
  ImageUrl = ImageUrl + "?time=" + randomNumber(1000, 100); //( new Date().getTime() )
  // alert(ImageUrl)

  $(".jcrop-holder").css("width", ImageWidth);
  $(".jcrop-holder").css("height", ImageHeight)
  $("#img_cropped_image_src").attr("src", ImageUrl)
  $("#img_cropped_image_src").css("width", ImageWidth)
  $("#img_cropped_image_src").css("height", ImageHeight)
  $(".jcrop-holder>img").attr("src", ImageUrl);
  $(".jcrop-holder>img").css("width", ImageWidth);
  $(".jcrop-holder>img").css("height", ImageHeight);
  $(".jcrop-holder>div>div>img").attr("src", ImageUrl);
  $(".jcrop-holder>div>div>img").css("width", ImageWidth);
  $(".jcrop-holder>div>div>img").css("height", ImageHeight);


  $("#span_cropped_image_src_filename_label").html(ImageName + ", <small>Width</small> : " + ImageWidth + ", <small>Height</small> : " + ImageHeight + ", <small>Ratio</small> : " +
    Number((parseInt(ImageWidth) / parseInt(ImageHeight)).toFixed(1)))
  CroppedImageName = ImageName
  $("#table_cropped_image").css("display", "inline")

  jQuery('#img_cropped_image_src').Jcrop({ // http://deepliquid.com/content/Jcrop.html
    onChange: showCoords,
    onSelect: ImageSelected
  });
  RemoveCroppedImage(false)
  //alert("AFTER")

}

function showCoords(c) {
  jQuery('#span_crop_width').html(c.w);
  jQuery('#span_crop_height').html(c.h);
  jQuery('#span_crop_ratio').html(Number((parseInt(c.w) / parseInt(c.h)).toFixed(1)));
}
;

function ImageSelected(c) {
  showCoords(c)
  var HRef = '{$base_url}admin/tour/create_cropped_image/tour_id/{$id}/imagename/' + encodeURIComponent(CroppedImageName) + '/x/' + c.x + '/y/' + c.y + '/x2/' + c.x2 + '/y2/' + c.y2 + '/w/' + c.w + '/h/' + c.h;
  //alert(HRef)
  $.getJSON(HRef, function (data) {
    if (data.ErrorCode == 0) {
      //  alert( var_dump(data) )
      CroppedImageToUpload = data.tmp_image_encoded
      $("#img_cropped_image_target").attr("src", data.tmp_image_url)
      $("#img_cropped_image_target").attr("height", data.w)
      $("#img_cropped_image_target").attr("height", data.h)
      $("#img_cropped_image_target").css("display", "inline")
    } else {
      alert(data.ErroMessage)
    }
  });
}

function CancelCropping() {
  $("#table_cropped_image").css("display", "none")
  $("#img_cropped_image_target").attr("src", "")
  $("#img_cropped_image_target").attr("height", "")
  $("#img_cropped_image_target").attr("height", "")
  $("#img_cropped_image_target").css("display", "none")
  jQuery('#span_crop_width').html('');
  jQuery('#span_crop_height').html('');
  jQuery('#span_crop_ratio').html('');
  $("#input_uploaded_filename").val('')

}

function RemoveCroppedImage(ClearFiles) {
  $("#img_cropped_image_target").attr("src", "")
  $("#img_cropped_image_target").attr("height", 0)
  $("#img_cropped_image_target").attr("height", 0)
  $("#img_cropped_image_target").css("display", "none")
  jQuery('#span_crop_width').html('');
  jQuery('#span_crop_height').html('');
  jQuery('#span_crop_ratio').html('');
  $("#input_uploaded_filename").val('')
  if (ClearFiles) {
    CroppedImageName = '';
    CroppedImageToUpload = '';
  }
}

function UploadCroppedImage() {
  var input_uploaded_filename = $("#input_uploaded_filename").val()
  if (input_uploaded_filename.trim() == "") {
    alert("Enter name of file for uploading !")
    $("#input_uploaded_filename").focus()
    return;
  }
  var HRef = '{$base_url}admin/tour/upload_cropped_image/tour_id/{$id}/cropped_image_toupload/' + encodeURIComponent(CroppedImageToUpload) + '/uploaded_filename/' + encodeURIComponent(input_uploaded_filename);
  //alert(HRef)
  $.getJSON(HRef, function (data) {
    //alert( var_dump(data) )
    if (data.ErrorCode == 0) {
      ReloadImagesGallery()
      CroppedImageName = '';
      CroppedImageToUpload = '';
    } else {
      alert(data.ErrorMessage)
    }
  });
}

function ReloadImagesGallery() {
  var HRef = '{$base_url}admin/tour/load_images_gallery/tour_id/{$id}';
  // alert(HRef)
  $.get(HRef, function (data) {
    // alert( var_dump(data) )
    $("#div_ImagesGallery").html(data);
    CancelCropping()
  });

}


function onChange_state_id( p_current_region_id, p_current_subregion_id ) {
  var state_id = $("#state_id option:selected").val()
  var current_region_id = $("#region_id option:selected").val()
  if (current_region_id == "" && typeof p_current_region_id != "undefined") {
    current_region_id = p_current_region_id
  }
  var HRef = "{$base_url}main/get_regions_list_by_state_id/state-id/" + state_id + "/format/html/current-region-id/" + current_region_id;
  $.getJSON(HRef, function (data) {
    if (data.ErrorCode == 0) {
      // alert(data.html)
      $("#region_id").html(data.html)
      $("#subregion_id").html('<option value="">Select a Subregion</option>')
      onChange_region_id( p_current_subregion_id )
    }
  });
}
function onChange_region_id(p_current_subregion_id, p_current_region_id) {
  var region_id = $("#region_id option:selected").val()
  var current_subregion_id = $("#subregion_id option:selected").val()
  if (current_subregion_id == "" && typeof p_current_subregion_id != "undefined") {
    current_subregion_id = p_current_subregion_id
  }
  if (region_id == "" && typeof p_current_region_id != "undefined") {
    region_id = p_current_region_id
  }
  var HRef = "{$base_url}main/get_subregions_list_by_region_id/region-id/" + region_id + "/format/html/current-subregion-id/" + current_subregion_id;
  $.getJSON(HRef, function (data) {
    // alert( "get_subregions_list_by_region_id::"+var_dump(data) )
    if (data.ErrorCode == 0) {
      $("#subregion_id").html(data.html)
    }
  });
}


function TourInquerysLoad() {
  var HRef = "{$base_url}admin/tour/load_tour_inquerys/tour_id/{$id}";
  $.get(HRef, function (data) {
    $('#div_LoadedTourInquerys').html(data);
  });
}

function TourReviewsLoad() {
  var HRef = "{$base_url}admin/tour/load_tour_reviews/tour_id/{$id}";
  $.get(HRef, function (data) {
    $('#div_LoadedTourReviews').html(data);
  });
}
function DeleteTour_Review(id) {
  if (!confirm("Do you want to delete this Tour Review ?")) return
  var HRef = "{$base_url}admin/tour/delete_tour_review/" + id;
  $.get(HRef, function (data) {
    if (parseInt(data.ErrorCode) == 0) {
      TourReviewsLoad()
    }
  });
}

function TourHighlightsLoad() {
  var HRef = "{$base_url}admin/tour/load_tour_highlights/tour_id/{$id}"
  $.get(HRef, function (data) {
    if (data.rows_count < 2) {
      $("#span_highlights_number_label").css("display", "inline")
      $("#span_highlights_number_label").html("You have " + data.rows_count + " highlight" + ( data.rows_count > 1 || data.rows_count == 0 ? "s" : "" ) + ". You must have at least 2 highlights")
    } else {
      $("#span_highlights_number_label").css("display", "none")
      $("#span_highlights_number_label").html("")
    }
    $('#div_LoadedHighlights').html(data.data);
  });
}

///////////////////////////////////////// Tour_Inquery ////////////////////////////////////////////////
function Tour_InqueryChangeStatus(id) {
  var HRef = "{$base_url}admin/tour/get_tour_inquery/id/" + id;
  $.get(HRef, function (data) {

    if (data.ErrorCode == 0) {

      $("#tour_inquery_id").val(data.TourInquery['id'])
      $("#tour_inquery_email").val(data.TourInquery['email'])
      $("#tour_inquery_full_name").val(data.TourInquery['full_name'])
      $("#tour_inquery_phone").val(data.TourInquery['phone'])
      $("#tour_inquery_extro_info").val(data.TourInquery['extro_info'])
      $("#tour_inquery_start_date").val(data.TourInquery['start_date'])
      $("#tour_inquery_end_date").val(data.TourInquery['end_date'])
      $("#tour_inquery_request_callback").val(data.TourInquery['request_callback'])
      $("#tour_inquery_status").val(data.TourInquery['status'])
      $("#tour_inquery_created_at").val(data.TourInquery['created_at'])


      $("#tour_inquery_change_status_dialog").dialog({
        buttons: {
          "Close": function () {
            $(this).dialog("close");
          }
        },
        hide: 'slide',
        show: 'slide',
        modal: true,
        height: 520,
        width: 720
      }).dialog("open");
    } // if ( data.ErrorCode== 0 ) {
  });

}
function DeleteTour_Inquerys(id) {
  if (!confirm("Do you want to delete this Tour Inquery ?")) return
  var HRef = "{$base_url}admin/tour/delete_tour_inquery/" + id;
  $.get(HRef, function (data) {
    if (parseInt(data.ErrorCode) == 0) {
      TourInquerysLoad()
    }
  });
}


function onChangedTourInquery(data) {
  if (parseInt(data.ErrorCode) == 0) {
    TourInquerysLoad()
    alert("Status of Tour Enquiry was changed !")
    $("#tour_inquery_change_status_dialog").dialog('destroy');
  } else {
    alert((data.ErrorMessage))
  }
}
///////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////// Tour_ReviewChangeStatus( /////////////////////////////////////////
function Tour_ReviewChangeStatus(id) {
  var HRef = "{$base_url}admin/tour/get_tour_review/id/" + id;
  $.get(HRef, function (data) {
    if (data.ErrorCode == 0) {
      $("#tour_review_id").val(data.TourReview['id'])
      $("#tour_review_email_inquiried").val(data.TourReview['email_inquiried'])
      $("#tour_review_full_name").val(data.TourReview['full_name'])
      $("#tour_review_review").val(data.TourReview['review'])
      $("#tour_review_flag_status").val(data.TourReview['flag_status'])

      $("#tour_review_stars_rating_type_id").val(data.TourReview['stars_rating_type_id'])
      $("#tour_review_stars_image").html('<img src="{$base_root_url}/{$images_dir}css/img/stars/stars' + data.TourReview['stars_rating_type_id'] + '.png">')
      $("#tour_review_created_at").val(data.TourReview['created_at'])

      $("#tour_review_change_status_dialog").dialog({
        buttons: {
          "Change Status": function () {
            var tour_review_flag_status = $("#tour_review_flag_status").val()
            var tour_review_stars_rating_type_id = $("#tour_review_stars_rating_type_id").val()

            if (tour_review_flag_status == "") {
              alert("Select changed Flag Status!")
              $('#tour_review_flag_status').focus();
              return;
            }

            if (tour_review_stars_rating_type_id == "") {
              alert("Select  Review Stars Rating !")
              $('#tour_review_stars_rating_type_id').focus();
              return;
            }

            var HRef = "{$base_url}admin/tour/change_tour_review"
            //alert(HRef)
            var DataArray = {
              "tour_id": '{$id}',
              "tour_review_id": data.TourReview['id'],
              "flag_status": tour_review_flag_status,
              "review": encodeURIComponent($("#tour_review_review").val()),
              "stars_rating_type_id": encodeURIComponent($("#tour_review_stars_rating_type_id").val()),
              'csrf_test_name': $.cookie('csrf_cookie_name')
            };
            //alert( var_dump(DataArray) )


            jQuery.ajax({
              url: HRef,
              type: "POST",
              data: DataArray,
              success: onChangedTourReview,
              dataType: "json"
            });
          },
          "Close": function () {
            $(this).dialog("close");
          }
        },
        hide: 'slide',
        show: 'slide',
        modal: true,
        height: 620,
        width: 720
      }).dialog("open");
    } // if ( data.ErrorCode== 0 ) {
  });

}
function DeleteTour_Reviews(id) {
  if (!confirm("Do you want to delete this Tour Review ?")) return
  var HRef = "{$base_url}admin/tour/delete_tour_review/" + id;
  $.get(HRef, function (data) {
    if (parseInt(data.ErrorCode) == 0) {
      TourReviewsLoad()
    }
  });
}


function onChangedTourReview(data) {
  //  alert( var_dump(data) )
  if (parseInt(data.ErrorCode) == 0) {
    TourReviewsLoad()
    alert("Status of Tour Review was changed !")
    $("#tour_review_change_status_dialog").dialog('destroy');
  } else {
    alert((data.ErrorMessage))
  }
}

///////////////////////////////////////////////////////////////////////////////////////////////
function RecreatePassword() {
  if (!confirm("Do you want to recreate password and send it to tour at his email ?")) return;
  var HRef = '{$base_url}admin/tour/recreate_password/id/{$id}';

  $.getJSON(HRef, function (data) {
    //alert("onRecreatedPassword data::"+var_dump(data) )
    if (parseInt(data.ErrorCode) == 0) {
      $('#span_tour_has_password').innerHTML = "Has password"
    } else {
      $('#span_tour_has_password').innerHTML = "<b>Has no password</b>"
    }
  });
}

function SetToActive() {
  if (!confirm("Do you want Set To Active this Tour and send email to his Operator ?")) return;
  var HRef = '{$base_url}admin/tour/set_to_active/id/{$id}';

  $.getJSON(HRef, function (data) {
//    alert("SetToActive data::"+var_dump(data) )
    if (parseInt(data.ErrorCode) == 0) {
      $('#span_status_label').html("Active")
      $("#span_status_activate_button_label").css("display", "none")
      $("#span_status_deactivate_button_label").css("display", "inline")
      alert('The tour is activated and email sent to his operator!')
    }
  });
}

function SetToInactive() {
  if (!confirm("Do you want Set To Inactive this Tour and send email to his Operator ?")) return;
  var HRef = '{$base_url}admin/tour/set_to_inactive/id/{$id}';
  //alert(HRef)

  $.getJSON(HRef, function (data) {
    // alert("SetToInactive data::"+var_dump(data) )
    if (parseInt(data.ErrorCode) == 0) {
      $('#span_status_label').html("Inactive")
      $("#span_status_activate_button_label").css("display", "inline")
      $("#span_status_deactivate_button_label").css("display", "none")
      alert('The tour is deactivated and email sent to his operator!')
    }
  });
}

function onSubmit(IsReopen) {
  var theForm = $("#form_tour_edit");
  $("#is_reopen").val(IsReopen)
  theForm.submit();
}

function DeleteTourImage(Image, TourDescription) {
  if (!confirm("Do you want to remove '" + TourDescription + "' Tour Image ?")) return;
  var HRef = "{$base_url}admin/tour/delete_tour_image/tour_id/{$id}/image/" + encodeURIComponent(Image)
  document.location = HRef;
}


///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////
function Tour_HighlightEdit(id) {
  var HRef = "{$base_url}admin/tour/get_tour_highlight/id/" + id;
  //alert(HRef)
  $.get(HRef, function (data) {

    //alert("data::" + var_dump(data))
    if (data.ErrorCode == 0) {

      $("#tour_highlight_name").val( '' )


      $("#tour_highlight_edit_dialog").dialog({
        buttons: {
          "Submit": function () {
            var tour_highlight_name = $("#tour_highlight_name").val()
            if (tour_highlight_name == "") {
              alert("Enter name !")
              $('#tour_highlight_name').focus();
              return;
            }


            var HRef = "{$base_url}admin/tour/change_tour_highlight"
            var DataArray = {
              "tour_id": {$id},
              "tour_highlight_id": ( data.ErrorMessage != "new" ? data.TourHighlight['id'] : ""),
              "name": encodeURIComponent(tour_highlight_name),
              'csrf_test_name': $.cookie('csrf_cookie_name')
            };

            jQuery.ajax({
              url: HRef,
              type: "POST",
              data: DataArray,
              success: onChangedTourHighlight,
              dataType: "json"
            });
          },
          "Close": function () {
            $(this).dialog("close");
          }
        },
        hide: 'slide',
        show: 'slide',
        modal: true,
        height: 520,
        width: 720
      }).dialog("open");

    } // if ( data.ErrorCode== 0 ) {
  });

}

function Tour_HighlightDelete(id) {
  if (!confirm("Do you want to delete this Tour Highlight ?")) return
  var HRef = "{$base_url}admin/tour/delete_tour_highlight/" + id;
  $.get(HRef, function (data) {
    if (parseInt(data.ErrorCode) == 0) {
      TourHighlightsLoad()
    }
  });
}
function onChangedTourHighlight(data) {
  //alert("onChangedTourHighlight data:"+var_dump(data))
  if (parseInt(data.ErrorCode) == 0) {
    TourHighlightsLoad()
    $("#tour_highlight_edit_dialog").dialog('destroy');
    //alert("Highlight was updated !")
  } else {
    alert((data.ErrorMessage))
  }
}


</script>
<script type="text/javascript" language="JavaScript" src="{$base_root_url}/{$js_dir}jquery/jquery.fancybox.pack.js"></script>
<link rel="stylesheet" href="{$base_root_url}/{$css_dir}jquery.fancybox.css" type="text/css" media="all"/>




<form action="{$base_url}admin/tour/edit/{$id}{$PageParametersWithSort}" method="post" accept-charset="utf-8" id="form_tour_edit" name="form_tour_edit"
      enctype="multipart/form-data">
{$csrf_token_name_hidden}
<input type="hidden" id="is_reopen" name="is_reopen" value="">

<h3>{if $IsInsert ne 1}Edit{else}New{/if}&nbsp;Tour</h3>

<div class="table_info" style="margin-bottom: 30px;"><span id="span_highlights_number_label"></span></div>

{if $images_upload_display_errors neq '' or {$validation_errors_text} neq ''}
    <div class="error" style="margin-bottom: 30px;">
        {$validation_errors_text}
        {$images_upload_display_errors}
    </div>
{/if}
{if $editor_message neq ''}
    <div class="flash_message" style="margin-bottom: 30px;">
        {$editor_message}
    </div>
{/if}

  <div id="tabs">
    <ul>
      <li id="tab_header_li_Tour_Details"><a class="{if $TabTour_DetailsHasError eq 1}error{/if}" href="#tabs-Tour-Details">Tour Details</a></li>
      <li id="tab_header_li_Choose_Package"><a class="{if $TabChoose_PackageHasError eq 1}error{/if}" href="#tabs-Choose-Package">Choose Package</a></li>

      <li id="tab_header_li_Tour_Setup"><a class="{if $TabTour_SetupHasError eq 1}error{/if}" href="#tabs-Tour-Setup">Tour Setup</a></li>

      <li id="tab_header_li_Tour_Gallery"><a  class="{if $TabTour_GalleryHasError eq 1}error{/if}" href="#tabs-Tour-Gallery">Tour Gallery</a></li>

      {if $Tour neq "" and $IsInsert neq 1 }
        <li id="tab_header_li_1"><a href="#tabs-1">Tour Enquiries</a></li>
        <li id="tab_header_li_4"><a href="#tabs-4">Reviews</a></li>
      {/if}
    </ul>


    <div id="tabs-Tour-Details">
        <ul class="form">
          {if $Tour neq "" and $IsInsert neq 1 }
            <li>
              <span>Id</span>
              <input type="text" id="id" name="id" value="{if $Tour neq ""}{$Tour.id}{/if}" size="8" readonly tabindex="-1">
            </li>
          {/if}
          <li>
            <span>Name</span>
            <input type="text" id="name" name="name" value="{if $Tour neq ""}{$Tour.name}{/if}" size="50" maxlength="100"
                   placeholder="Name of Tour. Must be unique in system" {set_field_error_tag fieldname="name"}>
          </li>
            {if $is_logged eq 1 and $user_role eq "Admin"}
            <li>
              <span>Slug</span>
              <input type="text" id="slug" name="slug" value="{if $Tour neq ""}{$Tour.slug}{/if}" size="50" maxlength="150" readonly tabindex="-1" >
            </li>
            {/if}
            {if $Tour neq "" and $IsInsert neq 1 }
            <li >
              <span>Created At</span>
              <input type="text" value="{show_formatted_datetime datetime_label=$Tour.created_at format= 'AsText'}" size="8" readonly tabindex="-1">
            </li >
            <li >
              <span>Updated At</span>
              <input type="text" value="{show_formatted_datetime datetime_label=$Tour.updated_at format= 'AsText'}" size="8" readonly tabindex="-1">
            </li >
          {/if}
          <li style="display: none;"></li>
        </ul>

      <fieldset class="edit-hostel-fieldset">
        <legend>Location</legend>
        <ul class="form">
          <li>
            <span>State</span>
            <select name="state_id" id="state_id" onchange="javascript:onChange_state_id()" {set_field_error_tag fieldname="state_id"}>
              {section name=row loop=$StateValueArray}
                <option
                  value="{$StateValueArray[row].key}" {if $Tour.state_id== $StateValueArray[row].key} selected {/if}>{$StateValueArray[row].value}</option>
              {/section}
            </select>
          </li>
          <li>
            <span>Region</span>
            <select name="region_id" id="region_id" onchange="javascript:onChange_region_id()" {set_field_error_tag fieldname="region_id"}>
              {section name=row loop=$RegionValueArray}
                <option
                  value="{$RegionValueArray[row].key}" {if $Tour.region_id== $RegionValueArray[row].key} selected {/if}>{$RegionValueArray[row].value}</option>
              {/section}
            </select>
          </li>
          <li>
            <span>Area</span>
            <select name="subregion_id" id="subregion_id" {set_field_error_tag fieldname="subregion_id"}>
              {section name=row loop=$SubregionValueArray}
                <option
                  value="{$SubregionValueArray[row].key}" {if $Tour.subregion_id== $SubregionValueArray[row].key} selected {/if}>{$SubregionValueArray[row].value}</option>
              {/section}
            </select>
          </li>
          <li style="display: none;"></li>
        </ul>
      </fieldset>

    </div>


    <div id="tabs-Choose-Package">
          <ul class="form">
            <li>
              <span>Package</span>
              <select name="feature" id="feature" {set_field_error_tag fieldname="feature"} onchange="javascript:onChangeFeature()"  >
                {section name=row loop=$TourFeatureValueArray}
                  <option
                    value="{$TourFeatureValueArray[row].key}" {if $Tour.feature== $TourFeatureValueArray[row].key} selected {/if}>{$TourFeatureValueArray[row].value}</option>
                {/section}
              </select>
            </li>
            <li>
              <span>Status</span>

              <div>
                <input type="hidden" id="status" name="status" value="{$Tour.status}">
                <span id="span_status_label">{show_tour_status_label status=$Tour.status ControllerRef= $ControllerRef}</span>
                {if $is_logged eq 1 and $user_role eq "Admin"}
                  <span id="span_status_activate_button_label"><input class="grey" type="button" value="Set To Active"
                                                                      onclick="javascript:SetToActive()"></span>
                  <span id="span_status_deactivate_button_label"><input class="grey" type="button" value="Set To Inactive"
                                                                        onclick="javascript:SetToInactive()"></span>
                {/if}
              </div>
            </li>
            <li style="display: none;"></li>
          </ul>
          {include 'admin/tour_edit_packages_list.tpl'  amount_per_month_standard=$amount_per_month_standard amount_per_month_featured=$amount_per_month_featured   base_url=$base_url base_root_url=$base_root_url images_dir=$images_dir images_dir_full_url=$images_dir_full_url css_dir=$css_dir js_dir=$js_dir LoggedUserName=$LoggedUserName DateFormat=$DateFormat admin_link=$admin_link}
</div>


    <div id="tabs-Tour-Setup">
      <ul class="form" >
        <li>
          <span>Email</span>
          <input type="text" id="email" name="email" value="{if $Tour neq ""}{$Tour.email}{/if}" size="20" maxlength="50" placeholder="Tour contact email address." {set_field_error_tag fieldname="email"}>
        </li>
        <li>
          <span>Category</span>
          <select name="category_id" id="category_id" {set_field_error_tag fieldname="category_id"}>
            {section name=row loop=$CategoryValueArray}
              <option
                value="{$CategoryValueArray[row].key}" {if $Tour.category_id== $CategoryValueArray[row].key} selected {/if}>{$CategoryValueArray[row].value}</option>
            {/section}
          </select>
        </li>
        <li>
          <span>Price</span>
          <input type="text" id="price" name="price" value="{if $Tour neq ""}{$Tour.price}{/if}" size="8" maxlength="8"
                 placeholder="Price of your Tour." {set_field_error_tag fieldname="price"}>
        </li>

        <li>
          <span>Website</span>
          <input type="text" id="website" name="website" value="{if $Tour neq ""}{$Tour.website}{/if}" size="50" maxlength="150"
                 placeholder="Tour website I.e www.backpackers.com.au" {set_field_error_tag fieldname="website"}>
        </li>

        <li>
          <span>Short Description</span>
          <textarea id="short_descr" name="short_descr" rows="2" style="min-height: 0px;"
                 size="50" maxlength="150" {set_field_error_tag fieldname="short_descr"} placeholder="Search result description in 150 characters.">{if $Tour neq ""}{$Tour.short_descr}{/if}</textarea>
        </li>
        <li>
          <span>Description</span>
          <textarea name="descr" id="descr" cols="60" rows="2" placeholder="Full description of your Tour in up to 500 characters." {set_field_error_tag fieldname="descr"}>{if $Tour neq ""}{$Tour.descr}{/if}</textarea>
        </li>
        <li>
          <span>About Us</span>
          <textarea name="about_us" id="about_us" cols="60" rows="4" placeholder="About your organisation in up to 2000 characters." {set_field_error_tag fieldname="about_us"}>
              {if $Tour neq ""}{$Tour.about_us}{/if}
          </textarea>
        </li>
        <li>
          <span>Schedule Pricing</span>
          <textarea name="schedule_pricing" id="schedule_pricing" cols="60" rows="4"
                    placeholder="Your Tours Schedule & Pricing in up to 2000 characters." {set_field_error_tag fieldname="schedule_pricing"}>
              {if $Tour neq ""}{$Tour.schedule_pricing}{/if}
          </textarea>
        </li>
        <li>
          <span>Additional Information</span>
          <textarea name="additional_info" id="additional_info" cols="60" rows="4"
                    placeholder="Any other Additional Information in up to 2000 characters can be written here." {set_field_error_tag fieldname="additional_info"}>
              {if $Tour neq ""}{$Tour.additional_info}{/if}
          </textarea>
        </li>
      </ul>

      <div id="div_LoadedHighlights"></div>
    </div>



    <div id="tabs-Tour-Gallery">
        <div id="div_ImagesGallery">
            {$ImagesGallery}
        </div>
        <ul class="form">
          {if $is_developer_comp eq 1 and $Tour neq "" and $IsInsert neq 1 }
          <li>
          <input class="grey" type="button" value="Multi Images Upload" onclick="javascript:ShowTourMultiImagesUpload()">
            <div id="tour_images_upload_preview_dialog" title="tour Images Upload Preview" style="display:none; width:500px;">
              <ul class="form">
                <li>
                  Select files for uploading
                </li>
                <li>
                  <input id="fileupload" type="file" name="files[]"
                         data-url="{$base_url}make_fileupload.php?tmp_directory={$tmp_directory}&tmp_url_directory={$tmp_url_directory}&image_preview_width={$image_preview_width}&image_preview_height={$image_preview_height}"
                         multiple>
                </li>
                <li>
                  <div id="progress">
                    <div class="bar" style="width: 0%;">Progress Bar Div</div>
                  </div>
                </li>
                <li>
                  <div id="div_upload_images_preview" style="border:0px dotted green; max-height:500px; overflow-y:auto;"></div>
                </li>
              </ul>
            </div>
          </li>
          {/if}
        <li style="color: #858f90;font-size: 10px;">
          The image must have size 145px × 100px or similar proportions for best display.
        </li>
          <li>
              <span>Add Image</span>
              <div>
                <input type="file" id="add_image" name="add_image" value="" size="50" maxlength="255">
              </div>
          </li>

          <li style="display: none;"></li>
      </ul>
    </div>


    <div id="tabs-1">
      <div id="div_LoadedTourInquerys"></div>
    </div>


    {* <div id="tabs-3">
      <div id="div_LoadedHighlights"></div>
    </div>  *}
    <div id="tabs-4">
      <div id="div_LoadedTourReviews"></div>
    </div>
  </div>


<ul class="edit-hostel-wrapper-1" style="width: 1040px;margin-top: 20px;">
  <li style="text-align: right;">
    <div style="display: inline-block;margin-left: auto;">
      <input class="grey" type="button" id="btn_submit_reopen" name="btn_submit_reopen" onclick="javascript:onSubmit(1);"
             value="{if $IsInsert ne 1}Update & Reopen{else}Add & Reopen{/if}">
      <input class="grey" type="button" id="btn_submit_list" name="btn_submit_list" onclick="javascript:onSubmit(0);"
             value="{if $IsInsert ne 1}Update & List{else}Add & List{/if}">
      <input class="grey" type="button" id="btn_cancel" name="btn_cancel" value="Cancel"
             onclick="javascript:document.location='{$base_url}admin/tour/index{$PageParametersWithSort}'">
    </div>
  </li>
</ul>

</form>

<div id="tour_inquery_change_status_dialog" title="View Booking Tour Enquiry" style="display:none">
  <ul class="form">
    <li>
      <span>Id</span>
      <input type="text" id="tour_inquery_id" name="tour_inquery_id" value="" size="8" readonly tabindex="-1">
    </li>
    <li>
      <span>Email</span>
      <input type="text" id="tour_inquery_email" name="tour_inquery_email" value="" size="30" maxlength="50" readonly tabindex="-1">
    </li>
    <li>
      <span>Full Name</span>
      <input type="text" id="tour_inquery_full_name" name="tour_inquery_full_name" value="" size="30" maxlength="10" readonly tabindex="-1">
    </li>
    <li>
      <span>Extro Info</span>
      <textarea id="tour_inquery_extro_info" name="tour_inquery_extro_info" value="" rows="8" cols="50" readonly></textarea>
    </li>

    {* <li>
      <span>Check-In Date</span>
      <input type="text" id="tour_inquery_start_date" name="tour_inquery_start_date" value="" size="20" maxlength="20" readonly tabindex="-1">
    </li>
    <li>
      <span>Check-Out Date</span>
      <input type="text" id="tour_inquery_end_date" name="tour_inquery_end_date" value="" size="20" maxlength="20" readonly tabindex="-1">
    </li>  *}

    <li>
      <span>Request Callback</span>
      <input type="text" id="tour_inquery_request_callback" name="tour_inquery_request_callback" value="" size="3" maxlength="5" readonly tabindex="-1">
    </li>

    {* <tr>
      <th class="editor_th_cell">
        Status&nbsp;:
      </th>
      <td>
        <select name="tour_inquery_status" id="tour_inquery_status" >
      {section name=row loop=$Tour_InqueryStatusValueArray}
          <option value="{$Tour_InqueryStatusValueArray[row].key}" {if $Tour.status== $Tour_InqueryStatusValueArray[row].key} selected {/if}>{$Tour_InqueryStatusValueArray[row].value}</option>
      {/section}
        </select>
      </td>
    </tr>  *}

    <li>
      <span>Created At</span>
      <input type="text" id="tour_inquery_created_at" name="tour_inquery_created_at" value="" size="20" maxlength="20" readonly tabindex="-1">
    </li>
    <li style="display: none"></li>
  </ul>
</div>


<div id="tour_review_change_status_dialog" title="Tour Review Change Status" style="display:none">

  <ul class="form">
    <li>
      <span>Id</span>
      <input type="text" id="tour_review_id" name="tour_review_id" value="" size="8" readonly tabindex="-1">
    </li>
    <li>
      <span>Email Inquiried</span>
      <input type="text" id="tour_review_email_inquiried" name="tour_review_email_inquiried" value="" size="30" maxlength="50" readonly tabindex="-1">
    </li>
    <li>
      <span>Full Name</span>
      <input type="text" id="tour_review_full_name" name="tour_review_full_name" value="" size="30" maxlength="10" readonly tabindex="-1">
    </
    <li>
    <li>
      <span>Review</span>
      <textarea id="tour_review_review" name="tour_review_review" value="" rows="6" cols="50"
                {if $is_logged eq 1 and $user_role eq "Operator"}readonly{/if} ></textarea>
    </li>
    <li>
      <span>Flag Status</span>
      <select name="tour_review_flag_status" id="tour_review_flag_status">
        {section name=row loop=$Tour_Review_Flag_StatusValueArray}
          <option
            value="{$Tour_Review_Flag_StatusValueArray[row].key}" {if $Tour.status== $Tour_Review_Flag_StatusValueArray[row].key} selected {/if}>{$Tour_Review_Flag_StatusValueArray[row].value}</option>
        {/section}
      </select>
    </li>
    <li>
      <span>Review Stars Rating</span>

      <div>
        <select name="tour_review_stars_rating_type_id" id="tour_review_stars_rating_type_id"
                {if $is_logged eq 1 and $user_role eq "Operator"}disabled{/if} >
          {section name=row loop=$Tour_Review_Stars_Rating_TypeArray}
            <option
              value="{$Tour_Review_Stars_Rating_TypeArray[row].key}" {if $Tour.status== $Tour_Review_Stars_Rating_TypeArray[row].key} selected {/if}>{$Tour_Review_Stars_Rating_TypeArray[row].value}</option>
          {/section}
        </select>
        <span id="tour_review_stars_image"></span>
      </div>
    </li>
    <li>
      <span>Created At</span>
      <input type="text" id="tour_review_created_at" name="tour_review_created_at" value="" size="20" maxlength="20" readonly tabindex="-1">
    </li>
    <li style="display: none;"></li>
  </ul>
</div>

<div id="tour_highlight_edit_dialog" title="Add Highlight" style="display:none">
  <ul class="form">


    <li>
      <span>Name</span>
      <input type="text" id="tour_highlight_name" name="tour_highlight_name" value="" size="60" maxlength="255">
    </li>


    <li style="display: none"></li>
  </ul>
</div>
