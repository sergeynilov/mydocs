<?php

class Mtour extends CI_Model {

  private $TourFeatureValueArray = Array('F' => 'Featured', 'S' => 'Standard' );
  private $TourStatusLabelValueArray = Array('A' => 'Active', 'I' => 'Inactive', 'N' => 'New');

  public function __construct() {
    parent::__construct();
  }


  public function getTourFeatureValueArray() {
    $ResArray = array();
    foreach ($this->TourFeatureValueArray as $Key => $Value) {
      $ResArray[] = array('key' => $Key, 'value' => $Value);
    }
    return $ResArray;
  }

  public function getTourFeatureLabel($Type) {
    if (!empty($this->TourFeatureValueArray[$Type])) {
      return $this->TourFeatureValueArray[$Type];
    }
    return '';
  }

  public function getTourStatusValueArray() {
    $ResArray = array();
    foreach ($this->TourStatusLabelValueArray as $Key => $Value) {
      $ResArray[] = array('key' => $Key, 'value' => $Value);
    }
    return $ResArray;
  }
  public function getTourStatusLabel($Type) {
    if (!empty($this->TourStatusLabelValueArray[$Type])) {
      return $this->TourStatusLabelValueArray[$Type];
    }
    return '';
  }


  public function getToursList($OutputFormatCount = false, $page = '', $filters= array(), $sorting = '', $sort = '' ) {
    if (empty($sorting))
      $sorting = 'tour.feature, tour.name';
    $config_data = $this->config->config;

		if ( !empty($filters['tours_rows_on_page']) ) {
			$per_page= $filters['tours_rows_on_page'];
		}
    if ( empty($per_page) ) $per_page= $config_data['per_page'];
    $limit = '';
    $offset = '';
    if (!empty($config_data) and $page > 0) {
      $limit = $per_page;
      $offset = ($page - 1) * $per_page;
    }

    if ( !empty($rows_limit) and $rows_limit > 0 ) {
      $limit= $rows_limit;
      $this->db->limit($limit);
    }
    if (!$OutputFormatCount and isset($limit) and (int) $limit > 0 and isset($offset) and is_numeric($page)) {
      $this->db->limit($limit, $offset);
    }

    if ( !empty($filters['filter_user_id']) ) {
      $this->db->where( 'user_id', $filters['filter_user_id'] );
    }

		if ( !empty($filters['filter_category_id']) ) {
			if ( is_array($filters['filter_category_id']) ) {

				if ( !AppUtils::is_empty_array($filters['filter_category_id']) ) {
  			  $this->db->where_in( 'category_id', $filters['filter_category_id'] );
				}
			} else {
				$this->db->where( 'category_id', $filters['filter_category_id'] );
			}
		}

    if ( !empty($filters['filter_feature']) ) {
      $this->db->where( 'feature', $filters['filter_feature'] );
    }

    if (!empty($filters['filter_state'])) {
			$or_condition = " ( ".  $this->db->dbprefix ."region.name like '%".$filters['filter_state']."%' OR  ".
				$this->db->dbprefix ."region.slug like '%".$filters['filter_state']."%' OR  ".
				$this->db->dbprefix ."subregion.name like '%".$filters['filter_state']."%' OR  ".
				$this->db->dbprefix ."subregion.slug like '%".$filters['filter_state']."%' OR  ".
				$this->db->dbprefix ."state.name like '%".$filters['filter_state']."%' OR " .
				$this->db->dbprefix ."tour.name like '%".$filters['filter_state']."%' OR " .
			  $this->db->dbprefix ."state.slug like '%".$filters['filter_state']."%' ) " ;
			$this->db->where($or_condition, '', false);
    }

		if (!empty($filters['filter_active_tours_list'])) {
			$this->db->where_in('tour.id', $filters['filter_active_tours_list'] );
		}



    if (!empty($filters['filter_state_id'])) {
      $this->db->where('tour.state_id', $filters['filter_state_id']);
    }

    if (!empty($filters['filter_region_id'])) {
      $this->db->where('tour.region_id', $filters['filter_region_id']);
    }

		if (!empty($filters['filter_subregion_id'])) {
			$this->db->where('tour.subregion_id', $filters['filter_subregion_id']);
		}

    if (!empty($filters['filter_status'])) {
      $this->db->where('tour.status', $filters['filter_status']);
    }
    $sorting_column= ''; $is_sorting_set= false; $calc_columns= '';
    if (!empty($sorting)) {
      if ( strtolower($sorting) == 'review_rating' ) {
        $is_sorting_set= true;
        if ( !$OutputFormatCount ) {
          $sorting_column= ', ( select avg(' . $this->db->dbprefix . 'tour_review.stars_rating_type_id) from `' . $this->db->dbprefix . 'tour_review` where `' . $this->db->dbprefix . 'tour_review`.`tour_id` = `' . $this->db->dbprefix .
            'tour`.`id` and `'. $this->db->dbprefix . 'tour_review`.`status` = \'A\' ) as tour_review_stars_rating_type_id ';
          $this->db->order_by( 'tour.feature, tour_review_stars_rating_type_id', 'desc' );
        }
      }

      if ( strtolower($sorting) == 'tours_by_feature_price' ) {
				$is_sorting_set= true;
				if ( !$OutputFormatCount ) {
					$this->db->order_by( 'tour.feature, tour.price', 'desc' );
				}
			}

      if ( strtolower($sorting) == 'tours_by_feature_name' ) {
				$is_sorting_set= true;
				if ( !$OutputFormatCount ) {
					$this->db->order_by( 'tour.feature asc, tour.name asc', '' );
				}
			}

			if ( !$is_sorting_set ) {
        $this->db->order_by($sorting, ( ( strtolower($sort) == 'desc' or strtolower($sort) == 'asc' ) ? $sort : ''));
      }
    }  
    if ( !empty($filters['filter_rating_value']) ) {
      $calc_columns= ', ( select avg(' . $this->db->dbprefix . 'tour_review.stars_rating_type_id) from `' . $this->db->dbprefix . 'tour_review` where `' . $this->db->dbprefix . 'tour_review`.`tour_id` = `' . $this->db->dbprefix .
            'tour`.`id` and `'. $this->db->dbprefix . 'tour_review`.`status` = \'A\' ) as tour_review_stars_rating_type_id ';
      $this->db->having( 'tour_review_stars_rating_type_id >= ' . $filters['filter_rating_value'] );
    }

    $this->db->join( 'region', 'region.id = tour.region_id', 'left' );
		$this->db->join( 'subregion', 'subregion.id = tour.subregion_id', 'left' );
    $this->db->join( 'state', 'state.id = tour.state_id', 'left' );
		$this->db->join( 'category', 'category.id = tour.category_id', 'left' );
    $this->db->join( 'user', 'user.id = tour.user_id', 'left' );
    $this->db->select(  ( ( !empty($filters['fields_for_select']) and strlen(trim($filters['fields_for_select'])) > 0)?"":'tour.*, ')  .
			'region.name as region_name, region.slug as region_slug, subregion.name as subregion_name, subregion.slug as subregion_slug, state.name as state_name, state.slug as state_slug, category.name as category_name, user.login as user_login' . $sorting_column  . $calc_columns  );


		if ($OutputFormatCount) {
      return $this->db->count_all_results('tour');
    } else {
      $this->db->distinct();
      $query = $this->db->from('tour');
      if ( !empty($filters['fields_for_select']) and strlen(trim($filters['fields_for_select'])) > 0) {
        $query->select($filters['fields_for_select']);
      }
      if ( !empty($filters['show_default_image']) ) {
        $result_array= $query->get()->result('array');
        foreach( $result_array as $key=>$Tour ) {
          $result_array[$key]['default_image']= $this->get_default_image( $Tour['id'] );
					$result_array[$key]['default_image_path']= $this->get_default_image_path( $Tour['id'] );
          $result_array[$key]['images_list']= $this->get_images_list( $Tour['id'] );
        }
        return $result_array;
      }
      return $query->get()->result('array');
    }
  }

  private function get_default_image( $tour_id ) {
    $config_array = $this->config->config;
    $filename_path = $config_array['document_root'] . $config_array['uploads_tours_dir'] . '-tour-' . $tour_id . DIRECTORY_SEPARATOR;
    $TourImages = get_filenames($filename_path);
    if (empty($TourImages) or !is_array($TourImages)) {
      return $config_array['base_root_url'] .DIRECTORY_SEPARATOR .$config_array['images_dir'] .DIRECTORY_SEPARATOR .  $config_array['empty_tour_image'];
      return '';
    }
    $TourImage= $TourImages[0];
    return $config_array['base_root_url'] . DIRECTORY_SEPARATOR . $config_array['uploads_tours_dir'] . '-tour-' . $tour_id . DIRECTORY_SEPARATOR . $TourImage;
  }

	private function get_default_image_path( $tour_id ) {
		$config_array = $this->config->config;
		$filename_path = $config_array['document_root'] . $config_array['uploads_tours_dir'] . '-tour-' . $tour_id . DIRECTORY_SEPARATOR;
		$ToursImages = get_filenames($filename_path);
		if (empty($ToursImages) or !is_array($ToursImages)) {
			return $config_array['document_root'] . DIRECTORY_SEPARATOR . $config_array['images_dir'] . $config_array['empty_tour_image'];
			return '';
		}
		$ToursImage= $ToursImages[0];
		return $config_array['document_root'] . DIRECTORY_SEPARATOR . $config_array['uploads_tours_dir'] . '-tour-' . $tour_id . DIRECTORY_SEPARATOR . $ToursImage;
	}

  private function get_images_list( $tour_id, $skip_first_image= true ) {
    $config_array = $this->config->config;
    $filename_path = $config_array['document_root'] . $config_array['uploads_tours_dir'] . '-tour-' . $tour_id . DIRECTORY_SEPARATOR;
    $TourImages = get_filenames($filename_path);
    if (empty($TourImages) or !is_array($TourImages)) {
      return $config_array['base_root_url'] .DIRECTORY_SEPARATOR .$config_array['images_dir'] .DIRECTORY_SEPARATOR .  $config_array['empty_tour_image'];
      return '';
    }
    $ResArray= array();
    $max_images_in_tour= $config_array['max_images_in_tour'];
    $I=0;
    foreach( $TourImages as $TourImage ) {
      $I++;
      if ( $I > $max_images_in_tour ) return $ResArray;
      if ( $skip_first_image and $I == 1 ) continue;
      $ResArray[]= $config_array['base_root_url'] . DIRECTORY_SEPARATOR . $config_array['uploads_tours_dir'] . '-tour-' . $tour_id . DIRECTORY_SEPARATOR . $TourImage;
    }
    return$ResArray;
  }



  public function getFlaggedReviewToursList( $OutputFormatCount= false, $filter_user_id = '', $sorting = '', $sort = '' ) {
    $prefix= $this->db->dbprefix;
    if (!empty($filter_user_id)) {
      $this->db->where('user_id', $filter_user_id);
    }
    $this->db->join( 'user', 'user.id = tour.user_id', 'left' );
    $flagged_tour_reviews_sql= ' , ( select count(*) from `' . $prefix . 'tour_review` where `' . $prefix . 'tour_review`.`tour_id` = `' . $prefix . 'tour`.`id` and ' . $prefix . 'tour_review.flag_status!= \'N\') as flagged_tour_review_count ' ;
    $this->db->select( 'tour.id, tour.name, tour.user_id, tour.updated_at, user.login as user_login' . $flagged_tour_reviews_sql );
    $this->db->having( '`flagged_tour_review_count` > 0 ' );
    if ($OutputFormatCount) {
      return $this->db->count_all_results('tour');
    } else {
      $query = $this->db->from('tour');
      return $query->get()->result('array');
    }
  }

  public function getNewInquirysToursList( $OutputFormatCount= false, $filter_user_id = '', $sorting = '', $sort = '' ) {
    $prefix= $this->db->dbprefix;
    if (!empty($filter_user_id)) {
      $this->db->where('user_id', $filter_user_id);
    }
    $this->db->join( 'user', 'user.id = tour.user_id', 'left' );

    $tour_new_inquirys_sql= ' , ( select count(*) from `' . $prefix . 'tour_inquery` where `' . $prefix . 'tour_inquery`.`tour_id` = `' . $prefix . 'tour`.`id` and ' . $prefix .
    'tour_inquery.status = \'N\') as tour_new_inquery_count ' ;

    $this->db->select( 'tour.id, tour.updated_at, tour.name, tour.user_id, user.login as user_login' . $tour_new_inquirys_sql );
    $this->db->having( '`tour_new_inquery_count` > 0 ' );

    if ($OutputFormatCount) {
      return $this->db->count_all_results('tour');
    } else {
      $query = $this->db->from('tour');
      return $query->get()->result('array');
    }

  }

  public function getRowById($id, $show_default_image= '') {

    $this->db->join( 'region', 'region.id = tour.region_id', 'left' );
		$this->db->join( 'subregion', 'subregion.id = tour.subregion_id', 'left' );
    $this->db->join( 'state', 'state.id = tour.state_id', 'left' );
    $this->db->join( 'user', 'user.id = tour.user_id', 'left' );
    $this->db->select( 'tour.*, region.name as region_name, region.slug as region_slug, subregion.name as subregion_name, subregion.slug as subregion_slug, state.name as state_name, state.slug as state_slug, user.login as user_login' );


    $query = $this->db->get_where('tour', array('tour.id' => $id), 1, 0);
    $ResultRow = $query->result('array');
    if (!empty($ResultRow[0])) {
      if ( $show_default_image ) {
        $ResultRow[0]['default_image']= $this->get_default_image( $id );
        $ResultRow[0]['images_list']= $this->get_images_list( $id );
        return $ResultRow[0];
      }
      return $ResultRow[0];
    }
    return false;
  }

  public function getRowByFieldName($field_name, $value, $get_multy_rows= false, $show_default_image= '') {
    if ( !$get_multy_rows ) {
      $query = $this->db->get_where('tour', array($field_name=>$value), 1, 0);
      $ResultRow = $query->result('array');
      if (!empty($ResultRow[0])) {
        if ( $show_default_image ) {
          $ResultRow[0]['default_image']= $this->get_default_image( $ResultRow[0]['id'] );
          $ResultRow[0]['images_list']= $this->get_images_list( $ResultRow[0]['id'] );
        }
        return $ResultRow[0];
      }
    } else {
      $query = $this->db->get_where('tour', array($field_name=>$value) );
      $ResultRows = $query->result('array');
      if (!empty($ResultRows)) {
        return $ResultRows;
      }
    }
    return false;
  }

  public function UpdateTour($id, $DataArray) {
    if (empty($id)) {
      $DataArray['created_at'] = AppUtils::ShowFormattedDateTime(time(), 'MySql');
      $DataArray['updated_at'] = AppUtils::ShowFormattedDateTime(time(), 'MySql');
      $Res = $this->db->insert('tour', $DataArray);
      if ($Res)
        return mysql_insert_id();
    } else {
      $DataArray['updated_at'] = AppUtils::ShowFormattedDateTime(time(), 'MySql');
      $Res = $this->db->update('tour', $DataArray, array('id' => $id));
      if ($Res)
        return $id;
    }
  }

  public function DeleteTour($id) {
    if (!empty($id)) {
      $this->db->where('id', $id);
      $Res = $this->db->delete('tour');
      return $Res;
    }
  }

  public function getRowByPassword($password) {
    $query = $this->db->get_where('tour', array('password' => $password), 1, 0);
    $ResultRow = $query->result('array');
    if (!empty($ResultRow[0])) {
      return $ResultRow[0];
    }
    return false;
  }


  public function UploadTourImage($ControllerRef, $TourId, $files_array, $post_array, $config_array) {
    $dir_path =  $config_array['document_root'] . $config_array['uploads_tours_dir'] . '-tour-' . $TourId . DIRECTORY_SEPARATOR;
    if (!is_dir($dir_path)) {
      mkdir($dir_path, 0777);
    }
    $upload_config['upload_path'] = $dir_path;
    if ( !empty($config_array['uploads_images_allowed_types']) ) $upload_config['allowed_types'] = $config_array['uploads_images_allowed_types'];
    if ( !empty($config_array['uploads_images_max_size']) ) $upload_config['max_size'] = $config_array['uploads_images_max_size'];
    if ( !empty($config_array['uploads_images_max_width']) ) $upload_config['max_width'] = $config_array['uploads_images_max_width'];
    if ( !empty($config_array['uploads_images_max_height']) ) $upload_config['max_height'] = $config_array['uploads_images_max_height'];

    $ControllerRef->load->library('upload', $upload_config);
    $add_image= '';

    if ( !empty($files_array['add_image']) ) {
      $ControllerRef->upload->do_upload('add_image');
      $add_image = $files_array['add_image']['name'];
    }
    $images_upload_display_errors = $ControllerRef->upload->display_errors();

    return $images_upload_display_errors;
  }

	public function UpdateAllPricing( $ControllerRef, $tour_standard, $tour_featured ) {

		$ToursList = $this->mtour->getToursList( false, '');
		foreach( $ToursList as $Tour ) { //get list of all tours in system
			AppUtils::deb($Tour, '$Tour::');
			if ( $Tour['feature']=="S" ) {
				$Res = $ControllerRef->mtour->UpdateTour( $Tour['id'], array('price'=>$tour_standard) );
			}
			if ( $Tour['feature']=="F" ) {
				$Res = $ControllerRef->mtour->UpdateTour( $Tour['id'], array('price'=>$tour_featured) );
			}
		}
		return count($ToursList);
	}

}