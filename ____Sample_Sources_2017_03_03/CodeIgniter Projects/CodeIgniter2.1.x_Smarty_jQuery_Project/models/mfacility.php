<?php

class Mfacility extends CI_Model {

  public function __construct() {
    parent::__construct();
  }


  public function getFacilitysSelectionList($OutputFormatCount = false, $page = '', $filter_name = '', $sorting = '', $sort = '') {
    $facilitysList = $this->mfacility->getFacilitysList(false, '', '', 'name', '', 'id,name');
    $ResArray = array();
    foreach ($facilitysList as $lfacility) {
      $ResArray[] = array('key' => $lfacility['id'], 'value' => $lfacility['name']);
    }
    return $ResArray;
  }

  // getfacilitysList(false, $page, $filter_name, $sorting, $sort, '', false);
  public function getFacilitysList($OutputFormatCount = false, $page = '', $filter_name = '', $sorting = '', $sort = '', $fields_for_select = '', $show_related_count= false) {
    if (empty($sorting))
      $sorting = 'name';
    $config_data = $this->config->config;

    $limit = '';
    $offset = '';
    if (!empty($config_data) and $page > 0) {
      $limit = $config_data['per_page'];
      $offset = ($page - 1) * $config_data['per_page'];
    }

    if (!$OutputFormatCount and isset($limit) and (int) $limit > 0 and isset($offset) and is_numeric($page)) {
      $this->db->limit($limit, $offset);
    }

    if (!empty($filter_name)) {
      $this->db->like('name', $filter_name);
    }

    if (!empty($sorting)) {
      $this->db->order_by($sorting, ( ( strtolower($sort) == 'desc' or strtolower($sort) == 'asc' ) ? $sort : ''));
    }

    if ($show_related_count) {
      $this->db->select('facility.*, ( select count(*) from `' . $this->db->dbprefix . 'property` where `' . $this->db->dbprefix . 'property`.`landlord_id` = `' . $this->db->dbprefix . 'facility`.`id`) as property_facility_count ');
    }

    if ($OutputFormatCount) {
      return $this->db->count_all_results('facility');
    } else {
      $query = $this->db->from('facility');
      if (strlen(trim($fields_for_select)) > 0) {
        $query->select($fields_for_select);
      }
      return $query->get()->result('array');
    }
  }

  public function getRowById($id) {
    $query = $this->db->get_where('facility', array('facility.id' => $id), 1, 0);
    $ResultRow = $query->result('array');
    if (!empty($ResultRow[0])) {
      return $ResultRow[0];
    }
    return false;
  }

  public function UpdateFacility($id, $DataArray) {
    if (empty($id)) {
      $DataArray['created_at'] = AppUtils::ShowFormattedDateTime(time(), 'MySql');
      $Res = $this->db->insert('facility', $DataArray);
      if ($Res)
        return mysql_insert_id();
    } else {
      $Res = $this->db->update('facility', $DataArray, array('id' => $id));
      if ($Res)
        return $id;
    }
  }

  public function DeleteFacility($id) {
    if (!empty($id)) {
      $this->db->where('id', $id);
      $Res = $this->db->delete('facility');
      return $Res;
    }
  }


}
?>