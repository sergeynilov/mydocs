<?php
class AppTwig
{
    private $m_twig_Environment;
    public function __construct()
    {
    }

    public function setParemeters($twig_Environment/*, $config_array*/) {
        $this->m_twig_Environment= $twig_Environment;

    }


    public function registerUserFunctions()
    {

        $filter = new Twig_SimpleFilter('concat_conditional_values', function ( $splitter, $values_array, $default_value ) {
            return AppUtils::concat_conditional_values( $values_array, $splitter, $default_value );
        });
        $this->m_twig_Environment->addFilter($filter);



        $filter = new Twig_SimpleFilter('set_content_paragraph', function ( $str ) {
            return preg_replace('~<p[^>]*>~','<p class="content_p">',$str);
        }); //http://forum.php.su/topic.php?forum=4&topic=2748
        $this->m_twig_Environment->addFilter($filter);


        $filter = new Twig_SimpleFilter('url_with_prefix', function ( $url ) {
            $A = preg_split('/http:\/\//', $url);
            if (count($A) == 1) {
                return 'http://' . $url;
            }
            return $url;
        });
        $this->m_twig_Environment->addFilter($filter);




        $filter = new Twig_SimpleFilter('show_formatted_price', function ( $price, $money_label, $money_class ) {
            if (empty($price) or !is_numeric($price)) return '<small>Have no price</small>';
            if ( !empty($money_label) and !empty($money_class) ) {
                return '<span class="'.$money_class.'">'.$money_label.$price.'</span>';
            }
            return $price;
        });
        $this->m_twig_Environment->addFilter($filter);


        /* Hostels Block Start */
        $filter = new Twig_SimpleFilter('show_hostel_review_stars_rating_type_label_label', function ( $stars_rating ) {
            return ReplaceSpaces(get_instance()->mhostel_review->getHostel_Review_Stars_Rating_TypeLabel($stars_rating) );
        });
        $this->m_twig_Environment->addFilter($filter);


        $filter = new Twig_SimpleFilter('get_hostel_room_type_label', function ( $room_type ) {
            $ci = & get_instance();
            $label= $ci->mhostel_room->getHostel_RoomTypeLabel($room_type);
            return $label['name'];
        });
        $this->m_twig_Environment->addFilter($filter);


        $filter = new Twig_SimpleFilter('show_hostel_make_view_url', function ($hostelName, $Hostel) {
            return 'hostel/' .
            (!empty($Hostel['state_slug']) ? $Hostel['state_slug'] : "-") . '/' .
            (!empty($Hostel['region_slug']) ? $Hostel['region_slug'] : '-') . '/' .
            (!empty($Hostel['subregion_slug']) ? $Hostel['subregion_slug'] : '-') . '/' .
            (!empty($Hostel['slug']) ? $Hostel['slug'] : '-');
        });
        $this->m_twig_Environment->addFilter($filter);


        $filter = new Twig_SimpleFilter('show_hostel_make_view_url', function ( $hostelName, $Hostel ) {
            return AppUtils::twShowHostelMakeViewUrl($Hostel);
        });
        $this->m_twig_Environment->addFilter($filter);


        $filter = new Twig_SimpleFilter('show_hostel_feature_label', function ($feature) {
            return ReplaceSpaces(get_instance()->mhostel->getHostelFeatureLabel($feature));
        });
        $this->m_twig_Environment->addFilter($filter);


        $filter = new Twig_SimpleFilter('show_hostel_status_label', function ($status) {
            return ReplaceSpaces(get_instance()->mhostel->getHostelStatusLabel($status));
        });
        $this->m_twig_Environment->addFilter($filter);


        $filter = new Twig_SimpleFilter('show_hostel_extra_details_yes_no_available_label', function ($value) {
            return ReplaceSpaces(get_instance()->mhostel_extra_details->getYesNoAvailableLabel($value));
        });
        $this->m_twig_Environment->addFilter($filter);


        $filter = new Twig_SimpleFilter('show_hostel_extra_details_parking_label', function ($value) {
            return ReplaceSpaces(get_instance()->mhostel_extra_details->getParkingLabel($value));
        });
        $this->m_twig_Environment->addFilter($filter);


        $filter = new Twig_SimpleFilter('show_hostel_extra_details_yes_no_label', function ($value) {
            return ReplaceSpaces(get_instance()->mhostel_extra_details->getYesNoLabel($value));
        });
        $this->m_twig_Environment->addFilter($filter);


        $filter = new Twig_SimpleFilter('show_hostel_extra_details_no_pay_label', function ($value) {
            return ReplaceSpaces(get_instance()->mhostel_extra_details->getNoPayLabel($value));
        });
        $this->m_twig_Environment->addFilter($filter);


        $filter = new Twig_SimpleFilter('show_hostel_extra_details_yes_no_cash_only_label', function ($value) {
            return ReplaceSpaces(get_instance()->mhostel_extra_details->getYesNoCashOnlyLabel($value));
        });
        $this->m_twig_Environment->addFilter($filter);

        /* Hostels Block End */




        /* Tours Block Start */
        $filter = new Twig_SimpleFilter('tour_flag_status_label', function ( $flag_status ) {
            return ReplaceSpaces(get_instance()->mtour_review->getTour_Review_Flag_StatusLabel( $flag_status ) );
        });
        $this->m_twig_Environment->addFilter($filter);


        $filter = new Twig_SimpleFilter('show_tour_inquery_request_callback_label', function ( $request_callback ) {
            return ReplaceSpaces(get_instance()->mtour_inquery->getTour_InqueryRequest_CallbackLabel($request_callback) );
        });
        $this->m_twig_Environment->addFilter($filter);


        $filter = new Twig_SimpleFilter('show_tour_inquery_status_label', function ( $status ) {
            return ReplaceSpaces(get_instance()->mtour_inquery->getTour_InqueryStatusLabel($status) );
        });
        $this->m_twig_Environment->addFilter($filter);


        $filter = new Twig_SimpleFilter('show_tour_review_stars_rating_type_label_label', function ( $stars_rating ) {
            return ReplaceSpaces(get_instance()->mtour_review->getTour_Review_Stars_Rating_TypeLabel($stars_rating) );
        });
        $this->m_twig_Environment->addFilter($filter);


        $filter = new Twig_SimpleFilter('show_tour_make_view_url', function ( $tourName, $Tour ) {
            return AppUtils::twShowTourMakeViewUrl($Tour);
        });
        $this->m_twig_Environment->addFilter($filter);


        $filter = new Twig_SimpleFilter('show_tour_feature_label', function ($feature) {
            return ReplaceSpaces(get_instance()->mtour->getTourFeatureLabel($feature));
        });
        $this->m_twig_Environment->addFilter($filter);


        $filter = new Twig_SimpleFilter('show_tour_status_label', function ($status) {
            return ReplaceSpaces(get_instance()->mtour->getTourStatusLabel($status));
        });
        $this->m_twig_Environment->addFilter($filter);

        $filter = new Twig_SimpleFilter('show_tour_make_view_url', function ($tourName, $Tour) {
            return 'tour/' .
            (!empty($Tour['state_slug']) ? $Tour['state_slug'] : "-") . '/' .
            (!empty($Tour['region_slug']) ? $Tour['region_slug'] : '-') . '/' .
            (!empty($Tour['subregion_slug']) ? $Tour['subregion_slug'] : '-') . '/' .
            (!empty($Tour['slug']) ? $Tour['slug'] : '-');
        });
        $this->m_twig_Environment->addFilter($filter);


        /* Tours Block End */





        $filter = new Twig_SimpleFilter('concat_str', function ( $str, $max_len, $add_str = '...', $show_help = false, $strip_tags = true ) {
            return AppUtils::ConcatStr( $str, $max_len, $add_str, $show_help, $strip_tags );
        });
        $this->m_twig_Environment->addFilter($filter);



        $function = new Twig_SimpleFunction('show_items_list', function ($items_list, $separator= ', ', $define_space_separators= array(), $show_first=false, $show_last=false) {
            $ResStr = '';
            if (!empty($items_list[0]['label'])) {
                $has_separator_after_item = (
                    (!empty($items_list[1]['label']) and strlen($items_list[1]['label']) > 0) or
                    (!empty($items_list[2]['label']) and strlen($items_list[2]['label']) > 0) or
                    (!empty($items_list[3]['label']) and strlen($items_list[3]['label']) > 0) or
                    (!empty($items_list[4]['label']) and strlen($items_list[4]['label']) > 0)
                );
                $ResStr .= $items_list[0]['label'] . ($has_separator_after_item ? (in_array(1, $define_space_separators) ? '' : $separator) : "");
            }
            $has_separator_after_item = false;
            if (!empty($items_list[1]['label'])) {
                $has_separator_after_item = (
                    (!empty($items_list[2]['label']) and strlen($items_list[2]['label']) > 0) or
                    (!empty($items_list[3]['label']) and strlen($items_list[3]['label']) > 0) or
                    (!empty($items_list[4]['label']) and strlen($items_list[1]['label']) > 0)
                );
                $ResStr .= $items_list[1]['label'] . ($has_separator_after_item ? $separator : "");
            }
            $has_separator_after_item = false;
            if (!empty($items_list[2]['label'])) {
                $has_separator_after_item = (
                    (!empty($items_list[3]['label']) and strlen($items_list[3]['label']) > 0) or
                    (!empty($items_list[4]['label']) and strlen($items_list[4]['label']) > 0)
                );
                $ResStr .= $items_list[2]['label'] . ($has_separator_after_item ? $separator : "");
            }
            $has_separator_after_item = false;
            if (!empty($items_list[3]['label'])) {
                $has_separator_after_item = (
                (!empty($items_list[4]['label']) and strlen($items_list[4]['label']) > 0)
                );
                $ResStr .= $items_list[3]['label'] . ($has_separator_after_item ? $separator : "");
            }
            $has_separator_after_item = false;
            if (!empty($items_list[4]['label'])) {
                $has_separator_after_item = false;
                $ResStr .= $items_list[4]['label'] . ($has_separator_after_item ? $separator : "");
            }
//            echo '<pre>::'.print_r($ResStr,true).'</pre>';
            return $ResStr;
        });

        $this->m_twig_Environment->addFunction($function);

        $function = new Twig_SimpleFunction('get_user_group_id', function ($label) {
            $ci = & get_instance();
            $group_id= $ci->muser->getGroupIdByName( $label );
            return $group_id;
        });
        $this->m_twig_Environment->addFunction($function);



        if ( ENVIRONMENT != 'development' ) {
            $function = new Twig_SimpleFunction('dump', function ($file_name) {
            });
            $this->m_twig_Environment->addFunction($function);
        }

        $function = new Twig_SimpleFunction('file_exists', function ($file_name) {
            return file_exists($file_name) and !is_dir($file_name);
        });
        $this->m_twig_Environment->addFunction($function);



        $function = new Twig_SimpleFunction('set_field_error_tag', function ($fieldname, $attr) {
            if (empty($attr)) $attr = ' class="has-error" ';
            $fieldvalue = strip_tags( form_error($fieldname) );
            if (!empty($fieldvalue)) {
                return $attr;
            }
        });
        $this->m_twig_Environment->addFunction($function);


        $function = new Twig_SimpleFunction('get_params', function ($request, $first_char='') {
            return AppUtils::get_params($request, $first_char);
        });
        $this->m_twig_Environment->addFunction($function);


        $function = new Twig_SimpleFunction('showListHeaderItem', function ($url, $filters_str, $field_title, $fieldname, $sort_direction, $sort ) {
            if (empty($field_title)) {
                $field_title = ucwords($fieldname);
            }
            $field_title = str_replace('', '&nbsp;', $field_title);
            $R = tplSortDirection( $fieldname, $sort_direction, $sort );
            $isFirstLetter = true;
            $filters_str .= ($isFirstLetter ? '/' : '/') . 'sort_direction/' . $R; //  $sort_direction;
            $filters_str .= '/sort/' . urlencode($fieldname);
            $filters_str .= '/fieldname/' . urlencode($fieldname);
            $imgHtlm = tplListSortingImage($fieldname, $sort, $sort_direction);
            $res_url = '<a href="' . $url . $filters_str . '"><span>' . $field_title . $imgHtlm . '</span></a>';
            return $res_url; //CHtml::link($field_title . "&nbsp;" . $imgHtlm, $url);
        });
        $this->m_twig_Environment->addFunction($function);


        $function = new Twig_SimpleFunction('prepare_chosen_select', function ($data_list, $id, $current_value='', $width='', $multiple='', $additive_class= '') {
            $Res = '<select data-placeholder="Select Region" style="width:' . (!empty($width) ? (string)$width : '350') . ' '  . ' " class="form-control chosen-select ' . $additive_class . '" ' . ($multiple ? 'multiple' : '') . ' tabindex="6" id="' . $id . '">
	<option value=""></option> ';
            $current_value= !empty($current_value) ? $current_value : '';
            foreach ($data_list as $data_item) {
                $Res .= '<optgroup label="' . $data_item['state_name'] . '">';
                foreach ($data_item['regions_sub_array'] as $region_id => $region_name) {
                    $Res .= '<option value="' . $region_id . '" '.( (int)$current_value== (int)$region_id ? 'selected' : '').' >' . $region_name . '</option>';
                }
            }
            $Res .= '</select>';
            return $Res;
        } , array('is_safe' => array('html') ) );
        $this->m_twig_Environment->addFunction($function);


        $filter = new Twig_SimpleFilter('urldecode', function ($string) {
            return urldecode($string);
        });
        $this->m_twig_Environment->addFilter($filter);


        $filter = new Twig_SimpleFilter('urldecode', function ($string) {
            return urldecode($string);
        });
        $this->m_twig_Environment->addFilter($filter);


        $filter = new Twig_SimpleFilter('get_user_active_label', function ( $active ) {
            return ReplaceSpaces(get_instance()->muser->getUserActiveTitle($active));
        });
        $this->m_twig_Environment->addFilter($filter);

        $filter = new Twig_SimpleFilter('stripslashes', function ( $str ) {
            return stripslashes($str);
        });
        $this->m_twig_Environment->addFilter($filter);


        $filter = new Twig_SimpleFilter('addslashes', function ( $str ) {
            return addslashes($str);
        });
        $this->m_twig_Environment->addFilter($filter);


        $filter = new Twig_SimpleFilter('nl2br', function ($str) {
            return nl2br($str);
        });
        $this->m_twig_Environment->addFilter($filter);


        $filter = new Twig_SimpleFilter('get_user_group_label', function ($user_id, $get_by_user_id= true) {
            $showOnlyFirst= ''; $separator= ', ';
            if ($get_by_user_id) {
                return get_instance()->muser->getUserGroupTitle($user_id, $showOnlyFirst, $separator);
            }
            else {
                return get_instance()->muser->getGroupNameById($user_id);
            }
        });
        $this->m_twig_Environment->addFilter($filter);


        $filter = new Twig_SimpleFilter('get_vote_active_label', function ( $active ) {
            return ReplaceSpaces(get_instance()->mvote->getVoteActiveTitle($active));
        });
        $this->m_twig_Environment->addFilter($filter);


        $filter = new Twig_SimpleFilter('date_format', function ($value, $format_time) {
            return ReplaceSpaces( AppUtils::ShowFormattedDateTime($value, $format_time) );
        });
        $this->m_twig_Environment->addFilter($filter);


        $filter = new Twig_SimpleFilter('show_region_label', function ($region_id) {
            $mdlRegion= get_instance()->mregion->getRowById($region_id);
            if ( !empty( $mdlRegion['name'] ) ) {
                return ReplaceSpaces( $mdlRegion['name'] );
            }
        });
        $this->m_twig_Environment->addFilter($filter);


        $filter = new Twig_SimpleFilter('show_time_label', function ($time) {
            $time = $time;
            if ($time <= 12) {
                return $time . 'AM';
            } else {
                return ($time - 12) . 'PM';
            }
        });
        $this->m_twig_Environment->addFilter($filter);


        $filter = new Twig_SimpleFilter('htmlspecialchars_decode', function ($str) {
            return $str;
        });
        $this->m_twig_Environment->addFilter($filter);

        $filter = new Twig_SimpleFilter('show_money_formatted', function ($money) {
            return money_format('%.2n', $money);
        });
        $this->m_twig_Environment->addFilter($filter);


        //$this->registerPlugin("function", "date_now", "print_current_date");
    }

} // class AppTwig

function ReplaceSpaces($S)
{
    $Pattern = '/([\s])/xsi';
    $S = preg_replace($Pattern, '&nbsp;', $S);
    return $S;
}

function tplSortDirection($fieldname, $sort_direction, $sort)
{
    return (($sort == $fieldname and $sort_direction == 'asc') ? "desc" : "asc");
}

function tplListSortingImage($fieldname, $sort, $sort_direction)
{
    $ci = & get_instance();
    $config_object = $ci->config;
    $config_array = $config_object->config;
    if ($sort == $fieldname and strtolower($sort_direction) == 'asc') {
        return '<i class="glyphicon glyphicon-arrow-down" style="display: inline-block;" ></i>';
    }
    if ($sort == $fieldname and strtolower($sort_direction) == 'desc') {
        return '<i class="glyphicon glyphicon-arrow-up" style="display: inline-block;" ></i>';
    }
}
