<?php

class Main extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
        $this->load->model('muser', '', true);
	}

	public function index()
	{
        $data= array();
        $this->load->model('mhostel', '', true);
        $this->load->model('mhostel_review', '', true);
        $this->load->model('mhostel_room', '', true);
        $this->load->model('mregion', '', true);
        $this->load->model('mstate', '', true);
        $this->load->model('mcms_item', '', true);
        $this->load->model('mtour', '', true);
        $this->load->model('mtour_review', '', true);
        $this->load->model('mcategory', '', true);
        $this->load->model('mbanner', '', true);
        $this->load->model('mvote', '', true);
        $this->load->model('mvote_item', '', true);
        $this->setPageTitle('Home');
        $this->setBreadcrumbs(array( array('title'=>'Home', 'url'=>$this->app_config['base_url'] . "/home" ) ) );
        $A = $this->mhostel->getHostelsList(false, '', '', 'F', '', '', '', '', 'A', 'hostel.created_at', 'desc', '', '', true);
        $MaxElements= 4;
        if ( $MaxElements> count($A) ) $MaxElements= count($A);
        $hostels_indexes= array_rand($A, $MaxElements);
        $HostelsList= array();
        $orig_width_xs = 300;   // 1 image in column
        $orig_height_xs = 225;
        $orig_width_sm = 240;   // 2 images in column
        $orig_height_sm = 180;
        $orig_width_md = 310;   // 2 images in column
        $orig_height_md = 232;
        foreach($hostels_indexes as $hostel_index) {
            $filename_path = $A[$hostel_index]['default_image_path'];
            $FilenameInfo = AppUtils::GetImageShowSize($filename_path, $orig_width_xs, $orig_height_xs);
            $A[$hostel_index]['width_xs']= $FilenameInfo['Width'];
            $A[$hostel_index]['height_xs']= $FilenameInfo['Height'];
            $A[$hostel_index]['orig_width_xs']= $orig_width_xs;
            $A[$hostel_index]['orig_height_xs']= $orig_height_xs;

            $FilenameInfo_sm = AppUtils::GetImageShowSize($filename_path, $orig_width_sm, $orig_height_sm);
            $A[$hostel_index]['width_sm']= $FilenameInfo_sm['Width'];
            $A[$hostel_index]['height_sm']= $FilenameInfo_sm['Height'];
            $A[$hostel_index]['orig_width_sm']= $orig_width_sm;
            $A[$hostel_index]['orig_height_sm']= $orig_height_sm;

            $FilenameInfo_md = AppUtils::GetImageShowSize($filename_path, $orig_width_md, $orig_height_md);
            $A[$hostel_index]['width_md']= $FilenameInfo_md['Width'];
            $A[$hostel_index]['height_md']= $FilenameInfo_md['Height'];
            $A[$hostel_index]['orig_width_md']= $orig_width_md;
            $A[$hostel_index]['orig_height_md']= $orig_height_md;
            $Hostel_Rooms= $this->mhostel_room->getHostel_RoomsList( false, '', '', $A[$hostel_index]['id'], '', '', '', '', '', '', false );
            $MinPrice= 0;
            foreach($Hostel_Rooms as $Hostel_Room) {
                if ( $Hostel_Room['price']> 0 and ( $Hostel_Room['price']< $MinPrice or $MinPrice == 0) ) {
                    $MinPrice= $Hostel_Room['price'];
                }
            }
            $A[$hostel_index]['price']= $MinPrice;
            $A[$hostel_index]['reviews_avg_rating']= (int)round($this->mhostel_review->getHostel_ReviewsAvgRating('',$A[$hostel_index]['id'], 'A' ), 0);

            $HostelsList[]= $A[$hostel_index];
        }
        $A = $this->mtour->getToursList( false, '', array( 'filter_feature'=>'F', 'filter_status'=>'A', 'show_default_image'=> 1 ), 'tour.created_at', 'desc' );
        if ( $MaxElements> count($A) ) $MaxElements= count($A);
        $tours_indexes= array_rand($A, $MaxElements);
        $ToursList= array();
        foreach($tours_indexes as $tour_index) {
            $filename_path = $A[$tour_index]['default_image_path'];
            $FilenameInfo = AppUtils::GetImageShowSize($filename_path, $orig_width_xs, $orig_height_xs);
            $A[$tour_index]['width_xs']= $FilenameInfo['Width'];
            $A[$tour_index]['height_xs']= $FilenameInfo['Height'];
            $A[$tour_index]['orig_width_xs']= $orig_width_xs;
            $A[$tour_index]['orig_height_xs']= $orig_height_xs;

            $FilenameInfo = AppUtils::GetImageShowSize($filename_path, $orig_width_sm, $orig_height_sm);
            $A[$tour_index]['width_sm']= $FilenameInfo['Width'];
            $A[$tour_index]['height_sm']= $FilenameInfo['Height'];
            $A[$tour_index]['orig_width_sm']= $orig_width_sm;
            $A[$tour_index]['orig_height_sm']= $orig_height_sm;

            $FilenameInfo = AppUtils::GetImageShowSize($filename_path, $orig_width_md, $orig_height_md);
            $A[$tour_index]['width_md']= $FilenameInfo['Width'];
            $A[$tour_index]['height_md']= $FilenameInfo['Height'];
            $A[$tour_index]['orig_width_md']= $orig_width_md;
            $A[$tour_index]['orig_height_md']= $orig_height_md;
            $A[$tour_index]['reviews_avg_rating']= (int)round($this->mtour_review->getTour_ReviewsAvgRating('',$A[$tour_index]['id'], 'A' ), 0);
            $ToursList[]= $A[$tour_index];
        }

        $RegionValueArray = $this->mregion->getRegionsSelectionList();
        $StateValueArray = $this->mstate->getStatesSelectionList();
        $create_a_listing_text = $this->mcms_item->getBodyContentByAlias('create_a_listing');
        $BlogArticlesList = $this->mcms_item->getCms_ItemsList(false, '', 'B', 'Y', '', '', '', 'cms_item.created_at', 'desc', '', false, true, 4);

        foreach ( $BlogArticlesList as $key=>$BlogArticle ) {
            $filename_path = $BlogArticle['default_image_path'];
            $FilenameInfo = AppUtils::GetImageShowSize($filename_path, $orig_width_xs, $orig_height_xs);
            $BlogArticlesList[$key]['width_xs']= $FilenameInfo['Width'];
            $BlogArticlesList[$key]['height_xs']= $FilenameInfo['Height'];
            $BlogArticlesList[$key]['orig_width_xs']= $orig_width_xs;
            $BlogArticlesList[$key]['orig_height_xs']= $orig_height_xs;

            $FilenameInfo = AppUtils::GetImageShowSize($filename_path, $orig_width_sm, $orig_height_sm);
            $BlogArticlesList[$key]['width_sm']= $FilenameInfo['Width'];
            $BlogArticlesList[$key]['height_sm']= $FilenameInfo['Height'];
            $BlogArticlesList[$key]['orig_width_sm']= $orig_width_sm;
            $BlogArticlesList[$key]['orig_height_sm']= $orig_height_sm;

            $FilenameInfo = AppUtils::GetImageShowSize($filename_path, $orig_width_md, $orig_height_md);
            $BlogArticlesList[$key]['width_md']= $FilenameInfo['Width'];
            $BlogArticlesList[$key]['height_md']= $FilenameInfo['Height'];
            $BlogArticlesList[$key]['orig_width_md']= $orig_width_md;
            $BlogArticlesList[$key]['orig_height_md']= $orig_height_md;
        }
        $CategorysList = $this->mcategory->getCategorysList(false);
        $banners_list=  $this->mbanner->getBannersList(false, '', array(), 'ordering',  '');
        $votes_list=  $this->mvote->getVotesList(false, '', array('active'=>'Y', 'show_vote_item_result_count'=> 1), 'ordering',  '');
        foreach( $votes_list as $next_key=>$next_vote ) {
            $vote_ItemsList= $this->mvote_item->getVote_ItemsList( false, '', array( 'vote_id'=>$next_vote['id'], 'show_vote_item_result_count'=> 1 ), '', '', '' );
            $votes_list[$next_key]['sub_items']= $vote_ItemsList;
        }

        $data = array('HostelsList' => $HostelsList, 'ToursList'=>$ToursList,  'RegionValueArray' => $RegionValueArray,
            'StateValueArray' => $StateValueArray, 'BlogArticlesList' => $BlogArticlesList, 'banners_list'=> $banners_list, 'votes_list'=> $votes_list,
            'CategorysList' => $CategorysList, 'create_a_listing_text' => $create_a_listing_text, 'votes_list'=> $votes_list,
        );
        $this->showTwigTemplate('main', $data, false, true, true);
	}

	public function invalid_url()
	{
		AppUtils::DebToFile( '-01 invalid_url::' ,false);
		$UriArray = $this->uri->uri_to_assoc(1);
		 $data = array('UriArray' => $UriArray);
	}

	public function cms_page()
	{
		$this->load->model('mcms_item', '', true);
        $this->load->model('mbanner', '', true);
        $this->load->model('mvote', '', true);
        $this->load->model('mvote_item', '', true);
		$UriArray = $this->uri->uri_to_assoc(1);

		$A= array_keys($UriArray);
		$PageKey= '';
		if ( !empty($A[0]) ) {
			$PageKey= $A[0];
		}
        $video_id = urldecode(AppUtils::getParameter($this, $UriArray, array(), 'video_id'));
		$cms_item = $this->mcms_item->getRowByFieldName('alias', $PageKey, true);
		$cms_item_text = $this->mcms_item->getBodyContentByAlias($PageKey, array(), true);
		$cms_item_header = $this->mcms_item->getBodyContentByAlias($PageKey, array(), false);

        $video_struct= array();
        $videos_list= array();
        if ( $PageKey == "about_us" ) {
            $videos_list= array(
                array('id'=> 1, 'video_name'=>'video.mp4', 'width'=> 200, 'height'=> 110, 'video_type'=> 'odd', 'video_ext'=> 'mp4'), //!
                array('id'=> 2, 'video_name'=>'installation.mp4', 'width'=> 640, 'height'=> 480, 'video_type'=> 'odd', 'video_ext'=> 'mp4' ), // !
                array('id'=> 3, 'video_name'=>'7Corazonvaliente.mp4', 'width'=> 640, 'height'=> 360, 'video_type'=> 'odd', 'video_ext'=> 'mp4' ),// !
                array('id'=> 4, 'video_name'=>'Вокальный урок 7 -Роза.avi', 'width'=> 640, 'height'=> 360, 'video_type'=> 'odd', 'video_ext'=> 'avi' ),// !


                array('id'=> 9, 'video_name'=>'вискас.mpg', 'width'=> 352, 'height'=> 288, 'video_type'=> 'mpg', 'video_ext'=> 'mpg' ), // ???
                array('id'=> 10, 'video_name'=>'This Is My Food.wmv', 'width'=> 720, 'height'=> 576, 'video_type'=> 'mpg', 'video_ext'=> 'wmv' ),  // ???


//                array('id'=> 5, 'video_name'=>'Маша и Медведь_18 серия.mkv', 'width'=> 1024, 'height'=> 576, 'video_type'=> 'avc1', 'video_ext'=> 'mp4' ),
            );
            if (empty($video_id)) $video_id= 4;
            foreach( $videos_list as $next_key=>$next_video ) {
                if ( $next_video['id'] == $video_id ) {
                    $video_struct= $next_video;
                    break;
                }
            }
        }
        $document_root = $this->app_config['document_root'];

        foreach ($videos_list as $next_key => $next_video) {
            $video_path = $document_root . 'uploads/video/' . $next_video['video_name'];
            $video_size_label= AppUtils::getFileSizeAsString( filesize($video_path) );
            $videos_list[$next_key]['size_label']= $video_size_label;
        }      // {{ base_url }}uploads/video/{{ video_struct.video_name }}

        $this->setPageTitle( $cms_item_header);
        $this->setBreadcrumbs(array( array('title'=>'Home', 'url'=>$this->app_config['base_url'] . "/home" ),
            array('title'=>$cms_item_header, 'url'=> $this->app_config['base_url'] . '/' . $PageKey ),
        ) );
        $cms_item_text_list= array();
        if ( $PageKey == 'terms_condition' ) {
            $cms_item_text_list= split_register_info($cms_item_text);
            $cms_item_text= '';
        }
        $banners_list=  $this->mbanner->getBannersList(false, '', array(), 'ordering',  '');
        $votes_list=  $this->mvote->getVotesList(false, '', array('active'=>'Y', 'show_vote_item_result_count'=> 1), 'ordering',  '');
        foreach( $votes_list as $next_key=>$next_vote ) {
            $vote_ItemsList= $this->mvote_item->getVote_ItemsList( false, '', array( 'vote_id'=>$next_vote['id'], 'show_vote_item_result_count'=> 1 ), '', '', '' );
            $votes_list[$next_key]['sub_items']= $vote_ItemsList;
        }

        $data = array( 'cms_item' => $cms_item, 'cms_item_header' => $cms_item_header, 'banners_list'=> $banners_list, 'cms_item_text' => $cms_item_text, 'cms_item_text_list'=> $cms_item_text_list, 'cms_page' => $PageKey, 'video_struct' => $video_struct, 'videos_list'=> $videos_list, 'votes_list'=> $votes_list );
		$this->showTwigTemplate('cms_item_text', $data, false, true, true);
	}

	public function get_cms_item_by_alias()
	{
		$this->load->model('mcms_item', '', true);
		$UriArray = $this->uri->uri_to_assoc(3);
		$cms_item = $this->mcms_item->getRowByFieldName('alias', $UriArray['alias'], true);
        $this->output->set_content_type('application/json')->set_output(json_encode(array('ErrorMessage' => "", 'ErrorCode' => 0, 'cms_item'=> $cms_item, 'content'=> $cms_item['content'] )));
	}


	public function insert_subscribe_to_newsletter()
	{
		$UriArray = $this->uri->uri_to_assoc(3);
		$post_array = $this->input->post();
		$user_email = urldecode(AppUtils::getParameter($this, $UriArray, $post_array, 'user_email'));
		$user_name = urldecode(AppUtils::getParameter($this, $UriArray, $post_array, 'user_name'));
        $this->output->set_content_type( 'application/json')->set_output(json_encode(array(  'ErrorMessage' => 'Test for "'.$user_name.'" for "'.($user_email).'" email!', 'ErrorCode' => 0 ) )  );
        return;
		//$api = new MCAPI($apikey);

		$merge_vars = array('FNAME'=>$user_name, 'LNAME'=>$user_name,
			/*'GROUPINGS'=>array(
				array('name'=>'Your Interests:', 'groups'=>'Bananas,Apples'),
				array('id'=>22, 'groups'=>'Trains'),
			) */
		);

		if ($api->errorCode){
			$this->output->set_content_type('application/json')->set_output(json_encode(array('ErrorMessage' => "Error adding subsriber to mailchimp.com, Code=".$api->errorCode . ", Msg=".$api->errorMessage, 'ErrorCode' => 1 )));
		} else {
			$this->output->set_content_type('application/json')->set_output(json_encode(array(  'ErrorMessage' => 'Subscribed - look for the confirmation email!', 'ErrorCode' => 0 )));
		}
	}

	public function send_contact_us()
	{
		$UriArray = $this->uri->uri_to_assoc(3);

		$post_array = $this->input->post();
		$name = urldecode(AppUtils::getParameter($this, $UriArray, $post_array, 'name'));
		$email = urldecode(AppUtils::getParameter($this, $UriArray, $post_array, 'email'));
		$message = urldecode(AppUtils::getParameter($this, $UriArray, $post_array, 'message'));

		$capture = AppUtils::getParameter($this, $UriArray, $post_array, 'capture');
		$blog_capture_word = trim($this->session->userdata('blog_capture_word'));
		$blog_capture_expired_time = $this->session->userdata('blog_capture_expired_time');
//		if ($blog_capture_word != $capture or $blog_capture_expired_time < time()) {
//			$this->output->set_content_type('application/json')->set_output(json_encode(array('ErrorMessage' => 'Invalid Capture', 'ErrorCode' => 1, 'ErrorFieldName' => 'contact_us_capture')));
//			return;
//		}

		$Content = $name . '(' . $email . ')' . ' wrote : ' . $message;
		$EmailOutput = AppUtils::SendEmail($this->app_config['contact_us_email'], 'Contact Us Email', $Content, false, '');
		if (!empty($EmailOutput)) {
			$this->output->set_content_type('application/json')->set_output(json_encode(array('ErrorMessage' => '', 'ErrorCode' => 0, 'EmailOutput' => $EmailOutput)));
			return;
		}
		$this->output->set_content_type('application/json')->set_output(json_encode(array('ErrorMessage' => 'Invalid Parameters', 'ErrorCode' => 1, 'id' => 0)));
	}

	public function show_capture_image()
	{
		$this->load->helper('captcha');
		$UriArray = $this->uri->uri_to_assoc(3);
		$capture_prefix = AppUtils::getParameter($this, $UriArray, array(), 'capture_prefix', '');
		$random_string = AppUtils::PrepareRandomWord(); // 'Random';
		$vals = array(
			'word' => $random_string,
			'img_path' => './images/captcha/',
			'img_url' => $this->app_config['base_root_url'] . '/images/captcha/', //'http://example.com/captcha/',
			'font_path' => $this->app_config['ttf_file_path'],
			'img_width' => '120',
			'img_height' => 30,
			'expiration' => 7200
		);
		$capture_image = create_captcha($vals);
		$this->session->set_userdata(array( $capture_prefix . 'word' => $random_string, $capture_prefix . 'expired_time' => time() + $vals['expiration']));
		echo $capture_image['image'];
	}

	public function blog_view()
	{
		$this->load->model('mcms_item', '', true);
		$this->load->model('mregion', '', true);
		$this->load->model('mstate', '', true);
		$this->load->model('mbanner', '', true);
		$this->load->model('mvote', '', true);
		$this->load->model('mvote_item', '', true);

		$UriArray = $this->uri->uri_to_assoc(2);
		$cms_item = $this->mcms_item->getRowById($UriArray['id'], true);

		$post_array = array();
		$page = AppUtils::getParameter($this, $UriArray, $post_array, 'page', 1);
		$filter_page_type = 'B';
		$filter_published = 'Y';
		$filter_author_id = '';
		$filter_date_1 = '';
		$filter_date_2 = '';
		$sorting = AppUtils::getParameter($this, $UriArray, $post_array, 'sorting', 'cms_item.created_at');
		$sort = AppUtils::getParameter($this, $UriArray, $post_array, 'sort', 'desc');
		$RowsInTable = $this->mcms_item->getCms_ItemsList(true, '', $filter_page_type, $filter_published, $filter_author_id, AppUtils::ConvirtDateToMySqlFormat($filter_date_1),	AppUtils::ConvirtDateToMySqlFormat($filter_date_2), $sorting, $sort);

		$this->load->library('pagination');
		$Cms_ItemsList = array();
		if ( $RowsInTable> 0 ) {
  		    $Cms_ItemsList = $this->mcms_item->getCms_ItemsList(false, $page, $filter_page_type, $filter_published, $filter_author_id, AppUtils::ConvirtDateToMySqlFormat($filter_date_1),
			AppUtils::ConvirtDateToMySqlFormat($filter_date_2), $sorting, $sort, '', true, true);
		}
        $pagination_config= $this->getTemplateParams(array(),'pagination');
        $pagination_config['base_url'] = base_url() . 'main/blog_view/' . $cms_item['alias'] . '/id/' . $cms_item['id'] . '/page';
        $pagination_config['total_rows'] = $RowsInTable;
        $pagination_config['uri_segment'] = 7;
        $pagination_config['page'] = $page;
        $this->pagination->initialize($pagination_config);

		$this->pagination->suffix = $this->PreparePageParameters($UriArray, $post_array, false, true);
		$pagination_links = $this->pagination->create_links();
        $orig_width = 320;
		$orig_height = 240;
  	    $filename_path = $cms_item['default_image_path'];
		$FilenameInfo = AppUtils::GetImageShowSize($filename_path, $orig_width, $orig_height);
		$cms_item['width']= $FilenameInfo['Width'];
		$cms_item['height']= $FilenameInfo['Height'];
        $this->setPageTitle( "Blog ".$cms_item['title'] );
        $this->setBreadcrumbs(array( array('title'=>'Home', 'url'=>$this->app_config['base_url'] . "/home" ),
            array('title'=>"Blog ".$cms_item['title'], 'url'=> $this->app_config['base_url'] . '/main/blog_view/' . $cms_item['alias'].'/id/'.$cms_item['id'] ),
        ) );

        $RegionValueArray = $this->mregion->getRegionsSelectionList();
        $StateValueArray = $this->mstate->getStatesSelectionList();
        $banners_list=  $this->mbanner->getBannersList(false, '', array(), 'ordering',  '');
        $votes_list=  $this->mvote->getVotesList(false, '', array('active'=>'Y', 'show_vote_item_result_count'=> 1), 'ordering',  '');
        foreach( $votes_list as $next_key=>$next_vote ) {
            $vote_ItemsList= $this->mvote_item->getVote_ItemsList( false, '', array( 'vote_id'=>$next_vote['id'], 'show_vote_item_result_count'=> 1 ), '', '', '' );
            $votes_list[$next_key]['sub_items']= $vote_ItemsList;
        }

		$data = array('page' => $page, 'PageParametersWithSort' => '', 'cms_item' => $cms_item, 'RowsInTable' => $RowsInTable, 'Cms_ItemsList' => $Cms_ItemsList, 'pagination_links' => $pagination_links, 'RegionValueArray'=> $RegionValueArray, 'StateValueArray'=> $StateValueArray, 'banners_list'=> $banners_list, 'votes_list'=> $votes_list );
		$this->showTwigTemplate( 'blog_view', $data, false, true, true );

    }


	public function operator_register() {
		$this->load->model('muser', '', true);
		$UriArray = $this->uri->uri_to_assoc(3);
		$post_array = $this->input->post();

		$login = AppUtils::getParameter($this, $UriArray, array(), 'login', '');
		$email = AppUtils::getParameter($this, $UriArray, array(), 'email', '');
		$password = AppUtils::getParameter($this, $UriArray, array(), 'password', '');
		$first_name = AppUtils::getParameter($this, $UriArray, array(), 'first_name', '');
		$last_name = AppUtils::getParameter($this, $UriArray, array(), 'last_name', '');
		$phone = AppUtils::getParameter($this, $UriArray, array(), 'phone', '');
		$company = AppUtils::getParameter($this, $UriArray, array(), 'company', '');
		$capture = AppUtils::getParameter($this, $UriArray, $post_array, 'capture');

		$operator_register_word = trim($this->session->userdata('operator_register_word'));
		$operator_register_expired_time = $this->session->userdata('operator_register_expired_time');
		if ($operator_register_word != $capture or $operator_register_expired_time < time()) {
			$this->output->set_content_type('application/json')->set_output(json_encode(array('ErrorMessage' => 'Invalid Capture !', 'ErrorCode' => 1, 'ErrorFieldName' => 'operator_register_capture')));
			return;
		}

		$lUser= $this->muser->getRowByFieldName( 'login', $login );
		if ( !empty($lUser) ) {
			$this->output->set_content_type('application/json')->set_output(json_encode(array('ErrorMessage' => 'There is already registered user with this login ', 'ErrorCode' => 1,
				'login' => $login, 'ErrorFieldName' => 'operator_register_login' ) ) );
			return;
		}

		$lUser= $this->muser->getRowByFieldName( 'email', $email );
		if ( !empty($lUser) ) {
			$this->output->set_content_type('application/json')->set_output(json_encode(array('ErrorMessage' => 'There is already registered user with this email ', 'ErrorCode' => 1,
				'login' => $login, 'ErrorFieldName' => 'operator_register_email' ) ) );
			return;
		}

		$OkResult = $this->muser->UpdateUser( '', Array( 'user_group'=>/*TODO*/'O', 'status'=>'A', 'login'=>$login, 'email'=>$email, 'password'=>md5($password),
			'first_name' => $first_name, 'last_name' => $last_name,	'company' => $company, 'phone' => $phone));
		if ( !$OkResult ) {
		  $this->output->set_content_type('application/json')->set_output(json_encode(array('ErrorMessage' => 'Database error inserting new Operator ', 'ErrorCode' => 1,
			  'login' => $login ) ) );
		} else {
		  $this->output->set_content_type('application/json')->set_output(json_encode(array('ErrorMessage' => '', 'ErrorCode' => 0,
			  'login' => $login ) ) );
		}

	}

    public function user_register() {
        $this->load->model('mcms_item', '', true);
        $this->load->model('muser', '', true);
        $UriArray = $this->uri->uri_to_assoc(3);
        $register_content_text = nl2br( $this->mcms_item->getBodyContentByAlias('register_content') );
        $register_info_array= split_register_info($register_content_text);


        $this->setPageTitle( "User Register");
        $this->setBreadcrumbs(array( array('title'=>'Home', 'url'=>$this->app_config['base_url'] . "/home" ),
            array('title'=>"User Register", 'url'=> $this->app_config['base_url'] . '/register' ),
        ) );

        $layout_config= $this->getTemplateParams(array() , 'layout');
        $editor_message = $this->session->flashdata('editor_message');
        $form_status = 'edit';
        $User = null;
        if (!empty($_POST)) { // form was submitted
            $this->user_register_form_validation();

            $validation_status = $this->form_validation->run();
            if ($validation_status != FALSE) {
                $this->db->trans_start();
                $additional_data= Array(
                    'username' => $this->input->post('username'), 'email' => $this->input->post('email'),
                    'password' => $this->input->post('password'),
                    'first_name' => $this->input->post('first_name'), 'last_name' => $this->input->post('last_name'),
                    'company' => $this->input->post('company'), 'phone' => $this->input->post('phone'), 'ip' => $_SERVER['REMOTE_ADDR'], 'created_on'=>time(),
                    'active'=> 0, 'created_at'=> AppUtils::ShowFormattedDateTime(time(), "MYSQL"));
                $OkResult= $this->ion_auth_model->register($this->input->post('username'), $this->input->post('password'), $this->input->post('email'), $additional_data, array($this->muser->getGroupIdByname(OPERATOR_GROUP_LABEL)) ); //need to test email activation
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    redirect('main/login/type/success/msg/' . urlencode ( 'Error creating your operator login !') );
                } else {
                    $this->db->trans_commit();
                    redirect('main/login/type/success/msg/' . urlencode ( 'Your operator login was created and email sent at your email !').'/action/show-login' );
                }
            }
            else {
                $form_status = 'invalid';
                $User = $this->edit_fill_current_data($User);
            }
        }
        $data = array( 'User'=> $User, 'form_status'=> $form_status, 'register_info_array'=> $register_info_array,  'register_content_text' => $register_content_text, 'editor_message' => $editor_message, 'validation_errors_text' => validation_errors($layout_config['backend_error_icon_start'], $layout_config['backend_error_icon_end']) );
        $this->showTwigTemplate('user_register', $data, false, true, true);

    }
    private function user_register_form_validation()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('email', 'email', 'required');
        $this->form_validation->set_rules('username', 'Username', 'required|is_unique[users.username]|callback_verify_username_has_no_space');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]');

        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('password_2', 'Confirm Password', 'required|matches[password]');
        $this->form_validation->set_rules('first_name', 'First Name', 'required');
        $this->form_validation->set_rules('last_name', 'Last Name', 'required');
        $this->form_validation->set_rules('company', 'Company Name', 'required');
        $this->form_validation->set_rules('phone', 'phone', 'required');
    }
    public function verify_username_has_no_space($str)
    {
        if (!(strpos(trim($str), ' ') === false)) { // "Description" - max 80 words
            $this->form_validation->set_message('verify_username_has_no_space', "Space can not be in username field ! ");
            return FALSE;
        }
        return TRUE;
    }

    private function edit_fill_current_data($User)
    {
        $User['username'] = set_value('username');
        $User['email'] = set_value('email');
        $User['first_name'] = set_value('first_name');
        $User['last_name'] = set_value('last_name');
        $User['company'] = set_value('company');
        $User['phone'] = set_value('phone');
        $User['password'] = '';
        return $User;
    }


	public function forgot_password()
    {
        $this->load->model('muser', '', true);
        $this->setPageTitle( "Forgot Password");
        $this->setBreadcrumbs(array( array('title'=>'Home', 'url'=>$this->app_config['base_url'] . "/home" ),
            array('title'=>"Forgot Password", 'url'=> $this->app_config['base_url'] . '/main/forgot_password' ),
        ) );
        $uriArray = $this->uri->uri_to_assoc(3);
        if (!empty($_POST)) { // form was submitted
        }
        $this->showTwigTemplate('forgot_password', array(), false, true, true);
    }

	public function login() {   //  http://local-tb.com/main/login
		$this->load->model('muser', '', true);
        $uriArray = $this->uri->uri_to_assoc(3);
        $type = urldecode( AppUtils::getParameter($this, $uriArray, array(), 'type') );
        $this->setPageTitle( "Login" );
        $this->setBreadcrumbs(array( array('title'=>'Home', 'url'=>$this->app_config['base_url'] . "/home" ),
            array('title'=>"Login", 'url'=> $this->app_config['base_url'] . '/login' ),
        ) );

//        echo '<pre>$uriArray::'.print_r($uriArray,true).'</pre>';
        if (!empty($_POST)) { // form was submitted
            $result= $this->ion_auth->login($_POST['username'],$_POST['password'] , !empty($_POST['remember']) );
//            echo '<pre>$result::'.print_r($result,true).'</pre>';
//            die("-1 XXZ");
            if ( $result ) {
                $user = $this->ion_auth->user()->row();
//                echo '<pre>--11$user::'.print_r($user,true).'</pre>';
                $user_groups= $this->ion_auth->get_users_groups( $user->id )->result('array');
//                echo '<pre>$user_groups::'.print_r($user_groups,true).'</pre>';
                if ( !empty($user) and !empty($user_groups) ) {
                    $user_groups_name= '';
                    foreach( $user_groups as $next_user_group ) {
                        $user_groups_name.= $next_user_group['name'].', ';
                    }
//                    echo '<pre>!!!::'.print_r(AppUtils::getRightSubstring($user_groups_name, 2),true).'</pre>';
                    if ( AppUtils::getRightSubstring($user_groups_name, 2) == ', ' ){
                        //echo '<pre>INSIDE</pre>';
                        $user_groups_name= substr($user_groups_name,0,strlen($user_groups_name)-2 );
                    }

//                    if (AppUtils::isDeveloperComp()) {
                        $admin_watch_operator_id= 4;
                        $admin_watch_operator_login = 'Operator_2';
//                    }


                    $user_data= array('is_logged' => 1, 'user_group' => 'Admin', 'logged_name' => $user->username, 'logged_user_id' => $user->id,
                        'last_login' => $user->last_login, 'admin_watch_operator_id' => -1, 'admin_watch_operator_login' => '', 'current_backend_template_name' => $user->backend_template_id, 'current_frontend_template_id' => $user->frontend_template_id, 'user_groups'=> $user_groups, 'user_groups_name'=> $user_groups_name, 'type'=> $type, 'admin_watch_operator_id' => $admin_watch_operator_id, 'admin_watch_operator_login' => $admin_watch_operator_login );

                    $this->muser->setAuthValues(Array('admin_watch_operator_id' => $UriArray['admin_watch_operator_id'], 'admin_watch_operator_login' => $AdminWatchOperatorUser['username']));


                    // backend_template_id] => 4
//                    echo '<pre>$user_data::'.print_r($user_data,true).'</pre>';
//                    die("-1 OK!");
                    $this->session->set_userdata($user_data);
                    redirect('admin/dashboard/index');
                }

            } else {
                redirect('main/login/type/warning/msg/' . urlencode ( 'Invalid login or password !').'/action/show-login' );

            }
            //die("-1 ! form was submitted");
        }
        $this->showTwigTemplate('login', array('type'=> $type), false, true, true);
	}

	public function logout() {   // http://local-tb.com/main/
		$this->load->model('muser', '', true);
        echo '<pre>MAIN logout $_POST::'.print_r($_POST,true).'</pre>';
//            die("-1 ! form was submitted");
        $this->ion_auth->logout();
        $this->session->unset_userdata(  array('is_logged' => false, 'user_group' => '', 'logged_name' => 'guest', 'logged_user_id' => -1, 'last_login' => '',                'admin_watch_operator_id' => -1, 'admin_watch_operator_login' => '', 'current_backend_template_name' => '' ,  'current_frontend_template_name' => '')  );
        redirect('main/msg/type/info/msg/' . urlencode ( 'You logged from system !').'/action/show-login' );
	}

	public function get_subregions_list_by_region_id() {
		$this->load->model('msubregion', '', true);
		$UriArray = $this->uri->uri_to_assoc(3);
		$region_id = urldecode( AppUtils::getParameter($this, $UriArray, array(), 'region-id') );
		$current_subregion_id = urldecode( AppUtils::getParameter($this, $UriArray, array(), 'current-subregion-id') );
		// current_subregion_id
		$format = urldecode( AppUtils::getParameter($this, $UriArray, array(), 'format', 'html') );
		$SubregionsList= $this->msubregion->getSubregionsList( false, '', $region_id );
		$html= '<option value="">Select an Area</option>';
		foreach( $SubregionsList as $Subregion ) {
			if ( $format== 'html' )
				$html.= '<option value="'.$Subregion['id'].'"  '.($current_subregion_id==$Subregion['id']?" selected ":"").' >'.$Subregion['name'].'</option>';
		}
		$this->output->set_content_type('application/json')->set_output(json_encode(array('ErrorMessage' => '', 'ErrorCode' => 0, 'html'=> $html ) ) );
	}

	public function get_regions_list_by_state_id() {
		$this->load->model('mregion', '', true);
		$UriArray = $this->uri->uri_to_assoc(3);
		$state_id = urldecode( AppUtils::getParameter($this, $UriArray, array(), 'state-id') );
		$current_region_id = urldecode( AppUtils::getParameter($this, $UriArray, array(), 'current-region-id') );
        //if (empty($current_region_id)) $current_region_id= 24;
		$format = urldecode( AppUtils::getParameter($this, $UriArray, array(), 'format', 'html') );
		$RegionsList= $this->mregion->getRegionsList( false, '', $state_id );
		$html= '<option value="">Select a Region</option>';
		foreach( $RegionsList as $Region ) {
			if ( $format== 'html' )
				$html.= '<option value="'.$Region['id'].'"  '.($current_region_id==$Region['id']?" selected ":"").'  >'.$Region['name'].'</option>';
		}
		$this->output->set_content_type('application/json')->set_output(json_encode(array('ErrorMessage' => '', 'ErrorCode' => 0, 'html'=> $html ) ) );
	}

    public function msg() {
        $uriArray = $this->uri->uri_to_assoc(3);
        $data = $uriArray;
        $this->setPageTitle( "Message " . urldecode($data['msg']));
        $this->setBreadcrumbs(array( array('title'=>'Home', 'url'=>$this->app_config['base_url'] . "/home" ),
            array('title'=>urldecode($data['msg']), 'url'=> $this->app_config['base_url'] . '/msg/' ),
        ) );

        //echo '<pre>!!$data::'.print_r($data,true).'</pre>';
        $this->showTwigTemplate('msg', $data, false, true, true);
    }



    public function get_selected_item_info()
    { // var HRef = "{{ base_url }}main/get_selected_item_info/hostel_id/"+hostel_id+"/filter_facilitys/"+filter_facilitys
        $this->load->model('mhostel', '', true);
        $UriArray = $this->uri->uri_to_assoc(3);
        $hostel_id = AppUtils::getParameter($this, $UriArray, array(), 'hostel_id' );
        $hostel= $this->mhostel->getRowById($hostel_id);
        if (empty($hostel)) {
            $this->output->set_content_type('application/json')->set_output(json_encode(array('ErrorMessage' => "Hostel not found", 'ErrorCode' => 1)));
            return;
        }
        $images_list = $this->mhostel->get_images_list( $hostel_id, false, $hostel['feature'], false,  '', '', false);
        $images_count= count($images_list);
        $html= AppUtils::concatArray( array('<b>'.$hostel['name'].'</b>', $hostel['postal_code'], $hostel['town'], $hostel['street_addr'], $hostel['phone'], $hostel['email'], ( $images_count> 0 ? ' has ' . count($images_list).' image(s)' : '' ) ), ', ', true );
        $this->output->set_content_type('application/json')->set_output(json_encode(array('ErrorMessage' => "", 'ErrorCode' => 0, 'html'=> $html, 'images_count'=> $images_count, 'hostel'=> $hostel )));

    }


    public function code_highlight()
    {
        $this->load->model('mcms_item', '', true);
        $format= !empty($_REQUEST['format']) ? $_REQUEST['format'] : 'php';
        $page= !empty($_REQUEST['page']) ? $_REQUEST['page'] : '';
        $data_line= !empty($_REQUEST['data-line']) ? $_REQUEST['data-line'] : '';
        if ( empty($page) ) {
            $data = array( 'page' => $page, 'code_to_highlight'=> 'No filename specifies');
        } else {
            $file_path= FCPATH.$page;
              if ( !file_exists($file_path) ) {
                $data = array( 'page' => $page, 'code_to_highlight' => 'file ' . $file_path .' not found');
            } else {
                $file_content= AppUtils::readFileAsText($file_path);
//                $file_content= str_replace( '<'.'script',  '['.'code language'.'=JavaScript'.']'.'<'.'script',$file_content);
//                $file_content= str_replace( '<'.'/script',  '['.'/code'.']'.'<'.'/script',$file_content);
//
//                $file_content= str_replace( '<'.'input',  htmlspecialchars('<'.'input')  ,$file_content);
//                $file_content= str_replace( '<'.'button',  htmlspecialchars('<'.'button')  ,$file_content);
//                $file_content= str_replace( '<'.'textarea',  htmlspecialchars('<'.'textarea')  ,$file_content);
//               $data = array( 'file_path' => $file_path, 'code_to_highlight' => '['.'code language'.'='.$format.']'.$file_content.'['.'/code'.']', 'format'=> $format);
                //$file_content= addslashes(str_replace( array("\r","\n"), " ", $file_content ));
                //$file_content= (str_replace( array("\r","\n"), " ", $file_content ));
                //$file_content= (str_replace( array("\r","\n"), " <br>", $file_content ));
                $file_content= addslashes(str_replace( array("\r","\n"), "/*BREAK LINE*/", $file_content ));
                $data = array( 'file_path' => $file_path, 'code_to_highlight' => $file_content, 'format'=> $format, 'data_line'=> $data_line );
                AppUtils::deb($data, '$data::');
            }
        }
        $this->showTwigTemplate('code_highlight_page', $data, false, false, false);
    }

    public function test()
    {
        $this->load->model('mcms_item', '', true);
        $UriArray = $this->uri->uri_to_assoc(1);
        $this->setPageTitle('Test page');
        $data = array( );
                    /*
        $res_array= split_register_info('<h3>Grow your Business on <strong>tb.com.au</strong>!</h3>
<address>tb.com.au, Australia\'s No.1 listed websites on Google, Bing and Yahoo.</address>
<p>Be a part of Australia\'s premium Backpacker location, create your online Hostel or Tour listing and start receiving enquiries.</p>
<hr>
<p>Our Hostel and Tour <strong>Standard Listings</strong> are <span  underline;">free</span> for a limited time only so register your operator account and get listed.</p>
<p>Want more exposure? Boost your listing with our comprehensive <strong>Featured listings</strong> for $34.95AUD per month - less than a $1.20AUD per day!<br><br>Our <strong>Featured listings</strong> are rotated on our high-traffic homepage and take priority within the our search results - Meaning all featured listings will be listed above ALL standard listings.</p>
<hr>
<h3>HOSTE<b>LS</h3>
<address>Each&nbsp;<strong>HOSTEL&nbsp;</strong>listing contains everything you need to know from:</address><address></address>
<p></p>
<ul>
<li><address>Detailed Hostel Information</address></li>
<li><address>Hostel Website Link</address></li>
<li><em>Book Now Link</em></li>
<li><address>Booking Enquiry Form</address></li>
<li><address>Room Pricing</address></li>
<li><address>Photo Gallery</address></li>
<li><address>Extra Details</address></li>
<li><address>Hostel Facilities</address></li>
<li><address>About Us<em> (Operator)</em></address></li>
<li><address>Cancellation Policy</address></li>
<li><address>Hostel Policy</address></li>
<li><address>Hostel Location</address></li>
<li><address>Backpacker Reviews</address></li>
</ul>
<h3>TOURS</h3>
<address>Each&nbsp;<strong>TOUR&nbsp;</strong>listing contains everything you need to know from:</address><address></address>
<p></p>
<ul>
<li><address>Detailed Tour Information</address></li>
<li><address>Tour Website Link</address></li>
<li><em>Book Now Link</em></li>
<li><address>Booking Enquiry Form</address></li>
<li><address>Tour Pricing</address></li>
<li><address>Photo Gallery</address></li>
<li><address>Tour Highlights</address></li>
<li><address>About Us<em> (Operator)</em></address></li>
<li><address>Tour Schedule &amp; Pricing</address></li>
<li><address>Additional Information</address></li>
<li><address>Backpacker Reviews</address></li>
</ul>
<p>For more details on our pricing create your free operator account and log in!</p>
<h3>Terms&conditions</h3>
Rerms and conditions text Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.' );     */
        $this->showTwigTemplate('test', $data, false, true, true);
    }


    public function cleared_test()
    {
        $this->load->model('mfacility', '', true);
        $this->load->model('mcms_item', '', true);
        $UriArray = $this->uri->uri_to_assoc(1);
        $this->setPageTitle('Test page');
        $facilitysList = $this->mfacility->getFacilitysList(false, '', '', '', '', '');
        $this->showTwigTemplate('cleared_test', array( 'facilitysList'=> $facilitysList ), false, true, true);
    }

    public function add_vote_item_result()
    {
        $this->load->model('mvote_item_result', '', true);
        $UriArray = $this->uri->uri_to_assoc(3);
        $post_array = $this->input->post();
        $vote_id = AppUtils::getParameter($this, $UriArray, $post_array, 'vote_id');
        $vote_item_id = AppUtils::getParameter($this, $UriArray, $post_array, 'vote_item_id');
        $LoggedUserData = $this->muser->getLoggedUserData();
        $logged_user_id = '';
        if ( !empty($LoggedUserData['user_group'])  ) {
            $logged_user_id = $LoggedUserData['logged_user_id'];
        }
        if ( empty( $logged_user_id ) ) {
            $logged_user_id = $this->get_testing_user_id();
        }

        $ok= $this->mvote_item_result->UpdateVote_Item_Result( '', array('vote_item_id' => $vote_item_id, 'user_id' => $logged_user_id ) );
        if ( empty($vote_item_id) or empty($logged_user_id) ) {
            $this->output->set_content_type('application/json')->set_output(json_encode(array('ErrorMessage' => 'Invalid Parameters', 'ErrorCode' => 1, 'id' => '')));
        } else {
            $this->output->set_content_type('application/json')->set_output(json_encode(array('ErrorMessage' => '', 'ErrorCode' => 0, 'id' => $ok)));
        }
    }

}


 function split_register_info($str= '')    { //
//     $str= '<h3>  header <b>1
//1</b>11 </h3> text <b>11111</b>  <h3>  header <b>222</b>    </h3>text <b>2222</b><h3>header <b>3333</b></h3>text <b>33333333</b><h3>header <b>444444</b></h3>text
// <b>44444</b>';
     //$str= '<h3>header 1111</h3>text 11111<h3>header 222</h3>text 2222';
     $matsches= '';

     $str = AppUtils::nl2br2($str,'');
     $str= str_replace( array("<br>","<br />", "<br/>"), '', $str);

     $ret = preg_match_all('~<h3>(.+?)</h3>(.+?)(?=<h3>|$)~ium', $str, $matsches);
     $ret_array= array();
     if( $ret > 0 ) {
         foreach( $matsches[1] as $next_key=>$next_header ) {
             if ( !empty($matsches[2][$next_key]) ) {
                 $ret_array[]= array( 'index'=> count($ret_array)+1,  'header'=>$next_header, 'text'=>$matsches[2][$next_key] );
             }
         }
     }
     return $ret_array;
}
