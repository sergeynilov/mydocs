<?php
//define("SET_DEBUG", true);
class Banner extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ( !$this->ion_auth->logged_in() or !$this->ion_auth->in_group( array(ADMIN_GROUP_LABEL)) or empty($this->session->userdata['is_logged']) ) {
            redirect( base_url() . 'main/msg/type/warning/msg/' . urlencode ( 'You have no access to this page !').'/action/show-login' );
        }
        $this->load->model('mbanner', '', true);
    }

    public function index()
    {
        $this->load->helper('text');
        $UriArray = $this->uri->uri_to_assoc(4);
        $editor_message = $this->session->flashdata('editor_message');
        $post_array = $this->input->post();

        $page = AppUtils::getParameter($this, $UriArray, $post_array, 'page', 1);
        $sort = AppUtils::getParameter($this, $UriArray, $post_array, 'sort');
        $sort_direction = AppUtils::getParameter($this, $UriArray, $post_array, 'sort_direction');
        $PageParametersWithSort = $this->PreparePageParameters($UriArray, $post_array, false, true);
        $PageParametersWithoutSort = $this->PreparePageParameters($UriArray, $post_array, false, false);

        $this->load->library('pagination');
        $pagination_config= $this->getTemplateParams(array(),'pagination');
        $pagination_config['base_url'] = base_url() . 'admin/banner/index/page/';

        $RowsInTable = $this->mbanner->getBannersList(true, '', array(), $sort, $sort_direction);
        $pagination_config['total_rows'] = $RowsInTable;
        $this->pagination->initialize($pagination_config);
        $BannersList = array();
        if ($RowsInTable > 0) {
            $BannersList = $this->mbanner->getBannersList(false, $page, array(), $sort, $sort_direction, '');
        }

        $this->pagination->suffix = $this->PreparePageParameters($UriArray, $post_array, false, true);
        $pagination_links = $this->pagination->create_links();
        $this->setPageTitle('Banners List');
        $this->setBreadcrumbs(array('Banners List'=>''));
        $data = array( 'BannersList' => $BannersList, 'RowsInTable' => $RowsInTable, 'page' => $page, 'PageParametersWithSort' => $PageParametersWithSort, 'PageParametersWithoutSort' => $PageParametersWithoutSort, 'sort' => $sort, 'sort_direction' => $sort_direction, 'pagination_links' => $pagination_links, 'editor_message' => $editor_message);
        $this->showTwigTemplate('banners_list', $data, true, true, true);
    }

    public function edit($id = 0)
    {
        $post_array = $this->input->post();
        $IsInsert = strtoupper($id) == 'NEW';
        $editor_message = $this->session->flashdata('editor_message');
        $this->edit_load();

        $LoggedUserData = $this->muser->getLoggedUserData();
        $logged_user_id = $LoggedUserData['logged_user_id'];
        $layout_config= $this->getTemplateParams(array() , 'layout');

        $UriArray = $this->uri->uri_to_assoc(5);
        $PageParametersWithSort = $this->PreparePageParameters($UriArray, $post_array, true, true);
        $Banner = null;



        $PageParametersWithSort = $this->PreparePageParameters($UriArray, null, true, true);
        $this->edit_form_validation($IsInsert, $id);

        $RedirectUrl = base_url() . 'admin/banner/index' . $PageParametersWithSort;
        if (!$IsInsert) {
            $Banner = $this->mbanner->getRowById($id);
            if (empty($Banner)) {
                $this->session->set_flashdata('editor_message', "banner '" . $id . "' not found");
                redirect($RedirectUrl);
                return;
            }
        }
        $form_status = 'edit';

        if (!empty($_POST)) { // form was submitted
            $is_reopen = $this->input->post('is_reopen');
            $validation_status = $this->form_validation->run();
            $WasFieldChanged = true;
            if ($validation_status != FALSE) {
                if (!$IsInsert) { // verify if in edit status Row was changed.
                    $WasFieldChanged = AppUtils::WasFieldChanged($Banner, $this->input->post(), array('id', 'created_at'));
                    if ( !$WasFieldChanged and !empty($_FILES['img_file_upload']['name']) ) {
                        $WasFieldChanged= true;
                    }
                    if ($WasFieldChanged == ''
                    ) {
                        if ($is_reopen)
                            $RedirectUrl = base_url() . 'admin/banner/edit/' . $post_array['id'] . $PageParametersWithSort;
                        redirect($RedirectUrl);
                        return;
                    }
                }


                $this->edit_makesave($IsInsert, $id, $is_reopen, $RedirectUrl, $LoggedUserData, $logged_user_id, $PageParametersWithSort, $post_array, $this->app_config, $WasFieldChanged);
            } else {
                $form_status = 'invalid';
                $Banner = $this->edit_fill_current_data($Banner, $IsInsert, $id, $LoggedUserData, $logged_user_id);

            }
        } // if (!empty($_POST)) { // form was submitted
        $this->setPageTitle(($IsInsert ? "Add" : "Update").' Banner');
        $this->setBreadcrumbs(array('Banners List' =>'admin/banner/index' . $PageParametersWithSort, ($IsInsert ? "Add" : "Update") =>'' ));
        $data = array('User' => AppUtils::SaveFormHTML($Banner), 'id' => $id, 'Banner' => $Banner, 'IsInsert' => $IsInsert, 'PageParametersWithSort' => $PageParametersWithSort, 'form_status' => $form_status, 'editor_message' => $editor_message, 'validation_errors_text' => validation_errors($layout_config['backend_error_icon_start'], $layout_config['backend_error_icon_end']) );
        $this->showTwigTemplate('banner_edit', $data, true, true, true);
    }

    private function edit_load()
    {
        get_instance()->load->model('mbanner', '', true);
        get_instance()->load->library('form_validation');
    }

    private function edit_form_validation($IsInsert, $id)
    {
        if ($IsInsert) {
            $this->form_validation->set_rules('title', 'Title', 'required|is_unique[banner.title]');
        } else {
            $this->form_validation->set_rules('title', 'Title', 'required|is_unique[banner.title.id.' . $id . ']');
        }
        $this->form_validation->set_rules('descr', 'Description', 'required');
        $this->form_validation->set_rules('href', 'Href', 'required|valid_url');
        $this->form_validation->set_rules('ordering', 'Ordering', 'required|integer');
    }

    private function edit_makesave($IsInsert, $id, $is_reopen, $RedirectUrl, $LoggedUserData, $logged_user_id, $PageParametersWithSort, $post_array, $config_array, $WasFieldChanged)
    {
        $this->db->trans_start();
        $update_data= array( 'title' => $post_array['title'], 'descr' => $post_array['descr'], 'href' => $post_array['href'], 'ordering' => $post_array['ordering'] );
        if ( !empty($_FILES['img_file_upload']['name']) ) {
            $update_data['img']= $_FILES['img_file_upload']['name'];
        }
        if ( $IsInsert ) {
            $BannerId = $this->mbanner->UpdateBanner('', $update_data );
        } else {
            $BannerId=  $this->mbanner->UpdateBanner( $post_array['id'], $update_data ) ;
        }
        if ($is_reopen) {
            $RedirectUrl = base_url() . 'admin/banner/edit/' . $BannerId . $PageParametersWithSort;
        }
        if ($BannerId) {

            $dir_path = $this->app_config['document_root'] . $this->app_config['uploads_banners_dir'];
            if (!is_dir($dir_path)) {
                mkdir($dir_path, 0777);
            }
            $upload_config['upload_path'] = $dir_path;
            if (!empty($this->app_config['uploads_images_allowed_types'])) $upload_config['allowed_types'] = $this->app_config['uploads_images_allowed_types'];
            if (!empty($this->app_config['uploads_images_max_size'])) $upload_config['max_size'] = $this->app_config['uploads_images_max_size'];
            if (!empty($this->app_config['uploads_images_max_width'])) $upload_config['max_width'] = $this->app_config['uploads_images_max_width'];
            if (!empty($this->app_config['uploads_images_max_height'])) $upload_config['max_height'] = $this->app_config['uploads_images_max_height'];

            $this->load->library('upload', $upload_config);

            if (!empty($_FILES['img_file_upload']['name'])) {
                $res= $this->upload->do_upload('img_file_upload');
            }

            $this->session->set_flashdata('editor_message', $BannerId . " : Banner" . " '" . $this->input->post('title') . "' was " . ($IsInsert ? "inserted" : "updated"));
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();
            }
            redirect($RedirectUrl);
            return;
        }
    }

    private function edit_fill_current_data($Banner, $IsInsert, $id, $LoggedUserData, $logged_user_id)
    {
        $Banner['id'] = $id;
        $Banner['img'] = set_value('img');
        $Banner['title'] = set_value('title');
        $Banner['descr'] = set_value('descr');
        $Banner['href'] = set_value('href');
        $Banner['ordering'] = set_value('ordering');
        return $Banner;
    }



    public function delete($id = 0)
    {
        $this->load->model('mbanner', '', true);

        if (!empty($id)) {
            $UriArray = $this->uri->uri_to_assoc(5);
            $PageParametersWithSort = $this->PreparePageParameters($UriArray, null, false, true);
            $RedirectUrl = base_url().'admin/banner/index' . $PageParametersWithSort;

            $Banner = $this->mbanner->getRowById($id);
            if (empty($Banner)) {
                $this->session->set_flashdata('editor_message', "Banner '" . $id . "' not found");
                redirect($RedirectUrl);
                return;
            }
            $bannerTitle = $Banner['title'];
            $bannerImg = $Banner['img'];
            $this->db->trans_start();
            $OkResult = $this->mbanner->DeleteBanner($id);
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();
                $dir_path = $this->app_config['document_root'] . DIRECTORY_SEPARATOR . $this->app_config['uploads_banners_dir'];
                if (file_exists($dir_path . $bannerImg)) {
                    unlink($dir_path . $bannerImg);
                }
            }
            if ($OkResult) {
                $this->session->set_flashdata('editor_message',  $id . " : Banner" . " '" . $bannerTitle . "' was deleted");
                redirect($RedirectUrl);
                return;
            }
        }

    }

    private function PreparePageParameters($UriArray, $_post_array, $WithPage, $WithSort)
    {
        $ResStr = '';
        if (!empty($_post_array)) { // form was submitted
            if ($WithPage) {
                $page = $this->input->post('page');
                $ResStr .= !empty($page) ? 'page/' . $page . '/' : 'page/1/';
            }
            if ($WithSort) {
                $sort_direction = $this->input->post('sort_direction');
                $ResStr .= !empty($sort_direction) ? 'sort_direction/' . $sort_direction . '/' : '';
                $sort = $this->input->post('sort');
                $ResStr .= !empty($sort) ? 'sort/' . $sort . '/' : '';
            }
        } else {
            if ($WithPage) {
                $ResStr .= !empty($UriArray['page']) ? 'page/' . $UriArray['page'] . '/' : 'page/1/';
            }
            if ($WithSort) {
                $ResStr .= !empty($UriArray['sort_direction']) ? 'sort_direction/' . $UriArray['sort_direction'] . '/' : '';
                $ResStr .= !empty($UriArray['sort']) ? 'sort/' . $UriArray['sort'] . '/' : '';
            }
        }
        if (substr($ResStr, strlen($ResStr) - 1, 1) == '/') {
            $ResStr = substr($ResStr, 0, strlen($ResStr) - 1);
        }
        return '/' . $ResStr;
    }

}

