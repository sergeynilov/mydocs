<?php
class Mbanner extends CI_Model
{
    private $ion_auth_config_tables= '';

    public function __construct()
    {
        parent::__construct();
        $this->load->library('ion_auth');
        $ci = & get_instance();
        //echo '<pre>$ci::'.print_r($ci,true).'</pre>';
        $this->ion_auth_config_tables= $ci->get_ion_auth_config_tables();
    }


    public function getBannersSelectionList($OutputFormatCount = false, $page = '', $filter_title = '', $sorting = '', $sort = '')
    {
        $bannersList = $this->mbanner->getBannersList(false, '', '', 'title', '', 'id,title');
        $ResArray = array();
        foreach ($bannersList as $lbanner) {
            $ResArray[] = array('key' => $lbanner['id'], 'value' => $lbanner['title']);
        }
        return $ResArray;
    }

    public function getBannersList($OutputFormatCount = false, $page = '', $filters = array(), $sorting = '', $sort = '')
    {
        if (empty($sorting))
            $sorting = 'ordering';
        $config_data = $this->config->config;
        $limit = !empty($filters['limit']) ? $filters['limit'] : '';
        $offset = !empty($filters['offset']) ? $filters['offset'] : '';
        $is_page_positive_integer= AppUtils::is_positive_integer($page);
        if ( !empty($page) and $is_page_positive_integer ) {
            $limit = '';
            $offset = '';
        }
        if (!empty($config_data) and $is_page_positive_integer) {
            $per_page= ( !empty($filters['per_page']) and AppUtils::is_positive_integer($filters['per_page']) ) ? $filters['per_page'] : $config_data['per_page'];
            $limit = $per_page;
            $offset = ($page - 1) * $per_page;
        }

        $additive_fields_for_select= "";
        $fields_for_select= $this->ion_auth_config_tables['banner'].".*";
        if ( !empty($filters['fields_for_select']) ) {
            $fields_for_select= $filters['fields_for_select'];
        }

        if ( ( !empty($limit) and AppUtils::is_positive_integer($limit) ) and ( !empty($offset) and AppUtils::is_positive_integer($offset) ) ) {
            $this->db->limit($limit, $offset);
        }

        if ( ( !empty($limit) and AppUtils::is_positive_integer($limit) ) ) {
            $this->db->limit($limit);
        }


        $fields_for_select.= ' ' . $additive_fields_for_select;

        if (!empty($sorting)) {
            $this->db->order_by($sorting, ((strtolower($sort) == 'desc' or strtolower($sort) == 'asc') ? $sort : ''));
        }


        if ($OutputFormatCount) {
            return $this->db->count_all_results($this->ion_auth_config_tables['banner']);
        } else {
            $query = $this->db->from($this->ion_auth_config_tables['banner']);
            if (strlen(trim($fields_for_select)) > 0) {
                $query->select($fields_for_select);
            }
            return $query->get()->result('array');
        }
    }

    public function checkIsTitleUnique($title, $banner_id='')
    {
        $config_data = $this->config->config;
        $this->db->like('title', $title);
        if (!empty($banner_id)) {
            $this->db->where('id != ' . $banner_id);
        }
        echo '<pre>$this->ion_auth_config_tables::'.print_r($this->ion_auth_config_tables,true).'</pre>';
        die("-1 XXZ");
        $checkCount= $this->db->count_all_results($this->ion_auth_config_tables['banner']);
        AppUtils::DebToFile(' checkIsTitleUnique $checkCount::' . print_r($checkCount, true), false);
        return $checkCount==0;
    }

    public function checkIsImgUnique($img, $banner_id='')
    {
        $config_data = $this->config->config;
        $this->db->like('img', $img);
        if (!empty($banner_id)) {
            $this->db->where('id != ' . $banner_id);
        }
        $checkCount= $this->db->count_all_results($this->ion_auth_config_tables['banner']);
        AppUtils::DebToFile(' checkIsImgUnique $checkCount::' . print_r($checkCount, true), false);
        return $checkCount==0;
    }

    public function getRowById( $id, $additive_params= array() )
    {
        $additive_fields_for_select= '';
        $fields_for_select= $this->ion_auth_config_tables['banner'] . '.*';
        if (!empty( $additive_params['fields_for_select'] )) {
            $fields_for_select= $additive_params['fields_for_select'];
        }

        $fields_for_select.= $additive_fields_for_select;
        $this->db->where( $this->ion_auth_config_tables['banner'] . '.id', $id);

        $this->db->select( $fields_for_select );

        $query = $this->db->from($this->ion_auth_config_tables['banner']);
        $resultRows = $query->get()->result('array');
        //echo '<pre>'.count($resultRows).'::$resultRows::'.print_r($resultRows,true).'</pre>';
        return $resultRows[0];
    }

    public function UpdateBanner($id, $DataArray)
    {
        if (empty($id)) {
            $DataArray['created_at'] = AppUtils::ShowFormattedDateTime(time(), 'MySql');
            $Res = $this->db->insert($this->ion_auth_config_tables['banner'], $DataArray);
            if ($Res)
                return AppUtils::getMySqlInsertId($this->db->conn_id);
        } else {
            $Res = $this->db->update($this->ion_auth_config_tables['banner'], $DataArray, array('id' => $id));
            if ($Res)
                return $id;
        }
    }

    public function DeleteBanner($id)
    {
        if (!empty($id)) {
            $this->db->where('id', $id);
            $Res = $this->db->delete($this->ion_auth_config_tables['banner']);
            return $Res;
        }
    }


}

?>