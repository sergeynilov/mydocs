<?php
class NsnReadWriteToExcel extends CApplicationComponent
{
	protected $alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  protected $header_style_array = array('font' => array('bold' => true, 'color' => array('rgb' => 'FF0000'), 'size' => 14, 'name' => 'Verdana', 'height'=>32) );
  protected $subheader_style_array = array('font' => array('bold' => false, 'color' => array('rgb' => '993333'), 'size' => 12, 'name' => 'Verdana'));

	protected $table_header_row_style_array = array('font' => array('bold' => true, 'color' => array('rgb' => '544954'), 'size' => 10, 'name' => 'Verdana'));
  protected $group_header_row_style_array = array('font' => array('bold' => true, 'color' => array('rgb' => '7095fa'), 'size' => 10, 'name' => 'Verdana'));

  protected $data_row_style_array = array('font' => array('bold' => false, 'color' => array('rgb' => '50493C'), 'size' => 9, 'name' => 'Courier'));
  protected $summary_row_style_array = array('font' => array('bold' => true, 'color' => array('rgb' => 'C00000'), 'size' => 11, 'name' => 'Courier'));


	protected $data_array;
	protected $title_text;
	protected $subtitle_text_cell;
	protected $title_text_cell;
	protected $subtitle_text;
	protected $top_indent = 0;
	protected $left_indent = 0;
	protected $cells_format = array();
	protected $summery_top_indent = 0;
	protected $summery_bottom_indent = 0;
	protected $attach_files_array= array();
	protected $attach_files_row_indent = 0;

	protected $auto_size_columns_array = array();
	protected $current_row = 1;

	protected $ReadWriteToExcelObj;
	protected $ExcelActiveSheetObj;

	public function load_excel()
	{
		$phpExcelPath = Yii::getPathOfAlias('ext.PHPExcel'); // http://www.yiiframework.com/wiki/101/how-to-use-phpexcel-external-library-with-yii/
		spl_autoload_unregister(array('YiiBase', 'autoload'));

		include($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');

		spl_autoload_unregister(array('YiiBase', 'autoload'));
		Yii::import('ext.PHPExcel.Classes.PHPExcel', true);
		spl_autoload_register(array('YiiBase', 'autoload'));
		$this->ReadWriteToExcelObj = new PHPExcel();
		Yii::import('ext.PHPExcel.XPHPExcel');
		$this->ReadWriteToExcelObj = XPHPExcel::createPHPExcel();
		$this->ExcelActiveSheetObj = $this->ReadWriteToExcelObj->setActiveSheetIndex(0);
		return $this;
	}

	public function init_data($data_array, $data_props_array )
	{
		if ($data_array !== false) $this->data_array = $data_array;
		foreach ($data_props_array as $prop_data_key => $prop_data_value) {
			if ($prop_data_key == 'title_text') {
				$this->title_text= $prop_data_value;
			}
			...
		}
		return $this;
	}

	public function set_sheet_props($props_array)
	{
		$ExcelObjProps = $this->ReadWriteToExcelObj->getProperties();
		foreach ($props_array as $prop_key => $prop_value) {
			if ($prop_key == 'creator') {
				$ExcelObjProps->setCreator($prop_value);
			}
			...
		}
		return $this;
	}

	public function set_style_props($style_props_array)
	{
		foreach ($style_props_array as $style_props_key => $style_prop_value) {
			if ($style_props_key == 'header_style') {
				$this->header_style_array= $style_prop_value;
			}
			...
		}
	}

	public function set_report_props($report_props_array)
	{
		foreach ($report_props_array as $report_prop_key => $report_prop_value) {
			if ($report_prop_key == 'top_indent') {
				$this->top_indent = $report_prop_value;
			}
			...
		}
		return $this;
	}

	public function fill_report_data()
	{
	...
	}


	public  function set_attached_files($attach_files_array, $row_indent)
	{
		$this->attach_files_array= $attach_files_array;
		$this->attach_files_row_indent = $row_indent;
	}

	private function make_attach_files()
	{
		$objDrawingPType = new PHPExcel_Worksheet_Drawing();
		...
		}
	}

private function int_coordsto_chars($row, $cell)
	{
		$cell = (int)$cell;
		...
		return $cell_char . ( isset($row) ? $row : "" );
	}

	public function open_excell($filename)
	{
		header('Content-Type: application/vnd.ms-excel');
		...
		return $this;
	}

	function __destruct()
	{
	}
}
