<?php

/**
 * This is the model class for table "jsn_order".
 *
 * The followings are the available columns in table 'order':
 * @property integer $id
 * @property string $name
 * @property string $created_at
 */
class Order extends CActiveRecord
{
	public $order_qty_sum = 0;
	public $max_created_at = '';
	public $min_created_at = '';
	public $filter_created_at_from = '';
	public $filter_created_at_till = '';
	public $good_name = '';
	private $StatusOptions = array('D' => 'Drift', 'C' => 'Canceled', 'W' => 'Waiting for payment', 'F' => 'Finished/Payed');

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'jsn_order';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'),
			array('name', 'unique',
				'allowEmpty' => false,
				'message' => 'Sorry, this Order Name has already been taken.'), // , 'max'=>50

			/* array('created_at','default',
				'value'=>new CDbExpression('NOW()'),
				'setOnEmpty'=>false,'on'=>'insert'),   */
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name', 'safe', 'on' => 'search'),
		);
	}

	public function beforeSave()
	{
		if ($this->isNewRecord) {
			//$this->created_at = new CDbExpression('NOW()');
		}
		return parent::beforeSave();
	}


	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'good' => array(self::BELONGS_TO, 'Good', 'good_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'created_at' => 'Created At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('created_at', $this->created_at, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	public function getStatusOptions($TopLabel = '')
	{
		if (!empty($TopLabel)) {
			$A[''] = $TopLabel;
			foreach ($this->StatusOptions as $key => $Value) {
				$A[$key] = $Value;
			}
			return $A;
		}
		return $this->StatusOptions;
	}

	public function getTourStatusLabel($Status)
	{
		if (!empty($this->StatusOptions [$Status])) {
			return $this->StatusOptions[$Status];
		}
		return '';
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Order the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}


	public function getOrdersList($list_output_format, $page = '', $filters = array(), $sort = '', $sort_direction = '')
	{
		if ($list_output_format != Appfuncs::$LIST_OUTPUT_FORMAT_COUNT and $list_output_format != Appfuncs::$LIST_OUTPUT_FORMAT_DATA_PROVIDER and $list_output_format != Appfuncs::$LIST_OUTPUT_FORMAT_LIST) {
			$list_output_format != Appfuncs::$LIST_OUTPUT_FORMAT_DATA_PROVIDER;
		}
		$enablePagination = Yii::app()->controller->app_config['enablePagination'];
		$admin_pageSize = Yii::app()->controller->app_config['admin_pageSize'];
		$pagination_filters = Appfuncs::copy_filters_array($filters, 'filter_', true);

		if (empty($sort)) $sort = 'O.created_at';
		if ($list_output_format == Appfuncs::$LIST_OUTPUT_FORMAT_COUNT) $page = '';
		$criteria = new CDbCriteria;
		$criteria->alias = 'O';
		$criteria->with = 'good';
		$criteria->select = 'O.*, good.name as good_name ';


		if (!empty($filters['good_id'])) {
			$criteria->compare('O.good_id', $filters['good_id']);
		}
		if (!empty($filters['client_id'])) {
			$criteria->compare('O.client_id', $filters['client_id']);
		}
		if (!empty($filters['status'])) {
			$criteria->compare('O.status', $filters['status']);
		}
		if (!empty($filters['department_id'])) {
			$criteria->compare('O.department_id', $filters['department_id']);
		}
		if ( !empty($filters['created_at_from']) and !empty($filters['created_at_till'])) {
			$criteria->addBetweenCondition('O.created_at', $filters['created_at_from'], $filters['created_at_till'] /*, 'OR'*/ );
		} else {
			if ( !empty($filters['created_at_from']) ) {
				$criteria->compare('O.created_at', '>=' . $filters['created_at_from'] );
			}
			if ( !empty($filters['created_at_till']) ) {
				$criteria->compare('O.created_at',  '<' . $filters['created_at_till'] );
			}
		}

		if (!empty($filters['limit'])) {
			$criteria->limit = $filters['limit'];
		}

		if (!empty($filters['offset'])) {
			$criteria->offset = $filters['offset'];
		}

		if (!empty($filters['manual_pagination'])) {
			Appfuncs::deb($filters, '$filters::');
			//$admin_pageSize = $filters['manual_pagination']['page_size'];
			if (empty($filters['enablePagination'])) {
				$PaginationObj = false;
				$criteria->limit = -1;
				$criteria->offset = -1;
			} else {
				$result_rows_start = (($filters['manual_pagination']['page_size']) * ($filters['manual_pagination']['page'] - 1)) + 1;
				$criteria->offset = $result_rows_start - 1;
				$criteria->limit = $filters['manual_pagination']['page_size'];
				$PaginationObj = false; //array('pageSize' => $admin_pageSize/*, 'params' => $pagination_filters*/ );
			}

		} else {
			if (!$enablePagination) {
				$PaginationObj = false;
				$criteria->limit = -1;
				$criteria->offset = -1;
			} else {
				$PaginationObj = ($list_output_format == Appfuncs::$LIST_OUTPUT_FORMAT_DATA_PROVIDER ? array('pageSize' => $admin_pageSize,
					'params' => $pagination_filters) : false);
			}
		}

		if ($list_output_format == Appfuncs::$LIST_OUTPUT_FORMAT_COUNT) {
			return Order::model()->count($criteria);
		} else {
			$criteria->order = $sort . ' ' . $sort_direction;
			$ActiveDataProvider = new CActiveDataProvider(get_class($this), array(
				'criteria' => $criteria,
				'pagination' => $PaginationObj,
			));
			if ($list_output_format == Appfuncs::$LIST_OUTPUT_FORMAT_DATA_PROVIDER) {
				return $ActiveDataProvider;
			}
			if ($list_output_format == Appfuncs::$LIST_OUTPUT_FORMAT_LIST) {
				return $ActiveDataProvider->getData();
			}
		}
	}

	public function getOrdersQtySumList($filters = array(), $sort = '', $sort_direction = '')
	{
		$tablePrefix = 'jsn_';
		$criteria = new CDbCriteria;
		$criteria->alias = 'O';
		$criteria->select = ' sum(O.order_qty) as order_qty_sum, O.good_id, good.name as good_name ';
		$criteria->condition = 'O.order_qty  > 0';
		if (!empty($filters['client_id'])) {
			$criteria->compare('O.client_id', $filters['client_id']);
		}
		if (!empty($filters['status'])) {
			$criteria->compare('O.status', $filters['status']);
		}
		if (!empty($filters['department_id'])) {
			$criteria->compare('O.department_id', $filters['department_id']);
		}
		if (!empty($filters['goods_list'])) {
			$criteria->compare('O.good_id', $filters['goods_list']);
		}
		if ( !empty($filters['created_at_from']) and !empty($filters['created_at_till'])) {
			$criteria->addBetweenCondition('O.created_at', $filters['created_at_from'], $filters['created_at_till'] /*, 'OR'*/ );
		} else {
			if ( !empty($filters['created_at_from']) ) {
				$criteria->compare('O.created_at', '>=' . $filters['created_at_from'] );
			}
			if ( !empty($filters['created_at_till']) ) {
				$criteria->compare('O.created_at',  '<' . $filters['created_at_till'] );
			}
		}

		$criteria->with = 'good';
		if (!empty($sort)) {
			$criteria->order = $sort . ' ' . $sort_direction;
		} else {
			$criteria->order = 'order_qty_sum DESC';
		}
		$criteria->group = 'O.good_id';
		$criteria->having = 'order_qty_sum > 0 ';
		return Order::model()->findAll($criteria);
	}


	public function getOrdersQtySumByCreatedAtList($filters = array(), $sort = '', $sort_direction = '')
	{
		$tablePrefix = 'jsn_';
		$criteria = new CDbCriteria;
		$criteria->alias = 'O';
		$criteria->select = ' sum(O.order_qty) as order_qty_sum, O.good_id, O.created_at, good.name as good_name  '; // , good.name as good_name
		$criteria->condition = 'O.order_qty  > 0';
		if (!empty($filters['client_id'])) {
			$criteria->compare('O.client_id', $filters['client_id']);
		}
		if (!empty($filters['status'])) {
			$criteria->compare('O.status', $filters['status']);
		}
		if (!empty($filters['department_id'])) {
			$criteria->compare('O.department_id', $filters['department_id']);
		}
		if (!empty($filters['goods_list'])) {
			$criteria->compare('O.good_id', $filters['goods_list']);
		}
		if ( !empty($filters['created_at_from']) and !empty($filters['created_at_till'])) {
			$criteria->addBetweenCondition('O.created_at', $filters['created_at_from'], $filters['created_at_till'] /*, 'OR'*/ );
		} else {
			if ( !empty($filters['created_at_from']) ) {
				$criteria->compare('O.created_at', '>=' . $filters['created_at_from'] );
			}
			if ( !empty($filters['created_at_till']) ) {
				$criteria->compare('O.created_at',  '<' . $filters['created_at_till'] );
			}
		}

		$criteria->with = 'good';
		if (!empty($sort)) {
			$criteria->order = $sort . ' ' . $sort_direction;
		} else {
			$criteria->order = 'order_qty_sum DESC';
		}
		$criteria->group = 'O.created_at, O.good_id';
		$criteria->having = 'order_qty_sum > 0 ';
		return Order::model()->findAll($criteria);
	}

	public function getOrdersMinMaxDate( $minmax_type= 'max', $filters = array(), $output_format='' ) {
		if ( $minmax_type!= 'max' and $minmax_type != 'min' ) $minmax_type= 'max';
		$tablePrefix = 'jsn_';
		$criteria = new CDbCriteria;
		$criteria->alias = 'O';
		$criteria->select = $minmax_type . '(O.created_at) as '.$minmax_type.'_created_at';
		$criteria->condition = 'O.order_qty  > 0';
		if (!empty($filters['client_id'])) {
			$criteria->compare('O.client_id', $filters['client_id']);
		}
		if (!empty($filters['status'])) {
			$criteria->compare('O.status', $filters['status']);
		}
		if (!empty($filters['department_id'])) {
			$criteria->compare('O.department_id', $filters['department_id']);
		}
		if (!empty($filters['goods_list'])) {
			$criteria->compare('O.good_id', $filters['goods_list']);
		}
		if ( !empty($filters['created_at_from']) and !empty($filters['created_at_till'])) {
			$criteria->addBetweenCondition('O.created_at', $filters['created_at_from'], $filters['created_at_till'] /*, 'OR'*/ );
		} else {
			if ( !empty($filters['created_at_from']) ) {
				$criteria->compare('O.created_at', '>=' . $filters['created_at_from'] );
			}
			if ( !empty($filters['created_at_till']) ) {
				$criteria->compare('O.created_at',  '<' . $filters['created_at_till'] );
			}
		}

		$res_value= Order::model()->find($criteria);
		if ( $output_format == Appfuncs::$DATE_OUTPUT_FORMAT_STRING or empty($output_format) ) {
			if ( $minmax_type== 'max' ) return $res_value->max_created_at;
			if ( $minmax_type== 'min' )return $res_value->min_created_at;
		}
		if ( $output_format == Appfuncs::$DATE_OUTPUT_FORMAT_UNXSTAMP ) {
			if ( $minmax_type== 'max' ) return strtotime($res_value->max_created_at);
			if ( $minmax_type== 'min' ) return strtotime($res_value->min_created_at);
		}
		if ( $output_format == Appfuncs::$DATE_OUTPUT_FORMAT_STRUCTURE ) {
			if ( $minmax_type== 'max' ) $created_at_unx= strtotime($res_value->max_created_at);
			if ( $minmax_type== 'min' ) $created_at_unx= strtotime($res_value->min_created_at);
			return array( 'day'=>(int)strftime("%d",$created_at_unx), 'month'=>(int)strftime("%m",$created_at_unx), 'year'=>(int)strftime("%Y",$created_at_unx),
				'hour'=>(int)strftime("%H",$created_at_unx), 'minute'=>(int)strftime("%d",$created_at_unx), 'second'=>(int)strftime("%S",$created_at_unx) );
		}


		return $res_value->max_created_at;
	}

	public function getOrdersSelectionList()
	{
		$OrdersList = $this->getOrdersList(Appfuncs::$LIST_OUTPUT_FORMAT_LIST, '', array(), '', '', 'C.name', true /* '', 'order.id, order.name'*/);
		$ResArray = array();
		foreach ($OrdersList as $lOrder) {
			$ResArray[$lOrder->id] = $lOrder->name;
		}
		return $ResArray;
	}


	public function getRowById($id)
	{
		$model = Order::model()->findByPk((int)$id);
		return $model;
	}

}