<?php

class Extentions_testingController extends Controller
{
	public $layout = '//layouts/backend_main';


	public function actionExcel_writing()
	{
		$client_id = $this->getParameter('select_client_id');
		$department_id = $this->getParameter('select_department_id' /*, '', 6 */ );
		$status = $this->getParameter('select_status');
		$filter_created_at_from = $this->getParameter('filter_created_at_from', 'Order');
		$filter_created_at_till = $this->getParameter('filter_created_at_till', 'Order');

		$goods_list = array( /*1, 5*/ );
		$sort = 'created_at';
		$sort_direction = 'asc';
		$OrdersList = Order::model()->getOrdersQtySumByCreatedAtList(array('client_id' => $client_id, 'department_id' => $department_id, 'status' => $status, 'goods_list' => $goods_list,
			'created_at_from' => $filter_created_at_from, 'created_at_till' => $filter_created_at_till), $sort, $sort_direction);
		$is_first_row = true;
		$current_created_at = '';
		$orders_by_date_subarray = array();
		foreach ($OrdersList as $Order) {
			if ($is_first_row or $current_created_at != $Order->created_at) {
				$un_created_at = strtotime($current_created_at);
				if (!$is_first_row) {
					$orders_created_at_array[] = array('created_at' => $current_created_at, 'created_at_day' => (int)strftime("%d", $un_created_at), 'created_at_month' => (int)strftime("%m",
							$un_created_at), 'created_at_year' => (int)strftime("%Y", $un_created_at), 'data' => $orders_by_date_subarray);
				}
				$current_created_at = $Order->created_at;
				$orders_by_date_subarray = array();
				$orders_by_date_subarray[] = array('good_name' => $Order->good_name, 'good_id' => $Order->good_id, 'order_qty_sum' => $Order->order_qty_sum);
			} else {
				$orders_by_date_subarray[] = array('good_name' => $Order->good_name, 'good_id' => $Order->good_id, 'order_qty_sum' => $Order->order_qty_sum);
			}
			$is_first_row = false;
		}
		$orders_created_at_array[] = array('created_at' => $current_created_at, 'created_at_day' => (int)strftime("%d", $un_created_at), 'created_at_month' => (int)strftime("%m",
				$un_created_at), 'created_at_year' => (int)strftime("%Y", $un_created_at), 'filter_created_at_from' => $filter_created_at_from, 'filter_created_at_till' => $filter_created_at_till, 'data' => $orders_by_date_subarray);

		$excel_data_array= array();
		$summery_fields_array= array( '', 'Common Sum:', 0 );
		foreach( $orders_created_at_array as $orders_created_at ) {
			$summery_byday_fields_array= array( '', 'Summary by day:', 0 );
			$excel_data_array[]= array( 'data'=>array( '', strftime($this->app_config['DateAsTextFormat'], mktime(null,null,null, $orders_created_at['created_at_month'], $orders_created_at['created_at_day'], $orders_created_at['created_at_year']) ), '', '', '' ), 'group'=>'group_header' );
			foreach( $orders_created_at['data'] as $orders_line ) {
				$excel_data_array[]= array( 'data'=>array( $orders_line['good_id'], $orders_line['good_name'], $orders_line['order_qty_sum'], '', '' ), 'group'=>'data_row' );
				$summery_byday_fields_array[2]+= $orders_line['order_qty_sum'];
				$summery_fields_array[2]+= $orders_line['order_qty_sum'];
			}
			$excel_data_array[]= array( 'data'=> $summery_byday_fields_array, 'group'=>'summary_row' );
		}
		$excel_data_array[]= array( 'data'=> $summery_fields_array, 'group'=>'summary_row' );

		$title_text= 'Orders daily report'; $subtitle_text= 'With selected parameters';
		$NsnReadWriteToExcel= new NsnReadWriteToExcel();
		$NsnReadWriteToExcel->load_excel()->set_sheet_props( array( 'creator'=>"Sergey Nilov", 'last_modified_by'=> "Sergey Nilov",
			'title'=> "Orders daily report",
			'subject'=>"Office 2007 XLSX Orders report", 'description'=>"Orders report created by Sergey Nilov.", 'keywords'=>"Orders report Sergey Nilov.",
			'category'=>"Orders report created by Sergey Nilov." ) );

		$NsnReadWriteToExcel->init_data( $excel_data_array, array('title_text'=>$title_text, 'subtitle_text'=>$subtitle_text,
		'title_text_cell'=>1,	'subtitle_text_cell'=>1, 'auto_size_columns'=>array(0,1,2) ) );
		$NsnReadWriteToExcel->set_report_props(array('top_indent'=> 2, 'left_indent'=>3, 'cells_format'=>array(2=>'#,##0.00'), 'summery_top_indent'=>1, 'summery_bottom_indent'=>2 ) );

		/* $styles_array= array();
		$styles_array['header_style']= array('font' => array('bold' => true, 'color' => array('rgb' => '00FF00'), 'size' => 14, 'name' => 'Bitstream Charter', 'height'=>32) );
		$styles_array['subheader_style']= array('font' => array('bold' => false, 'color' => array('rgb' => '3F2EFF'), 'size' => 12, 'name' => 'Bitstream Charter') );
		$styles_array['group_header_style']= array('font' => array('bold' => true, 'color' => array('rgb' => 'FF1245'), 'size' => 10, 'name' => 'Bitstream Charter'));
		$styles_array['data_row_style']= array('font' => array('bold' => false, 'color' => array('rgb' => '9BA301'), 'size' => 9, 'name' => 'Mukti Narrow'));
		$styles_array['summary_row_style']= array('font' => array('bold' => true, 'color' => array('rgb' => '50493C'), 'size' => 11, 'name' => 'Mukti Narrow'));
		$NsnReadWriteToExcel->set_style_props( $styles_array ); */
		if (!empty($this->app_config['attach_site_info_file']) and file_exists($this->app_config['attach_site_info_file'])) {
			$NsnReadWriteToExcel->set_attached_files( array($this->app_config['attach_site_info_file']), 10 );
		}
		$NsnReadWriteToExcel->fill_report_data()->open_excell('goods_reports_'.time().'.xls');
	}


	public function actionWriting_goods_into_excel()
	{ // writing_goods_into_excel
		if (extension_loaded('zip')) {
			//echo '<pre>$::'.print_r('zip INSALEED',true).'<pre>';
		}
		if (class_exists('ZipArchive')) {
			//echo '<pre>$::'.print_r('class_exists ZipArchive',true).'<pre>';
		}

		$phpExcelPath = Yii::getPathOfAlias('ext.PHPExcel'); // http://www.yiiframework.com/wiki/101/how-to-use-phpexcel-external-library-with-yii/
		spl_autoload_unregister(array('YiiBase', 'autoload'));
		include($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');

		spl_autoload_unregister(array('YiiBase', 'autoload'));
		Yii::import('ext.PHPExcel.Classes.PHPExcel', true);
		spl_autoload_register(array('YiiBase', 'autoload'));

		$objPHPExcel = new PHPExcel();

		Yii::import('ext.PHPExcel.XPHPExcel');
		$objPHPExcel = XPHPExcel::createPHPExcel();
		$objPHPExcel->getProperties()->setCreator("Report Maker")
			->setLastModifiedBy("Report Maker")
			->setTitle('Goods List')
			->setSubject('Goods List made by Report Maker')
			->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
			->setKeywords("office 2007 openxml php")
			->setCategory("Report Maker made this report file");

		$GoodsList = Good::model()->getGoodsList(Appfuncs::$LIST_OUTPUT_FORMAT_LIST, '', array(), 'goodgroup_id asc, name asc');
		$current_goodgroup_name = '';
		$first_row = true;
		$data_array = array();
		$group_text = '';
		$row_number = 2;
		$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A1', "Good Name")
			->setCellValue('B1', "Price")
			->setCellValue('C1', "Discount ( % )");
		$objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);

		foreach ($GoodsList as $Good) {
			$Goodgroup = Goodgroup::model()->getRowById($Good->goodgroup_id);
			if (empty($Goodgroup)) {
				$goodgroup_name = 'No Group';
			} else {
				$goodgroup_name = $Goodgroup->name;
			}
			if ($first_row or $current_goodgroup_name != $goodgroup_name) {
				if (!$first_row) $data_array[$current_goodgroup_name] = $group_text;
				$group_text = '';
				$current_goodgroup_name = $goodgroup_name;
				$row_number++;
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('B' . $row_number, $current_goodgroup_name);
				$objPHPExcel->getActiveSheet()->getStyle('B' . $row_number)->getFont()->setBold(true);
				$row_number++;
			}
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A' . $row_number, $Good->name)
				->setCellValue('B' . $row_number, $Good->price) // '#,##0.00'
				->setCellValue('C' . $row_number, $Good->discount);

			$objPHPExcel->getActiveSheet()->getStyle('B' . $row_number)->getNumberFormat()->setFormatCode('#,##0.00');
			$objPHPExcel->getActiveSheet()->getStyle('C' . $row_number)->getNumberFormat()->setFormatCode('#,##0.0');

			$styleArray = array('font' => array('bold' => true, 'color' => array('rgb' => 'FF0000'), 'size' => 12, 'name' => 'Verdana'));
			$objPHPExcel->getActiveSheet()->getStyle('C' . $row_number)->applyFromArray($styleArray);

			if ($first_row) {
				$first_row = false;
			}
			$row_number++;
		}

		if (!empty($this->app_config['attach_site_info_file']) and file_exists($this->app_config['attach_site_info_file'])) {
			$objDrawingPType = new PHPExcel_Worksheet_Drawing();
			$objDrawingPType->setWorksheet($objPHPExcel->getActiveSheet());
			$objDrawingPType->setName("Pareto By Type");
			$objDrawingPType->setPath($this->app_config['attach_site_info_file']);
			$objDrawingPType->setCoordinates('B' . ($row_number + 1));
			$objDrawingPType->setOffsetX(1);
			$objDrawingPType->setOffsetY(5);
		}
//

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);

// Redirect output to a clientâ€™s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="goods_list.xls"');
		header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
		Yii::app()->end();
	} // writing_goods_into_excel

	public function actionRestful_with_goods()
	{
		$this->render('restful_with_goods');
	}


	public function actionAdd_goods_to_orders()
	{ // delete from jsn_order where order_id > 10
		$MaxGoodsId = 21;
		$MaxDepartmentsId = 8;
		$MaxClientsId = 21;
		$status_values = array('D', 'C', 'W', 'F');
		$MaxOrdersRows = 1000;
		for ($I = 1; $I <= $MaxOrdersRows; $I++) {
			$NewOrder = new Order();

			$NewOrder->good_id = rand(1, $MaxGoodsId);
			$NewOrder->department_id = rand(1, $MaxDepartmentsId);
			$NewOrder->client_id = rand(1, $MaxClientsId);

			$status_i = rand(0, count($status_values) - 1);
			$status_value = 'D';
			if (!empty($status_values[$status_i])) {
				$status_value = $status_values[$status_i];
			}
			$NewOrder->status = $status_value;
			$NewOrder->discount = rand(0, 99) / 20;
			$NewOrder->info = 'Some info for row ' . $I;
			$NewOrder->order_price = rand(0, 99999) / 100;
			$NewOrder->order_qty = rand(0, 99);
			$day= rand(1, 30);
			$NewOrder->created_at =  strftime( $this->app_config["DateTimeMySqlFormat"], mktime(null,null,null, strftime("%m"),$day, strftime("%Y") ) );
			$NewOrder->save(false);
		}
		echo $MaxOrdersRows .' orders added';
	}

	public function actionGoods_export_into_csv()
	{
		$this->layout = '//layouts/plain';
		foreach (Yii::app()->log->routes as $route) {
			$route->enabled = false;
		}
		$GoodsList = Good::model()->getGoodsList(Appfuncs::$LIST_OUTPUT_FORMAT_LIST, '', array(), 'goodgroup_id asc, name asc');

		$dst_filename = 'registers-upto--' . strftime("%Y-%m-%d-%H-%M-%S") . ".csv";
		CsvExport::export(
			$GoodsList, // a CActiveRecord array OR any CModel array
			array('good_id' => array(),
				'name' => array(),
				'price' => array('format' => 'money'),
				'discount' => array('format' => 'decimal', 'decimals' => 1),
			),
			true, // boolPrintRows
			$dst_filename
		);
	}



	public function actionSortable_droppable()
	{
		$this->layout = '//layouts/plain';

		$cs = Yii::app()->clientScript;
		$cs->registerScriptFile( "http://rniemeyer.github.io/KnockMeOut/Scripts/jquery.tmpl.js", CClientScript::POS_HEAD);

		$GoodgroupsList=	Goodgroup::model()->getGoodgroupsList( Appfuncs::$LIST_OUTPUT_FORMAT_LIST, '', array(), 'ordering', 'asc' );
		$goodgroups_list= array();
		foreach( $GoodgroupsList as $Goodgroup ) {
			$goodgroups_list[$Goodgroup->goodgroup_id]= $Goodgroup->name;
		}
		//Appfuncs::deb($GoodsList, '$GoodsList::');
		/* 	$GoodsList= Good::model()->getGoodsList( Appfuncs::$LIST_OUTPUT_FORMAT_LIST, '', array(), 'goodgroup_id asc, name asc' );
	//Appfuncs::deb($GoodsList, '$GoodsList::');
	$current_goodgroup_name= ''; $first_row= true;
	$data_array= array();  $group_text= '';
	foreach( $GoodsList as $Good ) {
		$Goodgroup=	Goodgroup::model()->getRowById( $Good->goodgroup_id );
 */

		$this->render( 'sortable_droppable', array( 'goodgroups_list'=>$goodgroups_list ) );

		/*$this->layout = '//layouts/plain';
		foreach (Yii::app()->log->routes as $route) {
			$route->enabled = false;
		}
		$GoodsList = Good::model()->getGoodsList(Appfuncs::$LIST_OUTPUT_FORMAT_LIST, '', array(), 'goodgroup_id asc, name asc');

		$dst_filename = 'registers-upto--' . strftime("%Y-%m-%d-%H-%M-%S") . ".csv";
		CsvExport::export(
			$GoodsList, // a CActiveRecord array OR any CModel array
			array('good_id' => array(),
				'name' => array(),
				'price' => array('format' => 'money'),
				'discount' => array('format' => 'decimal', 'decimals' => 1),
			),
			true, // boolPrintRows
			$dst_filename
		); */
	}

}

