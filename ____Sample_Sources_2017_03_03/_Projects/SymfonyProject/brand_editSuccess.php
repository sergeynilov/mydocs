  <script type="text/javascript" language="JavaScript">
  <!--
  function onSubmit() {
    var theForm = document.getElementById("form_brand_edit");
    theForm.submit();
  }


  //-->
  </script>

<form action="<?php echo url_for('@admin_brand_edit?id='.$brand_id.'&page='.$page.'&is_selection='.$is_selection.'&filter_name='.$filter_name.'&filter_inactive='.$filter_inactive.'&sorting='.$sorting ) ?>" id="form_brand_edit" method="POST" enctype="multipart/form-data" >
<input type="hidden" value="<?php echo $sf_request->getParameter("id") ?>" id="id" >
<?php echo $form['_csrf_token']->render()?>
<div class="Div_Editor_Conteiner">
  <br><h2 class="EditorTitle"><?php echo __(!empty($brand_id)?'Edit':'Add').' '.__('Brand') ?></h2>
    <span class="error" style="list-style: none;"><?php echo $form->renderGlobalErrors() ?></span>

    <table class="Table_Editor_Conteiner">

      <tr>
        <td class="left">
          <?php echo strip_tags( $form[BrandPeer::ID]->renderLabel() ) ?>&nbsp;:
        </td>
        <td class="right">
         	<?php echo $form[ BrandPeer::ID ]->render(); ?>
  	      <span class="error">
  	        <?php echo ( $sf_request->isMethod('post') ? strip_tags( $form[ BrandPeer::ID ]->renderError() )  :"" ) ?>
  	      </span>
        </td>
      </tr>
    
      <tr>
        <td class="left">
          <?php echo strip_tags( $form[BrandPeer::NAME]->renderLabel() ) ?>&nbsp;:
        </td>
        <td class="right">
         	<?php echo $form[ BrandPeer::NAME ]->render(); ?>
  	      <span class="error">
  	        <?php echo ( $sf_request->isMethod('post') ? strip_tags( $form[ BrandPeer::NAME ]->renderError() )  :"" ) ?>
  	      </span>
        </td>
      </tr>
      
      <tr>
        <td class="left">
          <?php echo strip_tags( $form[BrandPeer::LOGOFILENAME]->renderLabel() ) ?>&nbsp;:
        </td>
        <td class="right">
         	<?php echo $form[ BrandPeer::LOGOFILENAME ]->render(); ?>
  	      <span class="error">
  	        <?php echo ( $sf_request->isMethod('post') ? strip_tags( $form[BrandPeer::LOGOFILENAME]->renderError() )  :"" ) ?>
  	      </span><br>
  	      <?php
            $images_brands_dir= sfConfig::get('app_application_images_brands_dir' );
            $lBrand= BrandPeer::retrieveByPK( $sf_request->getParameter('id') );
            if ( !empty($lBrand) ) {
              $ThumbnailSmallFilename= $lBrand->getSmallLogoFilename(false);
              if ( file_exists( sfConfig::get('sf_web_dir') . DIRECTORY_SEPARATOR . 'images'.DIRECTORY_SEPARATOR.$ThumbnailSmallFilename ) ) {
                $brand_tmbn_width_small= (int)sfConfig::get('app_application_brand_tmbn_width_small' );
                $brand_tmbn_height_small= (int)sfConfig::get('app_application_brand_tmbn_height_small' );
                $FileName= DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$images_brands_dir . DIRECTORY_SEPARATOR . $lBrand->getLogoFilename();            
                echo '<table border="0"><tr><td>';    
                echo $ThumbnailSmallFilename.'('.$brand_tmbn_width_small.'x'.$brand_tmbn_height_small.')'.':</td></tr>';
                          
                $ImageTag= image_tag(  $ThumbnailSmallFilename, array( 'border'=>'none' , 'alt'=>$ThumbnailSmallFilename )  );                        
                $NewSize= Util::GetImageShowSize( $FileName, $brand_tmbn_width_small, $brand_tmbn_height_small );
                echo '</td><td align="left" valign="top" >'.'<a class="thickbox" href="'.url_for( "@admin_showimage?type=brand&image=" . $FileName ) . '&alt='.$lBrand->getLogoFilename().
                '&height='.( ($brand_tmbn_width_small>$NewSize['OriginalHeight']?$brand_tmbn_width_small:$NewSize['OriginalHeight']) + 10 ).
                '&width='.( ($brand_tmbn_height_small>$NewSize['OriginalWidth']?$brand_tmbn_height_small:$NewSize['OriginalWidth']) + 10 ).
                '" title="'.$lBrand->getLogoFilename().'">'.$ImageTag.'</a> ';
                echo '</td></tr></table>';    
              }
            }
  	      ?>
        </td>
      </tr>      
      
      <tr>
        <td class="left">
          <?php echo strip_tags( $form[BrandPeer::INACTIVE]->renderLabel() ) ?>&nbsp;:
        </td>
        <td class="right">
         	<?php echo $form[ BrandPeer::INACTIVE ]->render(); ?>
  	      <span class="error">
  	        <?php echo ( $sf_request->isMethod('post') ? strip_tags( $form[ BrandPeer::INACTIVE ]->renderError() )  :"" ) ?>
  	      </span>
        </td>
      </tr>

      <tr>
        <td class="left">
          <?php echo strip_tags( $form[BrandPeer::SHORT_DESCRIPTION]->renderLabel() ) ?>&nbsp;:
        </td>
        <td class="right">
         	<?php echo $form[ BrandPeer::SHORT_DESCRIPTION ]->render(); ?>
  	      <span class="error">
  	        <?php echo ( $sf_request->isMethod('post') ? strip_tags( $form[ BrandPeer::SHORT_DESCRIPTION ]->renderError() )  :"" ) ?>
  	      </span>
        </td>
      </tr>
      
      <tr>
        <td class="left">
          <?php echo strip_tags( $form[BrandPeer::DESCRIPTION]->renderLabel() ) ?>&nbsp;:
        </td>
        <td class="right">
         	<?php echo $form[ BrandPeer::DESCRIPTION ]->render(); ?>
  	      <span class="error">
  	        <?php echo ( $sf_request->isMethod('post') ? strip_tags( $form[ BrandPeer::DESCRIPTION ]->renderError() )  :"" ) ?>
  	      </span>
        </td>
      </tr>
      
      <tr>
        <td class="left">
          <?php echo strip_tags( $form[BrandPeer::META_TITLE]->renderLabel() ) ?>&nbsp;:
        </td>
        <td class="right">
         	<?php echo $form[ BrandPeer::META_TITLE ]->render(); ?>
  	      <span class="error">
  	        <?php echo ( $sf_request->isMethod('post') ? strip_tags( $form[ BrandPeer::META_TITLE ]->renderError() )  :"" ) ?>
  	      </span>
        </td>
      </tr>

      <tr>
        <td class="left">
          <?php echo strip_tags( $form[BrandPeer::META_KEYWORDS]->renderLabel() ) ?>&nbsp;:
        </td>
        <td class="right">
         	<?php echo $form[ BrandPeer::META_KEYWORDS ]->render(); ?>
  	      <span class="error">
  	        <?php echo ( $sf_request->isMethod('post') ? strip_tags( $form[ BrandPeer::META_KEYWORDS ]->renderError() )  :"" ) ?>
  	      </span>
        </td>
      </tr>

      <tr>
        <td class="left">
          <?php echo strip_tags( $form[BrandPeer::FEATURED_BRAND]->renderLabel() ) ?>&nbsp;:
        </td>
        <td class="right">
         	<?php echo $form[ BrandPeer::FEATURED_BRAND ]->render(); ?>
  	      <span class="error">
  	        <?php echo ( $sf_request->isMethod('post') ? strip_tags( $form[ BrandPeer::FEATURED_BRAND ]->renderError() )  :"" ) ?>
  	      </span>
        </td>
      </tr>
      
      <tr>
        <td class="left">
          <?php echo strip_tags( $form[BrandPeer::CREATED_AT]->renderLabel() ) ?>&nbsp;:
        </td>
        <td class="right">
         	<?php echo $form[ BrandPeer::CREATED_AT ]->render(); ?>
  	      <span class="error">
  	        <?php echo ( $sf_request->isMethod('post') ? strip_tags( $form[ BrandPeer::CREATED_AT ]->renderError() )  :"" ) ?>
  	      </span>
        </td>
      </tr>


      <tr>
        <td class="left">
          <?php echo strip_tags( $form[BrandPeer::UPDATED_AT]->renderLabel() ) ?>&nbsp;:
        </td>
        <td class="right">
         	<?php echo $form[ BrandPeer::UPDATED_AT ]->render(); ?>
  	      <span class="error">
  	        <?php echo ( $sf_request->isMethod('post') ? strip_tags( $form[ BrandPeer::UPDATED_AT ]->renderError() )  :"" ) ?>
  	      </span>
        </td>
      </tr>
      
      
    <tr>
    <td></td>
      <td  style="padding-left:150px;">
        <?php if( empty($brand_id) ) : ?>
          <input type="button" id="action_submit" value="Add" onclick='javascript:onSubmit()' />
        <?php else: ?>
        <input type="button" id="action_submit" value="Update" onclick='javascript:onSubmit()' />
      <?php endif; ?>
      <input type="button" id="action_submit" value="Cancel" onclick='javascript:document.location="<?php echo url_for('@admin_brands?page='.$page.'&sorting='.$sorting.'&is_selection='.$is_selection.'&filter_name='.$filter_name.'&filter_inactive='.$filter_inactive) ?>"' />
      </td>
    </tr>
  </table>
</div>
</form>