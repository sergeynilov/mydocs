<?php
class BrandEditorForm extends sfForm
{
  public static $Brand;
  /**
	 * Configure form fields
	 */
  public function configure()
  {
    // form fields :

    $this->setWidgets(array(
    BrandPeer::ID => new sfWidgetFormInput( array(), array('readonly'=>'readonly', 'class'=>'readonly_field', 'onfocus'=>'javascript:document.getElementById("brand_brand.NAME").focus()' ) ),
    BrandPeer::NAME => new sfWidgetFormInput( array(),array('size'=>30,'maxlength'=>100, 'class'=>'text' ) ),
    BrandPeer::LOGOFILENAME => new sfWidgetFormInputFile( array(), array( 'size'=>30,'maxlength'=>100, 'class'=>'text' ) ),
    BrandPeer::CREATED_AT => new sfWidgetFormInput( array(), array( 'readonly'=>'readonly', 'class'=>'readonly_field' ) ),
    BrandPeer::UPDATED_AT => new sfWidgetFormInput( array(), array( 'readonly'=>'readonly', 'class'=>'readonly_field' ) ),
    
    BrandPeer::INACTIVE => new sfWidgetFormSelect( array('choices'=>BrandPeer::$IsInactiveArray ), array('class'=>'text') ),
    BrandPeer::SHORT_DESCRIPTION => new sfWidgetFormTextarea( array(),array('rows'=>3,'cols'=>80, 'class'=>'text') ),
    BrandPeer::DESCRIPTION => new sfWidgetFormTextareaTinyMCE( array( 'width'  => 550, 'height' => 350), array('class'=>'text') ) ,
    BrandPeer::META_TITLE => new sfWidgetFormInput( array(),array('size'=>50,'maxlength'=>100, 'class'=>'text') ),
    BrandPeer::META_KEYWORDS => new sfWidgetFormInput( array(),array('size'=>80,'maxlength'=>1000, 'class'=>'text') ),       
    BrandPeer::FEATURED_BRAND => new sfWidgetFormSelect( array('choices'=>BrandPeer::$IsFeaturedBrandArray ), array('class'=>'text') ),
    ));

    // form labels
    $this->widgetSchema->setLabel( BrandPeer::ID, 'Brand ID' );
    $this->widgetSchema->setLabel( BrandPeer::NAME, 'Brand name' );
    $this->widgetSchema->setLabel( BrandPeer::LOGOFILENAME, 'logo Filename' );
    $this->widgetSchema->setLabel( BrandPeer::CREATED_AT, 'Created At' );
    $this->widgetSchema->setLabel( BrandPeer::UPDATED_AT, 'Updated At' );
    
    $this->widgetSchema->setLabel( BrandPeer::INACTIVE, 'Inactive' );
    $this->widgetSchema->setLabel( BrandPeer::SHORT_DESCRIPTION, 'Short Description' );
    $this->widgetSchema->setLabel( BrandPeer::DESCRIPTION, 'Description' );
    $this->widgetSchema->setLabel( BrandPeer::META_TITLE, 'Meta Title' );
    $this->widgetSchema->setLabel( BrandPeer::META_KEYWORDS, 'Meta Keywords' );
    $this->widgetSchema->setLabel( BrandPeer::FEATURED_BRAND, 'Featured Brand' );
    $this->widgetSchema->setNameFormat('brand[%s]');

    if(self::$Brand instanceof Brand )
    {
      $this->setDefault( BrandPeer::ID, self::$Brand->getId() );
      $this->setDefault( BrandPeer::NAME, self::$Brand->getName() );
      $this->setDefault( BrandPeer::LOGOFILENAME, self::$Brand->getLogoFilename() );
      $this->setDefault( BrandPeer::CREATED_AT, self::$Brand->getCreatedAt( sfConfig::get('app_application_date_time_format' ) ) );
      $this->setDefault( BrandPeer::UPDATED_AT, self::$Brand->getUpdatedAt( sfConfig::get('app_application_date_time_format' ) ) );
      $this->setDefault( BrandPeer::INACTIVE, self::$Brand->getInactive() );
      $this->setDefault( BrandPeer::SHORT_DESCRIPTION, self::$Brand->getShortDescription() );
      $this->setDefault( BrandPeer::DESCRIPTION, self::$Brand->getDescription() );
      $this->setDefault( BrandPeer::META_TITLE, self::$Brand->getMetaTitle() );
      $this->setDefault( BrandPeer::META_KEYWORDS, self::$Brand->getMetaKeywords() );
      $this->setDefault( BrandPeer::FEATURED_BRAND, self::$Brand->getFeaturedBrand() );
    }
    // validators :
    $this->setValidators(array(
    BrandPeer::ID => new sfValidatorString ( array( 'required' => false), array( ) ), 
    
    BrandPeer::NAME => new sfValidatorAnd(array(
    new sfValidatorString(array(), array( ) ),
    new sfValidatorRegex(
    array('pattern' => "/\//", 'must_match' => false),
    array ('invalid' => "Name can not have slash(\"/\") character.")
    ),
    ),
    array('required'   => true),
    array('required'   => 'Name can not be empty.')
    ),
     
    
    BrandPeer::LOGOFILENAME => new sfValidatorFile( 
      array('required' => false,  'max_size' => sfConfig::get('app_application_max_image_upload_size') ) ),
    BrandPeer::CREATED_AT  => new sfValidatorString ( array( 'required' => false), array( ) ), 
    BrandPeer::UPDATED_AT  => new sfValidatorString ( array( 'required' => false), array( ) ), 

    BrandPeer::INACTIVE => new sfValidatorChoice(array('choices' => array_keys(BrandPeer::$IsInactiveArray),'required' => true)),
    BrandPeer::DESCRIPTION => new sfValidatorString (  array( 'required' => true), array('required' => "Description can not be empty." )  ),
    BrandPeer::SHORT_DESCRIPTION => new sfValidatorString ( array( 'required' => false ) ),
    BrandPeer::META_TITLE => new sfValidatorString ( array( 'required' => false ) ),
    BrandPeer::META_KEYWORDS => new sfValidatorString ( array( 'required' => false ) ),    
    BrandPeer::FEATURED_BRAND => new sfValidatorChoice(array('choices' => array_keys(BrandPeer::$IsFeaturedBrandArray),'required' => true)),    
    ) );

    // post validators :
    $this->validatorSchema->setPostValidator(
    new sfValidatorCallback(array('callback' => array($this, 'checkUniqueName')))  );

    // set render form schemas :
    $this->widgetSchema->setFormFormatterName('table');
  }



  /**
         * Validate Unique Title
         *
         * @param unknown_type $pValidator
         * @param unknown_type $pValues
         * @return unknown
         */
  public function checkUniqueName ($pValidator, $pValues)
  {
    $lError = array();
    $lHelpers = array();
    $Name= $pValues[BrandPeer::NAME];
    $Id= sfContext::getInstance()->getRequest()->getParameter('id',-1);
    $lSimilarBrand= BrandPeer::getSimilarBrand( $Name, $Id, false );
    if( !empty( $lSimilarBrand ) ){
      $lHelpers[BrandPeer::NAME] = 'Already exists Brand with this name.<br />';
      $lError [] = 'Already exists Brand with this name.';
    }
    if(!empty($lError))
    {
      $this->getWidgetSchema()->setHelps($lHelpers);
      $lErrorMessage = '';
      foreach ($lError as $lErr)
      {
        $lErrorMessage .= $lErr.'<br />';
      }
      throw new sfValidatorError($pValidator, $lErrorMessage);
    }
    return $pValues;
  }


}

?>