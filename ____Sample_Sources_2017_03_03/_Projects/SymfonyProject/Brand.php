<?php

require 'lib/model/om/BaseBrand.php';


/**
 * Skeleton subclass for representing a row from the 'brand' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    lib.model
 */
class Brand extends BaseBrand {

	/**
	 * Initializes internal state of Brand object.
	 * @see        parent::__construct()
	 */
	public function __construct()
	{
		// Make sure that parent constructor is always invoked, since that
		// is where any default values for this object are set.
		parent::__construct();
	}

	public function __toString()
	{
		return $this->getName();
	}
	
	public function getInactiveAsText() {
	  if ( $this->getInactive() ) return BrandPeer::$IsInactiveArray[1];
	  return BrandPeer::$IsInactiveArray[0];
	}

	public function getSmallLogoFilename($ShowImagesPath= true) {
    $images_brands_dir= sfConfig::get('app_application_images_brands_dir' );
    $Ext= strtolower( Util::GetFileNameExt($this->getLogoFilename()) );
    if ( $ShowImagesPath ) {
      return 'images/' . $images_brands_dir . DIRECTORY_SEPARATOR . 'brand_'.$this->getId().'_small.'.$Ext;
    }
    return $images_brands_dir . DIRECTORY_SEPARATOR . 'brand_'.$this->getId().'_small.'.$Ext;
	}
	
} // Brand
