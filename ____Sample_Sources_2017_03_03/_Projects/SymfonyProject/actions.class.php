<?php
 ...

  ////////////// BRANDS BEGIN ////////////////////////
  /**
  * Show list of brands in Administration Zone
  *
  * @param sfRequest $request A request object
  */
  public function executeBrands($request)
  {
    try
    {
      $this->form = new BrandFilterForm();
      $data= $request->getParameter('brand');

      $this->filter_name= '';
      $this->filter_inactive= '';

      $this->is_selection= $request->getParameter('is_selection');
      if ( $this->is_selection ) {
        $this->setLayout('layout_popup');
      }
      $response = $this->context->getResponse();
      if ( !$this->VerifyAdminAccess($request) ) return $this->redirect('@sf_guard_signin');

      if ($request->isMethod('post'))
      {
        if ( strlen($data['filter_name'])>0 ) {
          $this->filter_name= $data['filter_name'];
        }
        if ( strlen($data['filter_inactive'])> 0 ) {
          $this->filter_inactive= $data['filter_inactive'];
        }
      } else {
        $this->filter_name= $request->getParameter('filter_name');
        $this->filter_inactive= $request->getParameter('filter_inactive');

        $data= array(  'filter_name' => $this->filter_name, 'filter_inactive' => $this->filter_inactive );
      }
      if ( $this->filter_name=='-' ) $this->filter_name='';
      if ( $this->filter_inactive =='-' ) $this->filter_inactive='';
      $this->form->bind($data);

      $this->page= $request->getParameter('page',1);
      $this->sorting= $request->getParameter('sorting');
      $this->BrandsPager= BrandPeer::getBrands( $this->page, true, $this->filter_name, $this->filter_inactive, $this->sorting  );
    }
    catch (Exception $lException)
    {
      $this->logMessage($lException->getMessage(), 'err');
      return sfView::ERROR;
    }
  }


  /**
  * Show editor of Brand by its ID in Administration Zone
  *
  * @param sfRequest $request A request object
  */
  public function executeBrand_edit($request)
  {
    $response = $this->context->getResponse();
    $response->addJavascript( '/js/tiny_mce/tiny_mce.js' );
    $response->addJavascript( '/js/jquery/thickbox-compressed.js', 'last' );
    try
    {
      if ( !$this->VerifyAdminAccess($request) ) return $this->redirect('@sf_guard_signin');
      $this->brand_id= $request->getParameter('id');
      $this->sorting= $request->getParameter('sorting');
      $this->page= $request->getParameter('page',1);
      $this->is_selection= $request->getParameter('is_selection');
      $this->filter_name= $request->getParameter('filter_name');
      $this->filter_inactive= $request->getParameter('filter_inactive');

      if ( !empty($this->brand_id) ) {
        $this->Brand= BrandPeer::retrieveByPK($this->brand_id);
      }
      if ( !empty($this->brand_id) ) {
        BrandEditorForm::$Brand = $this->Brand;
      }
      $this->form = new BrandEditorForm();
      if ($request->isMethod('post'))
      {
        $data= $request->getParameter('brand');
        $this->form->bind($data, $request->getFiles('brand'));
        if ( $this->form->isValid() )
        {
          if ( empty($this->Brand) ) {
            $this->Brand= new Brand();
          }
          $this->Brand->setName( $data[BrandPeer::NAME] );

          $this->Brand->setInactive( $data[BrandPeer::INACTIVE] );
          $this->Brand->setShortDescription( $data[BrandPeer::SHORT_DESCRIPTION] );
          $this->Brand->setDescription( $data[BrandPeer::DESCRIPTION] );
          $this->Brand->setMetaTitle( $data[BrandPeer::META_TITLE] );
          $this->Brand->setMetaKeywords( $data[BrandPeer::META_KEYWORDS] );
          $this->Brand->setFeaturedBrand( $data[BrandPeer::FEATURED_BRAND] );

          $this->Brand->save();
          if ( !empty($_FILES['brand']['tmp_name'][BrandPeer::LOGOFILENAME]) ) {
            $TempFileName= $_FILES['brand']['tmp_name'][BrandPeer::LOGOFILENAME];
            $OrigFileName= $_FILES['brand']['name'][BrandPeer::LOGOFILENAME];
            $brand_id= $this->Brand->getId();
            $Ext= strtolower( Util::GetFileNameExt($OrigFileName) );
            $images_brands_dir= sfConfig::get('app_application_images_brands_dir' );
            $DestFilename= 'images/' . $images_brands_dir . DIRECTORY_SEPARATOR . 'brand_'.$brand_id.'.'.$Ext;
            rename( $TempFileName, $DestFilename );
            chmod( $DestFilename, 0777 );

            $this->Brand->setLogoFileName('brand_'.$brand_id.'.'.$Ext);
            $this->Brand->save();
            $ImageMagickConvertPath= sfConfig::get('app_application_ImageMagickConvertPath' );
            $ImageMagickResizeCommand= sfConfig::get('app_application_ImageMagickResizeCommand' );
            $ImageMagickFilterCommand= sfConfig::get('app_application_ImageMagickFilterCommand' );
            $brand_tmbn_width_small= sfConfig::get('app_application_brand_tmbn_width_small' );
            $brand_tmbn_height_small= sfConfig::get('app_application_brand_tmbn_height_small' );
            $ThumbnailSmallFilename= $this->Brand->getSmallLogoFilename();
            $ConvertCommand= $ImageMagickConvertPath . $ImageMagickResizeCommand . $brand_tmbn_width_small . 'x' . $brand_tmbn_height_small . $ImageMagickFilterCommand . $DestFilename.' '.$ThumbnailSmallFilename;
            $Ret= EXEC( $ConvertCommand, $retval);
            chmod ( $ThumbnailSmallFilename, 0644 );

          }
          $this->getUser()->setFlash('user_message', "Brand'".$this->Brand->getName()."' was ".( empty($this->brand_id)?"added.":"edited." ) );
          $this->redirect( '@admin_brands?page=' . $this->page.'&is_selection='.$this->is_selection.'&filter_name='.$this->filter_name.'&filter_inactive='.$this->filter_inactive.'&sorting='.$this->sorting );
        }
      }

    }
    catch (Exception $lException)
    {
      $this->logMessage($lException->getMessage(), 'err');
      return sfView::ERROR;
    }
  }


  public function executeBrand_products_count($request)
  {
    try
    {
      $response = $this->context->getResponse();
      $this->brand_id= $request->getParameter('id');
      $Brand= BrandPeer::retrieveByPK($this->brand_id);
      $RowsCount= '';
      if ( !empty($Brand) ) $RowsCount= $Brand->countProducts();
      echo json_encode(   array ( 'ErrorMessage'=>'', 'ErrorCode'=>0, 'RowsCount'=>$RowsCount, 'brand_id'=> $this->brand_id )   );
      return sfView::NONE;
    }
    catch (Exception $lException)
    {
      $this->logMessage($lException->getMessage(), 'err');
      return sfView::ERROR;
    }
  }

  /**
  *  delete Brand by its ID in Administration Zone
  */
  public function executeBrand_delete($request)
  {
    $this->is_selection= $request->getParameter('is_selection');
    if ( !$this->VerifyAdminAccess($request) ) return $this->redirect('@sf_guard_signin');
    $con = Propel::getConnection( BrandPeer::DATABASE_NAME, Propel::CONNECTION_WRITE );
    try
    {
      $this->brand_id= $request->getParameter('id');
      $this->page= $request->getParameter('page',1);
      $this->filter_name= $request->getParameter('filter_name');
      $this->filter_inactive= $request->getParameter('filter_inactive');
      $this->FilterType= $request->getParameter('filter');
      $this->sorting= $request->getParameter('sorting');
      $Brand= BrandPeer::retrieveByPK($this->brand_id);
      if ( !empty($Brand) ) {
        $con->beginTransaction();
        $DeletedBrandName= $Brand->getName();
        $Brand->delete();
        $con->commit();
        $sf_web_dir= sfConfig::get('sf_web_dir');
        $images_brands_dir= sfConfig::get('app_application_images_brands_dir' );
        $FileName= $sf_web_dir . DIRECTORY_SEPARATOR . 'images' .DIRECTORY_SEPARATOR . $images_brands_dir .
        DIRECTORY_SEPARATOR . $Brand->getLogoFilename();
        if ( file_exists($FileName) ) unlink($FileName);

        $ThumbnailSmallFilename= $Brand->getSmallLogoFilename(true);
        $FileName= $sf_web_dir . DIRECTORY_SEPARATOR . $ThumbnailSmallFilename;
        if ( file_exists($FileName) ) { Util::deb(':UNLINK:');  unlink($FileName);        }

        $this->getUser()->setFlash('user_message', "Brand '".$DeletedBrandName."' was deleted !" );
        $this->redirect( '@admin_brands?page=' . $this->page.'&is_selection='.$this->is_selection.'&filter_name='.$this->filter_name.'&filter_inactive='.$this->filter_inactive.'&sorting='.$this->sorting );
      }
    }
    catch (Exception $lException)
    {
      $con->rollBack();
      $this->logMessage($lException->getMessage(), 'err');
      return sfView::ERROR;
    }
  }
  ////////////// BRANDS END ////////////////////////

...
?>