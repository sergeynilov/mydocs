
<?php

require 'lib/model/om/BaseBrandPeer.php';


/**
 * Skeleton subclass for performing query and update operations on the 'brand' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    lib.model
 */
class BrandPeer extends BaseBrandPeer {
  public static $IsInactiveArray= array( 0=>'Active', 1=>'Inactive' );
  public static $IsFeaturedBrandArray= array(  0=>'Not Featured Brand', 1=>'Featured Brand' );

  public static function getBrands( $page=1, $ReturnPager=true, $filter_name='', $filter_inactive= '', $Sorting='NAME' ) {
    $c = new Criteria();
    if ( !empty($filter_name) and $filter_name!= '-' ) {
      $c->add( BrandPeer::NAME , '%'.$filter_name.'%', Criteria::LIKE );
    }
    if ( strlen($filter_inactive) > 0 and $filter_inactive!= '-' ) {
      $c->add( BrandPeer::INACTIVE , $filter_inactive );
    }
    if ( $Sorting=='CREATED_AT' or empty($Sorting) ) {
      $c->addDescendingOrderByColumn(BrandPeer::CREATED_AT);
      $c->addDescendingOrderByColumn(BrandPeer::ID);
    }
    if ( $Sorting=='NAME' ) {
      $c->addAscendingOrderByColumn(BrandPeer::NAME);
    }
    if ( $Sorting=='INACTIVE' ) {
      $c->addAscendingOrderByColumn(BrandPeer::INACTIVE);
    }
    if ( !$ReturnPager ) {
      return BrandPeer::doSelect( $c );
    }
    $pager = new sfPropelPager( 'Brand', (int)sfConfig::get('app_application_rows_in_pager' ) );
    $pager->setPage( $page );
    $pager->setCriteria($c);
    $pager->init();
    return $pager;
  }

  public static function getSimilarBrand($BrandName, $pBrandId, $IsEqual=true, $con = null)
  {
    $c = new Criteria();
    $c->add( BrandPeer::NAME, $BrandName );
    if ( $IsEqual ) {
      $c->add( BrandPeer::ID, $pBrandId );
    } else {
      $c->add( BrandPeer::ID, $pBrandId, Criteria::NOT_EQUAL );
    }
    if($lResult = BrandPeer::doSelect( $c, $con ) )
    {
      return $lResult[0];
    }
    return '';
  }

  public static function getSelectionList( $Code, $Name, $NewCode='', $NewCodeText='' ) {
    $List= BrandPeer::getBrands( 1, false );
    $ResArray= array();
    if ( !empty($Code) or !empty($Name) ) {
      $ResArray[$Code]= $Name;
    }
    foreach( $List as $lBrand ) {
      $ResArray[ $lBrand->getId() ]= $lBrand->getName();
    }
    if ( !empty($NewCode) and !empty($NewCodeText) ) {
      $ResArray[ $NewCode ]= $NewCodeText;      
    }
    return $ResArray;
  }
  
} // BrandPeer
