<?php

class InventoryItemsImport
{
    private $InventoryItemsImportCVSFile = '';
    private $ColumnsArray = array();
    private $InventaryItemsSkuArray = array();

    //calculate numbers of operations done to show info text
    private $ItemsDeletedAsDisabled = 0;
    private $ItemsSkippedAsNonExisting = 0;
    private $ItemsAdded = 0;
    private $ItemsModified = 0;
    private $ItemsSkippedAsExising = 0;
    private $ItemsDeletedAsNoneExising = 0;
    private $ImagesAdded = 0;
    private $ImageThumbnailsAdded = 0;
    private $InventoryCategoriesDeletedAsEmpty = 0;
    private $ItemsSkippedAsWithEmptyInventoryCategory = 0;


    public function setInventoryItemsImportCVSFile($pInventoryItemsImportCVSFile)
    {
        $this->InventoryItemsImportCVSFile = $pInventoryItemsImportCVSFile;
    }

    private function ReadCSVData() // read all lines of csv file
    {
        $CsvFileName = $this->InventoryItemsImportCVSFile;
        $ResArray = array();
        $reader = new sfCsvReader($CsvFileName, ',');
        $reader->open();

        $data_keys = $reader->read();

        $I = 0;
        while ($data = $reader->read()) {
            if (count($data) == 1) {
                continue;
            }
            $L = count($data);

            $data_mixed = array_combine($data_keys, $data);

            try {
                $this->ImportCvsData($I, $data_mixed);  // work and save next csv row
            } catch (Exception $x) {
                echo '<pre> Error ' . print_r($x, true) . ' On Row data::' . print_r($data, true) . '</pre><br>';
            }
            $I++;
        }

        $reader->close();
        return $ResArray;
    } // private function ReadCSVData() {  $ParentSKU, $ProductId, $isPageSetsListByDirs

    private function DeleteNonExistingInventoryItems()
    { // Deleteall Non Existing Inventory Items from db(which ar not in current csv file )
        $InventoryItemsList = InventoryItemPeer::getInventoryItems(1, false, false, '', '', '', '', false, '', '', '', '', '', '', '', '', 'SKU', '');
        foreach ($InventoryItemsList as $InventoryItem) {
            if (!in_array($InventoryItem->getSku(), $this->InventaryItemsSkuArray)) {
                $InventoryItem->delete();
                $this->ItemsDeletedAsNoneExising++;
            }
        }
        return true;
    }

    public function Run()
    {
        $CvsDataArray = $this->ReadCSVData();
        return true;
    } // public function Run() {


    private function ImportCvsData($CvsKey, $CvsDataRowValues) // work and save next csv row
    {

        $this->InventaryItemsSkuArray[] = trim($CvsDataRowValues['ItemCode']);
        $ItemCode = $CvsDataRowValues['ItemCode']; // get all fields of next csv row
        $Title = $CvsDataRowValues['UDF_IT_DESCRIPTION'];
        $Title = mb_convert_encoding($Title, 'UTF-8', 'Cp-1251');
        $Title = preg_replace("/®/", '(R)', $Title);
        $Title = preg_replace("/™/", '(TM)', $Title);
        $Brand = $CvsDataRowValues['UDF_BRAND'];
        $InventoryCategory = $CvsDataRowValues['ProductLine'];
        $DescriptionShort = $CvsDataRowValues['UDF_IT_DESCRIPTION'];
        $DescriptionShort = mb_convert_encoding($DescriptionShort, 'UTF-8', 'Cp-1251');
        $DescriptionShort = preg_replace("/®/", '(R)', $DescriptionShort);
        $DescriptionShort = preg_replace("/™/", '(TM)', $DescriptionShort);
        $DescriptionLong = $CvsDataRowValues['UDF_IT_LONG_DESCRIPTION'];
        $DescriptionLong = mb_convert_encoding($DescriptionLong, 'UTF-8', 'Cp-1251');
        $DescriptionLong = preg_replace("/®/", '(R)', $DescriptionLong);
        $DescriptionLong = preg_replace("/™/", '(TM)', $DescriptionLong);
        $Size = $CvsDataRowValues['UDF_SIZE'];
        if (empty($Size)) {
            $Size = $CvsDataRowValues['Category1'];
        }
        $Color = $CvsDataRowValues['UDF_COLOR'];
        if (empty($Color)) {
            $Color = $CvsDataRowValues['Category2'];
        }
        $Finish = $CvsDataRowValues['UDF_FINISH'];
        if (empty($Finish)) {
            $Finish = $CvsDataRowValues['Category3'];
        }
        $Options = $CvsDataRowValues['UDF_OPTIONS'];
        if (empty($Options)) {
            $Options = $CvsDataRowValues['Category4'];
        }
        $Gender = $CvsDataRowValues['UDF_IT_GENDER'];
        $Paperwork = $CvsDataRowValues['UDF_IT_PAPERWORK_REQUIRED'];
        $Sizing = $CvsDataRowValues['UDF_IT_SIZING_CHART'];
        $Customizable = $CvsDataRowValues['UDF_IT_CUSTOMIZABLE'];
        $Video = $CvsDataRowValues['UDF_IT_VIDEO'];
        $Documents = $CvsDataRowValues['UDF_IT_PRODUCT_DOCS'];
        $Hand = $CvsDataRowValues['UDF_IT_HAND'];
        $Matrix = $CvsDataRowValues['UDF_IT_MATRIX'];
        $RootItem = $CvsDataRowValues['UDF_IT_ROOTITEM'];
        $Features = $CvsDataRowValues['UDF_IT_FEATURES'];
        $UnitMeasure = $CvsDataRowValues['SalesUnitOfMeasure'];
        $QtyOnHand = $CvsDataRowValues['TotalQuantityOnHand'];
        $StdUnitPrice = $CvsDataRowValues['StandardUnitPrice'];
        $Msrp = $CvsDataRowValues['SuggestedRetailPrice'];
        $Clearance = $CvsDataRowValues['SalesPromotionCode'];
        $SaleStartDate = $CvsDataRowValues['SaleStartingDate'];
        $SaleEndDate = $CvsDataRowValues['SaleEndingDate'];
        $SaleMethod = $CvsDataRowValues['SaleMethod'];
        $SalePrice = $CvsDataRowValues['SalesPromotionPrice'];
        $SalePromoDiscount = $CvsDataRowValues['SalesPromotionDiscountPercent'];
        $TaxClass = $CvsDataRowValues['TaxClass'];
        $ShipWeight = $CvsDataRowValues['ShipWeight'];
        $SetupCharge = $CvsDataRowValues['SetupCharge'];
        $DateUpdated = $CvsDataRowValues['DateUpdated'];
        $TimeUpdated = $CvsDataRowValues['TimeUpdated'];
        $Enabled = $CvsDataRowValues['UDF_IT_ENABLED'];

        $AdditionalFreight = $CvsDataRowValues['UDF_ADDITIONAL_FREIGHT'];
        $FreeShipping = $CvsDataRowValues['UDF_FREE_SHIPPING'];
        if ($FreeShipping == 'Y') $FreeShipping = true;
        $PhoneOrder = $CvsDataRowValues['UDF_IT_PHONE_ONLY'];

        if ($PhoneOrder == 'Y') {
            $PhoneOrder = true;
        } else {
            $PhoneOrder = false;
        }

        $UDF_IT_SWC_BEIGE = $CvsDataRowValues['UDF_IT_SWC_BEIGE'];
        $UDF_IT_SWC_BLACK = $CvsDataRowValues['UDF_IT_SWC_BLACK'];
        $UDF_IT_SWC_BLUE = $CvsDataRowValues['UDF_IT_SWC_BLUE'];
        $UDF_IT_SWC_BROWN = $CvsDataRowValues['UDF_IT_SWC_BROWN'];
        $UDF_IT_SWC_CAMO = $CvsDataRowValues['UDF_IT_SWC_CAMO'];
        $UDF_IT_SWC_GOLD = $CvsDataRowValues['UDF_IT_SWC_GOLD'];
        $UDF_IT_SWC_GRAY = $CvsDataRowValues['UDF_IT_SWC_GRAY'];
        $UDF_IT_SWC_GREEN = $CvsDataRowValues['UDF_IT_SWC_GREEN'];
        $UDF_IT_SWC_ORANGE = $CvsDataRowValues['UDF_IT_SWC_ORANGE'];
        $UDF_IT_SWC_PINK = $CvsDataRowValues['UDF_IT_SWC_PINK'];
        $UDF_IT_SWC_PURPLE = $CvsDataRowValues['UDF_IT_SWC_PURPLE'];
        $UDF_IT_SWC_RED = $CvsDataRowValues['UDF_IT_SWC_RED'];
        $UDF_IT_SWC_SILVER = $CvsDataRowValues['UDF_IT_SWC_SILVER'];
        $UDF_IT_SWC_WHITE = $CvsDataRowValues['UDF_IT_SWC_WHITE'];
        $UDF_IT_SWC_YELLOW = $CvsDataRowValues['UDF_IT_SWC_YELLOW'];


        if ($Enabled != 'Y') {  // row marked for deletion
            $this->DeleteItemAsDisabled($ItemCode);
            return false;
        }

        if (empty($ItemCode)) return false;
        $lInventoryItem = InventoryItemPeer::getSimilarInventoryItem($ItemCode, true);

        if (!empty($lInventoryItem)) { // there is Inventory Item with given sku($ItemCode)
            $WasModified = false;
            if (strtotime($lInventoryItem->getDateUpdated()) < strtotime($DateUpdated)) {   // we modify which DateUpdated field is later of row in db
                $WasModified = true;
            } else {
                if (strtotime($lInventoryItem->getDateUpdated()) == strtotime($DateUpdated)) {
                    if (round($lInventoryItem->getTimeUpdated(), 5) < round($TimeUpdated, 5)) {
                        $WasModified = true;
                    }
                }
            }

            if ($WasModified) { // this  Inventory Item was modified at servers datas
                $lInventoryItem->setTitle($Title);
                if (!empty($Brand)) {
                    $lBrand = BrandPeer::getSimilarBrand(trim($Brand), -1, false); // check if exists brand of next inventory row
                    if (empty($lBrand)) {
                        $lBrand = new Brand(); // if not create and save it
                        $lBrand->setTitle(trim($Brand));
                        $lBrand->save();
                    }
                    $lInventoryItem->setBrandId($lBrand->getId()); // set brand to new Inventory
                }

                if (empty($InventoryCategory)) {
                    $this->ItemsSkippedAsWithEmptyInventoryCategory++;
                    return false;
                }
                $lInventoryCategory = InventoryCategoryPeer::getSimilarInventoryCategory($InventoryCategory, '', '');
                if (empty($lInventoryCategory)) { // check if exists category of next inventory row
                    $this->ItemsSkippedAsWithEmptyInventoryCategory++;
                    return false;
                }

                $lInventoryItem->setInventoryCategory($lInventoryCategory->getProductLine());

                $lInventoryItem->setDescriptionShort($DescriptionShort);
                $lInventoryItem->setDescriptionLong($DescriptionLong);
                $lInventoryItem->setSize($Size);
                $lInventoryItem->setColor($Color);
                $lInventoryItem->setFinish($Finish);
                $lInventoryItem->setOptions($Options);
                $lInventoryItem->setGender($Gender);
                $lInventoryItem->setPaperwork($Paperwork);
                $lInventoryItem->setSizing($Sizing);
                $lInventoryItem->setCustomizable($Customizable);
                $lInventoryItem->setVideo($Video);
                $lInventoryItem->setDocuments($Documents);
                $lInventoryItem->setHand($Hand); // assign all fields

                $lInventoryItem->setMatrix(strtoupper($Matrix) == 'Y');
                //Util::deb( $RootItem, ' $RootItem::' );
                if (!empty($RootItem)) {
                    $lInventoryItem->setRootItem($RootItem);
                }
                $lInventoryItem->setFeatures($Features);
                $lInventoryItem->setUnitMeasure($UnitMeasure);
                $lInventoryItem->setQtyOnHand($QtyOnHand);
                $lInventoryItem->setStdUnitPrice($StdUnitPrice);
                $lInventoryItem->setMsrp($Msrp);
                $lInventoryItem->setClearance($Clearance);
                if (!empty($SaleStartDate)) {
                    $lInventoryItem->setSaleStartDate(strtotime($SaleStartDate));
                } else {
                    $lInventoryItem->setSaleStartDate(null);
                }
                if (!empty($SaleEndDate)) {
                    $lInventoryItem->setSaleEndDate(strtotime($SaleEndDate));
                } else {
                    $lInventoryItem->setSaleEndDate(null);
                }
                $lInventoryItem->setSaleMethod($SaleMethod);
                $lInventoryItem->setSalePrice($SalePrice);
                $lInventoryItem->setSalePromoDiscount($SalePromoDiscount);
                $lInventoryItem->setTaxClass($TaxClass);
                $lInventoryItem->setShipWeight($ShipWeight);
                $lInventoryItem->setSetupCharge(strtoupper($SetupCharge) == "Y");
                $lInventoryItem->setDateUpdated(strtotime($DateUpdated));
                $lInventoryItem->setTimeUpdated(round($TimeUpdated, 5));

                $lInventoryItem->setAdditionalFreight($AdditionalFreight);
                // Util::deb( $FreeShipping, '-11 $FreeShipping::' );
                $lInventoryItem->setFreeShipping($FreeShipping);

                //Util::deb( $PhoneOrder, '-11 $PhoneOrder::' );
                $lInventoryItem->setPhoneOrder($PhoneOrder);
                $lInventoryItem->setUdfItSwcBeige(strtolower($UDF_IT_SWC_BEIGE) == 'y'); // set boolean fields
                $lInventoryItem->setUdfItSwcBlack(strtolower($UDF_IT_SWC_BLACK) == 'y');
                $lInventoryItem->setUdfItSwcBlue(strtolower($UDF_IT_SWC_BLUE) == 'y');
                $lInventoryItem->setUdfItSwcBrown(strtolower($UDF_IT_SWC_BROWN) == 'y');
                $lInventoryItem->setUdfItSwcCamo(strtolower($UDF_IT_SWC_CAMO) == 'y');
                $lInventoryItem->setUdfItSwcGold(strtolower($UDF_IT_SWC_GOLD) == 'y');
                $lInventoryItem->setUdfItSwcGray(strtolower($UDF_IT_SWC_GRAY) == 'y');
                $lInventoryItem->setUdfItSwcGreen(strtolower($UDF_IT_SWC_GREEN) == 'y');
                $lInventoryItem->setUdfItSwcOrange(strtolower($UDF_IT_SWC_ORANGE) == 'y');
                $lInventoryItem->setUdfItSwcPink(strtolower($UDF_IT_SWC_PINK) == 'y');
                $lInventoryItem->setUdfItSwcPurple(strtolower($UDF_IT_SWC_PURPLE) == 'y');
                $lInventoryItem->setUdfItSwcRed(strtolower($UDF_IT_SWC_RED) == 'y');
                $lInventoryItem->setUdfItSwcSilver(strtolower($UDF_IT_SWC_SILVER) == 'y');
                $lInventoryItem->setUdfItSwcWhite(strtolower($UDF_IT_SWC_WHITE) == 'y');
                $lInventoryItem->setUdfItSwcYellow(strtolower($UDF_IT_SWC_YELLOW) == 'y');


                $lInventoryItem->save();
                $this->ItemsModified++;
            } else {  // if ( $WasModified ) { // this  Inventory Item was modified at servers datas
                // Util::deb( '-3  SkippedAsExising::');

                //echo "SKU: $ItemCode<br />"; //debug

                $this->ItemsSkippedAsExising++;
            }

        } else { // Add new Inventory Item
            // Util::deb('-2 Add new::');
            $lInventoryItem = new InventoryItem();
            $lInventoryItem->setSku($ItemCode);
            $lInventoryItem->setTitle($Title);

            //$lInventoryItem->setBrandId($BrandId);
            // Util::deb( $Brand, ' ++ $Brand::' );
            if (!empty($Brand)) {
                $lBrand = BrandPeer::getSimilarBrand($Brand, -1, false);
                // Util::deb( $lBrand, ' ++$lBrand::' );
                if (empty($lBrand)) {
                    $lBrand = new Brand();
                    $lBrand->setTitle($Brand);
                    //Util::deb( $lBrand, ' SAVING lBrand::' );
                    $lBrand->save();
                }
                $lInventoryItem->setBrandId($lBrand->getId());
                //if ( $lInventoryItem->getBrandId()== 0 ) die("NULL -2");
            }


            // Util::deb( $InventoryCategory, ' $InventoryCategory::' );
            //Util::deb( $InventoryCategory, '-11 $InventoryCategory::' );
            if (empty($InventoryCategory)) {
                //Util::deb( $InventoryCategory, '-12 $InventoryCategory::' );
                $this->ItemsSkippedAsWithEmptyInventoryCategory++;
                return false;
            }
            $lInventoryCategory = InventoryCategoryPeer::getSimilarInventoryCategory($InventoryCategory, /*'Category '.*//*$InventoryCategory*/
                '', /*'Subcategory '.*//*$InventoryCategory*/
                '');
            //die("-1 DIE");
            if (empty($lInventoryCategory)) {
                Util::deb($lInventoryCategory, '-13 $lInventoryCategory::');
                $this->ItemsSkippedAsWithEmptyInventoryCategory++;
                return false;
            }
            // Util::deb( $lInventoryCategory, ' $lInventoryCategory::' );
            $lInventoryItem->setInventoryCategory($lInventoryCategory->getProductLine());

            //            $lInventoryItem->setInventoryCategory( $lInventoryCategory );
            $lInventoryItem->setDescriptionShort($DescriptionShort);
            $lInventoryItem->setDescriptionLong($DescriptionLong);
            $lInventoryItem->setSize($Size);
            $lInventoryItem->setColor($Color);
            $lInventoryItem->setFinish($Finish);
            $lInventoryItem->setOptions($Options);
            $lInventoryItem->setGender($Gender);
            $lInventoryItem->setPaperwork($Paperwork);
            $lInventoryItem->setSizing($Sizing);
            $lInventoryItem->setCustomizable($Customizable);
            $lInventoryItem->setVideo($Video);
            $lInventoryItem->setDocuments($Documents);
            $lInventoryItem->setHand($Hand);

            $lInventoryItem->setMatrix(strtoupper($Matrix) == 'Y');
            //Util::deb( $RootItem, ' -2 $RootItem::' );
            if (!empty($RootItem)) {
                $lInventoryItem->setRootItem($RootItem);
            }
            $lInventoryItem->setFeatures($Features);
            $lInventoryItem->setUnitMeasure($UnitMeasure);
            $lInventoryItem->setQtyOnHand($QtyOnHand);
            $lInventoryItem->setStdUnitPrice($StdUnitPrice);
            $lInventoryItem->setMsrp($Msrp);
            $lInventoryItem->setClearance($Clearance);
            if (!empty($SaleStartDate)) {
                $lInventoryItem->setSaleStartDate(strtotime($SaleStartDate));
            }
            if (!empty($SaleEndDate)) {
                $lInventoryItem->setSaleEndDate(strtotime($SaleEndDate));
            }
            $lInventoryItem->setSaleMethod($SaleMethod);
            $lInventoryItem->setSalePrice($SalePrice);
            $lInventoryItem->setSalePromoDiscount($SalePromoDiscount);
            $lInventoryItem->setTaxClass($TaxClass);
            $lInventoryItem->setShipWeight($ShipWeight);
            $lInventoryItem->setSetupCharge($SetupCharge);

            $lInventoryItem->setAdditionalFreight($AdditionalFreight);
            //Util::deb( $FreeShipping, '-2 $FreeShipping::' );
            $lInventoryItem->setFreeShipping($FreeShipping);

            // Util::deb( $PhoneOrder, '-2 $PhoneOrder::' );
            $lInventoryItem->setPhoneOrder($PhoneOrder);

            $lInventoryItem->setUdfItSwcBeige(strtolower($UDF_IT_SWC_BEIGE) == 'y');
            $lInventoryItem->setUdfItSwcBlack(strtolower($UDF_IT_SWC_BLACK) == 'y');
            $lInventoryItem->setUdfItSwcBlue(strtolower($UDF_IT_SWC_BLUE) == 'y');
            $lInventoryItem->setUdfItSwcBrown(strtolower($UDF_IT_SWC_BROWN) == 'y');
            $lInventoryItem->setUdfItSwcCamo(strtolower($UDF_IT_SWC_CAMO) == 'y');
            $lInventoryItem->setUdfItSwcGold(strtolower($UDF_IT_SWC_GOLD) == 'y');
            $lInventoryItem->setUdfItSwcGray(strtolower($UDF_IT_SWC_GRAY) == 'y');
            $lInventoryItem->setUdfItSwcGreen(strtolower($UDF_IT_SWC_GREEN) == 'y');
            $lInventoryItem->setUdfItSwcOrange(strtolower($UDF_IT_SWC_ORANGE) == 'y');
            $lInventoryItem->setUdfItSwcPink(strtolower($UDF_IT_SWC_PINK) == 'y');
            $lInventoryItem->setUdfItSwcPurple(strtolower($UDF_IT_SWC_PURPLE) == 'y');
            $lInventoryItem->setUdfItSwcRed(strtolower($UDF_IT_SWC_RED) == 'y');
            $lInventoryItem->setUdfItSwcSilver(strtolower($UDF_IT_SWC_SILVER) == 'y');
            $lInventoryItem->setUdfItSwcWhite(strtolower($UDF_IT_SWC_WHITE) == 'y');
            $lInventoryItem->setUdfItSwcYellow(strtolower($UDF_IT_SWC_YELLOW) == 'y');

            $lInventoryItem->setDateUpdated(strtotime($DateUpdated));
            $lInventoryItem->setTimeUpdated(round($TimeUpdated, 5));

            $lInventoryItem->save(); // save inventory item
            $this->ItemsAdded++;
        }

        $Debug = false;
        if ($ItemCode == '0-8001-065') { // debugging of some specific Inventory Item
            $Debug = true;
        }

        $sf_upload_dir = sfConfig::get('sf_upload_dir');
        $InventoryItemsThumbnails = sfConfig::get('app_application_InventoryItemsThumbnails');

        $ImportThumbnailImageFile = AppUtils::getImportThumbnailImageBySku($ItemCode, true);

        $DestFilename = $sf_upload_dir . DIRECTORY_SEPARATOR . $InventoryItemsThumbnails . DIRECTORY_SEPARATOR . basename($ImportThumbnailImageFile);

        if (!empty($ImportThumbnailImageFile)) { // copy image of inventory item, if we have it
            $CopyRes = copy($ImportThumbnailImageFile, $DestFilename);
            if ($CopyRes) {
                $this->ImageThumbnailsAdded++;
            }
            chmod($DestFilename, 0755);
        }


        $InventoryItems = sfConfig::get('app_application_InventoryItems');
        $ImportImageFileArray = AppUtils::getImportImagesBySku($ItemCode, true);
        foreach ($ImportImageFileArray as $ImportImageFile) { // copy all images of inventory item, if we have them
            $DestFilename = $sf_upload_dir . DIRECTORY_SEPARATOR . $InventoryItems . DIRECTORY_SEPARATOR . basename($ImportImageFile);
            if (!empty($ImportImageFile)) {
                $CopyRes = copy($ImportImageFile, $DestFilename);
                if ($CopyRes) {
                    $this->ImagesAdded++;
                }
                chmod($DestFilename, 0755);
            }
        }

        return true;
    } //function ImportCvsData($CvsDataRow) {

    public function getInfoText() // show info text with numbers of operations done
    {
        $Res = '<hr>';
        $Res .= 'File loaded: <b>' . $this->InventoryItemsImportCVSFile . '</b>. <br><br>&nbsp;&nbsp;';
        $Res .= 'Inventory Items Added: <b>' . $this->ItemsAdded . '</b><br>&nbsp;&nbsp;' .
            'Inventory Items Modified: <b>' . $this->ItemsModified . '</b><br>&nbsp;&nbsp;' .
            'Inventory Items Skipped As Exising: <b>' . $this->ItemsSkippedAsExising . '</b><br>&nbsp;&nbsp;' .
            'Inventory Items Deleted As Disabled: <b>' . $this->ItemsDeletedAsDisabled . '</b><br>&nbsp;&nbsp;' .
            'Inventory Items Skipped As Non Existing: <b>' . $this->ItemsSkippedAsNonExisting . '</b><br>&nbsp;&nbsp;' .
            'Inventory Items Skipped As With Empty Inventory Category: <b>' . $this->ItemsSkippedAsWithEmptyInventoryCategory . '</b><br>&nbsp;&nbsp;' .


            'Inventory Categories Deleted As Empty: <b>' . $this->InventoryCategoriesDeletedAsEmpty . '</b><br>&nbsp;&nbsp;' .
            'Inventory Items Images Copied: <b>' . $this->ImagesAdded . '</b><br>&nbsp;&nbsp;' .
            'Inventory Items Image Thumbnails Copied: <b>' . $this->ImageThumbnailsAdded . '</b><br>&nbsp;&nbsp;';
        return $Res;
    }

    private function DeleteItemAsDisabled($ItemCode)
    {  // row marked for deletion, so it is exists to delete it

        $lInventoryItem = InventoryItemPeer::getSimilarInventoryItem($ItemCode, true);
        if (!empty($lInventoryItem)) {
            $lInventoryItem->delete();
            $this->ItemsDeletedAsDisabled++;
            return true;
        } else {
            $this->ItemsSkippedAsNonExisting++;
            return false;
        }

    }


    private function DeleteNonExistingInventoryCategories()
    {
        return;
        $InventoryCategoriesList = InventoryCategoryPeer::getInventoryCategories(1, false);
        foreach ($InventoryCategoriesList as $lInventoryCategory) {
            $InventoryItemCount = InventoryItemPeer::getInventoryItemsByInventoryCategory(1, false, true, $lInventoryCategory->getProductLine());
            if ($InventoryItemCount == 0) {
                Util::deb($lInventoryCategory->getProductLine(), '$lInventoryCategory->getProductLine()::');
                Util::deb($InventoryItemCount, '$InventoryItemCount::');
                $lInventoryCategory->delete();
                $this->InventoryCategoriesDeletedAsEmpty++;
            }

        }
    }
}

// EXAMPLE OF USAGE :
public function executeInventory_items_import(sfWebRequest $request)
{

    $manually= $request->getParameter('manually');
    $small_test= $request->getParameter('small_test');

    $this->InfoText= '';
    include_once('lib/InventoryItemsImport.php');
    try
    {
        $InventoryItemsImport= new InventoryItemsImport();

        $sf_root_dir= sfConfig::get('sf_root_dir');
        if ( !$small_test ) {
            $InventoryItemsImportCVSFile= sfConfig::get('app_application_InventoryItemsImportCVSFile');
        } else {
            $InventoryItemsImportCVSFile= sfConfig::get('app_application_InventoryItemsImportCVSFile_SmallTest');
        }
        $InventoryItemsImport->setInventoryItemsImportCVSFile( $sf_root_dir . $InventoryItemsImportCVSFile );

        $ImportRes= $InventoryItemsImport->Run();
        echo '$ImportRes::'.print_r( $ImportRes, true ).'<br>';
        $this->InfoText= $InventoryItemsImport->getInfoText();
        if ( $manually ) {       // http://www.local-lamp.com/frontend_dev.php/cron_calculating_score?manually=1
        } else {
            return sfView::NONE;
        }
    }
    catch (Exception $lException)
    {
        $this->logMessage($lException->getMessage(), 'err');
        return sfView::ERROR;
    }
}



?>
