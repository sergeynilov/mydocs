<?php
class ImportColorMappings
{
	private $ItemsAdded = 0;
	private $ItemsDeleted = 0;
	private $RelatedInvetoryItemsDeleted = 0;
	private $SkippedAsNotModified = 0;

	private $OriginalExcelFileName;
	private $ExcelFileName;
	private $ColumnsArray;

	private function getColumnName($pColumnNumber)
	{
		foreach ($this->ColumnsArray as $ColumnNumber => $ColumnName) {
			if ((int)$pColumnNumber == (int)$ColumnNumber) return $ColumnName;
		}
	}

	public function setFileName($pExcelFileName)
	{
		$this->ExcelFileName = $pExcelFileName;
	}

	public function setOriginalFileName($pOriginalExcelFileName)
	{
		$this->OriginalExcelFileName = $pOriginalExcelFileName;
	}


	public function Run()
	{
		$this->ItemsAdded = 0;
		$this->RelatedInvetoryItemsDeleted = 0;
		$this->SkippedAsNotModified = 0;

		$ExcellArray = new sfExcelReader($this->ExcelFileName);
		$ExcellTabData = $ExcellArray->sheets[0]['cells'];
		$this->ColumnsArray = $ExcellTabData[1];

		for ($I = 2; $I <= count($ExcellTabData) + 250; $I++) {
			if (empty($ExcellTabData[$I])) continue;
			$DataArray = $ExcellTabData[$I];
			unset($lColorMapping);
			$Color = '';
			$swatch_color_1 = '';
			$swatch_color_2 = '';
			$swatch_color_3 = '';

			foreach ($DataArray as $ColumnNumber => $ColumnValue) { // all columns in dict
				$ColumnName = trim($this->getColumnName($ColumnNumber));
				$ColumnValue = trim($ColumnValue);
				if (strtolower($ColumnName) == strtolower('COLOR')) {
					$Color = $ColumnValue;
				}
				if (strtolower($ColumnName) == strtolower('swatch_color') and $ColumnNumber == 2 ) {
					$swatch_color_1 = $ColumnValue;
				}
				if (strtolower($ColumnName) == strtolower('swatch_color') and $ColumnNumber == 3 ) {
					$swatch_color_2 = $ColumnValue;
				}
				if (strtolower($ColumnName) == strtolower('swatch_color') and $ColumnNumber == 4 ) {
					$swatch_color_3 = $ColumnValue;
				}
			} // foreach( $datasArray as $ColumnNumber=>$ColumnValue ) { // all columns in dict

			if (empty($Color) or empty($swatch_color_1)) continue;

			try {
				$lColorMapping= new ColorMapping();
				$lColorMapping->setColor($Color);
				$lColorMapping->setSwatchColor1($swatch_color_1);
				$lColorMapping->setSwatchColor2($swatch_color_2);
				$lColorMapping->setSwatchColor3($swatch_color_3);
				$lColorMapping->save();
				$this->ItemsAdded ++;
			} catch (Exception $lException) {
				continue;
			}
		}

	}

	public function getDeleteRows()
	{
		$this->ItemsDeleted=0;
    $ColorMappingList= ColorMappingPeer::getColorMappings('', false);
		foreach($ColorMappingList as $lColorMapping) {
			$lColorMapping->delete();
			$this->ItemsDeleted++;
		}
	}

	public function getInfoText()
	{
		$Res = '<hr>';
		$Res .= 'Import from : <b>' . $this->OriginalExcelFileName . '</b>. <br><br>&nbsp;&nbsp;' .
		'Color Mappings Deleted: <b>' . $this->ItemsDeleted . '</b><br>&nbsp;&nbsp;' .
		'Color Mappings Added: <b>' . $this->ItemsAdded . '</b><br>&nbsp;&nbsp;';
		return $Res;
	}

}