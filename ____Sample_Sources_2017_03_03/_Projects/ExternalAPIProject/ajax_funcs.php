<?php
include_once( "DataLoadApi.php" );
include_once( "Utils.php" );
$task= $_GET['task'];
//echo '$task::'.$task.'<br>';
if ( $task== 'getCarModelsbyManufacturer' ) { 
  $ResArray= array();
  $value= $_GET['value'];
  $carModelArray= DataLoadAPI::getcarModel( $value );  
  echo json_encode( array('ErrorMessage'=>'', 'ErrorCode'=>0, 'value'=> $value, 'length'=>count($carModelArray), 'data'=>$carModelArray) );
}

if ( $task== 'getcarListsbyModel' ) { 
  $ResArray= array();
  $Manufacturer= $_GET['manufacturer'];
  $Model= $_GET['model'];
  $carListArray= DataLoadAPI::getcarList( $Manufacturer, $Model/*, '', 'Please select...'*/ );  
  echo json_encode( array('ErrorMessage'=>'', 'ErrorCode'=>0, 'model'=> $Model, 'manufacturer'=> $Manufacturer, 'length'=>count($carListArray), 'data'=>$carListArray) );
}


if ( $task== 'getVehicleFilterBy5Params' ) { 
  $ResArray= array();
  $Manufacturer= $_GET['manufacturer'];
  $Model= $_GET['model'];
  $EngineCapacity= $_GET['EngineCapacity'];
  $YearOfManufacture= $_GET['YearOfManufacture'];
  
  // getVehicleFilter($Manufacturer, $CarModel, $EngineCapacity, $YearOfManufacture, $SelectLabelValue= '', $SelectLabelText= '')
  $carListArray= DataLoadAPI::getVehicleFilter( $Manufacturer, $Model, $EngineCapacity, $YearOfManufacture );  
  echo json_encode( array('ErrorMessage'=>'', 'ErrorCode'=>0, 'model'=> $Model, 'manufacturer'=> $Manufacturer, 
  'EngineCapacity'=>$EngineCapacity, 'YearOfManufacture'=>$YearOfManufacture, 
  'length'=>count($carListArray), 'data'=>$carListArray) );
}

if ( $task== 'getTownsbyArea' ) { 
  $ResArray= array();
  $value= $_GET['value'];
  $selected_town= $_GET['selected_town'];  
  //echo '$value::'.$value.'<br>';  
  $TownsOfAreaArray= DataLoadAPI::getTownsOfArea( $value );  
  foreach( $TownsOfAreaArray as $key=>$name ) {
    $ResArray[]= array( 'id'=>$key,'name'=>$name );
  }
  //echo '$ResArray::'.print_r($ResArray).'<br>';
  echo json_encode( array('ErrorMessage'=>'', 'ErrorCode'=>0, 'value'=> $value, 'length'=>count($ResArray), 'data'=>$ResArray, 'selected_town'=>$selected_town) );
}



if ( $task== 'getProceedInfo' ) { 
  $ResArray= array();
  //$value= $_GET['value'];
  $Res_1= array( 'text'=>'Third Party<br>', 'value'=>'<b>&euro;&nbsp;548</b>' );
  $Res_2= array( 'text'=>'Third Party Fire & Theft<br>', 'value'=>'<b>&euro;&nbsp;594</b>' );
  $Res_3= array( 'text'=>'Fully comprehensive<br>', 'value'=>'<b>&euro;&nbsp;847</b>' );
  echo json_encode( array('ErrorMessage'=>'', 'ErrorCode'=>0, 'Res_2'=> $Res_2, 'Res_1'=> $Res_1,  'Res_3'=> $Res_3, 'length'=>3, 'data'=>$carModelArray) );
}
?>