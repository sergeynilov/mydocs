<?php 
include_once( "include/DataLoadApi.php" );
include_once( "include/Utils.php" );
$AreasArray= DataLoadAPI::getAreas('','Please select...');
$SexArray= DataLoadAPI::getSex();
$driverNCDYearsArray= DataLoadAPI::getdriverNCDYears('','Please select...');
$carManufacturerArray= DataLoadAPI::getcarManufacturer('','Please select...');
$carModelArray= array(''=>'Please select...');  //DataLoadAPI::getcarModel('','Please select...');
$carRegYearArray= DataLoadAPI::getCarRegYear('','Please select...');
$carEngSizeArray= DataLoadAPI::getcarEngSize('','Please select...');
$carListArray= array(''=>'Please select...');//DataLoadAPI::getcarList('AUDI','100', '','Please select...');
$SiteHost= '';
if ( !Util::isDeveloperComp() ) {
  $SiteHost= 'http://quotedevil.morsolutions.net/car_insurance/';
}
$data= '';


?>
<link rel="stylesheet" type="text/css" href="css/calendar.css" />    
<link rel="stylesheet" type="text/css" href="css/quotedevil.css" />
<!--[If gte IE 7]><link rel="stylesheet" type="text/css" href="../css/quotedevilie7.css" /><![endif]-->
<!--[if lt IE 7.]><link rel="stylesheet" type="text/css" href="../css/quotedevilie6.css" /><![endif]-->

<link href="css/jquery-ui-1.8.5.custom.css" rel="stylesheet" type="text/css" />

<style type='text/css'>  
#form_quick_quote_selectors input.error, #form_quick_quote_selectors textarea.error {
	background-color: #f99;
}
#form_quick_quote_selectors div.error, #form_quick_quote_selectors label.error {
	color: red;
}
#form_quick_quote_selectors div.errorlabels label {
	display: block;
}
#form_quick_quote_selectors label.checked {
  background:url("images/checked.gif") no-repeat 0px 0px;
  padding-top: 1px;
  padding-left: 10px;
  vertical-align: middle;
  width: 26px;
  white-space: nowrap;
}

</style>
  <script type="text/javascript" language="JavaScript" src="js/jquery/jquery.js"></script> 
  <script type="text/javascript" language="JavaScript" src="js/jquery/jquery.validate.pack.js"></script>

  <script type="text/javascript" language="JavaScript" src="js/jquery/jquery.selectbox-0.6.1.js"></script>
  <script type="text/javascript" language="JavaScript" src="js/jquery/jquery-ui-1.8.5.custom.min.js"></script>
  <script type="text/javascript" language="JavaScript" src="js/jquery/jquery.hoverIntent.minified.js"></script>
  <script type="text/javascript" language="JavaScript" src="js/jquery/jquery.bt.min.js"></script>
  <!--  http://www.lullabot.com/articles/announcing-beautytips-jquery-tooltip-plugin  -->
  <script type="text/javascript" language="JavaScript" src="js/funcs.js"></script>
  <script type="text/javascript" language="JavaScript" src="js/overlib/overlib.js"></script>
  
  <script type="text/javascript" language="JavaScript" src="js/jquery/ui.js"></script>
  <script type="text/javascript" language="JavaScript" src="js/jquery/ui.datepicker.js"></script>

  <script type="text/javascript" language="JavaScript">
  <!--

  function SetcarList(value) {
    //alert( "SetcarList value::"+value )
    SetDDLBActiveItem( 'carList', value)
    
  
  function onChangecarManufacturer(value) {
    if ( Trim(value)=="" ) {
      ClearDDLBItems( 'carModel', false )
      return;
    }
    var HRef= "<?php echo $SiteHost ?>include/ajax_funcs.php?task=getCarModelsbyManufacturer&value="+encodeURIComponent(value);
    //    alert(HRef)
    $.getJSON(HRef,
    {
    },
    getCarModelsbyManufacturer,

    function(x,y,z) {   //Some sort of error
      alert(x.responseText);
    }
    );
  }

  function getCarModelsbyManufacturer(data) {
    var ArrayLength= parseInt(data['length'])
    var Manufacturer= data['value']
    ClearDDLBItems( 'carModel', false ) //clear all items vbut first("-Select-")
    var DataArray= data['data']
    for( i=0; i< ArrayLength; i++ ) {
      if ( DataArray[i]['id'] && DataArray[i]['name'] ) {
        var Id= DataArray[i]['id']
        var Name= DataArray[i]['name']
        AddDDLBItem( 'carModel', Id, Name ); //Add all options to input selection
      }
    }
    SetDDLBActiveItem( 'carManufacturer', Manufacturer)
  }



  function onChangecarEngSize(model) {
    if ( Trim(model)=="" ) {
      ClearDDLBItems( 'carList', false )
      return;
    }
    var carManufacturer= document.getElementById("carManufacturer").value
    var carRegYear= document.getElementById("carRegYear").value
    var carModel= document.getElementById("carModel").value
    var carEngSize= document.getElementById("carEngSize").value

    var HRef= "<?php echo $SiteHost ?>include/ajax_funcs.php?task=getVehicleFilterBy5Params&model="+encodeURIComponent(carModel)+"&manufacturer="+encodeURIComponent(carManufacturer)+'&YearOfManufacture='+encodeURIComponent(carRegYear)+'&EngineCapacity='+encodeURIComponent( carEngSize );
    //alert(HRef)
    
    $.getJSON(HRef,
    {
    },
    getcarListsbyModel,

    function(x,y,z) {   //Some sort of error
      alert(x.responseText);
    }
    );
  }

  function getcarListsbyModel(data) {
    var ArrayLength= parseInt(data['length'])

    var Model= data['model']
    var Manufacturer= data['manufacturer']
    
    ClearDDLBItems( 'carList', false ) //clear all items but first("-Select-")
    var DataArray= data['data']
    for( i=0; i< ArrayLength; i++ ) {
      if ( DataArray[i]['id'] && DataArray[i]['name'] ) {
        var Id= DataArray[i]['id']
        var Name= DataArray[i]['name']
        AddDDLBItem( 'carList', Id, Name ); //Add all options to input selection
      }
    }
    SetDDLBActiveItem( 'carModel', Model)
    <?php if ( isset($data['carList']) ) { ?>
      SetcarList('<?php echo $data['carList']; ?>'); 
    <?php } ?>
  }


  $(function($) {
    $('.helpertip').bt({
      padding: 15,
      width: 170,
      spikeLength: 15,
      spikeGirth: 10,
      cornerRadius: 10,
      fill: 'rgba(253, 236, 236, 1)',
      strokeWidth: 2,
      strokeStyle: '#f0d2d4',
      cssStyles: {color: '#555555', fontSize: '11px', lineHeight:'13px'},
      positions: [ 'right'],
    });

    $("#form_quick_quote_selectors").validate({
      submitHandler: function(form) {
        //alert("SAVING ROUTINGS !!!  HRef::"/*+HRef*/);
        ShowProceedInfo()
        return true;
      },

      rules: { // http://jquery.bassistance.de/validate/demo/milk/
        carAreaKeptOvernight: { required: true },
        sexId: { required: true },
        driverAge: { required: true, number:true, range: [17, 99] },
        PhoneNumber: { required: true },
        Email: { required: true, email:true },
        driverNCDYears: { required: true },
        carManufacturer: { required: true  },
        carModel: { required: true },
        carRegYear: { required: true  },
        carEngSize: { required: true },
        carList: { required: true },
        carValue: { required: true, number:true, range: [1, 100000]  },
      },
      messages: {
        carAreaKeptOvernight: { required:"&nbsp;*" },
        sexId: { required: "&nbsp;*" },
        driverAge: { required: "&nbsp;*" },
        PhoneNumber: { required: "&nbsp;*" },
        Email: { required: "&nbsp;*" },
        driverNCDYears: { required: "&nbsp;*" },
        carManufacturer: { required: "&nbsp;*" },
        carModel: { required: "&nbsp;*" },
        carRegYear: { required: "&nbsp;*" },
        carEngSize: { required: "&nbsp;*" },
        carList: { required: "&nbsp;*" },
        carValue: { required: "&nbsp;*" },
      },
      success: function(label) {
        // set &nbsp; as text for IE
        //alert( "label::"+var_dump(label/*.id.toString()*/) )
        label.html("&nbsp;").addClass("checked");
      },


      
    });
    $('#form_quick_quote_selectors').bind('invalid-form.validate', function(e, validator) {
    });


    <?php if ( isset($data['carManufacturer']) ) { ?>
    onChangecarManufacturer('<?php echo $data['carManufacturer'] ?>')
    <?php } ?>
    


  });

  function FormSubmit() {
    var theForm= document.getElementById("form_quick_quote_selectors")
    var StyleDisplay= document.getElementById("div_proceed_info").style.display
    if ( StyleDisplay == "none" ) {
      return false
    }
    return true
  }

  function ShowProceedInfo(){
    document.getElementById("div_proceed_info").style.display= "block"
    document.getElementById("div_proceed_button").style.display= "block"
    
    var HRef= "<?php echo $SiteHost ?>include/ajax_funcs.php?task=getProceedInfo";
      //  alert(HRef)
    $.getJSON(HRef,
    {
    },
    getProceedInfo,

    function(x,y,z) {   //Some sort of error
      alert(x.responseText);
    }
    );
    
  }

  function getProceedInfo(data) {
    var ArrayLength= parseInt(data['length'])
    var Res_1= data['Res_1']
    var Res_3= data['Res_3']
    var Res_2= data['Res_2']
    
    document.getElementById("div_proceed_info_1").innerHTML= Res_1['text']+Res_1['value']
    document.getElementById("div_proceed_info_2").innerHTML= Res_2['text']+Res_2['value']
    document.getElementById("div_proceed_info_3").innerHTML= Res_3['text']+Res_3['value']
    //ClearDDLBItems( 'carModel', false ) //clear all items vbut first("-Select-")
    
  }
  
  //-->
  </script>

<div class="clr"></div>

<h2>Motor Insurance</h2>
<p class="description">Ma quande lingues coalesce, li grammatica del resultant lingue es plu simplic e regulari quam ti del coalescent lingues. Li nov lingua franca va esser plu simplic e regulari quam li existent Europan lingues. It va esser tam simplic quam Occidental: in fact, it va esser Occidental.Li nov lingua franca va esser plu simplic e regulari quam li existent Europan lingues. It va esser tam simplic quam Occidental:.</p>
<img src="images/img04.jpg" alt="" class="carInBg"/><div class="clr"></div>
<div class="whyYneed">> <a href="#" title="Why you need it?">Why you need it?</a></div>
<div class="border"></div>
<h3 class="carInsTitle">Motor Insurance</h3>

<div class="stepsBox">

<form action="car_insurance.php?task=update_quick_quote" id="form_quick_quote_selectors" name="form_quick_quote_selectors" method="POST" onsubmit="javascript:return FormSubmit()" >

	<div class="stepContenBox">
		<h3>STEP 1 - Example section header</h3>

    
		<div >
			<label for="carAreaKeptOvernight">Area kept overnight<span>*</span>:</label>
			<?php echo Util::select_tag( 'carAreaKeptOvernight', $AreasArray, isset($data['carAreaKeptOvernight'])?$data['carAreaKeptOvernight']:'', array('class'=>"input_item helpertip target")	 ); ?>
		</div>
    
		<div >
			<label >Gender<span >*</span>:</label>
			<span class="radioPad" >
			  <?php echo Util::radio_tag( 'sexId', $SexArray, isset($data['sexId'])?$data['sexId']:'', array('class'=>"input_item helpertip target") ); ?>
			</span>
		</div>

		<div>
		  <label>Your age<span>*</span>:</label>
 			<input class="input_itemalt helpertip target" title="<h6>Your age</h6><font>Your age Ma quande lingues coalesce, li grammatica del resultant lingue es plu simplic e regulari quam ti del coalescent lingues.</font>" type="text" name="driverAge" id="driverAge" value="<?php echo isset($data['driverAge'])?$data['driverAge']:'' ?>" maxlength="2" />		  
		</div>	

		<div>
		  <label>Phone<span>*</span>:</label>	
  		<input type="text" class="input_itemalt helpertip target" title="<h6>Your Phone</h6><font>Your phone Ma quande lingues coalesce, li grammatica del resultant lingue es plu simplic e regulari quam ti del coalescent lingues.</font>" id="PhoneNumber" name="PhoneNumber" value="<?php echo isset($data['PhoneNumber'])?$data['PhoneNumber']:'' ?>" style="text-align: left;" maxlength="20" />		  
		</div>

		<div>
		  <label>Email<span>*</span>:</label>			
  		<input type="text" class="input_itemalt helpertip target" title="<h6>Your email</h6><font>Your email Ma quande lingues coalesce, li grammatica del resultant lingue es plu simplic e regulari quam ti del coalescent lingues.</font>" id="Email" name="Email" value="<?php echo isset($data['Email'])?$data['Email']:'' ?>" style="text-align: left;" maxlength="100" />
		</div>

		<div>
			<label>No claims discount<span>*</span>:</label>
			<?php echo Util::select_tag( 'driverNCDYears', $driverNCDYearsArray, isset($data['driverNCDYears'])?$data['driverNCDYears']:'', array('class'=>"input_item helpertip target") ); ?>&nbsp;
      <?php $HelpText= addslashes('A No Claims Discount is earned for every year you hold an insurance policy in your own name without making a claim. Tip: It is widely believed that the maximum discount earned by insurers is capped at 5 years, however, insurers are allowing discounts beyond this so don’t be shy and select the total number of years (up to 8+) you’ve been driving claim free.') ?>
	    <a onmouseover="return overlib(' <?php echo $HelpText ?> ',CAPTION,'No claims discount ',BGCOLOR,'#2a323d',FGCOLOR,'#FFE4ED',CAPTIONSIZE,'2',TEXTSIZE,'2',HAUTO,VAUTO,WIDTH,'250');" onmouseout="return nd(); " > 
	      <img src="images/info.png" border="none" width="16" height="16">
	    </a>
		</div>

		<div>
			<label>Car manufacturer<span>*</span>:</label>
			<?php echo Util::select_tag( 'carManufacturer', $carManufacturerArray, isset($data['carManufacturer'])?$data['carManufacturer']:'', array('onchange'=>'javascript:onChangecarManufacturer(this.value)', 'class'=>"input_item helpertip target" ) ); ?>
		</div>

		<div>
			<label>Car model<span>*</span>:</label>
			<?php echo Util::select_tag( 'carModel', $carModelArray, isset($data['carModel'])?$data['carModel']:'', array( 'class'=>"input_item helpertip target" ) ); ?>
		</div>

		<div>
			<label>Car registered year<span>*</span>:</label>
			<?php echo Util::select_tag( 'carRegYear', $carRegYearArray, isset($data['carRegYear'])?$data['carRegYear']:'', array('class'=>"input_item helpertip target") ); ?><!-- <br> <input name="" type="text" value="YYYY  /  MM  /  DD" class="input_itemsmall" id="datepicker" /> -->
		</div>

		<div>
			<label>Car engine size<span>*</span>:</label>
			<?php echo Util::select_tag( 'carEngSize', $carEngSizeArray, isset($data['carEngSize'])?$data['carEngSize']:'', array('onchange'=>'javascript:onChangecarEngSize(this.value)', 'class'=>"input_item helpertip target") ); ?>
		</div>

		<div>
			<label>Car details<span>*</span>:</label>
			<?php echo Util::select_tag( 'carList', $carListArray, isset($data['carList'])?$data['carList']:'', array('class'=>"input_item helpertip target") ); ?>
		</div>
		
		<div>
			<label>Car value<span>*</span>:&euro;&nbsp;</label>
			<input type="text" class="input_itemalt helpertip target" title="<h6>Your car Value</h6><font>Your car Value Ma quande lingues coalesce, li grammatica del resultant lingue es plu simplic e regulari quam ti del coalescent lingues.</font>" id="carValue" name="carValue" maxlength="6" value="<?php echo isset($data['carValue'])?$data['carValue']:'' ?>"/>
		</div>

		<div>
		  <label></label>
		  <input type="image" src="images/show-quote-btn.png"/>
		</div>
		<div>
		  <label></label>
		  <em>Required fields are marked with <span>*</span></em>
		</div>
		
         
     <div id="div_proceed_info" style="display:none" >
       <h3>Premium</h3>
       <table border="0">
         <tr><td><div id="div_proceed_info_1" style="width:200px; height:100px; ">Info 1...</div></td>
         <td><div id="div_proceed_info_2" style="width:200px; height:100px; ">Info 2...</div></td>
         <td><div id="div_proceed_info_3" style="width:200px; height:100px; ">Info 3...</div></td></tr>
       </table>
     </div>
    
     <div id="div_proceed_button" style="display:none" >
       <input type="button" id="action_submit" value="" onclick="javascript:document.getElementById('form_quick_quote_selectors').submit()" />
       <p>This quote is inclusive of administration fee and is based on some assumed details. Please click proceed and answer some additional questions to get accuarate quotes from several insurers based on your actual details.</p>
     </div>
  
  </div>
  
</form>
</div>