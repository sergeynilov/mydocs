<?php 
class DataLoadAPI {
  private static $StartCarRegYear= 1900;
  private static $ClientRef;
  private static $Licenceyearsheld_Min=0;
  private static $Licenceyearsheld_Max=9;

  public static $PolicyholderPenaltypoints_Min=0;
  public static $PolicyholderPenaltypoints_Max=12;

  public static $OtherpolicyHowmanyyears_Min= 1;
  public static $OtherpolicyHowmanyyears_Max= 8;

  public static $ddlHearAboutUs_other_Code= 8;
  public static $SpouseRelationship2policyholder=18;
  public static $SpousePenaltypoints_Min=0;
  public static $SpousePenaltypoints_Max=12;

  public static $EngSize_Min=0.8;
  public static $EngSize_Max=2.2;

  private static $DateFormat= '%Y-%m-%d';// %H:%M:%S';
  private static function InitAPI() {
    DataLoadAPI::$ClientRef = new SoapClient('/hsphere/local/home/quotedevil/quotedevil.morsolutions.net/car_insurance/include/RelayFullCycleMotorService.WSDL', array( 'trace' => 1, 'location' => 'https://services2.relay.ie/relaymotorservicetwo/relaymotorservice.asmx' ) );

  }


  public static function getBusinessQuotation($SelectLabelValue= '', $SelectLabelText= '') {
    if (Util::isDeveloperComp()) {
      $NBDir= '/_SymfonyProjects/QuoteDevilAccess/_DOC/Motor Web Services/SampleSoap Messages/NB';
    } else {
      $NBDir= '/hsphere/local/home/quotedevil/quotedevil.morsolutions.net/car_insurance/include//Motor Web Services/SampleSoap Messages/NB';
    }
    $FileName= 'in_0f273e3c-41d3-4e46-9895-6a22971d6289.xml';
    $ResArray= array();

    if ( file_exists($NBDir.DIRECTORY_SEPARATOR.$FileName) ) {
      echo '<b>FILE EXISTS</b>';
    }


    /*$SampleFile = simplexml_load_file($NBDir.DIRECTORY_SEPARATOR.$FileName);
    echo '<pre>'.print_r($SampleFile,true).'</pre>';

    Util::deb($SampleFile,'$SampleFile::');
    if ( !empty($SampleFile) ) {
    foreach ($SampleFile->GetNewBusinessQuotation as $lGetNewBusinessQuotation) {
    Util::deb($lGetNewBusinessQuotation,'$lGetNewBusinessQuotation::');
    }
    } */

    /*$xml = simplexml_load_file($NBDir.DIRECTORY_SEPARATOR.$FileName);
    $namespaces = $xml->getNamespaces();
    $soap = $xml->children($namespaces['soap']);
    var_dump($soap->Body->children());
    foreach( $soap->Body->children() as $Key=>$Value ) {
    Util::deb($Key,'$Key::');
    Util::deb($Value,'$Value::');
    if ( $Key == 'GetNewBusinessQuotation' ) {
    foreach( $Value as $ArrayKey=>$ArrayValue ) {
    Util::deb($ArrayKey,'$ArrayKey::');
    Util::deb($ArrayValue,'$ArrayValue::');

    }
    }
    }*/




    if ( !Util::isDeveloperComp() ) {
      DataLoadAPI::InitAPI();
      echo 'RUN NOW::<br><hr><hr>';


      $TestData= DataLoadAPI::getTestData();
      echo '<hr><hr>$TestData::<pre>'.print_r($TestData,true).'</pre><br><hr><hr>';
      $QuotationMotorProposals = DataLoadAPI::$ClientRef->GetQuotationMotorProposal( $TestData );
      

      
      
      //$ModelDetails = DataLoadAPI::$ClientRef->__call("GetQuotationMotorProposal", $TestData );
      echo "getLastRequest():\n" . DataLoadAPI::$ClientRef->__getLastRequest() . "\n.LAST REQUSET DONE";

      echo 'QuotationMotorProposal::'.print_r($QuotationMotorProposal,true).'<br><hr><hr>';

      foreach($QuotationMotorProposal->GetQuotationMotorProposalResult->LookupItem as $item) {
        //echo "IN foreach getLastRequest():\n" . DataLoadAPI::$ClientRef->__getLastRequest() . "\n.LAST REQUSET DONE";
        if ( empty($item->Code) or empty($item->Description) ) continue;
        $ResArray[$item->Code]= $item->Description;
      }
    } else {
      $ResArray['BusinessQuotation test']= 'BusinessQuotation test';
    }
    if ( !empty($SelectLabelText) ) {
      $ResArray= Util::SetArrayHeader( array($SelectLabelValue=>$SelectLabelText), $ResArray );
    }
    return $ResArray;
    // $CarModels = DataLoadAPI::$ClientRef->GetVehicleModel(array('Manufacturer'=>$Manufacturer));

  }


  public static function getTestData() {
    $Proposer=	array('ProposerType'=>'eIndividual', 
    'TitleCode'=>'005', 
    'Title'=>'Mr.', 
    'ForeName'=>'Andrew',
    'SurName'=>'Gibson', 
    'Sex'=>'M', 
    'MaritalStatus'=>'M',
    'DateOfBirth'=>'1974-03-02T00:00:00', // -------- Quick Quote --- [today() - [Your age] + 1 day]
    'Address'=>
    array( 'Line1'=>'1 Some Street', 'Line2'=>'Some Town', 'Line3'=>'Anytown', 'Line4'=>'Dublin 2', 'FloodAreaChecked'=>false ),
    'Contact'=>
    array('Home'=>'34324234', 'Work'=>'', 'Mobile'=>'', 'Fax'=>'', 'Email'=>'agi@relay.ie'),
    'NCD'=>
    array('ClaimedYearsEarned'=>'5', 
    'DrivingExperienceYears'=>'0', 
    'ClaimedCountry'=>'IE',
    'ClaimedInsurer'=>'001', 
    'PreviousPolicyNumber'=>'12345678',
    'DrivingExperiencePolicyExpiryDate'=>strftime(DataLoadAPI::$DateFormat), // try this as default - if error use today()
    'ClaimedDiscountType'=>'S',
    'ClaimedBonusProtectionType'=>'N',
    'ClaimedProtectedInd'=>true,
    'ProtectionRequiredInd'=>true,
    'DrivingExperienceProvenInd'=>true,
    'ClaimedProvenInd'=>false,
    'PreviousPolicyExpiryDate'=>strftime(DataLoadAPI::$DateFormat), // try this as default - if error use today() / now()
    'RebrokeYearsProvided'=>false,
    'RebrokeYears'=>0
    )
    );

    $Policy= array( 'PolicyNumber>'=>'QWERTY12345',
    'StartTime'=>'000001',
    'EndTime'=>'235959',
    'PreviousInsurer'=>'999',
    'QuoteAuthor'=>'RLY',
    'CurrencyRequired'=>'EUR',
    'InceptionDate'=>'2010-11-27',
    'StartDate'=>strftime(DataLoadAPI::$DateFormat),// default to today() / now()
    'EndDate'=>strftime(DataLoadAPI::$DateFormat),// default to today() / now()
    'CurrentYear'=>'2010',
    'PreviouslyInsuredInd'=>true,
    'SecondCarQuotationInd'=>false );

    $Driver= array(
    'IrishDriverInfo'=>array( 'PRN'=>'0', 'RelationshipToProposer'=>'Z', 'Title'=>'005', 'Forename'=>'Test',
    'Surname'=>'Proposer', 'Sex'=>'M', 'MaritalStatus'=>'M', 'LicenceType'=>'I', 'LicenceCountry'=>'IE',
    'ProsecutionPending'=>false, 'LicenceRestrictionInd'=>false, 'QualificationsInd'=>false,
    'NonMotoringConviction'=>false, 'PrevRefusedCover'=>false, 'OtherVehicleOwned'=>false,
    'PrevRestrictiveTerms'=>false, 'RegisteredDisabled'=>false, 'ClaimsIndicator'=>false,
    'PenaltyPointsIndicator'=>false, 'ConvictionsInd'=>false, 'MedicalConditionsInd'=>false,
    'ResidentOutsideIreland'=>false, 'PermResident'=>true,'NonDrinker'=>true, 'TempAdditionalDriver'=>false,
    'DateOfBirth'=>'1977-12-18', 'IrelandResidencyDate'=>'1977-12-18',
    'IrelandLicenceDate'=>'2000-12-18', 'NameddriverNCDClaimedYears'=>6,
    'ResidentWithProposer'=>false, 'FullTimeUseOfOtherCar'=>false,
    'IsResidentWithProposer'=>false, 'PrevImposedTerms'=>false,
    'Occupation'=>
      array( 'IrishOccupationInfo'=>
        array('FullTimeEmployment'=>true,
        'OccupationCode'=>'AAC',
        'EmployersBusiness'=>'001',
        'EmploymentType'=>'E') ),
    'DrivesVehicle'=>
      array('IrishDrivesVehicleInfo'=>
        array( 'VehicleReferenceNumber'=>1, 
          'DrivingFrequency'=>'M', 
          'Use'=>4 ) ) ) );

    $Vehicle= array(
    'IrishVehicleInfo'=> 
      array( 'PRN'=>0, 
        'Value'=>8000, 
        'AnnualMilage'=>7000, 
        'NonStandardAudioValue'=>0,
        'CarPhoneValue'=>0, 
        'NoDriversFullLicence'=>1, 
        'NoOfSeats'=>5, 
        'ManufacturedYear'=>2000, 
        'FirstRegdYear'=>2000,
        'ModelCode'=>'95050358', 
        'ModelName'=>'MondeoLx',
        'KeptAt'=>'HA', 
        'AreaKeptAt'=>'CW00',	
        'CubicCapacity'=>1798, 
        'BodyType'=>5,
        'OvernightLocation'=>8,	
        'AreaRating'=>'CW00', 
        'Owner'=>1, 
        'RegistrationNo'=>'09CW00001', 
        'RegisteredKeeper'=>1,
        'DateManufactured'=>strftime(DataLoadAPI::$DateFormat), // -------- Quick Quote --- [Car Registered Year]
        'DateFirstRegistered'=>strftime(DataLoadAPI::$DateFormat), // -------- Quick Quote --- [Car Registered Year]
        'DatePurchased'=>strftime(DataLoadAPI::$DateFormat),  // -------- Quick Quote --- [Car Registered Year]
        'ModifiedInd'=>false, 
        'IrelandRegistered'=>true,
        'Imported'=>false, 
        'SecurityDeviceInd'=>true, 
        'TrailerInd'=>false, 
        'SecondCarInd'=>false,
        'TemporaryAddVehicle'=>false, 
        'TemporarySubInd'=>false, 
        'LeftOrRightHandDrive'=>82, 
        'ReferenceNumber'=>0,
        'Security'=>array( 'Type'=>1001,  ),
        'Uses'=>array(
          'Code'=>4,
        ),
        'DrivenBy'=>array(
          'IrishDrivenByInfo'=>array(
            'DriverReferenceNumber'=>1,
            'DrivingFrequency'=>'M',
          ),
        ),
        'VehicleType'=>0,
      ),
    );

    $Cover= array(
      'IrishCoverInfo'=>array(
        'Code'=>'01',
        'PeriodUnits'=>2,
        'Period'=>12,
        'CertificateNumber'=>0,
        'StartTime'=>'000001',
        'StartDate'=>strftime(DataLoadAPI::$DateFormat),//'2007-12-19T16:34:45.6406250-00:00', // --------- now()
        'ExpiryDate'=>strftime( DataLoadAPI::$DateFormat, Util::UnixDateTimeAddDay(time(),365) ),// '2008-12-18T00:00:00.0000000-00:00',  // --------- now() + 1 year
        'RequiredDrivers'=>1,
        'VehicleRefNo'=>1,
        'TotalTempMTA'=>0,
        'TotalTempAddDriver'=>0,
        'TotalTempAddVehicle'=>0,
        'TotalTempSub'=>0,
        'VoluntaryExcess'=>100,
        'WindscreenLimit'=>0,
      ),
    );

    $Intermediary= array(
      'Name'=>'RE0098',
      'Number'=>0,
    );


    $DiscountInfo= array(
      'IsWebQuote'=>false,
      'WebDiscountPercentage'=>0,
    );

    $RiskData= array( 'Proposer'=>$Proposer, 'Policy'=>$Policy, 'Driver'=>$Driver, 'Vehicle'=>$Vehicle, 'Cover'=>$Cover, 'Intermediary'=>$Intermediary, 'DiscountInfo'=>$DiscountInfo );
    
    
    
    //$EnableValidation= 'true';
    $TimeTravelDate= array( "Day"=>strftime("%d"), "Month"=>strftime("%m"), "Year"=>strftime("%Y"),
    "Hour"=>strftime("%H"), "Minute"=>strftime("%M"), "Second"=>strftime("%S"), "TimeTravelKey"=>"1234234" );

    //echo '$RiskData::<pre>'.print_r($RiskData,true).'</pre>';
    //echo '$EnableValidation::<pre>'.print_r($EnableValidation,true).'</pre>';
    //echo '$TimeTravelDate::<pre>'.print_r($TimeTravelDate,true).'</pre>';

    $Prem=array(
      'PremiumOverrideAmt'=>8000,
      'TotalPremium'=>8000,
      'PremiumBeforeLevy'=>8000,
      'PremiumAfterLevy'=>8000,
      'LevyPrecentage'=>3,
    );
    return array('RiskData'=>$RiskData, 'Prem'=> $Prem);
  }

  public static function getCarManufacturer($SelectLabelValue= '', $SelectLabelText= '') {
    $ResArray= array();
    if ( !Util::isDeveloperComp() ) {
      DataLoadAPI::InitAPI();
      $manufacturers = DataLoadAPI::$ClientRef->__soapCall('GetVehicleManufacturer', array());
      foreach($manufacturers->GetVehicleManufacturerResult->LookupItem as $item) {
        if ( empty($item->Code) or empty($item->Description) ) continue;
        $ResArray[$item->Code]= $item->Description;
      }
    } else {
      $ResArray['test']= 'test';
    }
    if ( !empty($SelectLabelText) ) {
      $ResArray= Util::SetArrayHeader( array($SelectLabelValue=>$SelectLabelText), $ResArray );
    }
    return $ResArray;
  }

  public static function getCarModel( $Manufacturer, $SelectLabelValue= '', $SelectLabelText= '') {
    $ResArray= array();
    if ( !Util::isDeveloperComp() ) {
      DataLoadAPI::InitAPI();
      error_reporting(E_ALL);
      ini_set("display_errors", 1);
      //var_dump(DataLoadAPI::$ClientRef->__getFunctions());
      //$CarModels = DataLoadAPI::$ClientRef->GetVehicleModel(array('Manufacturer'=>'AUDI'));
      $CarModels = DataLoadAPI::$ClientRef->GetVehicleModel(array('Manufacturer'=>$Manufacturer));


      foreach($CarModels->GetVehicleModelResult->LookupItem as $item) {
        //echo '$item::'.var_dump($item).'<br>';
        //if ( empty($item->Code) or empty($item->Description) ) continue;
        $ResArray[]= array( 'id'=>$item->Description, 'name'=>$item->Description);
      }
    } else {
      $ResArray[]= array( 'id'=>'test', 'name'=>'test');
    }
    if ( !empty($SelectLabelText) ) {
      $ResArray= Util::SetArrayHeader( array($SelectLabelValue=>$SelectLabelText), $ResArray );
    }
    return $ResArray;
  }

  public static function getVehicleFilter($Manufacturer, $CarModel, $EngineCapacity, $YearOfManufacture, $SelectLabelValue= '', $SelectLabelText= '') {
    $ResArray= array();
    if ( !Util::isDeveloperComp() ) {
      DataLoadAPI::InitAPI();
      error_reporting(E_ALL);
      ini_set("display_errors", 1);
      //$Manufacturer= 'AIXAM';
      //$CarModel= '400';

      //$EngineCapacity= '1.5';
      $FuelType= 'Petrol'; //'92'; //'95';
      //$YearOfManufacture= 1950;
      $VehicleFilter= array( 'VehicleFilter'=> array('Manufacturer'=>$Manufacturer, 'Model'=> $CarModel,
      'EngineCapacity'=> $EngineCapacity/*, "FuelType"=>$FuelType*/, "YearOfManufacture"=>$YearOfManufacture
      ) );
      //echo '<br>$VehicleFilter::<pre>'.print_r($VehicleFilter,true).'</pre>';
      $ModelDetails = DataLoadAPI::$ClientRef->GetVehicleModelDetail( $VehicleFilter );


      //DataLoadAPI::$ClientRef = SoapClient("some.wsdl", array('trace' => 1));
      //$result = DataLoadAPI::$ClientRef->SomeFunction();
      //echo "getLastRequest():\n" . DataLoadAPI::$ClientRef->__getLastRequest() . "\n.LAST REQUSET DONE";

      //echo '<br>$ModelDetails::<pre>'.print_r($ModelDetails,true).'</pre>';

      $I=0;
      foreach($ModelDetails->GetVehicleModelDetailResult->VehicleLookupItem as $item) {
        //echo "In foreach(ModelDetails-> getLastRequest():\n" . DataLoadAPI::$ClientRef->__getLastRequest() . "\n.LAST REQUSET DONE";
        $I++;
        $ResArray[]= array( 'id'=>$item->Code, 'name'=>$item->Description);
        if($I==10) break;
      }
    } else {
      $ResArray[]= array( 'id'=>'test', 'name'=>'test');
    }

    if ( !empty($SelectLabelText) ) {
      $ResArray= Util::SetArrayHeader( array($SelectLabelValue=>$SelectLabelText), $ResArray );
    }
    return $ResArray;
  }

  public static function getcarList($Manufacturer, $CarModel, $SelectLabelValue= '', $SelectLabelText= '') {
    $ResArray= array();
    if ( !Util::isDeveloperComp() ) {
      DataLoadAPI::InitAPI();
      error_reporting(E_ALL);
      ini_set("display_errors", 1);
      $ModelDetails = DataLoadAPI::$ClientRef->GetVehicleModelDetail(array('Manufacturer'=>$Manufacturer, 'Model'=> $CarModel));

      $I=0;
      foreach($ModelDetails->GetVehicleModelDetailResult->VehicleLookupItem as $item) {
        $I++;
        $ResArray[]= array( 'id'=>$item->Code, 'name'=>$item->Description);
        if($I==10) break;
      }
    } else {
      $ResArray[]= array( 'id'=>'test', 'name'=>'test');
    }

    if ( !empty($SelectLabelText) ) {
      $ResArray= Util::SetArrayHeader( array($SelectLabelValue=>$SelectLabelText), $ResArray );
    }
    return $ResArray;
  }


  public static function getAreasApiValues( $SelectLabelValue= '', $SelectLabelText= '' ) {
    $ResArray= array(
    "CX01"=>"Adrigole", "CX02"=>"Aghabullogue", "CX03"=>"Aghaville", "MH01"=>"Agher", "CX04"=>"Allihies", "LK02"=>"Annacotty", "LH25"=>"Annagassan", "WW01"=>"Annamoe", "MH64"=>"Ardanew", "MH02"=>"Ardcath", "LH09"=>"Ardee", "CX05"=>"Ardgroom", "WW34"=>"Arklow", "DL01"=>"Arranmore Island", "MH03"=>"Ashbourne", "WW02"=>"Ashford", "MH54"=>"Athboy", "MH04"=>"Athcarne", "CX5B"=>"Atherla", "LK03"=>"Athlunkard", "KE35"=>"Athy", "WW35"=>"Aughrim", "WW36"=>"Avoca", "MH05"=>"Baconstown", "LK04"=>"Ballinacurra", "WW03"=>"Ballinalea", "CX76"=>"Ballincollig", "CX5C"=>"Ballineen", "CX06"=>"Ballingeary", "CX77"=>"Ballinhassig", "MH65"=>"Ballinlough", "CX78"=>"Ballinlough", "CX07"=>"Ballinscarty", "CX79"=>"Ballintemple", "CX80"=>"Ballinure", "MH55"=>"Ballivor", "GX02"=>"Ballybane", "WD23"=>"Ballybeg", "KE01"=>"Ballybrack", "WD02"=>"Ballybricken", "GX03"=>"Ballybrit", "CX5D"=>"Ballycotton", "WW04"=>"Ballycullen", "CX08"=>"Ballydehob", "CX09"=>"Ballydesmond", "WW05"=>"Ballyduff", "CX5E"=>"Ballygarvan", "LK74"=>"Ballykeeffe", "WW06"=>"Ballyknockan", "WD03"=>"Ballymaclode", "CX5F"=>"Ballymacoda", "LH16"=>"Ballymakenny", "KE02"=>"Ballymore Eustace", "WD04"=>"Ballynakill", "WD05"=>"Ballynaneashagh", "LK56"=>"Ballynanty", "LK05"=>"Ballynantybeg", "MH66"=>"Ballynare", "CX3A"=>"Ballyphehane", "LK06"=>"Ballysheedy", "LK07"=>"Ballysimon", "KE36"=>"Ballytore", "WD06"=>"Ballytruckle", "CX81"=>"Ballyvolane", "CX10"=>"Ballyvourney", "MH67"=>"Balrath", "CX11"=>"Baltimore", "WW37"=>"Baltinglass", "LH17"=>"Baltray", "CX6A"=>"Bandon", "CX12"=>"Banteer", "CX13"=>"Bantry", "GX04"=>"Barna", "CX82"=>"Barnavara", "MH68"=>"Batterstown", "LK71"=>"Bawnmore", "CX14"=>"Bealnablath", "MH69"=>"Bective", "LK08"=>"Belfield", "MH70"=>"Bellwestown", "CX9G"=>"Bere Island", "MH06"=>"Bettystown", "WD07"=>"Bilberry", "CX83"=>"Bishopstown", "MH07"=>"Blackbull", "CX84"=>"Blackpool", "LH01"=>"Blackrock", "CX3D"=>"Blackrock", "CX85"=>"Blarney", "WW07"=>"Blessington", "CX15"=>"Blueford", "LK09"=>"Boherbouy", "CX16"=>"Boherboy", "KE03"=>"Boherhole", "MH71"=>"Bohermeen", "KE04"=>"Boleybeg", "MH72"=>"Boyerstown", "KE05"=>"Brannockstown", "WW08"=>"Bray", "WW09"=>"Brockagh", "CX17"=>"Butlerstown", "CX6B"=>"Buttevant", "LK10"=>"Caherdavin", "CX18"=>"Cahermore", "TS01"=>"Cahir", "CX9B"=>"Cape Clear Island", "CX19"=>"Cappen", "KE37"=>"Carbury", "MH73"=>"Carlanstown", "LH02"=>"Carlingford", "CW00"=>"Carlow", "MH74"=>"Carnacross", "WW38"=>"Carnew", "GX06"=>"Carnmore", "KE06"=>"Carragh", "CX20"=>"Carrigacooleen", "CX86"=>"Carrigaline", "CX21"=>"Carriganimmy", "GX37"=>"Carrowbrowne", "GX07"=>"Carrowmore", "LH10"=>"Castlebellingham", "KE38"=>"Castledermot", "GX08"=>"Castlegar", "MH75"=>"Castlejordan", "CX6C"=>"Castlemartyr", "MH56"=>"Castletown", "CX22"=>"Castletownbere", "CX23"=>"Castletownshend", "LK11"=>"Castletroy", "CN00"=>"Cavan", "KE07"=>"Celbridge", "LH26"=>"Chanonrock", "CX24"=>"Charleville", "GX09"=>"Claddagh", "KE08"=>"Clane", "CE00"=>"Clare", "CE02"=>"Clare - OTL", "MO01"=>"Clare Island", "GX10"=>"Claregalway", "LK12"=>"Clarina", "WD08"=>"Cleaboy", "MH76"=>"Cloghbrack", "CX87"=>"Clogheen", "LH11"=>"Clogherhead", "CX25"=>"Clonakilty", "MH08"=>"Clonalvy", "MH77"=>"Clonard", "KE09"=>"Cloncurry", "LK13"=>"Clondrinagh", "MH09"=>"Clonee", "MH78"=>"Cloneycavan", "CX26"=>"Cloonbanin", "GX11"=>"Cloonboo", "LK14"=>"Cloonlara", "GX12"=>"Clybaun", "DXCO"=>"Co. Dublin - OTL", "CX27"=>"Coachford", "CX6D"=>"Cobh", "LH12"=>"Collon", "CX28"=>"Connonagh", "GX13"=>"Coolagh", "LK15"=>"Coonagh", "LK60"=>"Corbally", "CE01"=>"Corbally", "KE10"=>"Corbally", "LK16"=>"Corcanree", "KE11"=>"Corduff", "CX00"=>"Cork", "CX29"=>"Cork City - OTL", "CX9A"=>"Cork Co. - OTL", "MH79"=>"Cortown", "CX30"=>"Courtmacsherry", "LK17"=>"Cratloe", "CX31"=>"Crookhaven", "CX32"=>"Crookstown", "MH10"=>"Cross Keys", "MH80"=>"Crossakeel", "MH11"=>"Culmullin", "MH12"=>"Curragha", "GX14"=>"Dangan", "CX33"=>"Deelish", "WW10"=>"Delgany", "MH13"=>"Donacarney", "KE12"=>"Donadea", "MH14"=>"Donaghmore", "MH81"=>"Donaghpatrick", "WW39"=>"Donard", "DL00"=>"Donegal", "DL03"=>"Donegal - OTL", "CX6E"=>"Doneraile", "CX88"=>"Donnybrook", "MH15"=>"Donore", "LK19"=>"Donoughmore", "LK20"=>"Dooradoyle", "CX4D"=>"Douglas", "LH40"=>"Dowdallshill", "CX34"=>"Drimoleague", "CX35"=>"Drinagh", "LH13"=>"Drogheda", "LH18"=>"Dromin", "LH27"=>"Dromiskin", "LK21"=>"Drumbanna", "LH19"=>"Drumcar", "MH82"=>"Drumconrath", "MH83"=>"Drumone", "MH16"=>"Drumree", "DX00"=>"Dublin", "DX01"=>"Dublin 01", "DX02"=>"Dublin 02", "DX03"=>"Dublin 03", "DX04"=>"Dublin 04", "DX05"=>"Dublin 05", "DX06"=>"Dublin 06", "DX07"=>"Dublin 07", "DX08"=>"Dublin 08", "DX09"=>"Dublin 09", "DX10"=>"Dublin 10", "DX11"=>"Dublin 11", "DX12"=>"Dublin 12", "DX13"=>"Dublin 13", "DX14"=>"Dublin 14", "DX15"=>"Dublin 15", "DX16"=>"Dublin 16", "DX17"=>"Dublin 17", "DX18"=>"Dublin 18", "DX19"=>"Dublin 19", "DX20"=>"Dublin 20", "DX21"=>"Dublin 21", "DX22"=>"Dublin 22", "DX23"=>"Dublin 23", "DX24"=>"Dublin 24", "DX6W"=>"Dublin 6W", "MH17"=>"Duleek", "MH18"=>"Dunboyne", "CX36"=>"Duncannon bridge", "LH03"=>"Dundalk", "MH84"=>"Dunderry", "KE13"=>"Dunfiorth", "WW40"=>"Dunlavin", "LH14"=>"Dunleer", "CX37"=>"Dunmanway", "MH19"=>"Dunsany", "MH20"=>"Dunshaughlin", "CX38"=>"Durrus", "KE14"=>"Eadestown", "MH52"=>"Enfield", "CX39"=>"Enniskean", "WW11"=>"Enniskerry", "CX40"=>"Eyeries", "CX89"=>"Fairhill", "CX3B"=>"Farranferris", "CX90"=>"Farranree", "LK61"=>"Farranshone", "WD09"=>"Farranshoneen", "CX6F"=>"Fermoy", "WD10"=>"Ferrybank", "MH85"=>"Fordstown", "MH21"=>"Fourknocks", "MH22"=>"Galtrim", "LK62"=>"Galvone", "GX00"=>"Galway", "GX01"=>"Galway City - OTL", "GX38"=>"Galway Co. - OTL", "MH23"=>"Garadice", "CX43"=>"Garahies", "MH24"=>"Garlow Cross", "CX41"=>"Garnish", "CX42"=>"Garrane", "MH50"=>"Garristown", "LK23"=>"Garryowen", "LK24"=>"Georgian Village", "CX3C"=>"Gillabbey", "CX44"=>"Glandore", "CX4E"=>"Glanmire", "CX91"=>"Glasheen", "WW12"=>"Glen of the Downs", "CX92"=>"Glenbrook", "WW32"=>"Glencree", "WW13"=>"Glendarragh", "WW41"=>"Glenealy", "CX45"=>"Glengarriff", "CX93"=>"Glounthaune", "CX46"=>"Goleen", "MH25"=>"Gormanstown", "GX15"=>"Gortatleva", "WD11"=>"Gracedieu", "WW14"=>"Granabeg", "LH04"=>"Grange", "MH26"=>"Grange", "LH20"=>"Grangebellew", "MH86"=>"Grangegeeth", "WW15"=>"Gravale", "MH27"=>"Greenanstown", "LH05"=>"Greenore", "WW16"=>"Greystones", "LK64"=>"Greystones", "CX94"=>"Gurranabraher", "LH06"=>"Gyles Quay", "CX9E"=>"Heir Island", "LK65"=>"Highfield", "CX95"=>"Hollyhill", "WW17"=>"Hollywood", "CX96"=>"Hop Island", "CX47"=>"Inchigeelagh", "GX41"=>"Inis Meain", "GX40"=>"Inis Mor", "GX42"=>"Inis Oirr", "MO02"=>"Inishbiggle", "GX39"=>"Inishbofin", "GX43"=>"Inishturk", "MH28"=>"Innfield", "LK25"=>"Irishtown", "LK26"=>"Janesboro", "KE15"=>"Johnstown", "MH29"=>"Julianstown", "CX48"=>"Kanturk", "CX49"=>"Kealkill", "MH57"=>"Kells", "MH30"=>"Kentstown", "KY00"=>"Kerry", "MH87"=>"Kilallen", "WD12"=>"Kilbarry", "MH31"=>"Kilbride", "WW18"=>"Kilbride", "KE16"=>"Kilcock", "MH32"=>"Kilcock", "CX50"=>"Kilcoe", "WW19"=>"Kilcoole", "CX51"=>"Kilcrohane", "KE39"=>"Kilcullen", "LH28"=>"Kilcurly", "LH29"=>"Kilcurry", "KE00"=>"Kildare", "KE45"=>"Kildare - OTL", "KE40"=>"Kildare Town", "LK27"=>"Kileely", "WW20"=>"Kilishey", "KK00"=>"Kilkenny", "KE17"=>"Kill", "CX7A"=>"Killeagh", "CX52"=>"Killeenleigh", "LK28"=>"Killonan", "WW21"=>"Kilmacanogue", "WW22"=>"Kilmalin", "LK76"=>"Kilmallock", "MH33"=>"Kilmessan", "CX53"=>"Kilmichael", "MH34"=>"Kilmore", "CX54"=>"Kilpatrick", "WW23"=>"Kilpedder", "CX55"=>"Kilronane", "LH15"=>"Kilsaran", "KE18"=>"Kilteel", "WW42"=>"Kiltegan", "KE19"=>"Kilwoghan", "LK29"=>"Kings Island", "WD13"=>"Kingsmeadow", "CX7C"=>"Kinsale", "LK30"=>"Knockalisheen", "CX56"=>"Knockatooan", "LH30"=>"Knockbridge", "GX30"=>"Knocknacarra", "CX57"=>"Knocknagree", "CX97"=>"Knocknaheeny", "CX3E"=>"Knockrea", "LK31"=>"Landsdowne", "LS00"=>"Laois", "WW24"=>"Laragh", "WD14"=>"Larchville", "MH35"=>"Laytown", "CX58"=>"Leap", "LM00"=>"Leitrim", "KE20"=>"Leixlip", "GX16"=>"Lenaboy", "LK00"=>"Limerick", "LK01"=>"Limerick City - OTL", "LK75"=>"Limerick Co. - OTL", "WD15"=>"Lisduggan", "CX98"=>"Little Island", "CX9F"=>"Long Island", "LD00"=>"Longford", "CX99"=>"Lota", "LH00"=>"Louth", "LH44"=>"Louth OTL", "DXC3"=>"Lucan", "LH08"=>"Lurgangreen", "CX59"=>"Macroom", "CX1A"=>"Mahon", "KE21"=>"Mainham", "DXC1"=>"Malahide", "CX7D"=>"Mallow", "WW33"=>"Manorkilbride", "LH31"=>"Mansfieldtown", "CX3F"=>"Mardyke", "CX1B"=>"Mayfield", "KE22"=>"Maynooth", "MO00"=>"Mayo", "MO03"=>"Mayo - OTL", "LK32"=>"Mayorstone", "MH00"=>"Meath", "MH63"=>"Meath - OTL", "LH37"=>"Mellifont", "GX36"=>"Menlo", "GX32"=>"Mervue", "CX7E"=>"Midleton", "CX60"=>"Millstreet", "CX7F"=>"Mitchelstown", "MN00"=>"Monaghan", "LH38"=>"Monasterboice", "KE41"=>"Monasterevin", "CX1C"=>"Monkstown", "CX1D"=>"Montenotte", "KE42"=>"Moone", "WD16"=>"Mount Sion", "LK33"=>"Moylish", "MH36"=>"Moynalvy", "LK34"=>"Moyross", "KE23"=>"Mucklon", "MH37"=>"Mullagh", "LK35"=>"Mungret", "GX18"=>"Muroogh", "KE24"=>"Naas", "MH58"=>"Navan", "CX61"=>"Nead", "CX1E"=>"New Inn", "KE34"=>"Newbridge", "GX33"=>"Newcastle", "WW25"=>"Newcastle", "CX62"=>"Newmarket", "KE25"=>"Newtown", "WD17"=>"Newtown", "WW26"=>"Newtown Mount Kennedy", "MH59"=>"Nobber", "CX63"=>"North Ring", "MH38"=>"Nuttstown", "OY00"=>"Offaly", "OY02"=>"Offaly - OTL", "MH60"=>"Oldcastle", "MH51"=>"Oldtown", "LH32"=>"Omeath", "GX19"=>"Oranmore", "LK37"=>"Palmerstown", "LK38"=>"Parteen", "CX1F"=>"Passagewest", "LK39"=>"Patrickswell", "LK40"=>"Pennywell", "MH39"=>"Pikecornor", "WD18"=>"Poleberry", "GX21"=>"Poolnarooma", "LH21"=>"Port", "DXC2"=>"Portmarnock", "CX4A"=>"Pouladuff", "LK41"=>"Prospect", "KE26"=>"Prosperous", "LK42"=>"Raheen", "GX22"=>"Rahoon", "MH40"=>"Rath", "LK43"=>"Rathbane", "CX64"=>"Rathcool", "LH41"=>"Rathcor", "CX8A"=>"Rathcormac", "KE43"=>"Rathdangan", "WW43"=>"Rathdrum", "KE27"=>"Rathernan", "MH41"=>"Rathfeigh", "KE28"=>"Rathmore", "MH42"=>"Rathmoylon", "WW44"=>"Rathnew", "MH53"=>"Rathoath", "MH43"=>"Ratoath", "LH33"=>"Reaghstown", "WW45"=>"Redcross", "CX65"=>"Reenascreena", "GX23"=>"Renmore", "LK44"=>"Rhebogue", "CX8B"=>"Ringaskiddy", "CX2A"=>"Riverstown", "WD19"=>"Roanmore", "KE29"=>"Robertstown", "CX2B"=>"Rochestown", "GX34"=>"Rockbarton", "LH42"=>"Roodstown", "RN00"=>"Roscommon", "MH44"=>"Ross", "CX66"=>"Rosscarbery", "WW27"=>"Roundwood", "LK45"=>"Roxboro", "CX67"=>"Rylane Cross", "KE30"=>"Sallins", "WW28"=>"Sally Gap", "CX2C"=>"Sallybrook", "GX24"=>"Salthill", "CX70"=>"Schull", "CX4B"=>"Shanakiel", "CX68"=>"Shanlaragh", "LH39"=>"Shanlis", "LK46"=>"Shannon Banks", "GX35"=>"Shantalla", "GX25"=>"Shantallow", "LK47"=>"Shelbourne", "CX9C"=>"Sherkin Island", "WW46"=>"Shillelagh", "WD20"=>"Shortcoure", "LK48"=>"Singland", "CX69"=>"Skibbereen", "MH45"=>"Skreen", "MH61"=>"Slane", "WD21"=>"Slievekeale", "SO00"=>"Sligo", "CX71"=>"Snave Bridge", "LK50"=>"South Hill", "CX2D"=>"Springmount", "LH22"=>"Stabannan", "MH49"=>"Stamullin", "KE31"=>"Staplestown", "KE32"=>"Straffan", "MH46"=>"Summerhill", "KE44"=>"Suncroft", "CX2E"=>"Sundays Well", "DXC4"=>"Swords", "LH34"=>"Tallanstown", "GX29"=>"Taylors Hill", "LH23"=>"Termonfeckin", "GX26"=>"Terryland", "LH35"=>"The Bush", "LK51"=>"Thomond Gate", "WD22"=>"Ticor", "KE33"=>"Timahoe", "CX72"=>"Timoleague", "WW47"=>"Tinahealy", "TN00"=>"Tipperary North", "TS00"=>"Tipperary South", "TS02"=>"Tipperary South - OTL", "CX2F"=>"Tivoli", "OY01"=>"Togher", "WW29"=>"Togher", "CX73"=>"Togher", "LH43"=>"Togher (Louth)", "CX74"=>"Toormore", "DL02"=>"Tory Island", "MH62"=>"Trim", "LH24"=>"Tullyallen", "CX4C"=>"Turners Cross", "MH47"=>"Tylas", "CX75"=>"Union Hall", "WW30"=>"Valleymount", "MH48"=>"Walterstown", "WD00"=>"Waterford", "WD01"=>"Waterford City - OTL", "WD24"=>"Waterford Co. - OTL", "CX8C"=>"Watergrasshill", "LK52"=>"Westbury", "WH00"=>"Westmeath", "LK53"=>"Weston", "WX00"=>"Wexford", "CX9D"=>"Whiddy Island", "CX8D"=>"Whitegate", "LH36"=>"Whites Town", "WW00"=>"Wicklow", "WW50"=>"Wicklow - OTL", "WW31"=>"Wicklow Gap", "WW48"=>"Wicklow Town", "CX8E"=>"Wilton", "WW49"=>"Woodenbridge", "LK54"=>"Woodview", "CX8F"=>"Youghal",     );
    if ( !empty($SelectLabelText) ) {
      $ResArray= Util::SetArrayHeader( array($SelectLabelValue=>$SelectLabelText), $ResArray );
    }
    return $ResArray;
  }
  public static function getAreas( $SelectLabelValue= '', $SelectLabelText= '' ) {
    $ResArray= array(
    "8"=>"County Carlow",
    "3"=>"County Cavan",
    "21"=>"County Clare",
    "17"=>"County Cork",
    "14"=>"County Donegal",
    "12"=>"County Dublin",
    "11"=>"County Galway",
    "4"=>"County Kerry",
    "25"=>"County Kildare",
    "6"=>"County Kilkenny",
    "1"=>"County Laois",
    "7"=>"County Leitrim",
    "9"=>"County Limerick",
    "13"=>"County Longford",
    "18"=>"County Louth",
    "26"=>"County Mayo",
    "22"=>"County Meath",
    "20"=>"County Monaghan",
    "5"=>"County Offaly",
    "2"=>"County Roscommon",
    "10"=>"County Sligo",
    "137"=>"County Tipperary North",
    "23"=>"County Tipperary South",
    "16"=>"County Waterford",
    "24"=>"County Westmeath",
    "15"=>"County Wexford",
    "19"=>"County Wicklow"
    );
    if ( !empty($SelectLabelText) ) {
      $ResArray= Util::SetArrayHeader( array($SelectLabelValue=>$SelectLabelText), $ResArray );
    }
    return $ResArray;
  }

  public static function getTownsOfArea($AreaId, $SelectLabelValue= '', $SelectLabelText= '') {
    if ( (int)$AreaId== 8) { //"8"=>"County Carlow",
      $ResArray= array (
      "9512"=>"Other",
      );
    }
    if ( (int)$AreaId== 3) { // "3"=>"County Cavan",
      $ResArray= array (
      "9517"=>"Other",
      );
    }
    if ( (int)$AreaId==21 ) {
      $ResArray= array ( // "21"=>"County Clare",
      "3520"=>"Corbally",
      "9664"=>"Other",
      );
    }
    if ( (int)$AreaId== 17) { // "17"=>"County Cork"
      $ResArray= array (
      "3637"=>"Adrigole",      "3638"=>"Aghabullogue",      "3641"=>"Aghaville",      "3645"=>"Allihies",      "3649"=>"Ardgroom",      "9416"=>"Atherla",      "3659"=>"Ballincollig",      "3665"=>"Ballineen",      "3666"=>"Ballingeary",      "3668"=>"Ballinhassig",      "9417"=>"Ballinlough",      "9418"=>"Ballinscarty",      "9415"=>"Ballintemple",      "9413"=>"Ballinure",      "3672"=>"Ballycotton",      "3674"=>"Ballydehob",      "3675"=>"Ballydesmond",      "3677"=>"Ballygarvan",      "3684"=>"Ballymacoda",      "3694"=>"Ballyphehane",      "9414"=>"Ballyvolane",      "3699"=>"Ballyvourney",      "3700"=>"Baltimore",      "3701"=>"Bandon",      "3703"=>"Banteer",      "3704"=>"Bantry",      "9425"=>"Barnavara",      "3707"=>"Bealnablath",      "3714"=>"Bere Island",      "9424"=>"Bishopstown",      "9423"=>"Blackpool",      "9421"=>"Blackrock",      "3716"=>"Blarney",      "9419"=>"Blueford",      "9420"=>"Boherboy",      "3721"=>"Butlerstown",      "3722"=>"Buttevant",      "3725"=>"Cahermore",      "9422"=>"Cape Clear Island",      "9452"=>"Cappen",      "9472"=>"Carrigacooleen",      "3730"=>"Carrigaline",      "9471"=>"Carriganimmy",      "3741"=>"Castlemartyr",      "3746"=>"Castletownbere",      "3749"=>"Castletownshend",      "3751"=>"Charleville",      "3756"=>"Clogheen",      "3758"=>"Clonakilty",      "3761"=>"Cloonbanin",      "3763"=>"Coachford",      "3764"=>"Cobh",      "9468"=>"Connonagh",      "3771"=>"Courtmacsherry",      "3774"=>"Crookhaven",      "3775"=>"Crookstown",      "9478"=>"Deelish",      "3788"=>"Doneraile",      "3789"=>"Donnybrook",      "3791"=>"Douglas",      "3792"=>"Drimoleague",      "3793"=>"Drinagh",      "9479"=>"Duncannon bridge",      "3800"=>"Dunmanway",      "3802"=>"Durrus",      "3804"=>"Enniskean",      "3806"=>"Eyeries",      "9473"=>"Fairhill",      "9474"=>"Farranferris",      "9477"=>"Farranree",      "3813"=>"Fermoy",      "9475"=>"Garahies",      "9476"=>"Garnish",      "9458"=>"Garrane",      "9459"=>"Gillabbey",      "3823"=>"Glandore",      "3825"=>"Glanmire",      "9460"=>"Glasheen",      "3830"=>"Glenbrook",      "3831"=>"Glengarriff",      "3833"=>"Glounthaune",      "3834"=>"Goleen",      "9455"=>"Gurranabraher",      "9456"=>"Heir Island",      "9453"=>"Hollyhill",      "9454"=>"Hop Island",      "3842"=>"Inchigeelagh",      "3846"=>"Kanturk",      "3848"=>"Kealkill",      "9457"=>"Kilcoe",      "3855"=>"Kilcrohane",      "3862"=>"Killeagh",      "9465"=>"Killeenleigh",      "3867"=>"Kilmichael",      "9464"=>"Kilpatrick",      "9467"=>"Kilronane",      "3870"=>"Kinsale",      "9466"=>"Knockatooan",      "3875"=>"Knocknagree",      "9463"=>"Knocknaheeny",      "9461"=>"Knockrea",      "3879"=>"Leap",      "3887"=>"Little Island",      "3889"=>"Long Island",      "3890"=>"Lota",      "3892"=>"Macroom",      "9462"=>"Mahon",      "3893"=>"Mallow",      "3895"=>"Mardyke",      "9648"=>"Mayfield",      "3899"=>"Midleton",      "3901"=>"Millstreet",      "3903"=>"Mitchelstown",      "3906"=>"Monkstown",      "9651"=>"Montenotte",      "9649"=>"Nead",      "9650"=>"New Inn",      "3913"=>"Newmarket",      "9645"=>"North Ring",      "9643"=>"Passagewest",      "9644"=>"Pouladuff",      "3927"=>"Rathcool",      "3928"=>"Rathcormac",      "9647"=>"Reenascreena",      "3935"=>"Ringaskiddy",      "3937"=>"Riverstown",      "3940"=>"Rochestown",      "3945"=>"Rosscarbery",      "9656"=>"Rylane Cross",      "3951"=>"Sallybrook",      "3952"=>"Schull",      "9657"=>"Shanakiel",      "3957"=>"Shanlaragh",      "3958"=>"Sherkin Island",      "3959"=>"Skibbereen",      "9653"=>"Snave Bridge",      "3960"=>"Springmount",      "9652"=>"Sundays Well",      "3965"=>"Timoleague",      "9655"=>"Tivoli",      "3966"=>"Togher",      "3968"=>"Toormore",      "9654"=>"Turners Cross",      "3974"=>"Union Hall",      "3979"=>"Watergrasshill",      "3981"=>"Whiddy Island",      "3983"=>"Whitegate",      "9638"=>"Wilton",      "3986"=>"Youghal",      "9646"=>"Other"      );
    }
    if ( (int)$AreaId== 14 ) { // "14"=>"County Donegal",
      $ResArray= array (
      "9436"=>"Arranmore Island",
      "4198"=>"Tory Island",
      "9434"=>"Other"
      );
    }
    if ( (int)$AreaId== 12) { // "12"=>"County Dublin",
      $ResArray= array (

      "9553"=>"Dublin 01",      "9550"=>"Dublin 02",      "9551"=>"Dublin 03",      "9545"=>"Dublin 04",      "9539"=>"Dublin 05",      "9540"=>"Dublin 06",      "9537"=>"Dublin 07",      "9538"=>"Dublin 08",      "9543"=>"Dublin 09",      "9544"=>"Dublin 10",      "9541"=>"Dublin 11",      "9542"=>"Dublin 12",      "9554"=>"Dublin 13",      "9562"=>"Dublin 14",      "9563"=>"Dublin 15",      "9560"=>"Dublin 16",      "9561"=>"Dublin 17",      "9566"=>"Dublin 18",      "9567"=>"Dublin 19",      "9564"=>"Dublin 20",      "9565"=>"Dublin 21",      "9559"=>"Dublin 22",      "9556"=>"Dublin 23",      "9557"=>"Dublin 24",      "9555"=>"Dublin 6W",      "4258"=>"Lucan",      "4260"=>"Malahide",      "4268"=>"Portmarnock",      "4286"=>"Swords",      "9558"=>"Other",

      );
    }
    if ( (int)$AreaId== 11) { // "11"=>"County Galway",
      $ResArray= array (
      "9578"=>"Ballybane",      "9576"=>"Ballybrit",      "4327"=>"Barna",      "4344"=>"Carnmore",      "9580"=>"Carrowbrowne",      "9579"=>"Carrowmore",      "4350"=>"Castlegar",      "9575"=>"Claddagh",      "4353"=>"Claregalway",      "9570"=>"Cloonboo",      "9568"=>"Clybaun",      "9569"=>"Coolagh",      "9573"=>"Dangan",      "9572"=>"Gortatleva",      "9581"=>"Inis Meain",      "9590"=>"Inis Mor",      "9591"=>"Inis Oirr",      "4406"=>"Inishbofin",      "4409"=>"Inishturk",      "9594"=>"Knocknacarra",      "9595"=>"Lenaboy",      "9592"=>"Menlo",      "9593"=>"Mervue",      "9589"=>"Muroogh",      "9584"=>"Newcastle",      "4461"=>"Oranmore",      "9583"=>"Poolnarooma",      "9587"=>"Rahoon",      "9588"=>"Renmore",      "9585"=>"Rockbarton",      "9586"=>"Salthill",      "9548"=>"Shantalla",      "9549"=>"Shantallow",      "9546"=>"Taylors Hill",      "9547"=>"Terryland",      "9582"=>"Other",
      );
    }
    if ( (int)$AreaId== 4) { // "4"=>"County Kerry",
      $ResArray= array (
      "9518"=>"Other",
      );
    }

    if ( (int)$AreaId== 25) { //"25"=>"County Kildare",
      $ResArray= array (
      "4641"=>"Athy",      "4643"=>"Ballybrack",      "4644"=>"Ballymore Eustace",      "9634"=>"Ballytore",      "9632"=>"Boherhole",      "9633"=>"Boleybeg",      "4654"=>"Brannockstown",      "4660"=>"Carbury",      "4661"=>"Carragh",      "4662"=>"Castledermot",      "4663"=>"Celbridge",      "4664"=>"Clane",      "4665"=>"Cloncurry",      "4670"=>"Corbally",      "4671"=>"Corduff",      "4677"=>"Donadea",      "9615"=>"Dunfiorth",      "9614"=>"Eadestown",      "4686"=>"Johnstown",      "4688"=>"Kilcock",      "4689"=>"Kilcullen",      "9617"=>"Kildare Town",      "4693"=>"Kill",      "4696"=>"Kilteel",      "4697"=>"Kilwoghan",      "4699"=>"Leixlip",      "4702"=>"Mainham",      "4703"=>"Maynooth",      "4706"=>"Monasterevin",      "4707"=>"Moone",      "4709"=>"Mucklon",      "4710"=>"Naas",      "4712"=>"Newbridge",      "4713"=>"Newtown",      "4717"=>"Prosperous",      "9621"=>"Rathdangan",      "4720"=>"Rathernan",      "4721"=>"Rathmore",      "4722"=>"Robertstown",      "4723"=>"Sallins",      "4724"=>"Staplestown",      "4725"=>"Straffan",      "4726"=>"Suncroft",      "4728"=>"Timahoe",      "9620"=>"Other",
      );
    }
    if ( (int)$AreaId== 6) { //"6"=>"County Kilkenny",
      $ResArray= array (
      "9521"=>"Other",
      );
    }
    if ( (int)$AreaId== 1 ) { // "1"=>"County Laois",
      $ResArray= array (
      "9519"=>"Other",
      );
    }
    if ( (int)$AreaId== 7) { // "7"=>"County Leitrim",
      $ResArray= array (
      "9516"=>"Other",
      );
    }
    if ( (int)$AreaId== 9) { // "9"=>"County Limerick",
      $ResArray= array (
      "4987"=>"Annacotty",      "9510"=>"Athlunkard",      "9511"=>"Ballinacurra",      "9514"=>"Ballykeeffe",      "9515"=>"Ballynanty",      "9513"=>"Ballynantybeg",      "5007"=>"Ballysheedy",      "5008"=>"Ballysimon",      "9532"=>"Bawnmore",      "9533"=>"Belfield",      "9531"=>"Boherbouy",      "5020"=>"Caherdavin",      "5027"=>"Castletroy",      "5029"=>"Clarina",      "9534"=>"Clondrinagh",      "9535"=>"Cloonlara",      "9530"=>"Coonagh",      "9525"=>"Corbally",      "9526"=>"Corcanree",      "9524"=>"Cratloe",      "5036"=>"Donoughmore",      "9528"=>"Dooradoyle",      "9529"=>"Drumbanna",      "9527"=>"Farranshone",      "5049"=>"Galvone",      "9490"=>"Garryowen",      "9491"=>"Georgian Village",      "9488"=>"Greystones",      "9489"=>"Highfield",      "9494"=>"Irishtown",      "9495"=>"Janesboro",      "9492"=>"Kileely",      "9493"=>"Killonan",      "5068"=>"Kilmallock",      "9482"=>"Kings Island",      "9483"=>"Knockalisheen",      "9480"=>"Landsdowne",      "9487"=>"Mayorstone",      "9484"=>"Moylish",      "9485"=>"Moyross",      "5089"=>"Mungret",      "9506"=>"Palmerstown",      "9504"=>"Parteen",      "5098"=>"Patrickswell",      "9508"=>"Pennywell",      "9509"=>"Prospect",      "5100"=>"Raheen",      "9507"=>"Rathbane",      "9503"=>"Rhebogue",      "9498"=>"Roxboro",      "9499"=>"Shannon Banks",      "9496"=>"Shelbourne",      "9497"=>"Singland",      "9501"=>"South Hill",      "9502"=>"Thomond Gate",      "5111"=>"Westbury",      "9500"=>"Weston",      "9536"=>"Woodview",      "9505"=>"Other",
      );
    }
    if ( (int)$AreaId== 13) { // "13"=>"County Longford",
      $ResArray= array (
      "9435"=>"Other",
      );
    }
    if ( (int)$AreaId== 18) { //"18"=>"County Louth",
      $ResArray= array (
      "5164"=>"Annagassan",      "5165"=>"Ardee",      "9637"=>"Ballymakenny",      "5167"=>"Baltray",      "5169"=>"Blackrock",      "5171"=>"Carlingford",      "5172"=>"Castlebellingham",      "9636"=>"Chanonrock",      "5173"=>"Clogherhead",      "5174"=>"Collon",      "9635"=>"Dowdallshill",      "5176"=>"Drogheda",      "5177"=>"Dromin",      "5178"=>"Dromiskin",      "5179"=>"Drumcar",      "5183"=>"Dundalk",      "5184"=>"Dunleer",      "5187"=>"Grange",      "5188"=>"Grangebellew",      "5189"=>"Greenore",      "9642"=>"Gyles Quay",      "5194"=>"Kilcurly",      "5195"=>"Kilcurry",      "5198"=>"Kilsaran",      "5199"=>"Knockbridge",      "5201"=>"Lurgangreen",      "9639"=>"Mansfieldtown",      "9640"=>"Mellifont",      "5203"=>"Monasterboice",      "5208"=>"Omeath",      "9670"=>"Port",      "5210"=>"Rathcor",      "9672"=>"Reaghstown",      "9673"=>"Roodstown",      "5218"=>"Shanlis",      "5220"=>"Stabannan",      "5221"=>"Tallanstown",      "5222"=>"Termonfeckin",      "9669"=>"The Bush",      "9668"=>"Togher (Louth)",      "5224"=>"Tullyallen",      "5226"=>"Whites Town",      "9671"=>"Other",
      );
    }

    if ( (int)$AreaId== 26) { // "26"=>"County Mayo",
      $ResArray= array (
      "5289"=>"Clare Island",      "5330"=>"Inishbiggle",      "9619"=>"Other",
      );
    }
    if ( (int)$AreaId== 22) { //"22"=>"County Meath",
      $ResArray= array (
      "5409"=>"Agher",      "9606"=>"Ardanew",      "5413"=>"Ardcath",      "5415"=>"Ashbourne",      "5417"=>"Athboy",      "9607"=>"Athcarne",      "5419"=>"Baconstown",      "5422"=>"Ballinlough",      "5423"=>"Ballivor",      "9605"=>"Ballynare",      "5426"=>"Balrath",      "5428"=>"Batterstown",      "5430"=>"Bective",      "9601"=>"Bellwestown",      "5432"=>"Bettystown",      "9604"=>"Blackbull",      "9602"=>"Bohermeen",      "9603"=>"Boyerstown",      "5438"=>"Carlanstown",      "9611"=>"Carnacross",      "5440"=>"Castlejordan",      "5442"=>"Castletown",      "9610"=>"Cloghbrack",      "9613"=>"Clonalvy",      "5445"=>"Clonard",      "5446"=>"Clonee",      "9612"=>"Cloneycavan",      "5450"=>"Cortown",      "9608"=>"Cross Keys",      "5452"=>"Crossakeel",      "5454"=>"Culmullin",      "5455"=>"Curragha",      "5457"=>"Donacarney",      "9609"=>"Donaghmore",      "5458"=>"Donaghpatrick",      "5459"=>"Donore",      "5463"=>"Drumconrath",      "5464"=>"Drumone",      "5465"=>"Drumree",      "5466"=>"Duleek",      "5467"=>"Dunboyne",      "5468"=>"Dunderry",      "5470"=>"Dunsany",      "5471"=>"Dunshaughlin",      "5472"=>"Enfield",      "5473"=>"Fordstown",      "5474"=>"Fourknocks",      "5475"=>"Galtrim",      "5476"=>"Garadice",      "9596"=>"Garlow Cross",      "5478"=>"Garristown",      "5480"=>"Gormanstown",      "5481"=>"Grange",      "9597"=>"Grangegeeth",      "5482"=>"Greenanstown",      "5486"=>"Innfield",      "5487"=>"Julianstown",      "5488"=>"Kells",      "5490"=>"Kentstown",      "9600"=>"Kilallen",      "5492"=>"Kilbride",      "5495"=>"Kilcock",      "5499"=>"Kilmessan",      "5501"=>"Kilmore",      "5508"=>"Laytown",      "5520"=>"Moynalvy",      "5521"=>"Mullagh",      "5523"=>"Navan",      "5524"=>"Nobber",      "9599"=>"Nuttstown",      "5526"=>"Oldcastle",      "9626"=>"Oldtown",      "9625"=>"Pikecornor",      "5531"=>"Rath",      "5533"=>"Rathfeigh",      "5535"=>"Rathmoylon",      "9628"=>"Rathoath",      "5537"=>"Ratoath",      "5540"=>"Ross",      "5543"=>"Skreen",      "5545"=>"Slane",      "9622"=>"Stamullin",      "5548"=>"Summerhill",      "5554"=>"Trim",      "9624"=>"Tylas",      "9623"=>"Walterstown",      "9627"=>"Other",
      );
    }
    if ( (int)$AreaId== 20) { // "20"=>"County Monaghan",
      $ResArray= array (
      "9663"=>"Other",
      );
    }
    if ( (int)$AreaId== 5) { // "5"=>"County Offaly",
      $ResArray= array (
      "5703"=>"Togher",      "9523"=>"Other",
      );
    }
    if ( (int)$AreaId== 2) { //"2"=>"County Roscommon",
      $ResArray= array (
      "9520"=>"Other",
      );
    }

    if ( (int)$AreaId== 10) { // "10"=>"County Sligo",
      $ResArray= array (
      "9577"=>"Other",
      );
    }
    if ( (int)$AreaId== 137) { //"137"=>"County Tipperary North",
      $ResArray= array (
      "9674"=>"Other",
      );
    }
    if ( (int)$AreaId== 23) { // "23"=>"County Tipperary South",
      $ResArray= array (
      "5983"=>"Cahir",      "9630"=>"Tipperary South - OTL",      "9629"=>"Other",
      );
    }

    if ( (int)$AreaId== 16) { //"16"=>"County Waterford",
      $ResArray= array (
      "6125"=>"Ballybeg",      "9437"=>"Ballybricken",      "9432"=>"Ballymaclode",      "9427"=>"Ballynakill",      "9428"=>"Ballynaneashagh",      "6134"=>"Ballytruckle",      "9426"=>"Bilberry",      "9430"=>"Cleaboy",      "9431"=>"Farranshoneen",      "6157"=>"Ferrybank",      "9429"=>"Gracedieu",      "9439"=>"Kilbarry",      "9446"=>"Kingsmeadow",      "9447"=>"Larchville",      "6183"=>"Lisduggan",      "9445"=>"Mount Sion",      "9450"=>"Newtown",      "9448"=>"Poleberry",      "9449"=>"Roanmore",      "9444"=>"Shortcoure",      "9442"=>"Slievekeale",      "9443"=>"Ticor",      "9440"=>"Waterford City - OTL",      "9441"=>"Waterford Co. - OTL",      "9451"=>"Other",
      );
    }
    if ( (int)$AreaId== 24) { // "24"=>"County Westmeath",
      $ResArray= array (
      "9631"=>"Other",
      );
    }
    if ( (int)$AreaId== 15) { // "15"=>"County Wexford",
      $ResArray= array (
      "9438"=>"Other",
      );
    }
    if ( (int)$AreaId== 19) { // "19"=>"County Wicklow"
      $ResArray= array (
      "6445"=>"Annamoe",      "6446"=>"Arklow",      "6447"=>"Ashford",      "6448"=>"Aughrim",      "6449"=>"Avoca",      "6454"=>"Ballinalea",      "6458"=>"Ballycullen",      "6459"=>"Ballyduff",      "6460"=>"Ballyknockan",      "6462"=>"Baltinglass",      "6464"=>"Blessington",      "6466"=>"Bray",      "6468"=>"Brockagh",      "6469"=>"Carnew",      "6475"=>"Delgany",      "6476"=>"Donard",      "6478"=>"Dunlavin",      "6479"=>"Enniskerry",      "6481"=>"Glen Of The Downs",      "6485"=>"Glencree",      "6487"=>"Glendarragh",      "6488"=>"Glenealy",      "6490"=>"Granabeg",      "6492"=>"Gravale",      "6494"=>"Greystones",      "6495"=>"Hollywood",      "6496"=>"Kilbride",      "6498"=>"Kilcoole",      "9661"=>"Kilishey",      "6502"=>"Kilmacanogue",      "6503"=>"Kilmalin",      "6505"=>"Kilpedder",      "6509"=>"Kiltegan",      "6514"=>"Laragh",      "9660"=>"Manorkilbride",      "6519"=>"Newcastle",      "9658"=>"Newtown Mount Kennedy",      "6526"=>"Rathdrum",      "6527"=>"Rathnew",      "6528"=>"Redcross",      "6529"=>"Roundwood",      "6530"=>"Sally Gap",      "6531"=>"Shillelagh",      "9667"=>"Tinahealy",      "6534"=>"Togher",      "6535"=>"Valleymount",      "9666"=>"Wicklow - OTL",      "6537"=>"Wicklow Gap",      "6538"=>"Wicklow Town",      "9662"=>"Woodenbridge",      "9659"=>"Other",
      );
    }


    if ( !empty($SelectLabelText) ) {
      $ResArray= Util::SetArrayHeader( array($SelectLabelValue=>$SelectLabelText), $ResArray );
    }
    return $ResArray;
  }



  public static function getSex($SelectLabelValue= '', $SelectLabelText= '') {
    $ResArray= array (
    "M"=>"Male",
    "F"=>"Female",
    );
    if ( !empty($SelectLabelText) ) {
      $ResArray= Util::SetArrayHeader( array($SelectLabelValue=>$SelectLabelText), $ResArray );
    }
    return $ResArray;
  }

  public static function getdriverNCDYears($SelectLabelValue= '', $SelectLabelText= '') {
    $ResArray= array (
    "0"=>"No discount",
    "1"=>"1 year",
    "2"=>"2 years",
    "3"=>"3 years",
    "4"=>"4 years",
    "5"=>"5 years",
    "6"=>"6 years",
    "7"=>"7 years",
    "8"=>"8 or more years",
    );
    if ( !empty($SelectLabelText) ) {
      $ResArray= Util::SetArrayHeader( array($SelectLabelValue=>$SelectLabelText), $ResArray );
    }
    return $ResArray;
  }





  public static function getCarRegYear($SelectLabelValue= '', $SelectLabelText= '') {
    $StartCarRegYear= DataLoadAPI::$StartCarRegYear;
    $CurrentYear= (int)strftime("%Y");
    if ( !empty($SelectLabelText) ) {
      $ResArray[$SelectLabelValue]= $SelectLabelText;
    }

    for( $Year= $CurrentYear; $Year>= $StartCarRegYear; $Year-- ) {
      $ResArray[$Year]= $Year;
    }
    return $ResArray;
  }


  public static function getcarEngSize($SelectLabelValue= '', $SelectLabelText= '') {
    for( $I= DataLoadAPI::$EngSize_Min; $I< DataLoadAPI::$EngSize_Max;$I= $I+0.1 ) {
      //$ResArray[(string)$I]= (string)$I*1000;
      $ResArray[(string)$I*1000]= (string)$I;
    }
    if ( !empty($SelectLabelText) ) {
      $ResArray= Util::SetArrayHeader( array($SelectLabelValue=>$SelectLabelText), $ResArray );
    }
    return $ResArray;
  }



  public static function getCoverType($SelectLabelValue= '', $SelectLabelText= '') {
    $ResArray= array (
    "01"=>"Comprehensive",
    "02"=>"Third Party",
    "03"=>"Third Party, fire and Theft"
    );
    if ( !empty($SelectLabelText) ) {
      $ResArray= Util::SetArrayHeader( array($SelectLabelValue=>$SelectLabelText), $ResArray );
    }
    return $ResArray;
  }


  public static function getVoluntaryExcess($SelectLabelValue= '', $SelectLabelText= '') {
    $ResArray= array (
    "0"=>"No Change - Standard policy excess to apply",
    "75"=>"Standard policy excess increased by €75",
    "100"=>"Standard policy excess increased by €100",
    "125"=>"Standard policy excess increased by €125",
    "190"=>"Standard policy excess increased by €190",
    "200"=>"Standard policy excess increased by €200",
    "250"=>"Standard policy excess increased by €250",
    "300"=>"Standard policy excess increased by €300",
    "320"=>"Standard policy excess increased by €320",
    "325"=>"Standard policy excess increased by €325",
    "350"=>"Standard policy excess increased by €350",
    "375"=>"Standard policy excess increased by €375",
    "500"=>"Standard policy excess increased by €500",
    "750"=>"Standard policy excess increased by €750",
    "850"=>"Standard policy excess increased by €850",
    "1000"=>"Standard policy excess increased by €1000",
    );
    if ( !empty($SelectLabelText) ) {
      $ResArray= Util::SetArrayHeader( array($SelectLabelValue=>$SelectLabelText), $ResArray );
    }
    return $ResArray;
  }

  public static function getClassofUse($SelectLabelValue= '', $SelectLabelText= '') {
    $ResArray= array (
    "1"=>"Class 1", "2"=>"Class 2", "3"=>"Class 3", "F"=>"Farmers", "4"=>"Social, Domestic And Pleasure",
    );
    if ( !empty($SelectLabelText) ) {
      $ResArray= Util::SetArrayHeader( array($SelectLabelValue=>$SelectLabelText), $ResArray );
    }
    return $ResArray;
  }

  public static function getCarisimport($SelectLabelValue= '', $SelectLabelText= '') {
    $ResArray= array (
    "true"=>"Yes",
    "false"=>"No"
    );
    if ( !empty($SelectLabelText) ) {
      $ResArray= Util::SetArrayHeader( array($SelectLabelValue=>$SelectLabelText), $ResArray );
    }
    return $ResArray;
  }

  public static function getLefthanddrive($SelectLabelValue= '', $SelectLabelText= '') {
    $ResArray= array (
    "true"=>"Yes",
    "false"=>"No"
    );
    if ( !empty($SelectLabelText) ) {
      $ResArray= Util::SetArrayHeader( array($SelectLabelValue=>$SelectLabelText), $ResArray );
    }
    return $ResArray;
  }


  public static function getPurchased($SelectLabelValue= '', $SelectLabelText= '') {
    $ResArray= array (
    "2010"=>"2010",
    );
    if ( !empty($SelectLabelText) ) {
      $ResArray= Util::SetArrayHeader( array($SelectLabelValue=>$SelectLabelText), $ResArray );
    }
    return $ResArray;
  }

  public static function getRegistrationCounty($SelectLabelValue= '', $SelectLabelText= '') {
    $ResArray= array (
    "C"=>"C",
    "CE"=>"CE",
    "CN"=>"CN",
    "CW"=>"CW",
    "D"=>"D",
    "DL"=>"DL",
    "G"=>"G",
    "KE"=>"KE",
    "KK"=>"KK",
    "KY"=>"KY",
    "L"=>"L",
    "LD"=>"LD",
    "LH"=>"LH",
    "LK"=>"LK",
    "LM"=>"LM",
    "LS"=>"LS",
    "MH"=>"MH",
    "MN"=>"MN",
    "MO"=>"MO",
    "OY"=>"OY",
    "RN"=>"RN",
    "SO"=>"SO",
    "TN"=>"TN",
    "TS"=>"TS",
    "W"=>"W",
    "WD"=>"WD",
    "WH"=>"WH",
    "WX"=>"WX",
    "WW"=>"WW",
    );
    if ( !empty($SelectLabelText) ) {
      $ResArray= Util::SetArrayHeader( array($SelectLabelValue=>$SelectLabelText), $ResArray );
    }
    return $ResArray;
  }



  public static function getSecuritydevice($SelectLabelValue= '', $SelectLabelText= '') {
    $ResArray= array (
    "1001"=>"Alarm", "1002"=>"Alarm & Immob.", "1003"=>"Alarm & Tracker", "1004"=>"Alarm, Immob. & Tracker", "1005"=>"Immob. & Tracker", "1006"=>"Immobiliser",
    );
    if ( !empty($SelectLabelText) ) {
      $ResArray= Util::SetArrayHeader( array($SelectLabelValue=>$SelectLabelText), $ResArray );
    }
    return $ResArray;
  }

  public static function getParkedovernight($SelectLabelValue= '', $SelectLabelText= '') {
    $ResArray= array (
    "1"=>"Carport",
    "2"=>"Garaged.",
    "3"=>"Kept on Private Property",
    "4"=>"Kept on Public Road",
    "5"=>"Parked On Drive"
    );

    if ( !empty($SelectLabelText) ) {
      $ResArray= Util::SetArrayHeader( array($SelectLabelValue=>$SelectLabelText), $ResArray );
    }
    return $ResArray;
  }



  public static function getAnnualmileage($SelectLabelValue= '', $SelectLabelText= '') {
    for($I=1; $I<=99; $I++ ) {
      $ResArray[$I*1000]= $I*1000;
    }
    if ( !empty($SelectLabelText) ) {
      $ResArray= Util::SetArrayHeader( array($SelectLabelValue=>$SelectLabelText), $ResArray );
    }
    return $ResArray;
  }





  public static function getTravelpass($SelectLabelValue= '', $SelectLabelText= '') {
    $ResArray= array (
    "true"=>"Yes",
    "false"=>"No"
    );
    if ( !empty($SelectLabelText) ) {
      $ResArray= Util::SetArrayHeader( array($SelectLabelValue=>$SelectLabelText), $ResArray );
    }
    return $ResArray;
  }

  public static function getSpouseorpartnerflltimeuseofothercar($SelectLabelValue= '', $SelectLabelText= '') {
    $ResArray= array (
    "true"=>"Yes",
    "false"=>"No",
    );
    if ( !empty($SelectLabelText) ) {
      $ResArray= Util::SetArrayHeader( array($SelectLabelValue=>$SelectLabelText), $ResArray );
    }
    return $ResArray;
  }

  public static function getAdvdrivingcourse($SelectLabelValue= '', $SelectLabelText= '') {
    $ResArray= array (
    "true"=>"Yes",
    "false"=>"No",
    );
    if ( !empty($SelectLabelText) ) {
      $ResArray= Util::SetArrayHeader( array($SelectLabelValue=>$SelectLabelText), $ResArray );
    }
    return $ResArray;
  }


  public static function getCurrentinsurer($SelectLabelValue= '', $SelectLabelText= '') {
    $ResArray= array (
    "1"=>"AIG Insurance",
    "2"=>"Chubb Insurance",
    "3"=>"Cigna Insurance of Europe",
    "4"=>"Eagle Star",
    "5"=>"Ecclesiastical Insurance Office Plc",
    "6"=>"Europa General",
    "7"=>"FBD Insurance",
    "8"=>"First Call Direct",
    "9"=>"Generali Insurance",
    "10"=>"Hibernian Aviva Insurance Ireland",
    "11"=>"Quinn Direct",
    "12"=>"Royal & SunAlliance",
    "13"=>"St. Paul Ireland Insurance",
    "14"=>"Torch",
    "15"=>"Cornhill",
    "16"=>"ARB Underwriting Ltd",
    "17"=>"Wright Way Underwriting",
    "18"=>"AXA Direct",
    "19"=>"AXA Broker",
    "20"=>"Allianz",
    "21"=>"Liberty Underwriting",
    "22"=>"AXA Insurance",
    "23"=>"Asgard",
    "24"=>"FC Rebroked AXA Broker",
    "25"=>"Sertus Underwriting",
    "26"=>"XS Direct",
    "27"=>"Prestige Underwriting Services",
    "28"=>"Kennco Underwriting Ltd",
    "29"=>"Unlisted Insurer - Not Ireland",
    "30"=>"Unlisted Insurer - Ireland",
    "31"=>"Setanta Insurance"
    );
    if ( !empty($SelectLabelText) ) {
      $ResArray= Util::SetArrayHeader( array($SelectLabelValue=>$SelectLabelText), $ResArray );
    }
    return $ResArray;
  }



  public static function getInsuredsomeoneelsepolicy($SelectLabelValue= '', $SelectLabelText= '') {
    $ResArray= array (
    "true"=>"Yes",
    "false"=>"No"
    );
    if ( !empty($SelectLabelText) ) {
      $ResArray= Util::SetArrayHeader( array($SelectLabelValue=>$SelectLabelText), $ResArray );
    }
    return $ResArray;
  }



  public static function getOtherdrivers($SelectLabelValue= '', $SelectLabelText= '') {
    $ResArray= array (
    "1"=>"Any Driver-excluding drivers under 17",
    "2"=>"Any Driver-excluding drivers under 25 or over 70",
    "3"=>"Any Driver-excluding drivers under 30 or over 70",
    "4"=>"Insured + Spouse",
    "5"=>"Insured and 1 Named driver",
    "6"=>"Insured and 2 Named drivers",
    "7"=>"Insured and 3 Named drivers",
    "8"=>"Insured and 4 Named drivers",
    "9"=>"Insured and 5 Named drivers",
    "10"=>"Insured and Parent",
    "11"=>"Insured Only",
    );
    if ( !empty($SelectLabelText) ) {
      $ResArray= Util::SetArrayHeader( array($SelectLabelValue=>$SelectLabelText), $ResArray );
    }
    return $ResArray;
  }


  public static function getTitle($SelectLabelValue= '', $SelectLabelText= '') {
    $ResArray= array (
    "027"=>"Br.", "001"=>"Dr.", "035"=>"Fr.", "002"=>"Miss.", "005"=>"Mr.", "004"=>"Mrs.", "003"=>"Ms.", "024"=>"Prof.", "006"=>"Rev.", "030"=>"Sr.",
    );
    if ( !empty($SelectLabelText) ) {
      $ResArray= Util::SetArrayHeader( array($SelectLabelValue=>$SelectLabelText), $ResArray );
    }
    return $ResArray;
  }

  // FOUND
  public static function getMaritalstatus($SelectLabelValue= '', $SelectLabelText= '') {
    $ResArray= array (
    "C"=>"Common Law Spouse", "D"=>"Divorced", "M"=>"Married", "A"=>"Separated", "S"=>"Single", "W"=>"Widowed",
    );
    if ( !empty($SelectLabelText) ) {
      $ResArray= Util::SetArrayHeader( array($SelectLabelValue=>$SelectLabelText), $ResArray );
    }
    return $ResArray;
  }



  public static function getOccupation($SelectLabelValue= '', $SelectLabelText= '') {
    $ResArray= array (
    "AAA"=>"Abattoir Worker", "AAB"=>"Accommodation Officer", "AAC"=>"Accountant", "AAD"=>"Actor", "AAE"=>"Actress", "AAF"=>"Actuary", "AAG"=>"Acupuncturist", "AAH"=>"Administration Staff", "AAI"=>"Advertising Contractor", "AAJ"=>"Aerial Erector", "AAK"=>"Agricultural Contractor", "AAL"=>"Air Corps", "AAM"=>"Air Traffic Controller", "AAN"=>"Aircraft Buyer", "AAO"=>"Aircraft Designer", "AAP"=>"Airline Check-in Staff", "AAQ"=>"Airline Staff", "AAR"=>"Airport Groundstaff", "AAS"=>"Airport Maintenance Staff", "AAT"=>"Airport Police", "AAU"=>"Alarm Installer", "AAV"=>"Almoner", "AAW"=>"Ambulance Crew", "AAX"=>"Ambulance Driver", "AAY"=>"Amusement Arcade Owner", "AAZ"=>"Amusement Arcade Worker", "ABA"=>"Anaesthetist", "ABB"=>"Animal Breeder", "ABC"=>"Animal Trainer", "ABD"=>"Antique Dealer", "ABE"=>"Applications Programmer", "ABF"=>"Arbitrator", "ABG"=>"Archaeologist", "ABH"=>"Architect", "ABI"=>"Archivist", "ABJ"=>"Army or Armed Forces.", "ABK"=>"Art Critic", "ABL"=>"Art Dealer", "ABM"=>"Artificial Inseminator", "ABN"=>"Artist", "ABO"=>"Asphalter", "ABP"=>"Assembly Worker", "ABQ"=>"Astrologer", "ABR"=>"Astronomer", "ABS"=>"Au Pair", "ABT"=>"Auctioneer", "ABU"=>"Audiologist", "ABV"=>"Auditor", "ABW"=>"Author", "ABX"=>"Auto Electrician", "ABY"=>"Auxiliary Nurse", "BAA"=>"Bacon Curer", "BAB"=>"Baggage Handler", "BAC"=>"Bailiff", "BAD"=>"Baker", "BAE"=>"Bank Staff", "BAF"=>"Bar Manager", "BAG"=>"Barber", "BAH"=>"Barmaid", "BAI"=>"Barman", "BAJ"=>"Barrister", "BAK"=>"Beautician", "BAL"=>"Betting Shop Employee", "BAM"=>"Bill Poster", "BAN"=>"Biochemist", "BAO"=>"Biologist", "BAP"=>"Blacksmith", "BAQ"=>"Blinds Installer", "BAR"=>"Boat Builder", "BAS"=>"Boiler Maker", "BAT"=>"Boiler Man", "BAU"=>"Book Binder", "BAV"=>"Book Seller", "BAW"=>"Book-Keeper", "BAX"=>"Botanist", "BAY"=>"Breadman", "BAZ"=>"Brewer", "BBA"=>"Brewery Worker", "BBB"=>"Bricklayer", "BBC"=>"Broadcaster", "BBD"=>"Builder", "BBE"=>"Builder Merchant", "BBF"=>"Builders Labourer", "BBG"=>"Building Contractor", "BBH"=>"Building Foreman", "BBI"=>"Building Inspector", "BBJ"=>"Building Surveyor", "BBK"=>"Bursar", "BBL"=>"Bus Conductor", "BBM"=>"Bus Driver", "BBQ"=>"Business Analyst", "BBN"=>"Butcher", "BBO"=>"Butler", "BBP"=>"Buyer", "CAA"=>"C.I.E. Employee", "CAB"=>"Cabinet Maker", "CEB"=>"Call Centre Agent", "CAC"=>"Calligrapher", "CAD"=>"Cameraman", "CAE"=>"Car Body Repairer", "CAF"=>"Car Builder", "CAG"=>"Car Dealer", "CAH"=>"Car Delivery Driver", "CAI"=>"Car Park Attendant", "CAJ"=>"Car Salesman", "CAK"=>"Car Valet", "CAL"=>"Care Assistant", "CAM"=>"Care Supervisor", "CAN"=>"Careers Officer", "CAO"=>"Caretaker", "CAP"=>"Carpenter", "CAQ"=>"Carpetfitter", "CAR"=>"Cartographer", "CAS"=>"Cartoonist", "CAT"=>"Cashier", "CAU"=>"Casual Worker", "CAV"=>"Caterer - Mobile", "CAW"=>"Catering Trade-Licensed", "CAX"=>"Caulker", "CAY"=>"Certified Accountant", "CAZ"=>"Chambermaid", "CBA"=>"Chandler", "CBB"=>"Charge Hand", "CBC"=>"Charity Worker", "CBD"=>"Chartered Surveyor", "CBE"=>"Charterer", "CBF"=>"Chauffeur", "CEC"=>"Checkout Clerk", "CBG"=>"Chef", "CBH"=>"Chef - Lienced Premises", "CBI"=>"Chemist", "CBJ"=>"Chicken Sexer", "CBK"=>"Child Minder", "CBL"=>"Childrens Entertainer", "CBM"=>"Chimney Sweep", "CBN"=>"Chiropodist", "CBO"=>"Chiropractor", "CBP"=>"Choreographer", "CBQ"=>"Church Officer", "CBR"=>"Church Warden", "CBS"=>"Cinema Assistant", "CBT"=>"Circus Proprietor", "CBU"=>"Circus Worker", "CBV"=>"Civil Servant", "CBW"=>"Claims Adjuster", "CBX"=>"Claims Assessor", "CBY"=>"Classroom Aide", "CBZ"=>"Cleaner", "CCA"=>"Clergyman", "CCB"=>"Cleric", "CCC"=>"Clerical Officer", "CCD"=>"Clerk", "CCE"=>"Clock Maker", "CCF"=>"Club Steward", "CCG"=>"Coach Builder", "CCH"=>"Coach Driver", "CCI"=>"Coal Merchant", "CCJ"=>"Coastguard", "CCK"=>"Cobbler", "CCL"=>"Commercial Artist", "CCM"=>"Commercial Traveller", "CCN"=>"Commission Agent", "CCO"=>"Commissionaire", "CCP"=>"Commissioned Officer", "CCQ"=>"Commodity Broker", "CCR"=>"Commodity Dealer", "CCS"=>"Community Worker", "CCT"=>"Company Secretary", "CCU"=>"Complementary Therapist", "CCV"=>"Composer", "CCW"=>"Compositor", "CCX"=>"Computer Analyst", "CCY"=>"Computer Consultant", "CCZ"=>"Computer Operator", "CDA"=>"Computer Programmer", "CDB"=>"Confectioner", "CDC"=>"Construction Worker", "CDD"=>"Consultant", "CDE"=>"Contractor", "CDF"=>"Conveyancer", "CDG"=>"Cook", "CDH"=>"Cooper", "CDI"=>"Coppersmith", "CDJ"=>"Copywriter", "CDK"=>"Coroner", "CDL"=>"Council Worker", "CDM"=>"Counsellor", "CDN"=>"County Councillor", "CDO"=>"Courier", "CDP"=>"Court Officer", "CDQ"=>"Craftsman", "CDR"=>"Crane Driver", "CDS"=>"Crane Operator", "CDT"=>"Credit Controller", "CDU"=>"Credit Draper", "CDV"=>"Croupier", "CDW"=>"Curator", "CDX"=>"Customer Liaison Officer", "CDY"=>"Customs & Excise Officer", "CDZ"=>"Cutler", "CEA"=>"Cutter", "DAA"=>"Dairy Worker", "DAB"=>"Dance Teacher", "DAC"=>"Dancer", "DAD"=>"Dark Room Technician", "DBW"=>"Database Administrator", "DAE"=>"Dealer - General", "DAF"=>"Debt Collector", "DAG"=>"Decorator", "DEF"=>"Defence Forces - PDFORRA", "DAH"=>"Delivery Courier", "DAI"=>"Delivery Roundsman", "DAJ"=>"Demolition Worker", "DAK"=>"Demonstrator", "DAL"=>"Dental Assistant", "DAM"=>"Dental Hygienist", "DAN"=>"Dental Nurse", "DAO"=>"Dental Surgeon", "DAP"=>"Dental Technician", "DAQ"=>"Dentist", "DAR"=>"Dermatologist", "DAS"=>"Despatch Driver", "DAT"=>"Despatch Rider", "DAU"=>"Despatch Worker", "DAV"=>"Dietician", "DAW"=>"Dinner Lady", "DAX"=>"Diplomat - U.K.", "DAY"=>"Diplomatic Staff - Other", "DAZ"=>"Diplomatic Staff - ROI", "DBA"=>"Director - Arts.", "DBB"=>"Director - Company", "DBC"=>"Director -Performing Arts", "DBD"=>"Disc Jockey", "DBE"=>"District Nurse", "DBF"=>"Diver", "DBG"=>"Docker", "DBH"=>"Dockyard Worker", "DBI"=>"Doctor", "DBJ"=>"Dog Groomer", "DBK"=>"Door To Door Collector", "DBL"=>"Doorman", "DBX"=>"Doorman - Pub/Club", "DBM"=>"Double Glazing Fitter", "DBN"=>"Double Glazing Salesman", "DBO"=>"Draper", "DBP"=>"Draughtsman", "DBQ"=>"Dressmaker", "DBR"=>"Driving Examiner", "DBS"=>"Driving Instructor", "DBT"=>"Dry Cleaner", "DBU"=>"Dustman", "DBV"=>"Dyer", "EAA"=>"Economist", "EAB"=>"Editor", "EAC"=>"Editor - Newspaper", "EAD"=>"Electrical Contractor", "EAE"=>"Electrician", "EAF"=>"Embalmer", "EAH"=>"Embassy Staff - Other", "EAI"=>"Embassy Staff - ROI", "EAG"=>"Embassy Staff (UK)", "EAJ"=>"Energy Analyst", "EAK"=>"Engineer", "EAL"=>"Engraver", "EAM"=>"Entertainer", "EAN"=>"Environmental Health Officer", "EAO"=>"Estate Agent", "EAP"=>"Estimator", "EAQ"=>"Evangelist", "EAV"=>"Event Promoter", "EAR"=>"Exhaust Fitter", "EAS"=>"Exhibition Organiser", "EAT"=>"Exotic Dancer", "EAU"=>"Exporter", "FAA"=>"Fabricator", "FAB"=>"Factory Inspector", "FAC"=>"Factory Worker", "FAD"=>"Fairground Worker", "FAE"=>"Farm Worker", "FAF"=>"Farmer", "FAG"=>"Farrier", "FAH"=>"FAS Trainee", "FAI"=>"FAS Trainer", "FAJ"=>"Fashion Designer", "FAK"=>"Fast Food Delivery Driver", "FAL"=>"Fence Erecter", "FAM"=>"Fibre Glass Moulder", "FAN"=>"Film Producer", "FAO"=>"Finance Controller", "FAP"=>"Financial Advisor", "FAQ"=>"Financial Analyst", "FAR"=>"Financier", "FAS"=>"Fire Officer", "FAT"=>"Firefighter", "FAU"=>"Firewoman", "FAV"=>"Firewood Merchant", "FAW"=>"Fish Farmer", "FAX"=>"Fish Filleter", "FAY"=>"Fish Fryer", "FAZ"=>"Fisherman", "FBA"=>"Fishmonger", "FBY"=>"Fitness Instructor", "FBB"=>"Fitter", "FBC"=>"Fitter - Tyre/Exhaust", "FBD"=>"Floor Layer", "FBE"=>"Florist", "FBZ"=>"Flower Arranger", "FBF"=>"Flying Instructor", "FBG"=>"Food Processor", "FBH"=>"Foreman", "FBI"=>"Forester", "FBJ"=>"Fork Lift Truck Driver", "FBK"=>"Forwarding Agent", "FBL"=>"Foundry Worker", "FBM"=>"French Polisher", "FBN"=>"Fruiterer", "FBO"=>"Fuel Merchant", "FBP"=>"Fund Raiser", "FBQ"=>"Funeral Director", "FBR"=>"Funeral Furnisher", "FBS"=>"Furnace Man", "FBT"=>"Furniture Dealer", "FBU"=>"Furniture Manufacturer", "FBV"=>"Furniture Remover", "FBW"=>"Furniture Restorer", "FBX"=>"Furrier", "GAA"=>"Gam. Club Stff - Lic Prem", "GAB"=>"Gam. Club Stff - Unl Prem", "GAC"=>"Gambling Club Staff", "GAD"=>"Gamekeeper", "GAE"=>"Garage Foreman", "GAF"=>"Garda", "GAG"=>"Gardener", "GAH"=>"Gas Fitter", "GAI"=>"Genealogist", "GAJ"=>"General Operative", "GAK"=>"General Practitioner", "GAL"=>"Geologist", "GAM"=>"Geophysicist", "GAN"=>"Glass Worker", "GAO"=>"Glazier", "GAP"=>"Goldsmith", "GAQ"=>"Graphic Designer", "GAR"=>"Grave Digger", "GAS"=>"Gravel Merchant", "GAT"=>"Green Keeper", "GAU"=>"Greengrocer", "GAV"=>"Groom", "GAW"=>"Groundsman", "GAX"=>"Guest House Proprietor", "GAY"=>"Gun Smith", "GAZ"=>"Gynaecologist", "HAA"=>"Hackney Driver", "HAB"=>"Hairdresser", "HAC"=>"Haulage Contractor", "HAD"=>"Hawker", "HAE"=>"Headteacher", "HAF"=>"Health and Safety Officer", "HAG"=>"Health Board Staff", "HAH"=>"Health Planner", "HAI"=>"Health Visitor", "HAJ"=>"Hearing Therapist", "HAK"=>"Heating Contractor", "HAL"=>"Herbalist", "HAM"=>"HGV Driver", "HAN"=>"Hire Car Driver", "HAO"=>"Hod Carrier", "HAP"=>"Home Help", "HAQ"=>"Home Maker", "HAR"=>"Homeopath", "HAS"=>"Horologist", "HAT"=>"Horse Breeder", "HAU"=>"Horse Dealer", "HAV"=>"Horse Riding Instructor", "HAW"=>"Horse Trader", "HAX"=>"Horticulturalist", "HAY"=>"Hospital Consultant", "HAZ"=>"Hospital Doctor", "HBA"=>"Hospital Staff", "HBB"=>"Hostel Staff", "HBC"=>"Hostess", "HBD"=>"House Parent", "HBE"=>"Househusband", "HBF"=>"Housekeeper", "HBG"=>"Housewife", "HBH"=>"Housing Officer", "IAA"=>"Ice Cream Vendor", "IAB"=>"Illustrator", "IAC"=>"Immigration Officer", "IAD"=>"Importer", "IAE"=>"Independent Means", "IAF"=>"Industrial Chemist", "IAG"=>"Industrial Designer", "IAH"=>"Industrial Relations Manager", "IAI"=>"Inland Revenue Officer", "IAJ"=>"Inspector - Insurance", "IAK"=>"Instrument Maker", "IAL"=>"Insurance Assessor", "IAT"=>"Insurance Intermediary", "IAM"=>"Insurance Representative", "IAN"=>"Interior Decorator", "IAO"=>"Interior Designer", "IAP"=>"Interpreter", "IAQ"=>"Interviewer", "IAR"=>"Irish Rail Staff", "IAS"=>"Ironmonger", "JAA"=>"Janitor", "JAB"=>"Jeweller", "JAC"=>"Jockey", "JAD"=>"Joiner", "JAE"=>"Journalist", "JAF"=>"Judge", "JAG"=>"Junk Shop Proprietor", "JAH"=>"Justice Of The Peace", "KAA"=>"Keep Fit Instructor", "KAB"=>"Kennel Hand", "KAC"=>"Kissagram Person", "KAD"=>"Kitchen Worker", "KAE"=>"Knitter", "LAA"=>"Laboratory Assistant", "LAB"=>"Laboratory Technician", "LAC"=>"Labourer", "LAD"=>"Laminator", "LAE"=>"Land Agent", "LAF"=>"Landlord", "LAG"=>"Landscape Gardener", "LAH"=>"Lathe Operator", "LAI"=>"Laundry Worker", "LAJ"=>"Lavatory Attendant", "LAK"=>"Lawyer", "LAL"=>"Leather Worker", "LAM"=>"Lecturer", "LAN"=>"Legal Assistant", "LAO"=>"Legal Executive", "LAP"=>"Legal Secretary", "LAQ"=>"Leisure Centre Attendant", "LAR"=>"Lens Grinder & Polisher", "LAS"=>"Librarian", "LAT"=>"Licensee", "LAU"=>"Lifeguard", "LAV"=>"Lift Attendant", "LAW"=>"Line Worker", "LAX"=>"Linesman", "LAY"=>"Linguist", "LAZ"=>"Literary Agent", "LBA"=>"Lithographer", "LBB"=>"Locksmith", "LBC"=>"Log Merchant", "LBD"=>"Lorry Driver", "LBE"=>"Loss Adjustor", "LBF"=>"Loss Assessor", "LBG"=>"Lumberjack", "MAA"=>"Machine Operator", "MAB"=>"Machinist", "MAC"=>"Magistrate", "MAD"=>"Maintenance Staff", "MAE"=>"Make Up Artist", "MAF"=>"Manager", "MAG"=>"Manicurist", "MAH"=>"Manufacturing Agent", "MAI"=>"Marine Pilot", "MAJ"=>"Market Analyst", "MAK"=>"Market Gardener", "MAL"=>"Market Researcher", "MAM"=>"Market Trader", "MAN"=>"Marketing Staff", "MAO"=>"Martial Arts Instructor", "MAP"=>"Masseur", "MAQ"=>"Masseuse", "MAR"=>"Mathematician", "MAS"=>"Matron", "MAT"=>"Mechanic", "MAU"=>"Medical Consultant", "MAV"=>"Medical Practitioner", "MAW"=>"Medical Secretary", "MAX"=>"Merchant Seaman", "MAY"=>"Messenger", "MAZ"=>"Metal Dealer", "MBA"=>"Metal Polisher", "MBB"=>"Metal Worker", "MBC"=>"Metallurgist", "MBD"=>"Meteorologist", "MBE"=>"Meter Reader", "MBF"=>"Microbiologist", "MBG"=>"Microfilm Operator", "MBH"=>"Midwife", "MBI"=>"Milkman", "MBJ"=>"Mill Worker", "MBK"=>"Miller", "MBL"=>"Milliner", "MBM"=>"Miner", "MBN"=>"Mineralologist", "MBO"=>"Minister of Religion", "MBP"=>"Mobile Disco Owner", "MBQ"=>"Model", "MBR"=>"Moneylender", "MBS"=>"Mortgage Broker", "MBT"=>"Mortician", "MBU"=>"Motor Mechanic", "MBV"=>"Motor Racing Organiser", "MBW"=>"Motor Trader", "MBX"=>"Museum Attendant", "MBY"=>"Music Teacher", "MBZ"=>"Musician", "NAA"=>"Nanny", "NAB"=>"Navigator", "NAC"=>"Negotiator", "NAD"=>"Neurologist", "NAE"=>"Newsagent", "NAF"=>"Night Watchman", "NAG"=>"Non Commissioned Officer", "NAH"=>"Not In Employment", "NAI"=>"Nun", "NAJ"=>"Nurse", "NAK"=>"Nursery Assistant", "NAL"=>"Nursery Nurse", "NAM"=>"Nurseryman", "OAA"=>"Occupational Therapist", "OAB"=>"Oculist", "OAC"=>"Off Licence Employee", "OAD"=>"Operations Supervisor", "OAE"=>"Optical Assistant", "OAF"=>"Optician", "OAG"=>"Organist", "OAH"=>"Osteopath", "OAI"=>"Outfitter", "PAA"=>"Packer", "PAB"=>"Painter", "PAC"=>"Painter And Decorator", "PAD"=>"Pallet Maker", "PAE"=>"Panel Beater", "PAF"=>"Paramedic", "PAG"=>"Park Attendant", "PAH"=>"Park Keeper", "PAI"=>"Party Planner", "PAJ"=>"Pathologist", "PAK"=>"Pattern Maker", "PAL"=>"Pattern Weaver", "PAM"=>"Pawnbroker", "PAN"=>"Personal Assistant", "PAO"=>"Personnel Officer", "PAP"=>"Pest Controller", "PAQ"=>"Petrol Station Attendant", "PAR"=>"Pharmaceutical Technician", "PAS"=>"Pharmacist", "PAT"=>"Photo Engraver", "PAU"=>"Photographer", "PAV"=>"Photographer - Location", "PAW"=>"Photographer - Studio", "PAX"=>"Physicist", "PAY"=>"Physiologist", "PAZ"=>"Physiotherapist", "PBA"=>"Piano Tuner", "PBB"=>"Picker", "PBC"=>"Picture Framer", "PBD"=>"Pilot (Air)", "PBE"=>"Pipe Fitter", "PBF"=>"Pipe Layer", "PBG"=>"Planning Officer", "PBH"=>"Plant Driver", "PBI"=>"Plant Operator", "PBJ"=>"Plasterer", "PBK"=>"Plater", "PBL"=>"Playgroup Leader", "PBM"=>"Plumber", "PBN"=>"Pool Attendant", "PBO"=>"Pools Collector", "PBP"=>"Porter", "PBQ"=>"Postman", "PBR"=>"Postwoman", "PBS"=>"Potter", "PBT"=>"Press Operator", "PBU"=>"Presser", "PBV"=>"Priest", "PBW"=>"Print Finisher", "PBX"=>"Printer", "PBY"=>"Prison Officer", "PBZ"=>"Private Investigator", "PCA"=>"Probation Officer", "PCB"=>"Process Worker", "PCC"=>"Procurator Fiscal", "PCD"=>"Produce Supervisor", "PCE"=>"Producer", "PCF"=>"Professor", "PCG"=>"Projectionist", "PCH"=>"Proof Reader", "PCI"=>"Property Buyer", "PCJ"=>"Property Dealer", "PCK"=>"Property Developer", "PCL"=>"Property Valuer", "PCM"=>"Proprietor", "PCN"=>"Prosthesist", "PCO"=>"Psychiatrist", "PCP"=>"Psychologist", "PCQ"=>"Psychotherapist", "PCR"=>"Public Relations Officer", "PCS"=>"Publican", "PCT"=>"Publisher", "PCU"=>"Purchaser", "PCV"=>"Purser", "QAA"=>"Quality Controller", "QAB"=>"Quantity Surveyor", "QAC"=>"Quarry Worker", "RAA"=>"Rabbi", "RAB"=>"Radio Presenter", "RAC"=>"Radio Producer", "RAD"=>"Radiographer", "RAE"=>"Radiologist", "RAF"=>"Receptionist", "RAG"=>"Rector", "RAH"=>"Reflexologist", "RAI"=>"Refuse Collector", "RAJ"=>"Registrar", "RAK"=>"Reporter", "RAL"=>"Researcher", "RAM"=>"Restaurateur", "RAN"=>"Restorer", "RAO"=>"Retired", "RAP"=>"Revenue Officer", "RAQ"=>"Rig Worker", "RAR"=>"Rigger", "RAS"=>"Riveter", "RAT"=>"Road Sweeper", "RAU"=>"Road Worker", "RAV"=>"Roofer", "SAA"=>"Saddler", "SAB"=>"Safety Officer", "SAC"=>"Sail Maker", "SAD"=>"Sailor", "SAE"=>"Sales Administrator", "SAF"=>"Sales Executive", "SAG"=>"Sales Representative", "SAH"=>"Sales Support", "SAI"=>"Salesman", "SAJ"=>"Sand Blaster", "SAK"=>"Sand/Gravel Merchant", "SAL"=>"Saw Miller", "SAM"=>"Scaffolder", "SAN"=>"School Crossing Warden", "SAO"=>"Scientist", "SAP"=>"Scrap Dealer", "SAQ"=>"Screen Printer", "SAR"=>"Screen Writer", "SAS"=>"Script Writer", "SAT"=>"Sculptor", "SAU"=>"Sea Captain", "SAV"=>"Seaman", "SAW"=>"Seamstress", "SAX"=>"Second Hand Dealer", "SAY"=>"Secretary", "SAZ"=>"Secretary and PA", "SBA"=>"Security Guard", "SBB"=>"Security Officer", "SBC"=>"Seedsman", "SBD"=>"Senator", "SBE"=>"Servant", "SBF"=>"Sexton", "SBG"=>"Sheet Metal Worker", "SBH"=>"Shelf Filler", "SBI"=>"Sheriff", "SBJ"=>"Ship Builder", "SBK"=>"Shipwright", "SBL"=>"Shoe Repairer", "SBM"=>"Shop Assistant", "SBN"=>"Shop Fitter", "SBO"=>"Shop Keeper", "SBP"=>"Shot Blaster", "SBQ"=>"Showman", "SBR"=>"Shunter", "SBS"=>"Signalman", "SBT"=>"Signwriter", "SBU"=>"Silversmith", "SBV"=>"Smallholder", "SBW"=>"Smelter", "SBX"=>"Social Worker", "SDK"=>"Software Developer", "SBY"=>"Solicitor", "SBZ"=>"Song Writer", "SCA"=>"Sorter", "SCB"=>"Speech Therapist", "SCC"=>"Spinner", "SCD"=>"Sports Centre Attendant", "SCE"=>"Sports Centre Staff", "SCF"=>"Sports Coach", "SCG"=>"Sportsman", "SCH"=>"Spray Painter", "SCI"=>"Stable Hand", "SCJ"=>"Stage Mover", "SCK"=>"Statistician", "SCL"=>"Steel Erector", "SCM"=>"Steel Worker", "SCN"=>"Steeplejack", "SCO"=>"Stenographer", "SCP"=>"Stevedore", "SCQ"=>"Stock Controller", "SCR"=>"Stockbroker", "SCS"=>"Stockman", "SCT"=>"Stonemason", "SCU"=>"Storeman", "SCV"=>"Street Entertainer", "SCW"=>"Street Trader", "SCX"=>"Stud Farm Proprietor", "SCY"=>"Stud Hand", "SCZ"=>"Stud Manager", "SDA"=>"Student", "SDB"=>"Student Nurse", "SDC"=>"Student Teacher", "SDD"=>"Supermarket Assistant", "SDE"=>"Supervisor", "SDF"=>"Supply Teacher", "SDG"=>"Surgeon", "SDH"=>"Surveyor", "SDI"=>"Surveyor - Chartered", "SDJ"=>"Systems Analyst", "TAA"=>"Tailor", "TAB"=>"Tanner", "TAC"=>"Tarmacadam Contractor", "TAD"=>"Tax Consultant", "TAE"=>"Tax Inspector", "TAF"=>"Taxi Driver", "TAG"=>"Taxidermist", "TAH"=>"TD/Senator", "TAI"=>"Tea Blender", "TAJ"=>"Tea Taster", "TAK"=>"Teacher", "TAL"=>"Teachers Assistant", "TAM"=>"Technician", "TAN"=>"Telephonist", "TCC"=>"Telesales Agent", "TAO"=>"Telesales Person", "TAP"=>"Television Presenter", "TAQ"=>"Telex Operator", "TAR"=>"Textile Printer", "TAS"=>"Textile Worker", "TAT"=>"Thatcher", "TAU"=>"Theatrical Agent", "TAV"=>"Therapist", "TAW"=>"Ticket Inspector", "TAX"=>"Tiler", "TAY"=>"Tobacconist", "TAZ"=>"Tool Maker", "TBA"=>"Tool Setter", "TBB"=>"Tour Courier", "TBC"=>"Tour Guide", "TBD"=>"Town Planner", "TBE"=>"Toy Maker", "TBF"=>"Tractor Driver", "TBG"=>"Trade Union Official", "TBH"=>"Trading Standards Officer", "TBI"=>"Traffic Warden", "TBJ"=>"Train Driver", "TBK"=>"Trainer", "TBL"=>"Training Officer", "TBM"=>"Translator", "TBN"=>"Transport Controller", "TBO"=>"Transport Officer", "TBP"=>"Travel Agent", "TBQ"=>"Travel Consultant", "TBR"=>"Travel Courier", "TBS"=>"Treasurer", "TBT"=>"Tree Feller", "TBU"=>"Tree Surgeon", "TBV"=>"Trinity House Pilot", "TBW"=>"Tunneller", "TBX"=>"Turf Accountant", "TBY"=>"Turner", "TBZ"=>"Tutor", "TCA"=>"Typesetter", "TCB"=>"Typist", "UAA"=>"Undertaker", "UAB"=>"Underwriter", "UAC"=>"Unemployed", "UAD"=>"Upholsterer", "UAE"=>"Usher", "VAA"=>"Valuer", "VAB"=>"Van Driver", "VAC"=>"VDU Operator", "VAD"=>"Vehicle Assessor", "VAE"=>"Verger", "VAF"=>"Veterinary Surgeon", "VAG"=>"Vicar", "VAH"=>"Voluntary Worker", "WAA"=>"Wages Clerk", "WAS"=>"Waiting Staff", "WAB"=>"Warehouseman", "WAC"=>"Waste Dealer", "WAD"=>"Waste Paper Merchant", "WAE"=>"Watchmaker", "WAF"=>"Weaver", "WAG"=>"Welder", "WAH"=>"Welfare Officer", "WAI"=>"Welfare Rights Officer", "WAJ"=>"Wheel Clamper", "WAK"=>"Wholesaler", "WAL"=>"Window Cleaner", "WAM"=>"Window Dresser", "WAN"=>"Windscreen Fitter", "WAO"=>"Wine Merchant", "WAP"=>"Wood Worker", "WAQ"=>"Wool Sorter", "WAR"=>"Writer", "YAA"=>"Youth Hostel Warden", "YAB"=>"Youth Worker", "ZAA"=>"Zoo Keeper", "ZAB"=>"Zoologist",
    );
    if ( !empty($SelectLabelText) ) {
      $ResArray= Util::SetArrayHeader( array($SelectLabelValue=>$SelectLabelText), $ResArray );
    }
    return $ResArray;
  }



  public static function getEmployersbusiness($SelectLabelValue= '', $SelectLabelText= '') {
    $ResArray= array (
    "188"=>"Abattoir", "001"=>"Accountancy", "189"=>"Acoustic Engineer", "190"=>"Actuarial Consultancy", "191"=>"Acupuncture", "881"=>"Addressing/Circularising Services", "192"=>"Adjuster", "002"=>"Advertising", "193"=>"Aerial Erector", "194"=>"Aerial Manufacturer", "195"=>"Aerial Photography", "196"=>"Aerial Supplier", "197"=>"Aerial Survey", "798"=>"Aerospace Industry", "198"=>"Agricultural Engineer", "882"=>"Agricultural Produce Distribution", "199"=>"Agriculture", "200"=>"Aircraft Construction", "201"=>"Aircraft Maintenance", "202"=>"Aircraft Repair", "203"=>"Airline", "906"=>"Airport Services", "204"=>"Ambulance Authority", "005"=>"Amusement", "006"=>"Amusement Arcade", "205"=>"Amusement Machine Supplier", "206"=>"Animal Breeding", "799"=>"Animal Feeds", "838"=>"Animal Rescue Home", "907"=>"Animal Training", "898"=>"Anthropology", "207"=>"Antique Restoration", "008"=>"Antiques", "901"=>"Arable Farming", "208"=>"Arbitration", "009"=>"Architecture", "163"=>"Armed Forces - Foreign", "162"=>"Armed Forces - Republic Of Ireland", "210"=>"Art", "211"=>"Art Gallery", "212"=>"Art Restoration", "213"=>"Art Valuation", "214"=>"Asphalt Contractor", "215"=>"Assessor", "216"=>"Astrology", "217"=>"Astronomy", "218"=>"Auction House", "014"=>"Auctioneer", "808"=>"Auditors", "929"=>"Aviva Staff", "219"=>"Baby Food Manufacturer", "220"=>"Baby Goods Manufacturer", "221"=>"Baby Goods Shop", "222"=>"Bagpipe Maker", "223"=>"Bakers Supplies", "224"=>"Bakery", "015"=>"Banking", "225"=>"Barrel Makers", "226"=>"Bathroom Design", "227"=>"Bathroom Installation", "171"=>"Beautician", "809"=>"Beauty Salon", "228"=>"Betting Shop", "839"=>"Bingo Hall", "229"=>"Blacksmith", "230"=>"Blast Cleaning", "231"=>"Blind Installation", "232"=>"Blind Manufacturer", "233"=>"Boarding Kennel", "234"=>"Boat Builder", "235"=>"Boat Hirer", "236"=>"Bookmaker - Off Course", "237"=>"Bookmaker - On Course", "238"=>"Booksellers", "239"=>"Bottled Gas Supplier", "240"=>"Brass Foundry", "241"=>"Breakdown Recovery", "242"=>"Breeding", "018"=>"Brewery", "243"=>"Brewery Transport", "244"=>"Brick Manufacturer", "245"=>"Brick Supplier", "019"=>"Broadcasting", "246"=>"Builder", "247"=>"Builders Merchant", "020"=>"Building Society", "021"=>"Building Trade", "248"=>"Business Consultancy", "822"=>"Business Systems", "249"=>"Business Training", "250"=>"Butchers", "251"=>"Cabinet Maker", "252"=>"Cable Manufacturer", "840"=>"Cable TV Installation", "253"=>"Cafe", "254"=>"Camp Site", "255"=>"Candle Dealer", "256"=>"Car Accessory Dealer", "257"=>"Car Delivery", "258"=>"Car Hire", "259"=>"Car Park Operator", "260"=>"Car Sales", "261"=>"Car Valeting", "262"=>"Caravan Hirer", "263"=>"Caravan Sales", "264"=>"Caravan Service", "265"=>"Caravan Site", "266"=>"Carpentry", "267"=>"Carpet Fitting", "268"=>"Cartography", "269"=>"Cash & Carry", "023"=>"Casino", "270"=>"Cask Maker", "884"=>"Catalogue Co-ordination", "883"=>"Catalogue Distribution", "024"=>"Catering - Licensed", "025"=>"Catering - Unlicensed", "271"=>"Cattery", "913"=>"Ceiling Contractor", "272"=>"Cement Suppliers", "273"=>"Central Heating Services", "026"=>"Charity", "274"=>"Chartering", "810"=>"Chemical Industry", "817"=>"Chemical Manufacturer", "275"=>"Chemist Shop", "854"=>"Child Minding", "276"=>"Childrens Panel", "277"=>"Chimney Sweeping", "278"=>"Chiropody", "279"=>"Choreography", "027"=>"Cinema", "028"=>"Circus", "893"=>"Citizens Advice Bureau", "280"=>"Civil Aviation", "281"=>"Civil Engineering", "029"=>"Civil Service", "282"=>"Cleaning Services", "885"=>"Clerical Services", "283"=>"Clock & Watch Manufacturer", "284"=>"Clock & Watch Repair", "858"=>"Clothing Manufacturer", "031"=>"Clothing Trade", "032"=>"Club", "285"=>"Coach Hirer", "286"=>"Coachbuilder", "033"=>"Coal Industry", "287"=>"Coal Merchant", "288"=>"Coffee Shop", "289"=>"Coin & Medal Dealer", "290"=>"Cold Store", "291"=>"Commissioners For Oaths", "034"=>"Commodities", "292"=>"Commodity Brokerage", "293"=>"Communications", "294"=>"Community Service", "818"=>"Computer Aided Design", "824"=>"Computer Distribution", "812"=>"Computer Sales", "295"=>"Computer Services", "886"=>"Computer Supplies", "181"=>"Computers", "035"=>"Computers - Hardware", "036"=>"Computers - Software", "296"=>"Concrete Supplier", "297"=>"Confectionery Manufacturer", "160"=>"Construction Industry", "298"=>"Consulting Engineering", "299"=>"Contact Lens Manufacturer", "300"=>"Container Hire", "301"=>"Conveyancers", "825"=>"Corporate Hospitality", "811"=>"Cosmetics", "302"=>"Costumiers", "303"=>"Cotton Mill", "304"=>"Courier Services", "305"=>"Crane Hire", "306"=>"Crane Manufacturer", "149"=>"Crop Spraying", "038"=>"Customs and Excise", "307"=>"Cutlery Craftsmen", "308"=>"Cycle Hire", "309"=>"Cycle Shop", "039"=>"Dairy", "900"=>"Dairy Farming", "310"=>"Data Processing", "311"=>"Data Protection", "911"=>"Dating Agency", "040"=>"Debt Collection", "312"=>"Decorating", "866"=>"Delicatessen", "313"=>"Delivery Service", "041"=>"Demolition", "042"=>"Dentistry", "314"=>"Department Store", "833"=>"Design Consultancy", "887"=>"Desktop Publishing Services", "315"=>"Despatch Services", "043"=>"Diplomatic Service", "044"=>"Discotheque", "316"=>"Distillers", "813"=>"Distribution", "317"=>"DIY Store", "318"=>"Dock Authority", "157"=>"Domestic Appliance Maintenance", "319"=>"Domestic Service", "172"=>"Double Glazing", "045"=>"Drapery", "320"=>"Driving Authority", "046"=>"Driving Instruction", "321"=>"Driving School", "800"=>"Dry Cleaning", "322"=>"Earth Removers", "897"=>"Ecology", "323"=>"Education", "047"=>"Education - Private", "138"=>"Education - State", "324"=>"Effluent Disposal", "325"=>"Egg Merchants", "823"=>"Electrical Appliance Manufacturer", "326"=>"Electrical Contractors", "834"=>"Electrical Goods Consultancy", "048"=>"Electricity Industry", "049"=>"Electronics", "327"=>"Embossers", "050"=>"Emergency Services", "328"=>"Employment Agency", "329"=>"Energy Consultancy", "139"=>"Engineering", "330"=>"Engineering Consultants", "182"=>"Engraving", "051"=>"Entertainment", "869"=>"Environmental Health", "052"=>"Estate Agency", "331"=>"Excavation", "332"=>"Exhibition Centre", "333"=>"Exhibition Organisers", "334"=>"Export", "335"=>"Export Agency", "336"=>"Exporter", "337"=>"Fabric Manufacturer", "867"=>"Fabrications", "053"=>"Fairground", "876"=>"Falconry", "168"=>"Fancy Goods", "338"=>"Fancy Goods Importer", "054"=>"Farming", "920"=>"Fascia Board Installer", "055"=>"Fashion", "339"=>"Fashion Design", "340"=>"Fashion House", "841"=>"Fast Food", "341"=>"Fencing Manufacturer", "342"=>"Ferry Service", "343"=>"Fertilizer Manufacturer", "344"=>"Fibre Glass Manufacturer", "345"=>"Filling Station", "346"=>"Film Manufacturing", "347"=>"Film Processing", "348"=>"Film Production", "349"=>"Finance Company", "350"=>"Financial Advisors", "056"=>"Financial Services", "351"=>"Fire Protection", "352"=>"Fireplace Installer", "353"=>"Fireplace Manufacturer", "354"=>"Fish & Chip Shop", "355"=>"Fish Farm", "150"=>"Fish Merchants", "842"=>"Fishing", "356"=>"Fishmonger", "914"=>"Fitted Bedroom Installer", "915"=>"Fitted Kitchen Installer", "916"=>"Flooring Construction", "918"=>"Flooring Installer", "917"=>"Flooring Services", "357"=>"Florists", "358"=>"Flying School", "359"=>"Food Exporter", "360"=>"Food Importer", "361"=>"Food Manufacturer", "814"=>"Food Production", "362"=>"Food Store", "363"=>"Football Pools", "364"=>"Forestry", "843"=>"Fostering/Adoption Agency", "365"=>"Foundry", "366"=>"Freezer Centre", "183"=>"Freight", "367"=>"Freight Agents", "815"=>"Freight Forwarders", "870"=>"French Polishing", "368"=>"Friendly Society", "871"=>"Frozen Food Distribution", "369"=>"Fuel Distribution", "370"=>"Fuel Merchant", "173"=>"Funeral Director", "371"=>"Fur Trade", "925"=>"Furniture Installer", "372"=>"Furniture Manufacture", "373"=>"Furniture Remover", "374"=>"Furniture Repair", "375"=>"Furniture Sales", "376"=>"Furniture Store", "377"=>"Furniture Storer", "378"=>"Furriers", "057"=>"Gambling", "379"=>"Game Breeders", "058"=>"Garage", "826"=>"Garden Centre", "380"=>"Gardening", "844"=>"Gas - Offshore", "381"=>"Gas Exploration", "059"=>"Gas Industry", "855"=>"Genealogy", "061"=>"General Dealer", "382"=>"General Store", "383"=>"Gift Shop", "384"=>"Glass Manufacture", "385"=>"Glaziers", "386"=>"Golf Club", "151"=>"Government - Foreign", "152"=>"Government - Republic Of Ireland", "388"=>"Gown Trade", "389"=>"Grain Merchants", "390"=>"Grain Mill", "895"=>"Graphic Design", "879"=>"Graphology", "391"=>"Greengrocer", "392"=>"Greeting Card Manufacturer", "393"=>"Greeting Card Sales", "394"=>"Greyhound Racing", "065"=>"Greyhounds", "395"=>"Grit Blasters", "396"=>"Grocer", "397"=>"Ground Maintenance", "829"=>"Guest House - Licensed", "830"=>"Guest House - Unlicensed", "919"=>"Guttering Installer", "066"=>"Hairdressing", "399"=>"Harbour Authority", "067"=>"Harbour Board", "400"=>"Hardware Manufacturer", "401"=>"Hardware Retailer", "174"=>"Haulage Contractors", "928"=>"Hazardous Chemical Transporters", "402"=>"Health Authority", "068"=>"Health Care - NHS", "069"=>"Health Care - Private", "403"=>"Health Centre", "404"=>"Health Club", "889"=>"Health Products Distribution", "405"=>"Heating Consultant", "406"=>"Heating Services", "407"=>"Hire Purchase", "409"=>"Hobby Shop", "410"=>"Holiday Accommodation", "070"=>"Holiday Camp", "411"=>"Holiday Centre", "412"=>"Home Help Services", "888"=>"Honey Producer", "071"=>"Horses", "184"=>"Horticulture", "413"=>"Hospital", "072"=>"Hotel - Licensed", "073"=>"Hotel - Unlicensed", "414"=>"House Builders", "927"=>"Househusband", "926"=>"Housewife", "415"=>"Housing Association", "416"=>"Housing Developers", "417"=>"Hypermarket", "074"=>"Ice Cream", "418"=>"Ice Cream Manufacturer", "419"=>"Ice Cream Shop", "420"=>"Ice Merchant", "421"=>"Ice Rink", "422"=>"Import", "423"=>"Importers", "424"=>"Industrial Building Manufacturer", "425"=>"Industrial Relations", "904"=>"Information Technology", "076"=>"Inland Revenue", "426"=>"Inn", "427"=>"Inspection Services", "428"=>"Instant Print Services", "429"=>"Insulation Engineers", "077"=>"Insurance", "430"=>"Insurance Advisor", "431"=>"Insurance Broker", "432"=>"Insurance Company", "433"=>"Insurance Consultant", "801"=>"Interior Design", "078"=>"Investment", "434"=>"Iron Foundry", "890"=>"Ironing Services", "435"=>"Ironmonger", "436"=>"Jam & Preserve Manufacturer", "437"=>"Jewellers", "185"=>"Jewellery", "438"=>"Jewellery Manufacturer", "439"=>"Joinery", "440"=>"Keep Fit", "441"=>"Kennels", "442"=>"Kitchen Equipment Manufacturer", "835"=>"Kitchen Manufacturer", "443"=>"Kitchen Planners", "444"=>"Knitwear Manufacturer", "445"=>"Laboratory", "446"=>"Ladder Manufacturer", "447"=>"Land Clearance", "448"=>"Land Draining", "449"=>"Landscaping", "450"=>"Lathe Manufacturer", "451"=>"Launderette", "153"=>"Laundry", "079"=>"Law and Order", "452"=>"Lawnmowers & Garden Machinery", "453"=>"Lawyers", "454"=>"Leasing Company", "455"=>"Legal System", "175"=>"Leisure Centre", "456"=>"Lens Manufacturer", "457"=>"Library", "458"=>"Lift Installers", "459"=>"Lift Maintenance", "460"=>"Lighting Installers", "461"=>"Linen Hire", "462"=>"Livery Stables", "463"=>"Livestock Carriers", "902"=>"Livestock Farming", "063"=>"Local Government", "464"=>"Local Government Authority", "465"=>"Local Newspaper", "466"=>"Locksmiths", "467"=>"Loft Insulation", "082"=>"Log And Firewood", "468"=>"Loss Adjusting", "469"=>"LPG Suppliers", "470"=>"Machine Tool Supplier", "471"=>"Machinery Dealers", "083"=>"Mail Order", "472"=>"Mail Order Supplier", "473"=>"Maintenance Services", "474"=>"Management Consultancy", "475"=>"Management Training", "476"=>"Mantle Trade", "084"=>"Manufacturing", "477"=>"Manufacturing Chemist", "802"=>"Marina", "478"=>"Market Garden", "085"=>"Market Gardeners", "868"=>"Market Research", "479"=>"Market Trading", "480"=>"Marketing", "481"=>"Marriage Guidance", "482"=>"Material Manufacturer", "483"=>"Mattress Manufacturer", "484"=>"Meat Market", "485"=>"Meat Products", "486"=>"Mechanical Handling", "803"=>"Medical research", "930"=>"Medical Services", "487"=>"Medical Suppliers", "086"=>"Merchant Navy", "488"=>"Metal Founders", "489"=>"Metal Treatment", "490"=>"Microfilm Services", "491"=>"Milk Delivery", "087"=>"Mining", "088"=>"Mobile Food", "492"=>"Mobile Shop", "875"=>"Model Manufacturer", "089"=>"Moneylenders", "493"=>"Monumental Masons", "845"=>"Motor Factor/Parts", "142"=>"Motor Manufacture", "143"=>"Motor Organisation", "090"=>"Motor Trade", "091"=>"Motorcar Racing", "092"=>"Motorcycle Racing", "903"=>"Motorcycle Trade", "494"=>"Motoring Organisation", "836"=>"Museums", "495"=>"Music Publisher", "496"=>"Music Retailer", "497"=>"Music School", "498"=>"Musical Instrument Manufacturer", "499"=>"National Newspaper", "856"=>"National Trust", "857"=>"National Trust For Scotland", "500"=>"Newsagents", "909"=>"Newspaper Wholesaler", "093"=>"Night Club", "819"=>"Noise Control", "186"=>"Not In Employment", "501"=>"Notaries", "095"=>"Nursery", "502"=>"Nursery School", "503"=>"Nursing Home", "846"=>"Oceanographic Survey", "096"=>"Off Licence", "504"=>"Off Licence Store", "921"=>"Office Equipment Repairer", "505"=>"Office Equipment Supplier", "506"=>"Office Fitters", "507"=>"Office Services", "847"=>"Oil - Offshore", "508"=>"Oil Company", "931"=>"Oil Distributors", "509"=>"Oil Exploration", "865"=>"Oil Terminal Operator", "510"=>"Opinion Polls", "097"=>"Optical Services", "511"=>"Optician", "512"=>"Optometrist", "513"=>"Organ Building", "905"=>"Outdoor Pursuits", "514"=>"Overall Hire & Maintenance", "515"=>"Packers And Storers", "516"=>"Paint Manufacturer", "517"=>"Paint Spraying", "098"=>"Painter And Decorator", "518"=>"Painting", "859"=>"Painting Contractor", "519"=>"Panel Beating", "804"=>"Paper Manufacture", "520"=>"Paperbag And Sack Manufacturer", "521"=>"Parcel Delivery", "923"=>"Partition Erector", "522"=>"Passenger Transport", "848"=>"Patent Office", "523"=>"Pawnbroker", "012"=>"Performing Arts", "524"=>"Personnel Consultancy", "525"=>"Pest And Vermin Control", "908"=>"Pet Services", "526"=>"Pet Shop", "100"=>"Petrochemical Industry", "527"=>"Petrol Pump Maintenance", "816"=>"Petrol Station", "528"=>"Pharmaceutical Supplier", "529"=>"Pharmacy", "530"=>"Photo Engraving", "531"=>"Photo Processing And Printing", "532"=>"Photographic Equipment Repairs", "101"=>"Photography", "533"=>"Physiotherapy", "534"=>"Piano Sales And Repairs", "535"=>"Piano Tuning", "872"=>"Picture Framing", "536"=>"Pig Farming", "537"=>"Pipe Cleaning", "538"=>"Pizza Delivery", "539"=>"Planning Consultancy", "102"=>"Plant Hire", "540"=>"Plant Manufacturer", "541"=>"Plant Sales", "542"=>"Plastic Sheeting Manufacturer", "805"=>"Plastics Manufacture", "543"=>"Playground Equipment Manufacturer", "544"=>"Plumbers Merchant", "103"=>"Plumbing", "896"=>"Plumbing & Heating", "545"=>"Political Consultancy", "546"=>"Political Party", "547"=>"Pollution Control", "548"=>"Pool Table Manufacturer", "549"=>"Pool Table Repairer", "550"=>"Pool Table Sales", "144"=>"Port Authority", "104"=>"Post Office", "552"=>"Potato Merchant", "553"=>"Pottery", "554"=>"Poultry Farm", "899"=>"Poultry Farming", "556"=>"Presentation Materials Supplier", "557"=>"Press Cutting Agency", "558"=>"Pressure Cleaning", "555"=>"Pre-Stressed Concrete Manufacturer", "559"=>"Print Type Services", "176"=>"Printer", "560"=>"Printing", "561"=>"Printing Engineering Services", "105"=>"Prison Service", "562"=>"Private Hire", "563"=>"Private Investigation", "564"=>"Private School", "565"=>"Process Engraving", "566"=>"Produce Importers", "567"=>"Project Management", "568"=>"Promotional Consultancy", "820"=>"Property Consultants", "106"=>"Property Developers", "821"=>"Property Letting", "569"=>"Property Owner", "107"=>"Property Services", "570"=>"Protective Clothing Manufacturer", "571"=>"Protective Clothing Supplier", "572"=>"Psychiatry", "573"=>"Psychology", "574"=>"Psychotherapy", "575"=>"Public Address System Supplier", "576"=>"Public Hire", "577"=>"Public Hirer", "578"=>"Public House", "579"=>"Public Relation Consultancy", "580"=>"Public School", "894"=>"Public Transport", "108"=>"Publishing", "109"=>"Publishing - Local Press", "110"=>"Publishing - National Press", "581"=>"Quality Assurance", "582"=>"Quantity Surveyors", "583"=>"Quarry", "111"=>"Quarrying", "584"=>"Race Course", "112"=>"Racehorses", "113"=>"Racing Or Rallies", "585"=>"Racing Stable", "586"=>"Radiator Repairs", "587"=>"Radiator Sales", "588"=>"Radiography", "589"=>"Rag Merchants", "114"=>"Railway", "590"=>"Rating And Valuation", "591"=>"Record Company", "592"=>"Record Shop", "593"=>"Recording Services", "594"=>"Recovery Services", "595"=>"Recruitment Agency", "596"=>"Recycling", "597"=>"Refrigeration", "598"=>"Refuse Collection", "599"=>"Reinforced Concrete Manufacturer", "115"=>"Religion", "177"=>"Removal Contractors", "600"=>"Remover And Storer", "601"=>"Rescue Services", "602"=>"Residential Home", "831"=>"Restaurant - Licensed", "832"=>"Restaurant - Unlicensed", "154"=>"Retailer - Mobile", "117"=>"Retailing", "932"=>"Retired", "603"=>"Riding School", "807"=>"Ring Sports", "604"=>"River Authority", "605"=>"Road Haulage", "606"=>"Road Repair", "607"=>"Road Surfacing", "849"=>"Robotics Manufacturer", "608"=>"Roller Shutter Manufacturer", "609"=>"Roofing Services", "610"=>"Rope Merchant", "611"=>"Roughcasters", "850"=>"RSPCA", "612"=>"Rubbish Disposal", "613"=>"Rubble Removers", "614"=>"Rustproofing Services", "615"=>"Saddlers And Harness Makers", "616"=>"Safe Installation And Removal", "617"=>"Safe Manufacturer", "618"=>"Safety Consultancy", "619"=>"Safety Equipment Supplier", "620"=>"Sail Makers And Repairers", "621"=>"Sailing Equipment Supplier", "622"=>"Salvage", "891"=>"Sample Distribution", "623"=>"Sample Distributors", "624"=>"Sand And Gravel Merchants", "625"=>"Sand Blasters", "626"=>"Satellite TV And Equipment Suppliers", "627"=>"Satellite TV Installers", "628"=>"Saw Sales And Repairs", "629"=>"Sawmill", "630"=>"Scaffolding Erection", "631"=>"Scaffolding Hire", "632"=>"School", "873"=>"School For Performing Arts", "851"=>"Scientific Research", "633"=>"Scrap Disposal", "634"=>"Scrap Metal Merchants", "635"=>"Screen Printing", "636"=>"Screenwriting", "637"=>"Sculptors", "638"=>"Secondhand Dealers", "639"=>"Secretarial Services", "640"=>"Secretarial Training", "860"=>"Security Equipment", "120"=>"Security Services", "641"=>"Seedsmen", "642"=>"Self Catering Accommodation", "643"=>"Self Drive Hire", "933"=>"Servicing", "644"=>"Sharpening Services", "645"=>"Sheet Metal Work", "646"=>"Sheriff Officers", "647"=>"Ship Building And Repair", "648"=>"Ship Chandlery", "649"=>"Ship Painting", "650"=>"Shipbroking", "651"=>"Shipping And Forwarding Agency", "652"=>"Shipping Company", "122"=>"Shipyard", "653"=>"Shoe Manufacturers", "654"=>"Shoe Repair", "655"=>"Shoe Shop", "656"=>"Shop Fitting", "657"=>"Shopping Centre", "658"=>"Shot Blasters", "659"=>"Shutter Manufacturer", "660"=>"Sightseeing Tours Operator", "661"=>"Sign Making", "123"=>"Signwriting", "662"=>"Site Clearance", "663"=>"Site Investigation", "664"=>"Skating Rink", "665"=>"Ski Centre", "666"=>"Skip Hire", "827"=>"Skip Maintenance", "667"=>"Slaughterhouse", "892"=>"Slimming Distribution", "668"=>"Snack Bar", "669"=>"Snooker Club", "670"=>"Social Club", "124"=>"Social Services", "671"=>"Soft Drinks Manufacturer", "672"=>"Soft Drinks Supplier", "673"=>"Soil Engineers", "674"=>"Solar Panel Manufacturer", "675"=>"Solar Panel Supplier", "178"=>"Solicitors", "676"=>"Sound Proofing", "677"=>"Spinners", "125"=>"Sports", "126"=>"Sports - Professional", "678"=>"Sports Centre", "679"=>"Sports Club", "680"=>"Sports Goods Shop", "681"=>"Sports Ground", "682"=>"Sports Promotion", "683"=>"Sportswear Manufacturer", "684"=>"Sportswear Supplier", "685"=>"Stables", "686"=>"Stamp Dealers", "687"=>"Stamp Manufacturer", "688"=>"Stamp Supplier Stationers", "874"=>"Stationers", "127"=>"Steel Industry", "689"=>"Steel Stockholders", "690"=>"Sterilising Services", "128"=>"Stockbroking", "691"=>"Stonemasonry", "692"=>"Street Trading", "693"=>"Structural Engineering", "694"=>"Studio", "179"=>"Supermarket", "695"=>"Surveying", "180"=>"Surveyors", "922"=>"Suspended Ceiling Repairer", "696"=>"Swimming Pool", "697"=>"Tailor And Outfitter", "698"=>"Take Away Food Supplier", "699"=>"Tank Cleaning Services", "700"=>"Tarpaulin Manufacturer", "701"=>"Tarpaulin Suppliers", "912"=>"Tattoo Parlour/Clinic", "702"=>"Tax Consultancy", "703"=>"Taxi Service", "704"=>"Taxidermy", "705"=>"Tea Importer", "706"=>"Tea Merchant", "707"=>"Tea Room", "708"=>"Technical College", "709"=>"Telecommunication Equipment Suppliers", "130"=>"Telecommunications", "710"=>"Telemarketing", "711"=>"Telephone Answering Service", "712"=>"Telesales", "166"=>"Television", "806"=>"Television Hire", "713"=>"Television Production", "714"=>"Television Repairer", "715"=>"Television Sales", "716"=>"Tent And Marquee Hirer", "837"=>"Territorial Army", "717"=>"Textile Manufacturer", "718"=>"Thatching", "167"=>"Theatre", "719"=>"Theatrical Suppliers", "853"=>"Theme Park", "720"=>"Tilers", "721"=>"Timber Importers", "156"=>"Timber Merchants", "722"=>"Timber Preservation", "723"=>"Timeshare Operators", "724"=>"Tobacco Importer", "725"=>"Tobacco Manufacturer", "726"=>"Tobacconist", "727"=>"Tomato Grower", "728"=>"Tool Hire", "729"=>"Tour Operator", "730"=>"Tourist Board", "731"=>"Tourist Information", "732"=>"Towel Supplier", "733"=>"Toy And Game Manufacturer", "734"=>"Toy And Game Supplier", "159"=>"Trade Association", "187"=>"Trade Demonstration", "158"=>"Trade Union", "735"=>"Trading Estate", "736"=>"Trading Standards Enforcement", "737"=>"Trailer Manufacturer", "738"=>"Trailer Supplier", "924"=>"Training Consultancy", "739"=>"Translators", "131"=>"Transport - PSV", "132"=>"Transport - Road", "145"=>"Travel And Tourism", "740"=>"Trinity House", "910"=>"Trust Company", "169"=>"Tupperware", "133"=>"Turf Accountants", "741"=>"TV And Radio", "852"=>"TV And Video Rental", "742"=>"Typewriter Services", "863"=>"Tyre Dealer", "864"=>"Tyre Manufacturer", "743"=>"Tyre Supplier And Fitting", "744"=>"Undertaker", "745"=>"Underwater Inspection", "746"=>"Underwriting Agency", "747"=>"Unemployed", "748"=>"Uniform Manufacturer", "749"=>"University", "000"=>"Un-specified", "750"=>"Upholstery", "751"=>"Vacuum Cleaner Repairs And Service", "752"=>"Valuation", "753"=>"Van And Lorry Hirer", "155"=>"Vehicle Hire - Self Drive", "754"=>"Vehicle Repairer", "934"=>"Vehicle Transporters", "755"=>"Vending Machine Manufacturer", "756"=>"Vending Machine Supplier", "828"=>"Vending Services", "757"=>"Veneering", "758"=>"Venetian Blind Manufacturer", "759"=>"Venetian Blind Supplier", "760"=>"Ventilation", "761"=>"Vermin Control", "134"=>"Veterinary", "762"=>"Veterinary Supplies", "763"=>"Veterinary Surgeons", "135"=>"Video Hire", "764"=>"Video Services", "765"=>"Vineyard", "766"=>"Voluntary Organisation", "767"=>"Wallpaper Manufacturer/Supplier", "768"=>"Washing Machine Repairs And Servicing", "769"=>"Waste Disposal", "770"=>"Watchmaker", "136"=>"Water Industry", "771"=>"Water Sports Centre", "772"=>"Waterproof Cover Manufacturer", "773"=>"Waterproof Cover Supplier", "774"=>"Weather Forecasting", "775"=>"Weather Services", "776"=>"Weights And Measures", "777"=>"Welding", "778"=>"Welfare Organisation", "779"=>"Whisky Blenders", "137"=>"Wholesaler", "780"=>"Window Cleaning", "781"=>"Wine And Spirit Merchants", "782"=>"Wine Bar", "783"=>"Wine Makers", "784"=>"Wire Rope Manufacturer", "785"=>"Wood Carving", "786"=>"Wood Preservation", "880"=>"Woodshavings Contractor", "787"=>"Woodworking", "788"=>"Woodworm And Dry Rot Control", "789"=>"Woollen Goods Manufacturer", "790"=>"Woollen Goods Shop", "791"=>"Woollen Mill", "792"=>"Wrought Iron Manufacturer", "793"=>"Yacht Building", "794"=>"Yacht Chandlery", "877"=>"Yacht Service And Management", "795"=>"Yarn Spinning", "796"=>"Youth Hostel Organisation", "878"=>"Youth Organisation", "797"=>"Zoo Operator",
    );
    if ( !empty($SelectLabelText) ) {
      $ResArray= Util::SetArrayHeader( array($SelectLabelValue=>$SelectLabelText), $ResArray );
    }
    return $ResArray;
  }


  public static function getEmploymentstatus($SelectLabelValue= '', $SelectLabelText= '') {
    $ResArray= array (    
    "E"=>"Employed", "H"=>"Household Duties", "R"=>"Retired", "S"=>"Self Employed", "U"=>"Unemployed",
    );
    if ( !empty($SelectLabelText) ) {
      $ResArray= Util::SetArrayHeader( array($SelectLabelValue=>$SelectLabelText), $ResArray );
    }
    return $ResArray;
  }



  public static function getLicencetype($SelectLabelValue= '', $SelectLabelText= '') {
    $ResArray= array (
      "I"=>"Foreign", "F"=>"Full EU", "C"=>"Full Irish", "D"=>"Full UK", "N"=>"International Licence", "B"=>"Provisional Irish",
    );
    if ( !empty($SelectLabelText) ) {
      $ResArray= Util::SetArrayHeader( array($SelectLabelValue=>$SelectLabelText), $ResArray );
    }
    return $ResArray;
  }


  public static function getddlHearAboutUs($SelectLabelValue= '', $SelectLabelText= '') {
    $ResArray= array (
    "21"=>"Applegreen",
    "20"=>"Facebook",
    "3"=>"Friend",
    "2"=>"Google Search",
    "7"=>"Radio",
    "5"=>"Web Search",
    "8"=>"Other",
    "6"=>"Other Print",
    );
    if ( !empty($SelectLabelText) ) {
      $ResArray= Util::SetArrayHeader( array($SelectLabelValue=>$SelectLabelText), $ResArray );
    }
    return $ResArray;
  }


  public static function getLicencecountry($SelectLabelValue= '', $SelectLabelText= '') {
    $ResArray= array (
    "AF"=>"Afghanistan", "AX"=>"Aland Islands", "AL"=>"Albania", "DZ"=>"Algeria", "AS"=>"American Samoa", "AD"=>"Andorra", "AO"=>"Angola", "AI"=>"Anguilla", "AQ"=>"Antarctica", "AG"=>"Antigua And Barbuda", "AR"=>"Argentina", "AM"=>"Armenia", "AW"=>"Aruba", "AU"=>"Australia", "AT"=>"Austria", "AZ"=>"Azerbaijan", "BS"=>"Bahamas", "BH"=>"Bahrain", "BD"=>"Bangladesh", "BB"=>"Barbados", "BY"=>"Belarus", "BE"=>"Belgium", "BZ"=>"Belize", "BJ"=>"Benin", "BM"=>"Bermuda", "BT"=>"Bhutan", "BO"=>"Bolivia", "BA"=>"Bosnia and Herzegovina", "BW"=>"Botswana", "BV"=>"Bouvet Island", "BR"=>"Brazil", "IO"=>"British Indian Ocean Territory", "BN"=>"Brunei Darussalam", "BG"=>"Bulgaria", "BF"=>"Burkina Faso", "BI"=>"Burundi", "KH"=>"Cambodia", "CM"=>"Cameroon", "CA"=>"Canada", "CAI"=>"Canary Islands", "CV"=>"Cape Verde", "KY"=>"Cayman Islands", "CF"=>"Central African Republic", "TD"=>"Chad", "CL"=>"Chile", "CN"=>"China", "CX"=>"Christmas Island", "CC"=>"Cocos (Keeling) Islands", "CO"=>"Colombia", "KM"=>"Comoros", "CG"=>"Congo", "CD"=>"Congo, The Democratic Republic of the", "CK"=>"Cook Islands", "CR"=>"Costa Rica", "CI"=>"Cote D'ivoire", "HR"=>"Croatia", "CU"=>"Cuba", "CY"=>"Cyprus", "CZ"=>"Czech Republic", "DK"=>"Denmark", "DJ"=>"Djibouti", "DM"=>"Dominica", "DO"=>"Dominican Republic", "EC"=>"Ecuador", "EG"=>"Egypt", "SV"=>"El Salvador", "GQ"=>"Equatorial Guinea", "ER"=>"Eritrea", "EE"=>"Estonia", "ET"=>"Ethiopia", "EUR"=>"European Union", "FK"=>"Falkland Islands (Malvinas)", "FO"=>"Faroe Islands", "FJ"=>"Fiji", "FI"=>"Finland", "FR"=>"France", "GF"=>"French Guiana", "PF"=>"French Polynesia", "TF"=>"French Southern Territories", "GA"=>"Gabon", "GM"=>"Gambia", "GE"=>"Georgia", "DE"=>"Germany", "GH"=>"Ghana", "GI"=>"Gibraltar", "GR"=>"Greece", "GL"=>"Greenland", "GD"=>"Grenada", "GP"=>"Guadeloupe", "GU"=>"Guam", "GT"=>"Guatemala", "GN"=>"Guinea", "GW"=>"Guinea Bissau", "GY"=>"Guyana", "HT"=>"Haiti", "HM"=>"Heard Island And Mcdonald Islands", "VA"=>"Holy See (Vatican City State)", "HN"=>"Honduras", "HK"=>"Hong Kong", "HU"=>"Hungary", "IS"=>"Iceland", "IN"=>"India", "ID"=>"Indonesia", "IR"=>"Iran, Islamic Republic Of", "IQ"=>"Iraq", "IE"=>"Ireland", "IL"=>"Israel", "IT"=>"Italy", "JM"=>"Jamaica", "JP"=>"Japan", "JO"=>"Jordan", "KZ"=>"Kazakhstan", "KE"=>"Kenya", "KI"=>"Kiribati", "KP"=>"Korea, Democratic People's Republic Of", "KR"=>"Korea, Republic Of", "KW"=>"Kuwait", "KG"=>"Kyrgyztan", "LA"=>"Lao People's Democratic Republic", "LV"=>"Latvia", "LB"=>"Lebanon", "LS"=>"Lesotho", "LR"=>"Liberia", "LY"=>"Libyan Arab Jamahiriya", "LI"=>"Liechtenstein", "LT"=>"Lithuania", "LU"=>"Luxemburg", "MO"=>"Macao", "MK"=>"Macedonia, The Former Yugoslav Republic of", "MG"=>"Madagascar", "MAD"=>"Madeira", "MW"=>"Malawi", "MY"=>"Malaysia", "MV"=>"Maldives", "ML"=>"Mali", "MT"=>"Malta", "MH"=>"Marshall Islands", "MQ"=>"Martinique", "MR"=>"Mauritania", "MU"=>"Mauritius", "YT"=>"Mayotte", "MX"=>"Mexico", "FM"=>"Micronesia, Federated States of", "MD"=>"Moldova, Republic of", "MC"=>"Monaco", "MN"=>"Mongolia", "MS"=>"Montserrat", "MA"=>"Morocco", "MZ"=>"Mozambique", "MM"=>"Myanmar", "NA"=>"Namibia", "NR"=>"Nauru", "NP"=>"Nepal", "NL"=>"Netherlands", "AN"=>"Netherlands Antilles", "NC"=>"New Caledonia", "NZ"=>"New Zealand", "NI"=>"Nicaragua", "NE"=>"Niger", "NG"=>"Nigeria", "NU"=>"Niue", "NF"=>"Norfolk Island", "MP"=>"Northern Mariana Island", "NO"=>"Norway", "OM"=>"Oman", "99"=>"Other", "98"=>"Other EU Country", "PK"=>"Pakistan", "PW"=>"Palau", "PS"=>"Palestinian Territory, Occupied", "PA"=>"Panama", "PG"=>"Papua New Guinea", "PY"=>"Paraguay", "PE"=>"Peru", "PH"=>"Philippines", "PN"=>"Pitcairn", "PL"=>"Poland", "PT"=>"Portugal", "PR"=>"Puerto Rico", "QA"=>"Qatar", "RE"=>"Reunion", "RO"=>"Romania", "RU"=>"Russian Federation", "RW"=>"Rwanda", "SH"=>"Saint Helena", "KN"=>"Saint Kitts And Nevis", "LC"=>"Saint Lucia", "PM"=>"Saint Pierre Et Miquelon", "VC"=>"Saint Vincent And The Grenadines", "WS"=>"Samoa", "SM"=>"San Marino", "ST"=>"Sao Tome And Principe", "SA"=>"Saudi Arabia", "SN"=>"Senegal", "CS"=>"Serbia and Montenegro", "SC"=>"Seychelles", "SL"=>"Sierra Leone", "SG"=>"Singapore", "SK"=>"Slovakia", "SI"=>"Slovenia", "SB"=>"Solomon Islands", "SO"=>"Somalia", "ZA"=>"South Africa", "GS"=>"South Georgia & the South Sandwich Islands", "ES"=>"Spain", "LK"=>"Sri Lanka", "SD"=>"Sudan", "SR"=>"Suriname", "SJ"=>"Svalbard And Jan Mayen", "SZ"=>"Swaziland", "SE"=>"Sweden", "CH"=>"Switzerland", "SY"=>"Syrian Arab Republic", "TW"=>"Taiwan Province Of China", "TJ"=>"Tajikistan", "TZ"=>"Tanzania, United Republic of", "TH"=>"Thailand", "TL"=>"Timor-Leste", "TG"=>"Togo", "TK"=>"Tokelau", "TO"=>"Tonga", "TT"=>"Trinidad And Tobago", "TN"=>"Tunisia", "TR"=>"Turkey", "TM"=>"Turkmenistan", "TC"=>"Turks And Caicos Islands", "TV"=>"Tuvalu", "UG"=>"Uganda", "UA"=>"Ukraine", "AE"=>"United Arab Emirates", "GB"=>"United Kingdom", "US"=>"United States", "UM"=>"United States Minor Outlying Islands", "UY"=>"Uruguay", "UZ"=>"Uzbekistan", "VU"=>"Vanuatu", "VE"=>"Venezuela", "VN"=>"Viet Nam", "VG"=>"Virgin Islands, British", "VI"=>"Virgin Islands, U.S.", "WF"=>"Wallis And Futuna", "EH"=>"Western Sahara", "YE"=>"Yemen", "ZM"=>"Zambia", "ZW"=>"Zimbabwe",    
    );
    if ( !empty($SelectLabelText) ) {
      $ResArray= Util::SetArrayHeader( array($SelectLabelValue=>$SelectLabelText), $ResArray );
    }
    return $ResArray;
  }




  public static function getLicenceyearsheld($SelectLabelValue= '', $SelectLabelText= '') {
    for( $I= DataLoadAPI::$Licenceyearsheld_Min; $I<= DataLoadAPI::$Licenceyearsheld_Max; $I++ ) {
      $ResArray[$I]= $I . ( $I == DataLoadAPI::$Licenceyearsheld_Max ? "+" : "" ) ;
    }
    if ( !empty($SelectLabelText) ) {
      $ResArray= Util::SetArrayHeader( array($SelectLabelValue=>$SelectLabelText), $ResArray );
    }
    return $ResArray;
  }

  public static function getPolicyholderPenaltypoints($SelectLabelValue= '', $SelectLabelText= '') {
    for( $I= DataLoadAPI::$PolicyholderPenaltypoints_Min; $I<= DataLoadAPI::$PolicyholderPenaltypoints_Max; $I++ ) {
      $ResArray[$I]= $I . ( $I == DataLoadAPI::$PolicyholderPenaltypoints_Max ? "" : "" ) ;
    }
    if ( !empty($SelectLabelText) ) {
      $ResArray= Util::SetArrayHeader( array($SelectLabelValue=>$SelectLabelText), $ResArray );
    }
    return $ResArray;
  }

  public static function getPolicyholderPenaltypointsOffences($SelectLabelValue= '', $SelectLabelText= '') {
    for( $I= DataLoadAPI::$PolicyholderPenaltypoints_Min; $I<= DataLoadAPI::$PolicyholderPenaltypoints_Max; $I++ ) {
      $ResArray[$I]= $I . ( $I == DataLoadAPI::$PolicyholderPenaltypoints_Max ? "" : "" ) ;
    }
    if ( !empty($SelectLabelText) ) {
      $ResArray= Util::SetArrayHeader( array($SelectLabelValue=>$SelectLabelText), $ResArray );
    }
    return $ResArray;
  }


  public static function getPolicyholderOffenceType($SelectLabelValue= '', $SelectLabelText= '') {
    $ResArray= array (
    "1"=>"Exceeding weight",
    "2"=>"Parking in dangerous position",
    "3"=>"No insurance",
    "4"=>"Failing to stop at warden",
    "5"=>"Breach of duty at accident",
    "6"=>"Failing to stop for Garda",
    "7"=>"Giving false details for licence",
    "8"=>"Vehicle exceeds width limit",
    "9"=>"Vehicle exceeds length limit",
    "10"=>"Worn tyres",
    "11"=>"Poor visibility of road",
    "12"=>"No test cert",
    "13"=>"Windscreen not safety glass",
    "14"=>"Poor windscreen wipers",
    "15"=>"No driving mirror",
    "16"=>"Poor brakes",
    "17"=>"Poor seat belt anchorage points",
    "18"=>"No seat belts",
    "19"=>"No crash helmet on motorbike",
    "20"=>"No passenger crash helmet",
    "21"=>"Truck with no rear crash bar",
    "22"=>"Truck with no side crash bar",
    "23"=>"No licence",
    "24"=>"Driving without seat belt",
    "25"=>"Rear passengers with no belt",
    "26"=>"No speed limiting device",
    "27"=>"Speed limiter not calibrated",
    "28"=>"Unsealed speed limiter",
    "29"=>"Using mobile phone while driving",
    "30"=>"Vehicle not equipped with lamps",
    "31"=>"Trailer  not equipped with lamps",
    "32"=>"Non use of projecting road lamps",
    "33"=>"Non use of trailer marker lamps",
    "34"=>"Trying to get licence while disqualified",
    "35"=>"No internal lighting in PSV",
    "36"=>"No indicators",
    "37"=>"Breaking speed restriction",
    "38"=>"Failure to yield right of way",
    "39"=>"Failure to drive on left",
    "40"=>"Dangerous overtaking",
    "41"=>"Poor driving at junction",
    "42"=>"Poor reversing",
    "43"=>"Breach of footway driving rules",
    "44"=>"Failure to enter roundabout on left",
    "45"=>"Failure to produce licence",
    "46"=>"Driving along or across median strip",
    "47"=>"Failure to comply with Garda signals",
    "48"=>"Failure to stop at stop sign",
    "49"=>"Failure to comply with traffic sign",
    "50"=>"Failure to keep left at certain signs",
    "51"=>"Crossing white line",
    "52"=>"Entering marked hatched area",
    "53"=>"Breach of lane markings",
    "54"=>"Failure to obey traffic lights",
    "55"=>"Failure to obey level crossing lights",
    "56"=>"Exceeding speed limit",
    "57"=>"Driving against motorway flow",
    "58"=>"Driving on banned areas of motorway",
    "59"=>"Driving vehicle restricted to 50 mph on outside lane of motorway",
    "60"=>"Failure to heed height restrictions",
    "61"=>"Provisional licence holder driving without supervision of qualified driver",
    "62"=>"Other",
    "63"=>"Driving vehicle when unfit",
    "64"=>"Insurer Penalty Point Update",
    "65"=>"Careless driving",
    "66"=>"Driving defective vehicle",
    );
    if ( !empty($SelectLabelText) ) {
      $ResArray= Util::SetArrayHeader( array($SelectLabelValue=>$SelectLabelText), $ResArray );
    }
    return $ResArray;
  }


  public static function getTravelpassprovider($SelectLabelValue= '', $SelectLabelText= '') {
    $ResArray= array (
    "1"=>"Bus Eireann",
    "2"=>"Dublin Bus",
    "3"=>"Irish Rail",
    "4"=>"Luas",
    );
    if ( !empty($SelectLabelText) ) {
      $ResArray= Util::SetArrayHeader( array($SelectLabelValue=>$SelectLabelText), $ResArray );
    }
    return $ResArray;
  }

  public static function getAdvdrivingcourseCourse($SelectLabelValue= '', $SelectLabelText= '') {
    $ResArray= array (
    "1"=>"Ignition Course Pass",
    "2"=>"Ignition Course Pass &amp; Curfew",
    "3"=>"Ignition Course Merit",
    "4"=>"Ignition Course Merit &amp; Curfew",
    "5"=>"Ignition Course Distinction",
    "6"=>"Ignition Course Distinction &amp; Curfew",
    "7"=>"IAM UK Advanced Driving Course",
    "8"=>"ROSPA Advanced Driving Course",
    "9"=>"Curfew",
    "10"=>"Primary Assessment",
    "11"=>"AF154 Licence Qualification",
    "12"=>"Driving School Module 1",
    "13"=>"Driving School Module 2",
    "14"=>"Driving School Module 3",
    "15"=>"Driving School Advanced Driving Course",
    "16"=>"Driving School Module 4",
    );
    if ( !empty($SelectLabelText) ) {
      $ResArray= Util::SetArrayHeader( array($SelectLabelValue=>$SelectLabelText), $ResArray );
    }
    return $ResArray;
  }


  public static function getNcdcountry($SelectLabelValue= '', $SelectLabelText= '') {
    $ResArray= array (
    "AF"=>"Afghanistan", "AX"=>"Aland Islands", "AL"=>"Albania", "DZ"=>"Algeria", "AS"=>"American Samoa", "AD"=>"Andorra", "AO"=>"Angola", "AI"=>"Anguilla", "AQ"=>"Antarctica", "AG"=>"Antigua And Barbuda", "AR"=>"Argentina", "AM"=>"Armenia", "AW"=>"Aruba", "AU"=>"Australia", "AT"=>"Austria", "AZ"=>"Azerbaijan", "BS"=>"Bahamas", "BH"=>"Bahrain", "BD"=>"Bangladesh", "BB"=>"Barbados", "BY"=>"Belarus", "BE"=>"Belgium", "BZ"=>"Belize", "BJ"=>"Benin", "BM"=>"Bermuda", "BT"=>"Bhutan", "BO"=>"Bolivia", "BA"=>"Bosnia and Herzegovina", "BW"=>"Botswana", "BV"=>"Bouvet Island", "BR"=>"Brazil", "IO"=>"British Indian Ocean Territory", "BN"=>"Brunei Darussalam", "BG"=>"Bulgaria", "BF"=>"Burkina Faso", "BI"=>"Burundi", "KH"=>"Cambodia", "CM"=>"Cameroon", "CA"=>"Canada", "CAI"=>"Canary Islands", "CV"=>"Cape Verde", "KY"=>"Cayman Islands", "CF"=>"Central African Republic", "TD"=>"Chad", "CL"=>"Chile", "CN"=>"China", "CX"=>"Christmas Island", "CC"=>"Cocos (Keeling) Islands", "CO"=>"Colombia", "KM"=>"Comoros", "CG"=>"Congo", "CD"=>"Congo, The Democratic Republic of the", "CK"=>"Cook Islands", "CR"=>"Costa Rica", "CI"=>"Cote D'ivoire", "HR"=>"Croatia", "CU"=>"Cuba", "CY"=>"Cyprus", "CZ"=>"Czech Republic", "DK"=>"Denmark", "DJ"=>"Djibouti", "DM"=>"Dominica", "DO"=>"Dominican Republic", "EC"=>"Ecuador", "EG"=>"Egypt", "SV"=>"El Salvador", "GQ"=>"Equatorial Guinea", "ER"=>"Eritrea", "EE"=>"Estonia", "ET"=>"Ethiopia", "EUR"=>"European Union", "FK"=>"Falkland Islands (Malvinas)", "FO"=>"Faroe Islands", "FJ"=>"Fiji", "FI"=>"Finland", "FR"=>"France", "GF"=>"French Guiana", "PF"=>"French Polynesia", "TF"=>"French Southern Territories", "GA"=>"Gabon", "GM"=>"Gambia", "GE"=>"Georgia", "DE"=>"Germany", "GH"=>"Ghana", "GI"=>"Gibraltar", "GR"=>"Greece", "GL"=>"Greenland", "GD"=>"Grenada", "GP"=>"Guadeloupe", "GU"=>"Guam", "GT"=>"Guatemala", "GN"=>"Guinea", "GW"=>"Guinea Bissau", "GY"=>"Guyana", "HT"=>"Haiti", "HM"=>"Heard Island And Mcdonald Islands", "VA"=>"Holy See (Vatican City State)", "HN"=>"Honduras", "HK"=>"Hong Kong", "HU"=>"Hungary", "IS"=>"Iceland", "IN"=>"India", "ID"=>"Indonesia", "IR"=>"Iran, Islamic Republic Of", "IQ"=>"Iraq", "IE"=>"Ireland", "IL"=>"Israel", "IT"=>"Italy", "JM"=>"Jamaica", "JP"=>"Japan", "JO"=>"Jordan", "KZ"=>"Kazakhstan", "KE"=>"Kenya", "KI"=>"Kiribati", "KP"=>"Korea, Democratic People's Republic Of", "KR"=>"Korea, Republic Of", "KW"=>"Kuwait", "KG"=>"Kyrgyztan", "LA"=>"Lao People's Democratic Republic", "LV"=>"Latvia", "LB"=>"Lebanon", "LS"=>"Lesotho", "LR"=>"Liberia", "LY"=>"Libyan Arab Jamahiriya", "LI"=>"Liechtenstein", "LT"=>"Lithuania", "LU"=>"Luxemburg", "MO"=>"Macao", "MK"=>"Macedonia, The Former Yugoslav Republic of", "MG"=>"Madagascar", "MAD"=>"Madeira", "MW"=>"Malawi", "MY"=>"Malaysia", "MV"=>"Maldives", "ML"=>"Mali", "MT"=>"Malta", "MH"=>"Marshall Islands", "MQ"=>"Martinique", "MR"=>"Mauritania", "MU"=>"Mauritius", "YT"=>"Mayotte", "MX"=>"Mexico", "FM"=>"Micronesia, Federated States of", "MD"=>"Moldova, Republic of", "MC"=>"Monaco", "MN"=>"Mongolia", "MS"=>"Montserrat", "MA"=>"Morocco", "MZ"=>"Mozambique", "MM"=>"Myanmar", "NA"=>"Namibia", "NR"=>"Nauru", "NP"=>"Nepal", "NL"=>"Netherlands", "AN"=>"Netherlands Antilles", "NC"=>"New Caledonia", "NZ"=>"New Zealand", "NI"=>"Nicaragua", "NE"=>"Niger", "NG"=>"Nigeria", "NU"=>"Niue", "NF"=>"Norfolk Island", "MP"=>"Northern Mariana Island", "NO"=>"Norway", "OM"=>"Oman", "99"=>"Other", "98"=>"Other EU Country", "PK"=>"Pakistan", "PW"=>"Palau", "PS"=>"Palestinian Territory, Occupied", "PA"=>"Panama", "PG"=>"Papua New Guinea", "PY"=>"Paraguay", "PE"=>"Peru", "PH"=>"Philippines", "PN"=>"Pitcairn", "PL"=>"Poland", "PT"=>"Portugal", "PR"=>"Puerto Rico", "QA"=>"Qatar", "RE"=>"Reunion", "RO"=>"Romania", "RU"=>"Russian Federation", "RW"=>"Rwanda", "SH"=>"Saint Helena", "KN"=>"Saint Kitts And Nevis", "LC"=>"Saint Lucia", "PM"=>"Saint Pierre Et Miquelon", "VC"=>"Saint Vincent And The Grenadines", "WS"=>"Samoa", "SM"=>"San Marino", "ST"=>"Sao Tome And Principe", "SA"=>"Saudi Arabia", "SN"=>"Senegal", "CS"=>"Serbia and Montenegro", "SC"=>"Seychelles", "SL"=>"Sierra Leone", "SG"=>"Singapore", "SK"=>"Slovakia", "SI"=>"Slovenia", "SB"=>"Solomon Islands", "SO"=>"Somalia", "ZA"=>"South Africa", "GS"=>"South Georgia & the South Sandwich Islands", "ES"=>"Spain", "LK"=>"Sri Lanka", "SD"=>"Sudan", "SR"=>"Suriname", "SJ"=>"Svalbard And Jan Mayen", "SZ"=>"Swaziland", "SE"=>"Sweden", "CH"=>"Switzerland", "SY"=>"Syrian Arab Republic", "TW"=>"Taiwan Province Of China", "TJ"=>"Tajikistan", "TZ"=>"Tanzania, United Republic of", "TH"=>"Thailand", "TL"=>"Timor-Leste", "TG"=>"Togo", "TK"=>"Tokelau", "TO"=>"Tonga", "TT"=>"Trinidad And Tobago", "TN"=>"Tunisia", "TR"=>"Turkey", "TM"=>"Turkmenistan", "TC"=>"Turks And Caicos Islands", "TV"=>"Tuvalu", "UG"=>"Uganda", "UA"=>"Ukraine", "AE"=>"United Arab Emirates", "GB"=>"United Kingdom", "US"=>"United States", "UM"=>"United States Minor Outlying Islands", "UY"=>"Uruguay", "UZ"=>"Uzbekistan", "VU"=>"Vanuatu", "VE"=>"Venezuela", "VN"=>"Viet Nam", "VG"=>"Virgin Islands, British", "VI"=>"Virgin Islands, U.S.", "WF"=>"Wallis And Futuna", "EH"=>"Western Sahara", "YE"=>"Yemen", "ZM"=>"Zambia", "ZW"=>"Zimbabwe",    
    );
    if ( !empty($SelectLabelText) ) {
      $ResArray= Util::SetArrayHeader( array($SelectLabelValue=>$SelectLabelText), $ResArray );
    }
    return $ResArray;
  }


  public static function getOtherpolicyExptype($SelectLabelValue= '', $SelectLabelText= '') {
    $ResArray= array (
    "1"=>"Full-time Company Car",
    "2"=>"Named Driver Experience",
    "3"=>"Insurance in Own Name",
    "4"=>"Part-time Company Car Experience",
    );
    if ( !empty($SelectLabelText) ) {
      $ResArray= Util::SetArrayHeader( array($SelectLabelValue=>$SelectLabelText), $ResArray );
    }
    return $ResArray;
  }

  public static function getOtherpolicyRelationship2policyholder($SelectLabelValue= '', $SelectLabelText= '') {
    $ResArray= array (
    "18"=>"Spouse",
    "4"=>"Common Law Spouse",
    "22"=>"Partner",
    "16"=>"Son",
    "5"=>"Daughter",
    "11"=>"Parent",
    "1"=>"Brother",
    "14"=>"Sister",
    "17"=>"Son in Law",
    "6"=>"Daughter in Law",
    "9"=>"Father in Law",
    "10"=>"Mother In Law",
    "2"=>"Brother in Law",
    "15"=>"Sister in Law",
    "13"=>"Relative",
    "12"=>"Proposer",
    "7"=>"Employee of Proposer",
    "8"=>"Employer of Proposer",
    "3"=>"Business Partner",
    "19"=>"Tenant",
    "21"=>"Friend",
    "20"=>"Unrelated",
    );
    if ( !empty($SelectLabelText) ) {
      $ResArray= Util::SetArrayHeader( array($SelectLabelValue=>$SelectLabelText), $ResArray );
    }
    return $ResArray;
  }


  public static function getOtherpolicyHowmanyyears($SelectLabelValue= '', $SelectLabelText= '') {
    for( $I= DataLoadAPI::$OtherpolicyHowmanyyears_Min; $I<= DataLoadAPI::$OtherpolicyHowmanyyears_Max; $I++ ) {
      $ResArray[$I]= $I . ( $I == DataLoadAPI::$OtherpolicyHowmanyyears_Max ? "+" : "" ) ;
    }
    if ( !empty($SelectLabelText) ) {
      $ResArray= Util::SetArrayHeader( array($SelectLabelValue=>$SelectLabelText), $ResArray );
    }
    return $ResArray;
  }


  public static function getSpouseFlltimeuseofothercar($SelectLabelValue= '', $SelectLabelText= '') {
    $ResArray= array (
    "true"=>"Yes",
    "false"=>"No",
    );
    if ( !empty($SelectLabelText) ) {
      $ResArray= Util::SetArrayHeader( array($SelectLabelValue=>$SelectLabelText), $ResArray );
    }
    return $ResArray;
  }

  public static function getSpouseResidewithpolicyholder($SelectLabelValue= '', $SelectLabelText= '') {
    $ResArray= array (
    "true"=>"Yes",
    "false"=>"No",
    );
    if ( !empty($SelectLabelText) ) {
      $ResArray= Util::SetArrayHeader( array($SelectLabelValue=>$SelectLabelText), $ResArray );
    }
    return $ResArray;
  }


  public static function getSpouseRelationship2policyholder($SelectLabelValue= '', $SelectLabelText= '') {
    $ResArray= array (
    "18"=>"Spouse",
    "4"=>"Common Law Spouse",
    "22"=>"Partner",
    "16"=>"Son",
    "5"=>"Daughter",
    "11"=>"Parent",
    "1"=>"Brother",
    "14"=>"Sister",
    "17"=>"Son in Law",
    "6"=>"Daughter in Law",
    "9"=>"Father in Law",
    "10"=>"Mother In Law",
    "2"=>"Brother in Law",
    "15"=>"Sister in Law",
    "13"=>"Relative",
    "12"=>"Proposer",
    "7"=>"Employee of Proposer",
    "8"=>"Employer of Proposer",
    "3"=>"Business Partner",
    "19"=>"Tenant",
    "21"=>"Friend",
    "20"=>"Unrelated",
    );
    if ( !empty($SelectLabelText) ) {
      $ResArray= Util::SetArrayHeader( array($SelectLabelValue=>$SelectLabelText), $ResArray );
    }
    return $ResArray;
  }

}

