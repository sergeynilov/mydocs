<?php

require 'lib/model/om/BaseInventoryItemPeer.php';


/**
 * Skeleton subclass for performing query and update operations on the 'inventory_item' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    lib.model
 */
class InventoryItemPeer extends BaseInventoryItemPeer {

  public static function getInventoryItemsCountByInventoryCategory( $filter_inventory_category= '' ) {
    $c = new Criteria();

   	$c->addJoin( InventoryItemPeer::INVENTORY_CATEGORY, InventoryCategoryPeer::PRODUCT_LINE, Criteria::LEFT_JOIN );
    $c->add( InventoryCategoryPeer::CATEGORY, $filter_inventory_category );

    return InventoryItemPeer::doCount( $c );
  }

  public static function getInventoryItemsByInventoryCategory( $page=1, $ReturnPager=true, $ReturnCount= false, $filter_inventory_category= '',
  $filter_matrix= '', $rows_in_pager= '' ) {
    $c = new Criteria();

    $c->add( InventoryItemPeer::INVENTORY_CATEGORY, $filter_inventory_category );

    if ( strlen($filter_matrix)> 0 and (int)$filter_matrix == 1 ) {
      $Matrix_Cond= $c->getNewCriterion( InventoryItemPeer::MATRIX, false );
      $RootItem_Cond= $c->getNewCriterion( InventoryItemPeer::SKU, 'inventory_item.SKU = inventory_item.ROOTITEM', Criteria::CUSTOM );
      $Matrix_Cond->addOr( $RootItem_Cond );
      $c->add( $Matrix_Cond );
    }

    if ( $ReturnCount ) {
      return InventoryItemPeer::doCount( $c );
    }
    if ( !$ReturnPager ) {
      return InventoryItemPeer::doSelect( $c );
    }
    if ( empty($rows_in_pager) ) $rows_in_pager= (int)sfConfig::get('app_application_rows_in_pager' );
    if ( $rows_in_pager >0 ) {
      $pager = new sfNPropelPager( 'InventoryItem', $rows_in_pager );
    }
    $pager->setPage( $page );
    $pager->setCriteria($c);
    $pager->init();
    return $pager;
  }


  public static function getInventoryItems( $page=1, $ReturnPager=true, $ReturnCount= false, $rows_in_pager='', $filter_matrix='', $filter_rootitem='',
  $filter_title='', $ExtendedSearch= false, $filter_sku='', $filter_category='', $filter_subcategory='', $filter_brand_id='', $filter_qty_on_hand_min='',  $filter_qty_on_hand_max='',  $filter_std_unit_price_min='' , $filter_std_unit_price_max='', $Sorting= '', $filter_sale_method='' ) {

    $ModuleName= sfContext::getInstance()->getModuleName();
    $ActionName= sfContext::getInstance()->getActionName();


    $c = new Criteria();

    $inventory_category_joined= false;
    if ( !empty($filter_category) ) {
     	$c->addJoin( InventoryItemPeer::INVENTORY_CATEGORY, InventoryCategoryPeer::PRODUCT_LINE, Criteria::LEFT_JOIN );
      $c->add( InventoryCategoryPeer::CATEGORY, $filter_category );
     	$inventory_category_joined= true;
    }

    if ( !empty($filter_subcategory) ) {
     	if ( !$inventory_category_joined ) {
     	  $c->addJoin( InventoryItemPeer::INVENTORY_CATEGORY, InventoryCategoryPeer::PRODUCT_LINE, Criteria::LEFT_JOIN );
     	  $inventory_category_joined= true;
     	}
      $c->add( InventoryCategoryPeer::SUBCATEGORY, $filter_subcategory );
     	$inventory_category_joined= true;
    }

    if ( strlen($filter_matrix)> 0 and (int)$filter_matrix == 1 ) {
      $Matrix_Cond= $c->getNewCriterion( InventoryItemPeer::MATRIX, false );
      $RootItem_Cond= $c->getNewCriterion( InventoryItemPeer::SKU, 'inventory_item.SKU = inventory_item.ROOTITEM', Criteria::CUSTOM );
      $Matrix_Cond->addOr( $RootItem_Cond );
      $c->add( $Matrix_Cond );
    }

    if ( strlen($filter_rootitem)> 0 ) {
      $c->add( InventoryItemPeer::ROOTITEM, $filter_rootitem );
    }

    if ( !empty($filter_title) and $filter_title!= '-' ) {
    	if ( $ExtendedSearch ) {

        $TITLE_Cond= $c->getNewCriterion( InventoryItemPeer::TITLE, '%'.$filter_title.'%', Criteria::LIKE );
        $DESCRIPTION_SHORT_Cond= $c->getNewCriterion( InventoryItemPeer::DESCRIPTION_SHORT, '%'.$filter_title.'%', Criteria::LIKE );
        $TITLE_Cond->addOr( $DESCRIPTION_SHORT_Cond );

        $DESCRIPTION_LONG_Cond= $c->getNewCriterion( InventoryItemPeer::DESCRIPTION_LONG, '%'.$filter_title.'%', Criteria::LIKE );
        $TITLE_Cond->addOr( $DESCRIPTION_LONG_Cond );

        $SIZE_Cond= $c->getNewCriterion( InventoryItemPeer::SIZE, '%'.$filter_title.'%', Criteria::LIKE );
        $TITLE_Cond->addOr( $SIZE_Cond );

        $COLOR_Cond= $c->getNewCriterion( InventoryItemPeer::COLOR, '%'.$filter_title.'%', Criteria::LIKE );
        $TITLE_Cond->addOr( $COLOR_Cond );

        $OPTIONS_Cond= $c->getNewCriterion( InventoryItemPeer::OPTIONS, '%'.$filter_title.'%', Criteria::LIKE );
        $TITLE_Cond->addOr( $OPTIONS_Cond );

        $GENDER_Cond= $c->getNewCriterion( InventoryItemPeer::GENDER, '%'.$filter_title.'%', Criteria::LIKE );
        $TITLE_Cond->addOr( $GENDER_Cond );

        $SKU_Cond= $c->getNewCriterion( InventoryItemPeer::SKU, $filter_title );
        $TITLE_Cond->addOr( $SKU_Cond );


         $BRAND_Cond= $c->getNewCriterion( BrandPeer::TITLE, '%'.$filter_title.'%', Criteria::LIKE );
         $c->addJoin( InventoryItemPeer::BRAND_ID, BrandPeer::ID, Criteria::LEFT_JOIN );
         $TITLE_Cond->addOr( $BRAND_Cond );

        $c->addOr( $TITLE_Cond ); //3) Yes, search should include description_short, description_long, brand, size, color, options, gender.

    	} else {
        $c->add( InventoryItemPeer::TITLE, '%'.$filter_title.'%', Criteria::LIKE );
    	}
    }

    if ( !empty($filter_sku) and $filter_sku!= '-' ) {
      $c->add( InventoryItemPeer::SKU, '%'.$filter_sku.'%', Criteria::LIKE );
    }
    if ( !empty($filter_brand_id) and $filter_brand_id!= '-' ) {
    	$c->addJoin( InventoryItemPeer::BRAND_ID, BrandPeer::ID, Criteria::LEFT_JOIN );
      $c->add( BrandPeer::ID, $filter_brand_id );
    }

    if (    (  !empty($filter_qty_on_hand_min) and $filter_qty_on_hand_min!= '-' ) and ( !empty($filter_qty_on_hand_max) and $filter_qty_on_hand_max!= '-' )  ) {
      $QtyOnHandMin_Cond= $c->getNewCriterion( InventoryItemPeer::QTY_ON_HAND, $filter_qty_on_hand_min, Criteria::GREATER_EQUAL );
      $QtyOnHandMax_Cond= $c->getNewCriterion( InventoryItemPeer::QTY_ON_HAND, $filter_qty_on_hand_max, Criteria::LESS_EQUAL );
      $QtyOnHandMin_Cond->addAnd( $QtyOnHandMax_Cond );
      $c->add( $QtyOnHandMin_Cond );
    } else {
      if ( !empty($filter_qty_on_hand_min) and $filter_qty_on_hand_min!= '-' ) {
        $c->add( InventoryItemPeer::QTY_ON_HAND, $filter_qty_on_hand_min, Criteria::GREATER_EQUAL  );
      }
      if ( !empty($filter_qty_on_hand_max) and $filter_qty_on_hand_max!= '-' ) {
        $c->add( InventoryItemPeer::QTY_ON_HAND, $filter_qty_on_hand_max, Criteria::LESS_EQUAL );
      }
    }

    if (    (  !empty($filter_std_unit_price_min) and $filter_std_unit_price_min!= '-' ) and ( !empty($filter_std_unit_price_max) and $filter_std_unit_price_max!= '-' )  ) {
      $StdUnitPriceMin_Cond= $c->getNewCriterion( InventoryItemPeer::STD_UNIT_PRICE, $filter_std_unit_price_min, Criteria::GREATER_EQUAL );
      $StdUnitPriceMax_Cond= $c->getNewCriterion( InventoryItemPeer::STD_UNIT_PRICE, $filter_std_unit_price_max, Criteria::LESS_EQUAL );
      $StdUnitPriceMin_Cond->addAnd( $StdUnitPriceMax_Cond );
      $c->add( $StdUnitPriceMin_Cond );
    } else {
      if ( !empty($filter_std_unit_price_min) and $filter_std_unit_price_min!= '-' ) {
        $c->add( InventoryItemPeer::STD_UNIT_PRICE, $filter_std_unit_price_min, Criteria::GREATER_EQUAL  );
      }
      if ( !empty($filter_std_unit_price_max) and $filter_std_unit_price_max!= '-' ) {
        $c->add( InventoryItemPeer::STD_UNIT_PRICE, $filter_std_unit_price_max, Criteria::LESS_EQUAL );
      }
    }
    if (  !empty($filter_sale_method)  ) {
      if ( is_array($filter_sale_method) ) {
        $c->add( InventoryItemPeer::SALE_METHOD, $filter_sale_method, Criteria::IN );
      } else {
        if ( $filter_sale_method!= '-' ) {
    	    $c->add( InventoryItemPeer::SALE_METHOD, $filter_sale_method );
        }
      }
    }


    if ( $Sorting=='CREATED_AT' ) {
      $c->addDescendingOrderByColumn(InventoryItemPeer::CREATED_AT);
    }
    if ( $Sorting=='SKU' ) {
      $c->addDescendingOrderByColumn(InventoryItemPeer::SKU);
    }
    if ( $Sorting=='TITLE' ) {
      $c->addAscendingOrderByColumn(InventoryItemPeer::TITLE);
    }
    if ( $Sorting=='BRAND' ) {
    	$c->addJoin( InventoryItemPeer::BRAND_ID, BrandPeer::ID, Criteria::LEFT_JOIN );
      $c->addAscendingOrderByColumn( BrandPeer::TITLE );
    }
    if ( $Sorting=='COLOR' ) {
      $c->addAscendingOrderByColumn( InventoryItemPeer::COLOR );
    }
    if ( $Sorting=='SIZE' ) {
      $c->addAscendingOrderByColumn( InventoryItemPeer::SIZE  );
    }


    if ( $Sorting=='QTY_ON_HAND' ) {
      $c->addAscendingOrderByColumn(InventoryItemPeer::QTY_ON_HAND);
    }
    if ( $Sorting == 'STD_UNIT_PRICE_LOW_TO_HIGN' ) {
      if ( $ModuleName == 'main' and $ActionName == 'specials' ) {
        $c->addAscendingOrderByColumn(InventoryItemPeer::SALE_PRICE );
        $c->addAscendingOrderByColumn(InventoryItemPeer::CLEARANCE );

      } else {
        $c->addAscendingOrderByColumn(InventoryItemPeer::STD_UNIT_PRICE);
      }
    }

    if ( $Sorting == 'STD_UNIT_PRICE_HIGN_TO_LOW' ) {
      $c->addDescendingOrderByColumn(InventoryItemPeer::STD_UNIT_PRICE);
    }


    if ( $Sorting=='CLEARANCE' ) {
      $c->addDescendingOrderByColumn(InventoryItemPeer::CLEARANCE);
    }
    $c->setDistinct();


    if ( $ReturnCount ) {
      return InventoryItemPeer::doCount( $c );
    }
    if ( !$ReturnPager ) {
      return InventoryItemPeer::doSelect( $c );
    }
    if ( empty($rows_in_pager) ) $rows_in_pager= (int)sfConfig::get('app_application_rows_in_pager' );
    $pager = new sfNPropelPager( 'InventoryItem', $rows_in_pager );
    $pager->setPage( $page );
    $pager->setCriteria($c);
    $pager->init();
    return $pager;
  }

  public static function getSimilarInventoryItem($InventoryItemSku, $IsEqual=true, $con = null)
  {
    $c = new Criteria();
    if ( $IsEqual ) {
      $c->add( InventoryItemPeer::SKU, $InventoryItemSku );
    } else {
      $c->add( InventoryItemPeer::SKU, $InventoryItemSku, Criteria::NOT_EQUAL );
    }
    if($lResult = InventoryItemPeer::doSelect( $c, $con ) )
    {
      return $lResult[0];
    }
    return '';
  }

  public static function getSelectionList( $Code='', $Name='', $NewCode='', $NewCodeText='', $HideOpenedInventoryItems= false, $filter_status= '' ) {
    $List= InventoryItemPeer::getInventoryItems( 1, false, false, '', '', '', '', '', '', '', '', '','' , '', $filter_status, '', '', '', $HideOpenedInventoryItems );
    $ResArray= array();
    if ( !empty($Code) or !empty($Name) ) {
      $ResArray[$Code]= $Name;
    }
    foreach( $List as $lInventoryItem ) {
      $ResArray[ $lInventoryItem->getId() ]= $lInventoryItem;
    }
    if ( !empty($NewCode) and !empty($NewCodeText) ) {
      $ResArray[ $NewCode ]= $NewCodeText;
    }
    return $ResArray;
  }

  public static function getColorsListByRootItem($RootItem)
  {
    $c = new Criteria();
    $c->add( InventoryItemPeer::ROOTITEM, $RootItem );
    $Color_Cond_1= $c->getNewCriterion( InventoryItemPeer::COLOR, null, Criteria::NOT_EQUAL );
    $Color_Cond_2= $c->getNewCriterion( InventoryItemPeer::COLOR, '', Criteria::NOT_EQUAL );
    $Color_Cond_1->addAnd( $Color_Cond_2 );
    $c->add( $Color_Cond_1 );

    $c->addAscendingOrderByColumn( InventoryItemPeer::COLOR );
    $c->setDistinct();
    $c->addSelectColumn('sku');
    $c->addSelectColumn('color');
    $c->addSelectColumn('std_unit_price');
    $ResArray= array();
    $stmt= InventoryItemPeer::doSelectStmt( $c );
    $result = $stmt->fetch();
    while($result) {
      $ResArray[ $result['sku'] ]= array( 'color'=> $result['color'], 'std_unit_price' => $result['std_unit_price'] );
      $result = $stmt->fetch();
    }
    return $ResArray;
  }

  public static function getSizesListByRootItem($RootItem)
  {
    $c = new Criteria();
    $c->add( InventoryItemPeer::ROOTITEM, $RootItem );
    $Size_Cond_1= $c->getNewCriterion( InventoryItemPeer::SIZE, null, Criteria::NOT_EQUAL );
    $Size_Cond_2= $c->getNewCriterion( InventoryItemPeer::SIZE, '', Criteria::NOT_EQUAL );
    $Size_Cond_1->addAnd( $Size_Cond_2 );
    $c->add( $Size_Cond_1 );

    $c->setDistinct();
    $c->addSelectColumn('sku');
    $c->addSelectColumn('size');
    $c->addSelectColumn('std_unit_price');
    $ResArray= array();
    $stmt= InventoryItemPeer::doSelectStmt( $c );
    $result = $stmt->fetch();
    while($result) {
      $ResArray[ $result['sku'] ]= array( 'size'=>$result['size'], 'std_unit_price' => $result['std_unit_price'] );
      $result = $stmt->fetch();
    }
    return $ResArray;
  }

  public static function getOptionsListByRootItem($RootItem)
  {
    $c = new Criteria();
    $c->add( InventoryItemPeer::ROOTITEM, $RootItem );
    $Options_Cond_1= $c->getNewCriterion( InventoryItemPeer::OPTIONS, null, Criteria::NOT_EQUAL );
    $Options_Cond_2= $c->getNewCriterion( InventoryItemPeer::OPTIONS, '', Criteria::NOT_EQUAL );
    $Options_Cond_1->addAnd( $Options_Cond_2 );
    $c->add( $Options_Cond_1 );

    $c->addAscendingOrderByColumn( InventoryItemPeer::OPTIONS );
    $c->setDistinct();
    $c->addSelectColumn('sku');
    $c->addSelectColumn('options');
    $c->addSelectColumn('std_unit_price');
    $ResArray= array();
    $stmt= InventoryItemPeer::doSelectStmt( $c );
    $result = $stmt->fetch();
    while($result) {
      $ResArray[ $result['sku'] ]= array( 'option'=>$result['options'], 'std_unit_price' => $result['std_unit_price'] );
      $result = $stmt->fetch();
    }
    return $ResArray;
  }


  public static function getHandsListByRootItem($RootItem)
  {
    $c = new Criteria();
    $c->add( InventoryItemPeer::ROOTITEM, $RootItem );
    $Hand_Cond_1= $c->getNewCriterion( InventoryItemPeer::HAND, null, Criteria::NOT_EQUAL );
    $Hand_Cond_2= $c->getNewCriterion( InventoryItemPeer::HAND, '', Criteria::NOT_EQUAL );
    $Hand_Cond_1->addAnd( $Hand_Cond_2 );
    $c->add( $Hand_Cond_1 );

    $c->addAscendingOrderByColumn( InventoryItemPeer::HAND );
    $c->setDistinct();
    $c->addSelectColumn('sku');
    $c->addSelectColumn('hand');
    $c->addSelectColumn('std_unit_price');
    $ResArray= array();
    $stmt= InventoryItemPeer::doSelectStmt( $c );
    $result = $stmt->fetch();
    while($result) {
      $ResArray[ $result['sku'] ]= array( 'hand'=>$result['hand'], 'std_unit_price' => $result['std_unit_price'] );
      $result = $stmt->fetch();
    }
    return $ResArray;
  }


  public static function getFinishesListByRootItem($RootItem)
  {
    $c = new Criteria();
    $c->add( InventoryItemPeer::ROOTITEM, $RootItem );
    $Finish_Cond_1= $c->getNewCriterion( InventoryItemPeer::FINISH, null, Criteria::NOT_EQUAL );
    $Finish_Cond_2= $c->getNewCriterion( InventoryItemPeer::FINISH, '', Criteria::NOT_EQUAL );
    $Finish_Cond_1->addAnd( $Finish_Cond_2 );
    $c->add( $Finish_Cond_1 );

    $c->addAscendingOrderByColumn( InventoryItemPeer::FINISH );
    $c->setDistinct();
    $c->addSelectColumn('sku');
    $c->addSelectColumn('finish');
    $c->addSelectColumn('std_unit_price');
    $ResArray= array();
    $stmt= InventoryItemPeer::doSelectStmt( $c );
    $result = $stmt->fetch();
    while($result) {
      $ResArray[ $result['sku'] ]= array( 'finish'=>$result['finish'], 'std_unit_price' => $result['std_unit_price'] );
      $result = $stmt->fetch();
    }
    return $ResArray;
  }

  public static function getGendersListByRootItem($RootItem)
  {
    $c = new Criteria();
    $c->add( InventoryItemPeer::ROOTITEM, $RootItem );
    $Gender_Cond_1= $c->getNewCriterion( InventoryItemPeer::GENDER, null, Criteria::NOT_EQUAL );
    $Gender_Cond_2= $c->getNewCriterion( InventoryItemPeer::GENDER, '', Criteria::NOT_EQUAL );
    $Gender_Cond_1->addAnd( $Gender_Cond_2 );
    $c->add( $Gender_Cond_1 );

    $c->addAscendingOrderByColumn( InventoryItemPeer::GENDER );
    $c->setDistinct();
    $c->addSelectColumn('sku');
    $c->addSelectColumn('gender');
    $c->addSelectColumn('std_unit_price');
    $ResArray= array();
    $stmt= InventoryItemPeer::doSelectStmt( $c );
    $result = $stmt->fetch();
    while($result) {
      $ResArray[ $result['sku'] ]= array( 'gender'=>$result['gender'], 'std_unit_price' => $result['std_unit_price'] );
      $result = $stmt->fetch();
    }
    return $ResArray;
  }

  public static function getDescriptionShortsListByRootItem($RootItem)
  {
    $c = new Criteria();
    $c->add( InventoryItemPeer::ROOTITEM, $RootItem );
    $DescriptionShort_Cond_1= $c->getNewCriterion( InventoryItemPeer::DESCRIPTION_SHORT, null, Criteria::NOT_EQUAL );
    $DescriptionShort_Cond_2= $c->getNewCriterion( InventoryItemPeer::DESCRIPTION_SHORT, '', Criteria::NOT_EQUAL );
    $DescriptionShort_Cond_1->addAnd( $DescriptionShort_Cond_2 );
    $c->add( $DescriptionShort_Cond_1 );

    $c->addAscendingOrderByColumn( InventoryItemPeer::DESCRIPTION_SHORT );
    $c->setDistinct();
    $c->addSelectColumn('sku');
    $c->addSelectColumn('description_short');
    $ResArray= array();
    $stmt= InventoryItemPeer::doSelectStmt( $c );
    $result = $stmt->fetch();
    while($result) {
      $ResArray[ $result['sku'] ]= $result['description_short'];
      $result = $stmt->fetch();
    }
    return $ResArray;
  }


  public static function getPaperworksListByRootItem($RootItem)
  {
    $c = new Criteria();
    $c->add( InventoryItemPeer::ROOTITEM, $RootItem );
    $Paperwork_Cond_1= $c->getNewCriterion( InventoryItemPeer::PAPERWORK, null, Criteria::NOT_EQUAL );
    $Paperwork_Cond_2= $c->getNewCriterion( InventoryItemPeer::PAPERWORK, '', Criteria::NOT_EQUAL );
    $Paperwork_Cond_1->addAnd( $Paperwork_Cond_2 );
    $c->add( $Paperwork_Cond_1 );

    $c->addAscendingOrderByColumn( InventoryItemPeer::PAPERWORK );
    $c->setDistinct();
    $c->addSelectColumn('sku');
    $c->addSelectColumn('paperwork');
    $ResArray= array();
    $stmt= InventoryItemPeer::doSelectStmt( $c );
    $result = $stmt->fetch();
    while($result) {
      $ResArray[ $result['sku'] ]= $result['paperwork'];
      $result = $stmt->fetch();
    }
    return $ResArray;
  }

  public static function getUnitMeasuresListByRootItem($RootItem)
  {
    $c = new Criteria();
    $c->add( InventoryItemPeer::ROOTITEM, $RootItem );
    $UnitMeasure_Cond_1= $c->getNewCriterion( InventoryItemPeer::UNIT_MEASURE, null, Criteria::NOT_EQUAL );
    $UnitMeasure_Cond_2= $c->getNewCriterion( InventoryItemPeer::UNIT_MEASURE, '', Criteria::NOT_EQUAL );
    $UnitMeasure_Cond_1->addAnd( $UnitMeasure_Cond_2 );
    $c->add( $UnitMeasure_Cond_1 );

    $c->addAscendingOrderByColumn( InventoryItemPeer::UNIT_MEASURE );
    $c->setDistinct();
    $c->addSelectColumn('sku');
    $c->addSelectColumn('unit_measure');
    $ResArray= array();
    $stmt= InventoryItemPeer::doSelectStmt( $c );
    $result = $stmt->fetch();
    while($result) {
      $ResArray[ $result['sku'] ]= $result['unit_measure'];
      $result = $stmt->fetch();
    }
    return $ResArray;
  }

  public static function getHandListByRootItem($RootItem)
  {
    $c = new Criteria();
    $c->add(InventoryItemPeer::ROOTITEM, $RootItem);
    $hand_cond_1 = $c->getNewCriterion(InventoryItemPeer::HAND, null, Criteria::NOT_EQUAL);
    $hand_cond_2 = $c->getNewCriterion(InventoryItemPeer::HAND, '', Criteria::NOT_EQUAL);
    $hand_cond_1->addAnd($hand_cond_2);
    $c->add($hand_cond_1);

    $c->addAscendingOrderByColumn(InventoryItemPeer::HAND);
    $c->setDistinct();
    $c->addSelectColumn('sku');
    $c->addSelectColumn('hand');
    $ResArray= array();
    $stmt= InventoryItemPeer::doSelectStmt($c);
    $result = $stmt->fetch();
    while ($result) {
        $ResArray[ $result['sku'] ]= $result['hand'];
        $result = $stmt->fetch();
    }
    return $ResArray;
  }


  public static function getHasDifferentPrices($RootItem)
  {
    $c = new Criteria();
    $c->add(InventoryItemPeer::ROOTITEM, $RootItem);
    $hand_cond_1 = $c->getNewCriterion(InventoryItemPeer::STD_UNIT_PRICE, null, Criteria::NOT_EQUAL);
    $hand_cond_2 = $c->getNewCriterion(InventoryItemPeer::STD_UNIT_PRICE, '', Criteria::NOT_EQUAL);
    $hand_cond_1->addAnd($hand_cond_2);
    $c->add($hand_cond_1);

    $c->addAscendingOrderByColumn(InventoryItemPeer::STD_UNIT_PRICE);
    $c->setDistinct();
    $c->addSelectColumn('std_unit_price');
    $ResArray= array();
    $stmt= InventoryItemPeer::doSelectStmt($c);
    $result = $stmt->fetch();
    $IsFirstRow= true;
    while ($result) {
      if( $IsFirstRow ) {
        $std_unit_price= $result['std_unit_price'];
        $IsFirstRow= false;
      } else {
        if ( $std_unit_price != $result['std_unit_price'] ) return true;
        $std_unit_price = $result['std_unit_price'];
      }
      $result = $stmt->fetch();
    }
    return false;
  }


} // InventoryItemPeer




