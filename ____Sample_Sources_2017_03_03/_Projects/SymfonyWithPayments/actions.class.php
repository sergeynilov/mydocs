<?php


/**
 * check_out actions.
 *
 * @package    sf_sandbox
 * @subpackage check_out
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class check_outActions extends sfActions {

  /**

   * Executes index action
   *
   * @param sfRequest $request A request object
   */
  public function executeCustomer(sfWebRequest $request) {
    $response = $this->context->getResponse();
    $response->addJavascript('/js/jquery/jquery.js', 'last');
    $response->addJavascript('/js/jquery/jquery-ui.js', 'last');
    try {
      $this->form = new NewCustomerForm();
      $response->setTitle("ROC Checkout");

      if ($request->isMethod('post')) {
        $new_customer_data = $request->getParameter('new_customer');
        $this->form->bind($new_customer_data);
        if ($this->form->isValid()) {
          if ($new_customer_data['account_type'] == 'New User' and !empty($new_customer_data['email'])) {
            Cart::setGuestUser($new_customer_data['email']);
            $Hours= time()+60*60*12*1;// will set the cookie to expire in 1 ( 12 hours ) day
            $response->setCookie( 'user_has_logged', '1', $Hours, '/' );
            $this->redirect('@check_out_shipping');
          }

          if ($new_customer_data['account_type'] == 'Existing User' and !empty($new_customer_data['returning_email'])) {
            $lsfGuardUser = sfGuardUserPeer::getByUsername($new_customer_data['returning_email']);
            Cart::setGuestUser($new_customer_data['returning_email']);
            $Hours= time()+60*60*12*1;
            $response->setCookie( 'user_has_logged', '1', $Hours, '/' );
            $this->redirect('@check_out_shipping?logged_user_id=' . (!empty($lsfGuardUser) ? $lsfGuardUser->getId() : '' ));
          }
        }
      }
    } catch (Exception $lException) {
      $this->logMessage($lException->getMessage(), 'err');
      return sfView::ERROR;
    }
  }

  public function executeShipping(sfWebRequest $request) {
    $response = $this->context->getResponse();
    $response->addJavascript('/js/jquery/jquery.js', 'last');
    $response->addJavascript('/js/jquery/jquery-ui.js', 'last');
    $con = Propel::getConnection(sfGuardGroupPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
    $this->MakeAutoSubmit= false;
    try {
      $logged_user_id = $request->getParameter('logged_user_id')
      ;
      $is_change = $request->getParameter('is_change')              ;
      $lUserShippingInfo = UserShippingInfoPeer::getUserShippingInfoByUserId( $logged_user_id );
      if (!empty($lUserShippingInfo)) {
        ShippingForm::$lUserShippingInfo = $lUserShippingInfo;
        if ($is_change!= 1) {
          $this->MakeAutoSubmit= true;
        }

      }

      $this->form = new ShippingForm();
      $response->setTitle("ROC Checkout");

      if ($request->isMethod('post')) {
        $shipping_data = $request->getParameter('shipping');
        $this->form->bind($shipping_data);
        if ($this->form->isValid()) {
          Cart::setShippingInfo($shipping_data);

          if (!empty($shipping_data['password']) and !empty($shipping_data['password_2']) and !empty($shipping_data['password']) == !empty($shipping_data['password_2'])) {

            $lNewSfUser = new sfGuardUser();
            $lNewSfUser->setName($shipping_data['name']);

            $GuestUserArray = Cart::getGuestUser();
            $lNewSfUser->setUsername($GuestUserArray['LoggedUserEmail']);
            $lNewSfUser->setIsActive(true);
            $lNewSfUser->setSalt('');
            $lNewSfUser->setPassword($shipping_data['password']);
            $lNewSfUser->setAlgorithm('md5');
            $lNewSfUser->setIsSuperAdmin(false);
            $con->beginTransaction();
            $lNewSfUser->save();

            $lsfGuardUserGroup = new sfGuardUserGroup();
            $lsfGuardUserGroup->setUserId($lNewSfUser->getId());
            $lsfGuardUserGroup->setGroupId(sfGuardGroupPeer::getGroupIdByName('public'));
            $lsfGuardUserGroup->save();
            $con->commit();
            $lBufUser = $lNewSfUser;
            $this->getUser()->addCredential('public');
            $this->getUser()->signIn($lBufUser);
          }
          $this->redirect('@check_out_review_order');
        }
      }
    } catch (Exception $lException) {
      $con->rollBack();
      $this->logMessage($lException->getMessage(), 'err');
      return sfView::ERROR;
    }
  }

  public function executeReview_order(sfWebRequest $request) {
    $response = $this->context->getResponse();
    $response->addJavascript('/js/jquery/jquery.js', 'last');
    $response->addJavascript('/js/jquery/jquery-ui.js', 'last');
    try {
      $this->form = new ReviewOrderForm();
      $response->setTitle("ROC Checkout");

      $mode = $request->getParameter('mode');
      $sku = $request->getParameter('sku');
      $product_quantity = $request->getParameter('product_quantity');
      if ($mode == 'update') {
        $Res = Cart::UpdateUsersCart($sku, $product_quantity, '');
      }

      if ($mode == 'delete') {
        $Res = Cart::UsersCartDeleteItem($sku);
      }

      if ($request->isMethod('post')) {
        $review_order = $request->getParameter('review_order');
        $this->form->bind($review_order);
        if ($this->form->isValid()) {
          Cart::setReviewOrder( $review_order, true );

          $shipping_tax_data = Cart::getTaxShipData();
          if ( empty($shipping_tax_data) ) {
            $this->CheckoutArray = Cart::getShippingInfo();
            $shipping_array = ShippingTaxes::getShippingRates($this->CheckoutArray['b_zip'], false, false);
            $tax_percent= Cart::getTaxPercentByState( $this->CheckoutArray['state'] );
            $common_sum = Cart::getCommonSum(false);
            $tax_sum = round( ( $common_sum / 100 * $tax_percent ), 2 ) ; // $shipping_tax_dat['est_tax'];
            $SelectedShippingValue= '';
            if ( !empty(  $shipping_array[ $review_order['shipping_service'] ]  ) ) {
              $SelectedShippingValue= $shipping_array[ $review_order['shipping_service'] ];
            }

            $out_str = $this->CheckoutArray['zip'] . ',' . ',' . $shipping_array['UPS-Ground'] . ',' . $shipping_array['UPS-3Day'] . ',' .
            $shipping_array['UPS-2Day'] . ',' . $shipping_array['UPS-NextDay'] . ',' . $tax_sum . ',' . $SelectedShippingValue . ',' . ( $common_sum + $SelectedShippingValue + $tax_sum );

            Cart::setTaxShipData($out_str, 'all');
            $shipping_tax_data = Cart::getTaxShipData();

          }
          $time= time();
          $NewOrdergetId = $this->MakeDataSaving("", $time, true );
          $lNewOrdergetId= OrderedPeer::retrieveByPK($NewOrdergetId);
          sfContext::getInstance()->getUser()->setAttribute( "last_order_id", $NewOrdergetId );
          $this->redirect('@check_out_payment');
        }
      }
    } catch (Exception $lException) {
      $this->logMessage($lException->getMessage(), 'err');
      return sfView::ERROR;
    }
  }

  public function executePayment(sfWebRequest $request) {
    $response = $this->context->getResponse();
    $response->addJavascript('/js/jquery/jquery.js', 'last');
    $response->addJavascript('/js/jquery/jquery-ui.js', 'last');
    try {
      $this->form = new PaymentMethodCreditCardForm();
      $response->setTitle("ROC Checkout");
      $payment_method = $request->getParameter('payment_method');
      $last_order_id= sfContext::getInstance()->getUser()->getAttribute( "last_order_id" );
      if ($payment_method == 'PP') {
        $this->redirect('@get_PayPal_HTML');
      }


      if ($payment_method == 'GC') {
      }
      if ($payment_method == 'DP') {
        $time= time();
        $last_order_id= sfContext::getInstance()->getUser()->getAttribute( "last_order_id" );
        $this->UpdateOrderStatus($last_order_id, "DP", "WO_" . $last_order_id );
        $this->redirect('@check_out_make_payment_report?transaction_id=' . "WO_" . $last_order_id . '&new_order_id=' . $last_order_id );
      }

      if ($request->isMethod('post')) {
        $payment = $request->getParameter('payment_type');
        $this->form->bind($payment);
        if ($this->form->isValid()) {
          Cart::setPaymentData($payment);
          $this->redirect('@check_out_make_payment');
        }
      }
    } catch (Exception $lException) {
      $this->logMessage($lException->getMessage(), 'err');
      return sfView::ERROR;
    }
  }

  private function UpdateOrderStatus($last_order_id, $payment_method, $transaction_id ) {
    $lOrder= OrderedPeer::retrieveByPK( $last_order_id );
    if ( !empty($lOrder) ) {
      $lOrder->setPaymentMethod($payment_method);
      $lOrder->setStatus("N");
      $lOrder->setTransactionId($transaction_id);
      if ($payment_method == 'DP') {
        $lOrder->setTax(0);
      }
      $lOrder->save();

      $GuestUserArray = Cart::getGuestUser();
      $LoggedUserEmail = $GuestUserArray['LoggedUserEmail'];
      $lLoggedUser = sfGuardUserPeer::retrieveByUsername($LoggedUserEmail);
      if (empty($lLoggedUser) and $payment_method != 'CC') {
        $this->redirect('@check_out_customer');
      }

      $Res = Cart::SendOrderEmailToAdmin( $lOrder, $lLoggedUser, /*$lNewOrder->getId(),*/ true);
      Cart::Clear();
      return true;
    }
    return false;
  }

  public function executeMake_payment(sfWebRequest $request) {
    $response = $this->context->getResponse();
    $response->addJavascript('/js/jquery/jquery.js', 'last');
    $response->addJavascript('/js/jquery/jquery-ui.js', 'last');
    try {
      require_once 'lib/anet_php_sdk/AuthorizeNet.php'; // The SDK
      $url = Util::getServerHost(sfContext::getInstance()->getConfiguration()) . "check_out/make_payment";     // http://YOUR_DOMAIN.com/direct_post.php";
      $this->response_code = $request->getParameter('response_code');
      $this->transaction_id = $request->getParameter('transaction_id');
      if ($this->response_code == '1' and !empty($this->transaction_id)) {
        $last_order_id= sfContext::getInstance()->getUser()->getAttribute( "last_order_id" );
        $this->UpdateOrderStatus($last_order_id, "CC",  "WO_" . $this->transaction_id );
        $this->redirect('@check_out_make_payment_report?transaction_id=' . $this->transaction_id . '&new_order_id=' . $NewOrdergetId);
        return;
      } // if ( $this->response_code== '1' and !empty($this->transaction_id) ) {

      $api_login_id = sfConfig::get('app_authorize_net_api_login_id');           // 'YOUR_API_LOGIN_ID';
      $transaction_key = sfConfig::get('app_authorize_net_transaction_key');     // 'YOUR_TRANSACTION_KEY';
      $md5_setting = sfConfig::get('app_authorize_net_md5_setting');         // 'YOUR_API_LN_OGIID'; // Your MD5 Setting
      $shipping_tax_data = Cart::getTaxShipData();  //  [postal_code] => 55555 [usps] => 0 [ups_ground] => 9.36 [ups_3_day_selected] => 14.03 [ups_2_day_air] => 18.70 [ups_next_day_air_saver] => 48.79
      $CommonSum = Cart::getCommonSum();

      $amount = $CommonSum;
      $this->PostDemoFormHTML = AuthorizeNetDPM::directPostDemo($url, $api_login_id, $transaction_key, $amount, $md5_setting);
    } catch (Exception $lException) {
      $this->logMessage($lException->getMessage(), 'err');
      return sfView::ERROR;
    }
  }

  private function MakeDataSaving($PaymantType, $transaction_id, $ResaveTransaction= false ) {
    try {
      $con = Propel::getConnection(OrderedPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
      $this->CheckoutArray = Cart::getShippingInfo();
      $lNewOrder = new Ordered();

      $GuestUserArray = Cart::getGuestUser();
      $LoggedUserEmail = $GuestUserArray['LoggedUserEmail'];
      $lLoggedUser = sfGuardUserPeer::retrieveByUsername($LoggedUserEmail);
      if (!empty($lLoggedUser)) {
        $lNewOrder->setUserId($lLoggedUser->getId());
      } else {
        $this->redirect('@check_out_customer');
      }
      $lNewOrder->setStatus('D'); // D-Draft, Order was submitted on Review Order page, but not submitted for payment with payment method

      $lNewOrder->setBCountry($this->CheckoutArray['b_country']);
      $lNewOrder->setBPhone($this->CheckoutArray['b_phone']);
      $lNewOrder->setBStreet($this->CheckoutArray['b_address']);
      $lNewOrder->setBStreet2($this->CheckoutArray['b_address_2']);
      $lNewOrder->setBCity($this->CheckoutArray['b_city']);
      $lNewOrder->setBState($this->CheckoutArray['b_state']);
      $lNewOrder->setBZip($this->CheckoutArray['b_zip']);

      $lNewOrder->setSCountry($this->CheckoutArray['country']);
      $lNewOrder->setSStreet($this->CheckoutArray['address']);
      $lNewOrder->setSStreet2($this->CheckoutArray['address_2']);
      $lNewOrder->setSCity($this->CheckoutArray['city']);
      $lNewOrder->setSState($this->CheckoutArray['state']);
      $lNewOrder->setSZip($this->CheckoutArray['zip']);

      $this->ReviewOrderArray = Cart::getReviewOrder();
      $lNewOrder->setNotes($this->ReviewOrderArray['notes']);
      $lNewOrder->setShipping($this->ReviewOrderArray['shipping_service']);

      $CommonSum = Cart::getCommonSum();
      $lNewOrder->setTotalPrice($CommonSum);

      $shipping_array = ShippingTaxes::getShippingRates($this->CheckoutArray['b_zip'], false, false);

      if ( $PaymantType == 'DP' ) {
        $tax_percent= 0;
        $tax_sum = 0;
        $lNewOrder->setTax( 0 );
        $TaxShipDataArray= Cart::getTaxShipData();

        $TaxShipDataArray['estimated_total']= $TaxShipDataArray['estimated_total'] - $TaxShipDataArray['est_tax'];

        $TaxShipDataArray['est_tax']= 0;
      } else {
        $tax_percent= Cart::getTaxPercentByState( $this->CheckoutArray['state'] );
        $tax_sum = round( ( $CommonSum / 100 * $tax_percent ), 2 ) ; // $shipping_tax_dat['est_tax'];
        $lNewOrder->setTax($tax_sum /* $this->shipping_tax_data_array['est_tax'] */);
      }

      if (!empty($shipping_array[$this->ReviewOrderArray['shipping_service']])) {
        $AdditionalFreightSum= Cart::getAdditionalFreightSum();
        $lNewOrder->setShippingCost($shipping_array[$this->ReviewOrderArray['shipping_service']] + $AdditionalFreightSum /* $this->shipping_tax_data_array['est_shipping'] */);
      }

      $lNewOrder->setDiscount(0); // TODO

      $this->PaymentDataArray = Cart::getPaymentData();
      if (empty($this->PaymentDataArray['payment_method'])) {
        $this->PaymentDataArray['payment_method'] = $PaymantType;
        Cart::setPaymentData(array('payment_method' => $PaymantType, 'card_number' => '', 'expiration_date_year' => '', 'expiration_date_month' => '', 'card_security_code' => ''));
      }

      $this->PaymentDataArray = Cart::getPaymentData();

      $con->beginTransaction();
      $lNewOrder->save();
      if ( $ResaveTransaction ) {
        $lNewOrder->save();
      }
      $UsersCartArray = Cart::getUsersCartArray();
      $UsersCartItemsSelectedCount = count($UsersCartArray);

      for ($Row = 0; $Row < $UsersCartItemsSelectedCount; $Row++) {
        if (!empty($UsersCartArray[$Row])) {
          $sku = $UsersCartArray[$Row]['sku'];
          $product_quantity = $UsersCartArray[$Row]['product_quantity'];
          $price_type = $UsersCartArray[$Row]['price_type'];
          $selected_price = (float)$UsersCartArray[$Row]['selected_price'];
          $lInventoryItem = InventoryItemPeer::getSimilarInventoryItem($sku);

          if (!empty($lInventoryItem)) {
            $lNewOrderedItem = new OrderedItem();
            $lNewOrderedItem->setOrderId($lNewOrder->getId());
            $lNewOrderedItem->setSku($sku);
            $lNewOrderedItem->setQty($product_quantity);
            $lNewOrderedItem->setUnitPrice( $lInventoryItem->getStdUnitPrice() );
                                  
            $lNewOrderedItem->setTitle( $lInventoryItem->getTitle() );
            $lNewOrderedItem->setSize( $lInventoryItem->getSize() );
            $lNewOrderedItem->setColor( $lInventoryItem->getColor() );
            $lNewOrderedItem->setOptions( $lInventoryItem->getOptions() );
            $lNewOrderedItem->setFinish( $lInventoryItem->getFinish() );
            $lNewOrderedItem->setHand( $lInventoryItem->getHand() );
            $lNewOrderedItem->setGender( $lInventoryItem->getGender() );
            $lNewOrderedItem->setUnitMeasure( $lInventoryItem->getUnitMeasure() );
            $lNewOrderedItem->setShipWeight( $lInventoryItem->getShipWeight() );
            
            if (!empty($price_type) and $price_type == 'special') {
              $lNewOrderedItem->setUnitPrice( $lInventoryItem->getClearance_Or_SalePrice() );
            }
            if (!empty($price_type) and $price_type == 'selected_price') {
              $lNewOrderedItem->setUnitPrice( $selected_price );
            }
            $lNewOrderedItem->save();
          }
        }
      }
      $con->commit();

      $this->CopiedPaymentData = Cart::getPaymentData();

      $this->getUser()->setAttribute('CopiedCheckoutCopiedArray', $this->CheckoutArray);
      $this->getUser()->setAttribute('CopiedhasLoggedUser', Cart::hasLoggedUser());
      $this->getUser()->setAttribute('CopiedLoggedUserEmail', Cart::getLoggedUserEmail());
      $this->getUser()->setAttribute('CopiedShippingAddress', Cart::hasShippingAddress());
      $this->getUser()->setAttribute('CopiedhasBillingAddress', Cart::hasBillingAddress());
      $this->getUser()->setAttribute('CopiedItemsCount', Cart::getItemsCount());
      $this->getUser()->setAttribute('Copied_shipping_tax_data', $TaxShipDataArray /*Cart::getTaxShipData()*/);
      $this->getUser()->setAttribute('CopiedCommonSum', Cart::getCommonSum());
      $this->getUser()->setAttribute('CopiedPaymentData', Cart::getPaymentData());

      return $lNewOrder->getId();
    } catch (Exception $lException) {
      $con->rollBack();
      return false;
    }
  }

  public function executeGet_AuthorizeNet_DPM_HTML(sfWebRequest $request) {
    $response = $this->context->getResponse();
    try {
      $url = Util::getServerHost(sfContext::getInstance()->getConfiguration()) . "check_out/make_payment";     // http://YOUR_DOMAIN.com/direct_post.php";
      $api_login_id = sfConfig::get('app_authorize_net_api_login_id');           // 'YOUR_API_LOGIN_ID';
      $transaction_key = sfConfig::get('app_authorize_net_transaction_key');     // 'YOUR_TRANSACTION_KEY';
      $md5_setting = sfConfig::get('app_authorize_net_md5_setting');         // 'YOUR_API_LOGIN_ID'; // Your MD5 Setting
      $shipping_tax_data = Cart::getTaxShipData();  //  [postal_code] => 55555 [usps] => 0 [ups_ground] => 9.36 [ups_3_day_selected] => 14.03 [ups_2_day_air] => 18.70 [ups_next_day_air_saver] => 48.79
      $CommonSum = Cart::getCommonSum();
      $amount = $shipping_tax_data['estimated_total'];
      echo AuthorizeNetDPM::directPostDemo($url, $api_login_id, $transaction_key, $amount, $md5_setting);
      return sfView::NONE;
    } catch (Exception $lException) {
      $this->logMessage($lException->getMessage(), 'err');
      return sfView::ERROR;
    }
  }

  public function executeGet_PayPal_HTML(sfWebRequest $request) {
    $response = $this->context->getResponse();
    $response->addJavascript('/js/jquery/jquery.js', 'last');
    $response->addJavascript('/js/jquery/jquery-ui.js', 'last');

    try {
      $GuestUserArray = Cart::getGuestUser();
      $LoggedUserEmail = $GuestUserArray['LoggedUserEmail'];

      $DeduggingPaypal= false;
      if ( $LoggedUserEmail == 'nilovserge@rbcmail.ru' or $LoggedUserEmail == 'nilov@softreactor.com' or $LoggedUserEmail== 'nilovserge@rambler.ru' or $LoggedUserEmail=='mstdmstd@gmail.com' ) {
        $DeduggingPaypal= true;
      }
      $is_debug = false;// $DeduggingPaypal;  //false;
      $url = Util::getServerHost(sfContext::getInstance()->getConfiguration()) . "check_out/make_payment";     // http://YOUR_DOMAIN.com/direct_post.php";
      $api_login_id = sfConfig::get('app_authorize_net_api_login_id');           // 'YOUR_API_LOGIN_ID';
      $transaction_key = sfConfig::get('app_authorize_net_transaction_key');     // 'YOUR_TRANSACTION_KEY';
      $md5_setting = sfConfig::get('app_authorize_net_md5_setting');         // 'YOUR_API_LOGIN_ID'; // Your MD5 Setting
      $shipping_tax_data = Cart::getTaxShipData();  //  [postal_code] => 55555 [usps] => 0 [ups_ground] => 9.36 [ups_3_day_selected] => 14.03 [ups_2_day_air] => 18.70 [ups_next_day_air_saver] => 48.79
      if ($is_debug)
      Util::deb($shipping_tax_data, '$shipping_tax_data::');
      $CommonSum = Cart::getCommonSum();
      $ItemsCount = Cart::getItemsCount();
      $UsersCartArray = Cart::getUsersCartArray();

      $amount = $CommonSum;
      $this->ReviewOrderArray = Cart::getReviewOrder();
      $shipping_array = ShippingTaxes::getShippingRates($this->CheckoutArray['b_zip'], false, false);

      $shipping_tax_data = Cart::getTaxShipData();  //  [postal_code] => 55555 [usps] => 0 [ups_ground] => 9.36 [ups_3_day_selected] => 14.03 [ups_2_day_air] => 18.70 [ups_next_day_air_saver] => 48.79
      $shipping_cost = $shipping_tax_data['est_shipping'];
      $CheckoutArray = Cart::getShippingInfo();
      $tax_percent= Cart::getTaxPercentByState( $CheckoutArray['state'] );

      if ($CheckoutArray['b_is_department_purchase'] == 1) {
        $tax_cost = 0;
      } else {
        $tax_cost = round( ( $CommonSum / 100 * $tax_percent ), 2 ) ; // $shipping_tax_dat['est_tax'];
      }

      if ($is_debug) {
        echo '-10:: $UsersCartArray:: <pre>' . print_r($UsersCartArray, true) . '</pre><br>';
        echo '-1:: $amount:: <pre>' . print_r($amount, true) . '</pre><br>';
        echo '-2:: $shipping_cost:: <pre>' . print_r($shipping_cost, true) . '</pre><br>';
        echo '-3:: $tax_cost:: <pre>' . print_r($tax_cost, true) . '</pre><br>';
        echo '-4:: $this->ReviewOrderArray:: <pre>' . print_r($this->ReviewOrderArray, true) . '</pre><br>';
        echo '-41:: 0%2e00:: <pre>' . print_r( urldecode(0%2e00) , true) . '</pre><br>';
        echo '-41:: $CheckoutArray:: <pre>' . print_r( $CheckoutArray , true) . '</pre><br>';
        echo '-42:: $tax_percent:: <pre>' . print_r( $tax_percent , true) . '</pre><br>';
      }

      include_once('lib/paypal.php');

      $requestParams = array(
      'RETURNURL' => Util::getServerHost(sfContext::getInstance()->getConfiguration()) . "check_out/paypal_payment_success", //   'http://www.yourdomain.com/payment/success',
      'CANCELURL' => Util::getServerHost(sfContext::getInstance()->getConfiguration()) . "check_out/payment"  // 'http://www.yourdomain.com/payment/cancelled'
      );

      $orderParams = array(
      'PAYMENTREQUEST_0_AMT' => $amount + $shipping_cost + $tax_cost, // Итоговая сумма для перевода. Указывать нужно два десятичных числа, разделенных точкой (.). По желанию можно использовать запятую (,) для разделения тысяч;
      'PAYMENTREQUEST_0_SHIPPINGAMT' => $shipping_cost, // Стоимость доставки заказа
      'PAYMENTREQUEST_0_TAXAMT' => $tax_cost,  //$tax_percent, // налоги
      'PAYMENTREQUEST_0_PAYMENTACTION' => 'Sale',  //$tax_percent, // налоги
      'PAYMENTREQUEST_0_CURRENCYCODE' => 'USD', // Этот параметр определяет валюту, в которой будут проводиться все операции. Указывать нужно трехзначный код. По-умолчанию установлено значение USD;
      'PAYMENTREQUEST_0_ITEMAMT' => $amount, // Итоговая стоимость, не включающая в себя комиссию, таксы, стоимость доставки и прочие доп. расходы. Если таковых не имеется, значение этого параметра должно соответствовать значению PAYMENTREQUEST_0_AMT.
      );

      $item = array();
      $Index = 0;
      $Summa = 0;
      foreach ($UsersCartArray as $NextProduct) {
        $lProduct = InventoryItemPeer::getSimilarInventoryItem($NextProduct['sku']);
        $item['L_PAYMENTREQUEST_0_NAME' . $Index] = $lProduct->getTitle();// WithRSymbol
        $item['L_PAYMENTREQUEST_0_DESC' . $Index] = $lProduct->getDescriptionShort();

        if ($NextProduct['price_type'] == 'special') {
          $item['L_PAYMENTREQUEST_0_AMT' . $Index] = $lProduct->getClearance_Or_SalePrice();
        } else {
          $item['L_PAYMENTREQUEST_0_AMT' . $Index] = $lProduct->getStdUnitPrice();
        }

        $item['L_PAYMENTREQUEST_0_QTY' . $Index] = $NextProduct['product_quantity'];
        $Summa+= $item['L_PAYMENTREQUEST_0_AMT' . $Index] * $item['L_PAYMENTREQUEST_0_QTY' . $Index];

        $Index++;
      }
      if ($is_debug)
      $PayPal = new PayPal( false /*$DeduggingPaypal*/ );


      $paypal_response = $PayPal->request("SetExpressCheckout", $requestParams + $orderParams + $item, $is_debug);
      if ($is_debug)
      if (is_array($paypal_response) && $paypal_response['ACK'] == 'Success') { // Запрос был успешно принят
        $token = $paypal_response['TOKEN'];
        if ($is_debug)
        $checkoutDetails = $PayPal->request('GetExpressCheckoutDetails', array('TOKEN' => $token) );
        if ( $is_debug ) echo '-7:: $checkoutDetails:: <pre>'. print_r( $checkoutDetails, true ) .'</pre><br>';
        if ( $DeduggingPaypal ) {
          $LocationUrl = 'Location: https://www.sandbox.paypal.com/webscr&cmd=_express-checkout&token=' . urlencode($token);
        } else {
          $LocationUrl= 'Location: https://www.paypal.com/webscr?cmd=_express-checkout&token=' . urlencode($token);
        }
        if ($is_debug) {
          echo '-8:: $LocationUrl:: <pre>' . print_r($LocationUrl, true) . '</pre><br>';
          die("is_debug");
        }
        header($LocationUrl);
        return;
      }

      return sfView::NONE;
    } catch (Exception $lException) {
      $this->logMessage($lException->getMessage(), 'err');
      return sfView::ERROR;
    }
  }

  public function executeGet_GoogleCheckout_HTML(sfWebRequest $request) {
    $response = $this->context->getResponse();
    $response->addJavascript('/js/jquery/jquery.js', 'last');
    $response->addJavascript('/js/jquery/jquery-ui.js', 'last');
    try {
    } catch (Exception $lException) {
      $this->logMessage($lException->getMessage(), 'err');
      return sfView::ERROR;
    }
  }


  public function executeGet_GoogleCheck_Button_HTML(sfWebRequest $request) {
    $response = $this->context->getResponse();
    try {
      $PaymentHTML= Cart::PrepareAndRedirectGoogleChechout();
      echo $PaymentHTML;
      return sfView::NONE;
    } catch (Exception $lException) {
      $this->logMessage($lException->getMessage(), 'err');
      return sfView::ERROR;
    }
  }

  public function executeGoogle_checkout_payment_success(sfWebRequest $request) {
    $response = $this->context->getResponse(); // http://www.adenium.net/oherron.com/web/check_out/paypal_payment_success?token=EC-7S971534L8940971K&PayerID=JTAEEQBPFEEYW {
    try {
      $time= time();
      $last_order_id= sfContext::getInstance()->getUser()->getAttribute( "last_order_id" );
      $this->UpdateOrderStatus($last_order_id, "GC",  "WO_" . $token );
      $this->redirect('@check_out_make_payment_report?transaction_id=' . $time . '&new_order_id=' . $last_order_id);
    } catch (Exception $lException) {
      $this->logMessage($lException->getMessage(), 'err');
      return sfView::ERROR;
    }
  }

  public function executePaypal_payment_success(sfWebRequest $request) {
    $response = $this->context->getResponse(); // http://www.adenium.net/oherron.com/web/check_out/paypal_payment_success?token=EC-7S971534L8940971K&PayerID=JTAEEQBPFEEYW {

    try {

      $token = $request->getParameter('token');
      $PayerID = $request->getParameter('PayerID');
      $last_order_id= sfContext::getInstance()->getUser()->getAttribute( "last_order_id" );
      $this->UpdateOrderStatus($last_order_id, "PP",  "WO_" . $token );
      include_once('lib/paypal.php');
      $PayPal = new PayPal();

      $checkoutDetails = $PayPal->request('GetExpressCheckoutDetails', array('TOKEN' => $token) );
      $orderParams = array(
      'PAYMENTREQUEST_0_AMT' => $checkoutDetails['ITEMAMT'] + $checkoutDetails['SHIPPINGAMT'] + $checkoutDetails['TAXAMT'],
      'PAYMENTREQUEST_0_CURRENCYCODE' => 'USD',
      'PAYMENTREQUEST_0_PAYMENTACTION' => 'Sale',
      'PAYMENTREQUEST_0_SHIPPINGAMT' => $checkoutDetails['SHIPPINGAMT'],
      'PAYMENTREQUEST_0_TAXAMT' => $checkoutDetails['TAXAMT'],
      'PAYMENTREQUEST_0_ITEMAMT' => $checkoutDetails['ITEMAMT']
      );

      $additionalParams = array(
      'TOKEN' => $token,
      'PAYERID' => $PayerID
      );

      $paypal_response = $PayPal->request("DoExpressCheckoutPayment", $orderParams + $additionalParams, $is_debug);
      if(is_array($paypal_response) && $paypal_response['ACK'] == 'Success') {
        $this->redirect('@check_out_make_payment_report?transaction_id=' . $token . '&new_order_id=' . $NewOrdergetId);
      } else {
        die ("ERROR");
      }

    } catch (Exception $lException) {
      $this->logMessage($lException->getMessage(), 'err');
      return sfView::ERROR;
    }
  }


  public function executePaypal_payment_cancelled(sfWebRequest $request) {
    $response = $this->context->getResponse();
    try {
    } catch (Exception $lException) {
      $this->logMessage($lException->getMessage(), 'err');
      return sfView::ERROR;
    }
  }


  public function executeGet_Department_Purchase_HTML(sfWebRequest $request) {
    $response = $this->context->getResponse();
    try {
      $url = Util::getServerHost(sfContext::getInstance()->getConfiguration()) . "check_out/make_payment";     // http://YOUR_DOMAIN.com/direct_post.php";
      $api_login_id = sfConfig::get('app_authorize_net_api_login_id');           // 'YOUR_API_LOGIN_ID';
      $transaction_key = sfConfig::get('app_authorize_net_transaction_key');     // 'YOUR_TRANSACTION_KEY';
      $md5_setting = sfConfig::get('app_authorize_net_md5_setting');         // 'YOUR_API_LOGIN_ID'; // Your MD5 Setting
      $shipping_tax_data = Cart::getTaxShipData();  //  [postal_code] => 55555 [usps] => 0 [ups_ground] => 9.36 [ups_3_day_selected] => 14.03 [ups_2_day_air] => 18.70 [ups_next_day_air_saver] => 48.79
      $CommonSum = Cart::getCommonSum();
      $amount = $CommonSum;
      echo 'Department Purchase';  //AuthorizeNetDPM::directPostDemo( $url, $api_login_id, $transaction_key, $amount, $md5_setting );
      //      return sfView::NONE;
    } catch (Exception $lException) {
      $this->logMessage($lException->getMessage(), 'err');
      return sfView::ERROR;
    }
  }

  public function executeMake_Department_Purchase_Save(sfWebRequest $request) {
    $response = $this->context->getResponse(); // http://www.adenium.net/oherron.com/web/check_out/paypal_payment_success?token=EC-7S971534L8940971K&PayerID=JTAEEQBPFEEYW {
    try {
      $token = $request->getParameter('token');
      $PayerID = $request->getParameter('PayerID');

      $NewOrdergetId = $this->MakeDataSaving("PP", $token, false );
      $this->redirect('@check_out_make_payment_report?transaction_id=' . $token . '&new_order_id=' . $NewOrdergetId);

    } catch (Exception $lException) {
      $this->logMessage($lException->getMessage(), 'err');
      return sfView::ERROR;
    }
  }

  public function executeMake_payment_report(sfWebRequest $request) {
    $response = $this->context->getResponse();
    $response->addJavascript('/js/jquery/jquery.js', 'last');
    $response->addJavascript('/js/jquery/jquery-ui.js', 'last');
    $this->transaction_id = $request->getParameter('transaction_id');
    $this->new_order_id = $request->getParameter('new_order_id');
    $this->lNewOrder = OrderedPeer::retrieveByPK($this->new_order_id);
    try {
      $this->CopiedCheckoutCopiedArray = sfContext::getInstance()->getUser()->getAttribute('CopiedCheckoutCopiedArray');
      $this->CopiedhasLoggedUser = sfContext::getInstance()->getUser()->getAttribute('CopiedhasLoggedUser');
      $this->CopiedLoggedUserEmail = sfContext::getInstance()->getUser()->getAttribute('CopiedLoggedUserEmail');
      $this->CopiedShippingAddress = sfContext::getInstance()->getUser()->getAttribute('CopiedShippingAddress');
      $this->CopiedhasBillingAddress = sfContext::getInstance()->getUser()->getAttribute('CopiedhasBillingAddress');
      $this->CopiedItemsCount = sfContext::getInstance()->getUser()->getAttribute('CopiedItemsCount');
      $this->Copied_shipping_tax_data = sfContext::getInstance()->getUser()->getAttribute('Copied_shipping_tax_data');
      $this->CopiedCommonSum = sfContext::getInstance()->getUser()->getAttribute('CopiedCommonSum');
      $this->CopiedPaymentData = sfContext::getInstance()->getUser()->getAttribute('CopiedPaymentData');
    } catch (Exception $lException) {
      $this->logMessage($lException->getMessage(), 'err');
      return sfView::ERROR;
    }

  }

}