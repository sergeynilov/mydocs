<?php
$HostForImage = AppUtils::getHostForImage(sfContext::getInstance()->getConfiguration());
$UsersCartArray = Cart::getUsersCartArray();
$UsersCartItemsSelectedCount = count($UsersCartArray);
$CheckoutArray = Cart::getShippingInfo();
$shipping_tax_data = Cart::getTaxShipData();
if ($CheckoutArray['b_is_department_purchase'] == 1) {
  $tax = 0;
  $shipping_tax_data['est_tax'] = 0;
  Cart::setTaxShipData($shipping_tax_data['est_tax'], 'tax');
} else {
  $tax= ShippingTaxes::Calculate_tax( $CheckoutArray['b_zip'], $CheckoutArray['b_state'], false );
}

if ( empty($shipping_tax_data) ) {
  $shipping_tax_data = ShippingTaxes::getShippingRates($CheckoutArray['b_zip'], true, false );
}

if ( empty($shipping_tax_data['est_tax']) ) {
  $shipping_tax_data['est_tax']= round( Cart::getCommonSum() / 100 * $tax, 2);
  $out_str = $CheckoutArray['b_zip'] . ',' . $shipping_tax_data['usps'] . ',' . $shipping_tax_data['ups_ground'] . ',' . $shipping_tax_data['ups_3_day_selected'] . ',' .
    $shipping_tax_data['ups_2_day_air'] . ',' . $shipping_tax_data['ups_next_day_air_saver'] . ',' . $shipping_tax_data['est_tax'] . ',' . ( empty($shipping_tax_data['est_shipping'])?0:$shipping_tax_data['est_shipping'] ) . ',' . ( empty($shipping_tax_data['estimated_total']) ? 0 : $shipping_tax_data['estimated_total'] );
  Cart::setTaxShipData($out_str, 'all');
}

$shipping_tax_data = Cart::getTaxShipData();

$AdditionalFreightSum= Cart::getAdditionalFreightSum();
 
?>

<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" language="JavaScript">
  <!--
  function UpdateCartProductQuantity( Sku ) {
    var product_quantity= document.getElementById("cart_product_quantity_"+Sku).value
    var Url= "<?php echo url_for('@check_out_review_order') ?>"
    Url= Url + "/mode/update/sku/"+Sku+"/product_quantity/"+product_quantity
    document.location= Url
  }

  function DeleteCartProduct( Sku ) {
    var Url= "<?php echo url_for('@check_out_review_order') ?>"
    Url= Url + "/mode/delete/sku/"+Sku
    document.location= Url
  }

  function SubmitOrder() {
    var theForm = document.getElementById("form_review_order");
    theForm.submit();
  }

  function ShippingServiceClicked( SelectedObject )
  {
    if ( SelectedObject.value == '' || typeof SelectedObject.value == "undefined" ) return true;

    var is_debug= false
    var zipcode = '<?php echo $CheckoutArray['b_zip'] ?>';
    var shipping_tax_data= <?php echo json_encode($shipping_tax_data) ?>;

    if ( is_debug ) alert( zipcode+"  SelectedObject.id::" + SelectedObject.id+"  SelectedObject.value::"+SelectedObject.value + "  shipping_tax_data::"+ var_dump(shipping_tax_data) )
    var tax= <?php echo ( !empty($tax) ? $tax : 0 ) ?>;
    var est_tax= 0;
    var est_common_sum = parseFloat('<?php echo Cart::getCommonSum(); ?>');
    var additional_freight_sum = parseFloat('<?php echo Cart::getAdditionalFreightSum(); ?>');
    var usps= 0;
    var est_total= 0;
    var CurrentShippingValue = '';
    if ( SelectedObject.value == 'UPS-Ground' ) CurrentShippingValue= shipping_tax_data['ups_ground']
    if ( SelectedObject.value == 'UPS-3Day' ) CurrentShippingValue= shipping_tax_data['ups_3_day_selected']
    if ( SelectedObject.value == 'UPS-2Day' ) CurrentShippingValue= shipping_tax_data['ups_2_day_air']
    if ( SelectedObject.value == 'UPS-NextDay' ) CurrentShippingValue= shipping_tax_data['ups_next_day_air_saver']
    if ( is_debug ) alert( "-1 tax::" + tax )

if ( is_debug ) alert( "-Z CurrentShippingValue::" + CurrentShippingValue + "  tax::" + tax )
    if (tax != '' && tax!= 0 ) {
      var est_tax   = est_common_sum / 100 * tax;
      if ( is_debug ) alert( "-1 est_tax::"+est_tax +"  est_common_sum::"+est_common_sum)
      est_tax = /*roundNumber( */ Math.round(est_tax * 100) / 100/*, 2 )*/;
      if ( is_debug ) alert( "-2 est_tax::"+est_tax +"  est_common_sum::"+est_common_sum )
            
      jQuery('#est_shipping').html('<b>$' + AddDecimalsDigit( parseFloat(CurrentShippingValue) + parseFloat(additional_freight_sum) ) + '</b>');
      if (est_tax > 0) {
        jQuery('#est_tax').html('<b>$' + AddDecimalsDigit(est_tax) + '</b>');
      }
      else {
        jQuery('#est_tax').html('<b>$0.00</b>');
      }
      if ( is_debug ) alert("-1 est_common_sum::" + est_common_sum )
      est_total = est_tax + est_common_sum ;
      est_total  = /*roundNumber( */ Math.round(est_total * 100) / 100 + parseFloat(CurrentShippingValue) + parseFloat(additional_freight_sum) /*, 2 )*/ ;
      if ( is_debug ) alert("-2 est_total::"+est_total )
      jQuery('#span_estimated_total').html('<b>$' + AddDecimalsDigit( est_total /*+ additional_freight_sum*/ ) + '</b>');
    }
    else {
      jQuery('#est_tax').html('<b>$0.00</b>');
      est_tax = 0;;
      est_total = est_common_sum + parseFloat(CurrentShippingValue) + parseFloat(additional_freight_sum);
      if ( is_debug ) alert("-3 est_total::"+est_total )
      
      jQuery('#est_shipping').html('<b>$' + AddDecimalsDigit( parseFloat(CurrentShippingValue) + parseFloat(additional_freight_sum) ) + '</b>');
      jQuery('#span_estimated_total').html( '<b>$'+ AddDecimalsDigit(est_total /*+ additional_freight_sum */ ) +'</b>' );

    }
    jQuery('#span_order_summary_est_shipping').html( '$' + ( AddDecimalsDigit( parseFloat(CurrentShippingValue) + parseFloat(additional_freight_sum) ) ) );

    jQuery('#span_order_summary_est_total').html( '$' + AddDecimalsDigit( parseFloat(est_total) /*+ parseFloat(additional_freight_sum)*/ ) );

    jQuery('#span_order_summary_est_common_sum').html( '$' + AddDecimalsDigit(est_common_sum) );

    jQuery('#span_order_summary_est_tax').html( '$' + AddDecimalsDigit(est_tax) );

    sendShipTaxInfo(zipcode, usps, shipping_tax_data['ups_ground'], shipping_tax_data['ups_3_day_selected'], shipping_tax_data['ups_2_day_air'], shipping_tax_data['ups_next_day_air_saver'], est_tax, CurrentShippingValue, est_total);

  }

  function sendShipTaxInfo(zipcode, usps, ups_ground, ups_3_day_selected, ups_2_day_air, ups_next_day_air_saver, est_tax, est_shipping, estimated_total)
  {
    if ( ups_ground == '' || typeof ups_ground == "undefined" ) return true;

    if ( ups_3_day_selected == '' || typeof ups_3_day_selected == "undefined" ) return true;
    if ( ups_2_day_air == '' || typeof ups_2_day_air == "undefined" ) return true;
    if ( ups_next_day_air_saver == '' || typeof ups_next_day_air_saver == "undefined" ) return true;
    //if ( ups_ground == '' || typeof ups_ground == "undefined" ) return true;

var url = '<?php echo url_for('main/review_order') ?>';
    var json_data = {
      'zipcode'                : zipcode,
      'usps'                   : usps,
      'ups_ground'             : ups_ground,
      'ups_3_day_selected'     : ups_3_day_selected,
      'ups_2_day_air'          : ups_2_day_air,
      'ups_next_day_air_saver' : ups_next_day_air_saver,
      'est_tax'                : est_tax,
      'est_shipping'           : est_shipping,
      'estimated_total'        : estimated_total
    };
    jQuery.post(url, json_data, function(data) {
      document.getElementById("span_loadingAnimation").style.display="none"
      return true;
    });

  }

  //-->
</script>

<style type="text/css">
  #checkout-review-order {border:4px solid #666; float:left; padding-bottom:37px; margin:0 0 0 37px; width:590px; background:#f1f2f4; margin-bottom:8em;}
  #checkout-review-order h5 {font:bold 120% arial,sans-serif; margin: 1.75em 0 .75em 2.1em; padding:0;}
  #checkout-review-order p {margin:0; padding:0; position:relative;}
  #checkout-review-order table {margin:0 0 0 2.65em; width:89%; border:none;}
  #checkout-review-order #products-table td { background-color:#fff; border-bottom:2px solid #ccc !important;}
  #checkout-review-order #products-table th {font-weight:normal; padding-bottom:3px; font-size:11px;}
  #checkout-review-order #products-table tr td img {padding:3px 0 3px 3px;}
  #checkout-review-order #shipping-and-total {background-color:#f1f2f4; padding-top:.5em; position:relative; top:-2px;}
  #checkout-review-order #total-table {border-top:2px solid #ccc !important; width:14em; position:absolute; top:8px; right:0;}
  #checkout-review-order #total-table td {border:none; border-bottom:2px solid #ccc !important; background:#fff; padding:.5em .5em .4em .5em; text-align:right; }
  #review-order-os {background-color:#f1f2f4 !important;}
  #review_order_notes {width:40em; height:10em; resize: none;}
</style>

<div id="checkout-review-order">

  <h5>Shopping Cart</h5>
  <?php if ($UsersCartItemsSelectedCount == 0) : ?>
    <strong style="margin-left:2.7em;">Your shopping cart is empty</strong>
  </div>
  <?php return; ?>
<?php endif; ?>

<?php if (!Cart::hasLoggedUser()) : ?>
  <strong style="margin-left:2.7em;">You are not logged in</strong>
  <?php return; ?>
<?php endif; ?>

<?php if (!Cart::hasShippingAddress()) : ?>
  <strong style="margin-left:2.7em;">You did not fill shipping address</strong>
  <?php return; ?>
<?php endif; ?>


<form action="<?php echo url_for('@check_out_review_order') ?>" id="form_review_order" method="POST" enctype="multipart/form-data" >
  <?php echo $form['_csrf_token']->render() ?>

  <table id="products-table" cellspacing="0" cellpadding="0" valign="top">
    <tr>
      <th colspan="2">Description</th>
      <th style=" padding-left:38px;">Qty</td>
      <th style="text-align:right; padding-right:12px;">Each</td>
      <th style="text-align:right; padding-right:12px;">Price</th>
    </tr>
    <?php for ($Row = 0; $Row < $UsersCartItemsSelectedCount; $Row++) : ?>

      <tr id="tr_product_listings_<?php echo $Row ?>">
        <?php if (!empty($UsersCartArray[$Row])) : ?>
          <?php include_partial('check_out/review_order_item', array('Row' => $Row, 'sku' => $UsersCartArray[$Row]['sku'], 'product_quantity' => $UsersCartArray[$Row]['product_quantity'],
              'price_type' => $UsersCartArray[$Row]['price_type'], 'selected_price' => $UsersCartArray[$Row]['selected_price'], 'UsersCartArray' => $UsersCartArray))
          ?>
        <?php else: ?>
          &nbsp;
        <?php endif; ?>

      <?php endfor; ?>
    </tr>
  </table>

  <table id="shipping-and-total" cellpadding="0" cellspacing="0" border="0">
    <tr>
      <td width="">
        <div style="margin:1.2em 0 2.5em 0; overflow:hidden;">
          <strong style="font-size:120%; margin-left:.25em;"><?php echo strip_tags($form['shipping_service']->renderLabel()) ?>&nbsp;</strong>
          <?php echo $form['shipping_service']->render(); ?>
 	        <span class="user_error" style="color:red;" >
            <?php echo ( $sf_request->isMethod('post') ? strip_tags($form['shipping_service']->renderError()) : "" ) ?>
 	        </span>
          &nbsp;<span style="border:0px dotted green;display:none;padding-left: 50px;" id="span_loadingAnimation"> <?php echo image_tag('loadingAnimation.gif', array('style' => "padding-top:0px;")) ?></span>
        </div>
      </td>
      <td style="position:relative;">

        <table id="total-table" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td>
              <strong>Subtotal:</strong>
            </td>
            <td>
              <strong><?php echo Cart::getCommonSum(true) ?></strong>
            </td>
          </tr>
          <tr>
            <td>
              Shipping:
            </td>
            <td>
              <span id="est_shipping" ><strong><?php echo (!empty($shipping_tax_data['est_shipping'])) ? Util::getDigitMoney($shipping_tax_data['est_shipping']+$AdditionalFreightSum, 'Money') : '' ?></strong></span>
            </td>
          </tr>
          <tr>
            <td>
              Tax:
            </td>
            <td>
              <span id="est_tax" ><strong><?php echo (!empty($shipping_tax_data['est_tax'])) ? Util::getDigitMoney($shipping_tax_data['est_tax'], 'Money') : '$0.00' ?></strong></span>
            </td>
          </tr>
          <tr>
            <td style="background:#FAE8C0;">
              <strong>Total:</strong>
            </td>
            <td style="background:#FAE8C0;">
              <span id="span_estimated_total" ><strong><?php echo (!empty($shipping_tax_data['estimated_total'])) ? Util::getDigitMoney($shipping_tax_data['estimated_total'] + $AdditionalFreightSum, 'Money') : '' ?></strong></span>
            </td>
          </tr>
        </table>

      </td>
    </tr>

    <tr>
      <td colspan="2">
        <?php echo strip_tags($form['notes']->renderLabel()) ?>&nbsp;:<br>
        <?php echo $form['notes']->render(); ?>
 	      <span class="error">
          <?php echo ( $sf_request->isMethod('post') ? strip_tags($form['notes']->renderError()) : "" ) ?>
 	      </span>
      </td>
    </tr>

    <tr>
      <td colspan="2">&nbsp;
      </td>
    </tr>

    <tr align="center">
      <td colspan="2">
        <input style="margin-top:1.2em;" class="submit-order-and-pay" type="button" onclick="javascript:SubmitOrder()" >
      </td>
    </tr>

  </table>

</form>

</div>
