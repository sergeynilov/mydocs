<?php

class Google_chart_testingController extends Controller
{
	public $layout = '//layouts/backend_main';

	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations		//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow all users to perform 'index' and 'view' actions
				'actions' => array('index', 'view',),
				'users' => array('*'),
			),
			array('allow',
				'actions' => array('orders_in_pie_chart', 'get_orders', ''),
				'users' => array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions' => array('admin', 'delete'),
				'users' => array('admin'),
			),
			array('deny', // deny all users
				'users' => array('*'),
			),
		);
	}

	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array();
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionOrders_in_pie_chart()
	{

		$orders_data_array = array();
		$orders_label_array = array();
		$orders_created_at_array = array();
		$is_post = false;
		$client_id = $this->getParameter('select_client_id');
		$department_id = $this->getParameter('select_department_id');
		$status = $this->getParameter('select_status');
		$chart_type = $this->getParameter('select_chart_type');
		$filter_created_at_from = $this->getParameter('filter_created_at_from', 'Order');
		$filter_created_at_till = $this->getParameter('filter_created_at_till', 'Order');

		$MaxDate = Order::model()->getOrdersMinMaxDate('max', array(), Appfuncs::$DATE_OUTPUT_FORMAT_STRUCTURE);
		$MinDate = Order::model()->getOrdersMinMaxDate('min', array(), Appfuncs::$DATE_OUTPUT_FORMAT_STRUCTURE);

		$data= array();
		if (!empty($_POST)) {
			$is_post = true;

			$sort = '';
			$sort_direction = '';
			if ($chart_type == 'ComboChart') {
				$goods_filter_array = array( /*1, 5 */);
				$sort = ' created_at, good_name ';
				$sort_direction = 'asc';
				$OrdersList = Order::model()->getOrdersQtySumByCreatedAtList(array('client_id' => $client_id, 'department_id' => $department_id, 'status' => $status, 'goods_list' => $goods_filter_array,
					'created_at_from' => $filter_created_at_from, 'created_at_till' => $filter_created_at_till), $sort, $sort_direction);
				$goods_list = array();
				if ( count($OrdersList) > 0 ) {
				  $goods_list[0]= 'Day';
				}
				$days_list = array();
				foreach ($OrdersList as $nextOrder) {
					if (!in_array($nextOrder->good_id, $goods_list)) {
						$goods_list[$nextOrder->good_id] = $nextOrder->good_name;
					}
					if (!in_array($nextOrder->created_at, $days_list)) {
						$days_list[/*$nextOrder->good_id*/] = $nextOrder->created_at;
					}
				}
				usort($days_list,'sort_days_list');
				$results_list = array();

				$day_subarray = array();
				foreach ($days_list as $day) {
					foreach ($goods_list as $good_id => $good_name) {
						$is_good_in_day_found = false;
						foreach ($OrdersList as $nextOrder) {
							if ($nextOrder->good_id == $good_id) {
								if ( $nextOrder->created_at == $day  ) {
								  $is_good_in_day_found = true;
								  $day_subarray[] = array( 'good_id'=>$good_id, 'order_qty_sum'=>$nextOrder->order_qty_sum, 'good_name'=>$nextOrder->good_name );
								}
							}
						} // foreach( $OrdersList as $nextOrder ) {
						if ( !$is_good_in_day_found ) {
							$day_subarray[] = array( 'good_id'=>$good_id, 'order_qty_sum'=>0, 'good_name'=> '' );
						}

					} // foreach( $goods_list as $good ) {
					$results_list[$day] = $day_subarray;
					$day_subarray= array();
				} // foreach( $days_list as $day ) {

				$data= array('goods_list'=>$goods_list, 'combo_chart_results_list'=>$results_list);
			} // if ($chart_type == 'ComboChart') {

			if ($chart_type == 'AnnotationCharts') {
				$goods_list = array( /*1, 5 */);
				$sort = 'created_at';
				$sort_direction = 'asc';
				$OrdersList = Order::model()->getOrdersQtySumByCreatedAtList(array('client_id' => $client_id, 'department_id' => $department_id, 'status' => $status, 'goods_list' => $goods_list,
					'created_at_from' => $filter_created_at_from, 'created_at_till' => $filter_created_at_till), $sort, $sort_direction);
				Appfuncs::deb($OrdersList, '$OrdersList::');
				$is_first_row = true;
				$current_created_at = '';
				$orders_by_date_subarray = array();
				foreach ($OrdersList as $Order) {
					if ($is_first_row or $current_created_at != $Order->created_at) {
						$un_created_at = strtotime($current_created_at);
						if (!$is_first_row) {
							$orders_created_at_array[] = array('created_at' => $current_created_at, 'created_at_day' => (int)strftime("%d", $un_created_at), 'created_at_month' => (int)strftime("%m",
									$un_created_at), 'created_at_year' => (int)strftime("%Y", $un_created_at), 'data' => $orders_by_date_subarray);
						}
						$current_created_at = $Order->created_at;
						$orders_by_date_subarray = array();
						$orders_by_date_subarray[] = array('good_name' => $Order->good_name, 'good_id' => $Order->good_id, 'order_qty_sum' => $Order->order_qty_sum);
					} else {
						$orders_by_date_subarray[] = array('good_name' => $Order->good_name, 'good_id' => $Order->good_id, 'order_qty_sum' => $Order->order_qty_sum);
					}
					$is_first_row = false;
				}
				$orders_created_at_array[] = array('created_at' => $current_created_at, 'created_at_day' => (int)strftime("%d", $un_created_at), 'created_at_month' => (int)strftime("%m",
						$un_created_at), 'created_at_year' => (int)strftime("%Y", $un_created_at), 'filter_created_at_from' => $filter_created_at_from, 'filter_created_at_till' => $filter_created_at_till, 'data' => $orders_by_date_subarray);
				Appfuncs::deb($orders_created_at_array, '$orders_created_at_array::');
				$data= array('labels_array' => $orders_label_array, 'created_at_array' => $orders_created_at_array);
			} // if ($chart_type == 'AnnotationCharts') {

			if ($chart_type == 'SlicesChart' or $chart_type == 'HistogramChart' or empty($chart_type)) {
				$OrdersList = Order::model()->getOrdersQtySumList(array('client_id' => $client_id, 'department_id' => $department_id, 'status' => $status, 'created_at_from' => $filter_created_at_from, 'created_at_till' => $filter_created_at_till), $sort, $sort_direction);
				foreach ($OrdersList as $Order) {
					$orders_label_array[] = $Order->good_name;
					$orders_data_array[] = $Order->order_qty_sum;
					//$orders_created_at_array[] = $Order->created_at;
				}
				$data= array('labels_array' => $orders_label_array, 'data_array' => $orders_data_array);
			} // if ($chart_type == 'SlicesChart' or $chart_type == 'HistogramChart' or empty($chart_type) ) {


		} // if (!empty($_POST)) {
		$this->render('//backend/google_chart_testing/orders_in_pie_chart', array( 'data'=> $data /*, 'orders_label_array' => $orders_label_array, 'orders_data_array' => $orders_data_array*/ ,
			'orders_created_at_array' => $orders_created_at_array, 'MaxDate' => $MaxDate, 'MinDate' => $MinDate, 'filter_created_at_from' => $filter_created_at_from, 'filter_created_at_till' => $filter_created_at_till, 'client_id' => $client_id, 'department_id' => $department_id, 'status' => $status, 'chart_type' => $chart_type, 'sort' => $sort, 'sort_direction' => $sort_direction,
			'is_post' => $is_post));
	}
}


function sort_days_list($a, $b)
{
	if (strtotime($a) == strtotime($b)) {
		return 0;
	}
	return ( strtotime($a) < strtotime($b) ) ? -1 : 1;
}