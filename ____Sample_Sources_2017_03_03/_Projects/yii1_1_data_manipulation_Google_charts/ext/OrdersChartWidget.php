<?php

class OrdersChartWidget extends CWidget
{
	public $chart_title;
	public $chart_type; // 'SlicesChart'  'HistogramChart'
	public $chart_width;
	public $dest_div_name;
	public $chart_height;
	public $data = array();

	public function run()
	{

		?>
		<script type="text/javascript" src="https://www.google.com/jsapi"></script>
		<script type="text/javascript" language="JavaScript">
			<!--


			// Load the Visualization API and the piechart package.

			// Set a callback to run when the Google Visualization API is loaded.
			<?php if ($this->chart_type == 'SlicesChart' or empty($this->chart_type) ) : ?>
			google.load('visualization', '1.0', {'packages': ['corechart']});
			google.setOnLoadCallback(drawSlicesChart);
			function drawSlicesChart() { // https://google-developers.appspot.com/chart/interactive/docs/basic_customizing_chart
				var data = new google.visualization.DataTable();
				data.addColumn('string', 'Topping');
				data.addColumn('number', 'Slices');
				data.addRows([
					<?php $L= count($this->data['labels_array']);
					for( $I= 0; $I< $L; $I++ ) : ?>
					['<?php echo $this->data['labels_array'][$I] ?>', <?php echo ( !empty($this->data['data_array'][$I]) ? $this->data['data_array'][$I] : 0 ) ?> ],
					<?php endfor; ?>
				]);

				// Set chart options
				var options = {'title': '<?php echo $this->chart_title ?>',
					'width': <?php echo $this->chart_width ?>, 'height': <?php echo $this->chart_height ?>,
					'legend': {position: 'bottom', textStyle: {color: 'blue', fontSize: 11, 'maxLines': 3 }},
					'is3D': true,
				};

				// Instantiate and draw our chart, passing in some options.
				var chart = new google.visualization.PieChart(document.getElementById('<?php echo $this->dest_div_name ?>'));
				chart.draw(data, options);
			} // function drawSlicesChart() { // https://google-developers.appspot.com/chart/interactive/docs/basic_customizing_chart
			<?php endif; ?>



			<?php if ($this->chart_type == 'HistogramChart' ) : ?>
			google.load('visualization', '1.0', {'packages': ['corechart']});
			google.setOnLoadCallback(drawHistogramChart);
			function drawHistogramChart() { // https://developers.google.com/chart/interactive/docs/gallery/histogram
				var data = google.visualization.arrayToDataTable([
					['Goods', 'Qty'],

					<?php $L= count($this->data['labels_array']);
					for( $I= 0; $I< $L; $I++ ) : ?>
					['<?php echo $this->data['labels_array'][$I] ?>', <?php echo ( !empty($this->data['data_array'][$I]) ? $this->data['data_array'][$I] : 0 ) ?> ],
					<?php endfor; ?>
				]);

				var options = {
					title: '<?php echo $this->chart_title ?>',
					legend: { position: 'none' },
				};

				var chart = new google.visualization.Histogram(document.getElementById('<?php echo $this->dest_div_name ?>'));
				chart.draw(data, options);
			}
			<?php endif; ?>


			<?php if ($this->chart_type == 'AnnotationCharts' ) : ?>
			google.load('visualization', '1.1', {'packages':['annotationchart']});
			google.setOnLoadCallback(drawAnnotationCharts);
			function drawAnnotationCharts() { // https://developers.google.com/chart/interactive/docs/gallery/annotationchart
				var data = new google.visualization.DataTable();
				data.addRows([
					<?php $L= count($this->data['created_at_array']);
					for( $I= 0; $I< $L; $I++ ) : ?>
					[new Date(<?php echo $this->data['created_at_array'][$I]['created_at_year'] ?>, <?php echo $this->data['created_at_array'][$I]['created_at_month'] ?>, <?php echo $this->data['created_at_array'][$I]['created_at_day'] ?>), <?php foreach( $this->data['created_at_array'][$I]['data'] as $good_array ) : echo " ".$good_array['order_qty_sum'].", '" . $good_array['good_name']."', '".$good_array['good_name']."', "; endforeach; ?>],
					<?php endfor;?>
				]);

				var chart = new google.visualization.AnnotationChart(document.getElementById('<?php echo $this->dest_div_name ?>'));
				var options = {
					displayAnnotations: true,
				};

				chart.draw(data, options);
			} // function drawAnnotationCharts() { // https://developers.google.com/chart/interactive/docs/gallery/annotationchart

			<?php endif; ?>


			<?php if ($this->chart_type == 'ComboChart' ) : ?>
			google.load('visualization', '1', {packages: ['corechart']});
			google.setOnLoadCallback(drawComboChart);
			function drawComboChart() {
				alert( "drawComboChart::")
				var data = google.visualization.arrayToDataTable([
					<?php $L= count($this->data['goods_list']); $I= 1; ?>
					[<?php foreach( $this->data['goods_list'] as $good_id=>$good_name ) :  echo "'".$good_name."'".( $I < $L ? ', ' : '' ); $I++; endforeach; ?>],
					<?php $L= count($this->data['combo_chart_results_list']); $I= 1; ?>
					<?php foreach( $this->data['combo_chart_results_list'] as $combo_chart_date=>$combo_chart_date_data_array ) : $LLL= count($combo_chart_date_data_array); $III= 1; ?>
					[<?php echo "'".str_replace(" 00:00:00","",$combo_chart_date)."'" ?>, <?php foreach( $combo_chart_date_data_array as $data_array ) :  if ( $III == 1) { $III++; continue; } echo $data_array['order_qty_sum'].( $III < $LLL ? ', ' : '' ); $III++; endforeach; ?> ],
					<?php endforeach; ?>
				]);

				var options = {
					title : '<?php echo $this->chart_title ?>',
					vAxis: {title: "Qty Ordered"},
					hAxis: {title: "Day"},
					seriesType: "bars",
				};

				var chart = new google.visualization.ComboChart(document.getElementById('<?php echo $this->dest_div_name ?>'));
				chart.draw(data, options);

			}  // function drawComboChart() {
			<?php endif; ?>

			//-->
		</script>
		<?php
	}

}