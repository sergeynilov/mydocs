<?php

  public static function getCodeBumpZipsByDistance($StateShortname, $Cityname, $GetOnlyFirstZip= false ) {
    $Cityname= str_replace(' ','+',$Cityname);
    $CodeBumpUnique= ConfigurationPeer::GetConfigurationValue( 'CodeBump Unique', '' );
    $CodeBumpDistance= (int)ConfigurationPeer::GetConfigurationValue( 'CodeBump Distance', 500 );

    $Url= 'http://codebump.com/services/PlaceLookup.asmx/GetPlacesWithin?AuthenticationHeader='.urlencode($CodeBumpUnique).'&place='.trim($Cityname).'&state='.$StateShortname.'&distance='.$CodeBumpDistance.'&placeTypeToFind=ZipCode';
    $Res= '';
    $GeoPlaceDistance = simplexml_load_file($Url);
    if ( !empty($GeoPlaceDistance) ) {
      foreach ($GeoPlaceDistance->GeoPlaceDistance as $GeoPlaceDistance) {
        if ( $GetOnlyFirstZip ) return (string)$GeoPlaceDistance->ToPlace;
        $Res.= $GeoPlaceDistance->ToPlace.',';
      }
    }
    return $Res;
  }



  public static function getCodeBumpByDistanceByStateCityName($StateShortname, $Cityname, $type= 'small', $ReturnType='' ) {
    $Cityname= str_replace(' ','+',$Cityname);
    $CodeBumpUnique= ConfigurationPeer::GetConfigurationValue( 'CodeBump Unique', '' );

    if ( $type== 'small' ) {
      $CodeBump_Distance_ByStateCity= ConfigurationPeer::GetConfigurationValue( 'CodeBump_Distance_ByStateCity', 50 );
    } else {
      $CodeBump_Distance_ByStateCity= (int)ConfigurationPeer::GetConfigurationValue( 'CodeBump_Maximum_Distance_ByStateCity', 200 );
    }
    Util::deb( $type, '$type::' );
    Util::deb($CodeBump_Distance_ByStateCity,'$CodeBump_Distance_ByStateCity');
    $Url= 'http://codebump.com/services/PlaceLookup.asmx/GetPlacesWithin?AuthenticationHeader='.urlencode($CodeBumpUnique).'&place='.trim($Cityname).'&state='.$StateShortname.'&distance='.$CodeBump_Distance_ByStateCity.'&placeTypeToFind=ZipCode';

    Util::deb($Url,'$Url::');
    $ResArray= array();
    $NearestDistance= -1;
    $GeoPlaceDistance = simplexml_load_file($Url);
    if ( !empty($GeoPlaceDistance) ) {
      foreach ($GeoPlaceDistance->GeoPlaceDistance as $NextGeoPlaceDistance) {
        if ( $NearestDistance == -1 or (int)$NextGeoPlaceDistance->Distance < $NearestDistance ) {
          $NearestDistance= (int)$NextGeoPlaceDistance->Distance;
        }
        $TempArray[]= array( 'ToState'=>(string)$NextGeoPlaceDistance->ToState,
        'ToPlace'=>(string)$NextGeoPlaceDistance->ToPlace,
        'Distance'=>(int)$NextGeoPlaceDistance->Distance);
        $ResArray[]= array( 'ToState'=>(string)$NextGeoPlaceDistance->ToState,
        'ToPlace'=>(string)$NextGeoPlaceDistance->ToPlace,
        'Distance'=>(int)$NextGeoPlaceDistance->Distance );
      }
    }
    return $ResArray;
  }


  public static function getNearestCityFromZip($zip) {
    $NearestCity= Util::getCodeBumpNearest( $zip );
    if ( !empty($NearestCity[0]['ToState']) and !empty($NearestCity[0]['ToPlace']) ) {
      return array( 'state_code'=>$NearestCity[0]['ToState'], 'city_name'=> $NearestCity[0]['ToPlace'] );
    }

  }

  public static function getCodeBumpNearestZips( $zip, $type= 'small', $ReturnType='' ) {
    //$Cityname= str_replace(' ','+',$Cityname);
    $CodeBumpUnique= ConfigurationPeer::GetConfigurationValue( 'CodeBump Unique', '' );
    if ( $type== 'small' ) {
      $CodeBumpDistance= (int)ConfigurationPeer::GetConfigurationValue( 'CodeBump Distance', 50 );
    } else {
      $CodeBumpDistance= (int)ConfigurationPeer::GetConfigurationValue( 'CodeBump Maximum Distance', 500 );
    }
    //
    $Url= 'http://codebump.com/services/PlaceLookup.asmx/GetPlacesWithin?AuthenticationHeader='.urlencode($CodeBumpUnique).'&place='.trim($zip).'&state=&distance='.$CodeBumpDistance.'&placeTypeToFind='.$ReturnType;
    $ResArray= array(); $TempArray= array();
    $GeoPlaceDistance = simplexml_load_file($Url);
    $NearestDistance= -1;
    if ( !empty($GeoPlaceDistance) ) {
      foreach ($GeoPlaceDistance->GeoPlaceDistance as $NextGeoPlaceDistance) {
        if ( $NearestDistance == -1 or (int)$NextGeoPlaceDistance->Distance < $NearestDistance ) {
          $NearestDistance= (int)$NextGeoPlaceDistance->Distance;
          //Util::deb($NearestDistance,'$NearestDistance::');
        }
        $TempArray[]= array( 'ToState'=>(string)$NextGeoPlaceDistance->ToState,
        'ToPlace'=>(string)$NextGeoPlaceDistance->ToPlace,
        'Distance'=>(int)$NextGeoPlaceDistance->Distance);
        $ResArray[]= array( 'ToState'=>(string)$NextGeoPlaceDistance->ToState,
        'ToPlace'=>(string)$NextGeoPlaceDistance->ToPlace,
        'Distance'=>(int)$NextGeoPlaceDistance->Distance );
      }
    }

    return $ResArray;
  }



  public static function getCodeBumpNearest( $zip ) {
    $CodeBumpUnique= ConfigurationPeer::GetConfigurationValue( 'CodeBump Unique', '' );
    $CodeBumpDistance= (int)ConfigurationPeer::GetConfigurationValue( 'CodeBump Distance', 1 );
    //
    $Url= 'http://codebump.com/services/PlaceLookup.asmx/GetPlacesWithin?AuthenticationHeader='.urlencode($CodeBumpUnique).'&place='.trim($zip).'&state=&distance='.$CodeBumpDistance.'&placeTypeToFind=City';
    $ResArray= array(); $TempArray= array();
    $GeoPlaceDistance = simplexml_load_file($Url);
    $NearestDistance= -1;
    if ( !empty($GeoPlaceDistance) ) {
      foreach ($GeoPlaceDistance->GeoPlaceDistance as $NextGeoPlaceDistance) {
        if ( $NearestDistance == -1 or (int)$NextGeoPlaceDistance->Distance < $NearestDistance ) {
          $NearestDistance= (int)$NextGeoPlaceDistance->Distance;
        }
        $TempArray[]= array( 'ToState'=>(string)$NextGeoPlaceDistance->ToState,
        'ToPlace'=>(string)$NextGeoPlaceDistance->ToPlace,
        'Distance'=>(int)$NextGeoPlaceDistance->Distance);
        $ResArray[]= array( 'ToState'=>(string)$NextGeoPlaceDistance->ToState,
        'ToPlace'=>(string)$NextGeoPlaceDistance->ToPlace,
        'Distance'=>(int)$NextGeoPlaceDistance->Distance );
        return $ResArray;
      }
    }

    return $ResArray;
  }


?>