<?php use_helper('Form') ?>
<?php use_helper('LightWindow') ?>
  <script type="text/javascript" language="JavaScript">
  <!--

  window.onload = InitPage;

  function InitPage() {
    var alert_text= '<?php echo $sf_context->getUser()->getAttribute('alert_text', '');
    $sf_context->getUser()->getAttributeHolder()->remove('alert_text'); ?>';
    if ( Trim(alert_text)!= '' ) {
      alert(alert_text)
    }
  }

  function SaveWatchAd() {
    var Message=''
    var MessageType= 'not_send'
    var Email= ''
    var HRef= '<?php echo Util::getServerHost( $this->context->getConfiguration(),true) ?>post/run_save_watch_ad?post_id=<?php echo $post_id ?>&message='+encodeURIComponent(Message)+'&message_type='+encodeURIComponent(MessageType)+
    '&email='+encodeURIComponent(Email);
    //alert( HRef );
    new Ajax.Request(HRef,{
      method:'post',
      parameters: '',
      asynchronous: false,
      onComplete: function(server)
      {
        if ( Trim(server.responseText)!="" ) {
          alert(server.responseText)
        } else {
          document.getElementById("span_save_ad_in_watchlist").style.display= "none";
          document.getElementById("span_watchlist_question_mark").style.display= "none";
          document.getElementById("span_delete_ad_from_watchlist").style.display= "inline";
        }
      }
    })
  }

  function WatchAdShowUpdatingImage(Show) {
    if ( Show ) {
      document.getElementById( "tr_WatchAd_updating_image" ).style.display= GetShowTRMethod();
    } else {
      document.getElementById( "tr_WatchAd_updating_image" ).style.display= "none";
    }
  }


  function onSubmit() {
    var theForm = document.getElementById("form_posting_view");
    var FullFieldName="email_to_friend_name"
    var S = document.getElementById(FullFieldName).value;
    if ( Trim(S) == "<?php echo PostPeer::$NameLabel ?>" ) {
      document.getElementById(FullFieldName).value=""
    }
    var FullFieldName="email_to_friend_email_to"
    var S = document.getElementById(FullFieldName).value;
    if ( Trim(S) == "<?php echo PostPeer::$EmailToLabel ?>" ) {
      document.getElementById(FullFieldName).value=""
    }
    var FullFieldName="email_to_friend_message"
    var S = document.getElementById(FullFieldName).value;
    if ( Trim(S) == "<?php echo PostPeer::$MessageLabel ?>" ) {
      document.getElementById(FullFieldName).value=""
    }
    theForm.submit();
  }

  function FieldOnFocus(FieldName) {
    var FullFieldName="email_to_friend_"+FieldName
    var FieldValue= ""
    if ( FieldName== "name" ) FieldValue= "<?php echo PostPeer::$NameLabel ?>";
    if ( FieldName== "email_to" ) FieldValue= "<?php echo PostPeer::$EmailToLabel ?>";
    if ( FieldName== "message" ) FieldValue= "<?php echo PostPeer::$MessageLabel ?>";
    var S = document.getElementById(FullFieldName).value;
    if ( Trim(S)==FieldValue ) {
      document.getElementById(FullFieldName).value= "";
    }
  }

  function FieldOnBlur(FieldName) {
    var FullFieldName="email_to_friend_"+FieldName
    var FieldValue= ""
    if ( FieldName== "name" ) FieldValue= "<?php echo PostPeer::$NameLabel ?>";
    if ( FieldName== "email_to" ) FieldValue= "<?php echo PostPeer::$EmailToLabel ?>";
    if ( FieldName== "message" ) FieldValue= "<?php echo PostPeer::$MessageLabel ?>";
    var S = document.getElementById(FullFieldName).value;
    if ( Trim(S)=="" ) {
      document.getElementById(FullFieldName).value= FieldValue;
    }
  }

  function ReportAdSave() {
    var S = document.getElementById("message").value;
    var HRef= '<?php echo Util::getServerHost( $this->context->getConfiguration(),true) ?>post/add_post_report?id=<?php echo $post_id ?>&message='+S;
    ShowUpdatingImage(true);
    new Ajax.Request(HRef,{
      method:'post',
      parameters: '',
      asynchronous: false,
      onComplete: function(server)
      {
        ShowUpdatingImage(false);
        document.getElementById("div_data").style.display= "none";
        document.getElementById("table_message").style.display= "block";
      }
    })

  }


  function ContactSellerSave() {
    //alert("BEGIN ContactSellerSave()")
    ShowUpdatingImage(true);
    var your_name= document.getElementById("contact_buyer_seller_your_name").value;
    if ( Trim(your_name)=="" ) {
      ShowUpdatingImage(false);
      alert('Fill "Your name" field ! ')
      document.getElementById("contact_buyer_seller_your_name").focus();
      return false;
    }

    var your_email= document.getElementById("contact_buyer_seller_your_email").value;
    if ( Trim(your_email)=="" ) {
      ShowUpdatingImage(false);
      alert('Fill "Your email" field ! ')
      document.getElementById("contact_buyer_seller_your_email").focus();
      return false;
    }

    if ( CheckEmail(your_email)=="" ) {
      ShowUpdatingImage(false);
      alert('Fill valid email ! ')
      document.getElementById("contact_buyer_seller_your_email").focus();
      return false;
    }

    var your_phone= document.getElementById("contact_buyer_seller_your_phone").value;

    var message= document.getElementById("contact_buyer_seller_message").value;
    if ( Trim(message)=="" ) {
      ShowUpdatingImage(false);
      alert('Fill "Message" field ! ')
      document.getElementById("contact_buyer_seller_message").focus();
      return false;
    }

    var verification= document.getElementById("contact_buyer_seller_verification").value;
    if ( Trim(verification)=="" ) {
      ShowUpdatingImage(false);
      alert('Fill "Verification" field ! ')
      document.getElementById("contact_buyer_seller_verification").focus();
      return false;
    }

    var send_to_me= document.getElementById("contact_buyer_seller_send_to_me").checked;





    var code = document.getElementById("contact_buyer_seller_verification").value;
    var HRef= '<?php echo Util::getServerHost( $this->context->getConfiguration(),true) ?>buyer/validate_captcha?code='+encodeURIComponent(code);

    //alert( HRef );
    new Ajax.Request(HRef,{
      method:'post',
      parameters: '',
      asynchronous: false,
      onComplete: function(server)
      {
        ShowUpdatingImage(false);
        //alert(server.responseText)
        if ( Trim(server.responseText)!="" ) {
          alert( var_dump(server.responseText) )
          document.getElementById("contact_buyer_seller_verification").focus();
        }

        if ( Trim(server.responseText)=="" ) { // ALL vALIDATIONS ARE good!
          var HRef= '<?php echo Util::getServerHost( $this->context->getConfiguration(),true) ?>buyer/add_contact?id=<?php echo $post_id ?>&your_name='+encodeURIComponent(your_name)+'&your_email='+encodeURIComponent(your_email)+
          '&your_phone='+encodeURIComponent(your_phone)+'&message='+encodeURIComponent(message)+
          '&verification='+encodeURIComponent(verification)+"&send_to_me="+encodeURIComponent(send_to_me);

          //alert( HRef );
          new Ajax.Request(HRef,{
            method:'post',
            parameters: '',
            asynchronous: false,
            onComplete: function(server)
            {
              ShowUpdatingImage(false);
              if ( Trim(server.responseText)!="" ) {
                alert(server.responseText)
              } else {
                document.getElementById("table_data").style.display= "none";
                document.getElementById("table_message").style.display= "block";
                document.getElementById("span_to_email").innerHTML= your_email
              }
            }
          })


        }

      }
    })
    //alert("END ContactSellerSave()")
  }



  function ShowUpdatingImage(Show) {
    if ( Show ) {
      document.getElementById( "tr_updating_image" ).style.display= GetShowTRMethod();
      //document.getElementById( "tr_input_UserCitysOfStates" ).style.display= "none";
    } else {
      document.getElementById( "tr_updating_image" ).style.display= "none";
      //document.getElementById( "tr_input_UserCitysOfStates" ).style.display= GetShowTRMethod();
    }
  }


  function MakeSearch() {
    var keyword= document.getElementById("keyword").value
    var search_category= document.getElementById("search_category").value
    var search_zip= document.getElementById("search_zip").value
    var search_type= GetSearchType()//document.getElementById("hid_search_type").value;

    var HRef= '<?php echo Util::getServerHost( $this->context->getConfiguration(),true) ?>search?keyword='+encodeURIComponent(keyword)+'&category='+encodeURIComponent(search_category)
    +'&zip='+encodeURIComponent(search_zip)+
    '&adtype='+encodeURIComponent(search_type);
    //alert(HRef)
    document.location= HRef;
  }

  function ShowReportAd() {
    var H= screen.availHeight - 160;
    var W= screen.availWidth - 200;
    var naProps = window.open( '<?php echo url_for($Posting->getPrintSemanticUrls()) ?>',
    "print_ad","status=no,modal=yes,scrollbars=1,width="+W+",height="+H+",left="+GetCenteredLeft(W)+", top="+GetCenteredTop(H) );
  }

  function ShowZIP(zip) {
    var H= screen.availHeight - 160;
    var W= screen.availWidth - 200;
    var naProps = window.open( "/post/showzip?zip="+encodeURIComponent(zip)+"",
    "showzip","status=no,modal=yes,scrollbars=1,width="+W+",height="+H+",left="+GetCenteredLeft(W)+", top="+GetCenteredTop(H) );
  }


  function DeleteAdFromWatchlist(PostId) {
    if ( !confirm(" Do you want to delete this ad from watchlist ? ") ) return;
    var HRef= '<?php echo Util::getServerHost( $this->context->getConfiguration(),true) ?>post/delete_ad_from_watch?post_id='+PostId;

    new Ajax.Request(HRef,{
      method:'post',
      parameters: '',
      asynchronous: false,
      onComplete: function(server)
      {
        //alert(server.responseText)
        if ( Trim(server.responseText)!="" ) {
          alert(server.responseText)
        } else {
          //alert("This post was deleted from Watchlist !")
          document.getElementById("span_save_ad_in_watchlist").style.display= "inline";
          document.getElementById("span_delete_ad_from_watchlist").style.display= "none";
        }
      }
    })

  }
  //-->
  </script>
<form action="<?php echo url_for('@post_view?id='.$post_id) ?>" id="form_posting_view" method="POST">
  <?php $PostingImagesDir= sfConfig::get('app_application_postingimages_directory');
  $ad_width= sfConfig::get('app_application_ad_width');
  $ad_height= sfConfig::get('app_application_ad_height');
  $ad_thumbnail_width= sfConfig::get('app_application_ad_thumbnail_width');
  $ad_thumbnail_height= sfConfig::get('app_application_ad_thumbnail_height');
  $ad_max_width= (int)sfConfig::get('app_application_ad_max_width');
  $ad_max_height= (int)sfConfig::get('app_application_ad_max_height');
  ?>
  <script type="text/javascript" language="JavaScript">
  <!--


  function MakeImageCurrent( ImageName, ImageIndex ) {
    var HRef= '<?php echo Util::getServerHost( $this->context->getConfiguration(),true) ?>main/get_image?image_name=<?php echo "/images/".$PostingImagesDir.'/' ?>'+ImageName+'&image_id=big_image&width=<?php echo $ad_width ?>&height=<?php echo $ad_height ?>';
    new Ajax.Request(HRef,{
      method:'post',
      parameters: '',
      asynchronous: false,
      onComplete: function(server)
      {
        document.getElementById( "div_big_image" ).innerHTML= server.responseText;
      }
    })
    document.getElementById("big_image").src= "/images/<?php echo $PostingImagesDir.'/' ?>"+ImageName;
  }

  //-->
  </script>

<div class="b-content-box b-second">
                <div class="b-left">
                                <div class="b-nav">
                        <ul class="b-nav-list">
                                                <li><?php
                                                $location_state_multiplier= (int)sfConfig::get('app_application_location_state_multiplier');
                                                $StateName= '';
                                                $StateZip= '';
                                                $StateId= 0;
                                                if ( !empty($lState) ) {
                                                  $StateName= $lState->getName();
                                                  $StateId= $lState->getId();
                                                  $StateZip= $lState->getFirstZip();
                                                }
                                                $CityName= '';
                                                $CityId= 0;
                                                $CityZip= '';
                                                if ( !empty($lCity) ) {
                                                  $CityName= $lCity->getName();
                                                  $CityId= $lCity->getId();
                                                  $CityZip= $lCity->getFirstZip();
                                                }
                                                $StateLocationId= $StateId*$location_state_multiplier;
                                                $CityLocationId= $StateId*$location_state_multiplier+$CityId;
                                                if ( $StateLocationId > 0 ) {
                                                  echo link_to($StateName,'@search?zip='.$StateZip);
                                                }
                                                ?></li>
                                                <li><?php if ( $StateLocationId > 0 ) echo '>' ?></li>
                                                <li><?php
                                                if ( $CityLocationId > 0 ) {
                                                  echo link_to($CityName,'@search?zip='.$CityZip);
                                                }
                                                ?></li>
                                                <li><?php if ( $CityLocationId > 0 ) echo '>' ?></li>

                                                <li><?php $locState= StatePeer::getStateByShortName( $Posting->getStateCode() );
                                                $StateName= !empty($locState)?$locState->getName():'';
                                                echo '<a onclick="javascript:document.getElementById(\'search_zip\').value=\''.$Posting->getStateCode().'\'; MakeSearch(); " style="cursor: pointer;" >'.
                                                $StateName.'</a> -> ' .
                                                '<a onclick="javascript:document.getElementById(\'search_zip\').value=\''.$Posting->getCityName().'\'; MakeSearch(); " style="cursor: pointer;" >' . $Posting->getCityName().'</a>'; ?></li>&nbsp;

                                                <!-- <li><?php echo stripslashes($Posting->getTitle()) ?></li> -->

                                        </ul>
                    <div class="b-nav-back"><?php
                    $PostPreviewArr= $sf_context->getUser()->getAttribute('post_preview_'.$Posting->getId());
                    $sf_context->getUser()->getAttributeHolder()->remove('post_preview_'.$Posting->getId());
                    echo
                    link_to('Back to search results',
                    '@search?keyword='.(!empty($PostPreviewArr['keyword'])?$PostPreviewArr['keyword']:'').
                    '&category='.(!empty($PostPreviewArr['category'])?$PostPreviewArr['category']:'').
                    '&zip='.(!empty($PostPreviewArr['zip'])?$PostPreviewArr['zip']:'').
                    '&adtype='.(!empty($PostPreviewArr['adtype'])?$PostPreviewArr['adtype']:'') ) ;
                    ?>&nbsp;</div>
                    <br clear="all" />
                </div>
                <div class="b-content-inbox">
                        <div class="b-headline">
                        <h2><?php echo stripslashes($Posting->getTitle()).' <span>['.$Posting->getArea().']</span>' ?></h2>
                        <div class="b-info">
                          Posted by <?php echo lw_link($Posting->getFirstName(), '@contact_seller?id='.$post_id, 'width=660 height=520') ?> on <?php echo $Posting->getUpdatedAt( sfConfig::get('app_application_date_time_month_astext' ) ) ?>
                            <span><i></i><?php echo link_to("Remove, edit, renew",'@edit_ad?id='.$Posting->getId() ) ?> </span>
                                                            </div>
                      </b>
                    </div>
                    <div class="b-content-img">
                            <table class="b-content-img-tbl">
                                <tr>
                                    <td>
                                                                        <?php if ( count($PostImagesList)> 0 ) : ?>
                                    <div class="postBigImg" id="div_big_image" >
                                    <?php
                                    if ( file_exists( str_replace( "\\", "/", sfConfig::get('sf_web_dir')."/images/".$PostingImagesDir.'/'.$PostImagesList[0]->getImage()) ) ) {
                                      $ImageFileName= "/images/".$PostingImagesDir.'/'.$PostImagesList[0]->getImage();
                                    }
                                    else {
                                      $ImageFileName= "/images/noimage.png";
                                    }
                                    $NewSize= Util::GetImageShowSize( $ImageFileName, $ad_width, $ad_height );
                                    $ImageTag= image_tag( $ImageFileName, array('alt'=>"View picture", 'width'=>$NewSize['Width'], 'height'=>$NewSize['Height'], 'id'=>'big_image',  'style'=>"cursor:hand", 'border'=>'none' ) ) ?>
                                    <a style="cursor:pointer;" onclick="javascript:ShowImageDialog('<?php echo $ImageFileName ?>')"> <?php echo $ImageTag ?> </a>
                                    </div>
                                    <?php else: ?>
                                    No images
                                    <?php endif ?>
                                </td>
                                <td>
                                <table border=0 cellpadding="0" cellspacing="0">
                                    <tr>
                                       <td class="postLeftCont" >

                                       </td>
                                      <td valign="top" class="postSmImg">
                                        <?php for($I=0; $I< count($PostImagesList); $I++ ) :

                                        if ( file_exists( str_replace( "\\", "/", sfConfig::get('sf_web_dir')."/images/".$PostingImagesDir.'/'.$PostImagesList[$I]->getImage()) ) ) {
                                          $ImageFileName= "/images/".$PostingImagesDir.'/'.$PostImagesList[$I]->getImage();
                                        }
                                        else {
                                          $ImageFileName= "/images/noimage.png";
                                        }

                                        $NewSize= Util::GetImageShowSize( $ImageFileName, $ad_thumbnail_width, $ad_thumbnail_height );
                                        ?>
                                          <?php if( $I==0 ): ?>
                                            <div id="div_image_0">
                                            <?php echo image_tag( $ImageFileName, array('alt'=>"View picture", 'onclick'=>"javascript:MakeImageCurrent('".$PostImagesList[$I]->getImage()."', ".$I." )", 'width'=>$NewSize['Width'], 'height'=>$NewSize['Height'], 'id'=>'image_'.$I ) ) ?>
                                            </div>
                                          <?php else: ?>
                                            <div id="div_image_<?php echo $I ?>" class="b-small-img">
                                            <?php echo image_tag( $ImageFileName, array('alt'=>"View picture", 'onclick'=>"javascript:MakeImageCurrent('".$PostImagesList[$I]->getImage()."', ".$I." )", 'width'=>$NewSize['Width'], 'height'=>$NewSize['Height'], 'id'=>'image_'.$I ) ) ?>
                                            </div>
                                          <?php endif; ?>
                                        <?php endfor; ?>
                                      </td>
                                    </tr>
                                  </table>
                              </td>
                          </tr>
                        </table>
                    </div>
                    <div class="b-content-text">
                                  <div class="b-text-left">
                                <!--<h3>Brand new kitchen tables to sell. </h3>-->
                                                        <p><?php echo stripslashes(str_replace("\n","<br>",$Posting->getDescription() ))?></p>
                        </div>
                        <div class="b-text-right">
                                <ul class="b-goods-list">
                                    <li>Price: $<?php echo $Posting->getPrice() ?></li>
                                <li>Category:
                                                                        <?php $CategoriesText= ''; $I=0;
                                                                        foreach( $lPostCategoryList as $lPostCategory ) {
                                                                          $lCategory= $lPostCategory->getCategory();
                                                                          if( !empty($lCategory) ) {
                                                                            $CategoriesText.= link_to( $lCategory->getName(). ( (count($lPostCategoryList)-1) == $I ?'':', ') ,'@search?category=' . $lCategory->getId() ) ;
                                                                          }
                                                                          $I++;
                                                                        }
                                                                        echo $CategoriesText; //= substr($CategoriesText,0,strlen($CategoriesText)-2);
                                    ?>
                                </li>
                                <li>Location: <a style="cursor:pointer;" onclick="javascript:ShowZIP('<?php echo $Posting->getZip() ?>')" >Show on Map</a></li>
                            </ul>
                        </div>
                        <br clear="all" />
                    </div>
                </div>
                <div class="b-feedback">
                        <div class="b-feedback-left">
                            <?php include_partial( 'post_view_info_tasks', array('Posting'=> $Posting, 'post_id'=>$post_id, 'isPostInWatchlist'=>$isPostInWatchlist ) ) ?>
                    </div>
                        <div class="b-feedback-right">
                            <?php include_partial( 'post_view_send_message', array('form'=> $form ) ) ?>
                    </div>
                    <br clear="all" />
                    </div>
            </div>
            <?php include_partial( 'search/random_banner_image', array( ) ) ?>
            <br clear="all" />
        </div>
    </div>
</form>