<?php

class import_csv {

//calculate numbers of operations done to show info text
  private $ProductDiagramPageSetsAdded= 0;
  private $ProductDiagramPageSetsSkipped= 0;
  
  private $ProductDiagramPagesAdded= 0;
  private $ProductDiagramPagesSkipped= 0;
  
  private $ProductDiagramPage_to_partsAdded= 0;
  private $ProductDiagramPage_to_partsSkipped= 0;
  private $SourceDir='';  // source directory of data to import
  private $OriginalFilename=''; // original filename for output message
  private $ProductId;  // host product id of uploaded data
  private $MessagesArray= array();
  
  public function setOriginalFilename($pOriginalFilename) { // set original filename for output message 
    $this->OriginalFilename= $pOriginalFilename;
  }

  public function setProductId($pProductId) { // set host product id of uploaded data
    $this->ProductId= $pProductId;
  }
  
  public function setSourceDir($pSourceDir) { // set source directory of data to import
    $this->SourceDir= $pSourceDir;
    if (!is_dir($this->SourceDir)) {
      echo '"'.  $this->SourceDir .'" is not a valid directory !';
      return false;
    }
    return true;
  }

  private function GetImageXLSFileName($CsvFileName, $Ext) {
    $A= split('\.',$CsvFileName);
    $ImageFileName= '';
    if (count($A)==2) $ImageFileName= $A[0].'.'.$Ext;
    return $ImageFileName;
  }

  private function getPageNumber($CsvFileName) { //C350-1.csv
    $A= split('\.',$CsvFileName);
    $PageNumber= '';
    if (count($A)==2) {
      $A= split( '\-', $A[0] );
      if (count($A)==2) {
        $PageNumber= $A[1];
      } else {                        
        return Util::getRightDigit($A[0]);
      }
    }
    return $PageNumber; 
  } // private function getPageNumber($CsvFileName, $NoSlash= false) { //C350-1.csv

  private function ReadCSVData( $ProductName, $ProductId ) { // Read csv-files from $this->SourceDir.$ProductName directory
    include_once('../plugins/sfCsvPlugin/lib/sfCsvReader.class.php'); // include libraries
    include_once('../plugins/sfExcelReaderPlugin/lib/vendor/ExcelReader/excel_reader2.php');
    include_once('../plugins/sfExcelReaderPlugin/lib/sfExcelReader.class.php');

    $DataArray= '';
    $SourceDir= $this->SourceDir.$ProductName;
    if (is_dir($SourceDir)) { //  Source Directory with files of different format
      if ($dh = opendir($SourceDir)) {
        while (($file = readdir($dh)) !== false) { // read all files in turn
          $ResToPartArray= array( array('columnId'=>'', 'columnCode'=>''), array('columnId'=>'', 'columnCode'=>'') );
          $Ext= strtolower( Util::GetFileNameExt($file) );
          if ( $Ext == 'csv' ) { // work any csv file and similar gif/xls files by name
            $PageNumber= $this->getPageNumber($file);    // get page number as file can be like C350-1.csv 
            $CsvFileName= $SourceDir.DIRECTORY_SEPARATOR.$file;
            $ImageFileName= $this->GetImageXLSFileName($CsvFileName,'gif');  // get name of relative file gif/xls by its index
            $ExcelFileName= $this->GetImageXLSFileName($CsvFileName,'xls');
            $ExcellArray = new sfExcelReader($ExcelFileName); // read all data from xls file
            $reader = new sfCsvReader( $CsvFileName );        // read all data from csv file
            $reader->open();
            $reader->setSelectColumns(array('columnId', 'columnCode'));
            while ( $data = $reader->read() ) {
              $ResToPartArray[]= $data;
            }
            $reader->close();
            $DataArray[]= array( 'PageNumber'=> $PageNumber, 'ToPartArray'=> $ResToPartArray, 'ImageFileName'=> $ImageFileName, 'FullImageFileName' => $ImageFileName ,  'ExcellArray'=> $ExcellArray->sheets[0]['cells'] );
          } // if ( $Ext == 'csv' ) { // work any csv file  and similar gif/xls files by name
        }
        closedir($dh);
      }
    }
    return $DataArray; 
  } // private function ReadCSVData( $ProductName, $ProductId ) {

  private function getPageSetsListByDirs() { // get list of pagesets from source dir
    $ResArray= array();
    if ($dh = opendir($this->SourceDir)) {
      while (($file = readdir($dh)) !== false) {
        if ( !is_dir($this->SourceDir.DIRECTORY_SEPARATOR.$file) or $file=='.' or $file== '..' )continue;
        $ResArray[]= $file;
      }
    }
    return $ResArray;
  }

  public function Run() {
    $ProductDiagramPagesetsArray= $this->getPageSetsListByDirs(); // get list of pagesets from source dir
    foreach( $ProductDiagramPagesetsArray as $ProductDiagramPagesetName ) { // list of pagesets
      $lProductDiagramPageset= ProductdiagrampagesetPeer::getSimilarProductDiagramPageset( $ProductDiagramPagesetName, $this->ProductId, -1, false );
      if ( !empty($lProductDiagramPageset) ) { // if we already have similar ProductDiagramPageset(by its name) do not add it but, show line in message.
        $this->MessagesArray[]= array( 'type'=>'ProductDiagramPageset', 'id'=>$lProductDiagramPageset->getId(), 'error'=>'Already exists Product Diagram Pageset with name "'.$ProductDiagramPagesetName.'", ID:'.$lProductDiagramPageset->getId() );
        $this->ProductDiagramPageSetsSkipped++;
        continue;
      }
      $lNewProductDiagramPageset= new ProductDiagramPageset();
      $lNewProductDiagramPageset->setName($ProductDiagramPagesetName);
      $lNewProductDiagramPageset->setProductId( $this->ProductId );
      $lNewProductDiagramPageset->setActive( true );
      $lNewProductDiagramPageset->save(); // add new Product Diagram Pageset
      $this->ProductDiagramPageSetsAdded++;
      $CvsDataArray= $this->ReadCSVData( $ProductDiagramPagesetName, $lNewProductDiagramPageset->getId() ); // Read csv-files from $this->SourceDir.$ProductName directory
      foreach( $CvsDataArray as $CvsData ) { // List of files for any $ProductDiagramPagesetName
        $ToPartArray= $CvsData['ToPartArray']; // read all fields 
        $ImageFileName= $CvsData['ImageFileName'];
        $ExcellArray= $CvsData['ExcellArray'];
        $PageNumber= $CvsData['PageNumber'];
        $FullImageFileName= $CvsData['FullImageFileName'];        
        
        $lProductDiagramPage= ProductdiagrampagePeer::getSimilarProductDiagramPage( $PageNumber, $lNewProductDiagramPageset->getId(), -1, false );
        if ( !empty($lProductDiagramPage) ) { // if we already have similar Product Diagram Page(by its Page Number and Product Diagram Pageset Id) do not add it but, show line in message.
          $this->MessagesArray[]= array( 'type'=>'ProductDiagramPage', 'id'=>$lNewProductDiagramPage->getId(), 'error'=>'Already exists Product Diagram Page with name "'.$PageNumber.'", ID:'.$lProductDiagramPage->getId() );
          $this->ProductDiagramPagesSkipped++;
          continue;
        }
        
        $lNewProductDiagramPage= new ProductDiagramPage();
        $lNewProductDiagramPage->setName( $PageNumber );
        $lNewProductDiagramPage->setProductDiagramPagesetId( $lNewProductDiagramPageset->getId() );
        $lNewProductDiagramPage->setActive( true );
        $lNewProductDiagramPage->setPagenumber( $PageNumber );
        $lNewProductDiagramPage->save(); // add new Product Diagram Page
        $this->ProductDiagramPagesAdded++;
        $Ext= strtolower( Util::GetFileNameExt($ImageFileName) );

        $images_product_images_dir= sfConfig::get('app_application_images_product_DiagramPage_dir' );
        $DestFilename= 'images/' . $images_product_images_dir . DIRECTORY_SEPARATOR . 'product_DiagramPage_'.$lNewProductDiagramPage->getId().'.'.$Ext;

        copy( $FullImageFileName, $DestFilename ); // copy product image
        chmod( $DestFilename, 0777 );
        $lNewProductDiagramPage->setFileName('product_DiagramPage_'.$lNewProductDiagramPage->getId().'.'.$Ext);
        $lNewProductDiagramPage->save(); // in newly added Product Diagram Page save ref to image file
        
        
        $LLL= count($ToPartArray);
        for( $III=2; $III< $LLL; $III++ ) { // add all Product diagram page To Part(detailization of product) rows
          $SKU= trim( $ToPartArray[$III]['columnCode'] );
          if ( empty($SKU) or $SKU=='_' or $SKU=='-' ) {
            $this->ProductDiagramPage_to_partsSkipped++;
            continue;
          }
          $PartRef= '';
          if ( !empty($ToPartArray[$III]['columnId']) ) {
            $PartRef= $ToPartArray[$III]['columnId'];
          }
          $lProduct= ProductPeer::getProductBySKU($SKU);
          if ( empty($lProduct) ) { // check that exists product with sku in Product diagram page To Part row
            $this->MessagesArray[]= array( 'type'=>'ProductDiagramPage_to_part', 'id'=>$lNewProductDiagramPage->getId(), 'error'=>'There is no Product with SKU "'.$SKU.'", ID:'.$lNewProductDiagramPage->getId() );
            $this->ProductDiagramPage_to_partsSkipped++;
            continue;
          }
                    
          $lProductDiagramPageToPart= ProductdiagrampageToPartPeer::getSimilarProductDiagramPageToPart( $PartRef, $lNewProductDiagramPage->getId(), -1, false );
          if ( !empty($lProductDiagramPage) ) { // if we already have similar Product Diagram Page(by its Part Ref and Product Diagram Page Id) do not add it but, show line in message.
            $this->MessagesArray[]= array( 'type'=>'ProductDiagramPage', 'id'=>$lNewProductDiagramPage->getId(), 'error'=>'Already exists Product Diagram Page with name "'.$PageNumber.'", ID:'.$lProductDiagramPage->getId() );
            $this->ProductDiagramPagesSkipped++;
            continue;
          }

          $lNewProductDiagramPage_to_part= new ProductdiagrampageToPart();
          $lNewProductDiagramPage_to_part->setProductDiagramPageId(  $lNewProductDiagramPage->getId()  );

          $lNewProductDiagramPage_to_part->setProductDiagramPagePartRef( $PartRef );
          $lNewProductDiagramPage_to_part->setPartId( $lProduct->getId() );
          $lNewProductDiagramPage_to_part->save(); // add new Product Diagram Page to part
          $this->ProductDiagramPage_to_partsAdded++;
        }  // for( $III=2; $III++; $III<=$LLL ) { // $LLL= count($ToPartArray);        
      } // foreach( $CvsDataArray as $CvsData ) { // List of files for any $ProductDiagramPagesetName

    } // foreach( $ProductDiagramPagesetsArray as $ProductDiagramPagesetName ) { // list of pagesets
    
  } // public function Run() {

  public function getInfoText() { // show info text with numbers of operations done
    $Res= '<hr>';
    $lProduct= ProductPeer::retrieveByPK( $this->ProductId );
    if ( !empty($lProduct) ) {
      $Res.= 'Selected Product: <b>'.$lProduct->getName().'</b>, ID: <b>'.$lProduct->getId().'</b>. <br>&nbsp;&nbsp;';
    }
    $Res.= 'Filename loaded: <b>'.$this->OriginalFilename.'</b>. <br><br>&nbsp;&nbsp;';
    
    $Res.= ' Product Diagram Page Sets Added: <b>' . $this->ProductDiagramPageSetsAdded.'</b><br>&nbsp;&nbsp;'.
    'Product Diagram Page Sets Skipped: <b>' . $this->ProductDiagramPageSetsSkipped . '</b><br><br>&nbsp;'.
    'Product Diagram Pages Added: <b>'. $this->ProductDiagramPagesAdded.'</b><br>&nbsp;&nbsp;'.
    'Product Diagram Pages Skipped: <b>'.$this->ProductDiagramPagesSkipped.'</b><br>&nbsp;&nbsp;'.
    'Product Diagram Page To Parts Added: <b>' . $this->ProductDiagramPage_to_partsAdded.'</b><br>&nbsp;&nbsp;'.
    'Product Diagram Page To Parts Skipped: <b>'.$this->ProductDiagramPage_to_partsSkipped.'</b><br>&nbsp;';
    return $Res;
  }

  public function getMessagesText() {
    $Res= '<hr>'.'<h3>Messages:</h3>';
    foreach( $this->MessagesArray as $Message ) {
      $Res.= 'Type:'.$Message['type'].',  error:'.$Message['error'].'<br>&nbsp;&nbsp;';
    }
    $Res.= '<hr>';    
    return $Res;
  }
  
}

    // That is how it is called :
    //include_once('../lib/import_csv.php');
    try
    {
      $this->MessagesText='';
      $this->form = new ImportCsvForm();
      if ($request->isMethod('post')) // form is submitted
      {
        $data= $request->getParameter('import_csv'); // selected file for import
        $this->form->bind( $data, $request->getFiles('import_csv') );
        if ( $this->form->isValid() )
        {
          $TempFileName= $_FILES['import_csv']['tmp_name']['zip_file']; // which zip file was selected
          $OrigFileName= $_FILES['import_csv']['name']['zip_file'];
          $Ext= strtolower( Util::GetFileNameExt($OrigFileName) );
          $UnpackedImportZip_dir= sfConfig::get('app_application_UnpackedImportZip_dir' );  // where to upack zip file for read data from it
          $ImportZipArchieve_dir= sfConfig::get('app_application_ImportZipArchieve_dir' );  // destination directory to keep archieve directory
          $TimeString= strftime( '%Y-%m-%d %H:%M:%S', time());
          $ZipFilename= str_replace(' ','_',$ImportZipArchieve_dir . DIRECTORY_SEPARATOR . 'archieve_zip_'.$TimeString ).'.'.$Ext;
          rename( $TempFileName, $ZipFilename );
          chmod( $ZipFilename, 0777 );

          $UnpackedZipDir= $UnpackedImportZip_dir . DIRECTORY_SEPARATOR . 'UnpackedZip_' . $TimeString;
          mkdir($UnpackedZipDir, 0777);

          $zip = new ZipArchive;
          if ($zip->open($ZipFilename) === TRUE) {
            $zip->extractTo($UnpackedZipDir ); // unpack zip file to directory
            $zip->close();
            //echo 'loaded.';
          } else {
            echo 'failed.';
          }
          $import_csv= new import_csv();
          $sf_root_dir= sfConfig::get('sf_root_dir');
          $SourceDir= $this->getUnpackedDataDir($UnpackedZipDir); // directory where archive zip will be unpacked
          $import_csv->setSourceDir($SourceDir); // set source directory of data to import
          $import_csv->setProductId($data['part_id']);  // host product id of uploaded data
          $import_csv->setOriginalFilename($OrigFileName);   // set original filename for output message 
          $import_csv->Run(); // run import
          $this->MessagesText= $import_csv->getMessagesText();
          $this->InfoText= $import_csv->getInfoText();
          
          $lErrorlog= new Errorlog(); // save history of uploading
          $lErrorlog->setClassname('Batch parts import');
          $lErrorlog->setMessage( $this->InfoText);
          $lErrorlog->setTrace( $this->MessagesText );
          $lErrorlog->setFile( $OrigFileName );
          $lErrorlog->setLine( $data['part_id'] );
          $lErrorlog->setCode( $SourceDir );
          if ( !empty($_SERVER['REQUEST_URI']) ) {
            $lErrorlog->setRequestUri( $_SERVER['REQUEST_URI'] );
          }
          $lErrorlog->setPostData( print_r($_POST,true) );
          $lErrorlog->setSessionData( print_r($_SESSION,true) );
                    $IP= ( !empty($HTTP_SERVER_VARS['REMOTE_ADDR']) ) ? $HTTP_SERVER_VARS['REMOTE_ADDR'] : ( ( !empty($HTTP_ENV_VARS['REMOTE_ADDR']) ) ? $HTTP_ENV_VARS['REMOTE_ADDR'] : getenv('REMOTE_ADDR') );
          $lErrorlog->setIp( $IP );
          $lErrorlog->setReferer( $_SERVER['HTTP_REFERER'] );
          $lErrorlog->setUserAgent( $_SERVER['HTTP_USER_AGENT'] );
          $lErrorlog->save(  );
          
        }
      }

    }
    catch (Exception $lException)
    {
      $this->logMessage($lException->getMessage(), 'err');
      return sfView::ERROR;
    }

?>