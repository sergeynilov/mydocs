<?php

require 'lib/model/om/BaseProductPeer.php';


/**
 * Skeleton subclass for performing query and update operations on the 'product' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    lib.model
 */


class ProductPeer extends BaseProductPeer {
  public static $IsPartArray= array( 0=>'Not Part', 1=>'Is Part' );
  public static $IsInactiveArray= array( 0=>'Active', 1=>'Inactive' );

  public static $IsFeaturedProductArray= array( 0=>'Not Featured Product', 1=>'Featured Product' );
  public static $IsFeaturedSpecialOfferArray= array( 0=>'Not Featured Special Offer', 1=>'Featured Special Offer' );

  
  public static function getProductDiagramPages( $parent_DiagramPageset_id ) {
    $c = new Criteria();
    $c->add( ProductDiagramPagesetPeer::ID, $parent_DiagramPageset_id );
    $c->addJoin(   ProductDiagramPagesetPeer::ID, ProductDiagramPagePeer::PRODUCTDIAGRAMPAGESETID,  Criteria::JOIN );
    
    $c->addAscendingOrderByColumn(ProductDiagramPagePeer::NAME);
    return ProductDiagramPagePeer::doSelect( $c );    
  }

  public static function getProductDiagramPagesets( $product_id ) {
    $c = new Criteria();
    $c->add( ProductDiagramPagesetPeer::PRODUCT_ID, $product_id );
    $c->addAscendingOrderByColumn(ProductDiagramPagesetPeer::ID);        
    return ProductDiagramPagesetPeer::doSelect( $c );    
  }

  
  public static function getProductImages( $product_id ) {
    $c = new Criteria();
    $c->add( ProductImagePeer::PRODUCT_ID, $product_id );
    $c->addAscendingOrderByColumn(ProductImagePeer::ID);        
    return ProductImagePeer::doSelect( $c );    
  }
  
  public static function getProductFeatures( $product_id ) {
    $c = new Criteria();
    $c->add( ProductFeaturePeer::PRODUCT_ID, $product_id );
    $c->addAscendingOrderByColumn(ProductFeaturePeer::ID);        
    return ProductFeaturePeer::doSelect( $c );    
  }
  
  public static function getProductSpecification( $product_id ) {
    $c = new Criteria();
    $c->add( ProductSpecificationPeer::PRODUCT_ID, $product_id );
    $c->addAscendingOrderByColumn(ProductSpecificationPeer::ID);        
    return ProductSpecificationPeer::doSelect( $c );    
  }
  
  public static function getProductSize( $product_id ) {
    $c = new Criteria();
    $c->add( ProductSizePeer::PRODUCT_ID, $product_id );
    $c->addAscendingOrderByColumn(ProductSizePeer::ID);        
    return ProductSizePeer::doSelect( $c );    
  }

  public static function getTiedProducts( $product_id, $ReturnOnlyCode= false ) {
    $c = new Criteria();
    $c->addJoin(   TiedProductPeer::TIED_PRODUCT_ID, ProductPeer::ID,  Criteria::JOIN );
    
    $c->add( TiedProductPeer::PARENT_PRODUCT_ID, $product_id );

    //$c->add( ProductPeer::ISPART, true );
    $c->addAscendingOrderByColumn(ProductPeer::NAME);  
    if ( $ReturnOnlyCode ) {
      $ResArray= array();
      $TiedProductList= ProductPeer::doSelect( $c );
      foreach( $TiedProductList as $TiedProduct ) {
        $ResArray[]= $TiedProduct->getId();
      }
      
      return $ResArray;
    }
    return ProductPeer::doSelect( $c );    
  }

  public static function getTiedCategories( $product_id, $ReturnOnlyCode= false ) {
    $c = new Criteria();
    $c->addJoin(  CategoryTiedProductPeer::CATEGORY_ID, CategoryPeer::ID,  Criteria::JOIN  );
    
    $c->add( CategoryTiedProductPeer::PARENT_PRODUCT_ID, $product_id );

    $c->addAscendingOrderByColumn(CategoryPeer::NAME);  
    if ( $ReturnOnlyCode ) {
      $ResArray= array();
      $TiedProductList= ProductPeer::doSelect( $c );
      foreach( $TiedProductList as $TiedProduct ) {
        $ResArray[]= $TiedProduct->getId();
      }
      
      return $ResArray;
    }
    return CategoryPeer::doSelect( $c );    
  }

  
  public static function getProducts( $page=1, $ReturnPager=true, $filter_name='', $filter_brand_id='', $filter_category_id='', $filter_ispart='', $filter_inactive= '', $filter_featured_product= '', $filter_sku='', $Sorting='NAME', $is_selection, $parent_product_id ) {
        
    $c = new Criteria();
    $SkipTiedProductsList= array();
    if ( $is_selection and $parent_product_id ) {      
      $SkipTiedProductsList= ProductPeer::getTiedProducts( $parent_product_id, true );
      $SkipTiedProductsList[]= $parent_product_id;
      //$c->add( ProductPeer::ID, $SkipTiedProductsList, Criteria::NOT_IN );
      //$filter_ispart=1; // in selection show products only with ispart = true
    }
    if ( !empty($filter_name) and $filter_name!= '-' ) {
      $c->add( ProductPeer::NAME , '%'.$filter_name.'%', Criteria::LIKE );
    }
    if ( !empty($filter_brand_id) and $filter_brand_id!= '-' ) {
      $c->add( ProductPeer::BRAND_ID , $filter_brand_id );
    }
    if ( !empty($filter_category_id) and $filter_category_id!= '-' ) {
      $c->add( ProductPeer::CATEGORY_ID , $filter_category_id );
    }
    if ( strlen($filter_ispart) > 0 and $filter_ispart!= '-' ) {
      $c->add( ProductPeer::ISPART , $filter_ispart );
    }
    
    if ( strlen($filter_inactive) > 0 and $filter_inactive!= '-' ) {
      $c->add( ProductPeer::INACTIVE , $filter_inactive );
    }
    if ( strlen($filter_featured_product) > 0 and $filter_featured_product!= '-' ) {
      $c->add( ProductPeer::FEATURED_PRODUCT , $filter_featured_product );
    }
    if ( strlen($filter_sku) > 0 and $filter_sku!= '-' ) {
      $c->add( ProductPeer::SKU , '%'.$filter_sku.'%', Criteria::LIKE );
    }

    
    if ( $Sorting=='CREATED_AT' or empty($Sorting) ) {
      $c->addDescendingOrderByColumn(ProductPeer::CREATED_AT);
    }
    if ( $Sorting=='NAME' ) {
      $c->addAscendingOrderByColumn(ProductPeer::NAME);    
    }
    if ( $Sorting=='CATEGORY_ID' ) {
      $c->addJoin( ProductPeer::CATEGORY_ID, CategoryPeer::ID,  Criteria::JOIN );
      $c->addAscendingOrderByColumn(CategoryPeer::NAME);    
    }
    if ( $Sorting=='BRAND_ID' ) {
      $c->addJoin( ProductPeer::BRAND_ID, BrandPeer::ID, Criteria::JOIN );
      $c->addAscendingOrderByColumn(BrandPeer::NAME);    
    }
    if ( $Sorting=='ISPART' ) {
      $c->addAscendingOrderByColumn(ProductPeer::ISPART);
    }
    
    if ( $Sorting=='INACTIVE' ) {
      $c->addAscendingOrderByColumn(ProductPeer::INACTIVE);
    }
    if ( $Sorting=='FEATURED_PRODUCT' ) {
      $c->addAscendingOrderByColumn(ProductPeer::FEATURED_PRODUCT);
    }
    if ( $Sorting=='FEATURED_SPECIAL_OFFER' ) {
      $c->addAscendingOrderByColumn(ProductPeer::FEATURED_SPECIAL_OFFER);
    }
    if ( $Sorting=='SKU' ) {
      $c->addAscendingOrderByColumn(ProductPeer::SKU);
    }
    
    if ( !$ReturnPager ) {
      return ProductPeer::doSelect( $c );
    }    
    $pager = new sfPropelPager( 'Product', (int)sfConfig::get('app_application_rows_in_pager' ) );
    $pager->setPage( $page );
    $pager->setCriteria($c);
    $pager->init();
    return $pager;
  }

  public static function getSimilarProduct($ProductName, $pProductId, $IsEqual=true, $ByField='NAME')
  {
    $c = new Criteria();
    if ( $ByField == 'NAME' ) {
      $c->add( ProductPeer::NAME, $ProductName );
    }
    if ( $ByField == 'SKU' ) {
      $c->add( ProductPeer::SKU, $ProductName );
    }
    if ( $IsEqual ) {
      $c->add( ProductPeer::ID, $pProductId );
    } else {
      $c->add( ProductPeer::ID, $pProductId, Criteria::NOT_EQUAL );
    }
    if($lResult = ProductPeer::doSelect( $c ) )
    {
      return $lResult[0];
    }
    return '';
  }

  public static function getProductBySKU( $SKU )
  {
    $c = new Criteria();
    $c->add( ProductPeer::SKU, $SKU );
    return ProductPeer::doSelectOne( $c );
  }
  
} // ProductPeer
