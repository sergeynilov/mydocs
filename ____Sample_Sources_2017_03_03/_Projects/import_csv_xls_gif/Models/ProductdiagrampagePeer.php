<?php

require 'lib/model/om/BaseProductdiagrampagePeer.php';


/**
 * Skeleton subclass for performing query and update operations on the 'productDiagramPage' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    lib.model
 */
class ProductdiagrampagePeer extends BaseProductdiagrampagePeer {

  public static $ActiveArray= array( 1=>'Is Active', 0=>'Not Active' );

  public static function getSimilarProductDiagramPage( $Name, $ProductDiagramPagesetId, $ProductDiagramPageId, $IsEqual=true )
  {
    $c = new Criteria();
    $c->add( ProductDiagramPagePeer::PRODUCTDIAGRAMPAGESETID, $ProductDiagramPagesetId );
    $c->add( ProductDiagramPagePeer::NAME, $Name );
    if ( $IsEqual ) {
      $c->add( ProductDiagramPagePeer::ID, $ProductDiagramPageId );
    } else {
      $c->add( ProductDiagramPagePeer::ID, $ProductDiagramPageId, Criteria::NOT_EQUAL );
    }
    if($lResult = ProductDiagramPagePeer::doSelect( $c ) )
    {
      return $lResult[0];
    }
    return '';
  }
  
} // ProductdiagrampagePeer
