<?php

require 'lib/model/om/BaseProductdiagrampageToPartPeer.php';


/**
 * Skeleton subclass for performing query and update operations on the 'productDiagramPage_to_part' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    lib.model
 */
class ProductdiagrampageToPartPeer extends BaseProductdiagrampageToPartPeer {
  
  public static function getProductDiagramPage_to_parts( $parent_DiagramPage_id ) {
    $c = new Criteria();
    $c->add( ProductdiagrampageToPartPeer::PRODUCTDIAGRAMPAGEID, $parent_DiagramPage_id );    
    $c->addAscendingOrderByColumn(ProductdiagrampageToPartPeer::PRODUCTDIAGRAMPAGEPARTREF);
    return ProductdiagrampageToPartPeer::doSelect( $c );    
  }
  
  public static function getProductDiagramPage_to_partsByPartId( $PartId ) {
    $c = new Criteria();
    $c->add( ProductdiagrampageToPartPeer::PARTID, $PartId );    
    $c->addAscendingOrderByColumn(ProductdiagrampageToPartPeer::PRODUCTDIAGRAMPAGEPARTREF);
    return ProductdiagrampageToPartPeer::doSelect( $c );    
  }
  
  public static function getSimilarProductDiagramPageToPart( $productDiagramPagePartRef, $parent_DiagramPage_id, $ProductDiagramPage_to_partId, $IsEqual=true )
  {
    $c = new Criteria();
    $c->add( ProductdiagrampageToPartPeer::PRODUCTDIAGRAMPAGEID, $parent_DiagramPage_id );
    $c->add( ProductdiagrampageToPartPeer::PRODUCTDIAGRAMPAGEPARTREF, $productDiagramPagePartRef );
    if ( $IsEqual ) {
      $c->add( ProductdiagrampageToPartPeer::ID, $ProductDiagramPage_to_partId );
    } else {
      $c->add( ProductdiagrampageToPartPeer::ID, $ProductDiagramPage_to_partId, Criteria::NOT_EQUAL );
    }
    return ProductdiagrampageToPartPeer::doSelectOne( $c );
  }

  
} // ProductdiagrampageToPartPeer
