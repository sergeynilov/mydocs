<?php

require 'lib/model/om/BaseProductdiagrampagesetPeer.php';


/**
 * Skeleton subclass for performing query and update operations on the 'productDiagramPageset' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    lib.model
 */
class ProductdiagrampagesetPeer extends BaseProductdiagrampagesetPeer {

  public static $ActiveArray= array( 1=>'Is Active', 0=>'Not Active' );

  public static function getSimilarProductDiagramPageset( $Name, $ProductId, $ProductDiagramPagesetId, $IsEqual=true )
  {
    $c = new Criteria();
    $c->add( ProductDiagramPagesetPeer::PRODUCT_ID, $ProductId );
    $c->add( ProductDiagramPagesetPeer::NAME, $Name );
    if ( $IsEqual ) {
      $c->add( ProductDiagramPagesetPeer::ID, $ProductDiagramPagesetId );
    } else {
      $c->add( ProductDiagramPagesetPeer::ID, $ProductDiagramPagesetId, Criteria::NOT_EQUAL );
    }
    if($lResult = ProductDiagramPagesetPeer::doSelect( $c ) )
    {
      return $lResult[0];
    }
    return '';
  }
  
} // ProductdiagrampagesetPeer
