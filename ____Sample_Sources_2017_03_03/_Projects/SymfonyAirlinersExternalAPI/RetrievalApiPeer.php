<?php

/**
 * Subclass for performing query and update operations on the 'retrieval_api' table.
 *
 * 
 *
 * @package lib.model
 */ 
class RetrievalApiPeer extends BaseRetrievalApiPeer
{

  public static $GetInformationArray= array( 'TOP'=>'Top Destination', 'MOST'=>'Most searched' );


  public static function getRetrievalApis()
  {
    $c = new Criteria();
    return RetrievalApiPeer::doSelect($c);
  }

  public static function getRetrievalApiByMethod($Method)
  {
    $c = new Criteria();
    $c->add( RetrievalApiPeer::METHOD, $Method );
    return RetrievalApiPeer::doSelectOne($c);
  }

  public static function getDate($DateUnix,$RetType, $AddZero=false) {
    if ( $RetType=='Day' ) {
      $Res= strftime( "%d", $DateUnix );
    }
    if ( $RetType=='YearMonth' ) {
      $Res= strftime( "%Y%m", $DateUnix );
    }
    if ( $RetType=='Month' ) {
      $Res= strftime( "%m", $DateUnix );
    }
    if ( empty($Res) ) return '';
    return ( (int)$Res<10 and $AddZero ? '0' : '' ).$Res;
  }  // getDate($Liner['return_depDate'],'Day',true)

  public static function getHostSiteLinkByService( $ServiceName, $Liner ) {
    $CurrentCulture= sfContext::getInstance()->getUser()->getCulture();
    $select_to= sfContext::getInstance()->getRequest()->getParameter('select_to');
    $select_from= sfContext::getInstance()->getRequest()->getParameter('select_from');
    $adults= sfContext::getInstance()->getRequest()->getParameter('adults');
    $childs= sfContext::getInstance()->getRequest()->getParameter('childs');
    $infants= sfContext::getInstance()->getRequest()->getParameter('infants');
    $datedepartures= sfContext::getInstance()->getRequest()->getParameter('datedepartures');
    $datereturns= sfContext::getInstance()->getRequest()->getParameter('datereturns');
    $flights_only= sfContext::getInstance()->getRequest()->getParameter('flights_only');
    $select_class= sfContext::getInstance()->getRequest()->getParameter('select_class');
    $single= sfContext::getInstance()->getRequest()->getParameter('single');



    if ($ServiceName=='Barcelo') {
      if ( empty($Liner['deeplink']) ) $ServiceName;

      $TradeDoublerUrl='';
      $TradeDoublerEnd='';
      $ProgId= '';//sfConfig::get('app_Barcelo_ProgId' );
      $Idioma='ES';
      if ( $CurrentCulture== 'pt_PT' ) {
        $ProgId= sfConfig::get('app_Barcelo_ProgIdPortugal' );
        $TradeDoublerCode= sfConfig::get('app_Barcelo_TradeDoublerPortugalCode' );
        $GParamPortugalCode= sfConfig::get('app_Barcelo_GParamPortugalCode' );
        $TradeDoublerUrl='http://clkuk.tradedoubler.com/click?p('.$ProgId.')a('.$TradeDoublerCode.')g('.$GParamPortugalCode.')url(';
        $TradeDoublerEnd=')';
        $Idioma='PT';
      }
      if ( $CurrentCulture== 'es_ES'  ) {
        $ProgId= sfConfig::get('app_Barcelo_ProgIdSpain' );
        $TradeDoublerCode= sfConfig::get('app_Barcelo_TradeDoublerSpainCode' );
        $GParamSpainCode= sfConfig::get('app_Barcelo_GParamSpainCode' );
        $TradeDoublerUrl='http://clkuk.tradedoubler.com/click?p('.$ProgId.')a('.$TradeDoublerCode.')g('.$GParamSpainCode.')url(';
        $TradeDoublerEnd=')';
        $Idioma='ES';
      }
      $Url=$TradeDoublerUrl.$Liner['deeplink'].$TradeDoublerEnd;
    } // if ($ServiceName=='Barcelo') {


    if ($ServiceName=='Spanair') {
      if ( empty($Liner['deeplink']) ) $ServiceName;
      $TradeDoublerUrl='';
      $TradeDoublerEnd='';
      $ProgId= '';//sfConfig::get('app_Spanair_ProgId' );
      if ( $CurrentCulture== 'pt_PT' /*or $CurrentCulture== 'pt_BR'*/ ) {
        $ProgId= sfConfig::get('app_Spanair_ProgIdPortugal' );
        $TradeDoublerCode= sfConfig::get('app_Spanair_TradeDoublerPortugalCode' );
        $GParamPortugalCode= sfConfig::get('app_Spanair_GParamPortugalCode' );
        $TradeDoublerUrl='http://clk.tradedoubler.com/click?a('.$TradeDoublerCode.')p('.$ProgId.')g('.$GParamPortugalCode.')url(';
        $TradeDoublerEnd=')';
      }
      if ( $CurrentCulture== 'es_ES'  ) {
        $ProgId= sfConfig::get('app_Spanair_ProgIdSpain' );
        $TradeDoublerCode= sfConfig::get('app_Spanair_TradeDoublerSpainCode' );
        $GParamSpainCode= sfConfig::get('app_Spanair_GParamSpainCode' );
        $TradeDoublerUrl='http://clkuk.tradedoubler.com/click?p('.$ProgId.')a('.$TradeDoublerCode.')g('.$GParamSpainCode.')url(';
        $TradeDoublerEnd=')';
      }
      $UrlCountry='com';
      $Url='';
      $Url=$TradeDoublerUrl.$Liner['deeplink'].$TradeDoublerEnd;

    } // if ($ServiceName=='Spanair') {


    if ( empty($Url) ) return $ServiceName;
    return '<a href="'.$Url.'" target="_blank" >'.Util::getServiceNameByCode($ServiceName).'</a>';
  }


...


  //////////////////// Logitravel Begin ///////////////////////
  public static function getLogitravelXMLData(  $depCity,  $arrCity,  $depDate,  $depTime,  $adults,  $childs,  $infants,  $oneWay, $retDate,  $retTime, $idxFlght,  $mktportal, $utm_source, $utm_medium,  $lng,  $ctry, $select_class, $LowcostParam, $UrlCircle, $PreparedXML  )
  {
    // for example, SPAIN uses the Buscar_3 service, PORTUGAL uses the Buscar_2)
    //Util::deb( $oneWay, '$oneWay::');
    $CurrentCulture= sfContext::getInstance()->getUser()->getCulture();
    //Util::deb($CurrentCulture,'$CurrentCulture::');
    $ServiceName='Buscar_3';
    if ($CurrentCulture=='pt_PT') {
      $ServiceName='Buscar_2';
    }
    $Url= 'http://www.logitravel.pt/agregadoraereo/webservice/wsaereo.asmx/'.$ServiceName.'?Medio=tradedoublerpt&Origen='.$depCity.'&Destino='.$arrCity.'&FechaIda='.$depDate.'&FechaVuelta='.$retDate.'&IdaYVuelta='.(!$oneWay?'true':'false').'&Adultos='.$adults.'&Ninyos='.$childs.'&Bebes='.$infants.'&clase='./*$select_class.*/'&vuelosLowCost='.
    ($LowcostParam?"True":"False").'&residente=False';
    $debug= sfContext::getInstance()->getRequest()->getParameter('debug' );
    if ( $debug ) {
      if ( !$UrlCircle ) return $Url;
      $F= $PreparedXML;
    } else {
      $ResArray= array();
      if ( $debug ) {
        Util::deb($Url,'$Url');
        $F= Util::FileAsString( $Url, false );
      } else {
        try {
          $F= Util::FileAsString( $Url, false );
        }
        catch (Exception $lException) {}
      }
    }
    return RetrievalApiPeer::LogitravelXMLparse( $F, (int)$adults,  (int)$childs,  (int)$infants );
  }


  public static function LogitravelXMLparse( $str, $adults,  $childs,  $infants )
  {
    if ( empty($str) ) return array();


    // x&idioma=PT&anuncio=2702992&idConsulta=43180910&origenSV
    $debug= sfContext::getInstance()->getRequest()->getParameter('debug' );
    //if ( $debug ) Util::deb($str,'$str::');
    $AnuncioPattern= '/anuncio='.'(.*?)idConsulta=/mix';
    $A= preg_match( $AnuncioPattern, $str, $Res );
    $AnuncioValue= '';
    if ( !empty($Res[1]) ) {
      $AnuncioValue= $Res[1];
      $AnuncioValue= str_replace('&amp;','',$AnuncioValue);
      $AnuncioValue= str_replace('&','',$AnuncioValue);
    }
    //if ( $debug ) Util::deb($AnuncioValue,'$AnuncioValue::');




    $idConsultaPattern= '/idConsulta='.'(.*?)origenSV/mix';
    $A= preg_match( $idConsultaPattern, $str, $Res );
    //if ( $debug ) Util::deb($Res,'???$Res::');
    $ConsultaValue= '';
    if ( !empty($Res[1]) ) {
      $ConsultaValue= $Res[1];
      $ConsultaValue= str_replace('&amp;','',$ConsultaValue);
      $ConsultaValue= str_replace('&','',$ConsultaValue);
    }
    //if ( $debug ) Util::deb($ConsultaValue,'$ConsultaValue::');


    $debug= sfContext::getInstance()->getRequest()->getParameter('debug' );
    $ResArray= array();
    try {
      if ( $debug ) {
        $xml = new SimpleXMLElement($str);
      } else {
        try {
          $xml = new SimpleXMLElement($str);
        }
        catch (Exception $lException) {return array();}
      }
      //Util::deb($xml,'$xml::');



      if (!$xml) {
        //Util::deb("ERROR::");
        $errors = libxml_get_errors();
        foreach ($errors as $error) {
          //echo display_xml_error($error, $xml);
        }
        libxml_clear_errors();
      }
      //Util::deb("Before::");

      $StartAirport= '';
      $EndAirport= '';
      $Price= '';
      $StartDate='';
      $StartTime='';
      $EndDate='';
      $EndTime='';
      $NumStops='';
      $Company='';
      $CompanyName='';
      $Currency='';

      $Return_StartAirport= '';
      $Return_EndAirport= '';
      $Return_Price= '';
      $Return_StartDate='';
      $Return_StartTime='';
      $Return_EndDate='';
      $Return_EndTime='';
      $Return_NumStops='';
      $Return_Company='';
      $Return_CompanyName='';
      $Return_Currency='';
      foreach( $xml->vuelo as $VueloObj ) { // Any Price Item
        //Util::deb("INSIDE::");
        //Util::deb($VueloObj,'$VueloObj::');
        $Par=(string)$VueloObj['par'];
        $PriceStr= (string)$VueloObj['tot'];
        $Price= floatval( str_replace(',','.',$PriceStr) );
        $Return_Price= $Price;
        // Util::deb($Par,'$Par::');
        $IsVueloReturn= false;
        foreach( $VueloObj as $Key=>$VueloStruct ) { // get Departure in 1st circle and Return in second
          //Util::deb($Key,'$Key::');
          //Util::deb($VueloStruct,'$VueloStruct::');
          if ( !$IsVueloReturn ) {
            $CarrierCode=(string)$VueloStruct['num'];//(string)$VueloStruct['tip'];
            //Util::deb($CarrierCode,'$CarrierCode::');


            $lIataAirline= IataAirlinePeer::getIataAirlineByCode(substr($CarrierCode,0,2));
            $CompanyName=substr($CarrierCode,0,2);
            if ( !empty($lIataAirline) ) {
              $CompanyName=$lIataAirline->getName();
            }
            //Util::deb($CompanyName,'$CompanyName::');
            $StartAirport=(string)$VueloStruct['ori'];
            $EndAirport= (string)$VueloStruct['des'];
            $StartDate= (string)$VueloStruct['fec'];
            $StartTime= (string)$VueloStruct['sal'];
            $EndTime= (string)$VueloStruct['lleg'];
            $EndDate= Util::AddDayByBothTimes( Util::StrToDate( $StartDate, "DMYSlashes" ), $StartTime, $EndTime, 'DMYSlashed' );
            $NumStops= 0;
            $Currency= 'EUR';
          } else{
            $CarrierCode=(string)$VueloStruct['num'];//(string)$VueloStruct['tip'];
            //Util::deb($CarrierCode,'$CarrierCode::');
            $lIataAirline= IataAirlinePeer::getIataAirlineByCode(substr($CarrierCode,0,2));
            $Return_CompanyName=substr($CarrierCode,0,2);
            if ( !empty($lIataAirline) ) {
              $Return_CompanyName=$lIataAirline->getName();
            }
            //Util::deb($Return_CompanyName,'$CarrierName::');
            $Return_StartAirport=(string)$VueloStruct['ori'];
            $Return_EndAirport= (string)$VueloStruct['des'];

            $Return_StartDate= (string)$VueloStruct['fec'];
            $Return_StartTime= (string)$VueloStruct['sal'];
            $Return_EndTime= (string)$VueloStruct['lleg'];
            $Return_EndDate= Util::AddDayByBothTimes( Util::StrToDate( $Return_StartDate, "DMYSlashes" ), $Return_StartTime, $Return_EndTime, 'DMYSlashed' );
            $Return_EndTime= (string)$VueloStruct['lleg'];
            $Return_NumStops= 0;
            $Return_Currency= 'EUR';
          }
          $IsVueloReturn= true;
        } // foreach( $VueloObj as $Key=>$VueloStruct ) { // get Departure in 1st circle and Return in second

        $ResArray[]= array(
        'Service' => 'Logitravel',
        'Price' => $Price,
        'PricesArray' => array( 'Departure'=>$Price,'Return'=>$Return_Price ),
        'depCity' => $StartAirport,
        'arrCity' => $EndAirport,
        'depDate_string' => $StartDate,
        'depDate' => Util::strToDate($StartDate,"DMYSlashes"),
        'depHour' => $StartTime,
        'arrDate_string' => $EndDate,
        'arrDate' => Util::strToDate($EndDate,"DMYSlashes"),
        'arrHour' => $EndTime,
        'company' => $CompanyName,
        'stops' => $NumStops,
        'currency' => $Currency,
        'deeplink' => $Par,
        'return_Price' => $Return_Price,
        'return_depCity' => $Return_StartAirport,
        'return_arrCity' => $Return_EndAirport,
        'return_depDate_string' => $Return_StartDate,
        'return_depDate' => Util::strToDate($Return_StartDate,"DMYSlashes"),
        'return_depHour' => $Return_StartTime,
        'return_arrDate_string' => $Return_EndDate,
        'return_arrDate' => Util::strToDate($Return_EndDate,"DMYSlashes"),
        'return_arrHour' => $Return_EndTime,
        'return_company' => $Return_CompanyName,
        'return_stops' => $Return_NumStops,
        'return_currency' => $Return_Currency,
        'param1'=>$AnuncioValue,
        'param2'=>$ConsultaValue,
        );

        //Util::deb( $ResArray, '$ResArray::' );
        //die("ERRE");

      } // foreach( $xml->vuelo as $VueloObj ) { // Any Price Item
      //Util::deb( $ResArray, '$ResArray::' );
      return $ResArray;
      //die("ResArray::");

    } // try {

    catch ( Exception $lException ) {
    }

  }

  //////////////////// Logitravel END ///////////////////////






  //////////////////// Netviagens Begin ///////////////////////
  public static function getNetviagensXMLData(  $depCity,  $arrCity,  $depDate,  $depTime,  $adults,  $childs,  $infants,  $oneWay, $retDate,  $retTime, $idxFlght,  $mktportal, $utm_source, $utm_medium,  $lng,  $ctry, $select_class, $UrlCircle, $PreparedXML  )
  {

    //Util::deb($depTime,'$depTime::');
    //Util::deb($retTime,'$retTime::');
    //Util::deb($select_class, '$select_class::');
    $Username= sfConfig::get('app_Netviagens_Username' );
    $Password= sfConfig::get('app_Netviagens_Password' );

    $Url= 'http://ws.netviagens.com/air.asmx/GetFares_GET?username='.$Username.'&Password='.$Password.'&startAirport='.$depCity.'&endAirport='.$arrCity.'&startDate='.$depDate.'&startTimeWindow='.$depTime.'&endTimeWindow=&returnDate='.$retDate.'&returnStartTimeWindow=&returnEndTimeWindow='.$retTime.'&airVendor=&adults='.$adults.'&children='.$childs.'&infants='.$infants.'&class='.$select_class.'&uselowfare=true';  // Class
    // http://ws.netviagens.com/air.asmx/GetFares_GET?username=NZanox7&Password=NJ3ABH6R&startAirport=lis&endAirport=mad&startDate=20090701&startTimeWindow=&endTimeWindow=&returnDate=&returnStartTimeWindow=&returnEndTimeWindow=&airVendor=&adults=1&children=0&infants=0
    $debug= sfContext::getInstance()->getRequest()->getParameter('debug' );
    if ( $debug ) {
      if ( !$UrlCircle ) return $Url;
      $F= $PreparedXML;
    } else {

      $ResArray= array();
      if ( $debug ) {
        Util::deb($Url,'$Url');
        $F= Util::FileAsString( $Url, false );
      } else  {
        try {
          $F= Util::FileAsString( $Url, false );
        }
        catch (Exception $lException) {}
      }
    }
    return RetrievalApiPeer::NetviagensXMLparse( $F, (int)$adults,  (int)$childs,  (int)$infants );

  }


  public static function NetviagensXMLparse( $str, $adults,  $childs,  $infants )
  {
    if ( empty($str) ) return array();
    $debug= sfContext::getInstance()->getRequest()->getParameter('debug' );
    $ResArray= array();
    try {
      if ( $debug ) {
        $xml = new SimpleXMLElement($str);
      } else {
        try {
          $xml = new SimpleXMLElement($str);
        }
        catch (Exception $lException) {return array();}
      }
      //Util::deb($xml,'$xml::');
      if (!$xml) {
        //Util::deb("ERROR::");
        $errors = libxml_get_errors();
        foreach ($errors as $error) {
          //echo display_xml_error($error, $xml);
        }
        libxml_clear_errors();
      }
      //Util::deb("Before::");


      $StartAirport= '';
      $EndAirport= '';
      $Price= '';
      $StartDate='';
      $StartTime='';
      $EndDate='';
      $EndTime='';
      $NumStops='';
      $Company='';
      $Currency='';

      $Return_StartAirport= '';
      $Return_EndAirport= '';
      $Return_Price= '';
      $Return_StartDate='';
      $Return_StartTime='';
      $Return_EndDate='';
      $Return_EndTime='';
      $Return_NumStops='';
      $Return_Company='';
      $Return_Currency='';
      foreach( $xml->AirFares as $AirFaresObj ) {
        //Util::deb("INSIDE::");
        //Util::deb($AirFaresObj,'$AirFaresObj::');

        foreach( $AirFaresObj->Result as $ResultObj ) {
          //Util::deb($ResultObj,'$ResultObj::');

          foreach( $ResultObj->FareItem as $FareItemObj ) {
            //Util::deb($FareItemObj,'$FareItemObj::');

            foreach( $FareItemObj->AirAvail as $AirAvailObj ) {
              //Util::deb($AirAvailObj,'$AirAvailObj::');


              $IsAirAvailReturn= false;
              foreach( $AirAvailObj->ResultSector as $ResultSectorObj ) { // Netviagens
                //Util::deb($AirAvailObj->ResultSector,'$AirAvailObj->ResultSector::');

                $NumStopsCount=-1; $DataWasFound= false;
                foreach( $ResultSectorObj->Connection as $ConnectionObj ) {
                  $DataWasFound= true;
                  //  Util::deb($ConnectionObj,'$ConnectionObj::');
                  $NumStopsCount++;
                  foreach( $ConnectionObj->Sector as $SectorObj ) {
                    //Util::deb($SectorObj,'$SectorObj::');
                    if ( $IsAirAvailReturn ) {
                      $Return_StartAirport= $SectorObj->StartAirport;
                      //Util::deb($Return_StartAirport,'$Return_StartAirport::');
                      $Return_EndAirport= $SectorObj->EndAirport;
                      //Util::deb($Return_EndAirport,'$Return_EndAirport::');
                      $Return_StartDate= (string)$SectorObj->StartDate;
                      //Util::deb($Return_StartDate, '$Return_StartDate::');
                      $Return_StartTime= (string)$SectorObj->StartTime;
                      $Return_EndDate= (string)$SectorObj->EndDate;
                      //Util::deb($Return_EndDate,'$Return_EndDate::');
                      $Return_EndTime= (string)$SectorObj->EndTime;
                      $Return_NumStops= $NumStopsCount;  //(int)$SectorObj->NumStops;
                      $Return_Company= (string)$SectorObj->AirVendor->Name;
                    } else {
                      $StartAirport= $SectorObj->StartAirport;
                      //Util::deb($StartAirport,'$StartAirport::');
                      $EndAirport= $SectorObj->EndAirport;
                      //Util::deb($EndAirport,'$EndAirport::');
                      $StartDate= (string)$SectorObj->StartDate;
                      $StartTime= (string)$SectorObj->StartTime;
                      $EndDate= (string)$SectorObj->EndDate;
                      $EndTime= (string)$SectorObj->EndTime;
                      $NumStops= $NumStopsCount;
                      $Company= (string)$SectorObj->AirVendor->Name;
                    }
                    //die("TTTTT");



                  } //  foreach( $ResultSectorObj->Sector as $SectorObj ) {
                } // foreach( $ResultSectorObj->Connection as $ConnectionObj ) {
                if ( !$DataWasFound ) {
                  $NumStopsCount++;
                  foreach( $ResultSectorObj->Sector as $SectorObj ) {
                    //Util::deb($SectorObj,'$SectorObj::');
                    if ( $IsAirAvailReturn ) {
                      $Return_StartAirport= $SectorObj->StartAirport;
                      //Util::deb($Return_StartAirport,'$Return_StartAirport::');
                      $Return_EndAirport= $SectorObj->EndAirport;
                      //Util::deb($Return_EndAirport,'$Return_EndAirport::');
                      $Return_StartDate= (string)$SectorObj->StartDate;
                      //Util::deb($Return_StartDate, '$Return_StartDate::');
                      $Return_StartTime= (string)$SectorObj->StartTime;
                      $Return_EndDate= (string)$SectorObj->EndDate;
                      //Util::deb($Return_EndDate,'$Return_EndDate::');
                      $Return_EndTime= (string)$SectorObj->EndTime;
                      $Return_NumStops= $NumStopsCount;  //(int)$SectorObj->NumStops;
                      $Return_Company= (string)$SectorObj->AirVendor->Name;
                    } else {
                      $StartAirport= $SectorObj->StartAirport;
                      //Util::deb($StartAirport,'$StartAirport::');
                      $EndAirport= $SectorObj->EndAirport;
                      //Util::deb($EndAirport,'$EndAirport::');
                      $StartDate= (string)$SectorObj->StartDate;
                      $StartTime= (string)$SectorObj->StartTime;
                      $EndDate= (string)$SectorObj->EndDate;
                      $EndTime= (string)$SectorObj->EndTime;
                      $NumStops= $NumStopsCount;
                      $Company= (string)$SectorObj->AirVendor->Name;
                    }
                    //die("TTTTT");
                  } //  foreach( $ResultSectorObj->Sector as $SectorObj ) {
                }


                $IsAirAvailReturn= true;

              } // foreach( $AirAvailObj->ResultSector as $ResultSectorObj ) {

            } // foreach( $FareItemObj->AirAvail as $AirAvailObj ) {



            foreach( $FareItemObj->Quotes as $QuotesObj ) {
              //Util::deb($QuotesObj,'$QuotesObj::');

              $IsQuoteObjReturn= 1; $Price=0;  $PricesArray = array('adults' => 0 , 'children' => 0 , 'babies' => 0);
              foreach( $QuotesObj->Quote as $QuoteObj ) {
                //Util::deb($QuoteObj,'$QuoteObj::');

                foreach( $QuoteObj->QuoteDetails as $QuoteDetailsObj ) {
                  //Util::deb($QuoteDetailsObj,'$QuoteDetailsObj::');
                  //Util::deb($IsQuoteObjReturn,'$IsQuoteObjReturn::');
                  $Price+= floatval($QuoteDetailsObj->Total);
                  $ItemPrice= floatval($QuoteDetailsObj->Total);
                  //Util::deb($Price,'Price::');
                  $Currency= (string)$QuoteDetailsObj['Currency'];
                  //Util::deb($Currency,'Currency::');
                } // foreach( $QuoteObj->QuoteDetails as $QuoteDetailsObj ) {
                //$IsQuoteObjReturn= true;

                // $adults,  $childs,  $infants
                if ($IsQuoteObjReturn == 1) { // 'adults'
                  $PricesArray['adults'] = $ItemPrice;
                }
                if ($IsQuoteObjReturn == 2) { // 'children'
                  $PricesArray['children'] = $ItemPrice;
                }
                if ($IsQuoteObjReturn == 3) { //'babies'
                  $PricesArray['babies'] = $ItemPrice;
                }
                if ($IsQuoteObjReturn == 3) { // 'babies'
                  $IsQuoteObjReturn = 0;
                }
                if ($IsQuoteObjReturn == 2) { // 'children'
                  $IsQuoteObjReturn = 3;
                }
                if ($IsQuoteObjReturn == 1) { // 'adults'
                  $IsQuoteObjReturn = 2;
                }

              } // foreach( $QuotesObj->Quote as $QuoteObj ) {


            } // foreach( $FareItemObj->Quotes as $QuotesObj ) {

            $PriceSumma=0;
            if ( !empty($PricesArray['adults']) ) $PriceSumma+= $PricesArray['adults']*$adults;
            if ( !empty($PricesArray['children']) ) $PriceSumma+= $PricesArray['children']*$childs;
            if ( !empty($PricesArray['babies']) ) $PriceSumma+= $PricesArray['babies']*$infants;
            //Util::deb($StartAirport,'$StartAirport::');
            //Util::deb($EndAirport,'$EndAirport::');
            if ( !empty($StartAirport) and !empty($EndAirport) and !empty($Return_StartAirport) and !empty($Return_EndAirport) ) {
              //Util::deb(Util::strToDate($Return_StartDate,"YMDSlashes"),'Util::strToDate($Return_StartDate,"YMDSlashes")::');
              //Util::deb(Util::strToDate($Return_EndDate,"YMDSlashes"),'Util::strToDate($Return_EndDate,"YMDSlashes")::');
              $ResArray[]= array(
              'Service' => 'Netviagens',
              'Price' => $PriceSumma,
              'PricesArray' => $PricesArray,
              'depCity' => (string)$StartAirport->Code,
              'arrCity' => (string)$EndAirport->Code,
              'depDate_string' => $StartDate,
              'depDate' => Util::strToDate($StartDate,"YMDSlashes"),
              'depHour' => $StartTime,
              'arrDate_string' => $EndDate,
              'arrDate' => Util::strToDate($EndDate,"YMDSlashes"),
              'arrHour' => $EndTime,
              'carrier' => '',
              'stops' => $NumStops,
              'company' => $Company,
              'currency' => $Currency,

              'return_Price' => $Return_Price,
              'return_depCity' => (string)$Return_StartAirport->Code,
              'return_arrCity' => (string)$Return_EndAirport->Code,
              'return_depDate_string' => $Return_StartDate,
              'return_depDate' => Util::strToDate($Return_StartDate,"YMDSlashes"),
              'return_depHour' => $Return_StartTime,
              'return_arrDate_string' => $Return_EndDate,
              'return_arrDate' => Util::strToDate($Return_EndDate,"YMDSlashes"),
              'return_arrHour' => $Return_EndTime,
              'return_carrier' => '',
              'return_stops' => $Return_NumStops,
              'return_company' => $Return_Company,
              'return_currency' => $Return_Currency,
              );
            }
            //Util::deb( $ResArray, '$ResArray::' );
            //die("-11 ResArray::");



          } // foreach( $ResultObj->FareItem as $FareItemObj ) {
        } // foreach( $AirFaresObj->Result as $ResultObj ) {
      } // foreach( $xml->AirFares as $AirFaresObj ) {
      //Util::deb( $ResArray, '$ResArray::' );
      //die("ResArray::");
      return $ResArray;

    } // try {

    catch ( Exception $lException ) {
    }

  }

  //////////////////// Netviagens END ///////////////////////



}



