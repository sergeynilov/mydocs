<?php use_helper('Javascript') ?>
<?php use_helper('Javascript') ?>
<?php use_helper('Form') ?>
<?php use_helper('jQuery') ?>

  <script type="text/javascript" language="JavaScript">
  <!--
  var IsAdd=false
  var CurrentAdminLangMessageKey=''


  function AddItem() {
    document.getElementById("tr_edit").style.display = GetShowTRMethod();
    var FirstFiedlName=''
    <?php foreach( $langs_in_system_array as $CurrentCulture=>$LangName ) : ?>
    if( Trim(FirstFiedlName=='') ) FirstFiedlName= "key_edit_<?php echo $CurrentCulture ?>"
    document.getElementById("key_edit_<?php echo $CurrentCulture ?>").value='';
    <?php endforeach ?>
    if(document.getElementById(FirstFiedlName)) document.getElementById(FirstFiedlName).focus()
    document.getElementById("span_edit_key").innerHTML = "";
    document.getElementById("span_edit_KeyName").style.display = "inline";
    IsAdd= true


  }

  function onCancel() {
    <?php foreach( $langs_in_system_array as $CurrentCulture=>$LangName ) : ?>
    document.getElementById("key_edit_<?php echo $CurrentCulture ?>").value= '';
    <?php endforeach ?>
    document.getElementById("span_edit_KeyName").value = "";
    CurrentAdminLangMessageKey=''
    document.getElementById("tr_edit").style.display = 'none';
    IsAdd= true
  }

  function onSave() {
    var ResStr='';
    var AllItemsFilled= true;
    var FirstFiedlName=''
    <?php foreach( $langs_in_system_array as $CurrentCulture=>$LangName ) : ?>
    if( Trim(FirstFiedlName=='') ) FirstFiedlName= "key_edit_<?php echo $CurrentCulture ?>"
    var Value= document.getElementById( "key_edit_<?php echo $CurrentCulture ?>" ).value;
    if ( Trim(Value)=="" ) AllItemsFilled= false;
    ResStr= ResStr+'lang_<?php echo $CurrentCulture ?>='+Value.replace ( /\./g, "::dot::" ).replace ( /\//g, "::slashed::" ).replace ( /&/g, "" )+';'
    <?php endforeach ?>
    if ( IsAdd ) {
      var Val= document.getElementById("span_edit_KeyName").value
      if ( Trim(Val)=="" ) {
        alert( "Enter key !" )
        document.getElementById("span_edit_KeyName").focus()
        return;
      }
    }
    if ( !AllItemsFilled ) {
      alert("Enter all values !")
      if(document.getElementById(FirstFiedlName)) document.getElementById(FirstFiedlName).focus()
      return;
    }

    if ( IsAdd ) {
      CurrentAdminLangMessageKey= document.getElementById("span_edit_KeyName").value
    }
    var HRef= '<?php echo Util::getServerHost( $this->context->getConfiguration(),true) ?>admin/UpdateDictionaryItem/key/'+encodeURIComponent(CurrentAdminLangMessageKey)+'/values/'+encodeURIComponent(ResStr);
    //alert(HRef)
    $.post(HRef,
    {
    },
    onDictionaryItemUpdated,
    function(x,y,z) {   // Some sort of error
      alert(x.responseText);
    }
    );

  }

  function onDictionaryItemUpdated(data) {
    //alert( "onDictionaryItemUpdated  IsAdd::"+IsAdd+":   "+var_dump(data) )
    if ( Trim(data)=="" ) {
      if ( IsAdd ) {
        var theForm = document.getElementById("form_edit");
        theForm.submit();
        alert("Saved!")
        return;
      }
      <?php foreach( $langs_in_system_array as $CurrentCulture=>$LangName ) : ?>
        var Value= document.getElementById("key_edit_<?php echo $CurrentCulture ?>").value;
        document.getElementById("key_" + CurrentAdminLangMessageKey + "_<?php echo $CurrentCulture ?>" ).value= Value;
        document.getElementById("key_edit_<?php echo $CurrentCulture ?>").value= '';
      <?php endforeach ?>
      document.getElementById("tr_edit").style.display = 'none';
      document.getElementById("span_edit_KeyName").value = "";
      CurrentAdminLangMessageKey= '';
      IsAdd= false;
      alert("Saved!")
    }
  }

  function DeleteItem( AdminLangMessageKey ) {
    if ( !confirm("Delete <"+AdminLangMessageKey+"> item ? ") ) return;
    CurrentAdminLangMessageKey= AdminLangMessageKey
    var HRef= '<?php echo Util::getServerHost( $this->context->getConfiguration(),true) ?>admin/DeleteDictionaryItem/key/'+CurrentAdminLangMessageKey;
    //alert(HRef)
    $.get(HRef,
    {
    },
    onDictionaryItemDeleted,
    function(x,y,z) {   //Some sort of error
      alert(x.responseText);
    }
    );
  }

  function onDictionaryItemDeleted(data) {
    //alert("onDictionaryItemDeleted data::"+data)
    if (Trim(data)=="") {
      document.getElementById("tr_"+CurrentAdminLangMessageKey).style.display = 'none';
      CurrentAdminLangMessageKey=''
    }
    alert("Deleted!")
    IsAdd= false;
  }

  function EditItem( AdminLangMessageKey ) {
    IsAdd= false;
    document.getElementById("tr_edit").style.display = GetShowTRMethod();
    document.getElementById("span_edit_key").innerHTML = AdminLangMessageKey;
    document.getElementById("span_edit_KeyName").value = "";
    document.getElementById("span_edit_KeyName").style.display = "none";
    var FirstFiedlName=''

    <?php foreach( $langs_in_system_array as $CurrentCulture=>$LangName ) : ?>
    if( Trim(FirstFiedlName=='') ) FirstFiedlName= "key_edit_<?php echo $CurrentCulture ?>"
    var Value= document.getElementById("key_" + AdminLangMessageKey + "_<?php echo $CurrentCulture ?>" ).value;
    //alert(Value)
    document.getElementById("key_edit_<?php echo $CurrentCulture ?>").value= Value;
    <?php endforeach ?>
    if(document.getElementById(FirstFiedlName)) document.getElementById(FirstFiedlName).focus()
    CurrentAdminLangMessageKey= AdminLangMessageKey
  }
  window.onload = InitPage;
  function InitPage() {
  }

  //-->
  </script>

  
<form id="form_edit" method="POST">
  
<?php if ( !empty($AdminLangMessageItemsArray) ) : ?>

  <table width="100%" border="0">


      <tr>
        <td colspan="20">
          <a href="#" onclick="javascript:AddItem()">Add</a>&nbsp;
        </td>
      </tr>

      <tr>
        <td>
          &nbsp;
        </td>
        <?php foreach( $langs_in_system_array as $CurrentCulture=>$LangName ) : ?>
        <td>
          <?php echo $LangName['name'] ?>
        </td>
        <?php endforeach ?>
        <td>
          &nbsp;&nbsp;
        </td>
      </tr>


      <tr id="tr_edit" style="display:none">
        <td>
          <b><i><span id="span_edit_key"></span></i></b>
          <?php echo input_tag( "span_edit_KeyName", '',array('size'=>30) ) ?>
        </td>
        <?php foreach( $langs_in_system_array as $CurrentCulture=>$LangName ) : ?>
        <td>
          <?php echo input_tag( "key_edit_".$CurrentCulture, '',array('size'=>30) ) ?><?php echo image_tag( Util::getImageSrcByCulture('select_country_'.$CurrentCulture,sfContext::getInstance()), array( 'alt'=>$LangName['name'], 'width'=>16, 'height'=>16 ) ) ?>
        </td>
        <?php endforeach ?>
        <td>
          <?php echo image_tag( Util::getImageSrcByCulture("save",sfContext::getInstance()), array( 'alt'=>__("Save"), 'onclick'=>'javascript:onSave()' ) ) ?>&nbsp;<?php echo image_tag( Util::getImageSrcByCulture("cancel",sfContext::getInstance()), array( 'alt'=>__("cancel"), 'onclick'=>'javascript:onCancel()') ) ?>
        </td>
        
        
      </tr>
      
      
    <?php foreach( $AdminLangMessageItemsArray as $AdminLangMessageKey => $AdminLangMessageItem ) : ?>
      <?php if ( !( strpos($AdminLangMessageKey,'admin_')===false) or !( strpos($AdminLangMessageKey,'sfeleadmini18n_')===false) ) continue; ?>
      <tr id="tr_<?php echo $AdminLangMessageKey ?>">
        <td>
          <?php /*echo $AdminLangMessageItem[1]*/ ?><i><?php echo $AdminLangMessageKey?></i>
        </td>

        
        <?php foreach( $langs_in_system_array as $CurrentCulture=>$LangName ) : ?>
        <td>
          <?php echo input_tag( "key_".$AdminLangMessageKey.'_'.$CurrentCulture, getValueByKeyAndLang($MessageItemsArray,$AdminLangMessageKey,$CurrentCulture),array('size'=>30, 'readonly'=>'readonly') ) ?><?php echo image_tag( Util::getImageSrcByCulture('select_country_'.$CurrentCulture,sfContext::getInstance()), array( 'alt'=>$LangName['name'], 'width'=>16, 'height'=>16 ) ) ?>
        </td>
        <?php endforeach ?>
        
        <td>
          <a href="#" onclick="javascript:EditItem('<?php echo $AdminLangMessageKey?>')">Edit</a>&nbsp;&nbsp;<a href="#" onclick="javascript:DeleteItem('<?php echo $AdminLangMessageKey?>')">Delete</a>
        </td>
        
      </tr>
    <?php endforeach; ?>


    
    
  </table>
<?php else:  ?>
  <b> No Messages </b>
<?php endif; ?>

<?php 

function getValueByKeyAndLang($MessageItemsArray,$Key,$CurrentCulture) {
  foreach( $MessageItemsArray as $KeyLang=>$MessageArray ) {
    if ( $KeyLang==$CurrentCulture ) {
      foreach( $MessageArray as $MessageKey=>$MessageItem ) {
        if ( $MessageKey == $Key ) {
          return $MessageItem[0];
        }
      }
    }
  }
  return '';
}

?>
</form>