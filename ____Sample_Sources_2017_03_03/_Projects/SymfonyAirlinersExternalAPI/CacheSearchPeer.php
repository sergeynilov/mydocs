<?php

/**
 * Subclass for performing query and update operations on the 'cache_search' table.
 *
 *
 *
 * @package lib.model
 */

class CacheSearchPeer extends BaseCacheSearchPeer
{
  public static $DebugLimit= 3;
  public static $SearchesCount= 0;
  public static $SearchesResultCount= 0;
  public static $UsedCitiesArray= array();
  public static $CitiesPairsArray= array();
  public static $ErrorsArray= array();
  public static $ResultsArrayByCities= array();
  private static function isInUsedCities( $DepartureCityId, $ArrivalCityId ) {
    return false;
    foreach( CacheSearchPeer::$UsedCitiesArray as $UsedCity ) {
      if ( $UsedCity['departure_id']== $DepartureCityId and $UsedCity['arrival_id']== $ArrivalCityId ) {
        return true;
      }
    }
    return false;
  }

  public static function RunCacheCron () {
    SearchSessionPeer::setSessionValue(false);
    CacheSearchPeer::$SearchesCount= 0;
    CacheSearchPeer::$SearchesResultCount= 0;
    $TimeStart= time();
    $langs_in_system_array= sfConfig::get('app_langs_in_system_array' );
    foreach( $langs_in_system_array as $key=>$lang ) {
      if ( $key=='pt_BR' or $key=='en_GB' ) continue; // must be uncommented FIXIT
      if ( Util::isDeveloperComp() and $key=='es_ES' ) continue;
      CacheSearchPeer::RunCacheCronByCulture($key);
    }

    $TimeEnd= time();
    $TimeLong= $TimeEnd-$TimeStart;
    $TimeStr= '  Started::'.strftime('%Y-%m-%d %H:%M:%S',$TimeStart).'  Ended::'.strftime('%Y-%m-%d %H:%M:%S',$TimeEnd).'<br>' . '  Time Long::'.strftime( '%H:%M:%S',$TimeLong )."\r\n";
    $CountsStr= '  SearchesCount::'.CacheSearchPeer::$SearchesCount.
    '  SearchesResultCount::'. CacheSearchPeer::$SearchesResultCount."\r\n";

    $Msg= $CountsStr.$TimeStr;
    $admin_email= sfConfig::get('app_notification_admin_email' );
    $I=1; $ErrorsStr= '';
    foreach( CacheSearchPeer::$ErrorsArray as $ErrorArr ) {
      $ErrorsStr.= '#'.$I.': '.$ErrorArr;
    }
    $I=1; $CitiesPairStr= '';
    foreach( CacheSearchPeer::$CitiesPairsArray as $CitiesPair ) {
      $CitiesPairStr.= '#'.$I.': '.$CitiesPair['DepartureCityId'].'.'.$CitiesPair['DepartureCityName'] .' -> '.
      $CitiesPair['ArrivalCityId'].'.'.$CitiesPair['ArrivalCityName'];
    }
    $I=1;
    foreach( CacheSearchPeer::$ResultsArrayByCities as $ResultArray ) {
      $Msg.= '#'.$I.': '.$ResultArray['service'] . ' Adult:'.$ResultArray['Adult']. ' Adult:'.$ResultArray['oneWay']
      . ' NextDepartureDay:'.$ResultArray['NextDepartureDay']. ' NextReturnDay:'.$ResultArray['NextReturnDay'].' '.
      $ResultArray['DepartureCityId'].'.' .$ResultArray['DepartureAirportId']. ' '.
      $ResultArray['DepartureCityName'].'.' .$ResultArray['DepartureAirportName'].' -> '.
      $ResultArray['ArrivalCityId'].'.' .$ResultArray['ArrivalAirportId']. ' '.
      $ResultArray['ArrivalCityName'].'.' .$ResultArray['ArrivalAirportName']. ' RowsAdded:'.
      $ResultArray['RowsAdded']. "\r\n";
      $I++;
    }
    SearchSessionPeer::setSessionValue(true, print_r( CacheSearchPeer::$ResultsArrayByCities, true )."\r\n\r\n\r\n".$CitiesPairStr."\r\n\r\n\r\n\r\n".$Msg."\r\n\r\n\r\n\r\n".$ErrorsStr);
    Util::SendEmail( $admin_email, sfConfig::get('app_notification_email'),  "Caching results", $Msg );
  }

  public static function RunCacheCronByCulture ($Culture) {
    $cache_cron_parameters= sfConfig::get('app_cache_cron_parameters_array' );
    $number_top_departure_cities= (int)sfConfig::get('app_application_number_top_departure_cities' );
    $number_top_arrival_cities= (int)sfConfig::get('app_application_number_top_arrival_cities' );

    $DeparturesCitiesList= CityInTopPeer::getCitiesInTopList("From", 0, $Culture );
    $ArrivalCitiesList= CityInTopPeer::getCitiesInTopList("To", 0, $Culture );

    $DepartureDates= (int)$cache_cron_parameters['DepartureDates'];
    $ReturnDates= (int)$cache_cron_parameters['ReturnDates'];
    $AdultsArray= $cache_cron_parameters['Adults'];
    $ChildrenArray= $cache_cron_parameters['Children'];
    $BabiesArray= $cache_cron_parameters['Babies'];
    $ClassArray= $cache_cron_parameters['Class'];
    $DirectFlightsOnlyArray= $cache_cron_parameters['DirectFlightsOnly'];
    $Now= time();
    foreach( $DeparturesCitiesList as $DepartureCity ) {
      reset($ArrivalCitiesList);
      foreach( $ArrivalCitiesList as $ArrivalCity ) {

        CacheSearchPeer::$CitiesPairsArray[]= array(
        'DepartureCityName'=>$DepartureCity->getCity()->getName(),
        'DepartureCityId'=>$DepartureCity->getCity()->getId(),
        'ArrivalCityName'=>$ArrivalCity->getCity()->getName(),
        'ArrivalCityId'=>$ArrivalCity->getCity()->getId()  );

        CacheSearchPeer::$UsedCitiesArray[]=array( 'departure_id'=>$DepartureCity->getCityId(), 'arrival_id'=>$ArrivalCity->getCityId() );

        for( $DepartureDay=1; $DepartureDay <= $DepartureDates; $DepartureDay++ ) {
          $NextDepartureDay= Util::UnixDateAddDay($Now,$DepartureDay);
          $NextDepartureDayString= strftime('%d%m%Y',$NextDepartureDay);
          for( $ReturnDay=1; $ReturnDay <= $ReturnDates; $ReturnDay++ ) {
            $NextReturnDay= Util::UnixDateAddDay($Now,$ReturnDay);
            $NextReturnDayString= strftime('%d%m%Y',$NextReturnDay);

            foreach( $AdultsArray as $Adult ) {
              foreach( $ChildrenArray as $Child ) {
                foreach( $BabiesArray as $Baby ) {
                  foreach( $ClassArray as $Class ) {
                    foreach( $DirectFlightsOnlyArray as $DirectFlightsOnly ) {
                      $RetStatus='ok';
                      $RetStatus= CacheSearchPeer::RunCacheSearch( $DepartureCity, $ArrivalCity, $NextDepartureDay, $NextDepartureDayString, $NextReturnDay, $NextReturnDayString, $Adult, $Child, $Baby, $Class, $DirectFlightsOnly );
                      if ( $RetStatus== 'ok') {
                        CacheSearchPeer::$SearchesCount++;
                      }
                      //die( "TTTTTT" );
                      if(Util::isDeveloperComp() and CacheSearchPeer::$SearchesCount>= CacheSearchPeer::$DebugLimit ) {
                        return;// for testing on developers site
                      }
                    }
                  }
                }
              } // foreach( $ChildrenArray as $Child ) {
            } // foreach( $AdultsArray as $Adult ) {
          } // for( $Return=0; $ReturnDay < $ReturnDates; $ReturnDay++ ) {
        } // for( $DepartureDay=0; $DepartureDay < $DepartureDates; $DepartureDay++ ) {
      }// foreach( $DeparturesCitiesList as $DepartureCity ) {
    } // foreach( $DeparturesCitiesList as $DepartureCity ) {

  }

  public static function RunCacheSearch( $DepartureCity, $ArrivalCity, $NextDepartureDay, $NextDepartureDayString, $NextReturnDay, $NextReturnDayString, $Adult, $Child, $Baby, $Class, $oneWay ) {
    try
    {
      if(Util::isDeveloperComp() and CacheSearchPeer::$SearchesCount>= CacheSearchPeer::$DebugLimit ) {
        return;// for testing on developers site
      }
      $depTime= '0000';
      $retTime= '0000';
      $idxFlght= 0;
      $flights_only= false;
      $mktportal= 'tradedoubler-'.strtoupper(Util::getCultureCountry( sfContext::getInstance() ));
      $utm_source= 'tradedoubler-'.strtoupper(Util::getCultureCountry( sfContext::getInstance() ));
      $utm_medium= 'affiliates';
      $APIResArray= array();
      $EDreamsResArray= array();
      $CurrentCulture= Util::getCultureCountry(sfContext::getInstance());
      $lng= Util::getCultureCountry(sfContext::getInstance());
      $ctry= strtoupper(Util::getCultureCountry(sfContext::getInstance()));
      $lDepartureAirport= $DepartureCity->getCity()->getMainAirport();
      $lArrivalAirport= $ArrivalCity->getCity()->getMainAirport();

      if ( !$lDepartureAirport or !$lArrivalAirport ) {
        return 'skipped';
      }
      $ResArray= array();
      $CurrentHour= (int)strftime('%H',time());

      $TerminalA_RunCacheHour= (int)sfConfig::get('app_TerminalA_RunCacheHour' );
      $terminala= $CurrentHour== $TerminalA_RunCacheHour;

      $Netviagens_RunCacheHour= (int)sfConfig::get('app_Netviagens_RunCacheHour' );
      $Netviagens= $CurrentHour== $Netviagens_RunCacheHour;


      $Barcelo_RunCacheHour= (int)sfConfig::get('app_Barcelo_RunCacheHour' );
      $Barcelo= $CurrentHour== $Barcelo_RunCacheHour;

      $Spanair_RunCacheHour= (int)sfConfig::get('app_Spanair_RunCacheHour' );
      $Spanair= $CurrentHour== $Spanair_RunCacheHour;

      $LFE_RunCacheHour= (int)sfConfig::get('app_LFE_Flight_RunCacheHour' );
      $LFE= true;//$CurrentHour== $LFE_RunCacheHour;

      $edreams= false;//$request->getParameter('edreams');


      if ( $terminala ) {
        $depDate= $NextDepartureDayString;
        $retDate= $NextReturnDayString;
        if ( !$oneWay and empty($retDate) ) $oneWay= true;
        $CachedDataId= CacheSearchPeer::HasCachedData( true, 'TerminalA', $lDepartureAirport->getId(), $lArrivalAirport->getId(),  $NextDepartureDay,  $Adult,  $Child,  $Baby, $oneWay, $NextReturnDay, ConfigurationPeer::setSelectedClass( $Class, 'TerminalA' ) );
        if ( !$CachedDataId ) {
          $DataArr = RetrievalApiPeer::getTerminalAXMLData( $lDepartureAirport->getCode(), $lArrivalAirport->getCode(), $depDate,  $depTime,  $Adult,  $Child,  $Baby,  $oneWay, $retDate,  $retTime, $idxFlght,  $mktportal, $utm_source, $utm_medium,  $lng,  $ctry, ConfigurationPeer::setSelectedClass( $Class, 'TerminalA' ) );
          foreach( $DataArr as $DataLiner ) {
            $APIResArray[]= $DataLiner;
          }
          CacheSearchPeer::UpdateCachedData( 'TerminalA', $lDepartureAirport->getId(), $lArrivalAirport->getId(), $NextDepartureDay, $Adult,  $Child,  $Baby,  $oneWay, $NextReturnDay, ConfigurationPeer::setSelectedClass($Class, 'TerminalA'), $APIResArray );
        }
        CacheSearchPeer::$SearchesResultCount+= count($APIResArray);
        CacheSearchPeer::$ResultsArrayByCities[]= CacheSearchPeer::AddResultsArrayByCities( 'TerminalA', $Adult, $oneWay, strftime('%Y-%m-%d',$NextDepartureDay), strftime('%Y-%m-%d',$NextReturnDay), $lDepartureAirport, $lArrivalAirport, count($APIResArray) );
      } // if ( $terminala ) {


...

      if ( $LFE ) {
        $depDate= $NextDepartureDayString;
        $retDate= $NextReturnDayString;
        if ( !$oneWay and empty($retDate) ) $oneWay= true;
        $CachedDataId= CacheSearchPeer::HasCachedData( true, 'LFE', $lDepartureAirport->getId(), $lArrivalAirport->getId(),  $NextDepartureDay,  $Adult,  $Child,  $Baby, $oneWay, $NextReturnDay, ConfigurationPeer::setSelectedClass( $Class, 'LFE' ) );
        //die("YTRR");
        if ( !$CachedDataId ) {
          $DataArr = RetrievalApiPeer::getLFEXMLData( $lDepartureAirport->getCode(), $lArrivalAirport->getCode(), Util::StrToDate($depDate,'','DMYSlashed'),  $depTime,  $Adult,  $Child,  $Baby,  $oneWay, Util::StrToDate($retDate,'','DMYSlashed'),  $retTime, $idxFlght,  $mktportal, $utm_source, $utm_medium,  $lng,  $ctry, ConfigurationPeer::setSelectedClass( $Class, 'LFE' ) );
          foreach( $DataArr as $DataLiner ) {
            $APIResArray[]= $DataLiner;
          }
          CacheSearchPeer::UpdateCachedData( 'LFE', $lDepartureAirport->getId(), $lArrivalAirport->getId(), $NextDepartureDay, $Adult,  $Child,  $Baby,  $oneWay, $NextReturnDay, ConfigurationPeer::setSelectedClass($Class, 'LFE'), $APIResArray );
        }
        CacheSearchPeer::$SearchesResultCount+= count($APIResArray);
        CacheSearchPeer::$ResultsArrayByCities[]= CacheSearchPeer::AddResultsArrayByCities( 'LFE', $Adult, $oneWay, strftime('%Y-%m-%d',$NextDepartureDay), strftime('%Y-%m-%d',$NextReturnDay), $lDepartureAirport, $lArrivalAirport, count($APIResArray) );

      } // if ( $LFE ) {
      return 'ok';
    }
    catch (Exception $lException)
    {
      CacheSearchPeer::$ErrorsArray[]=  ( $terminala ?'TerminalA':'').' ' . ( $Netviagens ?'Netviagens':'').' ' .
      ( $Barcelo ?'Barcelo':'').' ' . ( $Spanair ?'Spanair':'').' ' . ( $LFE ?'LFE':'').' ' . ( $edreams ?'edreams':'').' '.
      $DepartureCity->getCity()->getId().'.'.$DepartureCity->getCity()->getName().' -> '.
      $ArrivalCity->getCity()->getId().'.'.$ArrivalCity->getCity()->getName().',  '.
      $NextDepartureDayString.' : '.$NextReturnDayString. '  ' . $lException->getMessage();
      return 'error';
    }


  }
  public static function AddResultsArrayByCities( $Service, $Adult, $oneWay, $NextDepartureDay, $NextReturnDay, $lDepartureAirport, $lArrivalAirport, $RowsCount ) {
    $Res= array( 'service'=>$Service, 'Adult'=> $Adult, 'oneWay'=>$oneWay, 'NextDepartureDay'=>$NextDepartureDay,
    'NextReturnDay'=>$NextReturnDay, 'DepartureCityName'=>$lDepartureAirport->getCity()->getName(), 'DepartureAirportName'=>$lDepartureAirport->getName(), 'DepartureCityId'=>$lDepartureAirport->getCity()->getId(), 'DepartureAirportId'=>$lDepartureAirport->getId(),
    'ArrivalCityName'=>$lArrivalAirport->getCity()->getName(), 'ArrivalAirportName'=>$lArrivalAirport->getName(),
    'ArrivalCityId'=>$lArrivalAirport->getCity()->getId(), 'ArrivalAirportId'=>$lArrivalAirport->getId(),
    'RowsAdded'=>$RowsCount
    );
    return $Res;
  }

  public static function ClearCache()
  {
    $c = new Criteria();
    $c->addAscendingOrderByColumn( CacheSearchPeer::ID );
    $CacheSearchList= CacheSearchPeer::doSelect($c);
    $ClearedCount= 0;
    $ErrorCount= 0;
    foreach( $CacheSearchList as $CacheSearch ) {
      try {
        foreach($CacheSearch->getCacheSearchResults() as $CacheSearchResult) {
          $CacheSearchResult->delete();
        }
        $CacheSearch->delete();
        $ClearedCount++;
      }
      catch (Exception $lException) {
        $ErrorCount++;
      }
    }
    return array('ClearedCount'=>$ClearedCount, 'ErrorCount'=>$ErrorCount );
  }


  public static function HasCachedData( $CampareLastRenewed, $Service, $DepartureAirportId, $ArrivalAirportId, $depDate, $adults, $childs, $infants, $oneWay, $returnDate, $classtype, $param1='', $param2=''  ) {
    $c = new Criteria();
    $c->add( CacheSearchPeer::SERVICE, $Service );
    $c->add( CacheSearchPeer::DEP_AIRPORT_ID, $DepartureAirportId );
    $c->add( CacheSearchPeer::ARR_AIRPORT_ID, $ArrivalAirportId );
    $c->add( CacheSearchPeer::DEP_DATE, $depDate );
    $c->add( CacheSearchPeer::ADULTS, $adults );
    $c->add( CacheSearchPeer::CHILDS , $childs );
    $c->add( CacheSearchPeer::INFANTS , $infants );
    $c->add( CacheSearchPeer::ONEWAY , $oneWay );
    $c->add( CacheSearchPeer::RETURN_DATE , $returnDate );
    $c->add( CacheSearchPeer::CLASS_TYPE, $classtype );
    if ( $CampareLastRenewed ) {
      $cache_working_days= (int)ConfigurationPeer::GetConfigurationValue( 'cache_working_days', '2' );
      $CompareDate= Util::UnixDateTimeAddDay(time(),-$cache_working_days);
      $c->add( CacheSearchPeer::LAST_RENEWED, $CompareDate, Criteria::GREATER_THAN );
    }
    $lResult = CacheSearchPeer::doSelectOne( $c);
    return empty($lResult)?false:$lResult->getId();
  }

  public static function UpdateCachedData( $Service, $DepartureAirportId, $ArrivalAirportId, $depDate, $adults, $childs, $infants, $oneWay, $returnDate, $classtype, $APIResArray ) {
    if ( !Util::isDeveloperComp() ) {
      $CurrentLocale= setlocale( LC_ALL, '' );
      setlocale( LC_ALL, 'en_US.UTF-8' );
    }

    $CachedDataId= CacheSearchPeer::HasCachedData( false, $Service, $DepartureAirportId, $ArrivalAirportId, $depDate, $adults, $childs, $infants, $oneWay, $returnDate, $classtype );

    $lCacheSearch = CacheSearchPeer::getCacheSearchById($CachedDataId);
    if ( empty($lCacheSearch) ) {
      $lCacheSearch = new CacheSearch();
      $lCacheSearch->setService($Service);
      $lCacheSearch->setDepAirportId($DepartureAirportId);
      $lCacheSearch->setArrAirportId($ArrivalAirportId);
      $lCacheSearch->setDepDate($depDate);
      $lCacheSearch->setAdults($adults);
      $lCacheSearch->setChilds($childs);
      $lCacheSearch->setInfants($infants);
      $lCacheSearch->setOneWay($oneWay);
      $lCacheSearch->setReturnDate($returnDate);
      $lCacheSearch->setClassType($classtype);

      if (!empty($APIResArray[0]['param1'])) {
        $lCacheSearch->setParam1($APIResArray[0]['param1']);
      }
      if (!empty($APIResArray[0]['param2'])) {
        $lCacheSearch->setParam2($APIResArray[0]['param2']);
      }

    } else {
      foreach( $lCacheSearch->getCacheSearchResults() as $CacheSearchResult ) {
        $CacheSearchResult->delete();
      }
    }
    $lCacheSearch->setLastRenewed(time());
    $lCacheSearch->save();

    foreach( $APIResArray as $APIStruct ) {
      $lCacheSearchResult= new CacheSearchResult();
      $lCacheSearchResult->setCacheSearchId( $lCacheSearch->getId() );
      $lCacheSearchResult->setService( $APIStruct['Service'] );
      $lCacheSearchResult->setPrice( $APIStruct['Price'] );
      $lCacheSearchResult->setDepDateTime( Util::ToDateAddTime( $APIStruct['depDate'], $APIStruct['depHour'] ) ); // 1250719200  20:55
      $lCacheSearchResult->setArrDateTime( Util::ToDateAddTime( $APIStruct['arrDate'], $APIStruct['arrHour'] ) );
      $lCacheSearchResult->setCompany( trim($APIStruct['company']) );
      $lCacheSearchResult->setStops( $APIStruct['stops'] );
      $lCacheSearchResult->setCurrency( $APIStruct['currency'] );
      $lCacheSearchResult->setReturnDepDateTime( Util::ToDateAddTime( $APIStruct['return_depDate'], $APIStruct['return_depHour'] ) );
      $lCacheSearchResult->setReturnArrDateTime( Util::ToDateAddTime( $APIStruct['return_arrDate'], $APIStruct['return_arrHour'] ) );
      $lCacheSearchResult->setReturnCompany( $APIStruct['return_company'] );
      $lCacheSearchResult->setReturnStops( $APIStruct['return_stops'] );
      $lCacheSearchResult->setDeeplink( !empty($APIStruct['deeplink'])?$APIStruct['deeplink']:'' );
      $lCacheSearchResult->save();
    }


    if ( !Util::isDeveloperComp() ) {
      setlocale(LC_ALL, $CurrentLocale);
    }
  }

  public static function getCachedData( $CachedDataId ) {
    $lCacheSearch= CacheSearchPeer::getCacheSearchById($CachedDataId);
    $ResArray= array();
    if ( empty($lCacheSearch) ) return $ResArray;
    $lDepAirport= AirportPeer::getAirportById( $lCacheSearch->getDepAirportId() );
    $lArrAirport= AirportPeer::getAirportById( $lCacheSearch->getArrAirportId() );

    $c = new Criteria();
    $c->add( CacheSearchResultPeer::CACHE_SEARCH_ID, $CachedDataId );
    $c->addAscendingOrderByColumn( CacheSearchResultPeer::PRICE );
    $c->addAscendingOrderByColumn( CacheSearchResultPeer::DEP_DATETIME );
    $c->addAscendingOrderByColumn( CacheSearchResultPeer::RETURN_DEP_DATETIME );
    $A= CacheSearchResultPeer::doSelect($c);
    foreach( $A as $Liner ) {
      $Arr= array(
      'depCity'=>$lDepAirport->getCode(),
      'Service'=> $Liner->getService(),
      'Price'=> $Liner->getPrice(),
      'depDate'=> $Liner->getDepDateTimeAsArray('unix'),
      'depHour'=> $Liner->getDepDateTimeAsArray('time'),
      'arrCity'=> $lArrAirport->getCode(),
      'arrDate'=> $Liner->getArrDateTimeAsArray('unix'),
      'arrHour'=> $Liner->getArrDateTimeAsArray('time'),
      'company'=> $Liner->getCompany(),
      'stops'=> $Liner->getStops(),
      'currency'=> $Liner->getCurrency(),
      'return_depDate'=> $Liner->getReturnDepDateTimeAsArray('unix'),
      'return_depHour'=> $Liner->getReturnDepDateTimeAsArray('time'),
      'return_arrDate'=> $Liner->getReturnArrDateTimeAsArray('unix'),
      'return_arrHour'=> $Liner->getReturnArrDateTimeAsArray('time'),
      'return_company'=> $Liner->getReturnCompany(), 'return_stops'=> $Liner->getReturnStops(),
      'return_Price'=> 0, // TOFIX
      'return_arrCity'=>$lDepAirport->getCode(),
      'return_depCity'=> $lArrAirport->getCode(),
      'return_currency'=> $Liner->getCurrency(),
      'deeplink'=> $Liner->getDeeplink(),
      'param1'=> $lCacheSearch->getParam1(),
      'param2'=> $lCacheSearch->getParam2(),
      );
      $ResArray[]= $Arr;
    }
    return $ResArray;
  }

  public static function getCacheSearchById($pId)
  {
    $c = new Criteria();
    $c->add(CacheSearchPeer::ID, $pId);
    return CacheSearchPeer::doSelectOne($c);
  }

  public static function DeleteCacheSearchBeforeDate() {
    $c = new Criteria();
    $PriorDate= Util::UnixDateTimeAddDay(time(),-1);

    $SelectDate= mktime( 23, 59, 59 ,(int)strftime('%m',$PriorDate), (int)strftime('%d',$PriorDate), (int)strftime('%Y',$PriorDate) );

    $c->add(CacheSearchPeer::DEP_DATE , $SelectDate, Criteria::LESS_EQUAL );

    $CacheSearchList= CacheSearchPeer::doSelect($c);
    $ClearedCount= 0;
    $ErrorCount= 0;
    foreach( $CacheSearchList as $CacheSearch ) {
      try {
        foreach($CacheSearch->getCacheSearchResults() as $CacheSearchResult) {
          $CacheSearchResult->delete();
        }
        $CacheSearch->delete();
        $ClearedCount++;
      }
      catch (Exception $lException) {
        $ErrorCount++;
      }
    }
    return array('ClearedCount'=>$ClearedCount, 'ErrorCount'=>$ErrorCount );

  }

}

function MakePriceCompare($a, $b)
{
  if ($a['Price'] == $b['Price']) {

    if ($a['depDate'] == $b['depDate']) {


      $aHour= $a['depHour'];
      $bHour= $b['depHour'];

      $aHour= str_replace(':','',$aHour);
      $bHour= str_replace(':','',$bHour);

      if ($aHour == $bHour) {
        return 0;
      }
      return ($aHour < $bHour) ? -1 : 1;

    }
    return ($a['depDate'] < $b['depDate']) ? -1 : 1;

  }
  return ($a['Price'] < $b['Price']) ? -1 : 1;
}

/*

*/
