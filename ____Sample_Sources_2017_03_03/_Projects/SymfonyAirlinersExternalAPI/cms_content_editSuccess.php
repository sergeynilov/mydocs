    <td>
<?php use_helper('Form') ?>
  <script type="text/javascript" language="JavaScript">
  <!--
  function onSubmit() {
    var theForm = document.getElementById("form_cms_content_edit");
    theForm.submit();
  }

  //-->
  </script>

<form action="<?php echo url_for('@admin_cms_content_edit?id='.$cms_content_id.'&lang='.$sf_request->getParameter("lang").'&page='.$page.'&filter='.$FilterType.'&sorting='.$Sorting ) ?>" id="form_cms_content_edit" method="POST">
<?php echo input_hidden_tag('id', $sf_request->getParameter("id")) ?>

<div class="whiteBg">
	<div class="clear"> </div>

	<div class="alignCenter">
  	<h2 class="title"><?php echo __(!empty($cms_content_id)?'Edit':'Add').' '.__('CMS Content') ?></h2>
    <span class="error" style="list-style: none;"><?php echo $form->renderGlobalErrors() ?></span>
    
    <?php echo $form[ContentItemI18nPeer::CULTURE]->render() ?> 
    <table class="postEdit">

      <tr>
        <td class="left">
          <?php echo strip_tags( $form[ContentItemPeer::ITEMKEY]->renderLabel() ) ?>&nbsp:
        </td>
        <td class="right">
         	<?php 
         	if ( $sf_request->getParameter("lang") == Util::getDefaultLang() ) {
         	  echo $form[ ContentItemPeer::ITEMKEY ]->render();
         	} else {
         	  echo $ContentItem->getItemKey();
         	  echo '<input type="hidden" id="contentitem_cms_content_item.ITEMKEY" name="contentitem[cms_content_item.ITEMKEY]" value="'.$ContentItem->getItemKey().'">';
         	}
         	?>
  	      <span class="error">
  	        <?php echo ( $sf_request->isMethod('post') ? strip_tags( $form[ContentItemPeer::ITEMKEY]->renderError() )  :"" ) ?>
  	      </span>
        </td>
      </tr>
    
      <tr>
        <td class="left">
          <?php echo strip_tags( $form[ContentItemPeer::TYPE]->renderLabel() ) ?>&nbsp:
        </td>
        <td class="right">
         	<?php 
         	$A= ContentItem::$TypeChoices;
         	foreach( $A as $KeyA=>$ValueA ) {
         	  $A[$KeyA]= __($ValueA);
         	}
         	if ( $sf_request->getParameter("lang") == Util::getDefaultLang() ) {
         	  echo $form[ ContentItemPeer::TYPE ]->render();
         	} else {
         	  echo !empty($A[$ContentItem->getType()])?$A[$ContentItem->getType()]:'';
         	  echo '<input type="hidden" id="contentitem_cms_content_item.TYPE" name="contentitem[cms_content_item.TYPE]" value="'.$ContentItem->getType().'">';
         	}
         	?>
  	      <span class="error">
  	        <?php echo ( $sf_request->isMethod('post') ? strip_tags( $form[ContentItemPeer::TYPE]->renderError() )  :"" ) ?>
  	      </span>
        </td>
      </tr>
      <tr>
        <td class="left"> 
          <?php echo strip_tags( $form[ContentItemI18nPeer::TITLE]->renderLabel() ) ?>&nbsp:
        </td>
        <td class="right">					
          <?php if ( $sf_request->getParameter("lang") != Util::getDefaultLang() ) : ?>
            <?php echo __("Title of main language");?>&nbsp;:&nbsp;<br>
            <b><?php echo $MainLanguageTitle ?></b><br>
          <?php endif; ?>
        	<?php echo $form[ ContentItemI18nPeer::TITLE ]->render() ?> &nbsp;
        	<?php $LangName= Util::getLangNameByKey($sf_request->getParameter("lang")); 
        	echo image_tag( Util::getImageOfLang($sf_request->getParameter("lang")), array('alt'=>__($LangName),'border'=>'none') );
?>
    	    <span class="error">
  	        <?php echo ( $sf_request->isMethod('post') ? strip_tags( $form[ ContentItemI18nPeer::TITLE ]->renderError() )  :"" ) ?>
  	      </span>
        </td>
      </tr>
      <tr>
        <td class="left">
          <?php echo strip_tags( $form[ContentItemI18nPeer::BODY]->renderLabel(    ) ) ?>&nbsp:
        </td>
        <td class="right">						
          <?php if ( $sf_request->getParameter("lang") != Util::getDefaultLang() ) : ?>
            <?php echo __("Text of main language");?>&nbsp;:&nbsp;<br>
            <b><?php echo $MainLanguageText ?></b>
          <?php endif; ?>
        	<?php echo $form[ ContentItemI18nPeer::BODY ]->render(  ) ?>
    	    <span class="error"><?php echo ( $sf_request->isMethod('post') ? strip_tags( $form[ContentItemI18nPeer::BODY]->renderError() )  :"" ) ?></span>
  	      <br> <b> <?php echo __('Site variables') ?>: </b><br>
  	      <?php $Arr= Util::getSiteVariables();
  	      foreach( $Arr as $Key => $Value ) {
  	        $S= $Key.' => '.__($Value).'<br>';
  	      }
  	      echo $S;
          ?>
        </td>
      </tr>
<tr><td colspan=2 align="center">
    
		<p class="hr"></p>
		
        <?php 
        if ( !empty($ContentItem) ) {
          $langs_in_system_array= sfConfig::get('app_langs_in_system_array' );
          foreach( $langs_in_system_array as $key=>$lang ) {
            if ( $key== $sf_request->getParameter("lang") ) continue;

            $HasLangItem= false;
            foreach( $ContentItem->getContentItemI18ns() as $ContentItemI18n ) {
              if ( $ContentItemI18n->getCulture() == $key ) {
                $HasLangItem= true;
                break;
              }
            }
            $Img= image_tag( Util::getImageOfLang($key), array('alt'=>__($lang['name']),'border'=>'none') );
            echo link_to( __(!$HasLangItem?'add':'edit').'&nbsp;'.$Img.'&nbsp;&nbsp;&nbsp;&nbsp;','@admin_cms_content_edit?id='.
            $ContentItem->getId().'&lang='.$key.'&page='.$page.'&filter='.$FilterType.'&sorting='.$Sorting );
          }
        }
        ?>
         
    <p class="hr"></p>
		<?php if( empty($cms_content_id) ) : ?>
      <?php echo image_tag(Util::getImageSrcByCulture( "add", sfContext::getInstance()), array('alt'=>__("Add CMS Content"), 'onclick'=>'javascript:onSubmit()') ) ?>
		<?php else: ?>
      <?php echo image_tag( Util::getImageSrcByCulture("send",sfContext::getInstance()), array('alt'=>__("Save Changes"), 'onclick'=>'javascript:onSubmit()') ) ?>
		<?php endif; ?>
    
		<?php echo link_to( image_tag( Util::getImageSrcByCulture("cancel",sfContext::getInstance()), array('alt'=>__("Cancel"))), '@admin_cms_contents?page='.$page.'&filter='.$FilterType) ?>

        </td>
      </tr>
    </table>        
		
	</div>
</div>


</form>

    </td>