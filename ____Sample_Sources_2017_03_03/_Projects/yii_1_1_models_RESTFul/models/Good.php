<?php

/**
 * This is the model class for table "jsn_good".
 *
 * The followings are the available columns in table 'good':
 * @property integer $id
 * @property string $name
 * @property string $created_at
 */
class Good extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'jsn_good';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'),
			array('name', 'unique',
				'allowEmpty' => false,
				'message'=>'Sorry, this Good Name has already been taken.'), // , 'max'=>50
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name', 'safe', 'on'=>'search'),
		);
	}


	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'created_at' => 'Created At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('created_at',$this->created_at,true);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Good the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function afterFind() {
		$this->price = Yii::app()->format->number($this->price);
		return parent::afterFind();
	}

	public function getGoodsList( $list_output_format, $page = '', $filters = array(), $sort = '', $sort_direction = '' )
	{
		if ( $list_output_format!= Appfuncs::$LIST_OUTPUT_FORMAT_COUNT and $list_output_format!= Appfuncs::$LIST_OUTPUT_FORMAT_DATA_PROVIDER and $list_output_format!= Appfuncs::$LIST_OUTPUT_FORMAT_LIST ) {
			$list_output_format!= Appfuncs::$LIST_OUTPUT_FORMAT_DATA_PROVIDER;
		}
		$pagination_filters= Appfuncs::copy_filters_array( $filters, 'filter_',true);

		if ( empty($sort) ) $sort= 'C.name';
		if ( $list_output_format == Appfuncs::$LIST_OUTPUT_FORMAT_COUNT ) $page= '';
		$criteria=new CDbCriteria;
		$criteria->alias = 'C';
		if (!empty($filters['name'])) {
			$criteria->compare( 'name',  $filters['name'], true );
		}

		if (!empty($filters['name_strict'])) {
			$criteria->compare( 'name',  $filters['name_strict'] );
		}

		if (!empty($filters['goodgroup_id'])) {
			$criteria->compare( 'goodgroup_id',  $filters['goodgroup_id'] );
		}

		if (!empty($filters['good_id'])) {
			$criteria->compare( 'good_id',  $filters['good_id'] );
		}
		if (!empty($filters['client_id'])) {
			$criteria->compare( 'client_id',  $filters['client_id'] );
		}

		if (!empty($filters['limit'])) {
			$criteria->limit = $filters['limit'];
		}

		if (!empty($filters['offset'])) {
			$criteria->offset = $filters['offset'];
		}

		if($list_output_format == Appfuncs::$LIST_OUTPUT_FORMAT_COUNT) {
			return Good::model()->count($criteria);
		} else {
			$criteria->order = $sort . ' ' . $sort_direction;
			$ActiveDataProvider= new CActiveDataProvider(get_class($this), array(
				'criteria'=>$criteria,
				'pagination'=> ( $list_output_format == Appfuncs::$LIST_OUTPUT_FORMAT_DATA_PROVIDER ? array('pageSize'=>Yii::app()->controller->app_config['admin_pageSize'],'params'=> $pagination_filters, 'currentPage' => $page ) : false ),
			));
			if ( $list_output_format == Appfuncs::$LIST_OUTPUT_FORMAT_DATA_PROVIDER ) {
				return $ActiveDataProvider;
			}
			if ( $list_output_format == Appfuncs::$LIST_OUTPUT_FORMAT_LIST ) {
				return $ActiveDataProvider->getData();
			}
		}
	}

	public function getGoodsSelectionList( ) {
		$GoodsList = $this->getGoodsList(Appfuncs::$LIST_OUTPUT_FORMAT_LIST, '', array(), '', '', 'C.name', true /* '', 'good.id, good.name'*/ );
		$ResArray = array();
		foreach($GoodsList as $lGood) {
			$ResArray[$lGood->id] = $lGood->name;
		}
		return $ResArray;
	}


	public function getRowById( $id )
	{
		$model=Good::model()->findByPk((int)$id);
		return $model;
	}

	public function IsExistsSimilarByName( $good_id, $name ) {
		$good_id_condition= '';
		if ( !empty($good_id) ) {
			$good_id_condition= ' good_id != '. Yii::app()->db->quoteValue($good_id);
		}
		return Good::model()->findByAttributes( array('name'=>$name ),	$good_id_condition  );
	}

}