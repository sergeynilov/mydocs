<?php

class ClientController extends ERestController /*Controller*/
{

	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '//layouts/frontend_main';
	public $defaultAction = "index";

	/**
	 * @return array action filters
	 */
	public function _filters()
	{
		return array();
	}

	public function _accessRules()
	{

		return array();
	}

	public function doRestDelete($id)
	{
		$GoodsList = Good::model()->getGoodsList(Appfuncs::$LIST_OUTPUT_FORMAT_LIST, '', array('client_id' => $id));
		if (count($GoodsList) > 0) {
			echo CJSON::encode(array('ErrorMessage' => "Can not delete client with Id = '" . $id . "' as it has '" . count($GoodsList) . "' related good(s) !", 'ErrorCode' => 1, 'client_id' => id));
			Yii::app()->end();
		}

		$model = Client::model()->getRowById($id);
		if (!empty($model)) {
			$model->delete();
			echo CJSON::encode(array('ErrorMessage' => "", 'ErrorCode' => 0, 'client_id' => $id));
			Yii::app()->end();
		}
		echo CJSON::encode(array('ErrorMessage' => 'Invalid parameters', 'ErrorCode' => 1, 'client_id' => 0));
	}

	public function doRestCreate($data)
	{
		$field_must_be_required = Appfuncs::VerifyRequiredField($data, array('name', 'is_active', 'activation_key', 'login', 'password', 'active_till_date'));
		if (!empty($field_must_be_required)) {
			echo CJSON::encode(array('ErrorMessage' => " Field '" . $field_must_be_required . "' is required ! ", 'ErrorCode' => 1, 'ErrorField' => $field_must_be_required));
			Yii::app()->end();
		}
		//FilesFuncs::DebToFile('doRestCreate $data::' . print_r($data, true), false);

		$ClientsList = Client::model()->getClientsList(Appfuncs::$LIST_OUTPUT_FORMAT_LIST, '', array('name_strict' => urldecode($data['name'])), 'name asc');
		if (count($ClientsList) > 0) {
			echo CJSON::encode(array('ErrorMessage' => "There is already client with '" . urldecode($data['name']) . "' name! It must be unique!", 'ErrorCode' => 1, 'client_id' => 0));
			Yii::app()->end();
		}

		$ModelClient = new Client();
		$ModelClient->name = urldecode($data['name']);
		$ModelClient->is_active = urldecode($data['is_active']);
		$ModelClient->activation_key = urldecode($data['activation_key']);
		$ModelClient->login = urldecode($data['login']);
		$ModelClient->password = urldecode($data['password']);
		$ModelClient->active_till_date = $data['active_till_date'];
		if ($ModelClient->save(false)) {
			echo CJSON::encode(array('ErrorMessage' => '', 'ErrorCode' => 0, 'client_id' => $ModelClient->client_id));
			Yii::app()->end();
		}
		echo CJSON::encode(array('ErrorMessage' => 'Invalid Parameters', 'ErrorCode' => 1, 'client_id' => 0));
	}

	public function doRestUpdate($id, $data, $data2 = '')
	{ // http://www.yiiframework.com/forum/index.php/topic/49185-restfullyii-yii-user/
		foreach ($data as $key => $value) {
			$data[$key] = urldecode($value);
		}
		$field_must_be_required = Appfuncs::VerifyRequiredField($data, array('name', 'is_active', 'activation_key', 'login', 'password', 'active_till_date'));
		if (!empty($field_must_be_required)) {
			echo CJSON::encode(array('ErrorMessage' => " Field '" . $field_must_be_required . "' is required ! ", 'ErrorCode' => 1, 'ErrorField' => $field_must_be_required));
			Yii::app()->end();
		}

		$ModelClient = Client::model()->getRowById($id);
		if (!empty($ModelClient)) {

			$SimilarGood = Good::model()->IsExistsSimilarByName($id, $data['name']);
			if (count($SimilarGood) > 0) {
				echo CJSON::encode(array('ErrorMessage' => "There is already good with '" . $data['name'] . "' name! It must be unique!", 'ErrorCode' => 1, 'good_id' => 0,
					'ErrorField' => 'name'));
				Yii::app()->end();
			}
			$ModelClient->name = urldecode($data['name']);
			$ModelClient->is_active = urldecode($data['is_active']);
			$ModelClient->activation_key = urldecode($data['activation_key']);
			$ModelClient->login = urldecode($data['login']);
			$ModelClient->password = urldecode($data['password']);
			$ModelClient->active_till_date = urldecode($data['active_till_date']);
			if ($ModelClient->save(false)) {
				echo CJSON::encode(array('ErrorMessage' => '', 'ErrorCode' => 0, 'client_id' => $ModelClient->client_id));
				Yii::app()->end();
			}
		}
		echo CJSON::encode(array('ErrorMessage' => 'Invalid Parameters', 'ErrorCode' => 1, 'client_id' => 0));
	}


	public function doRestView($id)
	{
		$ClientModel = Client::model()->getRowById($id);
		if (count($ClientModel) == 1) {
			echo CJSON::encode(array('ErrorMessage' => '', 'ErrorCode' => 0, 'data' => $ClientModel));
			Yii::app()->end();
		}
		echo CJSON::encode(array('ErrorMessage' => 'Invalid Parameters', 'ErrorCode' => 1, 'data' => array()));
	}

	public function doRestList()
	{
		$sort = '';
		$sort_direction = '';
		if (!empty($this->restSort)) {
			$A = preg_split('/-/', $this->restSort);
			if (count($A) == 2) {
				$sort = $A[0];
				$sort_direction = $A[1];
			}
		}

		$ClientsList = Client::model()->getClientsList(Appfuncs::$LIST_OUTPUT_FORMAT_LIST, '', array('limit' => $this->restLimit, 'offset' => $this->restOffset), $sort, $sort_direction);
		$data_array = array();
		foreach ($ClientsList as $Client) {
			$data_array[] = array('client_id' => $Client->client_id, 'name' => $Client->name, 'is_active' => $Client->is_active, 'activation_key' => $Client->activation_key, 'login' => $Client->login, 'password' => $Client->password, 'active_till_date' => $Client->active_till_date, 'created_at' => $Client->created_at);
		}
		echo CJSON::encode(array('data' => $data_array));
	}

}