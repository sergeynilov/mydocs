<?php

class GoodController extends ERestController /*Controller*/
{

	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '//layouts/frontend_main';
	public $defaultAction = "index";

	/**
	 * @return array action filters
	 */
	public function _filters()
	{
		return array();
	}

	public function _accessRules()
	{
		return array();
	}

	public function doRestDelete($id)
	{
		$model = Good::model()->getRowById($id);
		if (!empty($model)) {
			$model->delete();
			echo CJSON::encode(array('ErrorMessage' => "", 'ErrorCode' => 0, 'good_id' => $id));
			Yii::app()->end();
		}
		echo CJSON::encode(array('ErrorMessage' => '', 'ErrorCode' => 1, 'good_id' => 0));
	}

	public function doRestCreate($data)
	{
		$field_must_be_required = Appfuncs::VerifyRequiredField($data, array('name', 'isatsatore', 'price', 'client_id', 'shop_address', 'discount'));
		if (!empty($field_must_be_required)) {
			echo CJSON::encode(array('ErrorMessage' => " Field '" . $field_must_be_required . "' is required ! ", 'ErrorCode' => 1, 'ErrorField' => $field_must_be_required));
			Yii::app()->end();
		}

		$SimilarGood= Good::model()->IsExistsSimilarByName( null, $data['name'] );
		if (count($SimilarGood) > 0) {
			echo CJSON::encode(array('ErrorMessage' => "There is already good with '" . $data['name'] . "' name! It must be unique!", 'ErrorCode' => 1, 'good_id' => 0,
				'ErrorField' => 'name'));
			Yii::app()->end();
		}

		if (!empty($data['goodgroup_id'])) {
			$GoodgroupModel = Goodgroup::model()->getRowById($data['goodgroup_id']);
			if (empty($GoodgroupModel)) {
				echo CJSON::encode(array('ErrorMessage' => " Parameter goodgroup_id must be empty or 1-5, but not '" . $data['goodgroup_id'] . "' ! ", 'ErrorCode' => 1, 'ErrorField' => 'goodgroup_id'));
				Yii::app()->end();
			}
		}

		if (!empty($data['client_id'])) {
			$ClientModel = Client::model()->getRowById($data['client_id']);
			if (empty($ClientModel)) {
				echo CJSON::encode(array('ErrorMessage' => " Parameter client_id must be valid clientId value from table above, but not '" . $data['client_id'] . "' ! ", 'ErrorCode' => 1, 'good_id' => 0, 'ErrorField' => 'client_id'));
				Yii::app()->end();
			}
		}

		$ModelGood = new Good();
		$ModelGood->name = $data['name'];
		$ModelGood->goodgroup_id = $data['goodgroup_id'];
		$ModelGood->isatsatore = $data['isatsatore'];
		$ModelGood->price = $data['price'];
		$ModelGood->client_id = $data['client_id'];
		$ModelGood->shop_address = $data['shop_address'];
		$ModelGood->discount = $data['discount'];

		if ($ModelGood->save(false)) {
			echo CJSON::encode(array('ErrorMessage' => '', 'ErrorCode' => 0, 'good_id' => $ModelGood->good_id));
			Yii::app()->end();
		}
		echo CJSON::encode(array('ErrorMessage' => 'Invalid Parameters', 'ErrorCode' => 1, 'good_id' => 0));
	}

	public function doRestUpdate($id, $data, $data2 = '')
	{
		foreach( $data as $key=> $value ) {
			$data[$key]= urldecode($value);
		}

		$field_must_be_required = Appfuncs::VerifyRequiredField($data, array('name', 'isatsatore', 'price', 'client_id', 'shop_address', 'discount'));
		if (!empty($field_must_be_required)) {
			echo CJSON::encode(array('ErrorMessage' => " Field '" . $field_must_be_required . "' is required ! ", 'ErrorCode' => 1, 'ErrorField' => $field_must_be_required));
			Yii::app()->end();
		}

		$ModelGood = Good::model()->getRowById($id);
		if (!empty($ModelGood)) {
			$SimilarGood= Good::model()->IsExistsSimilarByName( $id, $data['name'] );
			if (count($SimilarGood) > 0) {
				echo CJSON::encode(array('ErrorMessage' => "There is already good with '" . $data['name'] . "' name! It must be unique!", 'ErrorCode' => 1, 'good_id' => 0,
					'ErrorField' => 'name'));
				Yii::app()->end();
			}
			if (!empty($data['goodgroup_id'])) {
				$GoodgroupModel = Goodgroup::model()->getRowById($data['goodgroup_id']);
				if (empty($GoodgroupModel)) {
					echo CJSON::encode(array('ErrorMessage' => " Parameter goodgroup_id must be empty or 1-5, but not '" . $data['goodgroup_id'] . "' ! ", 'ErrorCode' => 1, 'ErrorField' => 'goodgroup_id'));
					Yii::app()->end();
				}
			}

			if (!empty($data['client_id'])) {
				$ClientModel = Client::model()->getRowById($data['client_id']);
				if (empty($ClientModel)) {
					echo CJSON::encode(array('ErrorMessage' => " Parameter client_id must be valid clientId value from table above, but not '" . $data['client_id'] . "' ! ", 'ErrorCode' => 1, 'good_id' => 0, 'ErrorField' => 'client_id'));
					Yii::app()->end();
				}
			}

			$ModelGood->name = $data['name'];
			$ModelGood->goodgroup_id = $data['goodgroup_id'];
			$ModelGood->isatsatore = $data['isatsatore'];
			$ModelGood->price = $data['price'];
			$ModelGood->client_id = $data['client_id'];
			$ModelGood->shop_address = $data['shop_address'];
			$ModelGood->discount = $data['discount'];
			if ($ModelGood->save(false)) {
				echo CJSON::encode(array('ErrorMessage' => '', 'ErrorCode' => 0, 'good_id' => $ModelGood->good_id));
				Yii::app()->end();
			}
		}
		echo CJSON::encode(array('ErrorMessage' => 'Invalid Parameters', 'ErrorCode' => 1, 'good_id' => 0));
	}

/////////////////////////

	public function doRestList()
	{
		$goodgroup_id = (!empty($_REQUEST['goodgroup_id']) ? $_REQUEST['goodgroup_id'] : '');
		$sort = '';
		$sort_direction = '';
		if ( !empty($this->restSort) ) {
			$A = preg_split('/-/', $this->restSort);
		  if (count($A) == 2) {
			  $sort = $A[0];
			  $sort_direction = $A[1];
			}
		}

		$GoodsList = Good::model()->getGoodsList(Appfuncs::$LIST_OUTPUT_FORMAT_LIST, '', array( /*'good_id'=> $good_id, */
			'goodgroup_id' => $goodgroup_id,'limit'=> $this->restLimit, 'offset'=> $this->restOffset), $sort, $sort_direction);
		$data_array = array();
		foreach ($GoodsList as $Good) {
			$Goodgroup = Goodgroup::model()->getRowById($Good->goodgroup_id);
			if (empty($Goodgroup)) {
				$goodgroup_name = 'No Group';
			} else {
				$goodgroup_name = $Goodgroup->name;
			}
			$data_array[] = array('good_id' => $Good->good_id, 'name' => $Good->name, 'price' => $Good->price, 'discount' => $Good->discount, 'goodgroup_id' => $Good->goodgroup_id, 'goodgroup_name' => $goodgroup_name, 'isatsatore' => $Good->isatsatore, 'client_id' => $Good->client_id, 'shop_address' => $Good->shop_address);
		}
		echo CJSON::encode(array('data' => $data_array));
	}

	public function doRestView($id)
	{
		$GoodModel = Good::model()->getRowById($id);
		if (count($GoodModel) == 1) {
			echo CJSON::encode(array('ErrorMessage' => '', 'ErrorCode' => 0, 'data' => $GoodModel));
			Yii::app()->end();
		}
		echo CJSON::encode(array('ErrorMessage' => 'Invalid Parameters', 'ErrorCode' => 1, 'data' => array()));
	}

}