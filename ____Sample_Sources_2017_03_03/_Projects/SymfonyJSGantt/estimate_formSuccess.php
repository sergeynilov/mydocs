<?php
$HostForImage= AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() );
$RateValue = 2; // TODO !!! Rate (as defined by the employee Resource) ?>
<script type="text/javascript" language="JavaScript">
<!--
var CurrentItemIndex= 0
var CurrentParentId= ''
var CurrentStartDate= ''
var CurrentAlternativeParentId= ''
var MaxItemIndex= 0
var AddAlternativeItemBox= false
var g
var ProjectItemInfosData= Array()
var	ProjectItemInfosDataLength= 0
var IsChanged= false
var OpenedItemsList= Array();
var IsFirstDataLoad= true;

var ProjectItemInfosDataBuffer= Array()
var	ProjectItemInfosDataBufferLength= 0
var MarkUpPercentage= 2;  // !!!   //Sales = Cost * Markup (mark-up is a percentage defined on the Company Preferences page)
var CurrentScheduleDurationDate= "<?php echo strftime( sfConfig::get('app_application_date_format'),  AppUtils::GetFirstDayOfWeek() ) ?>" ; // !!! TODO
var CurrentScheduleDurationDateGanttFormat= "<?php echo strftime( sfConfig::get('app_application_gantt_date_formatt'),  AppUtils::GetFirstDayOfWeek() ) ?>" ; // !!! TODO

var ColorsArray= Array( 	"000000", "6600cc",	"930000",  "0033cc", "663300",	"cc33cc", "006600",	"6666cc", "cc6600",
  "FFDBAA",	"0099cc", "669900",	"999966",  "00cc00", "66cccc", "cccc00",	"00ffcc",	"66ff00",	"FA0000",
  "ffcc66",	"330066", "99ff66", "00FFFF", "336666", "cc99cc", "ccffcc",	"33cc66", "993366", "FA8755",
  "997E43", "00C000", "C3C3C3" , "0CFFFF", "E8FF4F", "DCDCDC", "F2C76A", "008000", "FF00FF", "FACEB6" );



function getItemIndexByParentId( ProjectItemInfosData, ParentId ) {
  var L= ProjectItemInfosData.length
  for( var I= 0; I< L; I++ ) {
    if ( ProjectItemInfosData[I].id == ParentId ) {
      return ProjectItemInfosData[I].item_index;
    }
  }
  return 0;
}

function getItemIndexByTaskFollowsId( ProjectItemInfosData, TaskFollowsId ) {
  var L= ProjectItemInfosData.length
  for( var I= 0; I< L; I++ ) {
    if ( ProjectItemInfosData[I].id == TaskFollowsId ) {
      return ProjectItemInfosData[I].item_index;
    }
  }
  return 0;
}

function getItemInfoByItemIndex( ProjectItemInfosData, ItemIndex ) {
  var L= ProjectItemInfosData.length
  for( var I= 0; I< L; I++ ) {
    if ( ProjectItemInfosData[I].item_index == ItemIndex ) {
      return ProjectItemInfosData[I];
    }
  }
  return '';
}

function getItemInfoById( ProjectItemInfosData, Id ) {
  var L= ProjectItemInfosData.length
  for( var I= 0; I< L; I++ ) {
    if ( ProjectItemInfosData[I].id == Id ) {
      return ProjectItemInfosData[I];
    }
  }
  return '';
}

function SetCostValue(SrcField, SrcFieldName) { // Cost = Labor + Subcontract + Other + Equipment + Materials
	IsChanged= true
	var Labor= Trim( document.getElementById("labor_index").value );
	var Subcontract= Trim( document.getElementById("subcontract_index").value );
	var Other= Trim( document.getElementById("other_index").value );
	var Equipment= Trim( document.getElementById("equipment_index").value );
	var Materials= Trim( document.getElementById("materials_index").value );
	if ( !CheckFloat(  Trim(document.getElementById(SrcField).value)  ) ) {
    alert("Invalid Float format for "+SrcFieldName)
		document.getElementById(SrcField).focus()
		return;
	}
	Labor= parseFloat(Labor);
	if ( isNaN(Labor) ) Labor= 0;
	Subcontract= parseFloat(Subcontract)
	if ( isNaN(Subcontract) ) Subcontract= 0;
	Other= parseFloat(Other)
	if ( isNaN(Other) ) Other= 0;
	Equipment= parseFloat(Equipment)
	if ( isNaN(Equipment) ) Equipment= 0;
	Materials= parseFloat(Materials)
	if ( isNaN(Materials) ) Materials= 0;
	document.getElementById("cost_index").value= Labor + Subcontract + Other + Equipment + Materials
	onChangeCost()
}


function onChangeCost() { //Sales = Cost * Markup (mark-up is a percentage defined on the Company Preferences page)
	IsChanged= true
	var Cost= Trim( document.getElementById("cost_index").value );
	if ( Cost=="" ) {
		document.getElementById("cost_index").value= "0"
    document.getElementById("sales_index").value= "0";
		return;
	}
	if ( !CheckFloat( Cost ) ) {
    alert("Invalid Float format for Cost")
		document.getElementById("cost_index").focus()
		return;
	}
	Cost= parseFloat(Cost)
	if ( isNaN(Cost) ) Cost= 0;
	document.getElementById("sales_index").value= MarkUpPercentage * Cost;
}

function onChangeHrs() { // Subcontract = Hrs * Rate (as defined foemployeer the subcontractor Resource)
                         // Labor = Hrs * Rate (as defined by the  Resource)
	IsChanged= true
	var Hrs= Trim( document.getElementById("hrs_index").value );
	if ( Hrs=="" ) {
		document.getElementById("hrs_index").value= "0"
		Hrs= 0
	}
	if ( !CheckInteger( Hrs ) ) {
    alert("Invalid Integer format for Hrs")
		document.getElementById("cost_index").focus()
		return;
	}
	var Rate= Trim( document.getElementById("rate_index").value );
	Rate= parseFloat(Rate)
	if ( isNaN(Rate) ) Rate= 0;
  document.getElementById("subcontract_index").value= Hrs * Rate;
  document.getElementById("labor_index").value= Hrs * Rate;
  SetCostValue( 'subcontract_index', 'Subcontract' )
  // These values should re-calculate with Javascript whenever any of the related fields are updated. For example, if Count is updated then Materials should be recalculated using the new Count value.  If Hrs is updated, then Labor, Subcontract, Cost and Sales should all be recalculated.

}

function setOpenedItemsListItem( ItemIndex, toOpen ) { // to true= Open
  var L= OpenedItemsList.length
  if ( toOpen ) {
    var IsOpened= false
    for( var I= 0; I< L; I++ ) {
      if ( OpenedItemsList[I] == parseInt(ItemIndex) ) {
        IsOpened= true
        break;
      }
    }
    if ( !IsOpened ) OpenedItemsList[OpenedItemsList.length]= ItemIndex
  } else {
    var A= Array()
    for( var I= 0; I< L; I++ ) {
      // alert( "OpenedItemsList[I]::"+OpenedItemsList[I] + "  ItemIndex::"+ItemIndex )
      if ( OpenedItemsList[I] != parseInt(ItemIndex) ) {
        A[A.length]= OpenedItemsList[I]
        //alert( "A::"+var_dump(A) )
      }
    }
    OpenedItemsList= A
    // alert( "AFTER OpenedItemsList ::"+var_dump(OpenedItemsList) )
  }
}

function InitGantt() {
  document.getElementById('GanttChartDIV').innerHTML= '';
  g = null
  g = new JSGantt.GanttChart('g',document.getElementById('GanttChartDIV'), 'day');

	g.setShowRes(1); // Show/Hide Responsible (0/1)
	g.setShowDur(1); // Show/Hide Duration (0/1)
	g.setShowComp(0); // Show/Hide % Complete(0/1)
  g.setCaptionType('Resource');  // Set to Show Caption (None,Caption,Resource,Duration,Complete)

  g.setShowStartDate(0); // Show/Hide Start Date(0/1)
  g.setShowEndDate(0); // Show/Hide End Date(0/1)
  g.setDateInputFormat('mm/dd/yyyy')  // Set format of input dates ('mm/dd/yyyy', 'dd/mm/yyyy', 'yyyy-mm-dd')
  g.setDateDisplayFormat('mm/dd/yyyy') // Set format to display dates ('mm/dd/yyyy', 'dd/mm/yyyy', 'yyyy-mm-dd')
  g.setImagesDirectory( "<?php echo AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() ) ?>images" );
  g.setHideDateColumns( true );

  g.setFlaggedItemInfos( getFlaggedItemInfos( ProjectItemInfosData, ProjectItemInfosDataLength ) )
  var ItemsWithOptionsList= Array()
  var ItemsAreAlternativeList= Array()

  if( g ) {
    for( var J= 0; J< ProjectItemInfosDataLength; J++ ) {

      if( ProjectItemInfosData[J].option == 1) {
        ItemsWithOptionsList[ ItemsWithOptionsList.length ]= ProjectItemInfosData[J].item_index
      }


      var SearchableItemInfo= getItemInfoById( ProjectItemInfosData, ProjectItemInfosData[J].alternative_parent_id );
      if (SearchableItemInfo!= '') {
        ItemsAreAlternativeList[ ItemsAreAlternativeList.length ]= { 'item_index' : ProjectItemInfosData[J].item_index, 'alternative_parent_id' : SearchableItemInfo['item_index'] };
      }


    	if ( ProjectItemInfosData[J].start_date_gantt_format == '01/01/1970' ) continue;
    	if ( IsFirstDataLoad ) {
    	  ItemOpened= 1 // If list of opened Items is empty && first load of data - then show all opened
    	  OpenedItemsList[OpenedItemsList.length]= ProjectItemInfosData[J].item_index
    	} else {
    	  ItemOpened= 0
    	  for( JJJ= 0; JJJ < OpenedItemsList.length; JJJ++ ) {
    	    if( OpenedItemsList[JJJ] == ProjectItemInfosData[J].item_index ) {
    	      ItemOpened= 1
    	    }
    	  }
    	}
    	if ( J <= ColorsArray.length - 1 ) {
    	  ItemColor= ColorsArray[J];
    	} else {
    	  var KIndex= J % ColorsArray.length
    	  if ( KIndex >= 0 && KIndex< ColorsArray.length ) ItemColor= ColorsArray[KIndex];
    	}

      ItemIsGroup_Parent= 0
      ParentId= 0
      if (  ProjectItemInfosData[J].parent_id > 0 ) {
      	ParentId= ProjectItemInfosData[J].parent_id
      }

      var ItemIndexOfParent= getItemIndexByParentId( ProjectItemInfosData, ParentId )


      var ItemIndexOfTaskFollowsId= getItemIndexByTaskFollowsId( ProjectItemInfosData, ProjectItemInfosData[J].task_follows_id )
      if ( ProjectItemInfosData[J].SubitemsCount > 0  ) {
        ItemIsGroup_Parent=1
      }

      g.AddTaskItem(new JSGantt.TaskItem( ProjectItemInfosData[J].item_index /*1*/,  ProjectItemInfosData[J].item /*'Define Chart API'*/,     /*pStart*/ ProjectItemInfosData[J].start_date_gantt_format,          /*pEnd*/ ProjectItemInfosData[J].end_date_gantt_format,          /*pColor*//*'ff0000'*/ ItemColor, /*pLink*/ProjectItemInfosData[J].item_index, /* pMile*/0, /*pRes*/ProjectItemInfosData[J].employee_name, /*pComp*/0, /*pGroup*/ ItemIsGroup_Parent, /*pParent*/ItemIndexOfParent, /*pOpen*/ItemOpened, ItemIndexOfTaskFollowsId/* pDepend */  ));
    } // for( var J= 0; J< ProjectItemInfosDataLength; J++ ) {

    g.setItemsWithOptionsList(ItemsWithOptionsList);
    g.setItemsAreAlternativeList(ItemsAreAlternativeList);


    g.Draw();
    g.DrawDependencies();
    $("#theLeftTable tbody").sortable(   //http://stackoverflow.com/questions/4471520/jquery-ui-sortable-table-handle
      {      handle: 'td:first',  items: ".tr_sortable",
       update: function(event, ui) {

         var previous_item_id= ui.item[0].previousElementSibling.id
         if( Trim(previous_item_id).lenght=0 || typeof previous_item_id== "undefined" ) previous_item_id= 0
       	 var HRef= '<?php echo url_for('@admin_set_project_item_info_sorted?project_id='.$project_id.'&moving_item_id='); ?>' + ui.item[0].id+"/previous_item_id/"+previous_item_id +"/next_item_id/"+ui.item[0].nextElementSibling.id
  	     $.getJSON(HRef,   {  },
	         onItemInfoSorted,
	         function(x,y,z) {   //Some sort of error
		         alert(x.responseText);
	         }
	       );
       } // update: function(event, ui) {

     }

    ).disableSelection();
    IsFirstDataLoad= false
    return g
  }
  else
  {
    alert("not defined");
  }

} // function InitGantt() {

function onItemInfoSorted(data) {
	if ( parseInt(data.ErrorCode) == 0 ) {
	  var ChangedItemIndexesArray= data.ChangedItemIndexesArray
	  var OriginalOpenedItemsList= Array()
	  var L= ChangedItemIndexesArray.length
	  var LLL= OpenedItemsList.length
	  for( var I= 0; I< LLL; I++ ) {
	    OriginalOpenedItemsList[I]= OpenedItemsList[I]
	  }
	  for( var I= 0; I< L; I++ ) {
  	  for( var III= 0; III< LLL; III++ ) {
  	    if ( parseInt(OriginalOpenedItemsList[III]) == parseInt(ChangedItemIndexesArray[I].prior) ) {
  	      OpenedItemsList[III] = ChangedItemIndexesArray[I].current
  	      break;
  	    }
  	  }
	  }
	  LoadProjectItemInfoHeaders(1)
	}
}


function MakeMoveItemLeft( ItemInfoId ) {
  var HRef= '<?php echo url_for('@admin_make_move_item_left?project_id='.$project_id.'&moving_item_id='); ?>' + ItemInfoId
  // alert(HRef)
  $.getJSON(HRef,   {  },
	  onMakeMoveItemLeft,
	  function(x,y,z) {   //Some sort of error
	    alert(x.responseText);
	  }
	);
}

function onMakeMoveItemLeft(data) {
	if ( parseInt(data.ErrorCode) == 0 ) {
	  LoadProjectItemInfoHeaders(1)
	}
}

function MakeMoveItemRight( ItemInfoId ) {
  var HRef= '<?php echo url_for('@admin_make_move_item_right?project_id='.$project_id.'&moving_item_id='); ?>' + ItemInfoId
  $.getJSON(HRef,   {  },
	  onMakeMoveItemRight,
	  function(x,y,z) {   //Some sort of error
	    alert(x.responseText);
	  }
	);
}
function onMakeMoveItemRight(data) {
	if ( parseInt(data.ErrorCode) == 0 ) {
	  LoadProjectItemInfoHeaders(1)
	}
}


function FormUpdate( NextAction, ActionTypeSrcItemIndex, ActionTypeItemIndex ) {
	if ( !IsChanged ) return false;
	var validator = $("#estimate_form").validate();
	validator.valid();
	var valid = validator.form();
	if ( valid ) {
		MakeSave( NextAction, ActionTypeSrcItemIndex, ActionTypeItemIndex )
		return true;
	}
	return false;
}

function MakeSave( NextAction, ActionTypeSrcItemIndex, ActionTypeItemIndex ) {
	if ( !IsChanged )	return;

	if ( typeof g != "undefined"  ) {
	  OpenedItemsList= g.getOpenedItemsList();
	}
  var alternative_parent_obj= getItemInfoByItemIndex( ProjectItemInfosData, document.getElementById("hidden_CurrentAlternativeParentId").value )

  var DataArray= {
			"item_index" : CurrentItemIndex,
			"parent_id" : document.getElementById("hidden_show_CurrentParentId").value,
		  "item" :document.getElementById("name_index").value,
		  "start_date" : document.getElementById("hidden_show_CurrentStartDate").value,
		  "alternative_parent_id" : alternative_parent_obj.id,
		  "job_phase_id" : document.getElementById("job_phase_id_index").value,
		  "product_id" : document.getElementById("product_id_index").value,
		  "vendor_id" : document.getElementById("vendor_id_index").value,
		  "lead_time" : document.getElementById("lead_time_index").value.replace ( /\//g, "-" ),
		  "description" : document.getElementById("description_index").value,
		  "ccount" : document.getElementById("ccount_index").value,
		  "employee_id" : document.getElementById("employee_id_index").value,
		  "duration" : document.getElementById("duration_index").value,
		  "notes" : Trim(document.getElementById("notes_index").value),
		  "sales" : document.getElementById("sales_index").value,
		  "cost" : document.getElementById("cost_index").value,
		  "materials" : document.getElementById("materials_index").value,
		  "equipment" : document.getElementById("equipment_index").value,
		  "other" : document.getElementById("other_index").value,
		  "subcontract" : document.getElementById("subcontract_index").value,
		  "labor" : document.getElementById("labor_index").value,
		  "hrs" : document.getElementById("hrs_index").value,
		  "rate" : document.getElementById("rate_index").value,
		  "allowance" : ( document.getElementById("allowance_index").checked ? "1" : "0" ),
		  "selection_item" : ( document.getElementById("selection_item_index").checked ? "1" : "0" ),
		  "option" : ( document.getElementById("option_index").checked ? "1" : "0" ),
		  "private_memo" : ( document.getElementById("private_memo_index").checked ? "1" : "0" ),
		  "flag" : ( document.getElementById("flag_index").checked ? "1" : "0" ),
		  "flag_text" : Trim(document.getElementById("flag_text_index").value),
		  "task_follows_id" : document.getElementById("task_follows_id_index").value
	};

	$.ajax({
    url: '<?php echo url_for('@admin_update_project_item_info?project_id='.$project_id); ?>/next_action/' + NextAction +
    "/action_type_src_item_index/"+ActionTypeSrcItemIndex +/action_type_item_index/+ ActionTypeItemIndex,
    type: "POST",
    data: DataArray,
    success: onSaved,
    dataType: "json"
  });
}

function onSaved(data) {
	var next_action= data.next_action
	if ( next_action=="" || next_action == "alert" ) {
	  alert("Saved !")
	}
	var action_type_src_item_index= data.action_type_src_item_index
	var action_type_item_index= data.action_type_item_index
	LoadProjectItemInfoHeaders( 0, next_action, action_type_src_item_index, action_type_item_index )
	IsChanged= false
}

function AddAlternative( SrcItemIndex, ActionType, ItemIndex, IsAlternative ) {
  ItemIndex= parseInt( ItemIndex )
  SrcItemIndex= parseInt( SrcItemIndex )

  if (IsAlternative) {
    document.getElementById("hidden_CurrentAlternativeParentId").value= CurrentItemIndex
  } else{
    document.getElementById("hidden_CurrentAlternativeParentId").value= '';
  }
	if ( IsChanged ) {
		FormUpdate( ActionType, SrcItemIndex, ItemIndex )
		return;
	}
	IsChanged= true;
	MaxItemIndex= parseInt(MaxItemIndex)+1
	LoadTaskFollowsIds("AddAlternative")
	if ( ActionType == "" ) {
	  AddItem( "", "" )
	}
	if ( ActionType == "AddItem" ) {
	  AddItem( SrcItemIndex, ActionType )
	  ItemColor= "DCDCDC";
	  if ( MaxItemIndex <= ColorsArray.length - 1) ItemColor= ColorsArray[MaxItemIndex];
   	ParentId= SrcItemIndex

    var SrcItemInfo= getItemInfoByItemIndex( ProjectItemInfosData, SrcItemIndex )
    var start_date= SrcItemInfo.start_date
   	var end_date= SrcItemInfo.end_date
    var start_date_gantt= SrcItemInfo.start_date_gantt_format
    var end_date_gantt= SrcItemInfo.end_date_gantt_format

    var duration= SrcItemInfo.duration
    var parent_id= SrcItemInfo.parent_id // !!!
    CurrentParentId= parent_id
    CurrentStartDate= start_date
    CurrentAlternativeParentId= IsAlternative //!!!

    var task_follows_id= SrcItemInfo.task_follows_id

    for ( var I= ProjectItemInfosDataLength; I> ItemIndex+1; I-- ) {
      ProjectItemInfosData[I]= ProjectItemInfosData[I-1]
    }

    ProjectItemInfosData[ItemIndex+1]= {
      item_index : parseInt( MaxItemIndex /*SrcItemIndex*/ ) ,
   	  item : '(New Item)',
      parent_id : parent_id,
      start_date : start_date,
      end_date : end_date,
      start_date_gantt_format : start_date_gantt,
      end_date_gantt_format : end_date_gantt,
      employee_name : '',
      duration : duration,
      flag : 0,
   	 };

   	ProjectItemInfosDataLength= ProjectItemInfosDataLength+1

   	g= InitGantt()
   	document.getElementById("name_index").value= '(New Item)'
   	document.getElementById("duration_index").value= 1; //duration

	} // if ( ActionType == "AddItem" ) {

	if ( ActionType == "CopyItem" ) {
	  CopyItem( SrcItemIndex, ActionType )
	}

	CurrentItemIndex= MaxItemIndex
	document.getElementById("hidden_show_CurrentItemIndex").value= CurrentItemIndex
	document.getElementById("hidden_show_CurrentParentId").value= CurrentParentId
	document.getElementById("hidden_show_CurrentStartDate").value= CurrentStartDate
	ActionType= "";
	SrcItemIndex= "";
	return MaxItemIndex
}

function AddItem( srcItemIndex, ActionType ) {
	// alert( "AddItem(  srcItemIndex::"+srcItemIndex+"  ActionType::"+ActionType )
	document.getElementById( "name_index" ).value= ""
	document.getElementById( "job_phase_id_index" ).value= ""
	document.getElementById( "product_id_index" ).value= ""
	document.getElementById( "vendor_id_index" ).value= ""
	document.getElementById( "employee_id_index" ).value= ""
	document.getElementById( "ccount_index" ).value= ""
	document.getElementById( "lead_time_index" ).value= ""
	document.getElementById( "duration_index" ).value= ""
	//alert(  "document.getElementById( duration_index ).value::" + document.getElementById( "duration_index" ).value  )
	document.getElementById( "notes_index" ).value= ""
	document.getElementById( "description_index" ).value= ""
	document.getElementById( "sales_index" ).value= ""
	document.getElementById( "cost_index" ).value= ""
	document.getElementById( "materials_index" ).value= ""
	document.getElementById( "equipment_index" ).value= ""
	document.getElementById( "other_index" ).value= ""
	document.getElementById( "subcontract_index" ).value= ""
	document.getElementById( "labor_index" ).value= ""
	document.getElementById( "hrs_index" ).value= ""
	document.getElementById( "rate_index" ).value= "<?php echo $RateValue ?>"
	document.getElementById( "allowance_index" ).checked= false
	document.getElementById( "selection_item_index" ).checked= false
	document.getElementById( "option_index" ).checked= false
	document.getElementById( "private_memo_index" ).checked= false
	document.getElementById( "flag_index" ).checked= false
	document.getElementById( "flag_text_index" ).value= ""
  document.getElementById( "name_index" ).focus()
  if ( parseInt(srcItemIndex)> 0 ) {
  	var HRef= '<?php echo url_for('@admin_get_project_info_task_follows_id?project_id='.$project_id.'&item_index='); ?>' + srcItemIndex
  	$.getJSON(HRef,   {  },
	  onGetProjectInfoParentId,
	  function(x,y,z) {   //Some sort of error
		  alert(x.responseText);
	  }
	  );
  }
}
function onGetProjectInfoParentId(data) {
	if ( parseInt(data.ErrorCode) == 0 ) {
   	SetDDLBActiveItem( "task_follows_id_index", data.task_follows_id )
	}
}

function CopyItem( srcItemIndex, DestItemIndex ) {
  var HRef= '<?php echo url_for('@admin_load_project_item_info_by_item_index?project_id='.$project_id.'&add_alternative=1&item_index=') ?>'+srcItemIndex+'/max_item_index/'+MaxItemIndex
	AddAlternativeItemBox= true
	$.getJSON(HRef,   {  },
	onCopiedItem,
	function(x,y,z) {   //Some sort of error
		alert(x.responseText);
	}
	);
}

function onCopiedItem(data) {
	if ( parseInt(data.ErrorCode) != 0 ) return;
	document.getElementById( "name_index" ).value= data.Data['item']
	document.getElementById( "job_phase_id_index" ).value= data.Data['job_phase_id']
	document.getElementById( "product_id_index" ).value= data.Data['product_id']
	document.getElementById( "vendor_id_index" ).value= data.Data['vendor_id']
	document.getElementById( "employee_id_index" ).value= data.Data['employee_id']
	document.getElementById( "ccount_index" ).value= data.Data['ccount']
	document.getElementById( "lead_time_index" ).value= data.Data['lead_time']
	document.getElementById( "duration_index" ).value= data.Data['duration']
	document.getElementById( "notes_index" ).value= data.Data['notes']
	document.getElementById( "description_index" ).value= data.Data['description']
	document.getElementById( "sales_index" ).value= data.Data['sales']
	document.getElementById( "cost_index" ).value= data.Data['cost']
	document.getElementById( "materials_index" ).value= data.Data['materials']
	document.getElementById( "equipment_index" ).value= data.Data['equipment']
	document.getElementById( "other_index" ).value= data.Data['other']
	document.getElementById( "subcontract_index" ).value= data.Data['subcontract']
	document.getElementById( "labor_index" ).value= data.Data['labor']
	document.getElementById( "hrs_index" ).value= data.Data['hrs']
	document.getElementById( "rate_index" ).value= data.Data['rate']
	document.getElementById( "allowance_index" ).value= data.Data['']
	document.getElementById( "selection_item_index" ).value= data.Data['allowance']
	document.getElementById( "option_index" ).value= data.Data['option']
	document.getElementById( "private_memo_index" ).value= data.Data['private_memo']
	document.getElementById( "flag_index" ).checked = parseInt(data.Data['flag']) == 1,
	document.getElementById( "flag_text_index" ).value= data.Data['flag_text']

  document.getElementById( "name_index" ).focus()
  var srcItemIndex= data.Data['item_index']

  ItemColor= "DCDCDC";
	if ( MaxItemIndex <= ColorsArray.length - 1) ItemColor= ColorsArray[MaxItemIndex];
  TaskFollowsId= data.Data['task_follows_id']

  var start_date= data.Data['start_date']
  var end_date= data.Data['end_date']
  var start_date_gantt= data.Data['start_date_gantt_format']
  var end_date_gantt= data.Data['end_date_gantt_format']

  var duration= data.Data['duration']
  var task_follows_id= data.Data['task_follows_id']
  var parent_id= data.Data['parent_id'] //
  var alternative_parent_id= data.Data['alternative_parent_id'] //
  CurrentParentId= parent_id
  CurrentStartDate= start_date
  CurrentAlternativeParentId= alternative_parent_id

  for ( var I= ProjectItemInfosDataLength; I> srcItemIndex; I-- ) {
    ProjectItemInfosData[I]= ProjectItemInfosData[I-1]
  }

  document.getElementById("hidden_show_CurrentParentId").value= CurrentParentId;
  ProjectItemInfosData[srcItemIndex]= {
    item : data.Data['item'],   /* '(Copied Item)' */
    parent_id : parent_id,
    task_follows_id : task_follows_id,
    start_date : start_date,
    end_date : end_date,
    start_date_gantt_format : start_date_gantt,
    end_date_gantt_format : end_date_gantt,
    employee_name : '',
    duration : duration,
    flag : 0,
  };
  ProjectItemInfosDataLength= ProjectItemInfosDataLength+1

  InitGantt()

  if ( parseInt( data.Data['item_index'] )> 0 ) {
  	var HRef= '<?php echo url_for('@admin_get_project_info_task_follows_id?project_id='.$project_id.'&item_index='); ?>' + data.Data['item_index']
  	$.getJSON(HRef,   {  },
	  onGetProjectInfoParentId,
	  function(x,y,z) {   //Some sort of error
		  alert(x.responseText);
	  }
	  );
  }

}

function DeleteItem(pId) {
  if( parseInt(pId) <= 0 ) return ;
	var HRef= '<?php echo url_for('@admin_delete_project_info?project_id='.$project_id.'&item_index='); ?>' + pId
	$.getJSON(HRef,   {  },
	onDeleteItemed,
	function(x,y,z) {   //Some sort of error
		alert(x.responseText);
	}
	);
}

function onDeleteItemed(data) {
	if ( parseInt(data.ErrorCode) == 0 ) {
		if ( data.item_index == CurrentItemIndex ) {
    	LoadProjectItemInfoHeaders(1)
		} else {
		  LoadProjectItemInfoHeaders(0)
		}
	}
}

$(document).ready(function() {
	LoadProjectItemInfoHeaders( 1 );
	$("#estimate_form").validate({
		rules: { // http://jquery.bassistance.de/validate/demo/milk/
		},
		messages: {	},
		success: function(label) {
			label.html("&nbsp;").addClass("checked");
		},
	});

	jQuery.validator.addMethod("IntegerValue", function(entered_integer, element) {
		entered_integer = entered_integer.replace(/\s+/g, "");
		return this.optional(element) || entered_integer.length > 0 &&
		entered_integer.match(/^[0-9]+$/);
	}, "Invalid Integer");

	jQuery.validator.addMethod("MoneyValue", function(entered_money, element) {
		entered_money = entered_money.replace(/\s+/g, "");
		return this.optional(element) || entered_money.length > 0 &&
		entered_money.match(/^\d+(\.\d{0,2})?$/);
	}, "Invalid Money");

  AddValidateRules()
  var elem = document.getElementById("wrapper");
  if ( elem ) {
    elem.style.minWidth = "1663px";
  }


  var distFromTop = 20;
  var elOffset = $("#item_info_div").offset().top - distFromTop;
  $(window).bind('scroll',function() {
    if(window.pageYOffset>elOffset) {
      $("#item_info_div").css({
        'top':'-' + distFromTop + 'px',
        'position':'fixed'
        });
    } else {
      $("#item_info_div").css({
        'top':'auto',
        'position':'relative'
        });
    }
  })

});

function AddValidateRules() {
	$( "#name_index").rules("add", { required: true, messages: { required: "" } } ); // &nbsp;*
	$( "#duration_index" ).rules("add", { required: true, messages: { required: "" } } );
	$( "#duration_index" ).rules("add", { IntegerValue: true, messages: { IntegerValue: "Invalid Integer" } } );
}

function LoadAlternativeItems() {
  var CurrentItemIndexObj= getItemInfoByItemIndex( ProjectItemInfosData, CurrentItemIndex )
	var HRef= '<?php echo url_for('@admin_load_alternative_items?project_id='.$project_id) ?>/item_id/'+ CurrentItemIndexObj.id
	$.get(HRef,   {  },
	onLoadedAlternativeItems,
	function(x,y,z) {   //Some sort of error
		alert(x.responseText);
	}
	);
}

function onLoadedAlternativeItems(data) {
  document.getElementById("div_alternative_items").innerHTML= data
}

function LoadProjectItemInfoHeaders( ToReloadItemInfoData, next_action, action_type_src_item_index, action_type_item_index ) {
	var HRef= '<?php echo url_for('@admin_load_project_item_info_headers?project_id='.$project_id) ?>/to_reload_item_info_data/'+ToReloadItemInfoData+
	  "/next_action/" + next_action + "/action_type_src_item_index/" + action_type_src_item_index + "/action_type_item_index/" +action_type_item_index
	$.getJSON(HRef,   {  },
	onLoadedProjectItemInfoHeaders,
	function(x,y,z) {   //Some sort of error
		alert(x.responseText);
	}
	);
}
function onLoadedProjectItemInfoHeaders(data) {
	var ErrorCode=data.ErrorCode
	if ( ErrorCode > 0 ) {
		alert( data["ErrorMessage"] )
		return;
	}
	var DataLength= data.DataLength
	var ErrorCode= data.ErrorCode
	var to_reload_item_info_data= data.to_reload_item_info_data
	MaxItemIndex= data.MaxItemIndex

	ProjectItemInfosData= data.Data
	ProjectItemInfosDataLength= DataLength
	if ( DataLength > 0 && to_reload_item_info_data == 1 ) {
		LoadItemInfoByItemIndex( ProjectItemInfosData[0].item_index, false )
	}

	var next_action= data.next_action
	var action_type_src_item_index= data.action_type_src_item_index
	var action_type_item_index= data.action_type_item_index

	if ( next_action == "AddItem" ) {
	  AddAlternative( action_type_src_item_index, "AddItem", action_type_item_index, false )
	}
	if ( next_action == "CopyItem" ) {
	  AddAlternative( action_type_src_item_index, "CopyItem", action_type_item_index, false )
	}
	InitGantt()

	
	if ( DataLength == 0 ) {
		CurrentItemIndex= 1
		document.getElementById("hidden_show_CurrentItemIndex").value= CurrentItemIndex
		document.getElementById("hidden_show_CurrentParentId").value= CurrentParentId
		document.getElementById("hidden_show_CurrentStartDate").value= CurrentStartDate
		document.getElementById("hidden_CurrentAlternativeParentId").value= CurrentAlternativeParentId
		AddAlternative("","", -1, false)
	}
	CurrentParentId= ''
	CurrentStartDate= ''
  CurrentAlternativeParentId= '';
}

function LoadItemInfoByItemIndex( ItemIndex, MakeSaving ) {
  if ( MakeSaving && IsChanged ) {
	  var IsFormValid= FormUpdate( "LoadItemInfoByItemIndex", ItemIndex )
	  if ( !IsFormValid ) return;
	}
  document.getElementById("item_info_div").style.display= "none"
	document.getElementById("div_loadingAnimation").style.display= "block"

  var HRef= '<?php echo url_for('@admin_load_project_item_info_by_item_index?project_id='.$project_id.'&add_alternative=0&item_index=') ?>'+ItemIndex+'/MaxItemIndex/'+MaxItemIndex

	$.getJSON(HRef,   {  },
		onLoadedProjectItemInfoByInfoIndex,
		function(x,y,z) {   //Some sort of error
			alert(x.responseText);
		}
	);
}

function onLoadedProjectItemInfoByInfoIndex( data ) {
	IsChanged= false
	var DataLength= data.DataLength
	var DataArray= data.Data
	var ErrorCode=data.ErrorCode
	if ( ErrorCode > 0 ) {
		alert( data["ErrorMessage"] )
		return;
	}
	CurrentItemIndex= DataArray['item_index'];
	CurrentParentId= DataArray['parent_id'];
	CurrentStartDate= DataArray['start_date'];
	CurrentAlternativeParentId= DataArray['alternative_parent_id'];
	var subitems_count= DataArray['subitems_count']

	document.getElementById("hidden_show_CurrentItemIndex").value= CurrentItemIndex
	document.getElementById("hidden_show_CurrentParentId").value= CurrentParentId
	document.getElementById("hidden_show_CurrentStartDate").value= CurrentStartDate
	document.getElementById("hidden_CurrentAlternativeParentId").value= CurrentAlternativeParentId
	document.getElementById("name_index").value= DataArray['item'];
	document.getElementById("job_phase_id_index").value= DataArray['item'];
	document.getElementById("product_id_index").value= DataArray['product_id'];
	document.getElementById("vendor_id_index").value= DataArray['vendor_id'];
	document.getElementById("lead_time_index").value= DataArray['lead_time'];
	document.getElementById("description_index").value= DataArray['description'];
	document.getElementById("ccount_index").value= DataArray['ccount'];
	document.getElementById("employee_id_index").value= DataArray['employee_id'];
	document.getElementById("duration_index").value= DataArray['duration'];
	document.getElementById("notes_index").value= DataArray['notes'];
	document.getElementById("sales_index").value= DataArray['sales'];
	document.getElementById("cost_index").value= DataArray['cost'];
	document.getElementById("materials_index").value= DataArray['materials'];
	document.getElementById("equipment_index").value= DataArray['equipment'];
	document.getElementById("other_index").value= DataArray['other'];
	document.getElementById("subcontract_index").value= DataArray['subcontract'];
	document.getElementById("labor_index").value= DataArray['labor'];
	document.getElementById("hrs_index").value= DataArray['hrs'];
	document.getElementById("rate_index").value= DataArray['rate'];
	document.getElementById("allowance_index").checked= DataArray['allowance'];
	document.getElementById("selection_item_index").checked= DataArray['selection_item'];
	document.getElementById("option_index").checked= DataArray['option'];
  if (document.getElementById("option_index").checked) {
    document.getElementById("li_item_is_option").style.display= "inline"
  } else {
    document.getElementById("li_item_is_option").style.display= "none"
  }

	document.getElementById("private_memo_index").checked= DataArray['private_memo'];
	document.getElementById("task_follows_id_index").value= DataArray['task_follows_id'];

	document.getElementById("flag_index").checked= DataArray['flag'];
	document.getElementById("flag_text_index").value= DataArray['flag_text'];
	AddAlternativeItemBox= false
	LoadTaskFollowsIds("onLoadedProjectItemInfoByInfoIndex")
	CurrentParentId= ''
	CurrentStartDate= '';
  CurrentAlternativeParentId= '';
	setFieldsDisabled(subitems_count);
  LoadAlternativeItems()
  document.getElementById("item_info_div").style.display= "block"
	document.getElementById("div_loadingAnimation").style.display= "none"
}

function setFieldsDisabled(subitems_count) {
  if ( subitems_count > 0 ) {
    document.getElementById("product_id_index").disabled= true;
    document.getElementById("edit_btn").disabled= true;
    document.getElementById("vendor_id_index").disabled= true;
    document.getElementById("employee_id_index").disabled= true;
    document.getElementById("ccount_index").disabled= true;
    document.getElementById("lead_time_index").disabled= true;
    document.getElementById("duration_index").disabled= true;
    document.getElementById("notes_index").disabled= true;
    document.getElementById("sales_index").disabled= true;
    document.getElementById("cost_index").disabled= true;
    document.getElementById("materials_index").disabled= true;
    document.getElementById("equipment_index").disabled= true;
    document.getElementById("other_index").disabled= true;
    document.getElementById("subcontract_index").disabled= true;
    document.getElementById("labor_index").disabled= true;
    document.getElementById("hrs_index").disabled= true;
    document.getElementById("rate_index").disabled= true;
  } else {
    document.getElementById("product_id_index").disabled= false;
    document.getElementById("edit_btn").disabled= false;
    document.getElementById("vendor_id_index").disabled= false;
    document.getElementById("employee_id_index").disabled= false;
    document.getElementById("ccount_index").disabled= false;
    document.getElementById("lead_time_index").disabled= false;
    document.getElementById("duration_index").disabled= false;
    document.getElementById("notes_index").disabled= false;
    document.getElementById("sales_index").disabled= false;
    document.getElementById("cost_index").disabled= false;
    document.getElementById("materials_index").disabled= false;
    document.getElementById("equipment_index").disabled= false;
    document.getElementById("other_index").disabled= false;
    document.getElementById("subcontract_index").disabled= false;
    document.getElementById("labor_index").disabled= false;
    document.getElementById("hrs_index").disabled= false;
    document.getElementById("rate_index").disabled= false;
  }
}

function LoadTaskFollowsIds(Source) {
	var HRef= '<?php echo url_for('@admin_load_project_item_infos_list_by_project_id?project_id='.$project_id ) ?>/make_indent/1/sorting/START_DATE' ;// TASK_FOLLOWS_ID
	$.getJSON(HRef,   {  },
	onLoadedTaskFollowsIds,
	function(x,y,z) {   //Some sort of error
		alert(x.responseText);
	}
	);
}

function onLoadedTaskFollowsIds( data ) {
	var ErrorCode=data.ErrorCode
	if ( ErrorCode > 0 ) {
		alert( data["ErrorMessage"] )
		return;
	}
	var DataLength=data.DataLength
	var ErrorCode=data.ErrorCode
	var TaskFollowsArray= Array();
	for( var I= 0; I< DataLength; I++ ){
		//ItemsIndexesArray[ I ]= parseInt(data.Data[I].item_index)
		TaskFollowsArray[ I ]= parseInt(data.Data[I].task_follows_id)
	}

  ClearDDLBItems( "task_follows_id_index", false )

  var CurrentItemValue= ''

  for( JJJ= 0; JJJ< DataLength; JJJ++ ) {
		if ( data.Data[JJJ].item_index!= CurrentItemIndex ) {
		  AddDDLBItem( "task_follows_id_index", data.Data[JJJ].id, data.Data[JJJ].item )
		} else {
		  CurrentItemValue= TaskFollowsArray[JJJ]
		}
	}
	SetDDLBActiveItem( "task_follows_id_index", CurrentItemValue )
}



function ShowTextareaProjectInfo() {
	var textarea_project_info = document.getElementById("hidden_textarea_project_info").value;
	if ( textarea_project_info == "" ) textarea_project_info= " "
	var HRef= '<?php echo url_for('@admin_estimate_project_cvs_import?project_id='.$project_id ) ?>'
	tb_show( "Scope of Work", HRef/*, "/images/help.png"*/ );
}


function getFlaggedItemInfos( ProjectItemInfosData, ProjectItemInfosDataLength ) {
	var ResArray= Array();
  for( var I= 0; I< ProjectItemInfosDataLength; I++ ) {
    try {
      if ( ProjectItemInfosData[I].flag == 1 ) {
    	  ResArray[ ResArray.length ]= ProjectItemInfosData[I].item_index
      }
    }
    catch(e){}
  }
  return ResArray;
}

function onchangeOptionIndex() {
  IsChanged= true
  if (document.getElementById("option_index").checked) {
    document.getElementById("li_item_is_option").style.display= "inline"
  } else {
    document.getElementById("li_item_is_option").style.display= "none"
  }
}


//-->
</script>
<script type="text/javascript">
  window.onload=function()
  {
  var elem = document.getElementById("wrapper");
  elem.style.minWidth = "1663px";
  }
</script>


    <div id="gray_background_div" >
      <div id="content_div">
        <div class="title">
          <p>ESTIMATE FORM</p>
        </div>

        <form id="estimate_form" name="estimate_form" method="POST" action="" enctype="multipart/form-data" >
<input type="hidden" value="<?php echo ( !empty($ProjectInfo) ? $ProjectInfo->getInfo() : "") ?>" id="hidden_textarea_project_info" name="hidden_textarea_project_info">
<input type="hidden" name="action_type" id="action_type" value="" >
<?php echo $form['_csrf_token']->render()?>

<div id="field0_div" style="padding-bottom:85px !important;">
          <?php $Customer= $Project->getCustomer();
          include_partial( 'estimate_form_info', array( 'Project'=>$Project, 'Customer'=>$Customer, 'HostForImage'=> $HostForImage ) ) ?>

          <?php include_partial( 'estimate_form_top_buttons', array( 'Project'=>$Project, 'project_id'=>$project_id, 'Customer'=>$Customer, 'form' => $form, 'HostForImage'=> $HostForImage ) ) ?>
 </div>

          <div id="field1_div" style="padding-bottom:80px !important; margin-top:10px !important;">
          <?php include_partial( 'estimate_form_chart', array( 'Project'=>$Project, 'Customer'=>$Customer, 'HostForImage'=> $HostForImage ) ) ?>

          <input type="hidden" id="hidden_show_CurrentItemIndex" value="" >
          <input type="hidden" id="hidden_show_CurrentParentId" value="" >
          <input type="hidden" id="hidden_show_CurrentStartDate" value="" >
          <input type="hidden" id="hidden_CurrentAlternativeParentId" value="" >

          <?php include_partial( 'project_item_info_by_item_index2', array( 'Project'=>$Project, 'Customer'=>$Customer, 'RateValue'=> $RateValue, 'HostForImage'=> $HostForImage ) ) ?>

          </div><!-- close field1_div -->
        </form>


      </div><!-- close content_div -->
    </div><!-- close gray_background_div -->