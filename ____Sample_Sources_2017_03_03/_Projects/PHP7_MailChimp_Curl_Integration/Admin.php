<?php
include_once (APPPATH."libraries/AppMailchimp.php");
class Mailchimp extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('muser', '', true);
        $this->load->model('mdoc', '', true);
        $this->load->model('mdoc_type', '', true);
        $this->load->model('muser_bank_account', '', true);
    }


	/* MAILCHIMP FOLDERS BLOCK START */
	public function get_mailchimp_folders()
	{
		$uriParameters = $this->uri->uri_to_assoc(4);
		$return_json_data = appUtils::getParameter($this, $uriParameters, array(), 'return_json_data');
		$current_mailchimp_folder_id = appUtils::getParameter($this, $uriParameters, array(), 'current_mailchimp_folder_id');
		$appMailchimp= new appMailchimp();
		$mailchimpFoldersList= $appMailchimp->getFoldersList();
		if ( !empty($return_json_data) ) {
			$this->output->set_content_type( 'application/json')->set_output(json_encode(array('error_message' => '', 'error_code' => 0, 'mailchimpFoldersList'=> $mailchimpFoldersList, 'mailchimp_folders_lists_count'=> count($mailchimpFoldersList)) ) );
			return;
		}
		$html= $this->showTwigTemplate('common/select_mailchimp_folders', ['mailchimpFoldersList'=> $mailchimpFoldersList, 'current_mailchimp_folder_id'=> $current_mailchimp_folder_id], true, false, false);
		$this->output->set_content_type('application/json')->set_output( json_encode(array('error_message' => '', 'error_code' => 0, 'html' => $html )) );

	} // public function get_mailchimp_folders()

	...

	/* MAILCHIMP FOLDERS BLOCK END */


	/* MAILCHIMP FILES BLOCK START */

	...

	public function save_new_mailchimp_file()
	{
		$uriParameters = $this->uri->uri_to_assoc(4);
		$doc_type_id = appUtils::getParameter($this, $uriParameters, $this->m_postArray, 'doc_type_id');
		$doc_id = appUtils::getParameter($this, $uriParameters, $this->m_postArray, 'doc_id');
		$filename = appUtils::getParameter($this, $uriParameters, $this->m_postArray, 'filename');

		$doc_dir= $this->mdoc->getDocDir($doc_id);

		$filename_path= $doc_dir . $filename;
		if ( !file_exists($filename_path) ) {
			$this->output->set_content_type( 'application/json')->set_output( json_encode( array( 'error_message' => "File '".($filename_path)."' not found", 'error_code' => 1) ) );
			return;
		}
		$filename_content= appUtils::readFileAsText($filename_path);
		$docType= $this->mdoc_type->getRowById($doc_type_id);
		$mailchimp_folder_id= '';
		if ( !empty($docType['mailchimp_folder_id']) ) {
			$mailchimp_folder_id = $docType['mailchimp_folder_id'];
		}
		$appMailchimp= new appMailchimp();
		$newMailChimpFile=$appMailchimp->addFile($filename, $filename_content, $mailchimp_folder_id);

		if ( !empty($newMailChimpFile['error_code']) ) {
			$this->output->set_content_type( 'application/json')->set_output( json_encode( array( 'error_message' => $newMailChimpFile['error_message'], 'error_code' => $newMailChimpFile['error_code']) ) );
			return;
		}

		if ( empty($newMailChimpFile['error_code']) ) {
			$this->output->set_content_type( 'application/json')->set_output( json_encode( array( 'error_message' => '', 'error_code' => 0, 'new_file_id'=> $newMailChimpFile['new_file_id']) ) );
			return;
		}
	} // public function save_new_mailchimp_file()

	...

	/* MAILCHIMP TEMPLATES BLOCK START */


	public function delete_mailchimp_template()
	{
		$uriParameters = $this->uri->uri_to_assoc(4);
		$template_id = appUtils::getParameter($this, $uriParameters, array(), 'template_id');
		$appMailchimp= new appMailchimp();
		$deletedMailChimpTemplate=$appMailchimp->deleteTemplate($template_id);
		if ( !empty($deletedMailChimpTemplate['error_code']) ) {
			$this->output->set_content_type( 'application/json')->set_output( json_encode( array( 'error_message' => $deletedMailChimpTemplate['error_message'], 'error_code' => $deletedMailChimpTemplate['error_code']) ) );
			return;
		}

		if ( empty($deletedMailChimpTemplate['error_code']) ) {
			$this->output->set_content_type( 'application/json')->set_output( json_encode( array( 'error_message' => '', 'error_code' => 0, 'template_id'=> $deletedMailChimpTemplate['template_id']) ) );
			return;
		}
	} // public function delete_mailchimp_template()

	...

	/* MAILCHIMP TEMPLATES BLOCK END */


}