<?php
class appMailchimp {
	protected $m_ci;
	protected $m_User;
	protected $m_user_id;
	protected $m_category_id;
	protected $m_mailchimp_api_key;
	protected $m_timeout;
	protected $m_base_url;
	protected $m_default_folders_count= 9999;

	public function __construct() {
		$this->m_ci = &get_instance();
		$this->m_ci->load->model('mcategory', '', true);
		$this->m_timeout= 10;

		$this->m_mailchimp_api_key= !empty($this->m_ci->m_appConfig['mailchimp_api_key']) ? $this->m_ci->m_appConfig['mailchimp_api_key'] : '';

		$mailchimp_center = substr( $this->m_mailchimp_api_key, strpos( $this->m_mailchimp_api_key, '-' ) + 1 );
		$this->m_base_url= 'https://' . $mailchimp_center . '.api.mailchimp.com/3.0/';
	}

	public function setMailchimpApiKey( string $p_mailchimp_api_key ) {
		$this->m_mailchimp_api_key= $p_mailchimp_api_key;
	}

	public function setTimeout( int $p_timeout ) {
		$this->m_timeout= $p_timeout;
	}

	public function setDefaultFoldersCount( int $p_default_folders_count ) {
		$this->m_default_folders_count= $p_default_folders_count;
	}

	public function setUserId( int $p_user_id ) {
		$this->m_user_id= $p_user_id;
		$this->m_User= $this->muser->getRowById($p_user_id);
	}


	public function setUser( array $p_User ) {
		$this->m_User= $p_User;
		$this->m_user_id= $p_User['id'];
	}

	public function setCategoryId( int $p_category_id ) {
		$this->m_category_id= $p_category_id;
	}

	public function subscribeCategory( string $set_status ) : array {
		$validatedErrors= $this->hasValidatedErrors( [ 'user_email'=>true, 'category_id'=> true ] );
		if( $validatedErrors['error_code'] ) return $validatedErrors;
		$Category = $validatedErrors['Category'];

		$mailchimp_list_id   = $Category['mailchimp_list_id'];
		$mailchimp_list_name = $Category['name'];

		$mailchimp_user_id         = md5( strtolower( $this->m_User['email'] ) );
		$url = $this->m_base_url .'lists/' . $mailchimp_list_id . '/members/' . $mailchimp_user_id ;

		...
		if ( !empty($curlReturnedData->errors) and is_array($curlReturnedData->errors) ) { // there are errors
			$errors_text= $this->getErrorMessageText($curlReturnedData);
			return [ 'error_code' => ( !empty($curlReturnedData->status) ? $curlReturnedData->status : 1 ), 'error_message' => appUtils::trimRightSubString($errors_text,', ') ];
		} // if ( !empty($curlReturnedData->errors) and is_array($curlReturnedData->errors) ) {  there are errors

		if ( $http_code == 200 ) {
			return['error_code'=> 0, 'error_message' => "", 'success_message' => "You have successfully ".$set_status." to '" . $this->m_ci->m_appConfig['site_name'] . ", list '" . $mailchimp_list_name."' !" ];
		} else {
			switch ( $http_code ) {
				case 214:
					return [ 'error_code' => $http_code, '214:error_message' => " Error with code " . $http_code . $error_details . ' ! Check api key and List ID !' ];
				default:
					return [ 'error_code' => $http_code, $http_code.':error_message' => " Error with code " . $http_code . $error_details . ' ! Check api key and List ID !' ];
			}
		}

		return [ 'error_code' => 99, '99:error_message' => " Unknown Error ! ".$error_details." Check api key and List ID !" ];
	} // public function subscribeCategory( $set_status ) : array { // subscribe/unsubscribing Category/List


	/* MAILCHIMP LISTS API BLOCK START */

	public function getListsInfo() : array {
		$validatedErrors= $this->hasValidatedErrors();
		if( $validatedErrors['error_code'] ) return $validatedErrors;

		$url = $this->m_base_url .'lists';
		$ch= $this->initCurl($url,'GET');
		$curlReturnedData   = json_decode( curl_exec( $ch ) );
		if ( !empty($curlReturnedData->errors) and is_array($curlReturnedData->errors) ) { // there are errors
			$errors_text= $this->getErrorMessageText($curlReturnedData);
			return [ 'error_code' => ( !empty($curlReturnedData->status) ? $curlReturnedData->status : 1 ), 'error_message' => appUtils::trimRightSubString($errors_text,', ') ];
		} // if ( !empty($curlReturnedData->errors) and is_array($curlReturnedData->errors) ) { // there are errors
		$retArray= [];
		...
		return $retArray;
	}  // 	public function getListsInfo(  ) : array {


	public function getListMembers(string $list_id) : array {
		$validatedErrors= $this->hasValidatedErrors();
		if( $validatedErrors['error_code'] ) return $validatedErrors;
		...
		if ( !empty($curlReturnedData->errors) and is_array($curlReturnedData->errors) ) { // there are errors
			$errors_text= $this->getErrorMessageText($curlReturnedData);
			return [ 'error_code' => ( !empty($curlReturnedData->status) ? $curlReturnedData->status : 1 ), 'error_message' => appUtils::trimRightSubString($errors_text,', ') ];
		} // if ( !empty($curlReturnedData->errors) and is_array($curlReturnedData->errors) ) { // there are errors
		$retArray= [];
		if( !empty($curlReturnedData->members) ) {
			...
		}
		return $retArray;
	}  // 	public function getListMembers($list_id):array {

	/* MAILCHIMP LISTS API BLOCK END */



	/* MAILCHIMP TEMPLATES API BLOCK START */
	public function addTemplate( string $template_name, int $mailchimp_template_folder_id, string $content ) : array {
		$validatedErrors= $this->hasValidatedErrors();
		if( $validatedErrors['error_code'] ) return $validatedErrors;
		$url = $this->m_base_url .'templates' ;

		$templateInfo  = [
			'name' => $template_name,
			'folder_id'=> $mailchimp_template_folder_id,
			"html" => $content
		];
		...
		if ( !empty($curlReturnedData->errors) and is_array($curlReturnedData->errors) ) { // there are errors
			...
		} // if ( !empty($curlReturnedData->errors) and is_array($curlReturnedData->errors) ) { // there are errors
		...
	}  // public function addTemplate( $template_name ) : array { // add Template

	/* MAILCHIMP FOLDERS API BLOCK START */

	public function deleteFolder(int $folder_id ) : array {
		$validatedErrors= $this->hasValidatedErrors();
		...
		if ( !empty($curlReturnedData->status)   ) { // there are errors
			$errors_text= ( !empty($curlReturnedData->title) ? $curlReturnedData->title.", " : "" ) . ( !empty($curlReturnedData->detail) ? $curlReturnedData->detail.", " : "" );
			return [ 'error_code' => ( !empty($curlReturnedData->status) ? $curlReturnedData->status : 1 ), 'error_message' => appUtils::trimRightSubString($errors_text,', ') ];
		} // if ( !empty($curlReturnedData->errors) and is_array($curlReturnedData->errors) ) { // there are errors

		return['error_code'=> 0, 'error_message' => "", 'folder_id' => $folder_id ];
	}  // public function deleteFolder( $folder_id ) :
	/* MAILCHIMP FOLDERS API BLOCK START */


	/* MAILCHIMP FILES API BLOCK START */


	public function updateFile( int $file_id, $filename, string $filename_content, int $mailchimp_folder_id= 0 ) : array {

		$deletedMailChimpFile=$this->deleteFile($file_id);
		if ( !empty($deletedMailChimpFile['error_code']) ) return $deletedMailChimpFile;

		$newMailChimpFile= $this->addFile($filename, $filename_content, $mailchimp_folder_id);
		...
	}  // public function updateFile( int $file_id ) :



	/* MAILCHIMP FILES API BLOCK START */


	private function hasCurlOn() : bool {
		return function_exists('curl_version');
	}

	private function getErrorMessageText($returnData) : string {
		$errors_text= '';
		$errors_text= appUtils::trimRightSubString($errors_text,', ');
		...
		return $errors_text;
	}

	private function setParametersString(array $parametersList, bool $is_first_parameter= false) : string {
		if ( empty($parametersList) or !is_array($parametersList)) return '';
		$ret_str= '';
		$is_first_in_circle= true;
		foreach( $parametersList as $next_key=>$next_value ) {
			if ( $is_first_in_circle ) {
				$ret_str.= ( $is_first_parameter ? '?' : '&' ) . $next_key.'='.$next_value;
			} else {
				$ret_str.= '&' . $next_key.'='.$next_value;
			}
		}
		return $ret_str;
	}

	private function initCurl(string $url, string $request_type, string $src= '') {

		$ch = curl_init( trim($url) );
		curl_setopt( $ch, CURLOPT_USERPWD, 'user:' . $this->m_mailchimp_api_key );
		curl_setopt( $ch, CURLOPT_HTTPHEADER, [ 'Content-Type: application/json' ] );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_TIMEOUT, $this->m_timeout );
		curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, $request_type );
		curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
		return $ch;
	}

	public function hasValidatedErrors( array $makeChecks = [] ) : array {
		if ( empty( $this->m_mailchimp_api_key ) ) {
			...
		}
		if ( ! $this->hasCurlOn() ) {
			return [ 'error_code' => 3, 'error_message' => "3:Curl is not enabled !" ];
		}
		if ( in_array('user_email', $makeChecks) ) {
			...
		}

		$Category= '';
		if ( in_array('category_id', $makeChecks) ) {
			...
		}
		return [ 'error_code' => 0, 'error_message' => "", 'Category'=> $Category ];
	} // public function preValidate( $makeChecks ) : array {
}
