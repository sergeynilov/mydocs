
That is a integration from php7 application into MailChimp.
I had tasks to upload some data(files) and make subscribe/unsubscribe of users into lists in
php7/codeigniter3/jquery/bootstrap hosting application to MailChimp by user's account using MailChimp API and curl library.

1) Hosting application had category table with 2 additive fields : mailchimp_list_id varchar(20) NULL and is_mailchimp_active boolean, first hold ID of MailChimp list item and the second if this category was used for MailChimp subscription.

In category editor page there is popup MailChimp settings page
http://imgur.com/a/KJx2a
where there is listing of all lists in MailChimp and which assigned to current category is checked 1).

Any MailChimp list can be delete from Hosting application 2)
From Hosting application MailChimp list can be created 3).
Selecting MailChimp list 4) its ID would be assign to mailchimp_list_id field and so Hosting application category would be assigned with MailChimp list 5).

Also In listing of all lists
http://imgur.com/a/DaeQZ  1) for selected list 2) list of members can be visible with marks if the member is in users of Hosting application.


2) For logged user all categories checked as is_mailchimp_active = true (point above) has page 
http://imgur.com/a/xicFV  with list 1)
where they can subscribe 2) or unsubscribe 3) to MailChimp list related with categories.
Sum for any subscribed category 4) is read from Hosting application settings
Also User can subscribe to blog articles 5).
Common sum 6) is calculated from all subscribed elements based on prices in Hosting application settings


3) Hosting application had a lot of documents(prices, media, images ... ) which must be uploaded to MailChimp
Hosting application had document type table/listing which was related with File Folders of MailChimp by mailchimp_folder_id field.
In document type editor page there is popup MailChimp settings page
http://imgur.com/a/sd7oN
where there is listing of all folders in MailChimp and which assigned to current document type is checked 1).
Any MailChimp folder can be delete from Hosting application 2)
From Hosting application MailChimp folder can be created 3).
Selecting MailChimp folder 4) its ID would be assign to mailchimp_folder_id field and so Hosting application document type would be assigned with MailChimp folder 5).


4) In any similar way http://imgur.com/a/ei85L any document can be added/deleted/updated to mailchimp files under current document type( mailchimp files ).


5) Hosting application has Rich text Content editor so pages any document can be added/deleted/updated to mailchimp files
 http://imgur.com/a/HNa2X  under definite mailchimp template folder.


6) Next Hosting application settings
http://imgur.com/a/A1L5o
are used for MailChimp integration.


7) Also look at /_Projects/PHP7_MailChimp_Curl_Integration/Admin.php - json methods for
MailChimp data manipulation.


8) Also look at  /_Projects/PHP7_MailChimp_Curl_Integration/AppMailchimp.php - object with all methods working with MailChimp using curl.