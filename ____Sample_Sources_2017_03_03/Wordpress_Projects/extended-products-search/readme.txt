
At this link
http://dev2.softreactor.com/wp-plugings/extended-products-search
you can see demo of Extended products search with data(users, attributes, products with images, comments...).
This form is based NSN_WooExtSearch plugin, which is made by me.


1) The plugin has many options in admin's part, like :
  Common settings.
  How group prices.
http://imgur.com/a/vypfM
  Which data for search are visible in left boxes(Prices, Tags, Rating, Post Title etc...).
  Which attributes would be used for search.
  tp://imgur.com/a/kyK4B
http://imgur.com/a/cIo8Z
  Which fields would be shown in Listing of found products.
http://imgur.com/a/Dhz5r

2) Near with checkboxes there are names of attributes, categories, prices made as links and clicking on them search would be run at once. 
But also user can click on checkboxes and on "Search" button after that. So several parameters can be selected in different boxes.

3) In admin part for attributes it is possible to set for any attribute post article, which would union all products with this attribute.
In resulting products Listing for any product some attributes(in this demo data "brand") that is link to post article describing this attribute(in this demo data "brand");
Examples for Brand: MSI - it would be a link with  MSI description.
Below of this article there are links to product of this brand.

4) Under login( for this demo data under Subscriber_1  111111 ), in  "Filter Controls" box would be visible settings to save filters(not resulting products) and to make repeating search on saved filters.
