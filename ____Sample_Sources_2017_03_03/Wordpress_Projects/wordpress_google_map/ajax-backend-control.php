<?php
define('DOING_AJAX', true);
require_once( '../../../wp-load.php');
if (!defined('ArtistsSongs')) {
    exit; // Exit if accessed directly
}
require_once( 'appArtistsSongsControl.php'); // parent common control class file


class ajaxBackendControl extends appArtistsSongsControl  // extends parent common control class
{
    public function __construct($containerClass, $wpdb)
    {
        global $session;
        parent::__construct($containerClass, $wpdb, $session);
        $this->setValidationRules(array());
        require_once($this->m_pluginDir . 'models/songs.php'); // Used db model files
        require_once($this->m_pluginDir . 'models/artists.php');
        require_once($this->m_pluginDir . 'models/songs-artists.php');
        require_once($this->m_pluginDir . 'models/songs-jenres.php');
        $this->isDebug = false;
    }

    protected function readRequestParameters()
    {
        $this->m_action = $this->getParameter('action');
        if (isset($_GET['action']) and $_GET['action'] == 'upload-artist-photo') {
            $this->m_action = 'upload-artist-photo';
        }
    } // protected function readRequestParameters() {

    public function main() // entrance point
    {
        parent::main();
        $action = $this->getParameter('action');
        $m_parentControl = $this;

        if ($this->m_is_trimDataBeforeSave) {
            foreach ($_REQUEST as $nextKey => $nextValue) {
                $_REQUEST[$nextKey] = trim($nextValue);
            }
        }

        /* ARTIST'S CONCERT START BLOCK */
        if ($this->m_action == 'get-artist-concerts') { // get list of concerts by artist_id
            require_once($this->m_pluginDir . 'models/artist-concerts.php');
            require_once($this->m_pluginDir . 'models/artist-tour.php');
            require_once($this->m_pluginDir . 'models/posts.php');
            $postsTblSource = new Posts($this->m_wpdb);
            $currentBackendLang= nsnClass_appLangs::getCurrentBackendLanguage(2, __FILE__.' : '.__LINE__ );

            $artist_id = $this->getParameter('artist_id');
            if (empty($artist_id)) {
                echo json_encode(array('error_code' => 1, 'error_message' => nsnClass_nsnClass_appFuncs::addLog('"artist_id" parameter must be specified!', __FILE__ . ', ' . __LINE__, 'S', $this->m_wpdb), 'content' => ''));
                return;
            }
            $filter_artist_tour = $this->getParameter('filter_artist_tour');  // set filters for data request
            $search_in_description = $this->getParameter('search_in_description');
            $filter = $this->getParameter('filter');
            $sort = 'tour_id, event_date';
            $sort_direction = 'asc';

            $ArtistsConcertsTblSource = new ArtistsConcerts($this->m_wpdb); // create ArtistsConcerts class of model
            $artistsConcertsList = $ArtistsConcertsTblSource->getArtistsConcertsList(false, "" , array('artist_id' => $artist_id, 'show_user_display_name' => 1, 'filter' => $filter, 'tour_id'=> $filter_artist_tour,  'search_in_description'=> $search_in_description, 'filter'=> $filter ), $sort, $sort_direction); // retrieve list of data by given parameters

            $ArtistsToursTblSource = new ArtistTours($this->m_wpdb);     // create ArtistTours class of model
            $artistToursList= $ArtistsToursTblSource->getArtistToursList();
            foreach( $artistToursList as $nextKey=>$nextArtistTour ) {
                $concertsSubList= array();
                foreach( $artistsConcertsList as $nextArtistsConcert ) {
                    if ($nextArtistTour['ID'] == $nextArtistsConcert['tour_id']) {
                        $concertsSubList[]= array( 'tour_id'=> $nextArtistsConcert['tour_id'],  'ID'=> $nextArtistsConcert['ID'], 'country'=> $nextArtistsConcert['country'], 'event_date'=> $nextArtistsConcert['event_date'] );
                    }
                }
                uasort($concertsSubList,'cmpTourConcerts'); // sorted list of Tour Concerts by event datetime
                $artistToursList[$nextKey]['concertsSubList']= $concertsSubList;
                $artistToursList[$nextKey]['concertsSubListCount']= count($concertsSubList);
            }

            reset($artistsConcertsList);
            $now= time();
            foreach( $artistsConcertsList as $nextKey=>$artistsConcert ) { // Prepare list of data with type of event(Past, today, next days, future... and convinient format of datetime of event date...)
                $artistsConcertsList[$nextKey]['place_name']= stripslashes( $artistsConcertsList[$nextKey]['place_name'] );
                $artistsConcertsList[$nextKey]['description']= stripslashes( $artistsConcertsList[$nextKey]['description'] );
                $event_date= strtotime($artistsConcert['event_date']);
                $artistsConcertsList[$nextKey]['eventDateYear']= strftime('%Y',$event_date);
                $artistsConcertsList[$nextKey]['eventDateMonth']= strftime('%m',$event_date);
                $artistsConcertsList[$nextKey]['eventDateDay']= strftime('%d',$event_date);
                $artistsConcertsList[$nextKey]['eventDateHour']= strftime('%H',$event_date);
                $artistsConcertsList[$nextKey]['eventDateMinute']= strftime('%M',$event_date);
                $compareDatesByDay= nsnClass_appFuncs::compareDatesByDay( $event_date, $now );
                if ( $compareDatesByDay == 0 ) { // that is today
                    $artistsConcertsList[$nextKey]['futureDate']= true;
                    $artistsConcertsList[$nextKey]['todayEvent']= true;
                    $artistsConcertsList[$nextKey]['nearestEvent']= true;
                }
                if ( $compareDatesByDay == 1 ) { // that is future day
                    $artistsConcertsList[$nextKey]['futureDate']= true;
                    $artistsConcertsList[$nextKey]['todayEvent']= false;

                    $datesDifference= nsnClass_appFuncs::getDatesDifference( $event_date, $now );
                    $artistsConcertsList[$nextKey]['nearestEvent']= false;
                    if ($datesDifference > 0 and $datesDifference <= 2) { // TODO
                        $artistsConcertsList[$nextKey]['nearestEvent']= true;
                    }
                }
                if ( $compareDatesByDay == -1 ) { // that is past day
                    $artistsConcertsList[$nextKey]['futureDate']= false;
                    $artistsConcertsList[$nextKey]['todayEvent']= false;
                    $artistsConcertsList[$nextKey]['nearestEvent']= false;
                }
                $relatedConcertPost= $postsTblSource->getRelatedPostByMetaValue( $artistsConcertsList[$nextKey]['ID'], 'concert' , $currentBackendLang, false );
                $relatePostPermalink= '';
                $relatePostTitle= '';
                $artistsConcertsList[$nextKey]['relatedConcertPostId']= '';
                if ( !empty($relatedConcertPost) ) {
                    $relatePostPermalink= get_permalink($relatedConcertPost->ID);
                    $relatePostTitle= $relatedConcertPost->post_title;
                    $artistsConcertsList[$nextKey]['relatedConcertPostId']= $relatedConcertPost->ID;
                }
                $artistsConcertsList[$nextKey]['relatePostPermalink']= $relatePostPermalink;
                $artistsConcertsList[$nextKey]['relatePostTitle']= $relatePostTitle;
            }
            echo json_encode(array('error_code' => 0, 'error_message' => '', 'artistsConcertsList' => $artistsConcertsList, 'artistsConcertsCount' => count($artistsConcertsList), 'artistToursList'=> $artistToursList, 'artistToursListCount'=> count($artistToursList) ));
            exit;
        } // if ( $this->m_action == 'get-artist-concerts' ) {  // get list of concerts by artist_id


        if ($this->m_action == 'update-artist-concert') {  // add/modify concert of artist
            if (empty($_REQUEST['artist_id']) or empty($_REQUEST['place_name']) or empty($_REQUEST['lat']) or empty($_REQUEST['lng']) or empty($_REQUEST['description']) or empty($_REQUEST['event_date'])) {
                echo json_encode(array('error_code' => 1, 'error_message' => nsnClass_appFuncs::addLog('"artist_id", "place_name", "lat", "lng", "description" and "event_date" parameters must be specified!', __FILE__ . ', ' . __LINE__, 'S', $this->m_wpdb), 'content' => ''));
                return;
            }
            require_once($this->m_pluginDir . 'models/artist-concerts.php');
            $ArtistsConcertsTblSource = new ArtistsConcerts($this->m_wpdb); // create ArtistsConcerts class of model
            $errCode = $ArtistsConcertsTblSource->updateArtistsConcerts(!empty($_REQUEST['artist_concert_id']) ? $_REQUEST['artist_concert_id'] : '', array('artist_id' => $_REQUEST['artist_id'], 'place_name' => $_REQUEST['place_name'], 'lat' => $_REQUEST['lat'], 'lng' => $_REQUEST['lng'], 'description' => $_REQUEST['description'], 'event_date' => $_REQUEST['event_date'], 'participants' => $_REQUEST['participants'], 'country' => $_REQUEST['country']));  // add(if artist_concert_id is empty)/modify concert of artist

            if ( !empty($_REQUEST['concert_post_id']) ) { // concert can have related post - then add meta info for this link
                add_post_meta($_REQUEST['concert_post_id'], 'song_artist_item', 'concert', true);
                add_post_meta($_REQUEST['concert_post_id'], 'song_artist_item_id', $errCode, true);
                add_post_meta($_REQUEST['concert_post_id'], 'post_type', 'artists_songs', true);
                add_post_meta($_REQUEST['concert_post_id'], 'song artist description ' . $errCode . ' ...', true);
                add_post_meta($_REQUEST['concert_post_id'], 'song_artist_show_target', 'inline', true);
            }

            echo json_encode(array('error_code' => ((isset($errCode) and !is_numeric($errCode)) ? 1 : 0), 'error_message' => ( (isset($errCode) and is_numeric($errCode)) ? $errCode : '' ), 'artist_concert_id' => ( isset($_REQUEST['artist_concert_id']) ? $_REQUEST['artist_concert_id'] : "" ) ));
            exit;
        } // if ( $this->m_action == 'update-artist-concert' ) {   // add/modify concert of artist


        if ( $this->m_action == 'delete-artist-concert' ) { // delete artist concert by its ID
            if (empty($_REQUEST['artist_concert_id'])) {
                echo json_encode(array('error_code' => 1, 'error_message' => nsnClass_appFuncs::addLog('"artist_concert_id" parameter must be specified!', __FILE__ . ', ' . __LINE__, 'S', $this->m_wpdb), 'content' => ''));
                return;
            }
            require_once($this->m_pluginDir . 'models/artist-concerts.php');
            $ArtistsConcertsTblSource = new ArtistsConcerts($this->m_wpdb); // create ArtistsConcerts class of model
            $errCode= $ArtistsConcertsTblSource->deleteArtistsConcertsRow($_REQUEST['artist_concert_id']);
            if ( !empty($errCode) and is_numeric($errCode) ) {
                echo json_encode(array('error_code' => 1, 'error_message' => $errCode, 'artist_concert_id' => $_REQUEST['artist_concert_id']));
            } else {
                echo json_encode(array('error_code' => 0, 'error_message' => '', 'artist_concert_id' => $_REQUEST['artist_concert_id']));
            }
            exit;
        } // if ( $this->m_action == 'delete-artist-concert' ) {   // delete artist concert by its ID

        if ($_REQUEST['action'] == 'get-artist-concert-by-id') { // get concert by its ID
            require_once($this->m_pluginDir . 'models/artist-concerts.php');
            require_once($this->m_pluginDir . 'models/posts.php');
            $currentBackendLang= nsnClass_appLangs::getCurrentBackendLanguage(2, __FILE__.' : '.__LINE__ );
            $ArtistsConcertsTblSource = new ArtistsConcerts($this->m_wpdb); // create ArtistsConcerts class of model
            if (empty($_REQUEST['artist_concert_id'])) {
                echo json_encode(array('error_code' => 1, 'error_message' => nsnClass_appFuncs::addLog('"artist_concert_id" parameter must be specified!', __FILE__ . ', ' . __LINE__, 'S', $this->m_wpdb), 'content' => ''));
                return;
            }
            $postsTblSource = new Posts($this->m_wpdb);     // create Posts class of model
            $relatedConcertPost= $postsTblSource->getRelatedPostByMetaValue( $_REQUEST['artist_concert_id'], 'concert' , $currentBackendLang, false );
            // find first post/post_id by given meta value and type of Meta Value and lang  - post related for this concert

            $relatedConcertPostId= '';
            if ( !empty($relatedConcertPost) ) {
                $relatedConcertPostId = $relatedConcertPost->ID;
            }
            $with_inappropriates_count= isset($_REQUEST['with_inappropriates_count']) ? $_REQUEST['with_inappropriates_count'] : '';
            $show_artist_display_name= isset($_REQUEST['show_artist_display_name']) ? $_REQUEST['show_artist_display_name'] : '';

            $artistConcert = $ArtistsConcertsTblSource->getRowById($_REQUEST['artist_concert_id'], array('with_inappropriates_count' => $with_inappropriates_count, 'show_artist_display_name'=>$show_artist_display_name)); // retrieve concert by its ID with related info(if keys are true)
            $artistConcert['place_name']= stripslashes( $artistConcert['place_name'] );
            $artistConcert['description']= stripslashes( $artistConcert['description'] );
            $artistConcert['concert_post_id']= $relatedConcertPostId;
            $artistConcert['created'] = viewFuncs::formattedDateTime($artistConcert['created'], 'Common');
            echo json_encode(array('error_code' => 0, 'error_message' => '', 'artist_concert_id' => $_REQUEST['artist_concert_id'], 'artistConcert' => $artistConcert));
            exit;
        } // if ( $_REQUEST['action'] == 'get-artist-concert-by-id' ) {  // get concert by its ID

        if ($_REQUEST['action'] == 'autocomplete-events') { // get list of concerts by artist_id in autocomplete format
            require_once($this->m_pluginDir . 'models/artist-concerts.php');
            $artist_id = $this->getParameter('artist_id');
            $q = $this->getParameter('q');
            $search_in_description = $this->getParameter('search_in_description');
            $search_in_past_events = $this->getParameter('search_in_past_events');
            if (empty($artist_id) or !isset($q)) {
                echo json_encode(array('error_code' => 1, 'error_message' => nsnClass_appFuncs::addLog('"artist_id" and "q" parameters must be specified!', __FILE__ . ', ' . __LINE__, 'S', $this->m_wpdb), 'content' => ''));
                return;
            }
            $sort = $this->getParameter('sort');
            $sort_direction = $this->getParameter('sort_direction');

            $ArtistsConcertsTblSource = new ArtistsConcerts($this->m_wpdb); // create ArtistsConcerts class of model
            $artistsConcertsList = $ArtistsConcertsTblSource->getArtistsConcertsList(false, "" , array('artist_id' => $artist_id, 'place_name' => $q, 'search_in_past_events' => $search_in_past_events, 'search_in_description'=> $search_in_description ), $sort, $sort_direction); // retrieve list of data by given parameters
            $resArray=  array();
            foreach( $artistsConcertsList as $nextArtistsConcert ) {
                $resArray[]= array( 'place_name'=> $nextArtistsConcert['ID'] .' : ' . stripslashes($nextArtistsConcert['place_name']), 'lat'=> $nextArtistsConcert['lat'], 'lng'=> $nextArtistsConcert['lng'] );
            }
            echo json_encode($resArray);
            exit;
        } // if ( $_REQUEST['action'] == 'autocomplete-events' ) {  // get list of concerts by artist_id in autocomplete format
        /* ARTIST'S CONCERT END BLOCK */








    } // public function main() {



}

global $wpdb;
$ajaxBackendControlObj = new ajaxBackendControl(null, $wpdb); // ajax backend control class extends parent common control class
$ajaxBackendControlObj->main();  // entrance point


function cmpTourConcerts($a, $b) { // sorted list of Tour Concerts by event datetime
    if ($a['event_date'] == $b['event_date']) {
        return 0;
    }
    return ($a['event_date'] < $b['event_date']) ? -1 : 1;
}