var this_plugin_url = ''
var this_logged_user_id = ''
var this_m_pageUrl = ''

function googleMapStyles( Params ) {
    //alert( "googleMapStyles Params::"+var_dump(Params) )
    this_plugin_url = Params.plugin_url;
    this_logged_user_id = Params.logged_user_id;
    this_m_pageUrl = Params.m_pageUrl;
}

googleMapStyles.prototype.getMapStyleList = function () { // List of styles for selection
    var retList= [3];
    var item = []
    item['id'] = "Light";
    item['name'] = "Light Style";
    retList[0]= item

    var item = []
    item['id'] = "bluish";
    item['name'] = "Bluish Style";
    retList[1]= item

    var item = []
    item['id'] = "water";
    item['name'] = "Water Style";
    retList[2]= item
    return retList;
} // googleMapStyles.prototype.getMapSlayerList = function () {

googleMapStyles.prototype.getMapStyle= function(returnType) { // get Map Style by style
    var lightStyle = [
        {
            "featureType": "landscape",
            "stylers": [
                {
                    "hue": "#BEFF00"
                },
                {
                    "saturation": -35.599999999999994
                },
                {
                    "lightness": 132.20000000000002
                },
                {
                    "gamma": 1
                }
            ]
        },
        {
            "featureType": "road.highway",
            "stylers": [
                {
                    "hue": "#53FF00"
                },
                {
                    "saturation": 0
                },
                {
                    "lightness": -1.4210854715202004e-14
                },
                {
                    "gamma": 1
                }
            ]
        },
        {
            "featureType": "road.arterial",
            "stylers": [
                {
                    "hue": "#FBFF00"
                },
                {
                    "saturation": 0
                },
                {
                    "lightness": 0
                },
                {
                    "gamma": 1
                }
            ]
        },
        {
            "featureType": "road.local",
            "stylers": [
                {
                    "hue": "#00FFFD"
                },
                {
                    "saturation": 0
                },
                {
                    "lightness": 0
                },
                {
                    "gamma": 1
                }
            ]
        },
        {
            "featureType": "water",
            "stylers": [
                {
                    "hue": "#0078FF"
                },
                {
                    "saturation": 0
                },
                {
                    "lightness": 0
                },
                {
                    "gamma": 1
                }
            ]
        },
        {
            "featureType": "poi",
            "stylers": [
                {
                    "hue": "#9FFF00"
                },
                {
                    "saturation": 0
                },
                {
                    "lightness": 0
                },
                {
                    "gamma": 1
                }
            ]
        }
    ];

    var waterStyle = [
        {
            featureType: 'water',
            elementType: 'geometry.fill',
            stylers: [
                { color: '#adc9b8' }
            ]
        },{
            featureType: 'landscape.natural',
            elementType: 'all',
            stylers: [
                { hue: '#809f80' },
                { lightness: -35 }
            ]
        }
    ];

    var bluishStyle = [
        {
            stylers: [
                { hue: "#009999" },
                { saturation: -5 },
                { lightness: -40 }
            ]
        },{
            featureType: "road",
            elementType: "geometry",
            stylers: [
                { lightness: 100 },
                { visibility: "simplified" }
            ]
        },
        {
            featureType: "water",
            elementType: "geometry",
            stylers: [
                { hue: "#0000FF" },
                {saturation:-40}
            ]
        },
        {
            featureType: "administrative.neighborhood",
            elementType: "labels.text.stroke",
            stylers: [
                { color: "#E80000" },
                {weight: 1}
            ]
        },{
            featureType: "road",
            elementType: "labels.text",
            stylers: [
                { visibility: "off" }
            ]
        },
        {
            featureType: "road.highway",
            elementType: "geometry.fill",
            stylers: [
                { color: "#FF00FF" },
                {weight: 2}
            ]
        }
    ];
    if (returnType == 'light') return lightStyle;
    if (returnType == 'bluish') return bluishStyle;
    if (returnType == 'water') return waterStyle;
    return [];
}