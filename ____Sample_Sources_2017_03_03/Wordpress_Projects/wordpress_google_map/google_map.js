// How it is called :
backendArtistsEditorFuncs.prototype.initializeMapAllConcerts = function ( filter, filterArtistTour, yourLocationLat, yourLocationLng, isDataRefreshing ) {
    if ( typeof isDataRefreshing=="undefined") isDataRefreshing= false

    if ( !isFloat(yourLocationLat) ) yourLocationLat= ''
    if ( !isFloat(yourLocationLng) ) yourLocationLng= ''
    artist_id=5 // todo
    var cbx_search_in_description= $("#cbx_search_in_description").is(':checked') ? 1 : '0'
    $.post(ajax_object.backend_ajaxurl, {
            action: 'get-artist-concerts',
            lang : this_m_lang,
            artist_id: artist_id,
            search_in_description : cbx_search_in_description,
            filter : filter,
            filter_artist_tour : filterArtistTour,
            plugin_url : this_plugin_url
        },
        function (data) {
            //alert( "data.artistsConcertsList.length:::" + data.artistsConcertsList.length + "   data::" + var_dump(data))
            if (data.error_code == 0) {
                var pluginParams= { plugin_url:this_plugin_url, logged_user_id : this_logged_user_id, this_m_pageUrl : this_m_pageUrl }
                googleMap.this_m_isMapLoaded= false
                googleMap.clearEventsCount()
                googleMap.setSelectMapStyle($("#select_map_style").val())
                googleMap.setisDrawFiltersBox(true)
                googleMap.setisShowDistanceText(true)
                googleMap.setisTourAsDashedLine(false) // different)
                googleMap.setisShowDirectionsButton(true)
                googleMap.setisShowFilterOptionsButton(true)
                googleMap.setisEditAddInfoInline(true)
                googleMap.setSelectMapLayer($("#select_map_layer").val())
                googleMap.setSelectMapTile($("#select_map_tile").val())
                googleMap.setShowParticipantsCircles($("#cbx_show_participants_circles_by_click").is(':checked'))
                googleMap.setShowBorderCountriesWithConcerts($("#cbx_border_countries_with_concerts").is(':checked'))
                googleMap.setYourLocation( yourLocationLat, yourLocationLng, true, pluginParams )
                googleMap.setLocationData( data.artistsConcertsList )
                googleMap.fillLists(pluginParams, true, true, true, true, data.artistToursList,true)
                googleMap.showMapContent()
            } else {
                alert(data.error_message)
            }
        }, "json");

}






var directionRegime = function(){ // when selecting direction points can be in 4 regims
    return {
        'NO_DIRECTION':0,
        'WAITE_FOR_START_POINT':1,
        'WAITE_FOR_LAST_POINT':2,
        'POINTS_SELECTED':3,
    }
}();
// vars of app
var this_m_objgoogleMapControls;
var markerArray = [];
var this_plugin_url = ''
var this_logged_user_id = ''
var this_m_pageUrl = ''
var this_m_map
...
var this_m_selectDirectionRegime= directionRegime.NO_DIRECTION
var this_m_directionsStartPoint
var this_m_directionsEndPoint
var selfgoogleMap;




function googleMap(Params) { // create google object with init parameters
    selfgoogleMap = this;
    this_plugin_url = Params.plugin_url;
    this_logged_user_id = Params.logged_user_id;
    this_m_pageUrl = Params.m_pageUrl;
    this_m_artist_id = Params.m_artist_id;
...
    this_m_futureEventImageUrl= this_plugin_url + 'images/map/future.png';
    this_m_priorEventImageUrl= this_plugin_url + 'images/map/prior.png';
    this_m_todayEventImageUrl= this_plugin_url + 'images/map/today.png';
    this_m_nearestEventImageUrl= this_plugin_url + 'images/map/nearest.png';
    this_m_yourLocationEventImageUrl= this_plugin_url + 'images/map/your-location.png';
    this_m_infoImageUrl= this_plugin_url + 'images/map/info.png';
}

// set functions
googleMap.prototype.setisDrawFiltersBox = function ( isDrawFiltersBox ) {
    this_m_isDrawFiltersBox= isDrawFiltersBox
...
googleMap.prototype.makeDrawDirections= function (  ) {
    this_m_objgoogleMapControls.makeDrawDirections()

}

googleMap.prototype.setisShowDistanceText = function ( isShowDistanceText ) {
    this_m_isShowDistanceText= isShowDistanceText
}


googleMap.prototype.showMapContent= function () { // when all data are set - run calculating distance and show it
    selfgoogleMap.setDistanceForYourLocation(true, true)
}
googleMap.prototype.showLocationEvents= function (isDataRefreshing) { // after last Location Distance was retrieved with callback requests- show map with data
...
    this_m_map = new google.maps.Map(document.getElementById(this_m_div_map), {
        zoom: this_m_zoomLevel,
        panControl : true,
        mapTypeControl: true,
        mapTypeControlOptions: {
            style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
            position: google.maps.ControlPosition.BOTTOM_CENTER,
        },

        zoomControl : true,
        zoomControlOptions: {
            style: google.maps.ZoomControlStyle.LARGE,
        },

        scaleControl: true,
        streetViewControl: false,
        streetViewControlOptions: {
            position: google.maps.ControlPosition.LEFT_TOP
        },

        center: new google.maps.LatLng(this_m_defaultLocationLat, this_m_defaultLocationLng),
    });

    google.maps.event.addListener(this_m_map, 'zoom_changed', function() {
        selfgoogleMap.setPolylinesVisible()
        selfgoogleMap.setParticipantsCirclesVisible()
    });

    if ( this_m_layerName != "" ) {
        var objgoogleMapLayers= new googleMapLayers( pluginParams  );
        objgoogleMapLayers.setMapLayer(this_m_map, this_m_layerName);
    }

    if ( this_m_is_showBorderCountriesWithConcerts ) { // Border countries with concerts // WORKS ONLY ON LIVE SERVER !
        for (var j = 0; j < this_m_usedCountriesList.length; j++) {
            if (typeof this_m_usedCountriesList[j]['mc'] == "object" && this_m_usedCountriesList[j]['mc'] === null) {
                kmlUrl = this_plugin_url + "js/map/kml/" + this_m_usedCountriesList[j]['country'] + ".kml"
                var KmlLayer = new google.maps.KmlLayer({
                    url: kmlUrl,
                    suppressInfoWindows: true,
                    preserveViewport: false,
                    map: this_m_map
                });
            }
        }
    }  //if ( this_m_is_showBorderCountriesWithConcerts ) { // Border countries with concerts // WORKS ONLY ON LIVE SERVER !


    if ( this_m_isShowFilterOptionsButton ) {
        ...
    }


    if ( this_m_isShowDirectionsButton ) {
        var showDirectionsControlDiv = document.createElement('div');
        var showDirectionsControl = this_m_objgoogleMapControls.showDirectionsControl(showDirectionsControlDiv, this_m_map);
        showDirectionsControlDiv.index = 2;
        this_m_map.controls[google.maps.ControlPosition.TOP_RIGHT].push(showDirectionsControlDiv);
    }


    if ( this_m_tileName != "" ) {
        var objgoogleMapTiles= new googleMapTiles( pluginParams  );
        objgoogleMapTiles.switchMapToTime(this_m_map, this_m_tileName)
    } //if ( this_m_tileName != "" ) {


    var i;
    var locationEventHTML= '';

    for (i = 0; i < this_m_eventLocationsList.length; i++) { // rows of all location Events
        locationEventHTML = locationEventHTML + '<tr>'
        locationEventHTML = locationEventHTML + '<td><a style="cursor: pointer" onclick="javascript:selfgoogleMap.centerByEvent(' + this_m_eventLocationsList[i].lat + ',' + this_m_eventLocationsList[i].lng + ')">+</a></td>'

        if (this_m_eventLocationsList[i].futureDate) {
            if (this_m_eventLocationsList[i].todayEvent) {
                EventImageUrl = this_m_todayEventEventImageUrl
                this_m_todayEventsCount = this_m_todayEventsCount + 1;
            } else {
                if (this_m_eventLocationsList[i].nearestEvent) {
                    EventImageUrl = this_m_nearestEventImageUrl
                    this_m_nearestEventsCount = this_m_nearestEventsCount + 1;
                } else {
                    //alert( "futureEvent::"+var_dump(1) )
                    EventImageUrl = this_m_futureEventImageUrl
                    this_m_futureEventsCount = this_m_futureEventsCount + 1;
                }
            }
        } else {
            EventImageUrl = this_m_priorEventImageUrl
            this_m_priorEventsCount = this_m_priorEventsCount + 1;
        }

        var newMarker= objgoogleMapMarkers.addMarkerToMap(this_m_map, this_m_eventLocationsList[i].lat, this_m_eventLocationsList[i].lng, EventImageUrl);
        this_m_eventLocationsList[i].marker = newMarker

        for (var j = 0; j < this_m_usedCountriesList.length; j++) {
            if ( this_m_eventLocationsList[i].country == this_m_usedCountriesList[j]['country'] ) {
                if ( typeof this_m_usedCountriesList[j]['mc'] == "object" && this_m_usedCountriesList[j]['mc'] === null) {
                    var clusterStyles = [
                        { // all  countries with markers can be grouped in 1 cluster under flag of this country( flag must be in special directory )
                            url: this_plugin_url + 'images/country/'+this_m_usedCountriesList[j]['country']+'.png',
                            height: 32,
                            width: 32,
                            opt_anchor: [15, 15],
                            opt_textColor: '#222222',
                            opt_textSize: 13
                        }
                    ];
                    var mcOptions = {gridSize: 150, maxZoom: this_m_clustersMaxZoom, styles: clusterStyles};
                    var mc = new MarkerClusterer(this_m_map, [], mcOptions);
                    this_m_usedCountriesList[j]['mc']= mc
                }
                this_m_usedCountriesList[j]['mc'].addMarker(newMarker)
            }
        }

        if (this_m_is_showParticipantsCircle) {
            var participantsCircle= objgoogleMapMarkers.showParticipantsCircle(this_m_map, this_m_eventLocationsList[i])
            if ( participantsCircle ) {
                this_m_showParticipantsCirclesList[this_m_showParticipantsCirclesList.length] = participantsCircle
            }
        }

        var eventDescription = ''
        if (typeof this_m_eventLocationsList[i].description != "undefined") {
            eventDescription = stripSlashes(this_m_eventLocationsList[i].description)
        }
        ...
        locationEventHTML = locationEventHTML + '<td>' + nextEventText
        var eventYear = parseInt(this_m_eventLocationsList[i].eventDateYear)
        var eventMonth = parseInt(this_m_eventLocationsList[i].eventDateMonth) - 1
        var eventDay = parseInt(this_m_eventLocationsList[i].eventDateDay)
        var eventHour = parseInt(this_m_eventLocationsList[i].eventDateHour)
        var eventMinute = parseInt(this_m_eventLocationsList[i].eventDateMinute)
        var itemDate = new Date(eventYear, eventMonth, eventDay, eventHour, eventMinute)
        locationEventHTML = locationEventHTML + " On " + formatDate(itemDate) + '</td>'
        locationEventHTML = locationEventHTML + "<td>" + this_m_eventLocationsList[i].place_name + ". " + this_m_eventLocationsList[i].distanceText + '</td>'
        if (this_m_isShowYourLocation && this_m_isShowDistanceText) {
            eventDescription = eventDescription + ". Distance= <b>" + this_m_eventLocationsList[i].distanceText + "</b>"
        }

        if ( typeof this_m_eventLocationsList[i].participants != "undefined" && parseInt(this_m_eventLocationsList[i].participants) > 0 ) {
            eventDescription= eventDescription + ". <br>"+this_m_eventLocationsList[i].participants+" participants" + nextEventRelatePostHTML
        }
        locationEventHTML= locationEventHTML + "<td>" + eventDescription + '</td>'

        var markerEditHTML= ''
        if ( this_m_isEditAddInfoInline ) {
            markerEditHTML= '<br><a style="cursor: pointer" onclick="javascript:googleMap.mapEventEdit('+i+', '+this_m_eventLocationsList[i].ID+')">Edit<a>&nbsp;&nbsp;&nbsp;' + '<a style="cursor: pointer" onclick="javascript:googleMap.mapEventDelete('+i+', '+this_m_eventLocationsList[i].ID+')">Delete<a>';
        } else {
            markerEditHTML= markerEditHTML+'<br>'
        }


        markerEditHTML= markerEditHTML + '&nbsp;&nbsp;&nbsp;<a style="cursor: pointer" onclick="javascript:selfgoogleMap.centerByEvent('+this_m_eventLocationsList[i].lat+','+ this_m_eventLocationsList[i].lng+')">Center</a>';  // javascript:selfgoogleMap.centerByEvent(
        markerEditHTML= markerEditHTML + '<br>&nbsp;<a style="cursor: pointer" onclick="javascript:selfgoogleMap.showNearbyPlaces('+this_m_eventLocationsList[i].lat+','+ this_m_eventLocationsList[i].lng+')">Show Nearby Places</a>&nbsp;<br>At distance(in metres)&nbsp;<input type="input" value="200" id="nearby_places_in_meters" size="4" maxlength="4">';

        var tourName= selfgoogleMap.getTourNameByID( this_m_eventLocationsList[i].tour_id )
        if ( tourName != "" ) {
            tourName= ' in "' + tourName + "' tour."
        }
        this_m_eventLocationsList[i].markerIndex= i
        this_m_eventLocationsList[i].content= this_m_eventLocationsList[i].ID + ",  " + nextEventText + " on " + formatDate(itemDate) + " in " + this_m_eventLocationsList[i].place_name + " " + tourName + " " + eventDescription+markerEditHTML

        google.maps.event.addListener(newMarker, 'mouseover', (function(newMarker, i) {
            return function() {
                for ( var j = 0; j < this_m_eventLocationsList.length; j++ )
                    if ( this_m_eventLocationsList[j].markerIndex == i ) {
                        selfgoogleMap.closeCurrentInfoWindow()
                        this_m_currentInfoWindow= objgoogleMapMarkers.applyInfoWindow( this_m_map, newMarker, this_m_eventLocationsList[i].content )
                    }
                }

        })(newMarker, i));
            locationEventHTML= locationEventHTML + '</tr>'
    } // for (i = 0; i < this_m_eventLocationsList.length; i++) { // rows of all events
    if ( this_m_isShowYourLocation ) {
        selfgoogleMap.showYourLocation(this_m_map, pluginParams)
    }

    if ( jQuery.trim(this_m_div_data_table) != "" ) {
        this_m_objgoogleMapControls.setControlsContent( [ { field:this_m_div_data_table, value:locationEventHTML, type: "html"} ] )

    }
    if ( this_m_isDrawFiltersBox ) {
        googleMap.drawFiltersBox(this_m_map)
    }
    var objgoogleMapMarkers= new googleMapMarkers(pluginParams, selfgoogleMap)

    objgoogleMapMarkers.drawPointArrows( this_m_map, this_m_eventLocationsList, this_m_artistToursList, this_m_isTourAsDashedLine )



    selfgoogleMap.setPolylinesVisible()
    selfgoogleMap.setParticipantsCirclesVisible()
    selfgoogleMap.afterEventsLoaded()
    this_m_isMapLoaded= true;
} // googleMap.prototype.showLocationEvents = function (this_m_eventLocationsList) {  // after last Location Distance was retrieved with callback requests- show map with data

googleMap.prototype.afterEventsLoaded= function() {  // make some code after events/concerts are loaded
    for( var i= 0; i< this_m_afterLoadEventsList.length; i++ ) {
        if( this_m_afterLoadEventsList[i].eventType == "centerMarcenterMarkerker" ) {
            for (j = 0; j < this_m_eventLocationsList.length; j++) { // rows of all location Events
                if ( this_m_afterLoadEventsList[i].value == this_m_eventLocationsList[j].ID) {
                    // alert( " Found::" )
                    selfgoogleMap.centerByEvent(  this_m_eventLocationsList[j].lat, this_m_eventLocationsList[j].lng  )
                    break;
                }
            }
        }
    }
}

googleMap.prototype.setAfterLoadEvents= function(afterLoadEventsList) {
    this_m_afterLoadEventsList= afterLoadEventsList
}

googleMap.prototype.setPolylinesVisible= function() {  // depending on current zoom polylines between points are visible or hidden
    this_m_zoomLevel = this_m_map.getZoom();
    for( var i= 0; i< this_m_polylinesList.length; i++ ) {
        if ( this_m_zoomLevel <=  this_m_clustersMaxZoom ) {
            this_m_polylinesList[i]['polyline'].setVisible(false);
        } else {
            this_m_polylinesList[i]['polyline'].setVisible(true);
        }
    }
    selfgoogleMap.showInfo()
}

googleMap.prototype.setParticipantsCirclesVisible= function() {  // to show/hide participants circles around points
    this_m_zoomLevel = this_m_map.getZoom();
    for( var i= 0; i< this_m_showParticipantsCirclesList.length; i++ ) {
        if ( this_m_zoomLevel <=  this_m_clustersMaxZoom ) {
            this_m_showParticipantsCirclesList[i].participantsCircle.setVisible(false);
        } else {
            this_m_showParticipantsCirclesList[i].participantsCircle.setVisible(true);
        }
    }
    selfgoogleMap.showInfo()
}

googleMap.prototype.addEventPolyline= function(startPointID, lastPointID, polyline, index ) {
...
}


googleMap.prototype.closeCurrentInfoWindow= function() {
    if(this_m_currentInfoWindow) {
        this_m_currentInfoWindow.close();
    }
}

googleMap.prototype.getTourNameByID= function(tourID) {
    for (var i = 0; i < this_m_artistToursList.length; i++) {
        if ( this_m_artistToursList[i].ID == tourID ) {
            return this_m_artistToursList[i].tour_name;
        }
    }
    return '';
}

googleMap.prototype.showInfo= function() {
...
}
googleMap.prototype.drawFiltersBox= function(this_m_map) {
    var priorLabel= 'Past events (' + this_m_priorEventsCount + ')'
    if (this_m_currentFilter == 'prior' ) {
        priorLabel= '<b>Past events (' + this_m_priorEventsCount + ')</b>'
    }
    var resHTML= "<table><tr><td style=\"cursor: pointer;\" onclick=\"javascript:googleMap.filterSet('prior')\" >" + priorLabel + '</td><td> - <img width="15px" height="24px" style="cursor:pointer;margin-top:0px;" src="'+this_m_priorEventImageUrl+'" alt="Show only past events" onclick="javascript:googleMap.filterSet(\'prior\')" ></td></tr>'

    var todayLabel= 'Today\'s events (' + this_m_todayEventsCount + ')'
    if (this_m_currentFilter == 'today' ) {
        todayLabel= '<b>Today\'s events (' + this_m_todayEventsCount + ')</b>'
    }
    resHTML= resHTML + "<tr><td style=\"cursor: pointer;\" onclick=\"javascript:googleMap.filterSet('today')\" >" + todayLabel + '</td><td> - <img width="15px" height="24px" style="cursor:pointer;margin-top:0px;" src="'+this_m_todayEventImageUrl+'" alt="Show only today\'s events" onclick="javascript:googleMap.filterSet(\'today\')" ></td></tr>'

    var nearestLabel= 'Nearest events (' + this_m_nearestEventsCount + ')'
    if (this_m_currentFilter == 'nearest' ) {
        nearestLabel= '<b>Nearest events (' + this_m_nearestEventsCount + ')</b>'
    }
    resHTML= resHTML + "<tr><td style=\"cursor: pointer;\" onclick=\"javascript:googleMap.filterSet('nearest')\" >" + nearestLabel + '</td><td> - <img width="15px" height="24px" style="cursor:pointer;margin-top:0px;" src="'+this_m_nearestEventImageUrl+'" alt="Show only neares events" onclick="javascript:googleMap.filterSet(\'nearest\')" ></td></tr>+'

    var futureLabel= 'Future events (' + this_m_futureEventsCount + ')'
    if (this_m_currentFilter == 'future' ) {
        futureLabel= '<b>Future events (' + this_m_futureEventsCount + ')</b>'
    }
    resHTML= resHTML + "<tr><td style=\"cursor: pointer;\" onclick=\"javascript:googleMap.filterSet('future')\" >" + futureLabel + '</td><td> - <img width="15px" height="24px" style="cursor:pointer;margin-top:0px;" src="'+this_m_futureEventImageUrl+'" alt="Show only future events" onclick="javascript:googleMap.filterSet(\'future\')" ></td></tr>'

    var allEventsCount= this_m_priorEventsCount + this_m_todayEventsCount + this_m_nearestEventsCount + this_m_futureEventsCount
    var allLabel= 'All events (' + allEventsCount + ')'
    if (this_m_currentFilter == 'all' ) {
        allLabel= '<b>All events (' + allEventsCount + ')</b>'
    }
    resHTML= resHTML + "<tr><td>" + allLabel + '</td><td style="cursor: pointer;" onclick="javascript:googleMap.filterSet(\'\')" > - <img width="15px" height="24px" style="cursor:pointer;margin-top:0px;" src="'+this_m_allEventImageUrl+'" alt="Show all events" onclick="javascript:googleMap.filterSet(\'\')" >'
    resHTML= resHTML +  '</td></tr></table>'
    this_m_objgoogleMapControls.setControlsContent( [ { field:"div_filters_box", value:resHTML, type: "html"} ] )
}

googleMap.prototype.filterSet= function(filter) {
    this_m_currentFilter= filter       // retreieve data and redraw of map with other fiilter
    backendArtistsEditorFuncs.initializeMapAllConcerts( filter, '', this_m_yourLocationLat, this_m_yourLocationLng, true )
}


googleMap.prototype.mapEventEdit= function(mapIndex, eventId) { // retrieve data on event ( eventId ) and open popup page for editing it
    $.ajax({
        url: ajax_object.backend_ajaxurl,
        dataType: "json",
        type: 'post',
        data: {
            action: 'get-artist-concert-by-id',
            artist_concert_id: eventId,
            artist_id: this_m_artist_id
        },
        success: function (result) {
            if (result.error_code == 0) {
                this_m_objgoogleMapControls.showEventEditor( false, '', '', '', eventId, result.artistConcert )
            } else {
                alert(result.error_message)
            }
        }
    })
}


googleMap.prototype.mapEventDelete= function(mapIndex, eventId) {  // Delete concert by ID
    if (!confirm("Do you to delete this event ?")) return;
    $.ajax({
        url: ajax_object.backend_ajaxurl,
        dataType: "json",
        type: 'post',
        data: {
            action: 'delete-artist-concert',
            artist_concert_id: eventId,
            artist_id: this_m_artist_id,
        },
        success: function (result) {
            if (result.error_code == 0) {
                backendArtistsEditorFuncs.initializeMapAllConcerts('', '', this_m_yourLocationLat, this_m_yourLocationLng, true)
            } else {
                alert(result.error_message)
            }
        }
    })
}

googleMap.prototype.showYourLocation = function(this_m_map, pluginParams) { // show geoLocation of user with special  icon
    if ( this_m_isShowYourLocation && ( typeof this_m_yourLocationLat != "undefined" && this_m_yourLocationLat!= null ) && ( typeof this_m_yourLocationLng != "undefined" && this_m_yourLocationLng != '') ) {

        var newMarker = new google.maps.Marker({
            position: new google.maps.LatLng( this_m_yourLocationLat, this_m_yourLocationLng ),
            //icon: this_m_yourLocationEventImageUrl,
            icon: this_m_todayEventImageUrl,
            draggable: false,
            // animation : google.maps.Animation.BOUNCE  /* DROP */,
            title : 'You are here!',
            map: this_m_map
        });
        var objgoogleMapMarkers= new googleMapMarkers(pluginParams, selfgoogleMap)
        objgoogleMapMarkers.applyInfoWindow( this_m_map, newMarker, 'You are here!' )
    }
}


googleMap.prototype.handleNoGeolocation = function (errorFlag) {
...
}

googleMap.prototype.setYourLocation = function ( yourLocationLat, yourLocationLng, isGetLocation, pluginParams) { // show geolocation of user with some manuall points
    this_m_isShowYourLocation= true

    if ( ( typeof yourLocationLat != "undefined" && yourLocationLat!= null ) && ( typeof yourLocationLng != "undefined" && yourLocationLng != '') ) {
        this_m_yourLocationLat = yourLocationLat
        this_m_yourLocationLng = yourLocationLng
    } else {
...
    }
    return false;
} // googleMap.prototype.setYourLocation = function () {


googleMap.prototype.setLocationData = function (eventLocationsList) {  // set location points data/concerts Data
    this_m_eventLocationsList = eventLocationsList
    this_m_usedCountriesList = [];

    for (var i = 0; i < this_m_eventLocationsList.length; i++) {
        var nextCountry= eventLocationsList[i].country
        var isFound= false
        for (var j = 0; j < this_m_usedCountriesList.length; j++) {
            ...
        }
        if ( !isFound ) {
            ...
        }
    }
}

googleMap.prototype.setDistanceForYourLocation = function ( isDataRefreshing, isShowLocationEvents) { // calculate distance from Your Location point
    // by geolocalion or manual points with running callback requests. After all callback requests done then showLocationEvents is called to show maps
    if ( this_m_eventLocationsList.length == 0 && isShowLocationEvents ) { // NO DATA
        selfgoogleMap.showLocationEvents(isDataRefreshing)// after last Location Distance was retrieved with callback requests- show map with data
        return
    }
    var service = new google.maps.DistanceMatrixService();
    if (this_m_distanceLocationIndex< 0 ) this_m_distanceLocationIndex= 0;

    var yourLocation= new google.maps.LatLng( this_m_yourLocationLat, this_m_yourLocationLng )

    if ( typeof this_m_eventLocationsList[ this_m_distanceLocationIndex ] != "undefined" ) {
        this_m_curreventLocationId = this_m_eventLocationsList[this_m_distanceLocationIndex].ID
        service.getDistanceMatrix({
            origins: [yourLocation],
            destinations: [new google.maps.LatLng(this_m_eventLocationsList[this_m_distanceLocationIndex].lat, this_m_eventLocationsList[this_m_distanceLocationIndex].lng)],
            travelMode: google.maps.TravelMode.DRIVING,
            unitSystem: google.maps.UnitSystem.METRIC,
            avoidHighways: false,
            avoidTolls: false
        }, function (response, status) {
            callbackEvents(response, status, this_m_curreventLocationId, isDataRefreshing)
        });
    }


}

function callbackEvents(response, status, itemId, isDataRefreshing) { // callback of setDistanceForYourLocation - write all distance in this_m_eventLocationsList
// when last callback requests done then showLocationEvents is called to show maps
    var distanceText= ''
    for (var i = 0; i < response.rows.length; i++) {
        ...
    }

    for (var i = 0; i < this_m_eventLocationsList.length; i++) {
        if ( parseInt(this_m_eventLocationsList[i].ID) == parseInt(itemId) ) {
            this_m_eventLocationsList[i].distanceText= distanceText
        }
    }

    if (status != google.maps.DistanceMatrixStatus.OK) {
        alert('Error was: ' + status);
    }
    this_m_numberOfEventLocations++
    this_m_distanceLocationIndex++


    if (this_m_numberOfEventLocations>= this_m_eventLocationsList.length) {
        ...
    }

}

googleMap.prototype.centerByEvent = function (lat,lng) { // set some point at center
    this_m_map.setCenter( new google.maps.LatLng(lat,lng), 15);
    for (i = 0; i < this_m_eventLocationsList.length; i++) { // rows of all location Events
        if (this_m_eventLocationsList[i].lat == lat && this_m_eventLocationsList[i].lng == lng) {
            //alert("FOUND this_m_eventLocationsList[i]::" + var_dump(this_m_eventLocationsList[i]))
            if (typeof this_m_eventLocationsList[i].marker != "undefined") {
                this_m_eventLocationsList[i].marker.setAnimation(google.maps.Animation.BOUNCE);
            }
        } else {
            this_m_eventLocationsList[i].marker.setAnimation();
        }
    }
}

googleMap.prototype.showNearbyPlaces = function (lat,lng) { // show Nearby Places for some point
    var pluginParams= { plugin_url:this_plugin_url, logged_user_id : this_logged_user_id, this_m_pageUrl : this_m_pageUrl }
    var objgoogleMapMarkers= new googleMapMarkers(pluginParams, selfgoogleMap)
    objgoogleMapMarkers.showNearbyPlacesInMeters(this_m_map, lat,lng, $("#nearby_places_in_meters").val(), this_m_infoImageUrl)  // get list of nearby places for point
}

googleMap.prototype.showCurrentLocation = function () { // show Current Location ( by geolocation or set manually ) with special marker
    if (this_m_is_ShowCurrentLocation) {
        var pos = new google.maps.LatLng(this_m_defaultLocationLat, this_m_defaultLocationLng);
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(this_m_defaultLocationLat, this_m_defaultLocationLng),
            map: this_m_map
        });
        var infowindow = new google.maps.InfoWindow({
            map: this_m_map,
            position: pos,
            content: 'You are here.'
        });
        this_m_map.setCenter(pos);
    }

}


googleMap.prototype.autocomplete_eventsonFocus = function () {  // init autocomplete by concert
    this_m_objgoogleMapControls.autocompleteEventsInit();
}

googleMap.prototype.clearEventsCount = function () {
    this_m_futureEventsCount= 0;
    this_m_priorEventsCount= 0;
    this_m_todayEventsCount= 0;
    this_m_nearestEventsCount= 0;
}

googleMap.prototype.triggerAutocomplete = function () {
    //alert( "triggerAutocomplete()::" )
    $( "#autocomplete_events" ).change();
}
googleMap.prototype.fillLists = function (pluginParams, iSFillMapLayerList, iSFillMapTileList, iSFillMapStyleList, iSFillArtistToursList, artistToursList, fillSelectionList) {
    // set some initial Lists
    this_m_objgoogleMapControls = new googleMapControls(pluginParams)
    if ( iSFillMapLayerList ) {
        this_m_objgoogleMapControls.fillMapLayerList(pluginParams)
    }
    if ( iSFillMapTileList ) {
        this_m_objgoogleMapControls.fillMapTileList(pluginParams)
    }
    if ( iSFillMapStyleList ) {
        this_m_objgoogleMapControls.fillMapStyleList(pluginParams)
    }
    if ( iSFillArtistToursList ) {
        this_m_objgoogleMapControls.fillArtistToursList(artistToursList, fillSelectionList)
    }
    this_m_artistToursList= artistToursList
}

