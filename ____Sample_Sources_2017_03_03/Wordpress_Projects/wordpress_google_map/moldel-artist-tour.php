<?php
if ( ! defined( 'ArtistsSongs' ) ) {
    exit; // Exit if accessed directly
}
require_once( 'artists-songs-db.php');

class ArtistTours extends ArtistsSongsDB // ArtistTours class of model extends ArtistsSongsDB, class common for all app
{
    public function getMaxOrdering($filters = array(), $sort = 'title', $sort_direction = 'desc')
    {
        $sqlStatement = "SELECT max(ordering) AS maxOrdering FROM `" . $this->tbl_artist_tours . "`" ;
        $retResult = $this->m_wpdb->get_results($sqlStatement, ARRAY_A);
        if ( !empty($retResult[0]['maxOrdering']) ) {
            return $retResult[0]['maxOrdering'];
        }
        return 0;
    }

    public function getArtistToursSelectionList($filters = array(), $sort = 'title', $sort_direction = 'desc')
    {
        $usersList = $this->getArtistToursList(false, '', array(), $sort, $sort_direction, 'ID,display_name');
        $resArray = array();
        foreach ($usersList as $nextArtistTour) {
            $resArray[] = array('key' => $nextArtistTour['ID'], 'value' => $nextArtistTour['title']);
        }
        return $resArray;
    } // public function getArtistToursSelectionList($filters = array(), $sort = 'title', $sort_direction = 'desc')

    public function getArtistToursList($outputFormatCount = false, $paged = '', $filters = array(), $sort = '', $sort_direction = '') { // retrieve list of data by given parameters
        $hasWhereCond= false;
        if($outputFormatCount) { // return number of resulting rows - prepare sql for this
            $sqlStatement = "SELECT count(distinct " . $this->tbl_artist_tours . ".ID) AS rowsCount FROM `" . $this->tbl_artist_tours . "`";
        } else { // return result set with all fields of resulting rows - prepare sql for this
            $sqlStatement = "SELECT distinct `" . $this->tbl_artist_tours . "`.* FROM `" . $this->tbl_artist_tours . "`";
        }
        if ( !empty($filters['title']) ) { // set filter on title field
            $sqlStatement.= ( $hasWhereCond? "":" where " ) . ( $hasWhereCond? " AND ":"" ) . " title like '%".$filters['title']."%' ";
            $hasWhereCond= true;
        }
        if ( !empty($filters['strict_title']) ) { // set filter on title field
            $sqlStatement.= ( $hasWhereCond? "":" where " ) .  ( $hasWhereCond? " AND ":"" ) . " title = '".$filters['strict_title']."' ";
            $hasWhereCond= true;
        }

        if ( !empty($filters['is_active']) ) { // set filter on is_active field
            $sqlStatement.= ( $hasWhereCond? "":" where " ) .  ( $hasWhereCond? " AND ":"" ) . " is_active = '".$filters['is_active']."' ";
            $hasWhereCond= true;
        }

        if ( !empty($filters['id!=']) ) { // set filter on id field != value
            $sqlStatement.= ( $hasWhereCond? "":" where " ) .  ( $hasWhereCond? " AND ":"" ) . " ID != '".$filters['id!=']."' ";
            $hasWhereCond= true;
        }

        if (!$outputFormatCount) { // set order by if we do not return count of rows
            if (empty($sort)) {
                $sort = 'ordering';
                $sort_direction = 'asc';
            }
            if (!empty($sort)) {
                $sqlStatement .= " order by " . $sort . " ";
            }
            if (!empty($sort) and !empty($sort_direction)) {
                $sort_direction = nsnClass_appFuncs::getLimitedValue($sort_direction, array('asc', 'desc'), 'asc');
            }
            if (!empty($sort_direction)) {
                $sqlStatement .= " " . $sort_direction . " ";
            }
        } // if (!$outputFormatCount) { // set order by if we do not return count of rows

        $limitSqlSubstr= '';
        if ( !empty($paged) and !empty($filters['itemsPerPage']) ) { // if page is set - then calculate and set limit condition
            $itemsPerPage= (int)$filters['itemsPerPage'];
            $limitStart= ($paged - 1) * $itemsPerPage ;
            $limitSqlSubstr= ' LIMIT ' . $itemsPerPage . " OFFSET  " . $limitStart;
        }
        $sqlStatement.= $limitSqlSubstr;
        if ( !empty($filters['limit']) and is_numeric($filters['limit'])) {  // set number of rows
            $sqlStatement.=  " limit " . $filters['limit'] . " offset 0 ";
        }

        $retResult = $this->m_wpdb->get_results($sqlStatement, ARRAY_A);
        if($outputFormatCount) {
            return $retResult[0]['rowsCount']; // return number of resulting rows
        } else{
            return $retResult; // return resulting array with all fields of resulting rows
        }
    }   // public function getArtistToursList($outputFormatCount = false, $page = '', $filters = array(), $sort = '', $sort_direction = '') { // retrieve list of data by given parameters

    public function updateArtistTour($artist_tour_id, $dataArray)
    { // add(if artist_tour_id is empty)/modify tour of artist
        if (empty($artist_tour_id)) {
            $ret= $this->m_wpdb->insert( $this->tbl_artist_tours, $dataArray ); // add new tour of artist
            $ArtistTour_insert_id= $this->m_wpdb->insert_id;
            return ( $ret === false ? false : $ArtistTour_insert_id );
        } else {
            $ret= $this->m_wpdb->update( $this->tbl_artist_tours, $dataArray, array( 'ID' => $artist_tour_id )); //// modify concert of artist
            return ( $ret === false ? false :$artist_tour_id );
        }
    } //public function updateArtistTour($artist_tour_id, $postArray) // add(if artist_tour_id is empty)/modify tour of artist

    public function getRowById($artist_tour_id) { // retrieve artist tour by its ID
        $sqlStatement = "SELECT * FROM `" . $this->tbl_artist_tours . "` where ID= '" . $artist_tour_id ."' ";
        $retResult = $this->m_wpdb->get_results($sqlStatement, ARRAY_A);
        if (!empty($retResult[0])) {
            return $retResult[0];
        }
        return false;
    } // public function getRowById($artist_tour_id) {  // retrieve artist tour by its ID



    public function deleteArtistTourRow($artist_tour_id, $onlyReturnQueries= false ) { // delete artist tour by its ID
        if (!empty($artist_tour_id)) {
            $sqlQueries= array(
                "DELETE FROM `".$this->tbl_artist_concerts."` WHERE `tour_id` = '". $artist_tour_id ."'",

                "DELETE FROM `".$this->tbl_artist_tours."` WHERE `ID` = '". $artist_tour_id ."'");
            if ( $onlyReturnQueries ) return $sqlQueries;
            $runQueriesRes= htmlspecialchars( strip_tags( $this->runQueriesUnderTransaction( $sqlQueries )), ENT_QUOTES  ); // run all delete sql-statements as 1 transaction
            if (!empty($runQueriesRes)) {  // add error message to log
                nsnClass_appFuncs::addLog( $runQueriesRes, __FILE__.', '.__LINE__ , 'S', $this->m_wpdb );
            }
            return $runQueriesRes;
        }
        return false;
    } // public function deleteArtistTourRow($artist_tour_id) {   // delete artist tour by its ID

}