<?php  // artist_concerts
if ( ! defined( 'ArtistsSongs' ) ) {
    exit; // Exit if accessed directly
}
require_once( 'artists-songs-db.php');
class ArtistsConcerts extends ArtistsSongsDB  // ArtistsConcerts class of model extends ArtistsSongsDB, class common for all app
{

    public function getArtistsConcertsSelectionList($filters = array(), $sort = 'tour_id, event_date', $sort_direction = 'desc')
    {
        $usersList = $this->getArtistsConcertsList(false, '', array(), $sort, $sort_direction, 'ID,description');
        $resArray = array();
        foreach ($usersList as $nextArtist) {
            $resArray[] = array('key' => $nextArtist['ID'], 'value' => strip_tags( $nextArtist['description']) . ' on '.nsnClass_appFuncs::getFormattedDateTime($nextArtist['event_date'],'DateAsText') );
        }
        return $resArray;
    } // public function getArtistsConcertsSelectionList($filters = array(), $sort = 'title', $sort_direction = 'desc')

    public function getArtistsConcertsList($outputFormatCount = false, $paged = '', $filters = array(), $sort = '', $sort_direction = '')
    { // retrieve list of data by given parameters
        $hasWhereCond = false;
        $hasArtistDisplayNameJoined= !empty($filters['show_artist_display_name']);  // if in resulting sql to add related artist name

        $with_artist_is_active = !empty($filters['artist_is_active']); // if true then return only concerts for active artists
        $hasArtistsJoinedSubquery = '';
        $isArtistJoined= false;
        if ( $with_artist_is_active  ) {
            $hasArtistsJoinedSubquery= " JOIN `" . $this->tbl_artists."` ON `" . $this->tbl_artists."`.`ID` = `" . $this->tbl_artist_concerts . "` . `artist_id` ";
            $isArtistJoined= true;
        }

        if ($outputFormatCount) { // return number of resulting rows - prepare sql for this
            $sqlStatement = "SELECT count(distinct " . $this->tbl_artist_concerts . ".ID) AS rowsCount FROM `" . $this->tbl_artist_concerts . "` " . $hasArtistsJoinedSubquery;
        } else { // return result set with all fields of resulting rows
            $sqlStatement = "SELECT distinct `" . $this->tbl_artist_concerts . "`.* ".
                ($hasArtistDisplayNameJoined ? ", `" . $this->tbl_artists . "`.`name` as artist_name" : "") .
                " FROM `" . $this->tbl_artist_concerts . "`" .
                ( ( $hasArtistDisplayNameJoined and !$isArtistJoined ) ? " JOIN `" . $this->tbl_artists."` ON `" . $this->tbl_artists."`.`ID` = `" . $this->tbl_artist_concerts . "`.`artist_id`" : ""
                ) .
                $hasArtistsJoinedSubquery;
        }
        if (!empty($filters['artist_id'])) { // set filter on artist_id field
            $sqlStatement .= ($hasWhereCond ? "" : " where ") . ($hasWhereCond ? " AND " : "") . " `" . $this->tbl_artist_concerts . "`.artist_id = '" . $filters['artist_id'] . "' ";
            $hasWhereCond = true;
        }

        if (!empty($filters['tour_id'])) {  // set filter on tour_id field
            $sqlStatement .= ($hasWhereCond ? "" : " where ") . ($hasWhereCond ? " AND " : "") . " `" . $this->tbl_artist_concerts . "`.tour_id = '" . $filters['tour_id'] . "' ";
            $hasWhereCond = true;
        }

        if (!empty($filters['concert_id'])) {  // set filter on concert_id field
            $sqlStatement .= ($hasWhereCond ? "" : " where ") . ($hasWhereCond ? " AND " : "") . " `" . $this->tbl_artist_concerts . "`.ID = '" . $filters['concert_id'] . "' ";
            $hasWhereCond = true;
        }

        if ( !empty($filters['filter']) ) { // time filter
            $today= nsnClass_appFuncs::getFormattedDateTime(mktime(0,0,0),'mysql');
            $nextDay= nsnClass_appFuncs::getFormattedDateTime( nsnClass_appFuncs::unixDateTimeAddDay(mktime(0,0,0),(1)),'mysql');
            $nearestDay= nsnClass_appFuncs::getFormattedDateTime( nsnClass_appFuncs::unixDateTimeAddDay(mktime(0,0,0),(2+1)),'mysql');
            if ( $filters['filter'] == 'today' ) { // set date filters depending time type filter
                $sqlStatement.= ( $hasWhereCond? "":" where " ) . ( $hasWhereCond? " AND ":"" ) . " Date(`" . $this->tbl_artist_concerts . "`.event_date) = '".$today."' ";
                $hasWhereCond= true;
            }
            if ( $filters['filter'] == 'prior' ) {
                $sqlStatement.= ( $hasWhereCond? "":" where " ) . ( $hasWhereCond? " AND ":"" ) . " `" . $this->tbl_artist_concerts . "`.event_date < '".$today."' ";
                $hasWhereCond= true;
            }
            if ( $filters['filter'] == 'future' ) {
                $sqlStatement.= ( $hasWhereCond? "":" where " ) . ( $hasWhereCond? " AND ":"" ) . " `" . $this->tbl_artist_concerts . "`.event_date > '".$nearestDay."' ";
                $hasWhereCond= true;
            }
            if ( $filters['filter'] == 'nearest' ) {
                $sqlStatement.= ( $hasWhereCond? "":" where " ) . ( $hasWhereCond? " AND ":"" ) . " `" . $this->tbl_artist_concerts . "`.event_date < '".$nearestDay. "' AND  `" . $this->tbl_artist_concerts . "`.event_date >= '".$nextDay. "'  ";
                $hasWhereCond= true;
            }
        }

        if (  !empty($filters['place_name'])  and  !empty($filters['search_in_description'])  ) {  // set filter on place_name/description fields
            $sqlStatement.= ( $hasWhereCond? "":" where " ) . ( $hasWhereCond? " AND ":"" ) . " ( `" . $this->tbl_artist_concerts . "`.place_name like '%".$filters['place_name']."%' OR `" . $this->tbl_artist_concerts . "`.description like '%".$filters['place_name']."%' ) ";
            $hasWhereCond= true;
        }

        if (  !empty($filters['place_name'])  and  empty($filters['search_in_description'])  ) {   // set filter on place_name field
            $sqlStatement.= ( $hasWhereCond? "":" where " ) . ( $hasWhereCond? " AND ":"" ) . " `" . $this->tbl_artist_concerts . "`.place_name like '%".$filters['place_name']."%' ";
            $hasWhereCond= true;
        }

        if ( !empty($filters['description']) ) { // set filter on description field
            $sqlStatement.= ( $hasWhereCond? "":" where " ) . ( $hasWhereCond? " AND ":"" ) . " `" . $this->tbl_artist_concerts . "`.description like '%".$filters['description']."%' ";
            $hasWhereCond= true;
        }

        if (!empty($filters['id!='])) { // set filter on id field != value
            $sqlStatement .= ($hasWhereCond ? "" : " where ") . ($hasWhereCond ? " AND " : "") . " `" . $this->tbl_artist_concerts . "`.ID != '" . $filters['id!='] . "' ";
            $hasWheJenrereCond = true;
        }
        if ( $with_artist_is_active  ) { // if true then return only concerts for active artists
            $sqlStatement .= ($hasWhereCond ? "" : " where ") . ($hasWhereCond ? " AND " : "") . " `" . $this->tbl_artists . "`.is_active= 'A' ";
            $hasWheJenrereCond = true;
        }
        if (!$outputFormatCount) { // set order by if we do not return count of rows
            if (empty($sort)) {
                $sort = 'created';
                $sort_direction = 'desc';
            }
            if (!empty($sort)) {
                $sqlStatement .= " order by " . $sort . " ";
            }
            if (!empty($sort) and !empty($sort_direction)) {
                $sort_direction = nsnClass_appFuncs::getLimitedValue($sort_direction, array('asc', 'desc'), 'asc');
            }
            if (!empty($sort_direction)) {
                $sqlStatement .= " " . $sort_direction . " ";
            }
        } // if (!$outputFormatCount) { // set order by if we do not return count of rows

        $limitSqlSubstr= '';
        if ( !empty($paged) and !empty($filters['itemsPerPage']) ) { // if page is set - then calculate and set limit condition
            $itemsPerPage= (int)$filters['itemsPerPage'];
            $limitStart= ($paged - 1) * $itemsPerPage ;
            $limitSqlSubstr= ' LIMIT ' . $itemsPerPage . " OFFSET  " . $limitStart;
        }
        $sqlStatement.= $limitSqlSubstr;
        if ( !empty($filters['limit']) and is_numeric($filters['limit'])) { // set number of rows
            $sqlStatement.=  " limit " . $filters['limit'] . " offset 0 ";
        }

        $retResult = $this->m_wpdb->get_results($sqlStatement, ARRAY_A);
        if ($outputFormatCount) {
            return $retResult[0]['rowsCount']; // return number of resulting rows
        } else {
            return $retResult; // return resulting array with all fields of resulting rows
        }
    }   // public function getArtistsConcertsListList($outputFormatCount = false, $page = '', $filters = array(), $sort = '', $sort_direction = '') { // retrieve list of data by given parameters


    public function updateArtistsConcerts($artist_concert_id, $dataArray) // add(if artist_concert_id is empty)/modify concert of artist
    {
        if (empty($artist_concert_id)) { //
            $this->m_wpdb->insert( $this->tbl_artist_concerts, $dataArray ); // add new concert of artist
            return $this->m_wpdb->insert_id;;
        } else {
            $this->m_wpdb->update( $this->tbl_artist_concerts, $dataArray, array( 'ID' => $artist_concert_id )); // modify concert of artist
            return $artist_concert_id;
        }
    } //public function updateArtistConcert($artist_concert_id, $postArray) // add(if artist_concert_id is empty)/modify concert of artist


    public function getRowById($artist_concert_id, $filters= array()) { // retrieve concert by its ID with related info(if keys are true)
        $hasArtistDisplayNameJoined= !empty($filters['show_artist_display_name']); // show name of related artist if $filters['show_artist_display_name']= true

        $sqlStatement = "SELECT `".$this->tbl_artist_concerts."`.* ".
            ($hasArtistDisplayNameJoined ? ", `" . $this->tbl_artists . "`.`name` as artist_name" : "") .
            " FROM `" . $this->tbl_artist_concerts . "`" .
            ( ( $hasArtistDisplayNameJoined ) ? " JOIN `" . $this->tbl_artists."` ON `" . $this->tbl_artists."`.`ID` = `" . $this->tbl_artist_concerts . "`.`artist_id`" : ""
            ) .
            " where `".$this->tbl_artist_concerts."`.`ID`= '" . $artist_concert_id ."' ";

        $retResult = $this->m_wpdb->get_results($sqlStatement, ARRAY_A);
        if (isset($retResult[0])) {
            return $retResult[0];
        }
        return false;
    } // public function getRowById($artist_concert_id) { // retrieve concert by its ID with related info(if keys are true)

    public function deleteArtistsConcertsRow($artist_concert_id, $onlyReturnQueries = false) { // delete artist concert by its ID
        if (!empty($artist_concert_id)) {
            $sqlQueries = array(
                "DELETE FROM `".$this->tbl_artist_concerts."` WHERE `ID` = '". $artist_concert_id ."'");
            if ($onlyReturnQueries) return $sqlQueries;
            $runQueriesRes = nsnClass_appFuncs::stripErrorCode($this->runQueriesUnderTransaction($sqlQueries));  // run all delete sql-statements as 1 transaction

            if (!empty($runQueriesRes)) {  // add error message to log
                nsnClass_appFuncs::addLog($runQueriesRes, __FILE__ . ', ' . __LINE__, 'S', $this->m_wpdb);
            }
            return $runQueriesRes;
        }
        return false;
    } // public function deleteArtistsConcertsRow($artist_concert_id) { //// delete artist concert by its ID

}