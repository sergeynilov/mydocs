var this_plugin_url = ''
    ...
var self;
function googleMapMarkers( Params, selfgoogleMap ) {
    self = this;
    ...
}

googleMapMarkers.prototype.drawPointArrows = function (map, eventLocationsList, artistToursList, isTourAsDashedLine) { // set polyline between nearest(by date) concerts for any tour
    var from_place_name
    var to_place_name
    for (var i = 0; i < artistToursList.length; i++) {
        var Modul= ( i % 2 ) == 1
        for (var j = 1; j < eventLocationsList.length; j++) {
            if ( artistToursList[i].ID == eventLocationsList[j].tour_id ) {
                if (eventLocationsList[j-1].tour_id!= eventLocationsList[j].tour_id) continue;
                var startPoint= new google.maps.LatLng(eventLocationsList[j-1].lat, eventLocationsList[j-1].lng)
                var endPoint= new google.maps.LatLng(eventLocationsList[j].lat, eventLocationsList[j].lng)
                from_place_name= eventLocationsList[j-1].place_name
                to_place_name= eventLocationsList[j].place_name
                if ( isTourAsDashedLine ) {
                    var strokeOpacityValue= 0
                    var lineSymbol = {
                        path: 'M 0,-1 0,1',
                        strokeOpacity: 1,
                        scale: 2
                    };
                    var iconsSettings= [{
                        icon: lineSymbol,
                        offset: '0',
                        repeat: '20px',
                    }]
                } else {
                    if ( Modul ) {
                        var strokeOpacityValue= 1
                        var lineSymbolStart = {
                            path: google.maps.SymbolPath.FORWARD_OPEN_ARROW, //FORWARD_CLOSED_ARROW
                            geodesic: true,
                            scale: 3,
                            Color: artistToursList[i].color,
                            strokeColor: artistToursList[i].color,
                            strokeWeight: 0.4,
                            strokeOpacity: 1,
                            fillOpacity: 0.2,
                            fillColor: artistToursList[i].color
                        };
                        var lineSymbolEnd = {
                            path: google.maps.SymbolPath.FORWARD_OPEN_ARROW, //FORWARD_CLOSED_ARROW
                            geodesic: true,
                            scale: 5,
                            Color: artistToursList[i].color,
                            strokeColor: artistToursList[i].color,
                            strokeWeight: 0.4,
                            strokeOpacity: 1,
                            fillOpacity: 0.2,
                            fillColor: artistToursList[i].color
                        };
                    } else {
                        var lineSymbolStart = {
                            path: google.maps.SymbolPath.FORWARD_OPEN_ARROW, //FORWARD_CLOSED_ARROW
                            geodesic: true,
                            scale: 3,
                            Color: artistToursList[i].color,
                            strokeOpacity: 1,
                            strokeColor: artistToursList[i].color,
                            strokeWeight: 0.4,
                            fillOpacity: 1,
                            fillColor: artistToursList[i].color
                        };
                        var lineSymbolEnd = {
                            path: google.maps.SymbolPath.FORWARD_OPEN_ARROW, //FORWARD_CLOSED_ARROW
                            geodesic: true,
                            scale: 5,
                            Color: artistToursList[i].color,
                            strokeOpacity: 1,
                            strokeColor: artistToursList[i].color,
                            strokeWeight: 0.4,
                            fillOpacity: 1,
                            fillColor: artistToursList[i].color
                        };
                    } // if ( Modul ) {
                    var iconsSettings= [
                        {
                            icon: lineSymbolStart,
                            offset: '30%',
                        },
                        {
                            icon: lineSymbolEnd,
                            offset: '70%',
                        }
                    ]
                }

                var line = new google.maps.Polyline({
                    path: [ startPoint, endPoint ],
                    icons: iconsSettings,
                    map: map,
                    geodesic: true,
                    strokeOpacity: strokeOpacityValue,
                    strokeColor: artistToursList[i].color,
                    strokeWeight: 1.2,
                    from_place_name : from_place_name,
                    to_place_name : to_place_name
                });
                this_m_selfgoogleMap.addEventPolyline( eventLocationsList[j-1].ID, eventLocationsList[j].ID, line, j )
                lineSymbol = {}
            }
        }
    }
} // googleMapMarkers.prototype.drawPointArrows = function (map, eventLocationsList, artistToursList) { // set polyline between nearest(by date) concerts for any tour

googleMapMarkers.prototype.addMarkerToMap = function (map, lat, lng, imageUrl) { // add new Marker with given icon url
    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat, lng),
        icon: imageUrl,
        draggable: false,
        map: map
    });
    return marker;
} // googleMapMarkers.prototype.getMapSlayerList = function () {  // add new Marker with given icon url


googleMapMarkers.prototype.applyInfoWindow = function ( map, newMarker, contentText ) {
    ...
}

googleMapMarkers.prototype.showNearbyPlacesInMeters = function (map, lat,lng, nearbyPlacesInMeters, infoImageUrl) { // get list of nearby places for point
    this_m_map= map
    this_m_infoImageUrl= infoImageUrl
    if ( typeof nearbyPlacesInMeters == "undefined" && typeof nearbyPlacesInMeters == "object" ) return false;
    if ( parseInt(nearbyPlacesInMeters) <= 0 || isNaN(parseInt(nearbyPlacesInMeters) ) )  return false;
    var service = new google.maps.places.PlacesService(map);
    var point= new google.maps.LatLng(lat, lng)
    var request = {
        location: point,
        radius: nearbyPlacesInMeters, // in meters
        types: ['store']
    };
    service.nearbySearch(request, callbackNearbyPlaces);
} // googleMapMarkers.prototype.showNearbyPlacesInMeters = function (map, eventLocation) { // get list of nearby places for point

function callbackNearbyPlaces(results, status) { // callback for get list of nearby places for point
    if (status == google.maps.places.PlacesServiceStatus.OK) {
        for (var i = 0; i < results.length; i++) {
            createNearbyPlacesMarker(results[i], i);
        }
    }
} // function callbackNearbyPlaces(results, status) {

function createNearbyPlacesMarker(place, ind) {   //create new Nearby Places Marker by given points
    var placeLoc = place.geometry.location;
    var nearbyMarker = new google.maps.Marker({
        map: this_m_map,
        icon: this_m_infoImageUrl,
        position: place.geometry.location
    });
    google.maps.event.addListener(nearbyMarker, 'click', function() {
        var options = {
            map: this_m_map,
            position: place.geometry.location,
            content: place.name
        };
        var infowindow = new google.maps.InfoWindow(options);
        return infowindow.open(this_m_map, this);
    });
} // function createNearbyPlacesMarker(place) {  //create new Nearby Places Marker by given points

googleMapMarkers.prototype.showParticipantsCircle = function (map, eventLocation) {  // show participants circles around points
    if ( typeof eventLocation.participants == "object" ) return false;
    if ( parseInt(eventLocation.participants) <= 0 || isNaN(parseInt(eventLocation.participants) ) )  return false;
    ...
    if ( !eventLocation.futureDate && !eventLocation.todayEvent ) {
        var circleOptions = {
            strokeColor:  '#BCBCBC',
            strokeOpacity: 0.6,
            strokeWeight: 1,
            fillColor:  'red',
            fillOpacity: 0.35,
            map: map,
            center: circlePoint,
            radius: Math.sqrt(val)
        };
    }

    var participantsCircle = new google.maps.Circle(circleOptions);
    var ret = []
    ret['ID'] = eventLocation.ID;
    ret['participantsCircle'] = participantsCircle;
    return ret;
} // googleMapMarkers.prototype.showParticipantsCircle = function (map, eventLocation) { // show participants circles around points



