<?php
define('DOING_AJAX', true);

require_once( /* dirname( dirname( __FILE__ ) )*/ /*$wpRootDir.*/'../../../wp-load.php'); // TODO
/** Load WordPress Administration APIs */


if (!defined('nsnClass_wooImport')) {
    exit; // Exit if accessed directly
}
global $wpdb;//, $wp_session;

$action= !empty($_REQUEST['action']) ? $_REQUEST['action'] : '';
$user_id= !empty($_REQUEST['user_id']) ? $_REQUEST['user_id'] : '';
$plugin_dir= !empty($_REQUEST['plugin_dir']) ? $_REQUEST['plugin_dir'] : '';

require_once($plugin_dir . 'models/data-model.php');  // include model file with methods for data retrieving
require_once(ABSPATH . 'wp-admin/includes/taxonomy.php');
require_once(ABSPATH . 'wp-admin/includes/media.php');
require_once(ABSPATH . 'wp-admin/includes/file.php');
require_once(ABSPATH . 'wp-admin/includes/image.php');
$ModelsTblSource = new Models($wpdb);
$ext_categories_table_name= $ModelsTblSource->get_ext_categories_table_name();
$ext_templates_table_name= $ModelsTblSource->get_ext_templates_table_name();

if ($action == 'run-import') {   // http://local-wp.com/wp-content/plugins/woo-import/ajax-backend-control.php?action=run-import&plugin_dir=%2Fmnt%2FdiskD_Work%2Fwwwroot%2Fwp%2Fwp-content%2Fplugins%2Fwoo-import%2F&user_id=1
    $categoriesAddedSuccessfully= 0;
    $categoriesAddedFailed= 0;
    $postsAddedSuccessfully= 0;
    $postsAddedFailed= 0;
    $categoriesModifiedSuccessfully= 0;
    $categoriesModifiedFailed= 0;
    $postsModifiedSuccessfully= 0;
    $postsModifiedFailed= 0;
    remove_all_actions('save_post',10); // IMPORTANT


    ob_start();
    $categoriesToSkip= array();
    categoriesImport($ModelsTblSource, $user_id);  // Import of modified Categories
    templatesImport($ModelsTblSource, $user_id);
    $deletedRowsArray= clearDeletedRows($ModelsTblSource);
    $error_message = strip_tags(ob_get_contents());
    ob_end_clean();


    $info= 'Categories added : ' . ($categoriesAddedSuccessfully > 0 ? "<b>" : "" ) . $categoriesAddedSuccessfully . ($categoriesAddedSuccessfully > 0 ? "</b>" : "" ) .' successfully, ' .
        ( $categoriesAddedFailed > 0 ? "<b>" : "") . $categoriesAddedFailed  . ( $categoriesAddedFailed > 0 ? "</b>" : ""). ' failed.  ' .
        'Templates/Posts added : ' . ( $postsAddedSuccessfully > 0 ? "<b>" : "") . $postsAddedSuccessfully . ( $postsAddedSuccessfully > 0 ? "</b>" : "") .' successfully, ' .
        ( $postsAddedFailed > 0 ? "<b>" : "") . $postsAddedFailed . ( $postsAddedFailed > 0 ? "</b>" : "") . ' failed. <br>'  .

        'Categories modified : ' . ( $categoriesModifiedSuccessfully > 0 ? "<b>" : "") . $categoriesModifiedSuccessfully . ( $categoriesModifiedSuccessfully > 0 ? "</b>" : "") .' successfully, ' .
        ($categoriesModifiedFailed > 0 ? "<b>" : "") . $categoriesModifiedFailed . ($categoriesModifiedFailed > 0 ? "</b>" : "") . ' failed.   '.
        'Templates/Posts modified : ' . ( $postsModifiedSuccessfully > 0 ? "<b>" : "") . $postsModifiedSuccessfully . ( $postsModifiedSuccessfully > 0 ? "</b>" : "") .' successfully, ' .
        ($postsModifiedFailed ? "<b>" : "") . $postsModifiedFailed . ($postsModifiedFailed ? "</b>" : "") . ' failed. <br>' ;


    $info2= 'Posts deleted ' . ($deletedRowsArray['postsDeletedSuccessfully'] > 0 ? "<b>" : "") . $deletedRowsArray['postsDeletedSuccessfully'] . ($deletedRowsArray['postsDeletedSuccessfully'] > 0 ? "</b>" : ""). ' successfully, ' .
        ($deletedRowsArray['postsDeletedFailed']> 0 ? "<b>" : "") . $deletedRowsArray['postsDeletedFailed'] . ($deletedRowsArray['postsDeletedFailed']> 0 ? "</b>" : ""). ' failed. <br> ' .
        'Categories deleted ' . ($deletedRowsArray['categoriesDeletedSuccessfully']>0? "<b>" :"") . $deletedRowsArray['categoriesDeletedSuccessfully']. ($deletedRowsArray['categoriesDeletedSuccessfully']>0? "</b>" :""). ' successfully, '  .
        ($deletedRowsArray['categoriesDeletedFailed'] > 0 ? "<b>" : "") . $deletedRowsArray['categoriesDeletedFailed'] . ($deletedRowsArray['categoriesDeletedFailed'] > 0 ? "</b>" : ""). ' failed. <br> ';
    $retArray= $deletedRowsArray;
    $retArray['categoriesAddedSuccessfully']= $categoriesAddedSuccessfully;
    $retArray['categoriesAddedFailed']= $categoriesAddedFailed;
    $retArray['postsAddedSuccessfully']= $postsAddedSuccessfully;
    $retArray['postsModifiedFailed']= $postsModifiedFailed;
    $retArray['error_code']= ( empty($error_message) ? 0 : 1 );
    $retArray['error_message']= $error_message;
    $retArray['info']= $info . '<br><br>'. $info2;
    echo json_encode( $retArray );
}

function templatesImport($ModelsTblSource, $user_id)
{
    global $ext_templates_table_name, $wpdb, $postsAddedFailed, $postsAddedSuccessfully, $postsModifiedFailed, $postsModifiedSuccessfully, $categoriesToSkip;
    $modifiedTemplatesList= $ModelsTblSource->getTemplatesList(false, '', array('in_status'=>array('A','M','D'), 'categories_to_skip'=> $categoriesToSkip ));
    foreach( $modifiedTemplatesList as $nextModifiedTemplate ) { // list of all added/modified/deleted posts in OLD categories
        $nextModifiedTemplateStatus= $nextModifiedTemplate['status'];
        $nextModifiedTemplateWPId= $nextModifiedTemplate['wp_id'];
        if ( $nextModifiedTemplateStatus == 'A' ) { // To add new post to existing category

            $nextModifiedTemplateCategoryWPId=  getWPIdOfCategory($nextModifiedTemplate['category_id'] );

            $post_id= addWooCommerceProduct($ModelsTblSource, $user_id, $nextModifiedTemplateCategoryWPId , $nextModifiedTemplate);
            $updateExtTemplate= " UPDATE `".$ext_templates_table_name."` SET  `status`= 'U', `wp_id`= '".$post_id."' WHERE `id` = '" . $nextModifiedTemplate['id']."' ";
            $queryRet= $wpdb->query($updateExtTemplate);
            if ($queryRet===FALSE) {
                $postsAddedFailed++;
                break;
            } else {
                $postsAddedSuccessfully++;
            }

        } // if ( $nextModifiedTemplateStatus == 'A' ) { // To add new post to existing category

        if ( $nextModifiedTemplateStatus == 'M' and isset($nextModifiedTemplateWPId) and (integer)$nextModifiedTemplateWPId > 0) { // modify exiting post in existing category
            $post_id= updateWooCommerceProduct($ModelsTblSource, $user_id, $nextModifiedTemplate); // https://codex.wordpress.org/Function_Reference/wp_update_post
            $updateExtTemplate= " UPDATE `".$ext_templates_table_name."` SET  `status`= 'U' WHERE `id` = '" . $nextModifiedTemplate['id']."' ";
            $queryRet= $wpdb->query($updateExtTemplate);
            if ($queryRet===FALSE) {
                $postsModifiedFailed++;
                break;
            } else {
                $postsModifiedSuccessfully++;
            }

        } // if ( $nextModifiedTemplateStatus == 'M' and isset($nextModifiedTemplateWPId) and (integer)$nextModifiedTemplateWPId > 0) { // modify exiting post in existing category
    } //foreach( $modifiedTemplatesList as $nextModifiedTemplate ) { // list of all added/modified/deleted posts in OLD categories
}

function categoriesImport($ModelsTblSource, $user_id) {  // Import of modified Categories
    global $wpdb, $ext_categories_table_name, $categoriesAddedSuccessfully, $postsAddedSuccessfully, $ext_templates_table_name, $postsAddedFailed, $categoriesAddedFailed, $categoriesModifiedFailed, $categoriesModifiedSuccessfully, $categoriesToSkip;
    $categoriesList= $ModelsTblSource->getCategoriesList( ... ); // get list of modified categories
    foreach( $categoriesList as $nextCategory ) { // all new/modified/deleted categories in ci app
        if ( $nextCategory['status'] == '...' ) { // add new category
            $nextCategoryStruct = array('cat_name' => $nextCategory['name'], 'category_description' => $nextCategory['description'],... );

            $next_cat_id = wp_insert_category($nextCategoryStruct, true); // prepare structure for new category
            if (is_object($next_cat_id)) {
                if ( isset($next_cat_id->errors) ) {
                    if ( is_array($next_cat_id->errors) ) {
                        $categoriesToSkip[]= $nextCategory['id'];
                        echo '<b>'. /* $err_str */ print_r($next_cat_id->errors,true) . '</b>'; // show error text
                    }
                }
            }
            if ($next_cat_id and !is_object($next_cat_id) ) { // new category was added
                $categoriesAddedSuccessfully++;
                $updateExtCategory = " UPDATE `" . $ext_categories_table_name . "` ... ";
                $queryRet = $wpdb->query($updateExtCategory);
                if ($queryRet === FALSE) {
                    break;
                }

                // get list of updated templates of $nextCategory['id'] category
                $updatedTemplatesList= $ModelsTblSource->getTemplatesList(... 'category_id'=> $nextCategory['id'] ));
                foreach( $updatedTemplatesList as $nextUpdatedTemplate ) { // get list of all templates of created category - they all must be copied into wp products
                    $nextTemplateStatus= $nextUpdatedTemplate['status'];
                    if ( $nextTemplateStatus == 'A' ) { // add new post
                        $post_id= addWooCommerceProduct(...);
                        if ($post_id) {  // new post was added
                            $postsAddedSuccessfully++;
                            $updateExtTemplate= " UPDATE `".$ext_templates_table_name."` SET  ... ";
                            $queryRet= $wpdb->query($updateExtTemplate);
                            if ($queryRet===FALSE) {
                                break;
                            }
                        } else { // fail on post creation
                            $postsAddedFailed++;
                        }
                    } //if ( $nextTemplateStatus == 'A' ) { // add new post
                } // foreach( $updatedTemplatesList as $nextUpdatedTemplate ) { // get list of all templates of created category - they all must be copied into ww products
            } else { // fail on category creation
                $categoriesAddedFailed++;
            }
        } // if ( $nextCategory['status'] == 'A' ) { // add new category

        if ( $nextCategory['status'] == 'M' and !empty($nextCategory['wp_id']) and (integer)$nextCategory['wp_id']> 0 ) { // modified category
            $nextWPCategoryObject= get_cat_ID( $nextCategory['wp_id'] );
            if ( empty($nextWPCategoryObject) ) {
                $nextCategoryStruct = array('cat_name' => $nextCategory['name'], 'category_description' => $nextCategory['description']... );
                $update_term_ret = wp_insert_category($nextCategoryStruct, true); // prepare structure for new category
                if (is_object($update_term_ret)) {
                    if (isset($update_term_ret->errors)) {
                        if (is_array($update_term_ret->errors)) {
                            echo '<b>' . print_r($update_term_ret->errors, true) . '</b>';  // show error text
                        }
                    }
                }

            } else {
                $update_term_ret = wp_update_term($nextCategory['wp_id']...);
                if (is_object($update_term_ret)) {
                    if (isset($update_term_ret->errors)) {
                        if (is_array($update_term_ret->errors)) {
                            echo '<b>' . print_r($update_term_ret->errors, true) . '</b>';   // show error text
                        }
                    }
                }
            }



            if ( !is_object($update_term_ret) and empty($update_term_ret['errors']) ) { // there is an error modifying category
                $updateExtCategory = " UPDATE `" . $ext_categories_table_name . "` SET  ... ";
                $queryRet = $wpdb->query($updateExtCategory);
                if ($queryRet === FALSE) {
                    $categoriesModifiedFailed++;
                    break;
                } else {
                    $categoriesModifiedSuccessfully++;
                }
            } // if ( empty($update_term_ret['errors']) ) { // there is an error modifying category
        } // if ( $nextCategory['status'] == 'M' ) { // modified category


    } // foreach( $categoriesList as $nextCategory ) { // all new/modified/deleted categories in ci app

} // function categoriesImport($ModelsTblSource) {  // Import of modified Categories

function clearDeletedRows($ModelsTblSource) { // deleted from wordpress posts/categories which in external db marked as deleted
    global $wpdb;
    $ext_rows_deleted_table_name= $ModelsTblSource->get_ext_rows_deleted_table_name();
    $postsDeletedSuccessfully= 0;
    $postsDeletedFailed= 0;
    $categoriesDeletedFailed= 0;
    $categoriesDeletedSuccessfully= 0;
    $rowsDeletedTemplatesList= $ModelsTblSource->getRowsDeletedList(...);
    foreach( $rowsDeletedTemplatesList as $nextDeletedTemplate ) { // delete all templates
        $delete_post_res= wp_delete_post( $nextDeletedTemplate['delete_id'] );
        $deleteTemplateSql= " DELETE FROM `".$ext_rows_deleted_table_name."` WHERE `id` = '" . $nextDeletedTemplate['id']."' ";
        $queryRet= $wpdb->query($deleteTemplateSql);
        if ($queryRet===FALSE) {
            $postsDeletedFailed++;
            break;
        } else {
            $postsDeletedSuccessfully++;
        }
    }


    $rowsDeletedCategoriesList= $ModelsTblSource->getRowsDeletedList(...);
    foreach( $rowsDeletedCategoriesList as $nextDeletedCategory ) { // delete all categories
        $delete_category_res= wp_delete_term($nextDeletedCategory['delete_id'], 'product_cat');
        $deleteCategorySql= " DELETE FROM `".$ext_rows_deleted_table_name."` WHERE `id` = '" . $nextDeletedCategory['id']."' ";
        $queryRet= $wpdb->query($deleteCategorySql);
        if ($queryRet===FALSE) {
            $categoriesDeletedFailed++;
            break;
        } else {
            $categoriesDeletedSuccessfully++;
        }
    }
    return array( 'postsDeletedFailed' => $postsDeletedFailed, 'postsDeletedSuccessfully'=> $postsDeletedSuccessfully, 'categoriesDeletedFailed'=> $categoriesDeletedFailed,'categoriesDeletedSuccessfully'=> $categoriesDeletedSuccessfully );
}

function updateWooCommerceProduct($ModelsTblSource, $user_id, $nextUpdatedTemplate) // update Woo Commerce Product and related data in meta
{
    if (empty($nextUpdatedTemplate['wp_id'])) {
        return false;
    }
    $post_id= $nextUpdatedTemplate['wp_id'];

    $nextUpdatedTemplateCategoryId= $nextUpdatedTemplate['category_id'];
    if (empty($nextUpdatedTemplateCategoryId)) {
        return false;
    }

    $nextUpdatedTemplateCategoryWPId=  getWPIdOfCategory($nextUpdatedTemplateCategoryId);

    $my_post = array(
        'ID'           => $post_id,
        'post_title'   => $nextUpdatedTemplate['name'],
        'post_content' => $nextUpdatedTemplate['description'],
    );
    wp_update_post( $my_post, true );
    if (is_wp_error($post_id)) {
        $errors = $post_id->get_error_messages();
        foreach ($errors as $error) {
            echo $error;
        }
    }

    $catRes= wp_set_post_terms($post_id, array($nextUpdatedTemplateCategoryWPId), 'product_cat');

    update_post_meta( $post_id, '_width', $nextUpdatedTemplate['width'] );
    ...
    update_post_meta( $post_id, '_format', $nextUpdatedTemplate['format'] );

    $template_data= '';
    if (!empty($nextUpdatedTemplate['template'])) {
        if ( file_exists($nextUpdatedTemplate['template'])) {
            $template_data = file_get_contents($nextUpdatedTemplate['template']);
        }
    }

    update_post_meta( $post_id, '_template_data', $template_data);
    return $post_id;
} // function updateWooCommerceProduct($ModelsTblSource, $user_id, $next_cat_id, $nextUpdatedTemplate)// update Woo Commerce Product and related data in meta


function addWooCommerceProduct($ModelsTblSource, $user_id, $next_cat_id, $nextUpdatedTemplate) {

    $postStructure = array(
          'post_author' => $user_id,
          'post_content' => $nextUpdatedTemplate['description'],
          'post_status' => "publish",
          'post_title' => $nextUpdatedTemplate['name'],
          'post_name' => $nextUpdatedTemplate['sku'],
          'post_parent' => '',
        //'post_category' => array($next_cat_id),
        'post_type' => "product",

    );
    //Create post
    $wp_error= false;
    $post_id = wp_insert_post( $postStructure, $wp_error );
    $catRes= wp_set_post_terms($post_id, array($next_cat_id), 'product_cat');

    update_post_meta( $post_id, '_visibility', 'visible' );
    ...
    update_post_meta( $post_id, '_format', $nextUpdatedTemplate['format'] );

    $template_data= '';
    if (!empty($nextUpdatedTemplate['template'])) {
        if ( file_exists($nextUpdatedTemplate['template'])) {
            $template_data = file_get_contents($nextUpdatedTemplate['template']);

            $images =$nextUpdatedTemplate['images'];
            $file_path_obj= json_decode($images);
            if ( is_object($file_path_obj) and !empty($file_path_obj->bgImage1) ) {
                $filename= BACKEND_SITE_DIR . $file_path_obj->bgImage1;
                $ret= fetch_media($filename, $post_id);
            }

            $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), 'single-post-thumbnail' );
            $imageUrl= '';
            if (!empty($image[0])) {
                $imageUrl= $image[0];
            }

            $template_data_array= json_decode($template_data);
            foreach( $template_data_array->objects as $next_key => $next_template_data ) {
                if ( is_object($next_template_data) and $next_template_data->type == "image" and !empty($next_template_data->src) ) {
                    $template_data_array->objects[$next_key]->src= $imageUrl;
                }

            }
            update_post_meta( $post_id, '_template_data', json_encode($template_data_array) );
        }
    }

    update_post_meta( $post_id, '_sale_price_dates_from', "" );
    ...
    update_post_meta( $post_id, '_tax_status', "none" );
    return $post_id;
}

function changeUserOptionsLabels($user_options)
{
    $user_options_array= json_decode($user_options);
    $user_options_array= get_object_vars($user_options_array);
    $_ret_user_options_array= array();
    foreach( $user_options_array as $next_key=>$next_user_option ) {
        $_ret_user_options_array[ str_replace('-','_', $next_key) ]= $next_user_option;
    }
    return json_encode($_ret_user_options_array);
}

function getWPIdOfCategory($categoryId) { //get external ID of category by its wp ID
    global $ext_categories_table_name, $wpdb;
    $categorySql = " select  * from `" . $ext_categories_table_name . "`  where `id`= '" . $categoryId . "' ";
    $categoryRow = $wpdb->get_results($categorySql, ARRAY_A);
    if ( empty($categoryRow[0]['wp_id']) ) return false;
    return $categoryRow[0]['wp_id'];
}

/* Import media from url
 *
 * @param string $file_url URL of the existing file from the original site
 * @param int $post_id The post ID of the post to which the imported media is to be attached
 *
 * @return boolean True on success, false on failure
 */

function fetch_media($file_url, $post_id) { // http://nlb-creations.com/2012/09/26/how-to-programmatically-import-media-files-to-wordpress/
    require_once(ABSPATH . 'wp-load.php');
    require_once(ABSPATH . 'wp-admin/includes/image.php');
    global $wpdb;

    if(!$post_id) {
        return false;
    }
    ...
    return true;
}
