<?php
if ( ! defined( 'nsnClass_wooImport' ) ) {
    exit; // Exit if accessed directly
}
class Models
{
    protected $m_ext_rows_deleted;
    protected $m_ext_categories;
    protected $m_ext_templates;
    protected $m_ext_fonts;
    protected $m_wpdb;

    public function __construct($wpdb)
    {
        $this->m_wpdb = $wpdb;
        $this->m_ext_rows_deleted = 'dp_rows_deleted';
        $this->m_ext_categories = 'dp_categories';
        $this->m_ext_templates = 'dp_templates';
        $this->m_ext_fonts = 'dp_fonts';
    }

    public function get_ext_rows_deleted_table_name()
    {
        return $this->m_ext_rows_deleted;
    }

    public function get_ext_categories_table_name()
    {
        return $this->m_ext_categories;
    }

    public function get_ext_templates_table_name()
    {
        return $this->m_ext_templates;
    }


    private $m_templateFormatValueArray = Array('a6' => 'A6', 'a5' => 'A5', 'a4' => 'A4', 'a3' => 'A3', 'a2' => 'A2', 'fb_cov' => 'Facebook Cover');
    public function getTemplateFormatValueArray()
    {
        $ResArray = array();
        foreach ($this->m_templateFormatValueArray as $Key => $Value) {
            $ResArray[] = array('key' => $Key, 'value' => $Value);
        }
        return $ResArray;
    }

    public function getTemplateFormatLabel($status)
    {
        if (!empty($this->m_templateFormatValueArray[$status])) {
            return $this->m_templateFormatValueArray[$status];
        }
        return $status;
    }
    private $m_templateOrientationValueArray = Array('portrait' => 'Portrait', 'landscape' => 'Landscape');
    public function getTemplateOrientationValueArray()
    {
        $ResArray = array();
        foreach ($this->m_templateOrientationValueArray as $Key => $Value) {
            $ResArray[] = array('key' => $Key, 'value' => $Value);
        }
        return $ResArray;
    }


    public function getCategoriesList($outputFormatCount = false, $paged = '', $filters = array(), $sort = '', $sort_direction = '') {// retrieve list of data by given parameters
        $hasWhereCond= false;
        if($outputFormatCount) { // return number of resulting rows - prepare sql for this
            $sqlStatement = "SELECT count(distinct " . $this->m_ext_categories . ".id) AS rowsCount FROM `" . $this->m_ext_categories . "`";
        } else {  // return result set with all fields of resulting rows
            $sqlStatement = "SELECT distinct `" . $this->m_ext_categories . "`.* "." FROM `" . $this->m_ext_categories . "`";
        }
        if ( !empty($filters['name']) ) {  // set filter on name field
            $sqlStatement.= ( $hasWhereCond? "":" where " ) . ( $hasWhereCond? " AND ":"" ) . " name like '%".$filters['name']."%' ";
            $hasWhereCond= true;
        }
        if ( !empty($filters['strict_name']) ) { // set strict filter on name field
            $sqlStatement.= ( $hasWhereCond? "":" where " ) .  ( $hasWhereCond? " AND ":"" ) . " name = '".$filters['strict_name']."' ";
            $hasWhereCond= true;
        }

        if ( !empty($filters['description']) ) { // set filter on description field
            $sqlStatement.= ( $hasWhereCond? "":" where " ) . ( $hasWhereCond? " AND ":"" ) . " description like '%".$filters['description']."%' ";
            $hasWhereCond= true;
        }


        if ( !empty($filters['status']) ) {  // set filter on status field
            $sqlStatement.= ( $hasWhereCond? "":" where " ) .  ( $hasWhereCond? " AND ":"" ) . " status = '".$filters['status']."' ";
            $hasWhereCond= true;
        }

        if ( !empty($filters['in_status']) and is_array($filters['in_status']) and count($filters['in_status']) > 0 ) { // set filter on status field by array of values
            $subCondition= '';
            $l= count($filters['in_status']);
            ...
            $hasWhereCond= true;
        }

        if ( !empty($filters['wp_id']) ) {// set filter on id field != value
            $sqlStatement.= ( $hasWhereCond? "":" where " ) .  ( $hasWhereCond? " AND ":"" ) . " wp_id = '".$filters['wp_id']."' ";
            $hasWhereCond= true;
        }


        if (!$outputFormatCount) { // set order by if we do not return count of rows
            if (empty($sort)) {
                $sort = 'name';
                $sort_direction = 'desc';
            }
            if (!empty($sort)) {
                $sqlStatement .= " order by " . $sort . " ";
            }
            if (!empty($sort) and !empty($sort_direction)) {
                $sort_direction = getLimitedValue($sort_direction, array('asc', 'desc'), 'asc');
            }
            if (!empty($sort_direction)) {
                $sqlStatement .= " " . $sort_direction . " ";
            }
        } // if (!$outputFormatCount) { // set order by if we do not return count of rows


        $limitSqlSubstr= '';
        if ( !empty($paged) and !empty($filters['itemsPerPage']) ) { // if page is set - then calculate and set limit condition
            $itemsPerPage= (int)$filters['itemsPerPage'];
            $limitStart= ($paged - 1) * $itemsPerPage ;
            $limitSqlSubstr= ' LIMIT ' . $itemsPerPage . " OFFSET  " . $limitStart;
        }
        $sqlStatement.= $limitSqlSubstr;
        if ( !empty($filters['limit']) and is_numeric($filters['limit'])) { // set number of rows
            $sqlStatement.=  " limit " . $filters['limit'] . " offset 0 ";
        }

        $retResult = $this->m_wpdb->get_results($sqlStatement, ARRAY_A);
        if($outputFormatCount) {
            return $retResult[0]['rowsCount']; // return number of resulting rows
        } else{
            return $retResult; // return resulting array with all fields of resulting rows
        }
    }   // public function getCategoriesList($outputFormatCount = false, $page = '', $filters = array(), $sort = '', $sort_direction = '') {


    public function getTemplatesList($outputFormatCount = false, $paged = '', $filters = array(), $sort = '', $sort_direction = '') { // retrieve list of data by given parameters
        $hasWhereCond= false;
        if($outputFormatCount) {  // return number of resulting rows - prepare sql for this
            $sqlStatement = "SELECT count(distinct " . $this->m_ext_templates . ".id) AS rowsCount FROM `" . $this->m_ext_templates . "`";
        } else {  // return result set with all fields of resulting rows
            $sqlStatement = "SELECT distinct `" . $this->m_ext_templates . "`.* "." FROM `" . $this->m_ext_templates . "`";
        }
        if ( !empty($filters['name']) ) { // set filter on name field
            $sqlStatement.= ( $hasWhereCond? "":" where " ) . ( $hasWhereCond? " AND ":"" ) . " name like '%".$filters['name']."%' ";
            $hasWhereCond= true;
        }
        if ( !empty($filters['strict_name']) ) { // set strict filter on name field
            $sqlStatement.= ( $hasWhereCond? "":" where " ) .  ( $hasWhereCond? " AND ":"" ) . " name = '".$filters['strict_name']."' ";
            $hasWhereCond= true;
        }

        if ( !empty($filters['description']) ) { // set filter on description field
            $sqlStatement.= ( $hasWhereCond? "":" where " ) . ( $hasWhereCond? " AND ":"" ) . " description like '%".$filters['description']."%' ";
            $hasWhereCond= true;
        }

        if ( !empty($filters['category_id']) ) { // set filter on category_id field
            $sqlStatement.= ( $hasWhereCond? "":" where " ) .  ( $hasWhereCond? " AND ":"" ) . " category_id = '".$filters['category_id']."' ";
            $hasWhereCond= true;
        }

        if ( !empty($filters['format']) ) { // set filter on format field
            $sqlStatement.= ( $hasWhereCond? "":" where " ) .  ( $hasWhereCond? " AND ":"" ) . " format = '".$filters['format']."' ";
            $hasWhereCond= true;
        }

        if ( !empty($filters['status']) ) {  // set filter on status field
            $sqlStatement.= ( $hasWhereCond? "":" where " ) .  ( $hasWhereCond? " AND ":"" ) . " status = '".$filters['status']."' ";
            $hasWhereCond= true;
        }

        if ( !empty($filters['in_status']) and is_array($filters['in_status']) and count($filters['in_status']) > 0 ) {  // set filter on status field - array of values possible
            $subCondition= '';
            $l= count($filters['in_status']);
            for( $i= 0; $i< $l; $i++ ) {
                $subCondition.= " '". $filters['in_status'][$i] . ( $i< $l-1? "', " : "'" );
            }
            $sqlStatement.= ( $hasWhereCond? "":" where " ) .  ( $hasWhereCond? " AND ":"" ) . " status in ( ".$subCondition." ) ";
            $hasWhereCond= true;
        }


        if ( !empty($filters['wp_id']) ) {  // set filter on wp_id field
            $sqlStatement.= ( $hasWhereCond? "":" where " ) .  ( $hasWhereCond? " AND ":"" ) . " wp_id = '".$filters['wp_id']."' ";
            $hasWhereCond= true;
        }

        if ( !empty($filters['categories_to_skip']) and is_array($filters['categories_to_skip'] ) ) { // array of categories which must be not in result set
            ...
        }

        if (!$outputFormatCount) { // set order by if we do not return count of rows
            if (empty($sort)) {
                $sort = 'name';
                $sort_direction = 'desc';
            }
            if (!empty($sort)) {
                $sqlStatement .= " order by " . $sort . " ";
            }
            if (!empty($sort) and !empty($sort_direction)) {
                $sort_direction = getLimitedValue($sort_direction, array('asc', 'desc'), 'asc');
            }
            if (!empty($sort_direction)) {
                $sqlStatement .= " " . $sort_direction . " ";
            }
        } // if (!$outputFormatCount) { // set order by if we do not return count of rows


        $limitSqlSubstr= '';
        if ( !empty($paged) and !empty($filters['itemsPerPage']) ) { // if page is set - then calculate and set limit condition
            ...
        }
        $sqlStatement.= $limitSqlSubstr;
        if ( !empty($filters['limit']) and is_numeric($filters['limit'])) { // set number of rows
            ...
        }


        $retResult = $this->m_wpdb->get_results($sqlStatement, ARRAY_A);
        if($outputFormatCount) {
            return $retResult[0]['rowsCount']; // return number of resulting rows
        } else{
            return $retResult; // return resulting array with all fields of resulting rows
        }
    }   // public function getTemplatesList($outputFormatCount = false, $page = '', $filters = array(), $sort = '', $sort_direction = '') {

    public function getRowsDeletedList($outputFormatCount = false, $paged = '', $filters = array(), $sort = '', $sort_direction = '') { // retrieve list of ID of rows for deletion in wordpress
        $hasWhereCond= false;
        if($outputFormatCount) { // return number of resulting rows - prepare sql for this
            $sqlStatement = "SELECT count(distinct " . $this->m_ext_rows_deleted . ".id) AS rowsCount FROM `" . $this->m_ext_rows_deleted . "`";
        } else { // return result set with all fields of resulting rows
            $sqlStatement = "SELECT distinct `" . $this->m_ext_rows_deleted . "`.* "." FROM `" . $this->m_ext_rows_deleted . "`";
        }
        if ( !empty($filters['delete_type']) ) {  // set filter on delete_type field
            $sqlStatement.= ( $hasWhereCond? "":" where " ) .  ( $hasWhereCond? " AND ":"" ) . " delete_type = '".$filters['delete_type']."' ";
            $hasWhereCond= true;
        }

        if ( !empty($filters['id']) ) {  // set filter on id field
            $sqlStatement.= ( $hasWhereCond? "":" where " ) .  ( $hasWhereCond? " AND ":"" ) . " id = '".$filters['id']."' ";
            $hasWhereCond= true;
        }


        if (!$outputFormatCount) { // set order by if we do not return count of rows
            if (empty($sort)) {
                $sort = 'delete_type';
                $sort_direction = 'desc';
            }
            if (!empty($sort)) {
                $sqlStatement .= " order by " . $sort . " ";
            }
            if (!empty($sort) and !empty($sort_direction)) {
                $sort_direction = getLimitedValue($sort_direction, array('asc', 'desc'), 'asc');
            }
            if (!empty($sort_direction)) {
                $sqlStatement .= " " . $sort_direction . " ";
            }
        } // if (!$outputFormatCount) { // set order by if we do not return count of rows


        $limitSqlSubstr= '';
        if ( !empty($paged) and !empty($filters['itemsPerPage']) ) { // if page is set - then calculate and set limit condition
            $itemsPerPage= (int)$filters['itemsPerPage'];
            $limitStart= ($paged - 1) * $itemsPerPage ;
            $limitSqlSubstr= ' LIMIT ' . $itemsPerPage . " OFFSET  " . $limitStart;
        }
        $sqlStatement.= $limitSqlSubstr;
        if ( !empty($filters['limit']) and is_numeric($filters['limit'])) { // set number of rows
            $sqlStatement.=  " limit " . $filters['limit'] . " offset 0 ";
        }

        $retResult = $this->m_wpdb->get_results($sqlStatement, ARRAY_A);
        if($outputFormatCount) {
            return $retResult[0]['rowsCount'];  // return number of resulting rows
        } else{
            return $retResult; // return resulting array with all fields of resulting rows
        }
    }   // public function getRowsDeletedList($outputFormatCount = false, $page = '', $filters = array(), $sort = '', $sort_direction = '') { // retrieve list of ID of rows for deletion in wordpress


    // $this->FlyerTemplates->fonts_get()
    public function getFontsList($outputFormatCount = false, $paged = '', $filters = array(), $sort = '', $sort_direction = '')
    { // dp_fonts
        //return $this->fonts_get();
        $hasWhereCond= false;
        if($outputFormatCount) {
            $sqlStatement = "SELECT count(distinct " . $this->m_ext_fonts . ".id) AS rowsCount FROM `" . $this->m_ext_fonts . "`";
        } else {
            $sqlStatement = "SELECT distinct `" . $this->m_ext_fonts . "`.* "." FROM `" . $this->m_ext_fonts . "`";
        }


        if (!$outputFormatCount) { // set order by if we do not return count of rows
            if (empty($sort)) {
                $sort = 'group_name';
                $sort_direction = 'desc';
            }
            if (!empty($sort)) {
                $sqlStatement .= " order by " . $sort . " ";
            }
            if (!empty($sort) and !empty($sort_direction)) {
                $sort_direction = getLimitedValue($sort_direction, array('asc', 'desc'), 'asc');
            }
            if (!empty($sort_direction)) {
                $sqlStatement .= " " . $sort_direction . " ";
            }
        } // if (!$outputFormatCount) { // set order by if we do not return count of rows


        $limitSqlSubstr= '';
        if ( !empty($paged) and !empty($filters['itemsPerPage']) ) {
            $itemsPerPage= (int)$filters['itemsPerPage'];
            $limitStart= ($paged - 1) * $itemsPerPage ;
            $limitSqlSubstr= ' LIMIT ' . $itemsPerPage . " OFFSET  " . $limitStart;
        }
        $sqlStatement.= $limitSqlSubstr;
        if ( !empty($filters['limit']) and is_numeric($filters['limit'])) {
            $sqlStatement.=  " limit " . $filters['limit'] . " offset 0 ";
        }


        $retResult = $this->m_wpdb->get_results($sqlStatement, OBJECT);
        $resArray= array();
        foreach( $retResult as $nextKey=>$nextValue ) {
            $resArray[$nextValue->id]= $nextValue;
        }

        if($outputFormatCount) {
            return $retResult[0]['rowsCount'];
        } else{
            //return $retResult;
            return $resArray;
        }
    }

}