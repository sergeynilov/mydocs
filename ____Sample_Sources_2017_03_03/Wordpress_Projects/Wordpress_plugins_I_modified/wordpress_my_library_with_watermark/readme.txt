I had a task where images were products in wooCommerce orders and I had :

1) To show/hide watermark depending on status or wooCommerce order :
for Completed orders no watermark(user payed for it), for the rest of orders status image is with watermark(user has not payed for it yet).
I used WP Image Protect Lite plugin(https://wordpress.org/plugins/wp-image-protect/) for it. 
Advantage of this plugin is that it does not modify source image itself,
but adds watermak "on fly". Also this plugun is easy to use and good configurable.
Please view printscreens "Completed_without_watermark.png" and "Processing_with_watermark.png" in the directory.

I had to edit source of this plugin to make functionality I needed in different pages of wordpress site.

2) To make "My Library" page, where list of ordered images is shown with status, price etc and with link
to page with image, where image of order with Completed status(user payed for it) can be downloaded without watermark.
Please view printscreens "my_library_page.png" in the directory.

I did to create "My Library" page :
<?php
...
add_shortcode('show_my_library',  'show_my_library');
function show_my_library()
{
    $user_ID = get_current_user_id();
    $user_orders = get_orders_by_user_id($user_ID);
    if (empty($user_orders) or count($user_orders) == 0) {
        echo '<b>You have no orders!</b>';
        return;
    }
    ?>


    <table class="shop_table order_details">
        <thead>
        <tr>
            <th class="product-name">Order&nbsp;#</th>
            <th class="product-name">Template</th>
            <th class="product-total">Status</th>
            <th class="product-total">Total</th>
            <th class="product-total">Posted</th>
            <th class="product-total">&nbsp;</th>
        </tr>
        </thead>
        <tbody>

        <?php
        $itemsRows = array();
        foreach ($user_orders as $nextKey => $nextOrder) {
            $order = new WC_Order($nextOrder->ID);
            $items = $order->get_items();
            foreach ($items as $nextKey => $nextItem) {
                $item_qty = '';
                $item_product_id = '';
                $item_line_total = '';
                if (!empty($nextItem['item_meta']) and is_array($nextItem['item_meta'])) {
                    foreach ($nextItem['item_meta'] as $nextItemMetaKey => $nextItemMetaValue) {

                        if ($nextItemMetaKey == '_qty') {
                            if (is_array($nextItemMetaValue) and !empty($nextItemMetaValue[0])) {
                                $item_qty = $nextItemMetaValue[0];
                            }
                        }

                        if ($nextItemMetaKey == '_product_id') {
                            if (is_array($nextItemMetaValue) and !empty($nextItemMetaValue[0])) {
                                $item_product_id = $nextItemMetaValue[0];
                            }
                        }

                        if ($nextItemMetaKey == '_line_total') {
                            if (is_array($nextItemMetaValue) and !empty($nextItemMetaValue[0])) {
                                $item_line_total = $nextItemMetaValue[0];
                            }
                        }

                    }

                }
                $itemsRows[] = array('order_item_id' => $nextKey, 'order_id' => $nextOrder->ID, 'order_status' => $nextOrder->post_status, 'template_name' => $nextItem['name'], 'post_date' => $nextOrder->post_date, 'post_date_unix' => strtotime($nextOrder->post_date), 'post_status' => $nextOrder->post_status, 'guid' => $nextOrder->guid, 'item_qty' => $item_qty, 'item_product_id' => $item_product_id, 'item_line_total' => $item_line_total/*, 'item_meta'=> $nextItem['item_meta']*/);
            }
        }
        uasort($itemsRows, 'sortItemsRows');
        foreach ($itemsRows as $nextKey => $nextItem) {
            echo '<tr>';
            echo '<td> <a target="_blank" href="' . get_home_url() . '/' . '/my-account/view-order/' . $nextItem['order_id'] . '" a>' . $nextItem['order_id'] . '</a></td>';
            echo '<td>' . $nextItem['template_name'] . '</td>';
            echo '<td>' . ucfirst(str_replace('wc-', '', $nextItem['post_status'])) . '</td>';
            echo '<td>' . wc_price($nextItem['item_line_total']) . ' for ' . $nextItem['item_qty'] . ' item' . ($nextItem['item_qty'] > 1 ? "s" : "") . '</td>';
            echo '<td>' . strftime('%B %d, %Y %H:%M%p', strtotime($nextItem['post_date'])) . '</td>';
            echo '<td> <a href=" ' . WP_PLUGIN_URL . '/woo-dysplo-import/view_template.php?post_id=' . $nextItem['order_id'] . "&item_id=" . $nextItem['order_item_id'] . '" rel="nofollow" class="">View</a>
 </td>';
            echo '</tr>';
        }
        ?>
        </tbody>
    </table>
<?php
}
