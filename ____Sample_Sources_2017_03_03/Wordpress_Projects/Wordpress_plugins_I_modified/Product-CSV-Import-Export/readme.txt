I needed to import wooCommerce products to other site and after testing some plugings for this I got free plugin
Product CSV Import Export(BASIC) and have to modify it, as it does a lot, but not all what I need:

1) I made that while export all product's images to copied to subdirectory with resulting csv file, in this way I could copy csv file with all product's created images.
Importing this csv file subdirectory with images must be in the same directory, say in "/wp-content/uploads/WOOEXIM_Import" of site where import made.

2) I added possibility to import/export all product's attributes, which will be imported with products.
Making export admin can select which attributes he wants to import, like:
http://imgur.com/a/qSyPS
checked attributes would be imported for products.
Importing attributes there is checking, so existing attributes would not be copied, to avoid error while importing.

3) I added possibility to import/export all users of system, as they are used in product's comments.
Making export admin can not select which users he wants to import, like:
http://imgur.com/a/Vk2FT

All users would be imported into the system.
Importing users there is checking if such user exists(by email), so existing user will be used for comments.

4) In the list of products I added possibility to check which products must be imported.
On page where admin selects which csv file to upload I added new parameter "Search existing posts by" with "by sku" and "by title".
If in system already there are similar products by selected above criteria, the products would not be checked by default(but admin can check them)
and there is message that there are similar products, like:
http://imgur.com/a/mrdOG

5) So in case of attributes and products admin of site can decide which data to import, if he does not need to upload all.

6) At this link
http://dev2.softreactor.com/wp-plugings/extended-products-search
you can see demo of Extended products search with data(users, attributes, products with images, comments...) which were exported from my local server, uploaded
at this server(csv file with subdirectory of images) and imported to this wordpress site.
