<?php
define( 'DOING_AJAX', true );

require_once( '../../../wp-load.php' );

if ( ! defined( 'NSN_WooExtSearch' ) ) {
    exit; // Exit if accessed directly
}
require_once( 'appWooExtSearchControl.php');


class ajaxFrontendControl extends  appNSN_WooExtSearchControl
{
    /*  private $isDebug; */

    private $m_service_category_for_attrs_array= array();
    private $m_fields_to_show = array();
    private $m_attrs_to_show = array();
    private $m_woo_ext_search_options = '';
    private $m_sample_prices_list = array();
    public function __construct($containerClass, $wpdb)
    {
        global $session;
        parent::__construct($containerClass, $wpdb, $session);
        $this->setValidationRules( array() ); // must be called AFTER __construct
        $this->m_woo_ext_search_options = get_option('nsn-woo-ext-search-');
        $this->isDebug= true;
    }

    protected function readRequestParameters()
    {
        $this->m_action = $this->getParameter( 'action' );
        if ( isset($_GET['action']) and $_GET['action'] == 'upload-artist-photo' ) {
            $this->m_action= 'upload-artist-photo';
        }
    } // protected function readRequestParameters() {


    public function main() {
        $this->m_plugin_dir = trailingslashit(WP_PLUGIN_DIR.'/'.dirname(plugin_basename(__FILE__)));
        require_once(  'lib/app-nsn-woo-tools-data.php');
        parent::main();
        $this->isDebug= false;
//        if ($this->isDebug) echo '<pre>$_POST::'.print_r($_POST,true).'</pre>';
        if ($this->isDebug) echo '<pre>$_REQUEST::'.print_r($_REQUEST,true).'</pre>';
        if ($this->isDebug) echo '<pre>$this->m_GETDataArray::'.print_r($this->m_GETDataArray,true).'</pre>';
        if ($this->isDebug) echo '<pre>$this->m_POSTDataArray::'.print_r($this->m_POSTDataArray,true).'</pre>';
        $action = $this->getParameter( 'action' );
        $m_parentControl = $this;
        if ( $this->m_is_trimDataBeforeSave ) {
            foreach( $_REQUEST as $nextKey=>$nextValue ) {
                if ( !is_array($nextValue) ) {
                    $_REQUEST[$nextKey] = trim($nextValue);
                }
            }
        }

        if (empty($_REQUEST['action']) ) {
            echo json_encode( array( 'error_code' => 1, 'error_message'=>'action parameter must be!', 'current_user_id'=> $m_parentControl->m_currentUserId ) );
            exit;
        }
        if ($_REQUEST['action'] == 'get-woo-products-by-filter') {  // get list of products by filters
            $fields_to_show_array = ((!empty($this->m_woo_ext_search_options['fields_to_show_array']) and is_array($this->m_woo_ext_search_options['fields_to_show_array'])) ? $this->m_woo_ext_search_options['fields_to_show_array'] : array());

            $this->m_service_category_for_attrs_array = ((!empty($this->m_woo_ext_search_options['service_category_for_attrs_array']) and is_array($this->m_woo_ext_search_options['service_category_for_attrs_array'])) ? $this->m_woo_ext_search_options['service_category_for_attrs_array'] : array());

            foreach ($fields_to_show_array as $next_fields_to_show_name => $next_fields_to_show_value) {
                if ($next_fields_to_show_value == 'yes' and !empty($next_fields_to_show_name)) {
                    $this->m_fields_to_show[$next_fields_to_show_name] = true;
                }
            }


            $this->m_attrs_to_show = ((!empty($this->m_woo_ext_search_options['attribute_option_values']) and is_array($this->m_woo_ext_search_options['attribute_option_values'])) ? $this->m_woo_ext_search_options['attribute_option_values'] : array());
            $attrs_to_show_name_array= array();
            foreach( $this->m_attrs_to_show as $next_key => $next_attrs_to_show ) {
                if ( $next_attrs_to_show['show_attribute_in_extended_search'] == 'yes' ) {
                    $this->m_fields_to_show[$next_attrs_to_show['attr_name']]= 1;
                    $attrs_to_show_name_array[]= $next_attrs_to_show['attr_name'];
                }
            }

            $orderby = $this->getParameter('orderby', 'date');
            $items_per_page = $this->getParameter('items_per_page', '');
            $partial_sku = $this->getParameter('partial_sku', '');
            $sku = $this->getParameter('sku', '');
            $partial_title = $this->getParameter('partial_title', '');
            $post_title = $this->getParameter('post_title', '');
            $rating_min = $this->getParameter('rating_min', '');
            $rating_max = $this->getParameter('rating_max', '1');
            $only_in_stock = $this->getParameter('only_in_stock', 1);
            $page = $this->getParameter('page', '1');

            $filter_categories_list = $this->getParameter('categories_list', array());
            $attrs_checked_list = $this->getParameter('attrs_checked_list', array());

            if ( !empty($this->m_woo_ext_search_options['show_in_extended_search_price_ranges']) ) { //=> 5, 10, 20, 50, 100
                $this->m_sample_prices_list= nsnWooToolsData::prices_string_into_list( $this->m_woo_ext_search_options['show_in_extended_search_price_ranges'] );
            }
            $filter_prices_list = $this->getParameter('prices_list',array());
            $prices_conditions= array();
            $query_array= array();
            foreach( $filter_prices_list as $next_key => $next_filter_price ) {
               ...
            }


            $attrs_checked_tax_query_array= array();
            foreach( $attrs_checked_list as $next_key=>$next_attrs_checked ) {
               ...
            }



            $filter_tags_list = $this->getParameter('tags_list',array());
            $tags_list= array();
            if (!empty($filter_tags_list) and is_array($filter_tags_list)) {
                foreach( $filter_tags_list as $next_tag ) {
                    if ( !empty($next_tag) ) {
                        $tags_list[]= $next_tag;
                    }
                }
            }
            $categories_list= array();
            if (!empty($filter_categories_list) and is_array($filter_categories_list)) {
                foreach( $filter_categories_list as $next_category ) {
//                    echo '<pre>$next_category::'.print_r($next_category,true).'</pre>';
                    if ( !empty($next_category) ) {
                        $categories_list[]= $next_category;
                    }
                }
            }

            $meta_query_array= array();
            if ( ( count($query_array) > 0 ) or (0) ) {
                $meta_query_array = array('relation' => 'AND');
            }
            foreach( $query_array as $next_key => $next_query ) {
                $meta_query_array[]= $next_query;
            }


//            $sku= 'battery';
            if ( !empty($sku) ) {
                $meta_query_array[]= array(
                ...
                );
            }

            if ( $only_in_stock ) {
                $meta_query_array[]= array(
                ...
                );
            }

            $args = array(         //$filter_categories_list
            ...
            );
            if ( $partial_title ) {
                $args['sentence'] = true;
            } else {
                $args['exact'] = true;
            }

            if ( count($meta_query_array) > 0 ) {
                $args['meta_query'] = $meta_query_array;
            }

            if ( count($attrs_checked_tax_query_array) > 0 ) {
                $args['tax_query']= $attrs_checked_tax_query_array;
            }


            if ( count($categories_list) > 0 ) {
                $args['tax_query']= array(
                    array(
                    ...
                    )
                );
            }



            $products_list = array();

            $page_rows_start = -1;
            $page_rows_end = -1;

            $posts_per_page = (int)$this->m_woo_ext_search_options['products_per_page'];
            if ( !empty($items_per_page) and (int)$items_per_page > 0 ) {
                $posts_per_page= $items_per_page;
            }

//            $offset = ($page - 1) * $posts_per_page;
            if ( !empty($posts_per_page) and (int)$posts_per_page > 0) {
                $page_rows_start = ($page - 1) * $posts_per_page;
                $page_rows_end = $page_rows_start + $posts_per_page;
            }
            if (empty($posts_per_page) or !is_numeric($posts_per_page)) $posts_per_page= 10;

            $navigation_classes= array('current_page'=> 'nsn_woo_ext_search_current_page', 'next_link_page'=> 'nsn_woo_ext_search_next_link_page', 'external_div'=> 'nsn_woo_ext_search_external_div');

            $raw_products = get_posts( $args );
            $additive_attrs= array( 'woocommerce_price_format'=> get_woocommerce_price_format(), 'woocommerce_currency_symbol'=> get_woocommerce_currency_symbol( get_woocommerce_currency() ), 'decimal_separator' => wc_get_price_decimal_separator(), 'thousand_separator' => wc_get_price_thousand_separator(), 'decimals' => wc_get_price_decimals(), 'date_format'=> get_option('date_format'), 'time_format'=> get_option('time_format')   );
            $products= array();
            foreach( $raw_products as $next_key => $next_raw_product_post ) {
                ...
                $add_product_to_results= true;
                if ( !empty($tags_list) and is_array($tags_list) and count($tags_list) > 0 ) {
                    $product_tags_list = nsnWooToolsData::get_tags_list_by_product($next_raw_product_post, false);
                    if ( count($product_tags_list) == 0 ) { // this product has no tags at all
                        $add_product_to_results = false;
                    }
                    foreach ($product_tags_list as $next_key_product_tag => $next_product_tag) {
                        if (!in_array($next_product_tag->term_id, $tags_list)) {
                            $add_product_to_results = false;
                            break;
                        }
                    }
                } // if ( !empty($tags_list) and is_array($tags_list) and count($tags_list) > 0 ) {

                if ( $add_product_to_results ) {
                    $products[] = $woo_next_product;
                }
            }

            $paginationMaxPage = (int)( count($products) / $posts_per_page ) + ( count($products) % $posts_per_page == 0 ? 0 : 1 );
            if ($page > $paginationMaxPage) $page = 1;
            $navigationHTML = $this->makePageNavigation( $page, $posts_per_page, $paginationMaxPage, '', '', false, " nsn_woo_ext_search_frontendFuncsObj.loadProductsPage( '#page#' ) ", false, '', $navigation_classes );
            $index= 0;
            foreach( $products as $next_woo_product ) {
                if ( $page_rows_start >= 0 and $page_rows_end > 0 and $page_rows_start<= $index and $page_rows_end > $index ) {
                    $products_list[] = $next_woo_product;
                }
                $index++;
            }

            $data= array('products_list'=>$products_list, 'page'=> $page, 'total_products_count'=> count($products), 'filter_categories_list'=>$filter_categories_list,  'filter_prices_list'=> $filter_prices_list, 'fields_to_show' => $this->m_fields_to_show, 'service_category_for_attrs_array'=> $this->m_service_category_for_attrs_array, 'attrs_to_show'=> $this->m_attrs_to_show, 'navigationHTML' => $navigationHTML );
            $ret= twigpress_render_twig_template( $data, "white-simple/woo_ext_products_list.twig", false );
//            echo '<pre>$ret::'.print_r(htmlspecialchars($ret),true).'</pre>';
            echo json_encode( array( 'error_code' => 0, 'error_message'=>'', 'html'=> $ret , 'html'=> $ret , 'args'=> $args, 'products_count'=> count($products_list ) , 'total_products_count'=> count($products ) ) );
            return;
        }   // if ($_REQUEST['action'] == 'get-woo-products-by-filter') {  // get list of products by filters
/////////////////////////////////////////////

        if ($_REQUEST['action'] == 'load_bookmark_by_title') {  // save bookmark
            $user_id = (int)$this->getParameter('user_id', '');
            if (empty($user_id) or $user_id < 0 ) {
                echo json_encode( array( 'error_code' => 1, 'error_message'=>'You must login to system to make this operation !', 'current_user_id'=> $user_id ) );
                exit;
            }
            $bookmark_title = $this->getParameter('bookmark_title', '');
            $bookmarks_array = get_option($this->m_woo_ext_search_options['meta_info_key'].'bookmark-'.$user_id);
            foreach( $bookmarks_array as $next_bookmark ) {
                if ( strtolower(trim($next_bookmark['title'])) == strtolower(trim($bookmark_title)) ) {
                    echo json_encode( array( 'error_code' => '0', 'error_message'=> "", 'bookmark'=> $next_bookmark ) );
                    return;
                }
            }
            echo json_encode( array( 'error_code' => '1', 'error_message'=>"Bookmark '" . $bookmark_title . "' was not found !" ) );
            return;

        }
        if ($_REQUEST['action'] == 'load_bookmarks') {  // load bookmarks by user_id
            $user_id = $this->getParameter('user_id', '');
            if (empty($user_id) or $user_id < 0 ) {
                echo json_encode( array( 'error_code' => 1, 'error_message'=>'You must login to system to make this operation !', 'current_user_id'=> $user_id ) );
                exit;
            }
            $bookmarks_array = get_option($this->m_woo_ext_search_options['meta_info_key'].'bookmark-' . $user_id, array());

            $default_bookmark_title= '';
            $default_bookmark_index= '';
            $index= 0;
            foreach( $bookmarks_array as $next_key => $next_bookmark ) {
                if ( $next_bookmark['is_default'] == ("yes") ) {
                    $default_bookmark_title= $next_bookmark['title'];
                    $default_bookmark_index= $index;
                    $bookmarks_array[$next_key]['title']= $next_bookmark['title'] .' ( default ) ';
                    break;
                }
                $index++;
            }
            echo json_encode( array( 'error_code' => '0', 'error_message'=>'', 'bookmarks_array'=> $bookmarks_array, 'default_bookmark_title'=> $default_bookmark_title,       'default_bookmark_index' => $default_bookmark_index ) );
            return;
        } // if ($_REQUEST['action'] == 'load_bookmarks') {  // load bookmarks by user_id


        if ($_REQUEST['action'] == 'remove_bookmark') {  // remove bookmark
            $user_id = $this->getParameter('user_id', '');
            $bookmark_title = $this->getParameter('bookmark_title', '');
            $bookmarks_array = get_option($this->m_woo_ext_search_options['meta_info_key'].'bookmark-'.$user_id, array());
            $saved_bookmarks_array= array();
            foreach( $bookmarks_array as $next_key=>$next_bookmark_struct ) {
                if ( strtolower($next_bookmark_struct['title']) == strtolower($bookmark_title) ) continue;
                $saved_bookmarks_array[$next_key]= $next_bookmark_struct;
            }
            $ret = update_option($this->m_woo_ext_search_options['meta_info_key'].'bookmark-'.$user_id, $saved_bookmarks_array);
            echo json_encode( array( 'error_code' => ($ret?0:1), 'error_message'=>'', 'html'=> '' ) );
        } // if ($_REQUEST['action'] == 'remove_bookmark') {  // remove bookmark


        if ($_REQUEST['action'] == 'save_bookmark') {   // save bookmark

            $user_id = $this->getParameter('user_id', '');
            if (empty($user_id) or $user_id < 0 ) {
                echo json_encode( array( 'error_code' => 1, 'error_message'=>'You must login to system to make this operation !', 'current_user_id'=> $user_id ) );
                exit;
            }
            $bookmark_title = $this->getParameter('bookmark_title', '');
            $is_default = $this->getParameter('is_default', 'no');
            $orderby = $this->getParameter('orderby', '');
            $items_per_page = $this->getParameter('items_per_page', '');
            $partial_sku = $this->getParameter('partial_sku', '');
            $partial_title = $this->getParameter('partial_title', '');
            $sku = $this->getParameter('sku', '');
            $post_title = $this->getParameter('post_title', '');
            $rating_min = $this->getParameter('rating_min', '');
            $rating_max = $this->getParameter('rating_max', '');
            $only_in_stock = $this->getParameter('only_in_stock', '');

            $categories_list = $this->getParameter('categories_list', array());
            $prices_list = $this->getParameter('prices_list', array());
            $attrs_checked_list = $this->getParameter('attrs_checked_list', array());
            $bookmarks_array = get_option($this->m_woo_ext_search_options['meta_info_key'].'bookmark-'.$user_id, array());
            if ( strtolower($is_default) == "yes" ) {
                foreach ($bookmarks_array as $next_key => $next_bookmark_struct) {
                    $bookmarks_array[$next_key]['is_default']= "no";
                }
            }

            $options_array= array( 'user_id'=> $user_id, 'title'=> $bookmark_title, 'is_default' => $is_default, 'orderby'=> $orderby, 'items_per_page'=> $items_per_page, 'partial_sku'=> $partial_sku, 'partial_title'=> $partial_title, 'sku'=> $sku , 'post_title'=> $post_title, 'rating_min'=> $rating_min, 'rating_max'=> $rating_max, 'only_in_stock'=> $only_in_stock, /*'sale_price'=> $sale_price, */ 'categories_list'=> $categories_list, 'prices_list'=> $prices_list, 'attrs_checked_list'=>$attrs_checked_list );
            $bookmarks_array[$bookmark_title]= $options_array;
            $ret = update_option($this->m_woo_ext_search_options['meta_info_key'].'bookmark-' . $user_id, $bookmarks_array);
            echo json_encode( array( 'error_code' => ($ret?0:1), 'error_message'=>'', 'html'=> '' ) );
            return;

        } //        if ($_REQUEST['action'] == 'save_bookmark') {  // save bookmark







    } // public function main() {



    protected function showRowsListDataForm( $parentControl ) {

    }
    protected function addRowDataForm( $parentControl ) {
    }

    protected function initFieldsAddRow( $parentControl )
    {
    }


    protected function editRowDataForm( $parentControl ) {

    }

    protected function doBulkAction($action) {
    } // protected function doBulkAction() {

    protected function saveRowData( $action, $editorItem ) {
    }

    protected function deleteRowData()
    {
    }


    protected function preparePageParameters($WithPage, $WithSort) // Prepare url for pagination with all fields in filter and sort and sort_direction
    {
        return '';
    } // protected function preparePageParameters($WithPage, $WithSort) // Prepare url for pagination with all fields in filter and sort and sort_direction


}

global $wpdb;
$ajaxFrontendControlObj = new ajaxFrontendControl(null,$wpdb);
$ajaxFrontendControlObj->main();
