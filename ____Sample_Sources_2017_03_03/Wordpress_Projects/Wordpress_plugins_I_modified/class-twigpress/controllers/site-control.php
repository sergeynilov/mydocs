<?php
if ( ! defined( 'NSN_WooExtSearch' ) ) {
    exit; // Exit if accessed directly
}
require_once( 'appWooExtSearchControl.php'); //wp-content/plugins/wooExtSearch/appWooExtSearchControl.php

class siteControl extends  appNSN_WooExtSearchControl
{
    protected $m_songEditorItem;
    protected $m_attrParams;
    protected $m_action;
    protected $m_currentPost;

    public function __construct($containerClass, $wpdb, $attrAparams, $action, $currentPost)
    {
        global $session;
        $this->m_labelNonce= 'site-editor-nonce';
        $this->m_labelNonceInput= 'site-editor-nonce-input';
        $this->m_pageUrl= '/wp-admin/admin.php?page=NSN_WooExtSearch-editor&';
        $this->m_plugin_url= $containerClass->getPluginUrl();
        $this->m_plugin_dir= $containerClass->getPluginDir();
        $this->m_plugin_name= $containerClass->getPluginName();
        $this->m_plugin_prefix= $containerClass->getPluginPrefix();
        $this->m_prices_list= $containerClass->getPricesList();
        $this->m_currency_symbol= get_woocommerce_currency_symbol( get_woocommerce_currency() );
        $this->m_attrParams= $attrAparams;
        $this->m_action= $action;
        $this->m_currentPost= $currentPost;
        parent::__construct($containerClass, $wpdb, $session);
        require_once( $this->m_pluginDir . '/lib/app-nsn-woo-tools-data.php'); 
        $this->isDebug= true;
        $this->m_is_insert= false;
    }

    protected function readRequestParameters() {  // before main
    } // protected function readRequestParameters() {

    public function main() {
        $m_parentControl= $this;

        if ( $this->m_action == 'siteNSN_WooExtSearchForm' ) { 

            $args = array(
                ...
            );
            $products_list = get_posts( $args );
            ...
            $show_number_of_products= !empty($woo_ext_search_options['show_in_extended_search_show_number_of_products']) ? $woo_ext_search_options['show_in_extended_search_show_number_of_products'] : 'no';

            $categories_list= nsnWooToolsData::get_categories_list( array( 'taxonomy' => 'product_cat') );

            if ( $show_number_of_products == 'yes' ) {
                foreach( $categories_list as $next_key=>$next_category ) {
                    foreach( $products_list as $next_product ) {
                        $category_array = nsnWooToolsData::get_category_by_post_id( $next_product->ID, 'product_cat', array('id'=>'name', 'get_all_keys'=>1));
                        ... 
                    }
                }
            } //             if ( $show_number_of_products == 'yes' )

        reset($products_list);
            $tags_list = nsnWooToolsData::get_products_tags_list(true); //http://stackoverflow.com/questions/31478603/get-woocommerce-products-tags-for-array-of-products
            if ( $show_number_of_products == 'yes' )
            foreach( $tags_list as $next_tag_key=>$next_tag ) {
                foreach ($products_list as $next_key => $next_product) {
                    $next_product_tags= nsnWooToolsData::get_tags_list_by_product($next_product);
                    foreach( $next_product_tags as $next_product_tag_key=>$next_product_tag ) {
                        if ($next_product_tag['slug'] == $next_tag['slug']) {
                            if (empty($tags_list[$next_tag_key]['products_count'])) {
                                $tags_list[$next_tag_key]['products_count'] = 1;
                            } else {
                                $tags_list[$next_tag_key]['products_count'] = $tags_list[$next_tag_key]['products_count'] + 1;
                            }
                        }
                    }
                }
            }
            reset($products_list);
            $attrs_options_list= array();
            if (  !empty($woo_ext_search_options['attribute_option_values']) and is_array($woo_ext_search_options['attribute_option_values'])  ) {
                foreach( $woo_ext_search_options['attribute_option_values'] as $next_attr ) { // any attribute for products
                    if ( strtolower($next_attr['show_attribute_in_extended_search']) != 'yes' ) continue;
                    $attrs_list = nsnWooToolsData::get_products_attrs_list($next_attr['attr_name'], true);
                    if ( $show_number_of_products == 'yes' ) {
                        foreach ($products_list as $next_key => $next_product) {
                            $next_attr_values = wc_get_product_terms($next_product->ID, 'pa_'.$next_attr['attr_name'], array('fields' => 'names'));
                            foreach ($next_attr_values as $next_next_attr_value) {
                                foreach ($attrs_list as $next_next_attrs_list_key => $next_next_attrs_list_value) {
                                    if ($next_next_attr_value == $next_next_attrs_list_value['name']) {
                                        if (empty($attrs_list[$next_next_attrs_list_key]['products_count'])) {
                                            $attrs_list[$next_next_attrs_list_key]['products_count'] = 1;
                                        } else {
                                            $attrs_list[$next_next_attrs_list_key]['products_count'] = $attrs_list[$next_next_attrs_list_key]['products_count'] + 1;
                                        }
                                    }
                                }
                            }
                        }
                    } //             if ( $show_number_of_products == 'yes' ) {
                    $attrs_options_list[]= array( 'attr_name'=> $next_attr['attr_name'], 'show_attribute_link_to_post'=> $next_attr['show_attribute_link_to_post'], 'show_attribute_in_post_products_list'=> $next_attr['show_attribute_in_post_products_list'], 'attrs_list'=>$attrs_list );
                } //foreach( $woo_ext_search_options['attribute_option_values'] as $next_attr ) { // any attribute for products
            } // if (  !empty($woo_ext_search_options['attribute_option_values']) and is_array($woo_ext_search_options['attribute_option_values'])  ) {


            if ( $show_number_of_products == 'yes' ) {
                foreach( $products_list as $next_key=>$next_product ) {
                ...
                }
            } //             if ( $show_number_of_products == 'yes' ) {

            $woo_ext_search_options['run_search_by_click']= 'both'; // both / link /button
            $products_pagination_per_page_list= array();
            if ( !empty($woo_ext_search_options['products_pagination_per_page_list']) ) {
                $products_pagination_per_page_list = nsnWooToolsData::preg_split('/,/', $woo_ext_search_options['products_pagination_per_page_list']);
            }
            reset($products_list);

            $data= array('categories_list'=>$categories_list, 'tags_list'=> $tags_list,  'attrs_options_list'=> $attrs_options_list, 'prices_list'=> $this->m_prices_list, 'app_params'=> $app_params, 'plugin_prefix'=> $this->m_plugin_prefix, 'woo_ext_search_options'=> $woo_ext_search_options, 'currency_symbol' => $this->m_currency_symbol, 'products_pagination_per_page_list'=> $products_pagination_per_page_list );
            twigpress_render_twig_template( $data, "white-simple/woo_ext_search_form.twig" );
            return;
        } // if ( $this->m_action == 'siteShowMostRatingSongs' ) { // Show Most Rating Songs in widget


        if ($this->isDebug) echo '<hr><hr><hr>';


    } // public function main() {


    protected function saveRowData( $action, $editorItem ) {
    } // protected function saveRowData( $action, $editorItem ) {

    protected function showRowsListDataForm( $parentControl )
    {
    } // protected function showRowsListDataForm( $parentControl )   {

    protected function addRowDataForm( $parentControl )   {
    }

    protected function initFieldsAddRow( $parentControl )
    {
    }


    protected function editRowDataForm( $parentControl ) {

    } // protected function editRowDataForm( $parentControl ) {

    protected function deleteRowData()
    {
    }


    protected function doBulkAction( $action) {
    } // protected function doBulkAction() {

    protected function doValidation($data)
    {
    } // protected function doValidation($data) {

    protected function setNotValidatedData($editorItem, $fieldsArray) {
    } // protected function setNotValidatedData($editorItem, $fieldsArray) {

    protected function preparePageParameters($WithPage, $WithSort) // Prepare url for pagination with all fields in filter and sort and sort_direction
    {
   } // protected function preparePageParameters($WithPage, $WithSort) // Prepare url for pagination with all fields in filter and sort and sort_direction

}