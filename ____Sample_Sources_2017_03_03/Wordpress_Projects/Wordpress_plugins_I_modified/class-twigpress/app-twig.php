<?php
class appTwig
{
    public function __construct()
    { // that is class (it is inside directory of my p[lugin) for defintion off filters, functions and global variable of my app.
    }


    public function registerGlobals($twig_Environment)
    {
        $twig_Environment->addGlobal( "site_url", site_url() );
        $twig_Environment->addGlobal( "plugin_dir", WP_PLUGIN_DIR );
        $twig_Environment->addGlobal( "plugin_url", WP_PLUGIN_URL );
        $twig_Environment->addGlobal( "plugin_images", WP_PLUGIN_URL . '/woo-ext-search/images/' );
    }
    public function registerUserFunctions($twig_Environment)
    {


        $filter = new Twig_SimpleFilter('make_woo_product_brand_url', function ($brand_name) {
            return '/ext_search_form/brand?='.$brand_name;
        });
        $twig_Environment->addFilter($filter);

        $filter = new Twig_SimpleFilter('make_woo_product_category_url', function ($category_slug) {
            return site_url().'/product-category/' . $category_slug;
        });
        $twig_Environment->addFilter($filter);


        $filter = new Twig_SimpleFilter('make_post_url', function ($post_id) {
            $post= get_post($post_id);
            if ( !empty($post->post_name) ) {
                return site_url() . '/' . $post->post_name;
            }
        });
        $twig_Environment->addFilter($filter);

        ...
        
        $filter = new Twig_SimpleFilter('change_submit_key', function ($key) {
            return nsnWooToolsData::changeSubmitKey($key);
        });
        $twig_Environment->addFilter($filter);



        $filter = new Twig_SimpleFilter('d', function ($v, $t='') {
            echo '<pre>'.(!empty($t)? $t.'::':'').print_r($v,true).'</pre>';
        });
        $twig_Environment->addFilter($filter);


        $filter = new Twig_SimpleFilter('date_time', function ($dt) {
            $date_format= get_option('date_format');
            $time_format= get_option('time_format');
            return date( $date_format .' '. $time_format, mysql2date( 'U', $dt) );
        });
        $twig_Environment->addFilter($filter);

        ...

        $filter = new Twig_SimpleFilter('concat_conditional_values', function ( $splitter, $values_array, $default_value ) {
            return AppUtils::concat_conditional_values( $values_array, $splitter, $default_value );
        });
        $twig_Environment->addFilter($filter);


        $filter = new Twig_SimpleFilter('concat_str', function ( $str, $max_len, $add_str = '...', $show_help = false, $strip_tags = true ) {
            return nsnClass_appFuncs::ConcatStr( $str, $max_len, $add_str, $show_help, $strip_tags );
        });
        $twig_Environment->addFilter($filter);

        return $twig_Environment;
    }

} // class AppTwig

