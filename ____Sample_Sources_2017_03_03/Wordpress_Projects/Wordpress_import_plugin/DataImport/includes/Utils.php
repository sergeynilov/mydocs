<?php
class Util {

  public static function getColorsArray() {
    return array('FF0000','FFA0A0','FFC619','FFF6CC','C2FF08','E2FF6D','01FF38','A4FFB0','07FFAD','9DFFDF','0547FF','809AFF','C906FF', 'E092FF', '#E092FF');
  }
  
  public static function IsPluginActivated($PluginName) {
    $PluginsList = get_option( 'active_plugins', array() );
    foreach( $PluginsList as $Plugin ) {
      if ( !(strpos($Plugin,$PluginName)===false) ) return true;
    }
    return false;
  }

  public static function getLangNameByKey($LangKey) {
    global $q_config;
    $nsn_MultiLanguage= get_option('nsn_MultiLanguage');
    $nsn_MainLanguage= get_option('nsn_MainLanguage');
    if ( $nsn_MultiLanguage and ( empty($q_config) or !function_exists("qtrans_getSortedLanguages")) ) {
      return '';
    }
    $LangsInSystem= array();
    if( $nsn_MultiLanguage ) {
      foreach ( $q_config['language_name'] as $Key => $Name ) {
        if($Key==$LangKey)return $Name;
      }
    }
  }
    
  public static function getAdminLang() {
    return sfConfig::get('app_admins_lang','en_GB');
  }
  public static function getDefaultLang($ReturnArray=false) {
    $langs_in_system_array= sfConfig::get('app_langs_in_system_array' );
    foreach( $langs_in_system_array as $key=>$lang ) {
      if ( !$ReturnArray ) return $key;
      if ( $ReturnArray ) return $langs_in_system_array[0];
    }
  }

  public static function getImageOfLang($LangKey) {
    if ( empty($LangKey) ) return '';
    return "/images/langs/".$LangKey.".png";
  }

  public static function getLangsInSystem($GetNonsn_MultiLanguage_Key= false) {
    global $q_config;
    $nsn_MultiLanguage= get_option('nsn_MultiLanguage');
    $nsn_MainLanguage= get_option('nsn_MainLanguage');
    //Util::deb($nsn_MultiLanguage,'$nsn_MultiLanguage::');
    //Util::deb($nsn_MainLanguage,'$nsn_MainLanguage::');
   //Util::deb($q_config,'$q_config::');
    if ( $nsn_MultiLanguage and ( empty($q_config) or !function_exists("qtrans_getSortedLanguages")) ) {
      return array();
    }
    $LangsInSystem= array();
    if( $nsn_MultiLanguage or $GetNonsn_MultiLanguage_Key ) {
      foreach ( $q_config['enabled_languages'] as $LangKey ) {
        $LangName= '';
        foreach ( $q_config['language_name'] as $Key => $Name ) {
          if($Key==$LangKey) {$LangName= $Name; break; }
        }
        $LangsInSystem[$LangKey]= $LangName;
      }
    }
    return $LangsInSystem;
  }

  public static function getFileSizeAsString($FileSize) {
    if ( (int)$FileSize< 1024 ) return $FileSize . ' b';
    if ( (int)$FileSize< 1024*1024 ) return floor($FileSize/1024).' kb';
    return floor($FileSize/(1024*1024)).' mb';
  }

  public static function select_tag($Id, $OptionsArray, $CurrentValue='', $AttrArray=array(), $SelectorKey='', $SelectorText='' ) {
    $OptionsHTML= '';
    if ( !empty($SelectorKey) or !empty($SelectorText) ) {
      $OptionsHTML.= '<option value="'.$SelectorKey.'" ' . ( $SelectorKey==$CurrentValue?"selected":'' ) . ' > '.$SelectorText.'</option>';

    }
    foreach( $OptionsArray as $value=> $Text  ) {
      $OptionsHTML.= '<option value="'.$value.'" ' . ( $value==$CurrentValue?"selected":'' ) . ' > '.$Text.'</option>';
    }
    $AttrHTML= '';
    foreach( $AttrArray as $AttrKey=> $AttrValue  ) {
      $AttrHTML.= $AttrKey.'="'.$AttrValue.'" ';
    }
    return '<select name="'.$Id.'" id="'.$Id.'" '.$AttrHTML.' >'.$OptionsHTML.'</select>';
  }

  public static function AddFirstArrayElem( $Array, $Key, $Text ) {
    $ResArray=array($Key=> $Text);
    foreach( $Array as $K=>$V ) {
      $ResArray[$K]= $V;
    }
    return $ResArray;
  }

  public static function getLeftNonDigit($S) {
    $Res='';
    $L= strlen( $S );
    for($I= 0; $I< $L; $I++) {
      $Ch= substr($S,$I,1);
      if ( !($Ch>= '0' and $Ch<= '9') ) {
        $Res= $Ch.$Res;
      } else {
        return $Res;
      }
    }
  }

  public static function getRightNonDigit($S) {
    $Res='';
    $L= strlen( $S );
    for($I=$L-1; $I>=0; $I--) {
      $Ch= substr($S,$I,1);
      if ( $Ch>= 0 and $Ch<= '9' ) {
      } else {
        return substr($S,0,$I+1);
      }
    }
    return $S;
  }

  public static function getRightDigit($S) {
    $Res='';
    $L= strlen( $S );
    for($I=$L-1; $I>=0; $I--) {
      $Ch= substr($S,$I,1);
      if ( $Ch>= 0 and $Ch<= '9' ) {
        $Res= $Ch.$Res;
      } else {
        return $Res;
      }
    }
  }


  public static function GetTwitterProperty($username, $PropertyName='') {
    require_once(sfConfig::get('sf_lib_dir')."/twitter.lib.php");
    try {
      $password= '';//$request->getParameter('password');
      $twitter = new Twitter( $username, $password );
      $lUser= $twitter->showUser();
      try {
        $xml = new SimpleXMLElement($lUser);
      }
      catch (Exception $lException) {
        echo json_encode(  array ( 'ErrorMessage'=>$lException->getMessage(), 'ErrorCode'=>1 )  );
      }
      if (!$xml) {
        $errors = libxml_get_errors();
        foreach ($errors as $error) {
        }
        libxml_clear_errors();
      }
      if ( $PropertyName == 'statuses_count' ) {
        return (string)$xml->statuses_count;
      }
      $profile_image_url= (string)$xml->profile_image_url;
      return $profile_image_url;
    }
    catch (Exception $lException) {
      $this->logMessage($lException->getMessage(), 'err');
      return sfView::ERROR;
    }
  }

  public static function GetTwitterImage($username) {
    require_once(sfConfig::get('sf_lib_dir')."/twitter.lib.php");
    try {
      $password= '';//$request->getParameter('password');
      $twitter = new Twitter( $username, $password );
      $lUser= $twitter->showUser();
      try {
        $xml = new SimpleXMLElement($lUser);
      }
      catch (Exception $lException) {
        echo json_encode(  array ( 'ErrorMessage'=>$lException->getMessage(), 'ErrorCode'=>1 )  );
      }
      if (!$xml) {
        $errors = libxml_get_errors();
        foreach ($errors as $error) {
        }
        libxml_clear_errors();
      }
      $profile_image_url= (string)$xml->profile_image_url;
      return $profile_image_url;
    }
    catch (Exception $lException) {
      $this->logMessage($lException->getMessage(), 'err');
      return sfView::ERROR;
    }
  }

  public static function getBackenfIndexFileName($currentConfiguration) {
    if ( empty($_SERVER["HTTP_HOST"]) ) return 'backend.php';
    $Env= $currentConfiguration->getEnvironment();
    $Ref='';
    if ( $Env=='dev') return 'backend_dev.php';
    return 'backend.php';
  }
  // getRandomList( $charity_features_number, $IdCodes);
  public static function getRandomList( $MaxNumber, $Values) {
    $ResArray=array();
    $I=0; $MaxCircles= 100;
    //Util::deb( $MaxNumber,'$MaxNumber::' );
    while ( count($ResArray) < $MaxNumber and $I< $MaxCircles ) {
      $Index= rand ( 0, count($Values)-1 );
      //Util::deb($Index,'$Index::') ;
      if( !empty($Values[$Index]) ) {
        if ( !in_array($Values[$Index], $ResArray) ) {
          $ResArray[]= $Values[$Index];
          //Util::deb( $ResArray, 'INSIDE count($ResArray)::' );
        }
      }
      $I++;
      // Util::deb($I,'$I::');
      // Util::deb( count($ResArray), 'count($ResArray)::' );
      if ( $I >= $MaxCircles ) return $ResArray;
    }
    return $ResArray;
  }

  public static function cross_app_link_to($app, $route, $args=null) {
    /* get the host to build the absolute paths
    needed because this menu lets switch between sf apps
    */
    $host = sfContext::getInstance()->getRequest()->getHost() ;
    /* get the current environment. Needed to switch between the apps preserving
    the environment
    */
    $env = sfConfig::get('sf_environment');
    /* get the routing file
    */
    $appRoutingFile = sfConfig::get('sf_root_dir').DIRECTORY_SEPARATOR.'apps'.DIRECTORY_SEPARATOR.$app.DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'routing.yml' ;
    //Util::deb($appRoutingFile,'$appRoutingFile::');
    /* get the route in the routing file */
    /* first, substract the @ from the route name */
    if ( substr($route,0,1)=='@' ) {
      $route = substr( $route, 1, strlen($route) ) ;
    }
    if (file_exists($appRoutingFile)) {
      //Util::deb(-1);
      $yml = sfYaml::load($appRoutingFile) ;
      //Util::deb($yml,'$yml::');
      $routeUrl = $yml[$route]['url'] ;
      if ($args) {
        foreach ($args as $k => $v) {
          $routeUrl = str_replace(':'.$k, $v, $routeUrl) ;
        }
      }
      if (strrpos($routeUrl, '*') == strlen($routeUrl)-1) {
        $routeUrl = substr($routeUrl, 0, strlen($routeUrl)-2) ;
      }
    }
    if ($env == 'dev') {
      if ( $app== 'frontend' ) $app= 'index';
      $path = 'http://' . $host . '/' . $app . '.php' . $routeUrl ;
    }
    else {
      $path = 'http://' . $host . $routeUrl ;
    }
    return $path ;
  }


  public static function getErrorMessage($ErrorMessage,$ObjName) {
    $K= strpos($ErrorMessage,'a foreign key constraint fails');
    if ( !($K===false) ) return 'Can not delete '.$ObjName.' as it is used in other tables.';
    return $ErrorMessage;
  }

  public static function getWeekNameShort($Week,$Culture) {
    if ( $Culture == 'pt_PT' or $Culture == 'pt_BR' ) {
      $A= array( 'Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb' );
    }
    if ( $Culture == 'es_ES' ) {
      $A= array( 'Dom', 'Lun','Mar','Mie','Jue', 'Vie', 'Sáb' );
    }

    if ( $Culture == 'en_GB' ) {
      $A= array( 'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'  );
    }
    //Util::deb($Week,'$Week::');
    $WeekNumber= (int)$Week;
    if ( !empty( $A[$WeekNumber] ) ) return $A[$WeekNumber];
    return $Week;
  }

  public static function getMonthName($Month,$Culture) {
    if ( $Culture == 'pt_PT' or $Culture == 'pt_BR' ) {
      $A= array( 'Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro','Dezembro' );
    }
    if ( $Culture == 'es_ES' ) {
      $A= array( 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre' );
    }

    if ( $Culture == 'en_GB' ) {
      $A= array( 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December' );
    }
    $Mon= (int)$Month-1;
    if ( !empty( $A[$Mon] ) ) return $A[$Mon];
    return $Month;
  }

  public static function getServiceNameByCode($ServiceName) {
    $retrieval_api_array= sfConfig::get('app_retrieval_api_array' );
    if ( !empty($retrieval_api_array[$ServiceName]) ) return $retrieval_api_array[$ServiceName];
    return $ServiceName;
  }

  public static function DaysInMonth( $Year, $Month ) {
    Util::deb($Year,'DaysInMonth  $Year::');
    Util::deb($Month,'  $Month::');
    switch ($Month) {
      case 1:
        return 31;
      case 2:
        if ( ceil($Year % 4) == 0 )
        return 29; // ���������� ���
        else
        return 28;
      case 3:
        return 31;
      case 4:
        return 30;
      case 5:
        return 31;
      case 6:
        return 30;
      case 7:
        return 31;
      case 8:
        return 31;
      case 9:
        return 30;
      case 10:
        return 31;
      case 11:
        return 30;
      case 12:
        return 31;
    }
    return 0;
  }

  public static function getImageSrcByCulture( $ImageName, $Context ) {
    $CurrentCulture= $Context->getUser()->getCulture();
    $sf_web_dir= sfConfig::get('sf_web_dir');

    $FullImageName= $sf_web_dir.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$ImageName.'_'.$CurrentCulture.'.png';

    if( file_exists($FullImageName) ) return '/images/'.$ImageName.'_'.$CurrentCulture.'.png';
    return '/images/'.$ImageName.'.png';
  }

  public static function UnixDateTimeAddDay($dat,$DaysToAdd) {
    $StartYear= (int)strftime('%Y',$dat);
    $StartMonth= (int)strftime('%m',$dat);
    $StartDay= (int)strftime('%d',$dat);
    $StartHour= (int)strftime("%H",$dat);
    $StartMinutes= (int)strftime("%M",$dat);
    $StartSeconds= (int)strftime("%S",$dat);

    $end_dat= mktime( $StartHour, $StartMinutes, $StartSeconds, $StartMonth, $StartDay+$DaysToAdd, $StartYear );
    return $end_dat;
  }

  public static function getFlightTimeDuration( $depDate, $depHour, $arrDate, $arrHour, $HoursLabel, $MinutesLabel ) {
    $DateTimeStart= Util::ToDateAddTime($depDate, $depHour);
    $DateTimeEnd= Util::ToDateAddTime($arrDate, $arrHour);
    $Duration= $DateTimeEnd- $DateTimeStart;
    $minute= (int)($Duration/60);
    //Util::deb($minute,'$minute::');

    $hour= (int)($minute/60); // %H:%M
    //Util::deb($hour,'$hour::');
    if ($hour > 0) return $hour.$HoursLabel;
    return $minute.$MinutesLabel;
  }

  public static function ToDateAddTime( $dat, $StrHour ) { // 1250719200  20:55 {
    if ( strlen($dat)==0 ) return null;
    $year= strftime("%Y", $dat);
    $month= strftime("%m", $dat);
    $day= strftime("%d", $dat);
    $hour= 0;
    $minute=0;
    //Util::deb($dat,'ToDateAddTime( $dat::');
    //Util::deb($StrHour,'$StrHour::');
    if (strlen($StrHour)==4 and ( !strpos($StrHour,':') ) ) {
      $StrHour= substr($StrHour,0,2).":".substr($StrHour,2,2);
    }
    //Util::deb($StrHour,'$StrHour::');
    $A=preg_split( '/:/', $StrHour );
    if ( Count($A) == 2 ) {
      $hour= (int)$A[0];
      $minute= (int)$A[1];
    }
    //Util::deb($hour,'$hour::');
    //Util::deb($minute,'$minute::');
    return mktime( $hour, $minute, 0, $month, $day,$year);
  }

  public static function StrToUnixTime( $str, $Separator='' ) { //          1705  20:55 {
    if ( strlen($str)==0 ) return 0;
    if ( empty($Separator) ) {
      $Minute= substr($str,0,2);
      $Second= substr($str,2,2);
      return ( ((int)$Minute)*60+(int)$Second);
    }
  }

  public static function FromStringTimeDeleteSeconds($StringTime,$Separator=':') {
    $A= preg_split( '/'.$Separator.'/', $StringTime );
    if ( count($A) !== 3 ) return $StringTime;
    return $A[0].$Separator.$A[1];
  }

  public static function AddDayByBothTimes( $UnixDat, $FirstTime, $EndTime, $OutPutFormat='unix' ) {

    $FirstTime= str_replace( ':', '', $FirstTime );
    $EndTime= str_replace( ':', '', $EndTime );
    if ( $FirstTime> $EndTime ) {
      $UnixDat= Util::UnixDateTimeAddDay($UnixDat,1);
    }
    if ( $OutPutFormat == 'DMYSlashed' ) { //  (format DD/MM/YYYY)
      return strftime( '%d/%m/%Y', $UnixDat );
    }
    return $UnixDat;
  }

  public static function StrToDate($Dat,$StrDateFormat="DMYNoSlashes",$OutPutFormat='unix') { //20122009
    if ( empty($StrDateFormat) ) $StrDateFormat="DMYNoSlashes";
    if ( strlen($Dat)==0 ) return null;


    if ( $StrDateFormat == "YMDMinus" ) { // 2010-03-14
      $A=preg_split('/-/',$Dat);
      if ( count($A) != 3 ) return false;
      return mktime( 0,0,0,$A[1],$A[2],$A[0] );
    }

    if ( $StrDateFormat == "DMYSlashes" ) { // 27/01/2010
      $A=preg_split('///',$Dat);
      if ( count($A) != 3 ) return false;
      return mktime( 0,0,0,$A[1],$A[0],$A[2] );
    }

    if ( $StrDateFormat == "YMDSlashes" ) { //2010/01/20
      $A=preg_split('///',$Dat);
      if ( count($A) != 3 ) return false;
      return mktime( 0,0,0,$A[1],$A[2],$A[0] );
    }

    if ( $StrDateFormat == "DMYNoSlashes" ) { //20122009
      $Day= substr($Dat,0,2);
      $Month= substr($Dat,2,2);
      $Year= substr($Dat,4,4);
    }

    if ( $StrDateFormat == "YMDNoSlashes" ) { //20100120
      $Day= substr($Dat,6,2);
      $Month= substr($Dat,4,2);
      $Year= substr($Dat,0,4);
    }

    if ( $OutPutFormat=='StrYMD' ) {
      return $Year.$Month.$Day;
    }

    if ( $OutPutFormat == 'DMYSlashed' ) { //  (format DD/MM/YYYY)
      return $Day.'/'.$Month.'/'.$Year;
    }

    if ( $OutPutFormat == 'YMDMinus' ) { //  (format 2010-03-14)
      return $Year . '-' . $Month . '-' . $Day;
    }

    return mktime(0,0,0,(int)$Month,(int)$Day,(int)$Year);
  }

  public static function WriteAppCookieArray( $Arr, $Minutes=60 ) {
    if ( !is_array($Arr) ) return false;
    //Util::deb($Arr,'$Arr::');
    foreach( $Arr as $Key=> $Value ) {
      sfContext::getInstance()->getResponse()->setCookie( Util::getServerAsString().sfConfig::get('app_application_cookie_name').'_'.$Key, $Value, time()+60*$Minutes, '/'); // TODO
    }
  }

  public static function getAppCookieValue($CookieName) {
    $CookieValue= sfContext::getInstance()->getRequest()->getCookie(Util::getServerAsString().sfConfig::get('app_application_cookie_name').'_'.$CookieName );
    //Util::deb($CookieValue,'$CookieValue::');
    return $CookieValue;
  }

  /*public static function getFormattedDates($S) {
  return str_replace(';','',$S);
  } */

  public static function getDigitMoney( $Val, $Return='' ) {
    //Util::deb($Val,'$Val::');
    $Val= (string)$Val;
    $Val = str_replace(',','.',$Val );
    $Val = str_replace('.','&',$Val );
    $A= preg_split('/&/',$Val);
    if ( $Return== 'Decimal' ) {
      //Util::deb($A,'$A::');
      if ( empty($A[1]) ) return '00';
      if ( (int)$A[1] < 9 and strlen($A[1])==1 ) return '0'.$A[1];
      return $A[1];
    }
    if ( $Return== 'Digit' ) {
      $Val= $A[0];
      $Res='';
      $L= strlen($Val);
      $C=1;
      for (  $I= $L-1; $I>= 0; $I--  ) {
        $Ch= substr($Val,$I,1);
        $Res= $Ch.$Res;
        if ( $C % 3 == 0 and $I > 0) {
          $Res= '.'.$Res;
        }
        $C++;
      }
      return $Res;
    }
    return $Val;
  }


  public static function getServiceName($ServiceName) {
    $retrieval_api_array= sfConfig::get('app_retrieval_api_array' );
    foreach( $this->retrieval_api_array as $Key=>$Value ) {
      if ( $ServiceName == $Key ) {
        return $Value;
      }
    }
    return $ServiceName;
  }

  public static function FormatTime($S) { // 1455 07:50
    //Util::deb($S,'FormatTime $S::');
    if ( strlen($S)== 1 ) return '0'.$S.':00';
    if ( strlen($S)== 2 ) return $S.':00';
    if ( strlen($S)< 3 ) return $S;
    if ( strpos($S,':')> 0 ) return $S;
    if ( strlen($S)== 3 ) return '0'.substr($S,0,1).':'.substr($S,1,2);
    return substr($S,0,2).':'.substr($S,2,2);
  }


  public static function FileAsStringWithCurl( $Url, $Password ) {
    // инициализация сеанса
    ob_start();


    $ch = curl_init();
    // установка URL и других необходимых параметров
    curl_setopt($ch, CURLOPT_URL, $Url);



    //curl_setopt($ch, CURLOPT_USERPWD, $Password); //CURLOPT_USERPWD: Стока с именем пользователя и паролем в виде [username]:[password].
    curl_setopt( $ch, CURLOPT_HEADER, 0 );

    curl_setopt( $ch, CURLOPT_VERBOSE, 1 ); // CURLOPT_VERBOSE: При установке этого параметра в ненулевое значение cURL будет выводить подробные сообщения о всех производимых действиях.

    curl_setopt( $ch, CURLOPT_POST, 1 ); // CURLOPT_POST: При установке этого параметра в ненулевое значение будет отправлен HTTP запрос методом POST типа application/x-www-form-urlencoded, используемый браузерами при отправке форм.

    curl_setopt( $ch, CURLOPT_NOPROGRESS, 1 ); //CURLOPT_NOPROGRESS: При установке этого параметра в ненулевое значение не будет выводиться индикатор прогресса операции.

    // загрузка страницы и выдача её браузеру
    $Res= curl_exec($ch);
    Util::deb($Res,'$Res::');




    // завершение сеанса и освобождение ресурсов
    curl_close($ch);
    $out1 = ob_get_contents();

    ob_end_clean();
    echo '<hr><br>';
    var_dump($out1);
    echo '<hr><br>';

  }


  public static function FileAsString( $FileName, $isMissEmptyLines= false ) {
    $StrRows= file( $FileName ); // read File as 1 Array
    if ( !$StrRows ) return '';
    $L= Count($StrRows);
    $Res= '';
    for ( $I= 0; $I< $L; $I++ ) {
      $S= $StrRows[$I];
      if ( $isMissEmptyLines ) {
        $S= trim($S);
        if ( empty($S) ) continue;
        $S= $StrRows[$I];
        $Res= $Res . $S;
        continue;
      }
      $Res= $Res . $S . "\r\n";
    }
    return $Res;
  }



  /*public static function strToDateTime($S,$Format="DYM"){

  $A=preg_split('///',$S);
  if ( count($A) != 3 ) return false; // 20/12/2009
  if ( $Format=="DYMSlashed" ) {
  return mktime(0,0,0,$A[1],$A[0],$A[2]);
  }
  if ( $Format=="YMDSlashed" ) { //          2010/01/20
  return mktime(0,0,0,$A[1],$A[2],$A[0]);
  }
  } */

  public static function getCultureCountry($Context) {
    // // http://api.geoio.com/q.php?key=4XSVzU7V6e5TGDmh&qt=geoip&d=pipe&q=94.178.180.207
    $CurrentCulture= $Context->getUser()->getCulture();
    $A=preg_split('/_/',$CurrentCulture);
    if ( count($A)==2 ) return $A[1];
    return $CurrentCulture;
  }

  public static function getShortUrlByCulture($Culture) {
    $langs_in_system_array= sfConfig::get('app_langs_in_system_array' );
    foreach( $langs_in_system_array as $lang=>$LangName ) {
      if ( strtoupper($lang)==strtoupper($Culture) ) {
        return $LangName['shorturl'];
      }
    }
  }

  public static function getCultureByShortUrl($LangUrl) {
    $langs_in_system_array= sfConfig::get('app_langs_in_system_array' );
    foreach( $langs_in_system_array as $lang=>$LangName ) {
      if ( strtoupper($LangName['shorturl'])==strtoupper($LangUrl) ) {
        return $lang;
      }
    }
  }

  public static function SetDefaultCulture($Context) {
    $CurrentCulture= $Context->getUser()->getCulture();
    $langs_in_system_array= sfConfig::get('app_langs_in_system_array' );
    $CurrentIsSet= false;
    foreach( $langs_in_system_array as $key=>$lang ) {
      if( $key == $CurrentCulture ) {
        $CurrentIsSet=true;
        break;
      }
    }
    //Util::deb($CurrentIsSet,'$CurrentIsSet::');
    if ( !$CurrentIsSet ) {
      $default_culture= Util::getAppCookieValue('default_culture');
      //Util::deb($default_culture,'$default_culture::');
      if ( !empty($default_culture) ) {
        $Context->getUser()->setCulture($default_culture);
        setlocale(LC_ALL, $default_culture);
        //Util::deb("",-1);
        return true;
      }

      $CountryNameByIP= Util::getCountryNameByIP();
      //Util::deb($CountryNameByIP,'$CountryNameByIP::');
      //$CountryNameByIP='Brazil';
      $Lang=Util::getLangByCountryNameIP($CountryNameByIP);
      if ( !empty($Lang) ) {
        $Context->getUser()->setCulture( $Lang );
        setlocale(LC_ALL, Util::getDefaultLang());
        //Util::deb("",-2);
        return true;
      }

      $Context->getUser()->setCulture( Util::getDefaultLang() );
      setlocale(LC_ALL, Util::getDefaultLang());
      //Util::deb("",-3);
      return true;
    }
    return false;
  }

  public static function getLangByCountryNameIP($CountryNameByIP) {
    $langs_in_system_array= sfConfig::get('app_langs_in_system_array' );
    //Util::deb($langs_in_system_array,'$langs_in_system_array::');
    foreach( $langs_in_system_array as $lang=>$LangName ) {
      if ( strtoupper($CountryNameByIP)==strtoupper($LangName['geoname']) ) {
        //die("FOUND::".$lang);
        return $lang;
      }
    }

  }

  public static function getCountryNameByIP() {
    $GeoKey= sfConfig::get('app_application_GeoKey' );
    $IP= ( !empty($HTTP_SERVER_VARS['REMOTE_ADDR']) ) ? $HTTP_SERVER_VARS['REMOTE_ADDR'] : ( ( !empty($HTTP_ENV_VARS['REMOTE_ADDR']) ) ? $HTTP_ENV_VARS['REMOTE_ADDR'] : getenv('REMOTE_ADDR') );
    //Util::deb($IP,'$IP::');
    $Url= 'http://api.geoio.com/q.php?key='.$GeoKey.'&qt=geoip&d=pipe&q='.$IP;//.94.178.180.207';
    $Res= Util::FileAsString( $Url, true );
    //echo '<br>$Res::'.$Res.'<br>';
    $A= preg_split('/\|/',$Res);
    //Util::deb($A,'$A::');
    if( !empty($A[2]) ) {
      return $A[2];
    }
  }


  public static function SubstSpaces( $S ) {
    $S= str_replace('     ',' ', $S);
    $S= str_replace('    ',' ', $S);
    $S= str_replace('   ',' ', $S);
    $S= str_replace('  ',' ', $S);
    $S= str_replace(' ','-', $S);
    return $S;
  }

  public static function WriteToFileName($FileName,$contents, $CreateBackUp = false) {
    try {
      $CurrentDir= '';//str_replace( '\\','/', sfConfig::get('sf_web_dir') );
      //Util::deb($CurrentDir,'$CurrentDir::');
      if ( $CreateBackUp ) {
        copy($CurrentDir.'/'.$FileName,$CurrentDir.'/'.$FileName.'.backup');
      }
      $fd = fopen ( $CurrentDir.'/'.$FileName, "w" );
      fwrite ( $fd, $contents );
      fclose ($fd);
      return true;
    }
    catch (Exception $lException) {
      return false;
    }
  }

  public static function UrlToRawString($S) {
    $A= preg_split('///',$S);
    $Res= '/';
    $I=1;
    foreach( $A as $Key ) {
      if (empty($Key)) continue;
      if( $I<= 1 ) $Res.= $Key.'/';
      if( $I== 2 ) $Res.= $Key.'?';
      if( $I> 2 and ($I%2==0) ) $Res.= $Key. ($I<count($A)-1?'&':'') ;
      if( $I> 2 and ($I%2!=0) ) $Res.= $Key. ($I<count($A)-1?'=':'') ;
      $I++;
    }
    return $Res;
  }

  public static function getFirstDayOfMonth($dat) {
    $year= strftime("%Y", $dat);
    $month= strftime("%m", $dat);
    $day= strftime("%d", $dat);
    return mktime(0,0,0,$month,1,$year);
  }


  public static function getCodeBumpZipsByDistance($StateShortname, $Cityname ) {
    $Cityname= str_replace(' ','+',$Cityname);
    $CodeBumpUnique= ConfigurationPeer::GetConfigurationValue( 'CodeBump Unique', '' );
    $CodeBumpDistance= (int)ConfigurationPeer::GetConfigurationValue( 'CodeBump Distance', 500 );

    $Url= 'http://codebump.com/services/PlaceLookup.asmx/GetPlacesWithin?AuthenticationHeader='.urlencode($CodeBumpUnique).'&place='.trim($Cityname).'&state='.$StateShortname.'&distance='.$CodeBumpDistance.'&placeTypeToFind=ZipCode';

    $Res= '';
    $GeoPlaceDistance = simplexml_load_file($Url);
    if ( !empty($GeoPlaceDistance) ) {
      foreach ($GeoPlaceDistance->GeoPlaceDistance as $GeoPlaceDistance) {
        $Res.= $GeoPlaceDistance->ToPlace.',';
      }
    }
    return $Res;
  }


  public static function getCodeBumpNearestZips( $zip, $type= 'small' ) {
    //$Cityname= str_replace(' ','+',$Cityname);
    $CodeBumpUnique= ConfigurationPeer::GetConfigurationValue( 'CodeBump Unique', '' );
    if ( $type== 'small' ) {
      $CodeBumpDistance= (int)ConfigurationPeer::GetConfigurationValue( 'CodeBump Distance', 50 );
    } else {
      $CodeBumpDistance= (int)ConfigurationPeer::GetConfigurationValue( 'CodeBump Maximum Distance', 500 );
    }
    //
    $Url= 'http://codebump.com/services/PlaceLookup.asmx/GetPlacesWithin?AuthenticationHeader='.urlencode($CodeBumpUnique).'&place='.trim($zip).'&state=&distance='.$CodeBumpDistance.'&placeTypeToFind=City';
    Util::deb($Url,'$Url::');
    $ResArray= array();
    $GeoPlaceDistance = simplexml_load_file($Url);
    $NearesDistance= -1;
    if ( !empty($GeoPlaceDistance) ) {
      foreach ($GeoPlaceDistance->GeoPlaceDistance as $GeoPlaceDistance) {
        Util::deb($GeoPlaceDistance,'$GeoPlaceDistance::');
        if ( $NearesDistance < (int)$GeoPlaceDistance->Distance ) {
          $NearesDistance= (int)$GeoPlaceDistance->Distance;
        }
        $ResArray[]= $GeoPlaceDistance->ToPlace.',';
      }
    }
    Util::deb($Res,'$Res::');
    die("RRR");
    return $Res;
  }


  public static function getYesNoArray() {
    return array('0'=>'No','1'=>'Yes');
  }

  public static function getServerAsString() {
    if ( Util::isDeveloperComp() ) return 'developer_';
    return '';  //$_SERVER["HTTP_HOST"].'_';
  }

  public static function PadrWith( $S, $ToLen, $Char= ' ' ) {
    $Res= '';
    if ( strlen($S)<= $ToLen ) {
      for ( $I= strlen($S); $I<= $ToLen - 1; $I++)
      $Res= $Res.$Char;
      Return $S.$Res;
    }
    else
    return substr($S,0,$ToLen-1);
  }

  public static function PadlWith( $S, $ToLen, $Char= ' ' ) {
    $Res= '';
    if ( strlen($S)<= $ToLen ) {
      for ( $I= strlen($S); $I<= $ToLen - 1; $I++)
      $Res= $Res.$Char;
      Return $Res.$S;
    }
    else
    return substr($S,0,$ToLen-1);
  }


  public static function UnixDateAddDay($dat,$DaysToAdd) {
    $StartYear= (int)strftime('%Y',$dat);
    $StartMonth= (int)strftime('%m',$dat);
    $StartDay= (int)strftime('%d',$dat);
    $end_dat= mktime( 0, 0, 0, $StartMonth, $StartDay+$DaysToAdd, $StartYear );
    return $end_dat;
  }


  public static function getSiteRootDir() {
    if ( Util::isDeveloperComp() ) return '/web';
    return '/web';
  }
  public static function GetImageShowSize( $ImageFileName, $orig_width, $orig_height ) {
    /*Util::deb($ImageFileName,'$ImageFileName::');
    Util::deb($orig_width, '$orig_width::');
    Util::deb($orig_height,'$orig_height::');  */
    $ResArray= array( 'Width'=>0, 'Height'=>0, 'OriginalWidth'=>0, 'OriginalHeight'=>0 );
    //Util::deb(sfConfig::get('sf_root_dir'). Util::getSiteRootDir(). $ImageFileName,'sfConfig::get(sf_root_dir). Util::getSiteRootDir(). $ImageFileName');

    $FileArray=@getimagesize( str_replace("\\","/",sfConfig::get('sf_root_dir'). Util::getSiteRootDir(). $ImageFileName) );
    if (empty($FileArray)) return $ResArray;

    $width= (int)$FileArray[0];
    $height= (int)$FileArray[1];
    $ResArray['OriginalWidth']= $width;
    $ResArray['OriginalHeight']= $height;
    $ResArray['Width']= $width;
    $ResArray['Height']= $height;
    //Util::deb($ResArray,'-3 0000:');

    $Ratio= round($width/$height,3);

    if ( $width > $orig_width ) {
      $ResArray['Width']= (int)($orig_width);
      $ResArray['Height']= (int)($orig_width/$Ratio);
      //Util::deb($ResArray,'$ResArray::');
      if (  $ResArray['Width'] <= (int)$orig_width   and  $ResArray['Height']<= (int)$orig_height  ) {
        //Util::deb(-1);
        return $ResArray;
      }
      //Util::deb(-2);
      $width= $ResArray['Width'];
      $height= $ResArray['Height'];
      //Util::deb($ResArray,'-2  $ResArray::');

    }
    if ( $height > $orig_height and ((int)($orig_height/$Ratio)) <= $orig_width ) {
      //Util::deb( $Ratio, '-3 $Ratio::' );
      $ResArray['Width']= (int)($orig_height/$Ratio);
      $ResArray['Height']= (int)($orig_height);
      //Util::deb($ResArray,'-3  $ResArray::');
      return $ResArray;
    }
    if ( $height > $orig_height and ((int)($orig_height/$Ratio)) > $orig_width ) {
      //Util::deb( $Ratio, '-4  $Ratio::' );
      $ResArray['Width']= (int)($orig_height*$Ratio);
      $ResArray['Height']= (int)($ResArray['Width']/$Ratio);
      //Util::deb($ResArray,'-4  $ResArray::');
      return $ResArray;
    }
    return $ResArray;
  }

  public static function getSiteVariables() {
    return array( '[site_name]'=> 'Help Attack'/*ConfigurationPeer::GetConfigurationValue( 'name', 'Airliners site' )*/ );
  }

  public static function removeNewLineCharacter($str) {
    $buf_array = explode("\n", $str);
    $lResult = '';

    foreach ($buf_array as $string) {
      $lResult .= str_replace('"','\"',substr($string, 0, count($string) - 2));
    }

    return $lResult;

    //          return nl2br($str);

  }

  public static function SplitStringAsKeyValues( $Str, $PairSepr=';', $Sepr='=' ) {
    $ResArray=array();
    $A= preg_split('/'.$PairSepr.'/',$Str);
    if (is_array($A)) {
      foreach( $A as $Key=>$Value ) {
        if ( empty($Value) ) continue;
        $A_1= preg_split('/'.$Sepr.'/',$Value);
        if ( count($A_1)==2 ) {
          $ResArray[$A_1[0]]=$A_1[1];
        }
      }
    }

    return $ResArray;
  }

  /*
  Show list($StringsArray) of words($Sepr) without $Sepr if empty Words like Salem, Oregon, United States
  or Oregon, United States
  */
  public static function StringConcatWithSepr( $StringsArray, $Sepr ) {
    $ResText= '';
    $L= count($StringsArray);
    $ResText= $StringsArray[0];
    for( $I= 1; $I< $L; $I++ ) {
      $NextWord= $StringsArray[$I];
      $ResText= $ResText . ( (!empty($ResText) and !empty($NextWord) )?$Sepr:'' ) . $NextWord;
    }
    return $ResText;
  }

  /*
  in external link add prefix http:// if it is not defined
  */
  public static function WriteExternalUrl( $Url ) {
    $Url= trim($Url);
    $S= strtolower($Url);
    if ( strpos($S,'http://')===false and        strpos($S,'https://')===false ) return 'http://'.$S;
    return $S;
  }

  /*
  by type of web link return its image
  */
  public static function getWebImage($webType) {
    if ( $webType=="AIM" ) return 'aim.gif';
    if ( $webType=="Bebo" ) return 'bebo.gif';
    if ( $webType=="Facebook" ) return 'facebook.gif';
    if ( $webType=="Flickr" ) return 'flickr.gif';
    if ( $webType=="Friendster" ) return 'friendster.gif';
    if ( $webType=="Google Talk" ) return 'googletalk.gif';
    if ( $webType=="Blog" ) return 'blog.gif';
    if ( $webType=="MySpace" ) return 'myspace.gif';
    if ( $webType=="MSN Messenger" ) return 'msnmessenger.gif';
    if ( $webType=="LinkedIn" ) return 'linkedin.gif';
    if ( $webType=="Plaxo" ) return 'plaxo.gif';
    if ( $webType=="Other" ) return 'other.gif';
    if ( $webType=="icq" ) return 'icq.gif';
    if ( $webType=="Slide" ) return 'slide.gif';
    if ( $webType=="YouTube" ) return 'youtube.gif';
    if ( $webType=="Yahoo Messenger" ) return 'yahoomessenger.gif';
    if ( $webType=="Twitter" ) return 'twitter.gif';
    if ( $webType=="Work Website" ) return 'workwebsite.gif';
    if ( $webType=="Personal Website" ) return 'personalwebsite.gif';
    if ( $webType=="Skype" ) return 'skype.gif';
  }


  /*
  calc_period calc difference between 2 dates
  $date_start - first date, $date_finish -second date
  */
  public static function RecalcYearsPeriod($date_start, $date_finish) {
    if ( is_array($date_start) ) {
      $StartYear= (int)$date_start['year'];
      $StartMonth= (int)$date_start['month'];
      $StartDay= (int)$date_start['day'];
    } else {
      if ( $date_start> $date_finish ) return ''  ;
      $StartYear= (int)strftime('%Y',$date_start);
      $StartMonth= (int)strftime('%m',$date_start);
      $StartDay= (int)strftime('%d',$date_start);
    }

    $FinishYear= (int)strftime('%Y',$date_finish);
    $FinishMonth= (int)strftime('%m',$date_finish);
    $FinishDay= (int)strftime('%d',$date_finish);

    if ( $StartYear>= $FinishYear ) return 0;
    $ResYears= $FinishYear-$StartYear;
    if ( $StartMonth< $FinishMonth ) return $ResYears;
    if ( $StartMonth> $FinishMonth ) return $ResYears-1;

    if ( $StartDay<= $FinishDay ) return $ResYears; // both days are in the seven monthes
    if ( $StartDay> $FinishDay ) return $ResYears-1;
  }


  public static function GetNumerosValues( $KeyName,$_PostArray, $DateFormat= false) {
    $ResArray= array();
    foreach($_PostArray as $Key=> $Value) {
      $A= preg_split('/'.$KeyName.'/',$Key);
      //Util::deb($A,'$A::');
      if ( !$DateFormat ) {
        if ( count($A)==2 && $A[0]== '' and trim($Value)!='') {
          $ResArray[$Key]= $Value;
        }
      } else {
        if ( count($A)==2 && $A[0]== '' and is_array($Value)) {
          if ( count($Value)==3 ) {
            $ResArray[$Key]= $Value['year'].'-'.$Value['month'].'-'.$Value['day'];
          }
        }
        if ( count($A)==2 && $A[0]== '' and !is_array($Value)) {
          $ResArray[$Key]= $Value;
        }
      }
    }
    return $ResArray;
  }

  public static function getAppSiteHost() {
    if ( empty($_SERVER["HTTP_HOST"]) ) return false;
    return 'http://'.$_SERVER["HTTP_HOST"];
  }


  public static function getCurrentUrl($currentConfiguration) {
    if ( empty($_SERVER["HTTP_HOST"]) ) return false;
    $REQUEST_URI= $_SERVER["REQUEST_URI"];
    $Env= $currentConfiguration->getEnvironment();
    $Ref='';
    if ( $Env=='dev') {
      $REQUEST_URI= str_replace( "/frontend_dev.php/","",$REQUEST_URI );
      //Util::deb($REQUEST_URI,'--$REQUEST_URI::');
    }
    return $REQUEST_URI;
  }

  public static function getServerHost( $currentConfiguration, $AddIndexFile=true, $BackendApp=false ) {
    if ( empty($_SERVER["HTTP_HOST"]) ) return false;
    if ( empty($currentConfiguration) ) return self::getAppSiteHost();
    $Env= $currentConfiguration->getEnvironment();
    $Ref='';
    if ( $Env=='dev' and $AddIndexFile /*and Util::isDeveloperComp()*/ ) {
      if ( $BackendApp ) {
        $Ref= "/backend_dev.php";
      } else {
        $Ref= "/frontend_dev.php";
      }
    }
    return trim('http://'.$_SERVER["HTTP_HOST"].$Ref."/");
    /*   if ( empty($_SERVER["HTTP_HOST"]) ) return false;
    if ( empty($currentConfiguration) ) return self::getAppSiteHost();
    $Env= $currentConfiguration->getEnvironment();
    $Ref='';
    if ( $Env=='dev' and $AddIndexFile and Util::isDeveloperComp() ) {
    $Ref= '';// /  //"/index.php";
    }
    return 'http://'.$_SERVER["HTTP_HOST"].$Ref."/"; */
  }

  /**
   * Send email using server's mail configurations
   * $EmailTo- email for whom this email is sent can be in " UserName<UserEmail>' " format
   * $EmailFrom - email from whom this email is sent
   * $Title - title of email
   * $Subject - subject of emeil
   */
  public static function SendEmail($EmailTo, $EmailFrom, $Title, $Subject) {
    $headers  = 'MIME-Version: 1.0' . "\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\n";
    $headers .= 'To: ' . $EmailTo . "\n";
    $headers .= 'From: ' .$EmailFrom . "\n";
    //return;
    if ( Util::isDeveloperComp() ) {
      Util::deb(" SendEmail EmailTo::" . $EmailTo.'  $Subject:'.$Subject.' $headers:   '.$headers );
    } else {
      //die("!!!");
      mail( $EmailTo, $Title, $Subject, $headers );
    }
  }




  /**
   * form string $S trim substring at the right end if it equals $Substr
   */
  public static function TrimRightSubString( $S, $Substr ) {
    $Res= preg_match('/(.*?)('.preg_quote($Substr,"/").')$/si',$S, $A);
    if ( !empty($A[1]) ) return $A[1];
    return $S;
  }

  /**
   * verify if this developers(Of Nilov) local host
   */
  public static function isDeveloperComp() {
    if ( empty($_SERVER["HTTP_HOST"]) ) return false;
    if ( !(strpos($_SERVER["HTTP_HOST"],"localhost:93")===false)  or !(strpos($_SERVER["HTTP_HOST"],"www.local-absolute.com")===false) ) {
      return true;
    }
    return false;
  }

  /**
   * debugging function for developer's server Util::isDeveloperComp()
   * $Var - variable to show in human readable way
   * $Descr - label of debug
   * $isSql - if TRUE and $Var is sql-statement show sql-statement in human readable way
   */
  public static function deb($Var, $Descr= '', $isSql= false, $ToFile= '') {
    //    $isShow= ( sfContext::getInstance()->getModuleName()=='main' and sfContext::getInstance()->getActionName()== 'getLinersSearch' );
    //$isShow= false;
    if ( !Util::isDeveloperComp() and !$isShow ) return;
    //return;
    include_once("dbug.php");
    if ( strlen($ToFile)>2 ) {
      if ( is_array($Var) ) {
        Util::WriteToFileName($ToFile,HtmlSpecialChars($Descr).' : '.print_r($Var,true) );
      } else {
        Util::WriteToFileName($ToFile,HtmlSpecialChars($Descr).' : '.$Var );
      }
      return;
    }
    if ( !empty($Descr) ) {
      echo "<h5><b><u>".'<font color= green>'. HtmlSpecialChars($Descr) ." </font></u></b>";
    }
    if (is_array($Var))
    new dBug ( $Var );
    else if (is_object($Var))
    echo var_dump($Var);
    else {
      if ( gettype($Var)== 'string' ) {
        if ($isSql) {
          echo '<b><I>'.gettype($Var)."</I>".'&nbsp;$Var:=<font color= red>&nbsp;'.Util::ShowFormattedSql($Var)."</font></b></h5>";
        } else {
          echo '<b><I>'.gettype($Var)."</I>".'&nbsp;$Var:=<font color= red>&nbsp;'.HtmlSpecialChars($Var)."</font></b></h5>";
        }
      }
      else {
        echo '<b><I>'.gettype($Var)."</I>".'&nbsp;$Var:=<font color= red>&nbsp;'.$Var."</font></b></h5>";
      }
    }
  }

  public static function ShowFormattedSql( $Sql, $isBreakLine= true ) {
    $BreakLine= '';
    if ( $isBreakLine ) {
      $BreakLine= '<br>';
    }
    $Sql= ' '.$Sql.' ';
    $Sql = eregi_replace("insert into", "&nbsp;&nbsp;<B>INSERT INTO</B>", $Sql)  ;
    $Sql = eregi_replace("insert", "&nbsp;<B>INSERT</B>", $Sql)  ;
    $Sql = eregi_replace("delete", "&nbsp;<B>DELETE</B>", $Sql)  ;
    $Sql = eregi_replace("values", $BreakLine."&nbsp;&nbsp;<B>VALUES</B>", $Sql)  ;
    $Sql = eregi_replace("update", "&nbsp;<B>UPDATE</B>", $Sql)  ;
    $Sql = eregi_replace("straight_join", $BreakLine."<B>&nbsp;&nbsp;STRAIGHT_JOIN</B>", $Sql)  ;
    $Sql = eregi_replace("left join", $BreakLine."&nbsp;&nbsp;<B>LEFT JOIN</B>", $Sql)  ;
    //    $Sql = eregi_replace("join", $BreakLine."&nbsp;&nbsp;<B>JOIN</B>", $Sql)  ;
    $Sql = eregi_replace("select", "&nbsp;<B>SELECT</B>", $Sql)  ;
    $Sql = eregi_replace("from", $BreakLine."&nbsp;&nbsp;<B>FROM</B>", $Sql)  ;
    $Sql = eregi_replace("where", $BreakLine."&nbsp;&nbsp;<B>WHERE</B>", $Sql)  ;
    $Sql = eregi_replace("group by", $BreakLine."&nbsp;&nbsp;<B>GROUP BY</B>", $Sql)  ;
    $Sql = eregi_replace("having", $BreakLine."&nbsp;&nbsp;<B>HAVING</B>", $Sql)  ;
    $Sql = eregi_replace("order by", $BreakLine."&nbsp;&nbsp;<B>ORDER BY</B>", $Sql)  ;
    return $Sql;
  }


  public static function Dump($Obj) {
    $D= new Debug_Dump();
    return $D->out($Obj, "");
  }

  public static function isCurrentDay( $ValueYear, $ValueMonth, $ValueDay ) {
    $TodayDateValue= getdate();
    $TodayYear= $TodayDateValue['year'];
    $TodayMonth= $TodayDateValue['mon'];
    $TodayDay= $TodayDateValue['mday'];
    if ( $TodayYear== $ValueYear and $TodayMonth== $ValueMonth and $TodayDay== $ValueDay ) {
      return true;
    }
    return false;
  }

  public static function getTimeHoursDifference( $DateTimeArray, $MinutesLabel, $HoursLabel, $YesterdayLabel, $AgoLabel, $FromStartDateTimeArray=array(), $ReturnArray=false ) {
    if ( empty($FromStartDateTimeArray) ) {
      $FromStartDateTimeArray= getdate();
    }
    $Res='';
    //  Util::deb($DateTimeArray,'00 $DateTimeArray::');
    /*if( !is_array($DateTimeArray) ) {
    $Dat= $DateTimeArray;
    $DateTimeArray=array();
    $DateTimeArray['hours']= 0;
    $DateTimeArray['minutes']= 0;
    $DateTimeArray['seconds']= 0;
    $DateTimeArray['mon']= (int)strftime('%m',$Dat);
    $DateTimeArray['mday']= (int)strftime('%d',$Dat);
    $DateTimeArray['year']= (int)strftime('%Y',$Dat);
    Util::deb($DateTimeArray,'$DateTimeArray::');
    } */
    $DateBegin= mktime( $DateTimeArray['hours'],$DateTimeArray['minutes'], $DateTimeArray['seconds'], $DateTimeArray['mon'], $DateTimeArray['mday'], $DateTimeArray['year']);

    $DateEnd= mktime( $FromStartDateTimeArray['hours'], $FromStartDateTimeArray['minutes'], $FromStartDateTimeArray['seconds'], $FromStartDateTimeArray['mon'], $FromStartDateTimeArray['mday'], $FromStartDateTimeArray['year'] );
    //Util::deb( strftime("%Y-%m-%d %H:%M",$DateBegin),'strftime("%Y-%m-%d %H:%M",$DateBegin)::');
    //Util::deb( strftime("%Y-%m-%d %H:%M",$DateEnd),'strftime("%Y-%m-%d %H:%M",$DateEnd)::');

    $DateTimeRes= $DateEnd - $DateBegin;
    //Util::deb( strftime("%Y-%m-%d %H:%M",$DateTimeRes ),'strftime("%Y-%m-%d %H:%M",$DateTimeRes)::');
    //Util::deb($DateTimeRes,'$DateTimeRes::');

    //$ZoneHoursCount= Util::getZoneHours();
    //;Util::deb($ZoneHoursCount,'$ZoneHoursCount::');
    $YearsCount= strftime("%Y",$DateTimeRes) - 1970;
    $MonthsCount= strftime("%m",$DateTimeRes) - 1;
    $DaysCount= strftime("%d",$DateTimeRes) - 1;
    $HoursCount= strftime("%H",$DateTimeRes); // - $ZoneHoursCount;
    $MinutesCount= strftime("%M",$DateTimeRes);
    /*Util::deb($YearsCount,'$YearsCount::');
    Util::deb($MonthsCount,'$MonthsCount::');
    Util::deb($DaysCount,'$DaysCount::');
    Util::deb($HoursCount,'$HoursCount::');
    Util::deb($MinutesCount,'$MinutesCount::');    */
    if ( $ReturnArray ) {
      return array( 'year'=>$YearsCount, 'month'=> $MonthsCount, 'day'=>$DaysCount, 'hour'=>$HoursCount, 'minute'=>$MinutesCount );
    }
    //    die("!!!!");
    if ( $YearsCount> 0 ) {
      $Res.= date("l",$DateBegin).', '.strftime("%B",$DateBegin).' '.strftime("%d",$DateBegin).', '.strftime("%Y",$DateBegin);
    } else {

      if ( $MonthsCount> 0 ) {
        $Res.= date("l",$DateBegin).', '.strftime("%B",$DateBegin).' '.strftime("%d",$DateBegin);
      } else {

        if ( $DaysCount> 0 ) {

          if ( $DaysCount==1 ) {
            $Res.= $YesterdayLabel;
          } else {
            $Res.= date("l",$DateBegin).', '.strftime("%B",$DateBegin).' '.strftime("%d",$DateBegin);
          }
        } else {
          if ( substr($HoursCount,0,1)=='0' ) $HoursCount= substr($HoursCount,1,1);
          if ( substr($MinutesCount,0,1)=='0' ) $MinutesCount= substr($MinutesCount,1,1);
          if ( $HoursCount> 0 ) {
            $Res.= $HoursCount.' '.$HoursLabel.' '.$AgoLabel;
          } else {
            $Res.= $MinutesCount.' '.$MinutesLabel.' '.$AgoLabel;
          }
        }
      }
    }
    return $Res;
  }

  public static function f_now(  ) {
    $DateValue= getdate();
    $Year= $DateValue['year'];
    $Month= $DateValue['mon'];
    $Day= $DateValue['mday'];
    $Hour= $DateValue['hours'];
    $Minute= $DateValue['minutes'];
    $Second= $DateValue['seconds'];
    return $Year.'-'.$Month.'-'.$Day.' '.$Hour.':'.$Minute.':'.$Second;
  }

  public static function ShowErrors($form) {
    Util::deb( $form->renderGlobalErrors(),'$form->renderGlobalErrors()::' );
    foreach($form->getFormFieldSchema() as $name=>$formField) {
      $Err= $formField->renderError();
      if ( !empty($Err) ) {
        Util::deb( $formField->renderLabelName(), -21 );
        Util::deb( $formField->renderError(), -22 );
      }
    }
  }

  public static function getZoneHours() {
    //if ( !Util::isDeveloperComp() ) return 0;
    $S= date("O");
    //Util::deb($S,'++$S::');
    $Sign= substr($S,0,1);
    //Util::deb($Sign,'$Sign::');
    $HoursCount= ( (int)substr($S,1,2) ) * ($Sign=="-"?-1:1);
    return $HoursCount;
  }

  /* return sorted list of countries */
  public static function getCountriesList($Instance,$AddEmptyCountry=false,$type=false) {
    $c = sfCultureInfo::getInstance($Instance->getUser()->getCulture());
    $countries = $c->getCountries();
    //
    if ( $type=='short' ) {
      $countries_in_short_list_array= array_keys(sfConfig::get('app_countries_in_short_list_array' ));
      //Util::deb($countries_in_short_list_array,'$countries_in_short_list_array::');
      foreach( $countries as $Key=>$country ) {
        if ( !in_array($Key,$countries_in_short_list_array) ) {
          unset($countries[$Key]);
        }
      }
    }
    if ( $AddEmptyCountry ) $countries['']=$AddEmptyCountry;
    asort($countries);
    return $countries;
  }

  /* by Key of country return it's name */
  public static function getCountryName($Instance,$CountryKey, $CountriesList='') {
    if ( empty($CountriesList) or !is_array($CountriesList) ) {
      $CountriesList= Util::getCountriesList( $Instance, false );
    }
    foreach( $CountriesList as $key=>$name ) {
      if ( $CountryKey==$key ) return $name;
    }
    return '';
  }

  /* get list of states of USA */
  public static function getListOfStatesOfUSA() {
    return array (
    '' => '',
    'Alaska' => 'Alaska',
    'Alabama' => 'Alabama',
    'Arizona' => 'Arizona',
    'Arkansas' => 'Arkansas',
    'California' => 'California',
    'Colorado' => 'Colorado',
    'Connecticut' => 'Connecticut',
    'Delaware' => 'Delaware',
    'District of Columbia' => 'District of Columbia',
    'Florida' => 'Florida',
    'Georgia' => 'Georgia',
    'Hawaii' => 'Hawaii',
    'Idaho' => 'Idaho',
    'Illinois' => 'Illinois',
    'Indiana' => 'Indiana',
    'Iowa' => 'Iowa',
    'Kansas' => 'Kansas',
    'Kentucky' => 'Kentucky',
    'Louisiana' => 'Louisiana',
    'Maine' => 'Maine',
    'Maryland' => 'Maryland',
    'Massachusets' => 'Massachusets',
    'Michigan' => 'Michigan',
    'Minnesota' => 'Minnesota',
    'Mississipi' => 'Mississipi',
    'Missouri' => 'Missouri',
    'Montana' => 'Montana',
    'Nebraska' => 'Nebraska',
    'Nevada' => 'Nevada',
    'New Hampshire' => 'New Hampshire',
    'New Jersey' => 'New Jersey',
    'New Mexico' => 'New Mexico',
    'New York' => 'New York',
    'North Carolina' => 'North Carolina',
    'North Dacota' => 'North Dacota',
    'Ohio' => 'Ohio',
    'Oklahoma' => 'Oklahoma',
    'Oregon' => 'Oregon',
    'Pennsylvania' => 'Pennsylvania',
    'Rhode Island' => 'Rhode Island',
    'South Carolina' => 'South Carolina',
    'South Dacota' => 'South Dacota',
    'Tennessee' => 'Tennessee',
    'Texas' => 'Texas',
    'Utah' => 'Utah',
    'Vermont' => 'Vermont',
    'Virginia' => 'Virginia',
    'Washington' => 'Washington',
    'West Virginia' => 'West Virginia',
    'Wisconsin' => 'Wisconsin',
    'Wyoming' => 'Wyoming'
    );
  }


  public static function getTimezonesList($Instance) {
    $c = sfCultureInfo::getInstance($Instance->getUser()->getCulture());
    $timezone_groups = $c->getTimeZones();
    $display_key = 0;
    $timezones = array();
    foreach ($timezone_groups as $tz_group_key => $tz_group) {
      $array_key = null;
      foreach ($tz_group as $tz_key => $tz) {
        if ($tz_key == 0) $array_key = $tz;
        if ($tz_key == $display_key AND !empty($tz)) $timezones[$array_key] = $tz;
      }
    }
    $timezones = array_unique($timezones);
    asort($timezones);
    return $timezones;
  } //public static function getTimezonesList() {

  public static function getMonthsList($Instance) {
    $culture = sfCultureInfo::getInstance($Instance->getUser()->getCulture());
    $retval = array('culture'=>$culture);

    $dateFormatInfo = sfDateTimeFormatInfo::getInstance($culture);
    $date_format = strtolower($dateFormatInfo->getShortDatePattern());

    $retval['dateFormatInfo'] = $dateFormatInfo;

    $match_pattern = "/([dmy]+)(.*?)([dmy]+)(.*?)([dmy]+)/";
    if (!preg_match($match_pattern, $date_format, $match_arr)) {
      // if matching fails use en shortdate
      preg_match($match_pattern, 'm/d/yy', $match_arr);
    }

    $retval['date_seperator'] = $match_arr[2];

    // unset all but [dmy]+
    unset($match_arr[0], $match_arr[2], $match_arr[4]);

    $retval['date_order'] = array();
    foreach ($match_arr as $v) {
      // 'm/d/yy' => $retval[date_order] = array ('m', 'd', 'y');
      $retval['date_order'][] = $v[0];
    }
    return $retval;

  }

  public static function GetFileNameExt($FileName) {
    $K= strrpos( $FileName, "." );
    $Ext= '';
    if ( $K> 0 ) { // File has an extension
      $Ext= substr( $FileName, $K+1 );
    }
    return trim($Ext);
  }

}



class Debug_Dump {
  public static function get(&$obj, $LeftSp="") {
    if(is_array($obj)) {
      $type="Array[".count($obj)."]";
    } elseif(is_object($obj)) {
      $type="Object";
    } elseif(gettype($obj)=="boolean") {
      return $obj? "true" : "false";
    } else {
      return "\"".trim(ereg_Replace("\n","\\n",$obj))."\"";
    }
    $type= HtmlSpecialChars($type);
    $Buf= "<font color= green><b>".$type."</b></font>"; //<font color= green><b>
    $LeftSp .= "    ";
    for(Reset($obj); list($k,$v)=each($obj);) {
      if ("$k"=="GLOBALS") continue;
      if ( gettype($v)== "string")
      $v= HtmlSpecialChars($v);
      $Buf.="\n$LeftSp<b>".$k."</b> => ".Debug_Dump::get($v,$LeftSp);
    }
    return $Buf;
  }


  public static function out($obj) {
    return "<font size=3><pre>"./*HtmlSpecialChars(*/Debug_Dump::get($obj)/*)*/."</pre></font>";
  }

}