	<script type="text/javascript" src="/wp-includes/js/jquery/jquery.js"></script>
	<script type="text/javascript" src="<?php echo WP_PLUGIN_URL ?>/DataImport/js/funcs.js"></script>
<script type="text/javascript" >

function CalculateOnClick() {
  var zip_code= Trim(document.getElementById("zip_code").value);
  if ( zip_code=="" ) {
    alert("Enter Zip Code!")
    document.getElementById("zip_code").focus()
    return;
  }
  
  if ( !CheckInteger(zip_code) || zip_code.length != 5 ) {
    alert("Zip Code must be valid integer with 5 chars in length!")
    document.getElementById("zip_code").focus()
    return;
  }
  
  var rank= document.getElementById("rank").value;
  var year= document.getElementById("year").value;
  var HRef= "&zip_code="+encodeURIComponent(zip_code)+"&rank="+encodeURIComponent(rank)+"&year="+encodeURIComponent(year)
  
  var HRef= '<?php echo WP_PLUGIN_URL ?>'+"/DataImport/includes/ajax_funcs.php?task=make_bah_calc&zip_code="+encodeURIComponent(zip_code)+"&rank="+encodeURIComponent(rank)+"&year="+encodeURIComponent(year);
  //alert( "HRef::"+HRef );  
  
  jQuery.getJSON(HRef,
    {
    },
    onBahCalculated,

    function(x,y,z) {   //Some sort of error
      alert(x.responseText);
    }
  );    
}

function onBahCalculated(data) {
  var ErrorCode= data.ErrorCode
  if ( parseInt(ErrorCode)==0 ) {
    document.getElementById("div_bah_results").innerHTML= data.data;
  } 
}

</script>


<table width="90% valign="top" >
  
  <tr>
    <td >
      <?php ShowBahCalculator() ?>
    </td>
  </tr>

  
</table>
  
  <?php
function ShowBahCalculator() {  ?>
  <!-- BAH Calculator START -->    
<table width="600" style=" margin-left:50px;" cellpadding="6" valign="top">
  <tr>
    <td>
      <h2>BAH Calculator
        <span class="text">Updated: <?php echo strftime("%d/%m/%Y")?></span><br />
      </h2>
    </td>
  </tr>


  <tr>
    <td style="text-align:center"><br />

      <table width="100%" cellpadding="6" cellspacing="0" valign="top">
        <tr >
          <td width="33%" ><b>YEAR:</b></td>
          <td width="34%" ><b>DUTY 
          <a href="goto.cfm?link=http://zip4.usps.com/zip4/citytown_zip.jsp" target="_blank" ><b>ZIP CODE</b></a>:</b></td>
          <td width="33%"  ><b>PAY GRADE:</b></td>
        </tr>

        <tr >
          <td width="33%" >&nbsp;</td>
          <td width="34%" >&nbsp;</td>
          <td width="33%" >&nbsp;</td>
        </tr>

        <tr>
          <td width="33%" >
	          <select name="year" id="year" size="1" class="text" title="Select Year">
            <?php $YearsArray= array(2009,2010,2011, 2012); ?>
            <?php foreach( $YearsArray as $Year ) { ?>
              <option value="<?php echo $Year ?>" <?php echo ( (int)$Year==strftime("%Y") ?"selected":"") ?> ><?php echo $Year ?></option>
            <?php } ?>
            </select>  
          </td>
          <td width="34%" style="text-align:center"><input name="zip_code" id="zip_code" class="text" size=5 maxlength="5" title="Enter Zip Code"></td>
          <td width="33%" style="text-align:center">
            <select name="rank" id="rank" size=1 class="text" title="Enter Rank">
              <option value= "e1">E-1</option>
              <option value= "e2">E-2</option>
              <option value= "e3">E-3</option>
              <option value= "e4">E-4</option>
              <option value= "e5">E-5</option> 
              <option value= "e6">E-6</option>
              <option value= "e7">E-7</option>
              <option value= "e8">E-8</option>
              <option value= "e9">E-9</option>
              <option value= "w1">W-1</option>
              <option value= "w2">W-2</option> 
              <option value= "w3">W-3</option> 
              <option value= "w4">W-4</option>
              <option value= "w5">W-5</option>
              <option value= "o1e">O1E</option> 
              <option value= "o2e">O2E</option> 
              <option value= "o3e">O3E</option> 
              <option value= "o1">O-1</option> 
              <option value= "o2">O-2</option>
              <option value= "o3">O-3</option> 
              <option value= "o4">O-4</option> 
              <option value= "o5">O-5</option> 
              <option value= "o6">O-6</option> 
              <option value= "o7">O-7+</option>
            </select>
          </td>
        </tr>
      </table>
      <!--  calculator data entry END  --> 

      <br /><input type="button" name="submit2" value="CALCULATE" class="text" onclick="javascript:CalculateOnClick()"/><br /><br />

    </td>
  </tr>

  <tr>
    <td>
      <div id="div_bah_results"></div>
    </td>
  </tr>  
</table>
<!-- BAH Calculator END -->
<?php }  ?>