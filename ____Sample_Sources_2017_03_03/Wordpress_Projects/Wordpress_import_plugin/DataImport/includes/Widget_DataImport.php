<?php


function Widget_DataImport($args) {
  extract($args);
  global $q_config;

  
  echo $before_widget;
  echo $before_title;
  echo $after_title;
  $Title= 'Bah Calculator';
  $Url= 'bah-calc.php';
  
  echo '<br><a href="'.$Url.'">'.$Title.'</a><br>' /* . '<br><a href="'.$TestingUrl.'">'.$TestingTitle.'</a><br>'*/;
  
  echo $after_widget;
}

function Register_Widget_DataImport() {  
  register_sidebar_widget('Data Import', 'Widget_DataImport');  
  register_widget_control('Data Import', 'DataImport_OptionsEditor' );
}

function DataImport_OptionsEditor() {
  if (!empty($_REQUEST['DataImport_Title'])) {
    update_option('DataImport_Title', $_REQUEST['DataImport_Title']);
  }
  if (!empty($_REQUEST['DataImport_RememberCityInCookie'])) {
    update_option('DataImport_RememberCityInCookie', $_REQUEST['DataImport_RememberCityInCookie']);
  } else {
    delete_option('DataImport_RememberCityInCookie' );
  }

?>
    Title&nbsp;:&nbsp;<input type="text" name="DataImport_Title" value="<?php echo get_option('DataImport_Title') ?>" />
    Remember&nbsp;City&nbsp;In&nbsp;Cookie&nbsp;:&nbsp;<input type="checkbox" value="1" name="DataImport_RememberCityInCookie" <?php echo get_option('DataImport_RememberCityInCookie',0)?'checked':'' ?> />
<?
}

add_action('init', 'Register_Widget_DataImport');
add_filter('the_content', 'NSNBahCalculator_filter_the_content');

function NSNBahCalculator_filter_the_content($content_text) {
  $NSNBahCalculatorTagPattern= '/\[BahCalculator\]/i';
  $content_text= preg_replace_callback ( $NSNBahCalculatorTagPattern, "NSNBahCalculatorTagSubstitute", $content_text );
  return $content_text;
}



function NSNBahCalculatorTagSubstitute($matches) {
   //Util::deb( $matches, 'NSNBahCalculatorTagSubstitute   $matches::' );
  ob_start();
  include_once("bah-calc.php");
  $FileContent= ob_get_contents();
  ob_end_clean();
  return $FileContent;
}





function load_DataImport_validation() {
	//wp_enqueue_style( 'commentvalidation', WP_PLUGIN_URL . '/comment-validation/comment-validation.css');
	//wp_enqueue_script('jqueryvalidate', '/wp-includes/js/jquery/jquery.validate.pack.js');
	//Util::deb('wp_enqueue_script+++::');
	//wp_enqueue_script('commentvalidation', WP_PLUGIN_URL . '/comment-validation/comment-validation.js', array('jquery','jqueryvalidate'));
}