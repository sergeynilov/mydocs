<?php 
$DocumentRoot= $_SERVER['DOCUMENT_ROOT'];

$wpconfig = $DocumentRoot."/wp-config.php";
if (!file_exists($wpconfig))  {
  echo "Could not found wp-config.php. Error in path :\n\n".$wpconfig ;
  die;
}else {
}
require_once($wpconfig);
global $wpdb;

$task= $_REQUEST['task'];
$Data= array();


  //echo '<pre> -1 $task ::'.print_r( $task, true ).'</pre><br>';  
if ( $task== 'make_bah_calc') { // http://www.local-absolute.com/wp-content/plugins/DataImport/includes/ajax_funcs.php?task=make_bah_calc&zip_code=00501&rank=o3e&year=2012

  $zip_code= $_REQUEST['zip_code'];  // for testing 00501, 00544, 01001, 01002, 01003, 01007, 01008
  $rank= $_REQUEST['rank'];  /*  SELECT * FROM wp_bah_rates as br, wp_bah_zip_code as zc where  br.mha = zc.mha  */
  $year= $_REQUEST['year'];



  $Sql= "SELECT * FROM ".$wpdb->prefix."bah_rates as br,  ".$wpdb->prefix."bah_zip_code as zc   where br.year_pay = '" . $year . "' and zc.zip_code= '". $zip_code . "' and br.mha = zc.mha and br.has_dependents = 0 " ;
  // echo '<pre> -1 $Sql ::'.print_r( $Sql, true ).'</pre><br>';  
  $ResWithoutDependents = $wpdb->get_results( $Sql, ARRAY_A);
  if ( !empty($ResWithoutDependents[0]) ) { // found row!!
    $rank_value_without_dependents= '';
    if ( !empty( $ResWithoutDependents[0][$rank] ) ) {
      $rank_value_without_dependents= $ResWithoutDependents[0][$rank];
    }
  }

  $Sql= "SELECT * FROM ".$wpdb->prefix."bah_rates as br,  ".$wpdb->prefix."bah_zip_code as zc   where br.year_pay = '" . $year . "' and zc.zip_code= '". $zip_code . "' and br.mha = zc.mha and br.has_dependents = 1 " ;
  //echo '<pre> -2 $Sql ::'.print_r( $Sql, true ).'</pre><br>';  
  $ResWithDependents = $wpdb->get_results( $Sql, ARRAY_A);
  if ( !empty($ResWithDependents[0]) ) { // found row!!
    $rank_value_with_dependents= '';
    if ( !empty( $ResWithDependents[0][$rank] ) ) {
      $rank_value_with_dependents= $ResWithDependents[0][$rank];
    }
  }
  if ( !empty($rank_value_without_dependents) and !empty($rank_value_with_dependents) ) {
    $data= ShowResults( $wpdb, $zip_code, $rank, $year, $rank_value_without_dependents, $rank_value_with_dependents, $ResWithoutDependents, $ResWithDependents );
    echo json_encode(   array ( 'ErrorMessage'=>'', 'ErrorCode'=>0, 'data'=> $data )   );

  }
  else {
    $data= ShowResultsNotFound( $wpdb, $zip_code, $rank, $year );
    echo json_encode(   array ( 'ErrorMessage'=>'', 'ErrorCode'=>0, 'data'=> $data )   );
  }
}


function ShowResults( $wpdb, $zip_code, $rank, $year, $rank_value_without_dependents, $rank_value_with_dependents, $ResWithoutDependents, $ResWithDependents ) {
  $mha_name= '';
  $mha= '';
  if ( !empty( $ResWithDependents[0]['mha'] ) ) {
    $mha= $ResWithDependents[0]['mha'];
    $Sql= "SELECT name FROM ".$wpdb->prefix."bah_mha as m  where m.mha = '" . $ResWithDependents[0]['mha'] . "' " ;
    $Res = $wpdb->get_results( $Sql, ARRAY_A);
    if ( !empty($Res[0]['name']) ) {
      $mha_name= $Res[0]['name'];
    }
  }


  return '<H3 align="center" face="sans-serif">BASIC ALLOWANCE FOR HOUSING</H3>
</p><center><font face="sans-serif">Rate Query Results</font></center>
<br>
<TABLE WIDTH=100% BORDER=0 CELLPADDING=10 CELLSPACING=0 align="center" bgcolor="#FFFFFF">
  <TR> 
    <TD><b><font face="sans-serif" size="2">CY: </b>'.( $year> 2000? $year-2000 : $year ).'</font></TD>
  </TR>
  <TR> 
    <TD><b><font face="sans-serif" size="2">ZIP CODE: </b>'.$zip_code.'</font></TD>
  </TR>
  <TR> 
    <TD><b><font face="sans-serif" size="2">MILITARY HOUSING AREA:</b>
    '.$mha_name.'('.$mha.')</font></TD>
  </TR>

</TABLE></P>
<TABLE WIDTH=100% BORDER=1 CELLPADDING=10 CELLSPACING=0 align="center" bgcolor="#FFFFFF">
  <TR bgcolor="#000000"> 
    <TD colspan="2" bgcolor="#000066"><CENTER><b><font face="sans-serif" size="2" color="#FFFFFF">MONTHLY ALLOWANCE:</font></b></CENTER></TD>
  </TR>
  <TR> 
    <TD nowrap><CENTER><font face="sans-serif" size="2"><b>'.strtoupper($rank).'
</b> with DEPENDENTS:</FONT></CENTER></TD>
    <TD nowrap><CENTER><font face="sans-serif" size="2"><b>'.strtoupper($rank).'
</b> without DEPENDENTS:</FONT></CENTER></TD>

  </TR>
  <TR> 
    <TD><CENTER><font face="sans-serif" size="2">$ '.$rank_value_with_dependents.'</CENTER></TD>
    <TD><CENTER><font face="sans-serif" size="2">$ '.$rank_value_without_dependents.'</CENTER></TD>
  </TR>
  <TR> 
    <TD height="37" colspan="2" align="left" valign="middle"><font face="sans-serif" size="2">
See BAH Frequently Asked Questions for more information. For other BAH concerns, contact your service\'s BAH POC.</TD>
  </TR>

</TABLE>

';
}

function ShowResultsNotFound( $wpdb, $zip_code, $rank, $year ) {
  return '<H1>Zip Code Not Found</H1>
Sorry, zip code <EM>'.$zip_code.'</EM> was not found.
<P><HR><P>';  
}