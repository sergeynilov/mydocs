<?php
require_once('./admin.php');
$DataImportsRowsInList= (int)get_option('DataImportsRowsInList',10);
$current_screen= 'toplevel_page_DataImport/DataImport-plugin.class';
$errors = new WP_Error();
$role =& get_role( 'administrator' );
$EditorRole =& get_role( 'editor' );

$importing = '';
if ( isset($_FILES['importing']) ) {
  $importing = $_FILES['importing'];
}

$type_of_importing= '';
if ( isset($_REQUEST['type_of_importing']) ) {
  $type_of_importing = $_REQUEST['type_of_importing'];
}

$year_pay_rate= '';
if ( isset($_REQUEST['year_pay_rate']) ) {
  $year_pay_rate = $_REQUEST['year_pay_rate'];
}

$HasErrors= false;
if ( strlen($importing['tmp_name'])==0 and isset($_POST['type_of_importing']) ) {
  $errors->add( 'importing', __( "<strong>ERROR</strong>: File Of Importing must be selected! " ), array( 'form-field' => 'importing' ) );
  $HasErrors= true;
}

if ( strlen($type_of_importing)==0 and isset($_POST['type_of_importing']) ) {
  $errors->add( 'type_of_importing', __( "<strong>ERROR</strong>: Type Of Importing must be selected! " ), array( 'form-field' => 'type_of_importing' ) );
  $HasErrors= true;
}

if ( ( $type_of_importing == "bah_rates_with_dependents" or $type_of_importing == "bah_rates_without_dependents" ) and
empty($year_pay_rate) ) {
  $errors->add( 'year_pay_rate', __( "<strong>ERROR</strong>: For Importing of Bah Rates Year must be selected! " ), array( 'form-field' => 'year_pay_rate' ) );
  $HasErrors= true;
}

if ( !$HasErrors ) {
  if ( $type_of_importing == 'zip_code' ) {
    $OutPutStr= ImportZipCodes( $importing['name'], $importing['tmp_name'], $importing['size'] );
  }

  if ( $type_of_importing == 'mha' ) {
    $OutPutStr= ImportMhas( $importing['name'], $importing['tmp_name'], $importing['size'] );
  }

  if ( $type_of_importing == 'bah_rates_with_dependents' ) {
    $OutPutStr= ImportBahRates( $importing['name'], $importing['tmp_name'], $importing['size'], $year_pay_rate, 1 );
  }

  if ( $type_of_importing == 'bah_rates_without_dependents' ) {
    $OutPutStr= ImportBahRates( $importing['name'], $importing['tmp_name'], $importing['size'], $year_pay_rate, 0 );
  }
}

$ErrorsHTML= PrepareErrorsHTML( $errors, $DataImportCategoriesList, $DataImportCategoriesCount, $filter_active );

$RowsHTML= PrepareRowsHTML( $type_of_importing, $year_pay_rate );

$pagenaviActivated= Util::IsPluginActivated('wp-pagenavi/');
$css_file = '';
$PaginationHTML= '';
if ( $pagenaviActivated ) {
  $css_file = '<link rel="stylesheet" type="text/css" href="/wp-content/plugins/wp-pagenavi/pagenavi-css.css" media="screen" />';
  ob_start();
  wp_pagenavi( '', '', array('total_pages'=>$total_pages ) );
  $PaginationHTML= ob_get_contents();
  ob_end_clean();
}

$FootHTML= PrepareFootHTML( $DataImportsList, $DataImportsCount, $filter_data_import_category_id );
$TypeSelectionsHTML= PrepareTypeSelectionsHTML( array('all'=>'All','empty'=>'Empty','nonempty'=>'Non Empty'), 3, $filter_type );

$HeadHTML= PrepareHeadHTML( $DataImportsList, $DataImportsCount, $filter_data_import_category_id );
$BulkActionsHTML= PrepareBulkActionsHTML( $DataImportsList, $DataImportsCount, $filter_data_import_category_id );
$Res=  $ErrorsHTML.$css_file.'<div class="wrap">

	<div id="icon-users" class="icon32"><br /></div>
<h2>Data Import  </h2> <br><span style="weight:normal">' .$OutPutStr.'</span>



<script type=\'text/javascript\' >
function GetBrowserType() {
  if ( navigator.appName.indexOf("Microsoft") != -1 ) {
    return "MSIE";
  }
  if ( navigator.appName.indexOf("Opera") != -1 ) {
    return "Opera";
  }
  if ( navigator.appName.indexOf("Netscape") != -1 ) {
    return "Netscape";
  }

  return "";
}

function GetShowTRMethod() {
  var BrowserType= GetBrowserType();
  //alert("BrowserType:"+BrowserType);
  if ( BrowserType== "MSIE" ) return "inline";
  if ( BrowserType== "Opera" ) return "table-row";
  if ( BrowserType== "Netscape" ) return "table-row";
  return "";
}

function onDataImportDelete(Id) {
  if ( !confirm("Do you want to delete this Data Import ?") ) return;
  var HRef= "' /*. nsn_data_imports::getDataImportsListScript( array('action'=>'data_import_delete', 'id'=>'' ))*/.'"+Id;
  document.location= HRef;
}

function TypeOfImportingOnChange() {
  var type_of_importing= document.getElementById("type_of_importing").value;
  if ( type_of_importing == "bah_rates_with_dependents" || type_of_importing == "bah_rates_without_dependents" ) {
    document.getElementById("tr_year_pay_rate").style.display= GetShowTRMethod()
  } else {
    document.getElementById("tr_year_pay_rate").style.display= "none"
    document.getElementById("year_pay_rate").value= ""
  }
}
</script>


<form id="posts-filter" action="" method="post" enctype="multipart/form-data" >
<div class="tablenav">

' . ( count($DataImportsList)>0?$BulkActionsHTML:'' ) . '
</div>
<table class="widefat fixed" cellspacing="0">
'.(count($DataImportsList)>0?$HeadHTML:'').'
'.(count($DataImportsList)>0?$FootHTML:'').'
<tbody id="users" class="list:user user-list">
'.$RowsHTML.'
</tbody>
</table>'.$PaginationHTML.'

</form>
</div>
';
echo $Res;
return;
// contextual help - choose Help on the top right of admin panel to preview this.



function PrepareTypeSelectionsHTML( $GoodCategoriesList, $GoodCategoriesCount, $filter_type ) {
  return '';
}



function PrepareRowsHTML( $type_of_importing, $year_pay_rate ) {
  $DisplayHTML= ' style="display:none" ';
  if ( $type_of_importing=="bah_rates_with_dependents" or $type_of_importing=="bah_rates_without_dependents" ) {
    $DisplayHTML= '';
  }
  return
  '      <tr>
        <td class="left">
          Select data file for importing&nbsp;:
        </td>
        <td class="right">
         	<input size="30" maxlength="100" class="text" type="file" name="importing" id="importing" /><span class="error"></span>
        </td>
      </tr>
      
      
     <tr>
        <td class="left">
          Select type of importing&nbsp;:
        </td>
        <td class="right">
         	<select name="type_of_importing" id="type_of_importing" onchange="javascript:TypeOfImportingOnChange()">
         	  <option value=""> -Select type of importing- </option>
         	  <option value="zip_code" '.($type_of_importing=="zip_code"?"selected":"").'>Zip Codes</option>
         	  <option value="mha" '.($type_of_importing=="mha"?"selected":"").'>Military Housing Areas</option>         	  
         	  <option value="bah_rates_with_dependents" '.($type_of_importing=="bah_rates_with_dependents"?"selected":"").'>BAH Rates with dependents</option>
         	  <option value="bah_rates_without_dependents" '.($type_of_importing=="bah_rates_without_dependents"?"selected":"").'>BAH Rates without dependents</option>         	   
         	</select>
        </td>
      </tr>

     <tr '  . $DisplayHTML . ' id="tr_year_pay_rate" >
        <td class="left">
          Select Year of Pay Rate:
        </td>
        <td class="right">
         	<select name="year_pay_rate" id="year_pay_rate" >
         	  <option value=""> -Select Year of Pay Rate- </option>
         	  <option value="2009" '.($year_pay_rate=="2009"?"selected":"").' >2009</option>
         	  <option value="2010" '.($year_pay_rate=="2010"?"selected":"").' >2010</option>
         	  <option value="2011" '.($year_pay_rate=="2011"?"selected":"").' >2011</option>
         	  <option value="2012" '.($year_pay_rate=="2012"?"selected":"").' >2012</option>
         	</select>
        </td>
      </tr>
           
      <tr>
        <td class="left">
          &nbsp;
        </td>
        <td class="right">
         	<input type="submit" value="Import" class="button" />
        </td>
      </tr>      
      ';




  $LangsInSystem= Util::getLangsInSystem();
  $RowsHTML= '';
  $I= 0;
  foreach($DataImportsList as $DataImport) {
    $CssClass= $I%2==0?' class="alternate" ':'';
    $RowsHTML.= '
  <tr id=\'tr_data_import-'.$DataImport['id'].'\' '.$CssClass.'>
    <th scope=\'row\' class=\'check-column\'>
      <input type=\'checkbox\' name=\'data_imports[]\' id=\'data_import_'.$DataImport['id'].'\' class=\'administrator\' value=\''.$DataImport['id'].'\' />
    </th>
    <td class="username column-username">
      '.$DataImport['name'].'
      <div class="row-actions">
        <span class=\'edit\'><a href="'/*.nsn_data_imports::getDataImportsListScript( array('action'=>'show_data_import_editor', 'id'=>$DataImport['id']) )*/.'">Edit</a></span>&nbsp;
        <span class=\'edit\'><a style="cursor:pointer;" onclick="javascript:onDataImportDelete('.$DataImport['id'].')" >Delete</a></span>
      </div>

    </td>
    <td class="name column-name">&nbsp;'. ( !empty($LangsInSystem[$DataImport['lang']]
    )?$LangsInSystem[$DataImport['lang']]:$DataImport['lang'] ) .
    '</td>
    <td class="name column-name">&nbsp;'. ( empty($DataImport['ref'])?'Empty':'' ).'
    </td>
  </tr>';
    $I++;
  }
  return $RowsHTML;
}

function PrepareFootHTML( $DataImportsList, $DataImportsCount, $filter_data_import_category_id ) {
  $FootHTML= '<tfoot>
<tr class="thead">
	<th scope="col"  class="manage-column column-cb check-column" style=""><input type="checkbox" /></th>
	<th scope="col"  class="manage-column column-username" style="">City</th>
	<th scope="col" style="">Language</th>
	<th scope="col" style="">Empty</th>
</tr>
</tfoot>
';
  return $FootHTML;
}

function PrepareHeadHTML( $DataImportsList, $DataImportsCount, $filter_data_import_category_id ) {
  $HeadHTML= '<thead>
<tr class="thead">
	<th scope="col" id="cb" class="manage-column column-cb check-column" style=""><input type="checkbox" /></th>
	<th scope="col" class="manage-column column-username" style="">City</th>
	<th scope="col" style="">Language</th>
	<th scope="col" style="">Empty</th>
</tr>
</thead>
';
  return $HeadHTML;
}

function PrepareBulkActionsHTML( $DataImportsList, $DataImportsCount, $filter_data_import_category_id ) {
  $BulkActionsHTML= '<div class="alignleft actions">
<select name="action">
<option value="" selected="selected">Bulk Actions</option>
<option value="delete">Delete</option>
</select>
<input type="submit" value="Apply" name="doaction" id="doaction" class="button-secondary action" />

<br class="clear" />
</div>

';
  return $BulkActionsHTML;
}

function PrepareErrorsHTML( $errors, $DataImportCategoriesList, $DataImportCategoriesCount, $filter_active ) {
  $Res= '';
  if ( isset($errors) && is_wp_error( $errors ) and count($errors->get_error_messages()) > 0) {
    $Res= '	<div class="error"><ul>';
    foreach ( $errors->get_error_messages() as $err ) {
      $Res.= "<li>$err</li>\n";
    }
    $Res.='</ul></div>';
  }
  return $Res;
}


function ImportZipCodes(  $name, $tmp_name, $size  ) { // sorted_zipmha11.txt
  global $wpdb;
  $FileLinesArray= file($tmp_name);
  $I= 0;
  $RowsAdded= 0;
  $RowsSkippedAsExisting= 0;
  $RowsSkippedAsWrongStructure= 0;
  foreach( $FileLinesArray as $FileLine ) {
    if ( strlen(trim($FileLine))== 0 ) continue;
    $I++;  // 00501 NY218
    $A=preg_split( '/ /', $FileLine );
    if ( count($A)!= 2 ) {
      $RowsSkippedAsWrongStructure++;
      continue;
    }
    $ZipCode= $A[0];
    $Mha= $A[1];

    $Sql= "SELECT * FROM ".$wpdb->prefix."bah_zip_code where zip_code= '" .$ZipCode."' ";
    $Res = $wpdb->get_results( $Sql, ARRAY_A);
    $IsSkipped= false;
    if ( !empty($Res[0]['zip_code']) ) {
      $RowsSkippedAsExisting++;
      $IsSkipped= true;
    }
    if ( !$IsSkipped ) {
      $Sql= "Insert into ".$wpdb->prefix."bah_zip_code ( zip_code , mha ) values ( '".$ZipCode."', '".$Mha."' ) ";
      $rval = $wpdb->query($Sql);
      if ( $rval ) {
        $RowsAdded++;
      } else {
        $RowsSkippedAsWrongStructure++;
      }
    }
  }
  $OutPutStr= 'Zip Codes File "'.$name.'" ('.Util::getFileSizeAsString($size).') is uploaded. <b>' . $I.'</b> rows in file.<br>'.
  '<b>'.$RowsAdded.'</b> Rows Added, ' .
  '<b>'.$RowsSkippedAsWrongStructure.'</b> Rows Skipped As Error/Wrong Data Structure, ' .
  '<b>'.$RowsSkippedAsExisting.'</b> Rows Skipped As Existing. ' ;
  return $OutPutStr;
}

function ImportMhas( $name, $tmp_name, $size ) { // mhanames11.txt
  global $wpdb;
  $FileLinesArray= file($tmp_name);

  $I= 0;
  $RowsAdded= 0;
  $RowsSkippedAsExisting= 0;
  $RowsSkippedAsWrongStructure= 0;
  foreach( $FileLinesArray as $FileLine ) {
    if ( strlen(trim($FileLine))== 0 ) continue;
    $I++; // AK400;KETCHIKAN, AK
    $A=preg_split( '/;/', $FileLine );
    if ( count($A)!= 2 ) {
      $RowsSkippedAsWrongStructure++;
      continue;
    }
    $Mha= trim($A[0]);
    $Name= trim($A[1]);
    $A=preg_split( '/,/', $Name );
    $L= count($A);
    $Place= '';
    if ( $L>= 2 ) {
      $Place= trim($A[ $L - 1 ]);
      $Name= '';
      for( $III= 0; $III< $L; $III++ ) {
        $Name.= trim($A[$III]);
      }

    }

    $Sql= "SELECT * FROM ".$wpdb->prefix."bah_mha where mha= '" .$Mha."' ";
    $Res = $wpdb->get_results( $Sql, ARRAY_A);
    $IsSkipped= false;
    if ( !empty($Res[0]['mha']) ) {
      $RowsSkippedAsExisting++;
      $IsSkipped= true;
    }

    if ( !$IsSkipped ) {
      $Sql= "Insert into ".$wpdb->prefix."bah_mha ( mha, name, place ) values ( '".$Mha."', '".$Name."', '".$Place."' ) ";
      $rval = $wpdb->query($Sql);
      if ( $rval ) {
        $RowsAdded++;
      } else {
        $RowsSkippedAsWrongStructure++;
      }
    }
  }
  $OutPutStr= 'Military Housing Areas File "'.$name.'" ('.Util::getFileSizeAsString($size).') is uploaded.<b> ' . $I.'</b> rows in file.<br>' .
  '<b>'.$RowsAdded.'</b> Rows Added, ' .
  '<b>'.$RowsSkippedAsWrongStructure.'</b> Rows Skipped As Error/Wrong Data Structure, ' .
  '<b>'.$RowsSkippedAsExisting.'</b> Rows Skipped As Existing. ' ;
  return $OutPutStr;
}

function ImportBahRates( $name, $tmp_name, $size, $year_pay_rate, $has_dependents ) { // bahwo11.txt
  global $wpdb;
  $FileLinesArray= file($tmp_name);

  $I= 0;
  $RowsAdded= 0;
  $RowsSkippedAsExisting= 0;
  $RowsSkippedAsWrongStructure= 0;
  foreach( $FileLinesArray as $FileLine ) {
    if ( strlen(trim($FileLine))== 0 ) continue;
    $I++; // AK400,1296.00,1296.00,1296.00,1296.00,1503.00,1509.00,1578.00,1782.00,1869.00,1512.00,1779.00,1878.00,2028.00,2127.00,1728.00,1848.00,2004.00,1506.00,1659.00,1905.00,2115.00,2178.00,2280.00,2325.00,2325.00,2325.00,2325.00
    $A=preg_split( '/,/', $FileLine );
    if ( count($A)!= 28 ) {
      $RowsSkippedAsWrongStructure++;
      continue;
    }
    $Mha= $A[0];
    $E_1= $A[1];
    $E_2= $A[2];
    $E_3= $A[3];
    $E_4= $A[4];
    $E_5= $A[5];
    $E_6= $A[6];
    $E_7= $A[7];
    $E_8= $A[8];
    $E_9= $A[9];

    $W_1= $A[10];
    $W_2= $A[11];
    $W_3= $A[12];
    $W_4= $A[13];
    $W_5= $A[14];

    $O_1_E= $A[15];
    $O_2_E= $A[16];
    $O_3_E= $A[17];

    $O_1= $A[18];
    $O_2= $A[19];
    $O_3= $A[20];
    $O_4= $A[21];
    $O_5= $A[22];
    $O_6= $A[23];
    $O_7= $A[24];

    $Sql= "SELECT * FROM ".$wpdb->prefix."bah_rates where mha= '" .$Mha."' and has_dependents = '" . $has_dependents . "' ";
    $Res = $wpdb->get_results( $Sql, ARRAY_A);
    $IsSkipped= false;
    if ( !empty($Res[0]['mha']) ) {
      $RowsSkippedAsExisting++;
      $IsSkipped= true;
    }

    if ( !$IsSkipped ) {  
      $Sql= "Insert into ".$wpdb->prefix."bah_rates ( mha, year_pay, has_dependents, e1, e2, e3, e4, e5, e6, e7, e8, e9,
  w1, w2, w3, w4, w5,
  o1e, o2e, o3e,
  o1, o2, o3, o4, o5, o6, o7 ) values ( '".$Mha."', '".$year_pay_rate    ."', '".$has_dependents."', '" . 
      $E_1    ."', '".$E_2."', '".$E_3."', '".$E_4."', '".$E_5."', '".$E_6."', '".$E_7."', '".$E_8."', '".$E_9."', ".
  "'" . $WE_1. "', '" . $WE_2 . "', '" . $WE_3 . "', '" . $WE_4 . "', '" . $WE_5 . "', ".
  " '".$O_1_E."', '".$O_2_E."', '".$O_3_E."', ".
  " '".$O_1."', '".$O_2."',  '".$O_3."',  '".$O_4."',  '".$O_5."',  '".$O_6."',  '".$O_7."' ) ";

  $rval = $wpdb->query($Sql);
  if ( $rval ) {
    $RowsAdded++;
  } else {
    $RowsSkippedAsWrongStructure++;
  }
    }


  }
  $OutPutStr= 'BAH Rates with'. ( $has_dependents ? '' : 'out' ) . ' dependents File "'.$name.'" ('.Util::getFileSizeAsString($size).') is uploaded.<b> ' . $I.'</b> rows in file.<br>' .
  '<b>'.$RowsAdded.'</b> Rows Added, ' .
  '<b>'.$RowsSkippedAsWrongStructure.'</b> Rows Skipped As Error/Wrong Data Structure, ' .
  '<b>'.$RowsSkippedAsExisting.'</b> Rows Skipped As Existing. ' ;
  return $OutPutStr;
}
