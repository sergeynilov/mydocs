<?php

/*
 
 Info for WordPress:
 ==============================================================================
 Plugin Name: Bah Calculator/Data Import
 Plugin URI: 
 Description: This plugin is Bah Calculator and Data Import.
 Version: 1.0.1
 Author: Nilov Sergey
 Author URI: 
 Text Domain:
 Domain Path: /lang/
 
*/

/*
Plugin Name: 
*/
include_once( 'includes/Widget_DataImport.php' );
require_once('includes/Utils.php');
$DataImportPlugin = new DataImport_plugin_base();

$DataImportPlugin->setPageTitle('Data Import'); // название плагина (заголовок)
$DataImportPlugin->setMenuTitle('Data Import'); // название в меню
$DataImportPlugin->setAccessLevel(5); // уровень доступа
$DataImportPlugin->setAddPageTo(1); // куда добавлять страницу: 1=главное меню 2=настройки 3=управление 4=шаблоны

//register_uninstall_hook(__FILE__, 'DataImportUninstallOptions');
register_activation_hook(__FILE__, 'DataImportSetOptions');

//register_deactivation_hook(__FILE__, 'DataImportUnsetOptions');
register_uninstall_hook(__FILE__, 'DataImportUninstallOptions');

add_action('admin_menu', array($DataImportPlugin, 'add_admin_menu'));
$action= 'show_data_imports_list';
if ( !empty($_REQUEST['action']) )
$action= $_REQUEST['action'];

function DataImportEditorHeader() {
?>
<style type='text/css'>  
#form_data_import_edit input.error, #form_data_import_edit textarea.error {
	background-color: #f99;
}
#form_data_import_edit div.error, #form_data_import_edit label.error {
	color: red;
}
#form_data_import_edit div.errorlabels label {
	display: block;
}
</style>

<script type="text/javascript" >
jQuery(document).ready(function($) {
});
</script>
<?php
}

function load_DataImportEditor_scripts_styles() {
  wp_enqueue_script('jquery_validate', WP_PLUGIN_URL . '/DataImport/js/jquery.validate.pack.js', array('jquery'));
}


class DataImport_plugin_base {
  private $page_title;  // todo to public
  private $menu_title;
  private $access_level;
  private $add_page_to; // 1=add_menu_page 2=add_options_page 3=add_theme_page 4=add_management_page
  private $short_description;

  public function setPageTitle($page_title) {$this->page_title= $page_title;}
  public function setMenuTitle($menu_title) {$this->menu_title= $menu_title;}
  public function setAccessLevel($access_level) {$this->access_level= $access_level;}
  public function setAddPageTo($add_page_to) {$this->add_page_to= $add_page_to;}

  function DataImport_plugin_base()
  {
    $this->ReadOptions();
  }


  function ReadOptions() {
    //$this->Option_nsn_DataImportsRowsInList= get_option('nsn_DataImportsRowsInList');
  }

  function add_admin_menu()
  {


    if ( $this->add_page_to == 1 ) {
      $MessageLabel= '';
      if ( $DataImportsEmptyCount > 0 )  {
        $MessageLabel= "<span class='update-plugins count-1'><span class='plugin-count'>".$DataImportsEmptyCount."</span></span>";
      }
      $icon_url = WP_PLUGIN_URL . '/' . plugin_basename(dirname(__FILE__)) . '/icon.gif';
      $MenuSlug= __FILE__;
      add_menu_page($this->page_title, $this->menu_title.$MessageLabel, $this->access_level, $MenuSlug, array($this, 'ShowAdminPageContent'), $icon_url);
      if ( $DataImportsEmptyCount > 0 )  {
        add_submenu_page($MenuSlug, __('Empty informers'), __('Empty informers'.' ('.$DataImportsEmptyCount.')', 'pm4wp'), 0, 'DataImport/DataImport.php&action=&type=empty', 'show_empty_informers');
      }
    }
    elseif ( $this->add_page_to == 2 ) {
      add_options_page($this->page_title, $this->menu_title, $this->access_level, __FILE__, array($this, 'ShowAdminPageContent'));
    }
    elseif ( $this->add_page_to == 3 )
    add_management_page($this->page_title, $this->menu_title, $this->access_level, __FILE__, array($this, 'ShowAdminPageContent'));
    elseif ( $this->add_page_to == 4 )
    add_theme_page($this->page_title, $this->menu_title, $this->access_level, __FILE__, array($this, 'ShowAdminPageContent'));
  }

  function show_empty_informers() {
    global $wpdb, $current_user;
  }
  function ShowAdminPageContent() {
    $action= 'show_data_imports_list';
    if ( !empty($_REQUEST['action']) )
    $action= $_REQUEST['action'];
    if ( $action== 'update_data_imports_options' ) {
      wp_redirect($redirect);
    } // if ( $action== 'update_data_imports_options' ) {

    if ( $action== 'update_data_import' ) {
      wp_redirect($redirect);
    } // if ( $action== 'update_data_import' ) {
    if ( $action== 'data_import_delete' ) {
      wp_redirect($redirect);
    } // if ( $action== 'data_import_delete' ) {

    if ( $action== 'delete' ) {
      wp_redirect($redirect);
    } // if ( $action== 'data_import_delete' ) {

    ?>
<div class="wrap">
	<div id="icon-themes" class="icon32"><br /></div>
<?php
if ( $action== 'show_data_imports_list' ) {
  $this->ShowDataImportsList();
}
if ( $action== 'show_data_imports_options' ) {
  $this->ShowDataImportsOptions();
}
if ( $action== 'show_data_import_editor' ) {
  $this->ShowDataImportEditor();
}
?>

</div>   
<?php
  } // function ShowAdminPageContent() {

  function ShowDataImportsOptions() {
    require_once( 'includes/data_imports_options.php');
  } //function ShowDataImportsOptions() {

  function ShowDataImportsList() {
    require_once( 'includes/data_import_form.php');
  } // function ShowDataImportsList() {

  function ShowDataImportEditor() {
    require_once( 'includes/data_imports_editor.php');
  } // function ShowDataImportEditor() {

}	// class DataImport_plugin_base {


function DataImportSetOptions() {
  global $wpdb;
  $SqlArray= array();

  $SqlArray[]= "CREATE TABLE IF NOT EXISTS `".$wpdb->prefix."bah_mha` (
  `mha` varchar(5) NOT NULL,
  `name` varchar(40) NOT NULL,
  `place` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`mha`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
";
  
  
  $SqlArray[]= "CREATE TABLE IF NOT EXISTS `".$wpdb->prefix."bah_rates` (
  `mha` varchar(5) NOT NULL,
  `year_pay` int(2) NOT NULL,
  `has_dependents` int(1) NOT NULL DEFAULT '0',
  `e1` float(9,2) NOT NULL,
  `e2` float(9,2) NOT NULL,
  `e3` float(9,2) NOT NULL,
  `e4` float(9,2) NOT NULL,
  `e5` float(9,2) NOT NULL,
  `e6` float(9,2) NOT NULL,
  `e7` float(9,2) NOT NULL,
  `e8` float(9,2) NOT NULL,
  `e9` float(9,2) NOT NULL,
  `w1` float(9,2) NOT NULL,
  `w2` float(9,2) NOT NULL,
  `w3` float(9,2) NOT NULL,
  `w4` float(9,2) NOT NULL,
  `w5` float(9,2) NOT NULL,
  `o1e` float(9,2) NOT NULL,
  `o2e` float(9,2) NOT NULL,
  `o3e` float(9,2) NOT NULL,
  `o1` float(9,2) NOT NULL,
  `o2` float(9,2) NOT NULL,
  `o3` float(9,2) NOT NULL,
  `o4` float(9,2) NOT NULL,
  `o5` float(9,2) NOT NULL,
  `o6` float(9,2) NOT NULL,
  `o7` float(9,2) NOT NULL,
  PRIMARY KEY (`mha`,`has_dependents`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
";
  
  
  $SqlArray[]= "CREATE TABLE IF NOT EXISTS `".$wpdb->prefix."bah_zip_code` (
  `zip_code` varchar(5) NOT NULL,
  `mha` varchar(5) NOT NULL,
  PRIMARY KEY (`zip_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
";
 
  foreach($SqlArray as $Sql) {
    $wpdb->query($Sql);
  }

}

function DataImportUninstallOptions() {
  global $wpdb;
    
  $SqlArray= array();
  $SqlArray[]= "DROP TABLE IF EXISTS `".$wpdb->prefix."bah_mha`;";    
  $SqlArray[]= "DROP TABLE IF EXISTS `".$wpdb->prefix."bah_rates`;";    
  $SqlArray[]= "DROP TABLE IF EXISTS `".$wpdb->prefix."bah_zip_code`;";
 
  foreach($SqlArray as $Sql) {
    $wpdb->query($Sql);
  }
  //add_option('nsn_DataImport_RowsInList', 10);    
}

function addDataImportSettingsLink( $links, $file )
{
  /*if ( $file == 'DataImport/DataImport.php' ){
    $settings_link = '<a href="/wp-admin/admin.php?page=DataImport/DataImport.php">' . __('Settings') . '</a>';
    $links[]= $settings_link;
  }
  return $links; */
}


?>