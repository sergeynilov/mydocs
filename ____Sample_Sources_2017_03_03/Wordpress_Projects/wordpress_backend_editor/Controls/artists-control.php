<?php
if ( ! defined( 'ArtistsSongs' ) ) {
    exit; // Exit if accessed directly
}
require_once( 'appArtistsSongsControl.php'); // file with parent common control class
class artistsControl extends  appArtistsSongsControl
{
    private $m_artistEditorItem;         // Array of edited current Artist
    private $m_artistsTblSource;         // artists table source
    private $m_artistReviewsTblSource;   // artist Reviews table source
    private $m_songsArtistsTblSource;
    private $m_artistPhotosTblSource;
    private $m_artistPhotosInappropriatesTblSource;
    private $m_artist_id;                // artist id in add(null)/edit mode
    private $m_is_reopen;                // if true saving row the editor with this row is reopened
    private $m_filter_name;              // filters values
    private $m_filter_is_active;
    private $m_filter_has_concerts;
    private $m_filter_registered_at_1;
    private $m_filter_registered_at_2;
    private $m_filter_with_photos;
    private $m_filter_with_inappropriate_photos;
    private $m_filter_with_reviews;
    private $m_filter_with_inappropriate_reviews;
    private $m_artistsIsActiveValueArray;
    private $m_artistsHasConcertsValueArray;
    private $m_usersSelectionList;
    private $m_songJenresList;
    private $m_songsPageUrl;
    private $m_relatedArtistPost;


    public function __construct($containerClass, $wpdb)
    {
        global $session;
        $this->m_pageUrl= '/wp-admin/admin.php?page=ArtistsSongs-artists-editor&';    // url to for artist's editor
        $this->m_labelNonce= 'm_artists-editor-nonce';
        $this->m_labelNonceInput= 'm_artists-editor-nonce-input';
        $this->m_songsPageUrl= '/wp-admin/admin.php?page=ArtistsSongs-songs-editor&';
        parent::__construct($containerClass, $wpdb, $session);
        $this->setValidationRules( array('required'=>'artist_name,  artist_is_active, artist_has_concerts, artist_ordering, artist_email , artist_site, artist_birthday, artist_registered_at, artist_info   ', 'name'=> 'artist_name', 'integer'=> 'artist_ordering ', 'email'=>'artist_email', 'filename'=>'artist_image', 'website'=> 'artist_site', 'date'=>'artist_birthday, ', 'datetime'=>'artist_registered_at', 'skip_sanitize'=>'artist_info') ); // must be called AFTER __construct
        require_once( $this->m_pluginDir . 'models/artists.php');                 // Models used in the control
        require_once( $this->m_pluginDir . 'models/songs-artists.php');
        require_once( $this->m_pluginDir . 'models/artist-photos.php');
        require_once( $this->m_pluginDir . 'models/artist-photos-inappropriate.php');
        require_once( $this->m_pluginDir . 'models/users.php');
        require_once( $this->m_pluginDir . 'models/artist-reviews.php');
        require_once( $this->m_pluginDir . 'models/artist-reviews-inappropriate.php');
        include_once( $this->m_pluginDir . 'models/posts.php');
        $this->m_is_insert= false;
    }

    protected function readRequestParameters() // before main - read all possible parameters from request
    {
        $this->m_filter_name = $this->getParameter( 'filter_name', '' );
        $this->m_filter_is_active = $this->getParameter( 'filter_is_active', '' );
        $this->m_filter_registered_at_1 = $this->getParameter( 'filter_registered_at_1', '' );
        $this->m_filter_registered_at_2 = $this->getParameter( 'filter_registered_at_2', '' );
        $this->m_filter_with_photos = $this->getParameter( 'filter_with_photos', '' );
        $this->m_filter_with_inappropriate_photos = $this->getParameter( 'filter_with_inappropriate_photos', '' );
        $this->m_filter_with_reviews = $this->getParameter( 'filter_with_reviews', '' );
        $this->m_filter_with_inappropriate_reviews = $this->getParameter( 'filter_with_inappropriate_reviews', '' );

        $this->m_paged = $this->getParameter( 'paged', 1 );
        $this->m_sort = $this->getParameter('sort');
        $this->m_sort_direction = $this->getParameter( 'sort_direction');
        $this->m_pageParametersWithSort = $this->preparePageParameters(true, true);
        $this->m_pageParametersWithoutSort = $this->preparePageParameters(false /*true */, false);
        $this->m_artist_id = $this->getParameter( 'artist_id' );
        $this->m_is_reopen = $this->getParameter( 'is_reopen' );
        $this->m_is_insert= empty($this->m_GETDataArray['artist_id']) ? 1 : 0;
        $this->m_action = $this->getParameter( 'action', 'list' );
        $this->m_flashMessage = $this->getFlashMessage();   // TODO
    } // protected function readRequestParameters() { // before main - read all possible parameters from request

    public function main() {
        parent::main();
        if ( !in_array($this->m_action,array('list','edit', 'add', 'delete', 'update', 'confirm_delete' )) ) $this->m_action= 'list';  // List is default

        $this->m_artistsTblSource = new Artists($this->m_wpdb);
        $this->m_artistReviewsTblSource = new ArtistReviews($this->m_wpdb);
        $this->m_songsArtistsTblSource = new SongsArtists($this->m_wpdb);
        $this->m_artistPhotosTblSource = new ArtistPhotos($this->m_wpdb);
        $this->m_artistPhotosInappropriatesTblSource = new ArtistPhotosInappropriates($this->m_wpdb);
        $this->m_artistsReviewsInappropriatesTblSource = new ArtistsReviewsInappropriates($this->m_wpdb);
        $this->m_artistEditorItem= array('ID'=>'','name'=>'','is_active'=>'', 'has_concerts'=>'','ordering'=>'', 'email'=>'', 'site'=>'', 'birthday'=>'', 'registered_at'=>'', 'info'=> '',  'updated'=>'',  'created'=>'');

        $this->m_artistsIsActiveValueArray= nsnClass_appFuncs::SetArrayHeader(array(array('key' => '', 'value' =>esc_html__(' -Select active- ')) ), $this->m_artistsTblSource->getArtistsIsActiveValueArray() );

        $this->m_artistsHasConcertsValueArray= nsnClass_appFuncs::SetArrayHeader(array(array('key' => '', 'value' =>esc_html__(' -Select has concerts- ')) ), $this->m_artistsTblSource->getArtistsHasConcertsValueArray() );

        $this->reviewInappropriatesOperationsArray= nsnClass_appFuncs::SetArrayHeader(array(array('key' => '', 'value' =>esc_html__(' -Leave as it is- ')) ), $this->m_artistsReviewsInappropriatesTblSource->reviewInappropriatesOperationsArray() );

        $UsersTblSource = new Users($this->m_wpdb);
        $this->m_usersSelectionList= nsnClass_appFuncs::setArrayHeader( array(array('key' => '', 'value' => ' -Select User- ')), $UsersTblSource->getUsersSelectionList() );
        $songsJenresTblSource = new SongsJenres($this->m_wpdb);
        $this->m_songJenresList= $songsJenresTblSource->getEmptySongJenresList();

        $postReturnData= $this->doPOSTRequest( $this->m_action, $this->m_artistEditorItem, array('ID'=>'artist_id', 'name'=>'artist_name', 'is_active'=>'artist_is_active', 'has_concerts'=>'artist_has_concerts', 'ordering'=>'artist_ordering', 'email'=> 'artist_email' , 'site'=> 'artist_site', 'birthday'=>'artist_birthday', 'registered_at'=>'artist_registered_at', 'info'=> 'artist_info' ) );     // POST REQUEST WORKOUT
        $this->m_artistEditorItem= isset($postReturnData['editorItem']) ? $postReturnData['editorItem'] : $this->m_artistEditorItem;
        $this->m_action= isset($postReturnData['action']) ? $postReturnData['action'] : $this->m_action;  // set which action must be from from request parameter
        ob_start();
        $this->doEditorFlow($this);  // by $this->m_action define which control must be run
        $output = ob_get_contents ();
        ob_end_clean ();
        echo $output;
    } // public function main() {




    protected function showRowsListDataForm( $parentControl ) {  // show rows of artists from result source
        $m_parentControl= $parentControl;
        $m_parentControl->m_itemsPerPage= getAppVar('backend_itemsPerPage');
        $m_parentControl->m_artistsCount= $this->m_artistsTblSource->getArtistsList(true, '', array('name' => $this->m_filter_name, 'is_active' => $this->m_filter_is_active,  'has_concerts' => $this->m_filter_has_concerts, 'registered_at_1' => $this->m_filter_registered_at_1, 'registered_at_2' => $this->m_filter_registered_at_2, 'with_photos' => $this->m_filter_with_photos, 'with_inappropriate_photos' => $this->m_filter_with_inappropriate_photos, 'with_reviews' => $this->m_filter_with_reviews, 'with_inappropriate_reviews' => $this->m_filter_with_inappropriate_reviews), $this->m_sort, $this->m_sort_direction);

        $m_parentControl->m_artistsList= array();
        $paginationMaxPage= (int)($m_parentControl->m_artistsCount/$m_parentControl->m_itemsPerPage) + ($m_parentControl->m_artistsCount % $m_parentControl->m_itemsPerPage==0?0:1);
        if (empty($this->m_paged) or $this->m_paged > $paginationMaxPage ) $this->m_paged= 1;
        if( $m_parentControl->m_artistsCount > 0 ) {
            $m_parentControl->m_artistsList = $this->m_artistsTblSource->getArtistsList(false, $this->m_paged, array('name' => $this->m_filter_name, 'is_active' =>           $this->m_filter_is_active, 'has_concerts' => $this->m_filter_has_concerts, 'registered_at_1' => $this->m_filter_registered_at_1, 'registered_at_2' => $this->m_filter_registered_at_2, 'with_all_related_fields_count'=>1, 'with_photos' => $this->m_filter_with_photos, 'with_inappropriate_photos' => $this->m_filter_with_inappropriate_photos, 'with_reviews' => $this->m_filter_with_reviews, 'with_inappropriate_reviews' => $this->m_filter_with_inappropriate_reviews, 'itemsPerPage'=> $m_parentControl->m_itemsPerPage), $this->m_sort, $this->m_sort_direction);
        }
        $m_parentControl->m_navigationHTML= $this->makePageNavigation($this->m_paged, $m_parentControl->m_itemsPerPage, $paginationMaxPage, '', '', false);
        require_once($this->m_pluginDir . 'views/admin/header.php');
        require_once($this->m_pluginDir . 'views/admin/artists-list.php'); // show list of artists view with header/footer
        require_once($this->m_pluginDir . 'views/admin/footer.php');
    } // protected function showRowsListDataForm( $parentControl )   { // show rows of artists from result source


    protected function addRowDataForm( $parentControl ) {  // show editor of artist view in "insert" mode
        $m_parentControl = $parentControl;
        require_once($this->m_pluginDir . 'views/admin/header.php');
        require_once($this->m_pluginDir . 'views/admin/artists-editor.php');
        require_once($this->m_pluginDir . 'views/admin/footer.php');
    } //protected function addRowDataForm( $parentControl ) {  // show editor of artist view in "insert" mode

    protected function initFieldsAddRow( $parentControl ) // set init values for some fields
    {
    }


    protected function editRowDataForm( $parentControl )   // show editor of song view in "edit" mode
    {
        $m_parentControl= $parentControl;
        if( !$this->m_hasValidationErrors or (is_array($this->m_hasValidationErrors) and count($this->m_hasValidationErrors) == 0)  ) { // that is not error on submitting - get data from db
            $this->m_artistEditorItem = $this->m_artistsTblSource->getRowById($this->m_artist_id);
        }
        if (empty($this->m_artistEditorItem)) {
            $this->setFlashMessage( esc_html__( "Artist not found." ) );
            wp_safe_redirect($this->m_pageUrl . 'action=list' . $this->m_pageParametersWithSort);
        }
        $SongsTblSource = new Songs($this->m_wpdb);
        $this->m_songsGroupedList= $SongsTblSource->getGongsGroupedList();

        $postsTblSource = new Posts($this->m_wpdb);
        $this->m_relatedArtistPost= $postsTblSource->getRelatedPostByMetaValue( $this->m_artist_id, 'artist' , $this->m_currentBackendLang, false );

        require_once($this->m_pluginDir . 'views/admin/header.php');
        require_once($this->m_pluginDir . 'views/admin/artists-editor.php');
        require_once($this->m_pluginDir . 'views/admin/footer.php');
    } // protected function editRowDataForm( $parentControl ) { // show editor of song view in "edit" mode

    protected function confirmDeleteRowDataForm( $parentControl ) // show form for confirming of artist Deletion with preview of artist's data
    {
        $m_parentControl = $parentControl;
        $this->m_artistEditorItem = $this->m_artistsTblSource->getRowById($this->m_artist_id);
        if (empty($this->m_artistEditorItem)) {
            $this->setFlashMessage(esc_html__("Artist not found."));
            wp_safe_redirect($this->m_pageUrl . 'action=list' . $this->m_pageParametersWithSort);
        }
        require_once($this->m_pluginDir . 'views/admin/header.php');
        require_once($this->m_pluginDir . 'views/admin/artists-confirm-delete.php');
        require_once($this->m_pluginDir . 'views/admin/footer.php');
    } // protected function confirmDeleteRowDataForm( $parentControl ) { // show form for confirming of artist Deletion with preview of artist's data

    protected function doBulkAction( $action ) { // run selected bulk action
        $artistsIdToDelete= array();
        $artistsDeletionQueries= array();
        if ($action != 'bulk_delete') {
            $this->m_artistsTblSource->tranBegin();
            $hasError= false;
            ob_start();

            foreach( $this->m_POSTDataArray as $nextKey=>$nextValue ) {
                $A= nsnClass_appFuncs::splitStringIntoArray($nextKey, 'cb-select-');
                if (count($A) == 2 ) {
                    if ($action == 'bulk_active') {
                        $ret= $this->m_artistsTblSource->updateArtist( $A[1], array( 'is_active' => 'A' ) );
                        if( $ret === FALSE ) { // Return values: This function returns the number of rows updated, or false if there is an error.
                            $hasError = true;
                            break;
                        }
                    }
                    if ($action == 'bulk_inactive') {
                        $ret= $this->m_artistsTblSource->updateArtist( $A[1], array( 'is_active' => 'I' ) );
                        if( $ret === FALSE ) {
                            $hasError = true;
                            break;
                        }
                    }
                }
            }
            if ( !$hasError ) {  // There was NO error so commit all changes
                $this->m_artistsTblSource->tranCommit();
                ob_end_clean ();
            } else { // There WAS error make rollback
                $output = ob_get_contents ();
                ob_end_clean ();
                $this->m_artistsTblSource->tranRollback();
                nsnClass_appFuncs::addLog(strip_tags($output),  __FILE__.', '.__LINE__, 'S', $this->m_wpdb);
                $this->setFlashMessage(esc_html__("Internal error" . ' : ' . strip_tags($output)));
                wp_safe_redirect($this->m_pageUrl . 'action=list' . $this->m_pageParametersWithSort);
                return; // all queries run with error - return $output asa error code!
            }
        } // if ($action != 'bulk_delete') {

        if ($action == 'bulk_delete') {  // delete list of artists with directory of artist's image
            foreach( $this->m_POSTDataArray as $nextKey=>$nextValue ) {
                $A= nsnClass_appFuncs::splitStringIntoArray($nextKey, 'cb-select-');
                if (count($A) == 2 ) {
                    $artistsIdToDelete[]= $A[1]; // list of all IDs of artists for deletion
                    $A = $this->m_artistsTblSource->deleteArtistRow($A[1], /* $onlyReturnQueries */ true); // return sql for deletion
                    if (isset($A) and is_array($A)) {
                        foreach( $A as $nextKey=>$nextValue ) {
                            $artistsDeletionQueries[] = $nextValue; // list of all sql statements fort deletion of artists and all related data
                        }
                    }
                }
            }

            $runQueriesRes = htmlspecialchars(strip_tags($this->m_artistsTblSource->runQueriesUnderTransaction($artistsDeletionQueries)), ENT_QUOTES);
            if (isset($runQueriesRes)) { // there was error during deletion
                nsnClass_appFuncs::addLog($runQueriesRes, __FILE__ . ', ' . __LINE__, 'S', $this->m_wpdb);
                $this->setFlashMessage(esc_html__("Internal error" . ' : ' . $runQueriesRes));
                wp_safe_redirect($this->m_pageUrl . 'action=list' . $this->m_pageParametersWithSort);
                return;
            }

            foreach( $artistsIdToDelete as $nextartistsId ) {
                $imageDir = $this->m_artistsTblSource->getImageDir($nextartistsId);
                nsnClass_appFuncs::deleteDirectory($imageDir);
            }
        } //  if ($action == 'bulk_delete') { // delete list of artists with directory of artist's image


        if ( !empty($action) ) {
            $this->setFlashMessage(esc_html__("Bulk operation " . (isset($this->m_POSTDataArray['bulk_operation_title']) ? $this->m_POSTDataArray['bulk_operation_title'] : "") . ' done.'));
        }
        wp_safe_redirect(  $this->m_pageUrl . 'action=list' . $this->m_pageParametersWithSort );
    } // protected function doBulkAction() { // run selected bulk action

    protected function doValidation($data) { // Validate fields in validation list $this->m_validationRules 1 more validation on unique artist's name
        $this->m_hasValidationErrors= parent::doValidation($data);

        if (empty($this->m_hasValidationErrors['artist_name'])) { // Is there is no error on name - check is it unique
            $similarArtistsCount = $this->m_artistsTblSource->getArtistsList(true, '', array('strict_name' => $this->m_POSTDataArray['artist_name'], 'id!='=> ( $this->m_is_insert ? "" : $this->m_POSTDataArray['artist_id'] ) ));    // search similar name object
            if ( $similarArtistsCount> 0 ) {  // FOUND similar name object - raise error
                $this->m_hasValidationErrors['artist_name'] = array('field_name' => 'artist_name', 'error_label' => esc_html__('There is already artist named').' "' . $this->m_POSTDataArray['artist_name'] . '" !');
            }
        } // if (empty($resValidated['artist_name'])) { // Is there is no error on name - check is it unique


        if (empty($this->m_hasValidationErrors['artist_email'])) { // Is there is no error on email - check is it unique
            $similarArtistsCount = $this->m_artistsTblSource->getArtistsList(true, '', array('strict_email' => $this->m_POSTDataArray['artist_email'], 'id!='=> ( $this->m_is_insert ? "" : $this->m_POSTDataArray['artist_id'] ) ));    // search similar email object
            if ( $similarArtistsCount> 0 ) {  // FOUND similar email object - raise error
                $this->m_hasValidationErrors['artist_email'] = array('field_name' => 'artist_email', 'error_label' => esc_html__('There is already artist  with this email').' "' . $this->m_POSTDataArray['artist_email'] . '" !');
            }
        } // if (empty($resValidated['artist_email'])) { // Is there is no error on email - check is it unique


        if (empty($this->m_hasValidationErrors['artist_site'])) { // Is there is no error on site - check is it unique
            $similarArtistsCount = $this->m_artistsTblSource->getArtistsList(true, '', array('strict_site' => $this->m_POSTDataArray['artist_site'], 'id!='=> ( $this->m_is_insert ? "" : $this->m_POSTDataArray['artist_id'] ) ));    // search similar site object
            if ( $similarArtistsCount> 0 ) {  // FOUND similar site object - raise error
                $this->m_hasValidationErrors['artist_site'] = array('field_name' => 'artist_site', 'error_label' => esc_html__('There is already artist  this site').' "' . $this->m_POSTDataArray['artist_site'] . '" !');
            }
        } // if (empty($resValidated['artist_site'])) { // Is there is no error on site - check is it unique

        return $this->m_hasValidationErrors;  // return list of errors
    } // protected function doValidation($data, $rules) { // Validate fields in validation list $this->m_validationRules 1 more validation on unique artist's name

    protected function saveRowData( $action, $editorItem ) { // Save instert/update 1 artist in Artist's editor
        $ret_artist_id = $this->m_artistsTblSource->updateArtist((!empty($this->m_POSTDataArray['artist_id']) ? $this->m_POSTDataArray['artist_id'] : ""), array('name' => $this->m_POSTDataArray['artist_name'], 'is_active' => $this->m_POSTDataArray['artist_is_active'], 'has_concerts' => $this->m_POSTDataArray['artist_has_concerts'], 'ordering' => $this->m_POSTDataArray['artist_ordering'],   'email' => $this->m_POSTDataArray['artist_email'], 'site' => $this->m_POSTDataArray['artist_site'], 'birthday' => $this->m_POSTDataArray['artist_birthday'], 'registered_at' => $this->m_POSTDataArray['artist_registered_at'], 'info' => $this->m_POSTDataArray['artist_info']));
        if ( $this->m_is_insert and $ret_artist_id ) {
            nsnClass_appFuncs::addLog('new artist ID : '.$ret_artist_id.' : "'.$this->m_POSTDataArray['artist_name'].'" was added!',  __FILE__.', '.__LINE__, 'I', $this->m_wpdb);
        }

        $redirectUrl = $this->m_pageUrl . 'action=list' . $this->m_pageParametersWithSort;
        $this->setFlashMessage( esc_html__("Artist") ." " . ($this->m_is_insert ?esc_html__("added") :esc_html__("updated") ) );
        nsnClass_appFuncs::makeFileUpload($_FILES, 'artist_image', $ret_artist_id, $this->m_artistsTblSource,false);   // attach uploaded image
        if ($this->m_is_reopen) {   // To Reopen editor after updating
            $redirectUrl = $this->m_pageUrl . 'action=edit&artist_id=' . $ret_artist_id . $this->m_pageParametersWithSort;
            $this->setFlashMessage( esc_html__("Artist") ." " . ($this->m_is_insert ?esc_html__("added") :esc_html__("updated") ) );
        }
        wp_safe_redirect($redirectUrl);
    } // protected function saveRowData( $action, $editorItem ) { // Save instert/update 1 artist in Artist's editor

    protected function deleteRowData() {  // delete artist with directory of artist's image
        $retStr = $this->m_artistsTblSource->deleteArtistRow($this->m_artist_id);
        if (isset($retStr)) {
            $this->setFlashMessage( esc_html__( "Internal error" . ' : ' . $retStr ) );
            wp_safe_redirect( $this->m_pageUrl . 'action=list'.$this->m_pageParametersWithSort );
            return;
        }

        $imageDir = $this->m_artistsTblSource->getImageDir($this->m_artist_id);
        nsnClass_appFuncs::deleteDirectory($imageDir);
        $this->setFlashMessage( esc_html__( "Artist was deleted" ) );
        wp_safe_redirect( $this->m_pageUrl . 'action=list'.$this->m_pageParametersWithSort );

    } // protected function deleteRowData( ) {  // delete artist with directory of artist's image


    protected function preparePageParameters($WithPage, $WithSort) // Prepare url for pagination with all fields in filter and sort and sort_direction
    {
        $ResStr = '';
        if ( isset( $_POST ) ) { // form was submitted
            if ($WithPage) {
                $paged = isset($this->m_POSTDataArray['paged']) ? $this->m_POSTDataArray['paged'] : 1;
                $ResStr .= isset($paged) ? 'paged=' . $paged . '&' : 'paged=1&';
            }
            $filter_name = isset($this->m_POSTDataArray['filter_name']) ? $this->m_POSTDataArray['filter_name'] : '';
            $ResStr .= isset($filter_name) ? 'filter_name=' . $filter_name . '&' : '';
            $filter_is_active = isset($this->m_POSTDataArray['filter_is_active']) ? $this->m_POSTDataArray['filter_is_active'] : '';
            $ResStr .= isset($filter_is_active) ? 'filter_is_active=' . $filter_is_active . '&' : '';

            $filter_registered_at_1 = isset($this->m_POSTDataArray['filter_registered_at_1']) ? $this->m_POSTDataArray['filter_registered_at_1'] : '';
            $ResStr .= isset($filter_registered_at_1) ? 'filter_registered_at_1=' . $filter_registered_at_1 . '&' : '';
            $filter_registered_at_2 = isset($this->m_POSTDataArray['filter_registered_at_2']) ? $this->m_POSTDataArray['filter_registered_at_2'] : '';
            $ResStr .= isset($filter_registered_at_2) ? 'filter_registered_at_2=' . $filter_registered_at_2 . '&' : '';
            $filter_with_photos = isset($this->m_POSTDataArray['filter_with_photos']) ? $this->m_POSTDataArray['filter_with_photos'] : '';
            $ResStr .= isset($filter_with_photos) ? 'filter_with_photos=' . $filter_with_photos . '&' : '';
            $filter_with_inappropriate_photos = isset($this->m_POSTDataArray['filter_with_inappropriate_photos']) ? $this->m_POSTDataArray['filter_with_inappropriate_photos'] : '';
            $ResStr .= isset($filter_with_inappropriate_photos) ? 'filter_with_inappropriate_photos=' . $filter_with_inappropriate_photos . '&' : '';

            $filter_with_reviews = isset($this->m_POSTDataArray['filter_with_reviews']) ? $this->m_POSTDataArray['filter_with_reviews'] : '';
            $ResStr .= isset($filter_with_reviews) ? 'filter_with_reviews=' . $filter_with_reviews . '&' : '';
            $filter_with_inappropriate_reviews = isset($this->m_POSTDataArray['filter_with_inappropriate_reviews']) ? $this->m_POSTDataArray['filter_with_inappropriate_reviews'] : '';
            $ResStr .= isset($filter_with_inappropriate_reviews) ? 'filter_with_inappropriate_reviews=' . $filter_with_inappropriate_reviews . '&' : '';

            if ($WithSort) {
                $sort = isset($this->m_POSTDataArray['sort']) ? $this->m_POSTDataArray['sort'] : '';
                $ResStr .= isset($sort) ? 'sort=' . $sort . '&' : '';
                $sort_direction = isset($this->m_POSTDataArray['sort_direction']) ? $this->m_POSTDataArray['sort_direction'] : '';
                $ResStr .= isset($sort_direction) ? 'sort_direction=' . $sort_direction . '&' : '';
            }
        } else {
            if ($WithPage) {
                $ResStr .= isset($this->m_GETDataArray['paged']) ? 'paged=' . $this->m_GETDataArray['paged'] . '&' : 'paged=1&';
            }
            $ResStr .= isset($this->m_GETDataArray['filter_name']) ? 'filter_name=' . $this->m_GETDataArray['filter_name'] . '&' : '';
            $ResStr .= isset($this->m_GETDataArray['filter_is_active']) ? 'filter_is_active=' . $this->m_GETDataArray['filter_is_active'] . '&' : '';
            $ResStr .= isset($this->m_GETDataArray['filter_registered_at_1']) ? 'filter_registered_at_1=' . $this->m_GETDataArray['filter_registered_at_1'] . '&' : '';
            $ResStr .= isset($this->m_GETDataArray['filter_registered_at_2']) ? 'filter_registered_at_2=' . $this->m_GETDataArray['filter_registered_at_2'] . '&' : '';
            $ResStr .= isset($this->m_GETDataArray['filter_with_photos']) ? 'filter_with_photos=' . $this->m_GETDataArray['filter_with_photos'] . '&' : '';
            $ResStr .= isset($this->m_GETDataArray['filter_with_inappropriate_photos']) ? 'filter_with_inappropriate_photos=' . $this->m_GETDataArray['filter_with_inappropriate_photos'] . '&' : '';
            $ResStr .= isset($this->m_GETDataArray['filter_with_reviews']) ? 'filter_with_reviews=' . $this->m_GETDataArray['filter_with_reviews'] . '&' : '';
            $ResStr .= isset($this->m_GETDataArray['filter_with_inappropriate_reviews']) ? 'filter_with_inappropriate_reviews=' . $this->m_GETDataArray['filter_with_inappropriate_reviews'] . '&' : '';

            if ($WithSort) {
                $ResStr .= isset($this->m_GETDataArray['sort']) ? 'sort=' . $this->m_GETDataArray['sort'] . '&' : '';
                $ResStr .= isset($this->m_GETDataArray['sort_direction']) ? 'sort_direction=' . $this->m_GETDataArray['sort_direction'] . '&' : '';
            }
        }
        if (substr($ResStr, strlen($ResStr) - 1, 1) == '&') {
            $ResStr = substr($ResStr, 0, strlen($ResStr) - 1);
        }
        return '&' . $ResStr;
    } // protected function preparePageParameters($WithPage, $WithSort) // Prepare url for pagination with all fields in filter and sort and sort_direction

}