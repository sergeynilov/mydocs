<?php
define('DOING_AJAX', true);
require_once( '../../../wp-load.php'); /

if (!defined('ArtistsSongs')) {
    exit; // Exit if accessed directly
}

require_once( 'appArtistsSongsControl.php'); // parent common control class file
class ajaxBackendControl extends appArtistsSongsControl   // extends parent common control class
{
    public function __construct($containerClass, $wpdb)
    {
        global $session;
        parent::__construct($containerClass, $wpdb, $session);
        $this->setValidationRules(array()); // must be called  AFTER __construct
        require_once($this->m_pluginDir . 'models/songs.php');   // Models used in the control
        require_once($this->m_pluginDir . 'models/artists.php');
        require_once($this->m_pluginDir . 'models/songs-artists.php');
        require_once($this->m_pluginDir . 'models/songs-jenres.php');
    }

    protected function readRequestParameters() // before main - read all possible parameters from request
    {
        $this->m_action = $this->getParameter('action');
        if (isset($_GET['action']) and $_GET['action'] == 'upload-artist-photo') {
            $this->m_action = 'upload-artist-photo';
        }
    } // protected function readRequestParameters() { // before main - read all possible parameters from request

    public function main()   // entrance point
    {
        parent::main();
        $action = $this->getParameter('action');
        $m_parentControl = $this;

        if ($this->m_is_trimDataBeforeSave) {
            foreach ($_REQUEST as $nextKey => $nextValue) {
                $_REQUEST[$nextKey] = trim($nextValue);
            }
        }

        /* ARTIST'S CONCERT START BLOCK */
        if ($this->m_action == 'get-artist-concerts') {   // get list of concerts by artist_id
            require_once($this->m_pluginDir . 'models/artist-concerts.php');
            require_once($this->m_pluginDir . 'models/artist-tour.php');
            require_once($this->m_pluginDir . 'models/posts.php');
            $postsTblSource = new Posts($this->m_wpdb);

            $currentBackendLang= nsnClass_appLangs::getCurrentBackendLanguage(2, __FILE__.' : '.__LINE__ );

            $artist_id = $this->getParameter('artist_id');
            if (empty($artist_id)) {
                echo json_encode(array('error_code' => 1, 'error_message' => nsnClass_nsnClass_appFuncs::addLog('"artist_id" parameter must be specified!', __FILE__ . ', ' . __LINE__, 'S', $this->m_wpdb), 'content' => ''));
                return;
            }
            $filter = $this->getParameter('filter');
            $filter_artist_tour = $this->getParameter('filter_artist_tour');     // set filters for data request
            $search_in_description = $this->getParameter('search_in_description');
            $filter = $this->getParameter('filter');

            $plugin_url = $this->getParameter('plugin_url');
            $sort = 'tour_id, event_date';
            $sort_direction = 'asc';

            $ArtistsConcertsTblSource = new ArtistsConcerts($this->m_wpdb);   // create ArtistsConcerts class of model
            $artistsConcertsList = $ArtistsConcertsTblSource->getArtistsConcertsList(false, "" , array('artist_id' => $artist_id, 'show_user_display_name' => 1, 'filter' => $filter, 'tour_id'=> $filter_artist_tour,  'search_in_description'=> $search_in_description, 'filter'=> $filter ), $sort, $sort_direction);  // retrieve list of data be given parameters

            $ArtistsToursTblSource = new ArtistTours($this->m_wpdb);  // create ArtistTours class of model
            $artistToursList= $ArtistsToursTblSource->getArtistToursList();
            foreach( $artistToursList as $nextKey=>$nextArtistTour ) {
                $concertsSubList= array();
                foreach( $artistsConcertsList as $nextArtistsConcert ) {
                    if ($nextArtistTour['ID'] == $nextArtistsConcert['tour_id']) {
                        $concertsSubList[]= array( 'tour_id'=> $nextArtistsConcert['tour_id'],  'ID'=> $nextArtistsConcert['ID'], 'country'=> $nextArtistsConcert['country'], 'event_date'=> $nextArtistsConcert['event_date'] );
                    }
                }

                uasort($concertsSubList,'cmpTourConcerts');    // sorted list of Tour Concerts by event datetime
                $artistToursList[$nextKey]['concertsSubList']= $concertsSubList;
                $artistToursList[$nextKey]['concertsSubListCount']= count($concertsSubList);
            }

            reset($artistsConcertsList);
            $now= time();
            foreach( $artistsConcertsList as $nextKey=>$artistsConcert ) { // Prepare list of data with type of event(Past, today, next days, future... and convinient format of datetime of event date...)
                $artistsConcertsList[$nextKey]['place_name']= stripslashes( $artistsConcertsList[$nextKey]['place_name'] );
                $artistsConcertsList[$nextKey]['description']= stripslashes( $artistsConcertsList[$nextKey]['description'] );

                $event_date= strtotime($artistsConcert['event_date']);
                $artistsConcertsList[$nextKey]['eventDateYear']= strftime('%Y',$event_date);
                $artistsConcertsList[$nextKey]['eventDateMonth']= strftime('%m',$event_date);
                $artistsConcertsList[$nextKey]['eventDateDay']= strftime('%d',$event_date);
                $artistsConcertsList[$nextKey]['eventDateHour']= strftime('%H',$event_date);
                $artistsConcertsList[$nextKey]['eventDateMinute']= strftime('%M',$event_date);
                $compareDatesByDay= nsnClass_appFuncs::compareDatesByDay( $event_date, $now );
                if ( $compareDatesByDay == 0 ) { // that is today
                    $artistsConcertsList[$nextKey]['futureDate']= true;
                    $artistsConcertsList[$nextKey]['todayEvent']= true;
                    $artistsConcertsList[$nextKey]['nearestEvent']= true;
                }
                if ( $compareDatesByDay == 1 ) { // that is future day
                    $artistsConcertsList[$nextKey]['futureDate']= true;
                    $artistsConcertsList[$nextKey]['todayEvent']= false;

                    $datesDifference= nsnClass_appFuncs::getDatesDifference( $event_date, $now );
                    $artistsConcertsList[$nextKey]['nearestEvent']= false;
                    if ($datesDifference > 0 and $datesDifference <= 2) { // TODO
                        $artistsConcertsList[$nextKey]['nearestEvent']= true;
                    }
                }
                if ( $compareDatesByDay == -1 ) { // that is past day
                    $artistsConcertsList[$nextKey]['futureDate']= false;
                    $artistsConcertsList[$nextKey]['todayEvent']= false;
                    $artistsConcertsList[$nextKey]['nearestEvent']= false;
                }
                $relatedConcertPost= $postsTblSource->getRelatedPostByMetaValue( $artistsConcertsList[$nextKey]['ID'], 'concert' , $currentBackendLang, false );
                $relatePostPermalink= '';
                $relatePostTitle= '';
                $artistsConcertsList[$nextKey]['relatedConcertPostId']= '';
                if ( !empty($relatedConcertPost) ) {
                    $relatePostPermalink= get_permalink($relatedConcertPost->ID);
                    $relatePostTitle= $relatedConcertPost->post_title;
                    $artistsConcertsList[$nextKey]['relatedConcertPostId']= $relatedConcertPost->ID;
                }
                $artistsConcertsList[$nextKey]['relatePostPermalink']= $relatePostPermalink;
                $artistsConcertsList[$nextKey]['relatePostTitle']= $relatePostTitle;

            }
            echo json_encode(array('error_code' => 0, 'error_message' => '', 'artistsConcertsList' => $artistsConcertsList, 'artistsConcertsCount' => count($artistsConcertsList), 'artistToursList'=> $artistToursList, 'artistToursListCount'=> count($artistToursList) ));
            exit;
        } // if ( $this->m_action == 'get-artist-concerts' ) {  // get list of concerts by artist_id


        if ($this->m_action == 'update-artist-concert') {    // add/modify concert of artist
            if (empty($_REQUEST['artist_id']) or empty($_REQUEST['place_name']) or empty($_REQUEST['lat']) or empty($_REQUEST['lng']) or empty($_REQUEST['description']) or empty($_REQUEST['event_date'])) {
                echo json_encode(array('error_code' => 1, 'error_message' => nsnClass_appFuncs::addLog('"artist_id", "place_name", "lat", "lng", "description" and "event_date" parameters must be specified!', __FILE__ . ', ' . __LINE__, 'S', $this->m_wpdb), 'content' => ''));
                return;
            }
            require_once($this->m_pluginDir . 'models/artist-concerts.php');
            $ArtistsConcertsTblSource = new ArtistsConcerts($this->m_wpdb); // create ArtistsConcerts class of model
            $errCode = $ArtistsConcertsTblSource->updateArtistsConcerts(!empty($_REQUEST['artist_concert_id']) ? $_REQUEST['artist_concert_id'] : '', array('artist_id' => $_REQUEST['artist_id'], 'place_name' => $_REQUEST['place_name'], 'lat' => $_REQUEST['lat'], 'lng' => $_REQUEST['lng'], 'description' => $_REQUEST['description'], 'event_date' => $_REQUEST['event_date'], 'participants' => $_REQUEST['participants'], 'country' => $_REQUEST['country'])); // add(if artist_concert_id is empty)/modify concert of artist

            if ( !empty($_REQUEST['concert_post_id']) ) { // concert can have related post - then add meta info for this link
                add_post_meta($_REQUEST['concert_post_id'], 'song_artist_item', 'concert', true);
                add_post_meta($_REQUEST['concert_post_id'], 'song_artist_item_id', $errCode, true);
                add_post_meta($_REQUEST['concert_post_id'], 'post_type', 'artists_songs', true);
                add_post_meta($_REQUEST['concert_post_id'], 'song artist description ' . $errCode . ' ...', true);
                add_post_meta($_REQUEST['concert_post_id'], 'song_artist_show_target', 'inline', true);
            }

            echo json_encode(array('error_code' => ((isset($errCode) and !is_numeric($errCode)) ? 1 : 0), 'error_message' => ( (isset($errCode) and is_numeric($errCode)) ? $errCode : '' ), 'artist_concert_id' => ( isset($_REQUEST['artist_concert_id']) ? $_REQUEST['artist_concert_id'] : "" ) ));
            exit;
        } // if ( $this->m_action == 'update-artist-concert' ) {   // add/modify concert of artist


        if ( $this->m_action == 'delete-artist-concert' ) {  // delete artist concert by its ID
            if (empty($_REQUEST['artist_concert_id'])) {
                echo json_encode(array('error_code' => 1, 'error_message' => nsnClass_appFuncs::addLog('"artist_concert_id" parameter must be specified!', __FILE__ . ', ' . __LINE__, 'S', $this->m_wpdb), 'content' => ''));
                return;
            }
            require_once($this->m_pluginDir . 'models/artist-concerts.php');
            $ArtistsConcertsTblSource = new ArtistsConcerts($this->m_wpdb); // create ArtistsConcerts class of model
            $errCode= $ArtistsConcertsTblSource->deleteArtistsConcertsRow($_REQUEST['artist_concert_id']);
            if ( !empty($errCode) and is_numeric($errCode) ) {
                echo json_encode(array('error_code' => 1, 'error_message' => $errCode, 'artist_concert_id' => $_REQUEST['artist_concert_id']));
            } else {
                echo json_encode(array('error_code' => 0, 'error_message' => '', 'artist_concert_id' => $_REQUEST['artist_concert_id']));
            }
            exit;
        } // if ( $this->m_action == 'delete-artist-concert' ) {  // delete artist concert by its ID

        if ($_REQUEST['action'] == 'get-artist-concert-by-id') {  // get concert by its ID
            require_once($this->m_pluginDir . 'models/artist-concerts.php');
            require_once($this->m_pluginDir . 'models/posts.php');
            $currentBackendLang= nsnClass_appLangs::getCurrentBackendLanguage(2, __FILE__.' : '.__LINE__ );
            $ArtistsConcertsTblSource = new ArtistsConcerts($this->m_wpdb); // create ArtistsConcerts class of model
            if (empty($_REQUEST['artist_concert_id'])) {
                echo json_encode(array('error_code' => 1, 'error_message' => nsnClass_appFuncs::addLog('"artist_concert_id" parameter must be specified!', __FILE__ . ', ' . __LINE__, 'S', $this->m_wpdb), 'content' => ''));
                return;
            }
            $postsTblSource = new Posts($this->m_wpdb);    // create Posts class of model
            $relatedConcertPost= $postsTblSource->getRelatedPostByMetaValue( $_REQUEST['artist_concert_id'], 'concert' , $currentBackendLang, false );
            // find first post/post_id by given meta value and type of Meta Value and lang - post related for this concert

            $relatedConcertPostId= '';
            if ( !empty($relatedConcertPost) ) {
                $relatedConcertPostId = $relatedConcertPost->ID;
            }
            $with_inappropriates_count= isset($_REQUEST['with_inappropriates_count']) ? $_REQUEST['with_inappropriates_count'] : '';
            $show_artist_display_name= isset($_REQUEST['show_artist_display_name']) ? $_REQUEST['show_artist_display_name'] : '';

            $artistConcert = $ArtistsConcertsTblSource->getRowById($_REQUEST['artist_concert_id'], array('with_inappropriates_count' => $with_inappropriates_count, 'show_artist_display_name'=>$show_artist_display_name)); // retrieve concert by its ID with related info(if keys are true)
            $artistConcert['place_name']= stripslashes( $artistConcert['place_name'] );
            $artistConcert['description']= stripslashes( $artistConcert['description'] );
            $artistConcert['concert_post_id']= $relatedConcertPostId;
            $artistConcert['created'] = viewFuncs::formattedDateTime($artistConcert['created'], 'Common');
            echo json_encode(array('error_code' => 0, 'error_message' => '', 'artist_concert_id' => $_REQUEST['artist_concert_id'], 'artistConcert' => $artistConcert));
            exit;
        } // if ( $_REQUEST['action'] == 'get-artist-concert-by-id' ) {  // get concert by its ID

        if ($_REQUEST['action'] == 'autocomplete-events') {  // get list of concerts by artist_id in autocomplete format
            require_once($this->m_pluginDir . 'models/artist-concerts.php');
            $artist_id = $this->getParameter('artist_id');
            $q = $this->getParameter('q');
            $search_in_description = $this->getParameter('search_in_description');
            $search_in_past_events = $this->getParameter('search_in_past_events');
            if (empty($artist_id) or !isset($q)) {
                echo json_encode(array('error_code' => 1, 'error_message' => nsnClass_appFuncs::addLog('"artist_id" and "q" parameters must be specified!', __FILE__ . ', ' . __LINE__, 'S', $this->m_wpdb), 'content' => ''));
                return;
            }
            $sort = $this->getParameter('sort');
            $sort_direction = $this->getParameter('sort_direction');

            $ArtistsConcertsTblSource = new ArtistsConcerts($this->m_wpdb); // create ArtistsConcerts class of model
            $artistsConcertsList = $ArtistsConcertsTblSource->getArtistsConcertsList(false, "" , array('artist_id' => $artist_id, 'place_name' => $q, 'search_in_past_events' => $search_in_past_events, 'search_in_description'=> $search_in_description ), $sort, $sort_direction);// retrieve list of data be given parameters

            $resArray=  array();
            foreach( $artistsConcertsList as $nextArtistsConcert ) {
                $resArray[]= array( 'place_name'=> $nextArtistsConcert['ID'] .' : ' . stripslashes($nextArtistsConcert['place_name']), 'lat'=> $nextArtistsConcert['lat'], 'lng'=> $nextArtistsConcert['lng'] );
            }
            echo json_encode($resArray);
            exit;
        } // if ( $_REQUEST['action'] == 'autocomplete-events' ) {  // get list of concerts by artist_id in autocomplete format
        /* ARTIST'S CONCERT END BLOCK */



        /* ARTIST'S REVIEWS START BLOCK */
        if ($this->m_action == 'get-artist-reviews') { // retrieve reviews of artist by artist_id
            require_once($this->m_pluginDir . 'models/artist-reviews.php');
            $artist_id = $this->getParameter('artist_id');
            if (empty($artist_id)) {
                echo json_encode(array('error_code' => 1, 'error_message' => nsnClass_appFuncs::addLog('"artist_id" parameter must be specified!', __FILE__ . ', ' . __LINE__, 'S', $this->m_wpdb), 'content' => ''));
                return;
            }

            $plugin_url = $this->getParameter('plugin_url');
            $paged = $this->getParameter('paged', 1);
            $show_only_text = $this->getParameter('show_only_text', 0);
            $sort = $this->getParameter('sort');
            $sort_direction = $this->getParameter('sort_direction');

            $itemsPerPage = get_option('songs-artists-option_backend_itemsPerPage', 20);
            $ArtistReviewsTblSource = new ArtistReviews($this->m_wpdb);
            $artistReviewsCount = $ArtistReviewsTblSource->getArtistReviewsList(true, '', array('artist_id' => $artist_id), $sort, $sort_direction);
            $paginationMaxPage = (int)($artistReviewsCount / $itemsPerPage) + ($artistReviewsCount % $itemsPerPage == 0 ? 0 : 1);
            if ($paged > $paginationMaxPage) $paged = 1;
            $artistReviewsList = array();
            if ($artistReviewsCount > 0) {
                $artistReviewsList = $ArtistReviewsTblSource->getArtistReviewsList(false, ($show_only_text == 1 ? "" : $paged), array('artist_id' => $artist_id, 'show_user_display_name' => 1, 'show_inappropriate_count' => 1, 'itemsPerPage' => $itemsPerPage), $sort, $sort_direction);
            }
            $navigationHTML = $this->makePageNavigation($paged, $itemsPerPage, $paginationMaxPage, '', '', false, " backendArtistsEditorFuncs.loadArtistReviews( " . $artist_id . ", '#page#' ) ");
            ob_start();
            require($this->m_pluginDir . 'views/admin/artist-reviews-list.php');
            $content = ob_get_contents();
            ob_end_clean();
            echo json_encode(array('error_code' => 0, 'error_message' => '', 'content' => $content, 'reviewsCount' => $artistReviewsCount));
            exit;
        } // if ( $this->m_action == 'get-artist-reviews' ) { // retrieve reviews of artist by artist_id

        if ($this->m_action == 'update-artist-review') {  // add/modify artist's review
            if (empty($_REQUEST['artist_id']) or empty($_REQUEST['user_id']) or empty($_REQUEST['review_text'])) {
                echo json_encode(array('error_code' => 1, 'error_message' => nsnClass_appFuncs::addLog('"artist_id", "user_id" and "review_text" parameters must be specified!', __FILE__ . ', ' . __LINE__, 'S', $this->m_wpdb), 'content' => ''));
                return;
            }

            $artist_review_has_inappropriates = isset($_REQUEST['artist_review_has_inappropriates']) ? $_REQUEST['artist_review_has_inappropriates'] : '';
            require_once($this->m_pluginDir . 'models/artist-reviews.php');
            require_once($this->m_pluginDir . 'models/artist-reviews-inappropriate.php');
            $ArtistReviewsTblSource = new ArtistReviews($this->m_wpdb);

            $errCode= '';
            if ($artist_review_has_inappropriates == "D") {  // DELETE REVIEW
                $errCode = $ArtistReviewsTblSource->deleteArtistReviewsRow($_REQUEST['artist_review_id']);
                if ( empty($errCode) ) $errCode= $_REQUEST['artist_id'];
            } else {
                $errCode = $ArtistReviewsTblSource->updateArtistReviews(!empty($_REQUEST['artist_review_id']) ? $_REQUEST['artist_review_id'] : '', array('artist_id' => $_REQUEST['artist_id'], 'user_id' => $_REQUEST['user_id'], 'review_text' => $_REQUEST['review_text']));
                if ($artist_review_has_inappropriates == "C") {  // clear Inappropriate Mark
                    $ArtistsReviewsInappropriatesTblSource = new ArtistsReviewsInappropriates($this->m_wpdb);
                    $ArtistsReviewsInappropriatesTblSource->deleteByArtistsReviewsInappropriatesByArtistReviewId($_REQUEST['artist_review_id']);
                }
            }

            echo json_encode(array('error_code' => ((isset($errCode) and is_numeric($errCode)) ? 0 : 1), 'error_message' => ( (isset($errCode) and is_numeric($errCode)) ? $errCode : '' ), 'artist_review_id' => ( isset($_REQUEST['artist_review_id']) ? $_REQUEST['artist_review_id'] : "" ) ));
            exit;
        } // if ( $this->m_action == 'update-artist-review' ) { // add/modify artist's review


        if ($_REQUEST['action'] == 'delete-artist-review') { // delete artist's review by artist_review_id
            if (empty($_REQUEST['artist_review_id'])) {
                echo json_encode(array('error_code' => 1, 'error_message' => nsnClass_appFuncs::addLog('"artist_review_id" parameter must be specified!', __FILE__ . ', ' . __LINE__, 'S', $this->m_wpdb), 'content' => ''));
                return;
            }
            require_once($this->m_pluginDir . 'models/artist-reviews.php');
            $ArtistReviewsTblSource = new ArtistReviews($this->m_wpdb);

            $errCode= $ArtistReviewsTblSource->deleteArtistReviewsRow($_REQUEST['artist_review_id']);
            //echo '<pre>$errCode::'.print_r($errCode,true).'</pre>';
            if ( isset($errCode) and is_numeric($errCode) ) {
                echo json_encode(array('error_code' => 1, 'error_message' => $errCode, 'artist_review_id' => $_REQUEST['artist_review_id']));
            } else {
                echo json_encode(array('error_code' => 0, 'error_message' => '', 'artist_review_id' => $_REQUEST['artist_review_id']));
            }
            exit;
        } // if ( $_REQUEST['action'] == 'delete-artist-review' ) { // delete artist's review by artist_review_id


        if ($_REQUEST['action'] == 'get-artist-review-by-id') { // retrieve artist's review by artist_review_id
            require_once($this->m_pluginDir . 'models/artist-reviews.php');
            $ArtistReviewsTblSource = new ArtistReviews($this->m_wpdb);
            if (empty($_REQUEST['artist_review_id'])) {
                echo json_encode(array('error_code' => 1, 'error_message' => nsnClass_appFuncs::addLog('"artist_review_id" parameter must be specified!', __FILE__ . ', ' . __LINE__, 'S', $this->m_wpdb), 'content' => ''));
                return;
            }
            $with_inappropriates_count= isset($_REQUEST['with_inappropriates_count']) ? $_REQUEST['with_inappropriates_count'] : '';
            $show_artist_display_name= isset($_REQUEST['show_artist_display_name']) ? $_REQUEST['show_artist_display_name'] : '';

            $artistReview = $ArtistReviewsTblSource->getRowById($_REQUEST['artist_review_id'], array('with_inappropriates_count' => $with_inappropriates_count, 'show_artist_display_name'=>$show_artist_display_name)); // retrieve concert by its ID with related info(if keys are true)
            $artistReview['created'] = viewFuncs::formattedDateTime($artistReview['created'], 'Common');
            echo json_encode(array('error_code' => 0, 'error_message' => '', 'artist_review_id' => $_REQUEST['artist_review_id'], 'artistReview' => $artistReview));
            exit;
        } // if ( $_REQUEST['action'] == 'get-artist-review-by-id' ) {  // retrieve artist's review by artist_review_id
        /* ARTIST'S REVIEWS END BLOCK */


        /* ARTIST'S PHOTOS START BLOCK */
        if ($this->m_action == 'run-linked-ngg-gallery-operation') { // connection with NextGEN Gallery
            if (empty($_REQUEST['artist_id']) or empty($_REQUEST['linked_ngg_gallery_id']) or empty($_REQUEST['linked_ngg_gallery_name']) or empty ($_REQUEST['linked_ngg_gallery_id_operation'])) {
                echo json_encode(array('error_code' => 1, 'error_message' => nsnClass_appFuncs::addLog('"artist_id", "linked_ngg_gallery_id", "linked_ngg_gallery_name" and "linked_ngg_gallery_id_operation" parameters must be specified!', __FILE__ . ', ' . __LINE__, 'S', $this->m_wpdb), 'content' => ''));
                exit;
            }

            require_once($this->m_pluginDir . 'models/gallery.php');
            require_once( $this->m_pluginDir . 'models/artist-photos.php');
            $ArtistPhotosTblSource = new ArtistPhotos($this->m_wpdb);
            $upload_dir = nsnClass_appFuncs::getUploadDir();

            $galleryObj= '';
            if ( is_plugin_active( 'nextgen-gallery/nggallery.php' ) ) {
                $GalleriesTblSource = new Galleries($this->m_wpdb);
                $galleryObj = $GalleriesTblSource->getRowById($_REQUEST['linked_ngg_gallery_id']);
            }
            if ( empty($galleryObj) ) {
                echo json_encode(array('error_code' => 1, 'error_message' => nsnClass_appFuncs::addLog('"gallery not found!', __FILE__ . ', ' . __LINE__, 'S', $this->m_wpdb), 'content' => ''));
                exit;
            }


            $ArtistsTblSource = new Artists($this->m_wpdb);
            if ($_REQUEST['linked_ngg_gallery_id_operation'] == "no_operation") { // Artist's Gallery is not NextGEN Gallery
                $artist_id = $ArtistsTblSource->updateArtist($_REQUEST['artist_id'], array('ngg_gallery_gid' => ' null '));
                echo json_encode(array('error_code' => 0, 'error_message' => '', 'artist_id' => $artist_id));
                exit;
            } // if ( $_REQUEST['linked_ngg_gallery_id_operation']== "no_operation" ) {  // Artist's Gallery is not NextGEN Gallery


            if ($_REQUEST['linked_ngg_gallery_id_operation'] == "keep_link") {  // Artist's Gallery is link to existing NextGEN Gallery
                $ArtistPhotosTblSource->tranBegin();

                $artistPhotosList = $ArtistPhotosTblSource->getArtistPhotosList(false, '', array('artist_id' => $_REQUEST['artist_id']) );
                $filesToDelete= array();
                foreach ($artistPhotosList as $nextArtistPhoto) { // at first delete all rows in artist's Photos
                    $artist_photo_id = $ArtistPhotosTblSource->deleteArtistPhotosRow($nextArtistPhoto['ID']);
                    if (!$artist_photo_id) {
                        $ArtistPhotosTblSource->tranRollback();
                        echo json_encode(array('error_code' => 1, 'error_message' => 'DB Error deleting artist photo', 'artist_id' => $_REQUEST['artist_id']));
                        exit;
                    }
                    if (isset($nextArtistPhoto['photo_name'])) {

                        $dst_dirname = $upload_dir['basedir'] . DIRECTORY_SEPARATOR . 'artist_images' . DIRECTORY_SEPARATOR . 'artist_image_' . $nextArtistPhoto['artist_id'];
                        if (file_exists($dst_dirname . DIRECTORY_SEPARATOR . $nextArtistPhoto['photo_name'])) {
                            $filesToDelete[]= $dst_dirname . DIRECTORY_SEPARATOR . $nextArtistPhoto['photo_name'];
                        }
                    }
                } // foreach ($artistPhotosList as $nextArtistPhoto) { // at first delete all rows in artist's Photos
                foreach( $filesToDelete as $nextFileToDelete ) {
                    unlink($nextFileToDelete); // delete artist's Photos from disk
                }

                $artist_id = $ArtistsTblSource->updateArtist($_REQUEST['artist_id'], array('ngg_gallery_gid' => $_REQUEST['linked_ngg_gallery_id']));
                $ArtistPhotosTblSource->tranCommit();
                echo json_encode(array('error_code' => 0, 'error_message' => '', 'artist_id' => $artist_id));
                exit;
            } // if ( $_REQUEST['linked_ngg_gallery_id_operation']== "keep_link" ) {  // Artist's Gallery is link to existing NextGEN Gallery


            if ( $_REQUEST['linked_ngg_gallery_id_operation'] == "import_and_remove" ) { // Import into Artist's Gallery images from existing NextGEN Gallery
                $galleryImages= nggdb::get_gallery( $_REQUEST['linked_ngg_gallery_id'] );
                $dest_dirname = $upload_dir['basedir'] . DIRECTORY_SEPARATOR . 'artist_images' . DIRECTORY_SEPARATOR . 'artist_image_' . $_REQUEST['artist_id'];
                $artistImagesDirs = array($upload_dir['basedir'], $upload_dir['basedir'], $upload_dir['basedir'] . DIRECTORY_SEPARATOR . 'artist_images', $dest_dirname);

                $ArtistPhotosTblSource->tranBegin();
                $artistPhotosList = $ArtistPhotosTblSource->getArtistPhotosList(false, '', array('artist_id' => $_REQUEST['artist_id']) );
                $filesToDelete= array();
                foreach ($artistPhotosList as $nextArtistPhoto) { // at first delete all rows in artist's Photos
                    $artist_photo_id = $ArtistPhotosTblSource->deleteArtistPhotosRow($nextArtistPhoto['ID']);
                    if (!$artist_photo_id) {
                        $ArtistPhotosTblSource->tranRollback();
                        echo json_encode(array('error_code' => 1, 'error_message' => 'DB Error deleting artist photo', 'artist_id' => $_REQUEST['artist_id']));
                        exit;
                    }
                    if (isset($nextArtistPhoto['photo_name'])) {
                        $dst_dirname = $upload_dir['basedir'] . DIRECTORY_SEPARATOR . 'artist_images' . DIRECTORY_SEPARATOR . 'artist_image_' . $nextArtistPhoto['artist_id'];
                        if (file_exists($dst_dirname . DIRECTORY_SEPARATOR . $nextArtistPhoto['photo_name'])) {
                            $filesToDelete[]= $dst_dirname . DIRECTORY_SEPARATOR . $nextArtistPhoto['photo_name'];
                        }
                    }
                } // foreach ($artistPhotosList as $nextArtistPhoto) { // at first delete all rows in artist's Photos
                $ArtistPhotosTblSource->tranCommit();
                foreach( $filesToDelete as $nextFileToDelete ) {
                    unlink($nextFileToDelete); // delete artist's Photos from disk
                }

                foreach( $galleryImages as $nextGalleryImages ) { // Copy into empty Artist's Gallery images from existing NextGEN Gallery
                    $artist_photo_id= $ArtistPhotosTblSource->updateArtistPhotos( '', array( 'artist_id'=> $_REQUEST['artist_id'], 'description'=> $nextGalleryImages->_cache['description'], 'photo_name'=> $nextGalleryImages->_cache['filename'] ) ); // add rows in db at first
                    if (empty($artist_photo_id) ) {
                        $ArtistPhotosTblSource->tranRollback();
                        echo json_encode(array('error_code' => 1, 'error_message' => 'DB Error adding artist photo', 'artist_id' => $_REQUEST['artist_id']));
                        exit;
                    }

                    $srcFilenamePath= ABSPATH . $galleryObj['path'] . DIRECTORY_SEPARATOR . $nextGalleryImages->_cache['filename'];

                    $dest_filename = $dest_dirname . DIRECTORY_SEPARATOR . $nextGalleryImages->_cache['filename'];
                    $filesToCopy[]= array('src'=>$srcFilenamePath, 'dst'=>$dest_filename);
                } // foreach( $galleryImages as $nextGalleryImages ) { // Copy into empty Artist's Gallery images from existing NextGEN Gallery

                $ArtistsTblSource->updateArtist($_REQUEST['artist_id'], array('ngg_gallery_gid' => ' null '));
                $ArtistPhotosTblSource->tranCommit();
                nsnClass_appFuncs::createDir($artistImagesDirs);
                foreach( $filesToCopy as $nextFileToCopy ) {
                    $ret = copy($nextFileToCopy['src'], $nextFileToCopy['dst']); // copy files from array saved before
                }
                echo json_encode(array('error_code' => 0, 'error_message' => '', 'artist_id' => $_REQUEST['artist_id']));
                exit;

            } // if ( $_REQUEST['linked_ngg_gallery_id_operation'] == "import_and_remove" ) { // Import into Artist's Gallery images from existing NextGEN Gallery

            if ( $_REQUEST['linked_ngg_gallery_id_operation'] == "import_and_add" ) {  // Import into Artist's Gallery(withtout clearing) images from existing NextGEN Gallery
                require_once( $this->m_pluginDir . 'models/artist-photos.php');
                $ArtistPhotosTblSource = new ArtistPhotos($this->m_wpdb);
                $galleryImages= nggdb::get_gallery( $_REQUEST['linked_ngg_gallery_id'] );

                $upload_dir = nsnClass_appFuncs::getUploadDir();
                $dest_dirname = $upload_dir['basedir'] . DIRECTORY_SEPARATOR . 'artist_images' . DIRECTORY_SEPARATOR . 'artist_image_' . $_REQUEST['artist_id'];

                $artistImagesDirs = array($upload_dir['basedir'], $upload_dir['basedir'], $upload_dir['basedir'] . DIRECTORY_SEPARATOR . 'artist_images', $dest_dirname);
                $filesToCopy= array();
                $ArtistPhotosTblSource->tranBegin();
                foreach( $galleryImages as $nextGalleryImages ) { // Copy into empty Artist's Gallery images from existing NextGEN Gallery
                    $artist_photo_id= $ArtistPhotosTblSource->updateArtistPhotos( '', array( 'artist_id'=> $_REQUEST['artist_id'], 'description'=> $nextGalleryImages->_cache['description'], 'photo_name'=> $nextGalleryImages->_cache['filename'] ) );
                    if (empty($artist_photo_id) ) {
                        $ArtistPhotosTblSource->tranRollback();
                        echo json_encode(array('error_code' => 1, 'error_message' => 'DB Error adding artist photo', 'artist_id' => $_REQUEST['artist_id']));
                        exit;
                    }

                    $srcFilenamePath= ABSPATH . $galleryObj['path'] . DIRECTORY_SEPARATOR . $nextGalleryImages->_cache['filename'];
                    $dest_filename = $dest_dirname . DIRECTORY_SEPARATOR . $nextGalleryImages->_cache['filename'];
                    $filesToCopy[]= array('src'=>$srcFilenamePath, 'dst'=>$dest_filename);
                } // foreach( $galleryImages as $nextGalleryImages ) {  // Copy into empty Artist's Gallery images from existing NextGEN Gallery

                $ArtistsTblSource->updateArtist($_REQUEST['artist_id'], array('ngg_gallery_gid' => ' null '));
                $ArtistPhotosTblSource->tranCommit();
                nsnClass_appFuncs::createDir($artistImagesDirs);
                foreach( $filesToCopy as $nextFileToCopy ) {
                    $ret = copy($nextFileToCopy['src'], $nextFileToCopy['dst']);
                }
                echo json_encode(array('error_code' => 0, 'error_message' => '', 'artist_id' => $_REQUEST['artist_id']));
                exit;
            } // if ( $_REQUEST['linked_ngg_gallery_id_operation']== "import_and_add" ) {  // Import into Artist's Gallery(withtout clearing) images from existing NextGEN Gallery
            echo json_encode(array('error_code' => 1, 'error_message' => 'invalid operation', 'artist_id' => $_REQUEST['artist_id']));
            exit;
        } // if ( $this->m_action == 'run-linked-ngg-gallery-operation' ) { // connection with NextGEN Gallery


        if ($this->m_action == 'get-artist-photos') { // retrieve artist photos by artist_id
            require_once($this->m_pluginDir . 'models/artist-photos.php');
            require_once($this->m_pluginDir . 'models/gallery.php');
            $artist_id = $this->getParameter('artist_id');
            if (empty($artist_id)) {
                echo json_encode(array('error_code' => 1, 'error_message' => nsnClass_appFuncs::addLog('"artist_id" parameter must be specified!', __FILE__ . ', ' . __LINE__, 'S', $this->m_wpdb), 'content' => ''));
                return;
            }

            $this->m_galleryImagesPerRow = get_option('songs-artists-option_galleryImagesPerRow', 5);  // parameters gallery images from options
            $this->m_orig_width = get_option('songs-artists-option_thumbnailWidth', 100);
            $this->m_orig_height = get_option('songs-artists-option_thumbnailHeight', 80);

            $galleriesList= array();
            if ( is_plugin_active( 'nextgen-gallery/nggallery.php' ) ) {
                $GalleriesTblSource = new Galleries($this->m_wpdb);
                $galleriesList = $GalleriesTblSource->getGalleriesList(false, '', array('show_ngg_pictures_count'=>1), '', '');
            }

            $paged = $this->getParameter('paged', 1);
            $show_only_text = $this->getParameter('show_only_text', 0);
            $sort = $this->getParameter('sort');
            $sort_direction = $this->getParameter('sort_direction');
            $plugin_url = $this->getParameter('plugin_url');

            $itemsPerPage = get_option('songs-artists-option_backend_itemsPerPage', 20);
            $ArtistPhotosTblSource = new ArtistPhotos($this->m_wpdb);
            $ArtistsTblSource = new Artists($this->m_wpdb);
            $artistObj = $ArtistsTblSource->getRowById($artist_id);
            $artistPhotosCount = $ArtistPhotosTblSource->getArtistPhotosList(true, '', array('artist_id' => $artist_id), $sort, $sort_direction);
            $paginationMaxPage = (int)($artistPhotosCount / $itemsPerPage) + ($artistPhotosCount % $itemsPerPage == 0 ? 0 : 1);
            if ($paged > $paginationMaxPage) $paged = 1;
            $artistPhotosList = array();
            $inappropriatePhotosCount = 0;
            if ($artistPhotosCount > 0) {
                $artistPhotosList = $ArtistPhotosTblSource->getArtistPhotosList(false, ($show_only_text == 1 ? "" : $paged), array('artist_id' => $artist_id, 'show_artist_name' => 0, 'itemsPerPage' => $itemsPerPage, 'show_inappropriate_count' => 1), $sort, $sort_direction);
                // retrieve  artist photo's list with artist_name and inappropriate_count fields
                foreach ($artistPhotosList as $nextArtistPhotos) {
                    if ($nextArtistPhotos['inappropriateCount'] > 0) {
                        $inappropriatePhotosCount++;
                    }
                }
            }
            reset($artistPhotosList);
            $navigationHTML = $this->makePageNavigation($paged, $itemsPerPage, $paginationMaxPage, '', '', false, " backendArtistsEditorFuncs.loadArtistPhotos( " . $artist_id . ", '#page#' ) ");
            ob_start();
            require($this->m_pluginDir . 'views/admin/artist-photos-list.php');
            $content = ob_get_contents();
            ob_end_clean();
            echo json_encode(array('error_code' => 0, 'error_message' => '', 'ngg_gallery_gid'=>$artistObj['ngg_gallery_gid'],  'photosCount' => $artistPhotosCount, 'inappropriatePhotosCount' => $inappropriatePhotosCount, 'content' => $content));
            exit;
        } // if ($this->m_action == 'get-artist-photos') { // retrieve artist photos by artist_id


        if ($this->m_action == 'upload-artist-photo') { // upload artist's photo
            require_once($this->m_pluginDir . 'models/artist-photos.php');
            if (empty($_REQUEST['artist_id']) ) {
                echo json_encode(array('error_code' => 1, 'error_message' => '"artist_id" parameters must be specified!', 'content' => ''));
                return;
            }
            if ($m_parentControl->m_currentUserId <= 0) {
                echo json_encode(array('error_code' => 1, 'error_message' => ' Must be logged to system to upload artist photo!', 'content' => ''));
                return;
            }

            $orig_width = get_option('songs-artists-option_thumbnailWidth', 100); // parameters gallery images from options
            $orig_height = get_option('songs-artists-option_thumbnailHeight', 80);


            if (empty($_FILES['files']['tmp_name'][0]) or empty($_FILES['files']['name'][0])) {
                return json_encode(array('ErrorMessage' => nsnClass_appFuncs::addLog('File not uploaded', __FILE__ . ', ' . __LINE__, 'S', $this->m_wpdb), 'ErrorCode' => 1, 'dest_filename' => '', 'src_filename' => ''));
            }
            $src_filepath = $_FILES['files']['tmp_name'][0]; // path of loaded file
            $src_filename = $_FILES['files']['name'][0];  // name of loaded file
            $upload_dir = nsnClass_appFuncs::getUploadDir(); // d:\wwwroot\wp-songs\wp-content\uploads\tmp\artist_images\
            $dest_dirname = $upload_dir['basedir'] . DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR . 'artist_images' . DIRECTORY_SEPARATOR . 'artist_image_' . $_REQUEST['artist_id'] . '-' . session_id();

            $artistImagesDirs = array($upload_dir['basedir'], $upload_dir['basedir'] . DIRECTORY_SEPARATOR . 'tmp', $upload_dir['basedir'] . DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR . 'artist_images', $dest_dirname);
            nsnClass_appFuncs::createDir($artistImagesDirs);
            $dest_filename = $dest_dirname . DIRECTORY_SEPARATOR . $src_filename;
            $dest_filename_relative_url = $upload_dir['baseurl'] . '/tmp/artist_images/artist_image_' . $_REQUEST['artist_id'] . '-' . session_id() . '/' . $src_filename;
            $ret = move_uploaded_file($src_filepath, $dest_filename);

            $filesize = filesize($dest_filename);
            $resArray = array("files" => array("name" => $src_filename,
                "size" => $filesize,
                'FilenameInfo' => nsnClass_appFuncs::GetImageShowSize($dest_filename, $orig_width, $orig_height),
                "sizeLabel" => nsnClass_appFuncs::getFileSizeAsString($filesize),
                "url" => $dest_filename_relative_url . '?tm=' . time(),
            ));
            echo json_encode($resArray);
        } // if ( $this->m_action == 'upload-artist-photo' ) { // upload artist's photo



        if ($this->m_action == 'artist_add_new_song') { // add new song from artist's editor
            if (empty($_REQUEST['artist_id']) or empty($_REQUEST['title']) or empty($_REQUEST['ordering']) or empty($_REQUEST['is_active']) or empty($_REQUEST['song_jenre_selection'])) {
                echo json_encode(array('error_code' => 1, 'error_message' => nsnClass_appFuncs::addLog('"artist_id", "title", "ordering", "is_active" and "song_jenre_selection" parameters must be specified!', __FILE__ . ', ' . __LINE__, 'S', $this->m_wpdb), 'content' => ''));
                return;
            }
            $songsTblSource = new Songs($this->m_wpdb);
            $similarSongsCount = $songsTblSource->getSongsList(true, '', array('strict_title' => $_REQUEST['title'], ""));    // search similar title object
            if ($similarSongsCount > 0) {  // FOUND similar title object - raise error
                echo json_encode(array('error_code' => 1, 'error_message' => esc_html__('There is already song titled') . ' "' . $_REQUEST['title'] . '" !', 'content' => ''));
                return;
            }

            $ret_song_id = $songsTblSource->updateSong('', array('title' => $_REQUEST['title'], 'is_active' => $_REQUEST['is_active'], 'ordering' => $_REQUEST['ordering']));

            if (isset($_REQUEST['song_jenre_selection'])) {
                $jenresArr = nsnClass_appFuncs::splitStringIntoArray($_REQUEST['song_jenre_selection']);
                foreach ($jenresArr as $nextJenreKey => $nextJenreValue) {
                    if (empty($nextJenreValue)) continue;
                    $_REQUEST['cbx_song_jenre_' . $nextJenreValue] = $nextJenreValue;
                }
            }
            $songsJenresTblSource = new SongsJenres($this->m_wpdb);
            $songsJenresTblSource->updateSongJenres($ret_song_id, $_REQUEST);  // add song jenres by $ret_song_id(newly added song)

            $SongsArtistsTblSource = new SongsArtists($this->m_wpdb);
            $artist_song_id = $SongsArtistsTblSource->updateSongArtists('', array('artist_id' => $_REQUEST['artist_id'], 'song_id' => $ret_song_id)); // add new song to artist

            if (!empty($_POST['photo'])) { // upload son's photo if it was selected
                $upload_dir = nsnClass_appFuncs::getUploadDir(); // d:\wwwroot\wp-songs\wp-content\uploads\tmp\artist_new_song_image\
                $src_tmp_dirname = $upload_dir['basedir'] . DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR . 'artist_new_song_images' . DIRECTORY_SEPARATOR . 'artist_new_song_image_' . $_REQUEST['artist_id'] . '-' . session_id();
                $src_tmp_filename = $src_tmp_dirname . DIRECTORY_SEPARATOR . $_POST['photo'];
                $filenameExt = nsnClass_appFuncs::getFileNameExt($src_tmp_filename);

                $imageSongDir = $songsTblSource->getImageDir($ret_song_id);
                $songImagesDirs = array($upload_dir['basedir'], $upload_dir['basedir'] . DIRECTORY_SEPARATOR . 'song_images', $imageSongDir);
                nsnClass_appFuncs::createDir($songImagesDirs);

                $song_image_filepath = $songsTblSource->getImage($ret_song_id, $filenameExt, false);
                $ret = copy($src_tmp_filename, $song_image_filepath);
                if (file_exists($src_tmp_dirname)) {
                    nsnClass_appFuncs::DeleteDirectory($src_tmp_dirname);
                }
            }
            echo json_encode(array('error_code' => 0, 'error_message' => '', 'artist_song_id' => $artist_song_id));
        } // if ($this->m_action == 'artist_add_new_song') { // add new song from artist's editor


        if ($this->m_action == 'save-artist-photo') {  //  add artist's photo
            if (empty($_REQUEST['artist_id']) or empty($_REQUEST['description']) or empty($_REQUEST['photo_name'])) {
                echo json_encode(array('error_code' => 1, 'error_message' => nsnClass_appFuncs::addLog('"artist_id", "description" and "photo_name" parameters must be specified!', __FILE__ . ', ' . __LINE__, 'S', $this->m_wpdb), 'content' => ''));
                return;
            }

            $upload_dir = nsnClass_appFuncs::getUploadDir(); // d:\wwwroot\wp-songs\wp-content\uploads\tmp\artist_images\
            $tmp_src_dirname = $upload_dir['basedir'] . DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR . 'artist_images' . DIRECTORY_SEPARATOR . 'artist_image_' . $_REQUEST['artist_id'] . '-' . session_id();
            $dst_dirname = $upload_dir['basedir'] . DIRECTORY_SEPARATOR . 'artist_images' . DIRECTORY_SEPARATOR . 'artist_image_' . $_REQUEST['artist_id'];
            $artistImagesDirs = array($upload_dir['basedir'], $upload_dir['basedir'] . DIRECTORY_SEPARATOR . 'artist_images', $dst_dirname);
            nsnClass_appFuncs::createDir($artistImagesDirs);

            $ret = copy($tmp_src_dirname . DIRECTORY_SEPARATOR . $_REQUEST['photo_name'], $dst_dirname . DIRECTORY_SEPARATOR . $_REQUEST['photo_name']);
            require_once($this->m_pluginDir . 'models/artist-photos.php');
            $ArtistPhotosTblSource = new ArtistPhotos($this->m_wpdb);

            $artist_photo_id = $ArtistPhotosTblSource->updateArtistPhotos('', array('artist_id' => $_REQUEST['artist_id'], 'description' => $_REQUEST['description'], 'photo_name' => $_REQUEST['photo_name']));
            echo json_encode(array('error_code' => 0, 'error_message' => '', 'artist_photo_id' => $artist_photo_id));
            if ($artist_photo_id) {
                nsnClass_appFuncs::DeleteDirectory($tmp_src_dirname);
            }
            exit;
        } // if ( $this->m_action == 'save-artist-photo' ) { //  add artist's photo


        if ($_REQUEST['action'] == 'delete-artist-photo') { // delete artist_photo rows and image by artist_photo_id
            if (empty($_REQUEST['artist_photo_id'])) {
                echo json_encode(array('error_code' => 1, 'error_message' => nsnClass_appFuncs::addLog('"artist_photo_id" parameter must be specified!', __FILE__ . ', ' . __LINE__, 'S', $this->m_wpdb), 'content' => ''));
                return;
            }
            require_once($this->m_pluginDir . 'models/artist-photos.php');
            $ArtistPhotosTblSource = new ArtistPhotos($this->m_wpdb);
            $artistPhoto = $ArtistPhotosTblSource->getRowById($_REQUEST['artist_photo_id']);
            if (isset($artistPhoto['photo_name'])) {
                $upload_dir = nsnClass_appFuncs::getUploadDir();
                $dst_dirname = $upload_dir['basedir'] . DIRECTORY_SEPARATOR . 'artist_images' . DIRECTORY_SEPARATOR . 'artist_image_' . $artistPhoto['artist_id'];
                if (file_exists($dst_dirname . DIRECTORY_SEPARATOR . $artistPhoto['photo_name'])) {
                    unlink($dst_dirname . DIRECTORY_SEPARATOR . $artistPhoto['photo_name']);
                }
            }
            $artist_photo_id = $ArtistPhotosTblSource->deleteArtistPhotosRow($_REQUEST['artist_photo_id']);
            echo json_encode(array('error_code' => 0, 'error_message' => '', 'artist_photo_id' => $artist_photo_id));
            exit;
        } // if ( $_REQUEST['action'] == 'delete-artist-photo' ) {   // delete artist_photo rows and image by artist_photo_id


        if ($_REQUEST['action'] == 'clear-inappropriate-for-artist-photo') { //clear inappropriate flag for artist's photo by artist_photo_id
            if (empty($_REQUEST['artist_photo_id'])) {
                echo json_encode(array('error_code' => 1, 'error_message' => nsnClass_appFuncs::addLog('"artist_photo_id" parameter must be specified!', __FILE__ . ', ' . __LINE__, 'S', $this->m_wpdb), 'content' => ''));
                return;
            }
            require_once($this->m_pluginDir . 'models/artist-photos.php');
            require_once($this->m_pluginDir . 'models/artist-photos-inappropriate.php');
            $ArtistPhotosInappropriatesTblSource = new ArtistPhotosInappropriates($this->m_wpdb);
            $ArtistPhotosInappropriatesTblSource->deleteRowByArtistPhotoInappropriateId($_REQUEST['artist_photo_id']);
            echo json_encode(array('error_code' => 0, 'error_message' => '', 'artist_photo_id' => $_REQUEST['artist_photo_id']));
            exit;
        } // if ( $_REQUEST['action'] == 'clear-inappropriate-for-artist-photo' ) {  //clear inappropriate flag for artist's photo by artist_photo_id


        if ($_REQUEST['action'] == 'get-artist-photo-by-id') { // retrieve artist's photo by artist_photo_id
            require_once($this->m_pluginDir . 'models/artist-photos.php');
            $ArtistPhotosTblSource = new ArtistPhotos($this->m_wpdb);
            if (empty($_REQUEST['artist_photo_id'])) {
                echo json_encode(array('error_code' => 1, 'error_message' => nsnClass_appFuncs::addLog('"artist_photo_id" parameter must be specified!', __FILE__ . ', ' . __LINE__, 'S', $this->m_wpdb), 'content' => ''));
                return;
            }

            $artistReview = $ArtistPhotosTblSource->getRowById($_REQUEST['artist_photo_id']);
            $artistReview['created'] = viewFuncs::formattedDateTime($artistReview['created'], 'Common');
            echo json_encode(array('error_code' => 0, 'error_message' => '', 'artist_photo_id' => $_REQUEST['artist_photo_id'], 'artistReview' => $artistReview));
            exit;
        } // if ( $_REQUEST['action'] == 'get-artist-photo-by-id' ) { // retrieve artist's photo by artist_photo_id
        /* ARTIST'S PHOTOS END BLOCK */


        /* SONG'S ARTISTS START BLOCK */
        if ($this->m_action == 'get-song-artists') { // retrieve list of song's artists by song_id
            require_once($this->m_pluginDir . 'models/songs-artists.php');
            $song_id = $this->getParameter('song_id');
            if (empty($song_id)) {
                echo json_encode(array('error_code' => 1, 'error_message' => nsnClass_appFuncs::addLog('"song_id" parameter must be specified!', __FILE__ . ', ' . __LINE__, 'S', $this->m_wpdb), 'content' => ''));
                return;
            }

            $pageUrl = $this->getParameter('pageUrl');
            $artistsPageUrl = $this->getParameter('artistsPageUrl');
            $paged = $this->getParameter('paged');
            $sort = $this->getParameter('sort');
            $sort_direction = $this->getParameter('sort_direction');

            $itemsPerPage = get_option('songs-artists-option_backend_itemsPerPage', 20);
            $SongsArtistsTblSource = new SongsArtists($this->m_wpdb);
            $songArtistsCount = $SongsArtistsTblSource->getSongArtistsList(true, '', array('song_id' => $song_id), $sort, $sort_direction);
            //echo '<pre>$songArtistsCount::'.print_r($songArtistsCount,true).'</pre>';
            $paginationMaxPage = (int)($songArtistsCount / $itemsPerPage) + ($songArtistsCount % $itemsPerPage == 0 ? 0 : 1);
            if ($paged > $paginationMaxPage) $paged = 1;
            $songArtistsList = array();
            if ($songArtistsCount > 0) {
                $songArtistsList = $SongsArtistsTblSource->getSongArtistsList(false, $paged, array('song_id' => $song_id, 'show_artist_name' => 1, 'itemsPerPage' => $itemsPerPage), $sort, $sort_direction);
            }
            $navigationHTML = $this->makePageNavigation($paged, $itemsPerPage, $paginationMaxPage, '', '', false, " backendSongsEditorFuncs.loadSongArtistsList( " . $song_id . ", '#page#' ) ");
            ob_start();
            require($this->m_pluginDir . 'views/admin/song-artists-list.php');
            $content = ob_get_contents();
            ob_end_clean();
            echo json_encode(array('error_code' => 0, 'error_message' => '', 'content' => $content, 'artistsCount' => $songArtistsCount));
            exit;
        } // if ( $this->m_action == 'get-song-artists' ) { // retrieve list of song's artists by song_id
        /* SONG'S ARTISTS END BLOCK */


        /* ARTIST'S SONGS START BLOCK */
        if ($this->m_action == 'get-artist-songs') {  // retrieve list of artist's songs by artist_id
            require_once($this->m_pluginDir . 'models/songs-artists.php');
            $artist_id = $this->getParameter('artist_id');
            if (empty($artist_id)) {
                echo json_encode(array('error_code' => 1, 'error_message' => nsnClass_appFuncs::addLog('"artist_id" parameter must be specified!', __FILE__ . ', ' . __LINE__, 'S', $this->m_wpdb), 'content' => ''));
                return;
            }

            $m_songsPageUrl = $this->getParameter('m_songsPageUrl', '');
            $paged = $this->getParameter('paged', 1);
            $show_only_text = $this->getParameter('show_only_text', 0);
            $sort = $this->getParameter('sort');
            $sort_direction = $this->getParameter('sort_direction');

            $itemsPerPage = get_option('songs-artists-option_backend_itemsPerPage', 20);
            $SongsArtistsTblSource = new SongsArtists($this->m_wpdb);
            $artistsSongsCount = $SongsArtistsTblSource->getSongArtistsList(true, '', array('artist_id' => $artist_id), $sort, $sort_direction);
            $paginationMaxPage = (int)($artistsSongsCount / $itemsPerPage) + ($artistsSongsCount % $itemsPerPage == 0 ? 0 : 1);
            if ($paged > $paginationMaxPage) $paged = 1;
            $artistsSongsList = array();
            if ($artistsSongsCount > 0) {
                $artistsSongsList = $SongsArtistsTblSource->getSongArtistsList(false, ($show_only_text == 1 ? "" : $paged), array('artist_id' => $artist_id, 'show_song_title' => 1, 'itemsPerPage' => $itemsPerPage), $sort, $sort_direction);
            }
            $navigationHTML = $this->makePageNavigation($paged, $itemsPerPage, $paginationMaxPage, '', '', false, " backendArtistsEditorFuncs.loadSongsArtists( " . $artist_id . ", '#page#' ) ");
            ob_start();
            require($this->m_pluginDir . 'views/admin/artist-songs-list.php');
            $content = ob_get_contents();
            ob_end_clean();
            echo json_encode(array('error_code' => 0, 'error_message' => '', 'content' => $content, 'songsCount' => $artistsSongsCount));
            exit;
        } // if ( $this->m_action == 'get-artist-songs' ) { // retrieve list of artist's songs by artist_id

        if ($this->m_action == 'update-artist-song') {
            if (empty($_REQUEST['artist_id']) or empty($_REQUEST['song_id'])) {
                echo json_encode(array('error_code' => 1, 'error_message' => nsnClass_appFuncs::addLog('"artist_id" and "song_id" parameters must be specified!', __FILE__ . ', ' . __LINE__, 'S', $this->m_wpdb), 'content' => ''));
                return;
            }
            require_once($this->m_pluginDir . 'models/songs-artists.php');
            $SongsArtistsTblSource = new SongsArtists($this->m_wpdb);

            $artist_song_id = $SongsArtistsTblSource->updateSongArtists(!empty($_REQUEST['artist_song_id']) ? $_REQUEST['artist_song_id'] : '', array('artist_id' => $_REQUEST['artist_id'], 'song_id' => $_REQUEST['song_id']));
            echo json_encode(array('error_code' => 0, 'error_message' => '', 'artist_song_id' => $artist_song_id));
            exit;
        } // if ( $this->m_action == 'update-artist-song' ) {


        if ($_REQUEST['action'] == 'delete-artist-song') { // delete artist's song by artist_song_id
            if (empty($_REQUEST['artist_song_id'])) {
                echo json_encode(array('error_code' => 1, 'error_message' => nsnClass_appFuncs::addLog('"artist_song_id" parameter must be specified!', __FILE__ . ', ' . __LINE__, 'S', $this->m_wpdb), 'content' => ''));
                return;
            }
            require_once($this->m_pluginDir . 'models/songs-artists.php');
            $SongsArtistsTblSource = new SongsArtists($this->m_wpdb);

            $artist_song_id = $SongsArtistsTblSource->deleteSongsArtistsRow($_REQUEST['artist_song_id']);
            echo json_encode(array('error_code' => 0, 'error_message' => '', 'artist_song_id' => $artist_song_id));
            exit;
        } // if ( $_REQUEST['action'] == 'delete-artist-song' ) { // delete artist's song by artist_song_id


        if ($_REQUEST['action'] == 'get-artist-song-by-id') { // retrieve artist's songby artist_song_id
            require_once($this->m_pluginDir . 'models/songs-artists.php');
            $SongsArtistsTblSource = new SongsArtists($this->m_wpdb);
            if (empty($_REQUEST['artist_song_id'])) {
                echo json_encode(array('error_code' => 1, 'error_message' => nsnClass_appFuncs::addLog('"artist_song_id" parameter must be specified!', __FILE__ . ', ' . __LINE__, 'S', $this->m_wpdb), 'content' => ''));
                return;
            }

            $artistSong = $SongsArtistsTblSource->getRowById($_REQUEST['artist_song_id']);
            $artistSong['created'] = viewFuncs::formattedDateTime($artistSong['created'], 'Common');
            echo json_encode(array('error_code' => 0, 'error_message' => '', 'artist_song_id' => $_REQUEST['artist_song_id'], 'artistSong' => $artistSong));
            exit;
        } // if ( $_REQUEST['action'] == 'get-artist-song-by-id' ) { // retrieve artist's songby artist_song_id

        /* ARTIST'S SONGS END BLOCK */


    } // public function main() {


    protected function showRowsListDataForm($parentControl)
    {

    }

    protected function addRowDataForm($parentControl)
    {
    }

    protected function initFieldsAddRow($parentControl)
    {
    }

    protected function editRowDataForm($parentControl)
    {

    }

    protected function doBulkAction($action)
    {
    } // protected function doBulkAction() {

    protected function saveRowData($action, $editorItem)
    {
    }

    protected function deleteRowData()
    {
    }

    protected function preparePageParameters($WithPage, $WithSort) // Prepare url for pagination with all fields in filter and sort and sort_direction
    {
        return '';
    } // protected function preparePageParameters($WithPage, $WithSort) // Prepare url for pagination with all fields in filter and sort and sort_direction

}

global $wpdb;
$ajaxBackendControlObj = new ajaxBackendControl(null, $wpdb); // ajax backend control class extends parent common control class
$ajaxBackendControlObj->main();  // entrance point


function cmpTourConcerts($a, $b) { // sorted list of Tour Concerts by event datetime
    if ($a['event_date'] == $b['event_date']) {
        return 0;
    }
    return ($a['event_date'] < $b['event_date']) ? -1 : 1;
}