<?php
if ( ! defined( 'ArtistsSongs' ) ) {
    exit; // Exit if accessed directly
}
require_once( 'appArtistsSongsControl.php');   // file with parent common control class
class songsControl extends  appArtistsSongsControl
{
    private $m_songEditorItem; // Array of edited current Song
    private $m_songsJenresTblSource;  // songs_jenres table source
    private $m_songsTblSource;   // songs table source
    private $m_song_id;          // songs id in add(null)/edit mode
    private $m_is_reopen;        // if true saving row the editor with this row is reopened
    private $m_songJenresList;
    private $m_filter_title;     // filters values
    private $m_filter_is_active;
    private $m_filter_cbx_song_jenre_;
    private $m_artistsPageUrl;
    private $m_songsOrderedList;


    public function __construct($containerClass, $wpdb)
    {
        global $session;
        $this->m_labelNonce= 'songs-editor-nonce';
        $this->m_labelNonceInput= 'songs-editor-nonce-input';
        $this->m_pageUrl= '/wp-admin/admin.php?page=ArtistsSongs-songs-editor&';    // url to for songs editor
        $this->m_artistsPageUrl= '/wp-admin/admin.php?page=ArtistsSongs-artists-editor&';  // link to artists editor in system
        parent::__construct($containerClass, $wpdb, $session);
        $this->setValidationRules( array('required'=>'song_title,  song_is_active, song_ordering  ', 'name'=> '', 'integer'=> 'song_ordering ') ); // must be called AFTER __construct

        require_once( $this->m_pluginDir . 'models/songs.php'); // Models used in the control
        require_once( $this->m_pluginDir . 'models/songs-artists.php');
        require_once( $this->m_pluginDir . 'models/songs-jenres.php');
        include_once( $this->m_pluginDir . 'models/posts.php');
        $this->m_is_insert= false;
    }

    protected function readRequestParameters() {  // before main - read all possible parameters from request
        $this->m_filter_title = $this->getParameter( 'filter_title', '' );
        $this->m_filter_is_active = $this->getParameter( 'filter_is_active', '' );
        $this->m_filter_cbx_song_jenre_ = $this->getParameter( 'filter_cbx_song_jenre_', '' );

        $this->m_paged = $this->getParameter( 'paged', 1 );

        $this->m_sort = $this->getParameter('sort');
        $this->m_sort_direction = $this->getParameter( 'sort_direction');
        $this->m_pageParametersWithSort = $this->preparePageParameters(true, true);
        $this->m_pageParametersWithoutSort = $this->preparePageParameters(false, false);
        $this->m_song_id = $this->getParameter( 'song_id' );
        $this->m_is_reopen = $this->getParameter( 'is_reopen' );
        $this->m_is_insert= empty($this->m_GETDataArray['song_id']) ? 1 : 0;
        $this->m_action = $this->getParameter( 'action', 'list' );    //
        $this->m_flashMessage = $this->getFlashMessage();   // TODO       PhpStorm-139.1348

    } // protected function readRequestParameters() { // before main - read all possible parameters from request

    public function main() { // entrance point
        parent::main();
        if ( !in_array($this->m_action,array('list','edit', 'add', 'delete', 'update')) ) $this->m_action= 'list';  // List is default

        $this->m_songsTblSource = new Songs($this->m_wpdb);    // Init Tables Sources
        $this->m_songsArtistsTblSource = new SongsArtists($this->m_wpdb);
        $this->m_songsJenresTblSource = new SongsJenres($this->m_wpdb);
        $this->m_songJenresList= $this->m_songsJenresTblSource->getEmptySongJenresList();
        $this->m_songEditorItem= array('ID'=>'','title'=>'','is_active'=>'','ordering'=>'','created'=>'');

        $validatedFields= array('ID'=>'song_id', 'title'=>'song_title', 'is_active'=>'song_is_active', 'ordering'=>'song_ordering' );
        $languagesList=  nsnClass_appLangs::getLanguagesList('',true);
        if ( !empty($languagesList) ) { // if site is multilingual then title field for all languages is required
            foreach ($languagesList as $nextLangSlug => $nextLang) {
                if ($nextLangSlug != nsnClass_appLangs::getDefaultLanguage()) {
                    $validatedFields['song_title_' . $nextLangSlug] = 'song_title_' . $nextLangSlug;
                }
            }
        }
        $postReturnData= $this->doPOSTRequest( $this->m_action, $this->m_songEditorItem, $validatedFields );  // POST REQUEST WORKOUT
        $this->m_songEditorItem= isset($postReturnData['editorItem']) ? $postReturnData['editorItem'] : $this->m_songEditorItem;
        $this->m_action= isset($postReturnData['action']) ? $postReturnData['action'] : $this->m_action; // set which action must be from from request parameter

        $this->m_songsIsActiveValueArray= nsnClass_appFuncs::SetArrayHeader(array(array('key' => '', 'value' => esc_html__(' -Select active- ')) ), $this->m_songsTblSource->getSongsIsActiveValueArray() ); // get pairs key=>value for is_active selection input

        ob_start();
        $this->doEditorFlow($this);   // by $this->m_action define which control must be run
        $output = ob_get_contents ();
        ob_end_clean ();
        echo $output;
    } // public function main() { // entrance point

    protected function saveRowData( $action, $editorItem ) { // Save inster/update 1 song in Song's editor
        $hasError= false;
        $this->m_songsTblSource->tranBegin();
        if ( !$this->m_is_insert ) {
            $origSong = $this->m_songsTblSource->getRowById($this->m_POSTDataArray['song_id']); // retrieve original song in "edit" mode
            if ( isset($origSong) ) {
                $origOrdering = $origSong['ordering']; // set ordering for current song and for the rest of songs to keep ordering field for all table
                $newOrdering = $this->m_POSTDataArray['song_ordering'];
                if ( $origOrdering < $newOrdering  ) { // new Ordering is higher then original 3->7
                    for($step=$origOrdering+1; $step<= $newOrdering; $step++) {
                        $updateOrderingSqlCommand= "UPDATE `".$this->m_songsTblSource->tbl_songs."` SET  `ordering`= '".($step-1)."' WHERE `ordering` =
                        '" . $step."' ";
                        $queryRet= $this->m_wpdb->query($updateOrderingSqlCommand);
                        if ($queryRet===FALSE) {
                            $hasError = true;
                            break;
                        }
                    }
                } // if ( $origOrdering < $newOrdering  ) { // new Ordering is higher then original 3->7

                if ( $origOrdering > $newOrdering  ) { // new Ordering is higher then original 4->2
                    for($step=$origOrdering-1; $step>= $newOrdering; $step--) {
                        $updateOrderingSqlCommand= "UPDATE `".$this->m_songsTblSource->tbl_songs."` SET  `ordering`= '".($step+1)."' WHERE `ordering` =
                        '" . $step."' ";
                        $queryRet= $this->m_wpdb->query($updateOrderingSqlCommand);
                        if ($queryRet===FALSE) {
                            $hasError = true;
                            break;
                        }
                    }
                } // if ( $origOrdering < $newOrdering  ) { // new Ordering is higher then original 3->7

            }
        }

        if (!$hasError) { // run next sql-statement if in one of prior queries there were no errors
            $updateDate= array ( 'title' => $this->m_POSTDataArray['song_title'], 'is_active' => $this->m_POSTDataArray['song_is_active'], 'ordering' => $this->m_POSTDataArray['song_ordering'] );
            $updateDate= $this->setLangUpdate( $updateDate, $this->m_POSTDataArray, 'song_title' );

            $ret_song_id = $this->m_songsTblSource->updateSong((!empty($this->m_POSTDataArray['song_id']) ? $this->m_POSTDataArray['song_id'] : ""), $updateDate );
            if ($ret_song_id === FALSE) $hasError = true;
        }

        if (!$hasError) { // run next sql-statement if in one of prior queries there were no errors
            $ret_song_jenre_id = $this->m_songsJenresTblSource->updateSongJenres($ret_song_id, $this->m_POSTDataArray);
            if ($ret_song_jenre_id === FALSE) $hasError = true;
        }
        if ( !$hasError ) {  // There was NO error so commit all changes
        } else { // There WAS error make rollback
            $this->m_songsTblSource->tranRollback();

        }

        $redirectUrl = $this->m_pageUrl . 'action=list' . $this->m_pageParametersWithSort;
        $this->setFlashMessage( esc_html__("Song")." " . ($this->m_is_insert ?esc_html__("added") :esc_html__("updated") ) );

        nsnClass_appFuncs::makeFileUpload($_FILES, 'song_image', $ret_song_id, $this->m_songsTblSource, false);  // attach uploaded image
        if ($this->m_is_reopen) {   // To Reopen editor after updating
            $redirectUrl = $this->m_pageUrl . 'action=edit&song_id=' . $ret_song_id . $this->m_pageParametersWithSort;
            $this->setFlashMessage( esc_html__("Song")." " . ($this->m_is_insert ?esc_html__("added") :esc_html__("updated") ) );
        }
        wp_safe_redirect($redirectUrl);

    } // protected function saveRowData( $action, $editorItem ) {

    protected function showRowsListDataForm( $parentControl ) // show rows of songs from result source
    {
        $m_parentControl= $parentControl;
        $m_parentControl->m_itemsPerPage= getAppVar('backend_itemsPerPage');
        $this->m_songJenresList = $this->fillSongJenresList($this->m_songJenresList);


        $m_parentControl->m_songsCount = $this->m_songsTblSource->getSongsList(true, '', array( 'title' => $this->m_filter_title, 'is_active' => $this->m_filter_is_active, 'cbx_song_jenre_' => $this->m_filter_cbx_song_jenre_), $this->m_sort, $this->m_sort_direction);
        $m_parentControl->m_songsList= array();
        $m_parentControl->m_paginationMaxPage= (int)($m_parentControl->m_songsCount/$m_parentControl->m_itemsPerPage) + ($m_parentControl->m_songsCount % $m_parentControl->m_itemsPerPage==0?0:1);
        if (empty($this->m_paged) or $this->m_paged > $m_parentControl->m_paginationMaxPage ) $this->m_paged= 1;

        if( $m_parentControl->m_songsCount > 0 ) {
            $m_parentControl->m_songsList = $this->m_songsTblSource->getSongsList(false, $this->m_paged, array( 'title' => $this->m_filter_title, 'is_active' => $this->m_filter_is_active, 'cbx_song_jenre_' => $this->m_filter_cbx_song_jenre_, 'itemsPerPage'=> $m_parentControl->m_itemsPerPage), $this->m_sort, $this->m_sort_direction);
        }

        $m_parentControl->m_navigationHTML= $this->makePageNavigation($this->m_paged, $m_parentControl->m_itemsPerPage, $m_parentControl->m_paginationMaxPage, '', '', false);
        require_once($this->m_pluginDir . 'views/admin/header.php');
        require_once($this->m_pluginDir . 'views/admin/songs-list.php'); // show list of songs view with header/footer
        require_once($this->m_pluginDir . 'views/admin/footer.php');
    } // protected function showRowsListDataForm( $parentControl )   {

    protected function addRowDataForm( $parentControl )   { // show editor of song view in "insert"
        $m_parentControl= $parentControl;
        $this->setSongsOrderedList('');  // prepare list with ordering of all songs in system
        $languagesList=  nsnClass_appLangs::getLanguagesList('',true);
        if ( !empty($languagesList) ) { // if site is multilingual then fill title for all languages with null value
            foreach ($languagesList as $nextLangSlug => $nextLang) {
                if ($nextLangSlug != nsnClass_appLangs::getDefaultLanguage()) {
                    if (!$this->m_hasValidationErrors) {
                        $this->m_songEditorItem['song_title_' . $nextLangSlug] = '';
                    }
                }
            }
        }
        require_once($this->m_pluginDir . 'views/admin/header.php');
        require_once($this->m_pluginDir . 'views/admin/songs-editor.php'); // show editor of song view in "insert" mode with header/footer
        require_once($this->m_pluginDir . 'views/admin/footer.php');
    } //protected function addRowDataForm( $parentControl )   { // show editor of song view in "insert"

    protected function initFieldsAddRow( $parentControl ) // set init values for some fields
    {
        $maxOrdering= $this->m_songsTblSource->getMaxOrdering();
        $this->m_songEditorItem['ordering']= $maxOrdering + 1;
        $this->m_songEditorItem['is_active']= 'A';
    }

    protected function editRowDataForm( $parentControl ) {  // show editor of song view in "edit" mode
        $m_parentControl= $parentControl;

        if( !$this->m_hasValidationErrors or (is_array($this->m_hasValidationErrors) and count($this->m_hasValidationErrors) == 0)  ) { // that is not error on submitting - get data from db
            $this->m_songEditorItem = $this->m_songsTblSource->getRowById($this->m_song_id);
            $languagesList=  nsnClass_appLangs::getLanguagesList('',true);
            if ( !empty($languagesList) ) {
                foreach ($languagesList as $nextLangSlug => $nextLang) {
                    if ($nextLangSlug != nsnClass_appLangs::getDefaultLanguage()) {
                        if (!$this->m_hasValidationErrors) {
                            $this->m_songEditorItem['song_title_' . $nextLangSlug] = $this->m_songsTblSource->getSongByLang($this->m_song_id, $nextLangSlug, 'title');
                        }
                    }
                }
            }
            $this->m_songJenresList= $this->m_songsJenresTblSource->getSongJenresList(false, '', $filters = array('song_id'=>$this->m_song_id), '', '', true);
        }
        if (empty($this->m_songEditorItem)) {
            $this->setFlashMessage( esc_html__("Song not found." ) );
            wp_safe_redirect($this->m_pageUrl . 'action=list' . $this->m_pageParametersWithSort);
        }
        $this->setSongsOrderedList($this->m_songEditorItem['ordering']); // prepare list with ordering of all songs in system

        $postsTblSource = new Posts($this->m_wpdb);
        $this->m_relatedSongPost= $postsTblSource->getRelatedPostByMetaValue( $this->m_song_id, 'song' , $this->m_currentBackendLang, false );

        require_once($this->m_pluginDir . 'views/admin/header.php');
        require_once($this->m_pluginDir . 'views/admin/songs-editor.php');
        require_once($this->m_pluginDir . 'views/admin/footer.php');
    } // protected function editRowDataForm( $parentControl ) {  // show editor of song view in "edit"

    protected function setSongsOrderedList($currentOrdering) // prepare list with ordering of all songs in system
    {
        $this->m_songsOrderedList = array();
        $songsList = $this->m_songsTblSource->getSongsList(false, '', array('lang'=> $this->m_defaultLanguage ), 'ordering');
        $maxOrdering = 0;
        foreach ($songsList as $nextKey => $nextValue) {
            $this->m_songsOrderedList[] = array('key' => $nextValue['ordering'], 'value' => ($nextValue['ordering'] == $currentOrdering ? "" : esc_html__('Before') ) . ' ' . $nextValue['ordering'] . '->' . $nextValue['title']);
            if ($maxOrdering < $nextValue['ordering']) {
                $maxOrdering = $nextValue['ordering'];
            }
        }
        $this->m_songsOrderedList[] = array('key' => ($maxOrdering + 1), 'value' => esc_html__('Set as last item'));
    } // protected function setSongsOrderedList($currentOrdering) // prepare list with ordering of all songs in system

    protected function deleteRowData() // delete song with directory of song's image
    {
        $retStr = $this->m_songsTblSource->deleteSongRow($this->m_song_id);
        if (isset($retStr)) {
            $this->setFlashMessage( esc_html__( "Internal error" . ' : ' . $retStr ) );
            wp_safe_redirect( $this->m_pageUrl . 'action=list'.$this->m_pageParametersWithSort );
            return;
        }

        $imageDir = $this->m_songsTblSource->getImageDir($this->m_song_id);
        nsnClass_appFuncs::deleteDirectory($imageDir);
        $this->setFlashMessage( esc_html__("Song was deleted" ) );
        wp_safe_redirect( $this->m_pageUrl . 'action=list'.$this->m_pageParametersWithSort );
    } // protected function deleteRowData()  // delete song with directory of song's image

    protected function fillSongJenresList($songJenresList) // prepare song jenres list with checked items
    {
        $isPost= false;
        if ( !empty($this->m_POSTDataArray) ) {
            $postArray= $this->m_POSTDataArray;
            $isPost= true;
        } else {
            $postArray= $this->m_GETDataArray;
        }
        foreach( $songJenresList as $nextKey=>$nextSongJenreValue ) {
            reset($postArray);
            foreach ( $postArray as $nextPostKey=> $nextPostValue) {
                $A= preg_split('/filter_cbx_song_jenre_/', $nextPostKey);
                if (count($A)==2) {
                    if ( $isPost ) {
                        if ($nextSongJenreValue['music_jenre_id'] == $A[1]) {
                            $songJenresList[$nextKey]['music_jenre_is_checked'] = 1;
                        }
                    } else {
                        $A_3= nsnClass_appFuncs::splitStringIntoArray($nextPostValue);
                        if ( in_array($nextSongJenreValue['music_jenre_id'], $A_3) ) {
                            $songJenresList[$nextKey]['music_jenre_is_checked'] = 1;
                        }
                    }
                }
            }
        }
        return $songJenresList;
    } // protected function fillSongJenresList($songJenresList)  // prepare song jenres list with checked items

    protected function doBulkAction( $action) { // run selected bulk action
        $songsIdToDelete= array();
        $songsDeletionQueries= array();
        if ($action != 'bulk_delete') {
            $this->m_songsTblSource->tranBegin();
            $hasError= false;
            ob_start();

            foreach( $this->m_POSTDataArray as $nextKey=>$nextValue ) {
                $A= nsnClass_appFuncs::splitStringIntoArray($nextKey, 'cb-select-');
                if (count($A) == 2 ) {
                    if ($action == 'bulk_active') {  // Set list of songs as Active
                        $ret= $this->m_songsTblSource->updateSong( $A[1], array( 'is_active' => 'A' ) );
                        if( $ret === FALSE ) { // Return values: This function returns the number of rows updated, or false if there is an error.
                            $hasError = true;
                            break;
                        }
                    }
                    if ($action == 'bulk_inactive') { // Set list of songs as Inactive
                        $ret= $this->m_songsTblSource->updateSong( $A[1], array( 'is_active' => 'I' ) );
                        if( $ret === FALSE ) {
                            $hasError = true;
                            break;
                        }
                    }
                }
            }
            if ( !$hasError ) {  // There was NO error so commit all changes
                $this->m_songsTblSource->tranCommit();
                ob_end_clean ();
            } else { // There WAS error make rollback
                $output = ob_get_contents ();
                ob_end_clean ();
                $this->m_songsTblSource->tranRollback();
                nsnClass_appFuncs::addLog(strip_tags($output),  __FILE__.', '.__LINE__, 'S', $this->m_wpdb);
                $this->setFlashMessage(esc_html__("Internal error" . ' : ' . strip_tags($output)));
                wp_safe_redirect($this->m_pageUrl . 'action=list' . $this->m_pageParametersWithSort);
                return; // all queries run with error - return $output asa error code!
            }
        } // if ($action != 'bulk_delete') {

        if ($action == 'bulk_delete') { // delete list of songs with directory of song's image
            foreach( $this->m_POSTDataArray as $nextKey=>$nextValue ) {
                $A= nsnClass_appFuncs::splitStringIntoArray($nextKey, 'cb-select-');
                if (count($A) == 2 ) {
                    $songsIdToDelete[]= $A[1]; // list of all IDs of songs for deletion
                    $A = $this->m_songsTblSource->deleteSongRow($A[1], /* $onlyReturnQueries */ true); // return sql for deletion
                    if (isset($A) and is_array($A)) {
                        foreach( $A as $nextKey=>$nextValue ) {
                            $songsDeletionQueries[] = $nextValue; // list of all sql statements fort deletion of songs and all related data
                        }
                    }
                }
            }
            $runQueriesRes = htmlspecialchars(strip_tags($this->m_songsTblSource->runQueriesUnderTransaction($songsDeletionQueries)), ENT_QUOTES);
            if (isset($runQueriesRes)) { // there was error during deletion
                nsnClass_appFuncs::addLog($runQueriesRes, __FILE__ . ', ' . __LINE__, 'S', $this->m_wpdb);
                $this->setFlashMessage(esc_html__("Internal error" . ' : ' . $runQueriesRes));
                wp_safe_redirect($this->m_pageUrl . 'action=list' . $this->m_pageParametersWithSort);
                return;
            }

            foreach( $songsIdToDelete as $nextsongsId ) {
                $imageDir = $this->m_songsTblSource->getImageDir($nextsongsId);
                nsnClass_appFuncs::deleteDirectory($imageDir);
            }
        } // if ($action == 'bulk_delete') { // delete list of songs with directory of song's image
        if ( !empty($action) ) {
            $this->setFlashMessage(esc_html__("Bulk operation " . (isset($this->m_POSTDataArray['bulk_operation_title']) ? $this->m_POSTDataArray['bulk_operation_title'] : "") . ' done.'));
        }
        wp_safe_redirect(  $this->m_pageUrl . 'action=list' . $this->m_pageParametersWithSort );
    } // protected function doBulkAction() {  // run selected bulk action

    protected function doValidation($data)  // Validate fields in validation list $this->m_validationRules and 2 more validations
    {
        $this->m_hasValidationErrors= parent::doValidation($data);
        if (empty($this->m_hasValidationErrors['song_title'])) { // Is there is no error on title - check is it unique
            $similarSongsCount = $this->m_songsTblSource->getSongsList(true, '', array('lang'=> $this->m_defaultLanguage, 'strict_title' => $this->m_POSTDataArray['song_title'], 'id!='=> ( $this->m_is_insert ? "" : $this->m_POSTDataArray['song_id'] ) ));    // search similar title object
            if ( $similarSongsCount> 0 ) {  // FOUND similar title object - raise error
                $this->m_hasValidationErrors['song_title'] = array('field_name' => 'song_title', 'error_label' => esc_html__('There is already song titled').' "' . $this->m_POSTDataArray['song_title'] . '" !');
            }
        } // if (empty($resValidated['song_title'])) { // Is there is no error on title - check is it unique

        if ( !$this->hasItemChecked($this->m_POSTDataArray, 'cbx_song_jenre_') ) { // Is there is no error on title - check is it unique
            $this->m_hasValidationErrors['cbx_song_jenre_'] = array('field_name' => 'cbx_song_jenre_', 'error_label' => esc_html__(' At least 1 jenre must be checked !'));
        }
        return $this->m_hasValidationErrors; // return list of errors
    } // protected function doValidation($data) {  // Validate fields in validation list $this->m_validationRules and 2 more validations

    protected function setNotValidatedData($editorItem, $fieldsArray) {  // in form with validation errors check jenres fields
        $editorItem= parent::setNotValidatedData($editorItem, $fieldsArray);
        $songJenresList= $this->m_songsJenresTblSource->getEmptySongJenresList();
        $this->m_songJenresList= $this->setItemsChecked( $songJenresList, $this->m_POSTDataArray, 'cbx_song_jenre_', 'music_jenre_id', 'music_jenre_is_checked'); // TODO
        return $editorItem;
    } // protected function setNotValidatedData($editorItem, $fieldsArray) { // in form with validation errors check jenres fields

    protected function preparePageParameters($WithPage, $WithSort) // Prepare url for pagination with all fields in filter and sort and sort_direction
    {
        $ResStr = ''; // prepare string with all filters parameters
        if (!empty( $_POST ) ) { // form was submitted
            if ($WithPage) {
                $paged = $this->m_POSTDataArray['paged'];
                $ResStr .= isset($paged) ? 'paged=' . $paged. '&' : 'paged=1&';
            }
            $filter_title = $this->m_POSTDataArray['filter_title'];
            $ResStr .= isset($filter_title) ? 'filter_title=' . $filter_title . '&' : '';
            $filter_is_active = $this->m_POSTDataArray['filter_is_active'];
            $ResStr .= isset($filter_is_active) ? 'filter_is_active=' . $filter_is_active . '&' : '';
            $filter_cbx_song_jenre_ = $this->m_POSTDataArray['filter_cbx_song_jenre_'];
            $ResStr .= isset($filter_cbx_song_jenre_) ? 'filter_cbx_song_jenre_=' . $filter_cbx_song_jenre_ . '&' : '';
            if ($WithSort) {
                $sort = $this->m_POSTDataArray['sort'];
                $ResStr .= isset($sort) ? 'sort=' . $sort . '&' : '';
                $sort_direction = $this->m_POSTDataArray['sort_direction'];
                $ResStr .= isset($sort_direction) ? 'sort_direction=' . $sort_direction . '&' : '';
            }
        } else {
            if ($WithPage) {
                $ResStr .= isset($this->m_GETDataArray['paged']) ? 'paged=' . $this->m_GETDataArray['paged'] . '&' : 'paged=1&';
            }
            $ResStr .= isset($this->m_GETDataArray['filter_title']) ? 'filter_title=' . $this->m_GETDataArray['filter_title'] . '&' : '';
            $ResStr .= isset($this->m_GETDataArray['filter_is_active']) ? 'filter_is_active=' . $this->m_GETDataArray['filter_is_active'] . '&' : '';
            $ResStr .= isset($this->m_GETDataArray['filter_cbx_song_jenre_']) ? 'filter_cbx_song_jenre_=' . $this->m_GETDataArray['filter_cbx_song_jenre_'] . '&' : '';
            if ($WithSort) {
                $ResStr .= isset($this->m_GETDataArray['sort']) ? 'sort=' . $this->m_GETDataArray['sort'] . '&' : '';
                $ResStr .= isset($this->m_GETDataArray['sort_direction']) ? 'sort_direction=' . $this->m_GETDataArray['sort_direction'] . '&' : '';
            }
        }
        if (substr($ResStr, strlen($ResStr) - 1, 1) == '&') {
            $ResStr = substr($ResStr, 0, strlen($ResStr) - 1);
        }
        return '&' . $ResStr;
    } // protected function preparePageParameters($WithPage, $WithSort) // Prepare url for pagination with all fields in filter and sort and sort_direction

}

