<?php  // artist_reviews
if ( ! defined( 'ArtistsSongs' ) ) {
    exit; // Exit if accessed directly
}
require_once( 'artists-songs-db.php');
class ArtistReviews extends ArtistsSongsDB
{

    public function getArtistReviewsList($outputFormatCount = false, $paged = '', $filters = array(), $sort = '', $sort_direction = '')
    {
        DebToFile('-getArtistReviewsList  START::' );
        $hasWhereCond = false;
        $hasUserDisplayNameJoined= !empty($filters['show_user_display_name']);
        $hasArtistDisplayNameJoined= !empty($filters['show_artist_display_name']);

        $showInappropriateCount= !empty($filters['show_inappropriate_count']);
        $showInappropriateCountQuery= '';
        if ( $showInappropriateCount ) {
            $showInappropriateCountQuery= ', ( select count(*) from `' . $this->tbl_artist_reviews_inappropriate . '` where `artist_review_id`= `' . $this->tbl_artist_reviews . '`.`ID` ) as inappropriateCount';
        }

        $with_artist_is_active = !empty($filters['artist_is_active']);
        $hasArtistsJoinedSubquery = '';
        $isArtistJoined= false;
        if ( $with_artist_is_active  ) {
            $hasArtistsJoinedSubquery= " JOIN `" . $this->tbl_artists."` ON `" . $this->tbl_artists."`.`ID` = `" . $this->tbl_artist_reviews . "` . `artist_id` ";
            $isArtistJoined= true;
        }
        //DebToFile('+++getArtistReviewsList  $hasArtistsJoinedSubquery::' . print_r($hasArtistsJoinedSubquery, true));

        if ($outputFormatCount) {
            $sqlStatement = "SELECT count(distinct " . $this->tbl_artist_reviews . ".ID) AS rowsCount FROM `" . $this->tbl_artist_reviews . "` " . $hasArtistsJoinedSubquery;
        } else {
            $sqlStatement = "SELECT distinct `" . $this->tbl_artist_reviews . "`.* ".
                ($hasUserDisplayNameJoined ? ", `" . $this->m_wpdb->prefix . "users`.display_name" : "") .
                ($hasArtistDisplayNameJoined ? ", `" . $this->tbl_artists . "`.`name` as artist_name" : "") .
                $showInappropriateCountQuery.
                " FROM `" . $this->tbl_artist_reviews . "`" .
                ( $hasUserDisplayNameJoined ? " JOIN `" . $this->m_wpdb->prefix."users` ON `" . $this->m_wpdb->prefix."users`.`ID` = `" . $this->tbl_artist_reviews . "`.`user_id`" : "" ) .
                ( ( $hasArtistDisplayNameJoined and !$isArtistJoined ) ? " JOIN `" . $this->tbl_artists."` ON `" . $this->tbl_artists."`.`ID` = `" . $this->tbl_artist_reviews . "`.`artist_id`" : ""
        ) .
                $hasArtistsJoinedSubquery;
        }
        if (!empty($filters['artist_id'])) {
            $sqlStatement .= ($hasWhereCond ? "" : " where ") . ($hasWhereCond ? " AND " : "") . " `" . $this->tbl_artist_reviews . "`.artist_id = '" . $filters['artist_id'] . "' ";
            $hasWhereCond = true;
        }
        if (!empty($filters['user_id'])) {
            $sqlStatement .= ($hasWhereCond ? "" : " where ") . ($hasWhereCond ? " AND " : "") . " `" . $this->tbl_artist_reviews . "`.user_id = '" . $filters['user_id'] . "' ";
            $hasWhereCond = true;
        }

        if ( !empty($filters['review_text']) ) {
            $sqlStatement.= ( $hasWhereCond? "":" where " ) . ( $hasWhereCond? " AND ":"" ) . " `" . $this->tbl_artist_reviews . "`.review_text like '%".$filters['review_text']."%' ";
            $hasWhereCond= true;
        }

        if (!empty($filters['id!='])) {
            $sqlStatement .= ($hasWhereCond ? "" : " where ") . ($hasWhereCond ? " AND " : "") . " `" . $this->tbl_artist_reviews . "`.ID != '" . $filters['id!='] . "' ";
            $hasWheJenrereCond = true;
        }
        if ( $with_artist_is_active  ) {
            $sqlStatement .= ($hasWhereCond ? "" : " where ") . ($hasWhereCond ? " AND " : "") . " `" . $this->tbl_artists . "`.is_active= 'A' ";
            $hasWheJenrereCond = true;
        }

        //echo '<pre>$sort::'.print_r($sort,true).'</pre>';
        //echo '<pre>$sort_direction::'.print_r($sort_direction,true).'</pre>';
        if (!$outputFormatCount) { // set order by if we do not return count of rows
            if (empty($sort)) {
                $sort = 'created';
                $sort_direction = 'desc';
            }
            if (!empty($sort)) {
                $sqlStatement .= " order by " . $sort . " ";
            }
            if (!empty($sort) and !empty($sort_direction)) {
                $sort_direction = nsnClass_appFuncs::getLimitedValue($sort_direction, array('asc', 'desc'), 'asc');
            }
            if (!empty($sort_direction)) {
                $sqlStatement .= " " . $sort_direction . " ";
            }
        } // if (!$outputFormatCount) { // set order by if we do not return count of rows

        $limitSqlSubstr= '';
        if ( !empty($paged) and !empty($filters['itemsPerPage']) ) {
            $itemsPerPage= (int)$filters['itemsPerPage'];
            $limitStart= ($paged - 1) * $itemsPerPage ;
            $limitSqlSubstr= ' LIMIT ' . $itemsPerPage . " OFFSET  " . $limitStart;
        }
        $sqlStatement.= $limitSqlSubstr;
        if ( !empty($filters['limit']) and is_numeric($filters['limit'])) {
            $sqlStatement.=  " limit " . $filters['limit'] . " offset 0 ";
        }

        DebToFile('-getArtistReviewsList  $sqlStatement::' . print_r(nsnClass_appFuncs::showFormattedSql($sqlStatement, false),true));
        $retResult = $this->m_wpdb->get_results($sqlStatement, ARRAY_A);
        if ($outputFormatCount) {
            return $retResult[0]['rowsCount'];
        } else {
            return $retResult;
        }
        return false;
    }   // public function getArtistReviewsListList($outputFormatCount = false, $page = '', $filters = array(), $sort = '', $sort_direction = '') {


    public function updateArtistReviews($artist_review_id, $dataArray)
    {
        //$dataArray['fgfdg']= 'dfsfda';
        if (empty($artist_review_id)) {
            $this->m_wpdb->insert( $this->tbl_artist_reviews, $dataArray );
            return $this->m_wpdb->insert_id;;
        } else {
            $this->m_wpdb->update( $this->tbl_artist_reviews, $dataArray, array( 'ID' => $artist_review_id ));
            return $artist_review_id;
        }
    } //public function updateArtistReview($artist_review_id, $postArray)


    public function getRowById($artist_review_id, $filters= array()) {
        $hasArtistDisplayNameJoined= !empty($filters['show_artist_display_name']);

        $withInappropriatesCount= !empty($filters['with_inappropriates_count']);
        $withInappropriatesCountQuery= '';
        if ( $withInappropriatesCount ) {
            $withInappropriatesCountQuery= ', ( select count(*) from `' . $this->tbl_artist_reviews_inappropriate . '` where `' . $this->tbl_artist_reviews_inappropriate . '`.`artist_review_id`= `' . $this->tbl_artist_reviews . '`.`ID` ) as reviewsInappropriatesCount';
        }
        $sqlStatement = "SELECT `".$this->tbl_artist_reviews."`.* ".
            ($hasArtistDisplayNameJoined ? ", `" . $this->tbl_artists . "`.`name` as artist_name" : "") .
            $withInappropriatesCountQuery .
            " FROM `" . $this->tbl_artist_reviews . "`" .
            ( ( $hasArtistDisplayNameJoined ) ? " JOIN `" . $this->tbl_artists."` ON `" . $this->tbl_artists."`.`ID` = `" . $this->tbl_artist_reviews . "`.`artist_id`" : ""
            ) .
            " where `".$this->tbl_artist_reviews."`.`ID`= '" . $artist_review_id ."' ";

        DebToFile('++ArtistReviews getRowById  $sqlStatement::' . print_r(nsnClass_appFuncs::showFormattedSql($sqlStatement, false),true));
        $retResult = $this->m_wpdb->get_results($sqlStatement, ARRAY_A);
        if (!empty($retResult[0])) {

            return $retResult[0];
        }
        return false;
    } // public function getRowById($artist_review_id) {

    public function deleteArtistReviewsRow($artist_review_id, $onlyReturnQueries = false) {
        if (!empty($artist_review_id)) {
            $sqlQueries = array(
                "DELETE FROM `".$this->tbl_artist_reviews_inappropriate."` WHERE `artist_review_id` = '". $artist_review_id ."'",
                "DELETE FROM `".$this->tbl_artist_reviews."` WHERE `ID` = '". $artist_review_id ."'");
            if ($onlyReturnQueries) return $sqlQueries;
            $runQueriesRes = nsnClass_appFuncs::stripErrorCode($this->runQueriesUnderTransaction($sqlQueries));

            if (!empty($runQueriesRes)) {
                nsnClass_appFuncs::addLog($runQueriesRes, __FILE__ . ', ' . __LINE__, 'S', $this->m_wpdb);
            }
            return $runQueriesRes;
        }
        return false;
    } // public function deleteArtistReviewsRow($artist_review_id) {

}
//}