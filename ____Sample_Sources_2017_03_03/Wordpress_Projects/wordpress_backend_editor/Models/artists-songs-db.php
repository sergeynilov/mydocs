<?php
if ( ! defined( 'ArtistsSongs' ) ) {
    exit; // Exit if accessed directly
}
//return;


class ArtistsSongsDB // file with common control class
//
{
    public $m_catMusicJenreName; // table's name vars
    public $m_catMusicJenreSlug;
    ...
    public $tbl_song_jenres;
    ...
    public function ArtistsSongsDB($wpdb, $plugin_dir='', $plugin_url='')
    {
        $this->m_wpdb = $wpdb;
        $this->m_plugin_dir = $plugin_dir;
        $this->m_plugin_url = $plugin_url;
        $this->m_taxonomyName = 'artists_songs_tax';  // common app options : taxonomy and postType
        $this->m_postType = 'artists_songs';
        ...
        $this->m_catMusicJenreName = 'Artists&Songs music jenres';    // this category music jenre name will be imported into wp category
        $this->m_catMusicJenreSlug = 'artists-songs-music-jenres';    // this category music jenre slug will be imported into wp category
        $this->tbl_song_jenres = $this->m_wpdb->prefix . $this->m_dbTablesPrefix . 'song_jenres';  // table name in wp database
        ...
        $this->m_artistsAdded = 0;    // number of artists added in the in the end of importing
        $this->m_songsAdded = 0;      // number of songs added in the in the end of importing
        $this->m_defaultJenresList= array(1=>'pop',2=>'rock',3=>'jazz',4=>'classic',5=>'rap');
    }


    public function InitDBStructure($addTestData = false)  // tables created on plugin activation
    {
        $charset_collate = $this->getCharsetCollate();
        $current_user_id = get_current_user_id();
        $sqlCommandsList = [ 'KEY_CHECKS_0'=>' SET FOREIGN_KEY_CHECKS = 0; '];

        /*   tbl_songs BEGIN   */
        $sqlTableSongs = "     CREATE TABLE `" . $this->tbl_songs . "` (
        `ID` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
        `title` VARCHAR(100) NOT NULL DEFAULT '',
        `ordering` INT(10) UNSIGNED NULL,
        `is_active` enum('A','I') default 'I',
        `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (`ID`),
        KEY `ind_" . $this->tbl_songs . "_is_active_ordering` (`is_active`,`ordering`),
        UNIQUE KEY `" . $this->tbl_songs . "_title_UNIQUE` (`title`)
    )" . $charset_collate . ";";

        $sqlCommandsList[$this->tbl_songs] = $sqlTableSongs;
        /*   tbl_songs END   */

        ...

        /*   tbl_song_jenres BEGIN   */
        $sqlTableSongsJenres = "     CREATE TABLE `" . $this->tbl_song_jenres . "` (
        `ID` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
        `song_id` INT(10) UNSIGNED NOT NULL,
        `jenre_id` INT(10) UNSIGNED NOT NULL,
        `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (`ID`)
    )" . $charset_collate . ";";   // tbl_song_jenres
        $sqlTableSongsJenresForeignKeys= '
ALTER TABLE `' . $this->tbl_song_jenres . '`
  ADD CONSTRAINT `fk_' . $this->tbl_song_jenres . '_song_id` FOREIGN KEY (`song_id`) REFERENCES `'. $this->tbl_songs.'` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;';
        $sqlCommandsList[$this->tbl_song_jenres] = $sqlTableSongsJenres ;
        $sqlCommandsList[$this->tbl_song_jenres.'_fk'] = $sqlTableSongsJenresForeignKeys;
        /*   tbl_song_jenres END   */

        ...

        /*   tbl_artist_photos_inappropriate BEGIN   */
        $sqlTableArtistPhotosInappropriate = "     CREATE TABLE `" . $this->tbl_artist_photos_inappropriate . "` (
        `ID` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
        `artist_photo_id` INT(10) UNSIGNED NOT NULL,
        `user_id` BIGINT(20) UNSIGNED NOT NULL,
        `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (`ID`)
    )" . $charset_collate . ";";   // tbl_artist_photos_inappropriate
        $sqlTableArtistPhotosInappropriateForeignKeys= '
ALTER TABLE `' . $this->tbl_artist_photos_inappropriate . '`
  ADD CONSTRAINT `fk_' . $this->tbl_artist_photos_inappropriate . '_user_id` FOREIGN KEY (`user_id`) REFERENCES `'. $this->m_wpdb->prefix . 'users` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_' . $this->tbl_artist_photos_inappropriate . '_artist_photo_id` FOREIGN KEY (`artist_photo_id`) REFERENCES `'.$this->tbl_artist_photos.'` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;';
        $sqlCommandsList[$this->tbl_artist_photos_inappropriate] = $sqlTableArtistPhotosInappropriate ;
        $sqlCommandsList[$this->tbl_artist_photos_inappropriate.'_fk'] = $sqlTableArtistPhotosInappropriateForeignKeys;
        /*   tbl_artist_photos_inappropriate END   */

        ...

        /*   tbl_song_artists BEGIN   */
        $sqlTableSongsArtists = "     CREATE TABLE `" . $this->tbl_song_artists . "` (
        `ID` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
        `song_id` INT(10) UNSIGNED NOT NULL,
        `artist_id` INT(10) UNSIGNED NOT NULL,
        `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (`ID`)
    )" . $charset_collate . ";";   // tbl_song_artists
        $sqlTableSongsArtistsForeignKeys= '
ALTER TABLE `' . $this->tbl_song_artists . '`
  ADD CONSTRAINT `fk_' . $this->tbl_song_artists . '_song_id` FOREIGN KEY (`song_id`) REFERENCES `' . $this->tbl_songs . '` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_' . $this->tbl_song_artists . '_artist_id` FOREIGN KEY (`artist_id`) REFERENCES `' . $this->tbl_artists . '` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;';
        $sqlCommandsList[$this->tbl_song_artists] = $sqlTableSongsArtists;
        $sqlCommandsList[$this->tbl_song_artists.'_fk'] = $sqlTableSongsArtistsForeignKeys;
        /*   tbl_song_artists END   */




        $sqlCommandsList['KEY_CHECKS_1'] = [' SET FOREIGN_KEY_CHECKS = 1; '];
        if ($this->isDebug) echo '<pre>$sqlCommandsList::' . print_r($sqlCommandsList, true) . '</pre><br>';

        foreach ($sqlCommandsList as $tblName => $tblStructure) {  // run all sql commands written before
            if ($this->m_wpdb->get_var("show tables like '" . $tblName . "'") != $tblName) {
                dbDelta($tblStructure);
            }
        }
        if ($addTestData) {
            $this->insertDemoData( $current_user_id );
        }
        return true;
    } // public function InitDBStructure($addTestData= false)

    public function insertDemoData( $current_user_id )
    { // insert demo data for tables created in InitDBStructure : use wp methods for valid data writing
        $generatedTestUsersList = array();
        $languagesList=  nsnClass_appLangs::getLanguagesList();  // all languages installed in system
        $hasLanguages= nsnClass_appLangs::hasLanguages(); // if system is multilingual
        if ( !empty($languagesList) and is_array($languagesList) ) {
            $hasLanguages= true;
        } else {
            $languagesList=  array(1);
        }


        /* create 1st sample user */
        $user_name= 'songs test User';
        $user_email= 'songs-test-user@mail.com';
        $user_id = username_exists( $user_name );
        if ( !$user_id and email_exists($user_email) == false ) {
            $user_password = '111111';
            $user_id = wp_create_user( $user_name, $user_password, $user_email );
        }
        $generatedTestUsersList[] = array('user_id'=>$user_id, 'user_name'=> $user_name, 'user_email'=> $user_email);

        /* create 2nd sample user */
        $user_name= 'songs Other Test User';
        $user_email= 'songs-other-test-user@mail.com';
        $user_id = username_exists( $user_name );
        if ( !$user_id and email_exists($user_email) == false ) {
            $user_password = '111111';
            $user_id = wp_create_user( $user_name, $user_password, $user_email );
        }
        $generatedTestUsersList[] = array('user_id'=>$user_id, 'user_name'=> $user_name, 'user_email'=> $user_email);


        $catMusicJenresIdList= array();
        $MusicJenresIdResArrayList= array();
        ...
        foreach( $languagesList as $nextLang ) { // for all languages installed in system

            $langLabel= $nextLang . ' : ';
            $langSlug= $nextLang .'-';
            if ( !$hasLanguages ) {
                $langLabel= '';
                $langSlug= '';
            }
            // create categories for app data
            $catCommonAll = array('cat_name' => $langLabel . $this->m_catCommonAllName, 'category_description' => $langLabel . 'CommonAll Category for all Artists&Songs posts', 'category_nicename' =>  $langSlug .  $this->m_catCommonAllSlug, 'category_parent' => '', 'taxonomy' => $this->m_taxonomyName);
            $catCommonAllId = wp_insert_category($catCommonAll); // create wp category by sample data and keep its ID for creating of posts with this category ID
            nsnClass_appLangs::setTermLanguage($catCommonAllId, $nextLang);
            $catCommonAllIdList[] = array( 'lang'=>$nextLang, 'category_id'=>$catCommonAllId );
            $catCommonAllIdResArrayList[$nextLang]= $catCommonAllId;

            ...

            /////////////////
            $catMusicJenre = array('cat_name' => $langLabel . $this->m_catMusicJenreName/*'Artists&Songs music jenres'*/, 'category_description' => $langLabel . 'Music jenres Category for all posts describing music jenre', 'category_nicename' => $langSlug . $this->m_catMusicJenreSlug, 'category_parent' => '', 'taxonomy' => $this->m_taxonomyName);
            $catMusicJenreId= wp_insert_category($catMusicJenre);
            $catMusicJenresIdList[] = array( 'lang'=>$nextLang, 'category_id'=> $catMusicJenreId);
            nsnClass_appLangs::setTermLanguage($catMusicJenreId, $nextLang);
            $MusicJenresIdResArrayList[$nextLang]= $catMusicJenreId;

            ...

            $catSong = array('cat_name' => $langLabel . $this->m_catSongsName, 'category_description' => $langLabel . 'Songs Category for all posts describing songs', 'category_nicename' => $langSlug . $this->m_catSongsSlug, 'category_parent' => '', 'taxonomy' => $this->m_taxonomyName);
            $catSongsId = wp_insert_category($catSong);
            $catSongIdList[] = array( 'lang'=>$nextLang, 'category_id'=>$catSongsId);
            nsnClass_appLangs::setTermLanguage($catSongsId, $nextLang);
            $catSongsIdResArrayList[$nextLang]= $catSongsId;

        }

        nsnClass_appLangs::saveTermTranslations($catCommonAllIdResArrayList);  // create term for next Language
        nsnClass_appLangs::saveTermTranslations($MusicJenresIdResArrayList);
        nsnClass_appLangs::saveTermTranslations($catSongsIdResArrayList);

        $sqlCommandsList= array();

        $insertedSongsList= $sampleData->setSongs($SongsTblSource);
        ...
        if ( $hasLanguages ) {
            foreach ($insertedSongsList as $nextInsertedSong) {
                foreach ($languagesList as $nextLang) { // adding all songs for all langs by sample data
                    if ($nextLang != nsnClass_appLangs::getDefaultLanguage()) {
                        $sqlCommandsList[] = "insert into `" . $this->tbl_song_langs . "` ( `song_id`, `title`, `lang`, `created` ) values( " . $nextInsertedSong['song_ID'] . ", '" .
                            $nextLang . ": " . addslashes($nextInsertedSong['song_title']) . "','" . $nextLang . "', '" . nsnClass_appFuncs::generateTimestampAmplitude() . "') ;";
                    }
                }
            }
        }
        $this->m_songsAdded = count($insertedSongsList);
        ...

        $L= count($generatedTestUsersList);
        $generatedTestUsersId= array();
        foreach( $generatedTestUsersList as $nextGeneratedTestUser ) {
            $generatedTestUsersId[]= $nextGeneratedTestUser['user_id'];
        }
        $rand= rand(0, $L-1);
        $randomUserId= ( !empty( $generatedTestUsersId[$rand] ) ? $generatedTestUsersId[$rand] : get_current_user_id()  );
        $sqlCommandsList[] = "insert into `" . $this->tbl_artist_reviews . "` ( `ID`, `user_id`, `artist_id`, `review_text`, `created` ) values(1, $randomUserId, 1, 'artist_reviews # 1', '".nsnClass_appFuncs::generateTimestampAmplitude()."') ;";
        $sqlCommandsList[] = "insert into `" . $this->tbl_artist_reviews . "` ( `ID`, `user_id`, `artist_id`, `review_text`, `created` ) values(2, $randomUserId, 2, 'artist_reviews # 2', '".nsnClass_appFuncs::generateTimestampAmplitude()."') ; ";
        $sqlCommandsList[] = "insert into `" . $this->tbl_artist_reviews . "` ( `ID`, `user_id`, `artist_id`, `review_text`, `created` ) values(3, $randomUserId, 3, 'artist_reviews # 3', '".nsnClass_appFuncs::generateTimestampAmplitude()."') ;";
        $sqlCommandsList[] = "insert into `" . $this->tbl_artist_reviews . "` ( `ID`, `user_id`, `artist_id`, `review_text`, `created` ) values(4, $randomUserId, 2, 'artist_reviews # 4', '".nsnClass_appFuncs::generateTimestampAmplitude()."') ;";

        ...

        $artistReviewsList = $sampleData->setArtistReviews();
        foreach( $artistReviewsList as $nextArtistsReview ) {
            $sqlCommandsList[] = $nextArtistsReview;
        }
        ///////////////////////////


        $songsList = $SongsTblSource->getSongsList(false, '', array( ));
        $upload_dir = nsnClass_appFuncs::getUploadDir(); // d:\wwwroot\wp-songs\wp-content\uploads\tmp\song_new_song_image\
        foreach ($songsList as $nextSong) {
            //$allowed_types= getAppVar('uploads_images_allowed_types');  // 'gif|jpeg|jpg|png|bmp';
            //$songDir = $this->m_songsTblSource->getImageDir($nextArtist['ID'] );
            $song_id = $nextSong['ID'];
            //$relativePath = '__SampleData' . DIRECTORY_SEPARATOR . 'song_image_' . $song_id;


            $dst_dirname = $upload_dir['basedir'] . DIRECTORY_SEPARATOR . 'song_images' . DIRECTORY_SEPARATOR . 'song_image_' . $song_id;

            $songImagesDirs = array($upload_dir['basedir'], $upload_dir['basedir'] . DIRECTORY_SEPARATOR . 'song_images', $dst_dirname);

            $songSrcDir = '__SampleData' . DIRECTORY_SEPARATOR . 'song_image_' . $nextSong['ID'] . DIRECTORY_SEPARATOR . 'song_image_' . $nextSong['ID'];

            $allowed_types = get_option('songs-songs-option_uploads_images_allowed_types', 'gif|jpeg|jpg|png|bmp|tif');
            $allowed_types_array = preg_split('/\|/', $allowed_types);

            //echo '<pre>$allowed_types_array::'.print_r($allowed_types_array,true).'</pre>';

            foreach ($allowed_types_array as $next_allowed_type) {
                $song_image_filepath = $this->m_plugin_dir . $songSrcDir . '.' . strtolower($next_allowed_type);

                $dstImageFullPath = $SongsTblSource->getImage($nextSong['ID'], $next_allowed_type, false);
                if (file_exists($song_image_filepath)) {
                    nsnClass_appFuncs::createDir($songImagesDirs, true);
                    $ret = copy($song_image_filepath, $dstImageFullPath);
                }
            }

        }

        /////////////////////////////////////

        require_once( 'artist-photos.php');
        $ArtistPhotosTblSource = new ArtistPhotos($this->m_wpdb);
        require_once( 'artists.php');
        $ArtistsTblSource = new Artists($this->m_wpdb);
        $artistsList = $ArtistsTblSource->getArtistsList(false, '', array( ));
        $upload_dir = nsnClass_appFuncs::getUploadDir(); // d:\wwwroot\wp-songs\wp-content\uploads\tmp\artist_new_song_image\
        foreach ($artistsList as $nextArtist) {
            $artist_id= $nextArtist['ID'];
            $relativePath= '__SampleData'.DIRECTORY_SEPARATOR.'artist_image_' . $artist_id;
            $dst_dirname= $upload_dir['basedir'] . DIRECTORY_SEPARATOR . 'artist_images' . DIRECTORY_SEPARATOR . 'artist_image_' . $artist_id;

            $artistImagesDirs = array($upload_dir['basedir'], $upload_dir['basedir'] . DIRECTORY_SEPARATOR . 'artist_images', $dst_dirname);
            nsnClass_appFuncs::createDir($artistImagesDirs,true);
            $artistSrcDir= '__SampleData'.DIRECTORY_SEPARATOR.'artist_image_' . $nextArtist['ID'] . DIRECTORY_SEPARATOR . 'artist_image_' . $nextArtist['ID'];
            $allowed_types= get_option('songs-artists-option_uploads_images_allowed_types', 'gif|jpeg|jpg|png|bmp|tif');
            $allowed_types_array= preg_split('/\|/', $allowed_types);
            foreach($allowed_types_array as $next_allowed_type) {
                $artist_image_filepath = $this->m_plugin_dir . $artistSrcDir . '.' . strtolower($next_allowed_type);
                $dstImageFullPath = $ArtistsTblSource->getImage($nextAfrtist['ID'],$next_allowed_type,false);
                if ( file_exists($artist_image_filepath) ) {
                    $ret = copy($artist_image_filepath, $dstImageFullPath);
                }
            }

            $artistPhotosList = $ArtistPhotosTblSource->getArtistPhotosList(false, '', array( 'artist_id'=> $nextArtist['ID'] ));
            foreach ($artistPhotosList as $nextArtistsPhoto) { // photos of any Artist
                $photo_name= $nextArtistsPhoto['photo_name'];
                $srcFilename= $this->m_plugin_dir . $relativePath . DIRECTORY_SEPARATOR . $photo_name;
                $dstFilename= $dst_dirname . DIRECTORY_SEPARATOR . $nextArtistsPhoto['photo_name'];
                $ret= copy(  $srcFilename, $dstFilename );
            } // foreach ($artistPhotosList as $nextArtistsPhoto) { // photos of any Artist
        }

        $sqlCommandsList= array();
        $ArtistPhotosTblSource = new ArtistPhotos($this->m_wpdb);
        $artistPhotosList = $ArtistPhotosTblSource->getArtistPhotosList(false, '', array());
        foreach ($artistPhotosList as $nextArtistsPhoto) {
            $randVal= rand(1,10);
            if ( $randVal==3 or $randVal == 6 or $randVal==8 ) {
                foreach( $generatedTestUsersId as $nextTestUser ) {
                    $sqlCommandsList[] = "insert into `" . $this->tbl_artist_photos_inappropriate . "` ( `user_id`, `artist_photo_id`, `created` ) values( ".$nextTestUser." , " . $nextArtistsPhoto['ID'] . ", '" . nsnClass_appFuncs::generateTimestampAmplitude() . "') ;";
                }
            }
        }
        foreach ($sqlCommandsList as $insertData) {
            $this->m_wpdb->query($insertData);
        }

        ...

    } // public function insertTestData() {

    public function ClearDBStructure()  // remove app's tables on plugin deactivation
    {

        require_once( 'artist-photos.php');
        $ArtistPhotosTblSource = new ArtistPhotos($this->m_wpdb);
        $artistPhotosList = $ArtistPhotosTblSource->getArtistPhotosList(false, '', array( ));
        $upload_dir = nsnClass_appFuncs::getUploadDir(); // d:\wwwroot\wp-songs\wp-content\uploads\tmp\artist_new_song_image\
        foreach ($artistPhotosList as $nextArtistsPhoto) {
            $artist_id = $nextArtistsPhoto['artist_id'];
            $dst_dirname = $upload_dir['basedir'] . DIRECTORY_SEPARATOR . 'artist_images' . DIRECTORY_SEPARATOR . 'artist_image_' . $artist_id;
            nsnClass_appFuncs::DeleteDirectory($dst_dirname);
        }

        $sqlCommandsList = [];
        $sqlCommandsList['KEY_CHECKS_0'] = [' SET FOREIGN_KEY_CHECKS = 0; '];
        $sqlCommandsList[$this->tbl_user_songs] = "DROP TABLE IF EXISTS " . $this->tbl_user_songs;
        ...
        $sqlCommandsList[$this->tbl_artists] = "DROP TABLE IF EXISTS " . $this->tbl_artists;

        $sqlCommandsList['songs Test User'] = "DELETE FROM `" . $this->m_wpdb->prefix . "users` where user_login = 'songs Test User' ";
        $sqlCommandsList['songs Other Test User'] = "DELETE FROM `" . $this->m_wpdb->prefix . "users` where user_login = 'songs Other Test User' ";


        $musicJenresCategory= get_category_by_slug( $this->m_catMusicJenreSlug /*'artists-songs-music-jenres'*/ );
        if (!empty($musicJenresCategory)) {
            query_posts( 'post_type='.$this->m_postType.'&term='.$musicJenresCategory->term_id );
            while (have_posts()) :
                the_post();
                $postId= get_the_ID();
                if ( !empty($postId) ) {
                    wp_delete_post($postId); // Delete post and all meta for it
                }
            endwhile;
            $wp_delete_categoryRes= wp_delete_category( $musicJenresCategory->cat_ID );
        }

        $languagesList=  nsnClass_appLangs::getLanguagesList();  // all languages installed in system
        $hasLanguages= false;
        if ( !empty($languagesList) and is_array($languagesList) ) {
            $hasLanguages= true;
        } else {
            $languagesList=  array(1);
        }
        foreach( $languagesList as $nextLang ) {
            $langSlug= $nextLang .'-';
            if ( !$hasLanguages ) {
                $langSlug= '';
            }

            $artistsCategory = get_term_by('slug',  $langSlug . $this->m_catMusicJenreSlug, $this->m_taxonomyName);
            if (!empty($artistsCategory)) {
                query_posts('post_type='.$this->m_postType.'&term=' . $artistsCategory->term_id);
                while (have_posts()) :
                    the_post();
                    $postId = get_the_ID();
                    if (!empty($postId)) {
                        wp_delete_post($postId);   // Delete post and all meta for it
                        delete_post_meta( $postId, 'song_artist_item' );
                        delete_post_meta( $postId, 'song_artist_item_id' );
                        delete_post_meta( $postId, 'post_type' );
                        delete_post_meta( $postId, 'song_artist_descr' );
                        delete_post_meta( $postId, 'song_artist_show_target' );
                    }
                endwhile;
                $wp_delete_categoryRes = wp_delete_term($artistsCategory->term_id, $this->m_taxonomyName);
            }

            ...

        } //         foreach( $languagesList as $nextLang ) {
        $sqlCommandsList['KEY_CHECKS_1'] = [' SET FOREIGN_KEY_CHECKS = 1; '];
        $sqlCommandsList['songs-artists-option_'] = "DELETE FROM `" . $this->m_wpdb->prefix . "options` where option_name like 'songs-artists-option_%' ";
        foreach ($sqlCommandsList as $tblName => $tblDrop) {
            if ($this->isDebug) echo '<pre>$tblDrop::' . print_r($tblDrop, true) . '</pre><br>';
            $this->m_wpdb->query($tblDrop);
        }
    } //public function ClearDBStructure()



    public function getCharsetCollate()
    {
        require_once(ABSPATH . 'wp-admin/upgrade-functions.php');
        $retResult = '';
        if (version_compare(mysql_get_server_info(), '4.1.0', '>=')) {
            if (!empty($this->m_wpdb->charset)) {
                $retResult = "DEFAULT CHARACTER SET " . $this->m_wpdb->charset;
            }
            if (!empty($this->m_wpdb->collate)) {
                $retResult .= " COLLATE " . $this->m_wpdb->collate;
            }
        }
        return $retResult;
    } // public function getCharsetCollate()

    public function runQueriesUnderTransaction($queriesList, $isInsert= false)
    {
        if (!is_array($queriesList) or empty($queriesList)) return '';
        $this->tranBegin();
        $hasError= false;
        ob_start();
        foreach( $queriesList as $nextKey=>$nextQuery ) {
            $ret= $this->m_wpdb->query($nextQuery); // * @return int|false Number of rows affected/selected or false on error
            if( $ret === FALSE ) {
                $hasError = true;
                break;
            }
        }
        if ( !$hasError ) {
            $this->tranCommit();
            $output = ob_get_contents ();
            ob_end_clean ();
            return ''; // all queries run successfully - return '' - as no error!
        } else {
            $output = ob_get_contents ();
            ob_end_clean ();
            $this->tranRollback();
            return $output; // all queries run with error - return $output asa error code!
        }
    } // public function getCharsetCollate()

    public function tranBegin()
    {
        if ( !empty($this->m_wpdb) ) {
            $Res= $this->m_wpdb->query( "BEGIN" );
        }
    }

    public function tranRollback()
    {
        if ( !empty($this->m_wpdb) ) {
            $Res= $this->m_wpdb->query( "ROLLBACK" );
        }
    }

    public function tranCommit()
    {
        if ( !empty($this->m_wpdb) ) {
            $Res= $this->m_wpdb->query( "COMMIT" );
        }
    }

}

