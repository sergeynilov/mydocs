<?php
if ( ! defined( 'ArtistsSongs' ) ) {
    exit; // Exit if accessed directly
}
require_once( 'artists-songs-db.php');
class ArtistPhotosInappropriates extends ArtistsSongsDB // ArtistPhotosInappropriates class of model extends ArtistsSongsDB, class common for all app
{

    public function getArtistPhotosInappropriatesList($outputFormatCount = false, $paged = '', $filters = array(), $sort = '', $sort_direction = '')
    {  // retrieve list of data by given parameters
        $hasWhereCond = false;
        $hasArtistsJoined= !empty($filters['artist_id']);
        $hasArtistsJoinedSubquery= '';
        if ( $hasArtistsJoined ) {
            $hasArtistsJoinedSubquery= " JOIN `" . $this->tbl_artist_photos."` ON `" . $this->tbl_artist_photos."`.`ID` = `" . $this->tbl_artist_photos_inappropriate . "`.`artist_photo_id` ";
        }

        $hasUserDisplayNameJoined= !empty($filters['show_user_display_name']);
        if ($outputFormatCount) { // return number of resulting rows - prepare sql for this
            $sqlStatement = "SELECT count(distinct " . $this->tbl_artist_photos_inappropriate . ".artist_photo_id) AS rowsCount FROM `" . $this->tbl_artist_photos_inappropriate . "`" . $hasArtistsJoinedSubquery;
        } else { // return result set with all fields of resulting rows - prepare sql for this
            $sqlStatement = "SELECT distinct `" . $this->tbl_artist_photos_inappropriate . "`.* ".($hasUserDisplayNameJoined ? ", `" . $this->m_wpdb->prefix . "users`.display_name" : "")." FROM `" . $this->tbl_artist_photos_inappropriate . "`" . ( $hasUserDisplayNameJoined ? " JOIN `" . $this->m_wpdb->prefix."users` ON `" . $this->m_wpdb->prefix."users`.`ID` = `" . $this->tbl_artist_photos_inappropriate . "`.`user_id`" : "" )  . $hasArtistsJoinedSubquery;
        }
        if (!empty($filters['artist_id'])) {  // set filter on artist_id field
            $sqlStatement .= ($hasWhereCond ? "" : " where ") . ($hasWhereCond ? " AND " : "") . " `" . $this->tbl_artist_photos . "`.artist_id = '" . $filters['artist_id'] . "' ";
            $hasWhereCond = true;
        }

        if (!empty($filters['artist_photo_id'])) {  // set filter on artist_photo_id field
            $sqlStatement .= ($hasWhereCond ? "" : " where ") . ($hasWhereCond ? " AND " : "") . " `" . $this->tbl_artist_photos_inappropriate . "`.artist_photo_id = '" . $filters['artist_photo_id'] . "' ";
            $hasWhereCond = true;
        }

        if (!empty($filters['id!='])) {  // set filter on id field != value
            $sqlStatement .= ($hasWhereCond ? "" : " where ") . ($hasWhereCond ? " AND " : "") . " `" . $this->tbl_artist_photos_inappropriate . "`.ID != '" . $filters['id!='] . "' ";
            $hasWheJenrereCond = true;
        }

        if (!$outputFormatCount) { // set order by if we do not return count of rows
            if (empty($sort)) {
                $sort = 'created';
                $sort_direction = 'desc';
            }
            if (!empty($sort)) {
                $sqlStatement .= " order by " . $sort . " ";
            }
            if (!empty($sort) and !empty($sort_direction)) {
                $sort_direction = nsnClass_appFuncs::getLimitedValue($sort_direction, array('asc', 'desc'), 'asc');
            }
            if (!empty($sort_direction)) {
                $sqlStatement .= " " . $sort_direction . " ";
            }
        } // if (!$outputFormatCount) { // set order by if we do not return count of rows

        $limitSqlSubstr= '';
        if ( !empty($paged) and !empty($filters['itemsPerPage']) ) { // if page is set - then calculate and set limit condition
            $itemsPerPage= (int)$filters['itemsPerPage'];
            $limitStart= ($paged - 1) * $itemsPerPage ;
            $limitSqlSubstr= ' LIMIT ' . $itemsPerPage . " OFFSET  " . $limitStart;
        }
        $sqlStatement.= $limitSqlSubstr;
        if ( !empty($filters['limit']) and is_numeric($filters['limit'])) {  // set number of rows
            $sqlStatement.=  " limit " . $filters['limit'] . " offset 0 ";
        }

        $retResult = $this->m_wpdb->get_results($sqlStatement, ARRAY_A);
        if ($outputFormatCount) {
            return $retResult[0]['rowsCount']; // return number of resulting rows
        } else {
            return $retResult; // return resulting array with all fields of resulting rows
        }
        return false;
    }   // public function getArtistPhotosInappropriatesListList($outputFormatCount = false, $page = '', $filters = array(), $sort = '', $sort_direction = '') {


    public function updateArtistPhotosInappropriates($artist_photo_inappropriate_id, $dataArray)
    { // add(if $artist_photo_inappropriate_id is empty)/modify ArtistPhotosInappropriates
        if (empty($artist_photo_inappropriate_id)) {
            $this->m_wpdb->insert( $this->tbl_artist_photos_inappropriate, $dataArray ); // add new tour of artist
            return $this->m_wpdb->insert_id;;
        } else {
            $this->m_wpdb->update( $this->tbl_artist_photos_inappropriate, $dataArray, array( 'ID' => $artist_photo_inappropriate_id ));
            return $artist_photo_inappropriate_id;
        }
    } //public function updateArtistReview($artist_photo_inappropriate_id, $postArray)


    public function getRowById($artist_photo_inappropriate_id) { // retrieve artist photo inappropriate by its ID
        $sqlStatement = "SELECT * FROM `" . $this->tbl_artist_photos_inappropriate . "` where ID= '" . $artist_photo_inappropriate_id ."' ";
        $retResult = $this->m_wpdb->get_results($sqlStatement, ARRAY_A);
        if (isset($retResult[0])) {
            return $retResult[0];
        }
        return false;
    } // public function getRowById($artist_photo_inappropriate_id) { // retrieve artist photo inappropriate by its ID

    public function deleteArtistPhotosInappropriatesRow($artist_photo_inappropriate_id) { // delete artist photos inappropriate by its ID
        if (!empty($artist_photo_inappropriate_id)) {
            $Res= $this->m_wpdb->query("DELETE FROM `".$this->tbl_artist_photos_inappropriate."` WHERE `ID` = '". $artist_photo_inappropriate_id ."'");
            return !($Res===false);
        }
        return false;
    } // public function deleteArtistPhotosInappropriatesRow($artist_photo_inappropriate_id) { // delete artist photos inappropriate by its ID

    public function deleteRowByArtistPhotoInappropriateId($artist_photo_inappropriate_id) {
        if (!empty($artist_photo_inappropriate_id)) {
            $Res= $this->m_wpdb->query("DELETE FROM `".$this->tbl_artist_photos_inappropriate."` WHERE `artist_photo_id` = '". $artist_photo_inappropriate_id ."'");
            return !($Res===false);
        }
        return false;
    } // public function deleteRowByArtistPhotoInappropriateId($artist_photo_inappropriate_id) {

}
//}