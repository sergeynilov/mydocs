<?php
if ( ! defined( 'ArtistsSongs' ) ) {
    exit; // Exit if accessed directly
}
require_once( 'artists-songs-db.php');
class Logs extends ArtistsSongsDB // Logs class of model extends ArtistsSongsDB, class common for all app
{

    public function getLogsList($outputFormatCount = false, $page = '', $filters = array(), $sort = '', $sort_direction = '') { // retrieve list of data by given parameters
        $hasWhereCond= false;
        if($outputFormatCount) {  // return number of resulting rows - prepare sql for this
            $sqlStatement = "SELECT count( distinct " . $this->tbl_logs . ".ID ) AS rowsCount FROM `" . $this->tbl_logs . "`";
        } else { // return result set with all fields of resulting rows - prepare sql for this
            $sqlStatement = "SELECT distinct `" . $this->tbl_logs . "`.* FROM `" . $this->tbl_logs . "`";
        }
        if ( !empty($filters['error_from']) ) {  // set filter on error_from field
            $sqlStatement.= ( $hasWhereCond? "":" where " ) . ( $hasWhereCond? " AND ":"" ) . " error_from like '%".$filters['error_from']."%' ";
            $hasWhereCond= true;
        }

        if (!empty($filters['user_id'])) {  // set filter on user_id field
            $sqlStatement .= ($hasWhereCond ? "" : " where ") . ($hasWhereCond ? " AND " : "") . " `" . $this->tbl_logs . "`.user_id = '" . $filters['user_id'] . "' ";
            $hasWhereCond = true;
        }

        if (!$outputFormatCount) { // set order by if we do not return count of rows
            if (empty($sort)) {
                $sort = 'created';
                $sort_direction = 'desc';
            }
            if (!empty($sort)) {
                $sqlStatement .= " order by " . $sort . " ";
            }
            if (!empty($sort) and !empty($sort_direction)) {
                $sort_direction = nsnClass_appFuncs::getLimitedValue($sort_direction, array('asc', 'desc'), 'asc');
            }
            if (!empty($sort_direction)) {
                $sqlStatement .= " " . $sort_direction . " ";
            }
        } // if (!$outputFormatCount) { // set order by if we do not return count of rows

        $limitSqlSubstr= '';
        if ( !empty($filters['paged']) and !empty($filters['itemsPerPage']) ) { // if page is set - then calculate and set limit condition
            $itemsPerPage= (int)$filters['itemsPerPage'];
            $paged= (int)$filters['paged'];
            $limitStart= ($paged - 1) * $itemsPerPage ;
            $limitSqlSubstr= ' LIMIT ' . $itemsPerPage . " OFFSET  " . $limitStart;
        }
        $sqlStatement.= $limitSqlSubstr;
        if ( !empty($filters['limit']) and is_numeric($filters['limit'])) { // set number of rows
            $sqlStatement.=  " limit " . $filters['limit'] . " offset 0 ";
        }

        $retResult = $this->m_wpdb->get_results($sqlStatement, ARRAY_A);
        if($outputFormatCount) {
            return $retResult[0]['rowsCount']; // return number of resulting rows
        } else{
            return $retResult; // return resulting array with all fields of resulting rows
        }
    }   // public function getLogsList($outputFormatCount = false, $page = '', $filters = array(), $sort = '', $sort_direction = '') {

    public function updateLog($log_id, $dataArray)
    { // add(if $log_id is empty)/modify log
        if (empty($log_id)) {
            $ret= $this->m_wpdb->insert( $this->tbl_logs, $dataArray ); // add new log
            return ( $ret === false ? false : $this->m_wpdb->insert_id );
        } else {
            $ret= $this->m_wpdb->update( $this->tbl_logs, $dataArray, array( 'ID' => $log_id ));
            return ( $ret === false ? false : $log_id );
        }
    } //public function updateLog($log_id, $postArray)

    public function getRowById($log_id) {  // retrieve log by its ID
        $sqlStatement = "SELECT * FROM `" . $this->tbl_logs . "` where ID= '" . $log_id ."' ";
        $retResult = $this->m_wpdb->get_results($sqlStatement, ARRAY_A);
        if (!empty($retResult[0])) {
            return $retResult[0];
        }
        return false;
    } // public function getRowById($log_id) { // retrieve log by its ID


    public function deleteLogRow($log_id) { // delete log by its ID
        if (!empty($log_id)) {
            $Res= $this->m_wpdb->query("DELETE FROM `".$this->tbl_logs."` WHERE `ID` = '". $log_id ."'");
            return !($Res===false);
        }
        return false;
    } // public function deleteLogRow($log_id) {  // delete log by its ID

}