<?php
if (!defined('ArtistsSongs')) {
    exit; // Exit if accessed directly
}
?>

<?php if (count($artistPhotosList) > 0) : ?>
    <table class="footable table table-primary default footable-loaded" border="0px dotted green">
        <?php $Counter = 0;
        $ImagesGalleryHTML = '';

        foreach ($artistPhotosList as $nextArtistPhoto):
            $inappropriateImg = '';
            $photosInappropriateCount = $nextArtistPhoto['inappropriateCount'];
            if ($photosInappropriateCount > 0) {
                if ($show_only_text == 1) {
                    $inappropriateImg = ' <img src="' . $plugin_url . '/images/is_inappropriate.png" alt="Is Inappropriate" title="Is Inappropriate">';
                } else {
                    $inappropriateImg = ' <a style="cursor:pointer" onclick="javascript:backendArtistsEditorFuncs.clearArtistPhotoInappropriate(' . $nextArtistPhoto['ID'] . ',\'' . $nextArtistPhoto['photo_name'] . ' : ' . $nextArtistPhoto['description'] . '\');" ><img src="' . $plugin_url . '/images/is_inappropriate.png" alt="Is Inappropriate" title="Is Inappropriate"></a>';
                }
            }

            $ArtistPhotosTblSource = new ArtistPhotos($this->m_wpdb);
            $filename_path = $ArtistPhotosTblSource->getImage($nextArtistPhoto['artist_id'], $nextArtistPhoto['ID'], $nextArtistPhoto['photo_name'], false, true);
            $filename_url = $ArtistPhotosTblSource->getImage($nextArtistPhoto['artist_id'], $nextArtistPhoto['ID'], $nextArtistPhoto['photo_name'], true, true);//'/' .
            $Filesize = filesize($filename_path);
            $filesizeLabel = nsnClass_appFuncs::getFileSizeAsString(filesize($filename_path));
            $FilenameInfo = nsnClass_appFuncs::GetImageShowSize($filename_path, $m_parentControl->m_orig_width, $m_parentControl->m_orig_height, false);
            $deletelinkHTML = '';
            if ($show_only_text == 0) {
                $deletelinkHTML = '<a style="cursor:pointer" onclick="javascript:backendArtistsEditorFuncs.deleteArtistPhoto(' . $nextArtistPhoto['ID'] . ',\'' . $nextArtistPhoto['photo_name'] . ' : ' . $nextArtistPhoto['description'] . '\');" ><img src="' . $plugin_url . 'images/delete.png"></a>';
            }

            $ImagesGalleryHTML .= (($Counter % $m_parentControl->m_galleryImagesPerRow) == 0 ? '</tr><tr>' : '') . '<td><a class="fancybox-button" rel="fancybox-button" alt="' . $nextArtistPhoto['description'] . '"  title="' . $nextArtistPhoto['description'] . '" href="' . $filename_url . '"><img src="' . $filename_url . '" width="' . $FilenameInfo['Width'] . '" height="' . $FilenameInfo['Height'] . '" /></a>&nbsp;&nbsp;<br>&nbsp;' . $deletelinkHTML . '&nbsp;&nbsp;&nbsp;' . $inappropriateImg . $nextArtistPhoto['photo_name'] . ', ' . $filesizeLabel . ', &nbsp;' . $FilenameInfo['OriginalWidth'] . '*' . $FilenameInfo['OriginalHeight'] . '</td>'; ?>

            <? $Counter++; endforeach; ?>

        <?= $ImagesGalleryHTML; ?>

        <?php if ($navigationHTML != '') : ?>
            <tr>
                <td colspan="<?php echo $m_parentControl->m_galleryImagesPerRow; ?>">
                    <?php echo $navigationHTML; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <?php echo($itemsPerPage > $artistPhotosCount ? $artistPhotosCount : $itemsPerPage); ?> <?php echo esc_html__("Rows of"); ?> <?php echo $artistPhotosCount; ?>
                    (<?php echo esc_html__("Page") ?> # <?php echo $paged ?>)
                </td>
            </tr>
        <?php endif; ?>

    </table>

<?php endif; ?>

<?php if (count($galleriesList) > 0) : ?>
    <table class="footable table table-primary default footable-loaded" border="0px dotted green">
        <tr>
            <td>
                <select id="linked_ngg_gallery_id" name="linked_ngg_gallery_id">
                    <option value=""><?php echo esc_html__(" -Select NextGEN Gallery- ") ?></option>
                    <?php foreach ($galleriesList as $key => $nextItem) { ?>
                        <option value="<?php echo $nextItem['gid'] ?>" <?php echo($nextItem['gid'] == $artistObj['ngg_gallery_gid'] ? " selected " : "") ?> ><?php echo $nextItem['title']. ' ('. ($nextItem['picturesCount'] . ' ' . esc_html__("pictures") . ')') ?></option>
                    <?php } ?>
                </select>&nbsp;&nbsp;
            </td>
            <td>
            <input type="radio" name="linked_ngg_gallery_id_operation" id="linked_ngg_gallery_id_operation_import_and_add" value="import_and_add"><?php echo esc_html__("Import all images and description from selected NextGEN Gallery and add to existing images") ?><br>
            <input type="radio" name="linked_ngg_gallery_id_operation" id="linked_ngg_gallery_id_operation_import_and_remove" value="import_and_remove"><?php echo esc_html__("Import all images and description from selected NextGEN Gallery and remove existing images") ?><br>
            <input type="radio" name="linked_ngg_gallery_id_operation" id="linked_ngg_gallery_id_operation_keep_link" <?php echo (!empty($artistObj['ngg_gallery_gid']) ? "checked" :"") ?> value="keep_link"><?php echo esc_html__("Keep link to NextGEN Gallery(images will be shown from selected library)") ?><br>

             <input type="radio" name="linked_ngg_gallery_id_operation" id="linked_ngg_gallery_id_operation_no_operation" value="no_operation"><?php echo esc_html__("Clear Keep link and clear all images of artist") ?><br>

            <a style="cursor:pointer" onclick="javascript:backendArtistsEditorFuncs.MakeNextGENGalleryLink('<?php echo $artist_id?>');" >
                <img src="<?php echo WP_PLUGIN_URL ?>/nextgen-gallery/products/photocrati_nextgen/modules/ngglegacy/admin/images/nextgen_16_color.png"><?php echo esc_html__("Run Operation") ?>
            </a>

            </td>
        </tr>
    </table>

<?php endif; ?>