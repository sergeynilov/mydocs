<?php
if ( ! defined( 'ArtistsSongs' ) ) {
    exit; // Exit if accessed directly
}
?>

<script type="text/javascript" language="JavaScript" src="<?php echo get_home_url() ?>/wp-content/plugins/ckeditor-for-wordpress/ckeditor/ckeditor.js"
        xmlns="http://www.w3.org/1999/html"></script>


<script type="text/javascript" language="JavaScript" src="<?php echo $plugin_url; ?>js/jquery/jquery.datetimepicker.js"></script>
<link rel="stylesheet" href="<?php echo $plugin_url; ?>css/jquery.datetimepicker.css" type="text/css" media="all"/>
<script type="text/javascript" language="JavaScript" src="<?php echo $plugin_url; ?>js/jquery/jquery.fancybox.pack.js"></script>
<link rel="stylesheet" href="<?php echo $plugin_url; ?>css/jquery.fancybox.css" type="text/css" media="all"/>


<link type="text/css" rel="stylesheet" href="<?php echo $plugin_url; ?>/css/jQuery-File-Upload/jquery.fileupload.css" media="screen"/>
<script type="text/javascript" src="<?php echo $plugin_url; ?>/js/jQuery-File-Upload/vendor/jquery.ui.widget.js" charset="UTF-8"></script>
<script type="text/javascript" src="<?php echo $plugin_url; ?>/js/jQuery-File-Upload/jquery.iframe-transport.js" charset="UTF-8"></script>
<script type="text/javascript" src="<?php echo $plugin_url; ?>/js/jQuery-File-Upload/jquery.fileupload.js" charset="UTF-8"></script>

<link rel="stylesheet" href="<?php echo $plugin_url; ?>css/chosen.css" type="text/css" media="all"/>
<script type="text/javascript" language="JavaScript" src="<?php echo $plugin_url; ?>js/jquery/chosen.jquery.js"></script>



<script type="text/javascript" language="JavaScript">
    /*<![CDATA[*/

    var backendArtistsEditorFuncs = new backendArtistsEditorFuncs( { // must be called before jQuery(document).ready(function ($) {
        <?php echo nsnClass_appFuncs::prepareParams( true,  array('plugin_url'=> $plugin_url, 'm_pageUrl'=> $m_pageUrl,  'm_songsPageUrl'=> $m_songsPageUrl,'m_artist_id'=>$m_parentControl->m_artist_id , 'm_is_insert'=> $m_parentControl->m_is_insert, "m_artist_registered_at"=>nsnClass_appFuncs::cutSecondsFromDateTimeString($m_parentControl->m_artistEditorItem['registered_at']) )
         )?>
    } );


    jQuery(document).ready(function ($) {
        backendArtistsEditorFuncs.onEditorInit()
    });

    /*]]>*/
</script>

<div class="form-editor-wrap" >
    <h3><?php echo ( $m_parentControl->m_action=="add"?esc_html__("Add"):esc_html__("Edit") ) ?> <?php echo esc_html__("Artist")?></h3>

    <form method="post"  class="validate"  action="<?php echo $m_parentControl->m_pageUrl; ?>&artist_id=<?php echo $m_parentControl->m_artist_id?>" id="form_artist_editor" enctype="multipart/form-data" >
        <input type="hidden" name="<?php echo $m_parentControl->m_labelNonceInput ?>" value="<?php echo wp_create_nonce($m_parentControl->m_labelNonce) ?>" />
        <input type="hidden" value="update" id="action" name="action">
        <input type="hidden" id="is_reopen" name="is_reopen" value="">
        <input type="hidden" id="paged" name="paged" value="<?php echo $m_parentControl->m_paged; ?>">
        <input type="hidden" id="sort" name="sort" value="<?php echo $m_parentControl->m_sort; ?>">
        <input type="hidden" id="sort_direction" name="sort_direction" value="<?php echo $m_parentControl->m_sort_direction; ?>">
        <input type="hidden" id="filter_name" name="filter_name" value="<?php echo $m_parentControl->m_filter_name ?>">
        <input type="hidden" id="filter_is_active" name="filter_is_active" value="<?php echo $m_parentControl->m_filter_is_active ?>">

        <input type="hidden" id="filter_registered_at_1" name="filter_registered_at_1" value="<?php echo $m_parentControl->m_filter_registered_at_1 ?>">
        <input type="hidden" id="filter_registered_at_2" name="filter_registered_at_2" value="<?php echo $m_parentControl->m_filter_registered_at_2 ?>">

        <input type="hidden" id="filter_with_photos" name="filter_with_photos" value="<?php echo $m_parentControl->m_filter_with_photos ?>">
        <input type="hidden" id="filter_with_inappropriate_photos" name="filter_with_inappropriate_photos" value="<?php echo $m_parentControl->m_filter_with_inappropriate_photos ?>">
        <input type="hidden" id="filter_with_reviews" name="filter_with_reviews" value="<?php echo $m_parentControl->m_filter_with_reviews ?>">
        <input type="hidden" id="filter_with_inappropriate_reviews" name="filter_with_inappropriate_reviews" value="<?php echo $m_parentControl->m_filter_with_inappropriate_reviews ?>">

        <div class="form-artist_song-field term-ID-wrap" style="<?php echo ((int)$m_parentControl->m_is_insert == 1) ? "display:none" : "" ?>" >
            <label for="artist_id"><?php echo esc_html__("ID")?></label>
            <input type="text" size="8" value="<?php echo $m_parentControl->m_artistEditorItem['ID']?>" id="artist_id" name="artist_id" style="border: 1px dotted #808080; text-align: right;" readonly>
            <p>Unique ID if Artist. Is not editable.</p>
        </div>

        <div class="form-artist_song-field form-require term-name-wrap <?php echo showErrorMessage("artist_name","name",$m_parentControl->m_hasValidationErrors, true) ?>">
            <label for="artist_name"><?php echo esc_html__("Name")?></label>
            <input type="text" aria-required="true" value="<?php echo $m_parentControl->m_artistEditorItem['name']?>" id="artist_name" name="artist_name" maxlength="100" size="50"  >&nbsp;
            <?php echo showErrorMessage("artist_name","name",$m_parentControl->m_hasValidationErrors, false) ?>
            <p>Name of artist. Required and must be unique in system.</p>
        </div>

        <div class="form-artist_song-field form-require term-ordering-wrap <?php echo showErrorMessage("artist_ordering","ordering",$m_parentControl->m_hasValidationErrors, true) ?>">
            <label for="artist_ordering"><?php echo esc_html__("Ordering")?></label>
            <input type="text" aria-required="true" value="<?php echo $m_parentControl->m_artistEditorItem['ordering']?>" id="artist_ordering" name="artist_ordering" maxlength="8" size="8" style=" text-align: right;" >&nbsp;
            <?php echo showErrorMessage("artist_ordering","ordering",$m_parentControl->m_hasValidationErrors, false) ?>
            <p>Ordering of artist. Required and must be valid integer.</p>
        </div>

        <div class="form-artist_song-field form-require term-active-wrap <?php echo showErrorMessage("artist_is_active","is_active",$m_parentControl->m_hasValidationErrors, true) ?>">
            <label for="artist_is_active"><?php echo esc_html__("Active"); ?></label>
            <select class="postform" aria-required="true" id="artist_is_active" name="artist_is_active">
                <?php foreach( $m_parentControl->m_artistsIsActiveValueArray as $key=>$nextItem ) { ?>
                    <option value="<?php echo $nextItem['key'] ?>" <?php echo ($nextItem['key'] == $m_parentControl->m_artistEditorItem['is_active'] ? " selected " : "" ) ?>  class="level-0" ><?php echo $nextItem['value'] ?></option>
                <?php }  ?>
            </select>&nbsp;
            <?php echo showErrorMessage("artist_is_active","is_active",$m_parentControl->m_hasValidationErrors, false) ?>
            <p>Select if artist is active.</p>
        </div>


        <div class="form-artist_song-field form-require term-email-wrap <?php echo showErrorMessage("artist_email","email",$m_parentControl->m_hasValidationErrors, true) ?>">
            <label for="artist_email"><?php echo esc_html__("Email")?></label>
            <input type="text" aria-required="true" value="<?php echo $m_parentControl->m_artistEditorItem['email']?>" id="artist_email" name="artist_email"  maxlength="50" size="50"  >&nbsp;
            <?php echo showErrorMessage("artist_email","email",$m_parentControl->m_hasValidationErrors, false) ?>
            <p>Email of artist. Required and must be unique in system.</p>
        </div>

        <div class="form-artist_song-field form-require term-site-wrap <?php echo showErrorMessage("artist_site","site",$m_parentControl->m_hasValidationErrors, true) ?>" >
            <label for="artist_site" ><?php echo esc_html__("Site")?></label>
            <input type="text" aria-required="true" value="<?php echo $m_parentControl->m_artistEditorItem['site']?>" id="artist_site" name="artist_site" maxlength="50" size="50" >&nbsp;
            <?php echo showErrorMessage("artist_site","site",$m_parentControl->m_hasValidationErrors, false) ?>
            <p>Site of artist. Required and must be valid url, like "http(s)://site.com".</p>
        </div>


        <div class="form-artist_song-field form-require term-birthday-wrap <?php echo showErrorMessage("artist_birthday","birthday",$m_parentControl->m_hasValidationErrors, true) ?>">
            <label for="artist_birthday"><?php echo esc_html__("Birthday")?></label>
            <input type="text" aria-required="true" value="<?php echo $m_parentControl->m_artistEditorItem['birthday']?>" id="artist_birthday" name="artist_birthday"  maxlength="10" size="10" >&nbsp;
            <?php echo showErrorMessage("artist_birthday","birthday",$m_parentControl->m_hasValidationErrors, false) ?>
            <p>Birthday of artist. Required and selectable from popup.</p>
        </div>


        <div class="form-artist_song-field form-require term-registered_at-wrap <?php echo showErrorMessage("artist_registered_at","registered_at",$m_parentControl->m_hasValidationErrors, true) ?>">
            <label for="artist_registered_at"><?php echo esc_html__("Registered At")?>&nbsp;<?php echo $m_parentControl->m_artistEditorItem['registered_at']?>&nbsp;</label>
            <input type="text"  data-format="yyyy-MM-dd hh:mm" aria-required="true"  value="<?php echo $m_parentControl->m_artistEditorItem['registered_at']?>" id="artist_registered_at" name="artist_registered_at"  maxlength="50" size="20" >&nbsp;
            <?php echo showErrorMessage("artist_registered_at","registered_at",$m_parentControl->m_hasValidationErrors, false) ?>
            <p>Registered date of artist. Required and selectable from popup.</p>
        </div>

        <div class="form-artist_song-field form-require term-info-wrap <?php echo showErrorMessage("artist_info","info",$m_parentControl->m_hasValidationErrors, true) ?>">
            <label for="artist_info"><?php echo esc_html__("Info")?></label>
            <textarea  aria-required="true" id="artist_info" name="artist_info" rows="8" cols="80"><?php echo trim(stripslashes($m_parentControl->m_artistEditorItem['info'])) ?></textarea>&nbsp;
            <?php echo showErrorMessage("artist_info","info",$m_parentControl->m_hasValidationErrors, false) ?>
            <p>Info of artist. Required.</p>
        </div>


        <div class="form-artist_song-field form-require term-info-wrap <?php echo showErrorMessage("artist_image","artist_image",$m_parentControl->m_hasValidationErrors, true) ?>">
            <label for="artist_image"><?php echo esc_html__("Cover Image")?></label>
            <input type="file" id="artist_image" name="artist_image" value="" size="50" maxlength="255" class="browser button ">&nbsp;
            <?php $artist_image_fileurl= $m_parentControl->m_artistsTblSource->getImage($m_parentControl->m_artistEditorItem['ID'], '', true);
            $artist_image_filepath= $m_parentControl->m_artistsTblSource->getImage($m_parentControl->m_artistEditorItem['ID'], '', false);
            if((int)$m_parentControl->m_is_insert != 1 and file_exists($artist_image_filepath)) :
                $filesizeLabel = esc_html__(nsnClass_appFuncs::getFileSizeAsString( filesize($artist_image_filepath) ));
                $filenameInfo = nsnClass_appFuncs::getImageShowSize($artist_image_fileurl, $thumbnailWidth, $thumbnailHeight);
                if (!empty($artist_image_fileurl)) : ?>
                    <a class="fancybox-button" rel="fancybox-button" alt="description"   href="<?php echo $artist_image_fileurl ?>" >
                        <img src="<?php echo $artist_image_fileurl ?>" width="<?php echo $filenameInfo['Width']?>" height="<?php echo $filenameInfo['Height']?>" /></a>&nbsp;
                    <?php echo $filesizeLabel ?>, <?php echo $filenameInfo['OriginalWidth']?>*<?php echo $filenameInfo['OriginalHeight']?>
                <?php endif; ?>
            <?php endif; ?>
            <?php echo showErrorMessage("artist_image","artist_image",$m_parentControl->m_hasValidationErrors, false) ?>
            <p>Cover Image of artist. Required.</p>
        </div>


        <div class="form-artist_song-field term-updated-wrap" style="<?php echo ((int)$m_parentControl->m_is_insert == 1) ? "display:none" : "" ?>" >
            <label for="artist_updated"><?php echo esc_html__("Updated")?></label>
            <input type="text" size="20" value="<?php echo viewFuncs::formattedDateTime($m_parentControl->m_artistEditorItem['updated'], 'Common')?>" id="artist_updated" name="artist_updated" style="border: 1px dotted #808080;" readonly>
            <p>Time of last updating if Artist. Is not editable.</p>
        </div>

        <div class="form-artist_song-field term-created-wrap" style="<?php echo ((int)$m_parentControl->m_is_insert == 1) ? "display:none" : "" ?>" >
            <label for="artist_created"><?php echo esc_html__("Created")?></label>
            <input type="text" size="20" value="<?php echo viewFuncs::formattedDateTime($m_parentControl->m_artistEditorItem['created'], 'Common') ?>" id="artist_created" name="artist_created" style="border: 1px dotted #808080;" readonly>
            <p>Time of creation of Artist. Is not editable.</p>
        </div>


        <?php if ( !empty($m_parentControl->m_relatedArtistPost) ) :  ?>
            <a href="/wp-admin/post.php?post=<?php echo $m_parentControl->m_relatedArtistPost->ID ?>&action=edit" >Related Post Editor:&nbsp;"<?php echo $m_parentControl->m_relatedArtistPost->post_title  ?>"</a><br>
            <a href="<?php echo get_permalink($m_parentControl->m_relatedArtistPost->ID )  ?>" >Related Post Preview:&nbsp;"<?php echo $m_parentControl->m_relatedArtistPost->post_title  ?>"</a><br>
        <?php endif;   ?>

        <div>
            <p class="submit">
                <button type="submit" class="button button-primary" onclick="javascript:backendArtistsEditorFuncs.onSubmit(1);"><?php echo esc_html__($m_parentControl->m_action=="add"?"Add":"Update") ?> & Reopen</button>&nbsp;&nbsp;
                <button type="submit" class="button button-primary" onclick="javascript:backendArtistsEditorFuncs.onSubmit(0);"><?php echo esc_html__($m_parentControl->m_action=="add"?"Add":"Update") ?></button>&nbsp;&nbsp;
                <button type="reset" class="button action" onclick="javascript:document.location='<?php echo $m_parentControl->m_pageUrl; ?>&action=list<?php echo $m_parentControl->m_pageParametersWithSort?>'"><?php echo esc_html__("Cancel"); ?></button>&nbsp;&nbsp;
            </p>
        </div>


        <div style="<?php echo ($m_parentControl->m_is_insert == 1) ? "display:none" : "" ?>" >
            <div id="tabs">
                <ul>
                    <li><a href="#tabs-artist-reviews">Reviews<span id="span-artist-reviews-title"></span></a></li>
                    <li><a href="#tabs-artist-songs">Songs<span id="span-artist-songs-title"></span></a></li>
                    <li><a href="#tabs-artist-photos">Photos Gallery<span id="span-artist-photos-title"></a></li>

                    <?php if (0/* $m_parentControl->m_is_insert != 1 and $m_parentControl->m_artistEditorItem['has_concerts']=='Y'*/) : ?>
                    <li><a href="#tabs-artist-concerts">Concerts<span id="span-artist-concerts-title"></a></li>
                    <?php endif?>

                    <li><a href="#tabs-artist-tours">Tours<span id="span-artist-tours-title"></a></li>
                </ul>
                <div id="tabs-artist-reviews">
                    <div id="div-artist-reviews"></div>
                </div>
                <div id="tabs-artist-songs">
                    <div id="div-artist-songs"></div>
                </div>
                <div id="tabs-artist-photos">


                <span >
                    <div id="div_upload_photo">
                        Upload your photo:&nbsp;
                        <span>Select file</span>
                        <input id="fileupload"  type="file" class="artist_photo_fileupload" name="files[]" multiple>
                    </div >
                </span>


                    <div id="div_save_upload_photo" style="display: none">
                        <table style="border: 0px dotted green;">
                            <tr>
                                <td rowspan="3">
                                    <img id="img_preview_image" alt="Preview" title="Preview" width="128" height="128" style="border: 0px dotted red;">
                                    <input type="hidden" id="hidden_selected_photo">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="1">&nbsp;&nbsp;
                                    <input id="input_photo_description" type="text" maxlength="120" size="40" placeholder="Enter photo description here">
                                </td>
                            </tr>
                            <tr>
                                <td >&nbsp;&nbsp;
                                    <button type="button" class="btn btn-default" data-dismiss="modal" onclick="javascript:backendArtistsEditorFuncs.UploadPhoto();"><i></i>Upload Photo</button>&nbsp;&nbsp;&nbsp;&nbsp;
                                    <button type="button" class="btn btn-default" data-dismiss="modal" onclick="javascript:backendArtistsEditorFuncs.CancelUploadPhoto();"><i></i>Cancel</button>
                                </td>
                            </tr>
                        </table>
                    </div>

                    <div id="div-artist-photos"></div>

                </div>
                <div id="tabs-artist-concerts">
                    Concerts
                </div>
                <div id="tabs-artist-tours">
                    tours content
                </div>


            </div>
        </div>

        </table>
    </form>


    <div id="artist_review_dialog" title="Artist's Review" style="display: none;">
        <h4>
            You are <span id="span_artist_review_mode_label"></span>ing review for artist <span id="span_artist_review_artist_name"></span>
        </h4>
        <input type="hidden" id="artist_review_ID" name="artist_review_ID" value="">

        <div class="form-artist_song-field form-require term-artist_review_user_id-wrap" >
            <label for="artist_review_user_id"><?php echo esc_html__("User"); ?></label>
            <select class="postform" aria-required="true" id="artist_review_user_id" name="artist_review_user_id">
                <?php foreach( $m_parentControl->m_usersSelectionList as $key=>$nextUser ) { ?>
                    <option value="<?php echo $nextUser['key'] ?>" class="level-0" > <?php echo $nextUser['value'] ?></option>
                <?php }  ?>
            </select>
            <p>Select user of Review Text. Required.</p>
        </div>


        <div>&nbsp;</div>


        <div class="form-artist_song-field form-require term-review_text-wrap ">
            <label for="artist_review_review_text"><?php echo esc_html__("Review Text")?></label>
            <textarea  aria-required="true" id="artist_review_review_text" name="artist_review_review_text" rows="12" cols="80"></textarea>
            <p>Review Text of artist. Required.</p>
        </div>



        <div class="form-artist_song-field form-require term-review_has_inappropriates-wrap " style="display: none" id="div_review_has_inappropriates">
            <label for="artist_review_review_has_inappropriates"><?php echo esc_html__("Review is marked as inappropriate.")?></label>
            <select class="postform" aria-required="true" id="artist_review_has_inappropriates" name="artist_review_has_inappropriates">
                <?php foreach( $m_parentControl->reviewInappropriatesOperationsArray as $key=>$nextItem ) { ?>
                    <option value="<?php echo $nextItem['key'] ?>" class="level-0" ><?php echo $nextItem['value'] ?></option>
                <?php }  ?>
            </select>&nbsp;
            <img align="absmiddle" src="<?php echo $plugin_url ?>images/is_inappropriate.png" alt="Is Inappropriate" title="Is Inappropriate">
            <p>Clear inappropriate mark or delete this review.</p>
        </div>




        <div  id="div_artist_review_created" class="form-artist_song-field term-created-wrap" >
            <label for="artist_review_created"><?php echo esc_html__("Created")?></label>
            <input type="text" size="20" value="<?php echo viewFuncs::formattedDateTime($m_parentControl->m_artistEditorItem['created'], 'Common') ?>" id="artist_review_created" name="artist_created" style="border: 1px dotted #808080;" readonly>
            <p>Time of creation of Artist' Review. Is not editable.</p>
        </div>


    </div>

    <div id="artist_song_dialog" title="Artist's Song" style="display: none;min-width: 680px;" class="form-wrap">
        <h4>
            You are <span id="span_artist_song_mode_label"></span>ing song for artist <span id="span_artist_review_artist_name"></span>
        </h4>
        <input type="hidden" id="artist_song_ID" name="artist_song_ID" value="">

        <div class="form-artist_song-field form-require term-artist_song_song_id-wrap" id="div_artist_song_song_id" >
            <label for="artist_song_song_id"><?php echo esc_html__("Song"); ?></label>


            <div id="div_selectExistingSong">
                <select data-placeholder="Select Region" style="width:350px;" class="chosen-select"  id="artist_song_song_id">
                    <option value=""></option> ';

                    <?php echo '<pre>$m_parentControl->m_songsGroupedList::'.print_r($m_parentControl->m_songsGroupedList,true).'</pre>';
                    foreach ( $m_parentControl->m_songsGroupedList as $nextSongGrouped) : ?>
                    <optgroup label="<?php echo $nextSongGrouped['jenre_name'] ?> " >
                        <?php if ( !empty($nextSongGrouped['songsSubArray']) ) { ?>
                        <?php foreach ($nextSongGrouped['songsSubArray'] as $song_id => $song_name) : ?>
                            <option value="<?php echo $song_id ?>" ><?php echo $song_name ?></option>
                        <?php endforeach; ?>
                        <?php } ?>
                        <?php endforeach; ?>
                </select>&nbsp;<button type="submit" class="button button-primary" onclick="javascript:backendArtistsEditorFuncs.inArtistAddNewSong();">Add New Song</button>
            </div>
            <div id="div_inArtistAddNewSong" style="display: none">
                <?php require_once( $this->m_pluginDir . 'views/admin/in_artist_add_new_song.php' ); ?>
            </div>

        </div>

        <div  id="div_artist_song_created" class="form-artist_song-field term-created-wrap" >
            <label for="artist_song_created"><?php echo esc_html__("Created")?></label>
            <input type="text" size="20" value="<?php echo viewFuncs::formattedDateTime($m_parentControl->m_artistEditorItem['created'], 'Common') ?>" id="artist_song_created" name="artist_created" style="border: 1px dotted #808080;" readonly>
            <p>Time of creation of Artist' Song. Is not editable.</p>
        </div>




    </div>


</div> <!-- div class="form-editor-wrap" -->