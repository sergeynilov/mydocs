<?php
if (!defined('ArtistsSongs')) {
    exit; // Exit if accessed directly
}
$languagesList=  nsnClass_appLangs::getLanguagesList('',true);
?>

<script type="text/javascript" language="JavaScript">
    /*<![CDATA[*/

    var backendSongsEditorFuncs = new backendSongsEditorFuncs( { // must be called befOre jQuery(document).ready(function ($) {
        <?php echo nsnClass_appFuncs::prepareParams( true,  array('plugin_url'=> $plugin_url, 'm_pageUrl'=> $m_pageUrl ) )?>
    } );

    /*]]>*/
</script>

<h4><?php echo esc_html__("Songs list");  ?></h4>

<form  class="search-form" action="<?php echo $m_parentControl->m_pageUrl; ?>action=list<?php echo $m_parentControl->m_pageParametersWithSort ?>" method="post" accept-charset="utf-8" id="form_songs" name="form_songs" enctype="multipart/form-data">


    <input type="hidden" name="<?php echo $m_parentControl->m_labelNonceInput ?>" value="<?php echo wp_create_nonce($m_parentControl->m_labelNonce) ?>" />
    <input type="hidden" id="paged" name="paged" value="<?php echo $m_parentControl->m_paged ?>">
    <input type="hidden" id="sort" name="sort" value="<?php echo $m_parentControl->m_sort ?>">
    <input type="hidden" id="sort_direction" name="sort_direction" value="<?php echo $m_parentControl->m_sort_direction ?>">
    <input type="hidden" id="filter_cbx_song_jenre_" name="filter_cbx_song_jenre_" value="<?php echo $m_parentControl->m_filter_cbx_song_jenre_ ?>">

    <p class="search-box">
    <table  style="border: 1px dotted gray; min-width: 480px;" >
        <tr>
            <td width="60%" colspan="2"><?php echo esc_html__("Filter Setup") ; ?></td>
            <td width="40%">
                <?php if ($m_parentControl->m_navigationHTML!= '') : ?>
                <?php echo ( $m_parentControl->m_itemsPerPage > $m_parentControl->m_songsCount ? $m_parentControl->m_songsCount : $m_parentControl->m_itemsPerPage ); ?> <?php echo esc_html__("Rows of");?> <?php echo $m_parentControl->m_songsCount; ?> (<?php echo esc_html__("Page") ?> # <?php echo $m_parentControl->m_paged ?>)
                <?php endif; ?>
            </td>
        </tr>


        <tr>
            <td width="30%">
                <input type="text" value="<?php echo $m_parentControl->m_filter_title ?>" id="filter_title" name="filter_title" size="20" maxlength="100" align="" placeholder="<?php echo esc_html__("Song Title Filter") ?>">
            </td>
            <td width="30%" rowspan="2">
                <table>
                    <?php
                    foreach ($m_parentControl->m_songJenresList as $nextMusicJenre) : ?>
                        <tr>
                            <td>
                                <?php echo esc_html( nsnClass_appFuncs::getLSepr( $nextMusicJenre['music_jenre_name'], '=' )) ?>&nbsp;:
                            </td>
                            <td>
                                <input type="checkbox" value="<?php echo $nextMusicJenre['music_jenre_id'] ?>"
                                       id="filter_cbx_song_jenre_<?php echo $nextMusicJenre['music_jenre_id'] ?>"
                                       name="filter_cbx_song_jenre_<?php echo $nextMusicJenre['music_jenre_id'] ?>" <?php echo $nextMusicJenre['music_jenre_is_checked'] ? "checked" : "" ?>
                                       class="cbx_song_jenres_selection_filters">
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </td>
        </tr>

        <tr>
            <td width="30%">
                <select id="filter_is_active" name="filter_is_active">
                    <?php foreach ($m_parentControl->m_songsIsActiveValueArray as $key => $nextItem) { ?>
                        <option
                            value="<?php echo $nextItem['key'] ?>" <?php echo($nextItem['key'] == $m_parentControl->m_filter_is_active ? " selected " : "") ?> ><?php echo $nextItem['value'] ?></option>
                    <?php } ?>
                </select>
            </td>
            <td width="40%">
                <input type="button" value="<?php echo esc_html__("Filter") ?>" onclick="javascript:backendSongsEditorFuncs.onRunFilter()">
            </td>
        </tr>

        <tr>
            <td colspan="3">
                <label for="bulk-action-selector-top" class="screen-reader-text">Select bulk action</label>
                <select name="action" id="bulk-action-selector-top">
                    <option value=""><?php echo esc_html__("Bulk Actions") ?></option>
                    <option value="bulk_active"><?php echo esc_html__("Set to active") ?></option>
                    <option value="bulk_inactive"><?php echo esc_html__("Set to inactive") ?></option>
                    <option value="bulk_delete" class="hide-if-no-js"><?php echo esc_html__("Delete") ?></option>
                </select>
                <input name="doaction" id="doaction" class="button action" value="<?php echo esc_html__("Apply") ?>" onclick="javascript:backendSongsEditorFuncs.onBulkApply('<?php echo esc_html__('Select bulk operation')?>', '<?php echo esc_html__("For bulk operations at least 1 item must be selected!")?>', '<?php echo esc_html__("Do you want to") ?>', '<?php echo esc_html__("all selected items") ?>')" type="button">
                <input type="hidden" id="bulk_operation_title" name="bulk_operation_title" >
                <input type="hidden" id="bulk_action" name="bulk_action" >
            </td>
        </tr>

    </table>
    </p>     <!-- p class="search-box" -->


    <table style="border: 1px solid gray;"  class="wp-list-table widefat fixed tags" >


        <?php if ($m_parentControl->m_songsCount == 0) : ?>
        <thead>
        <tr>
            <td colspan="4">
                <div><b><?php echo esc_html__("No Songs Found!") ?></b></div>
                <div>
                    <a class="button" href="<?php echo $m_parentControl->m_pageUrl; ?>action=add<?php echo $m_parentControl->m_pageParametersWithSort ?>" style="cursor: pointer"><?php echo esc_html__("Add Song") ?> </a>
                </div>
            </td>
        </tr>
        </thead>

    </table>
</form>
            <?php return; ?>
        <?php endif; ?>

<thead>
<tr>

    <th style="" class="manage-column column-cb check-column" id="cb" scope="col">
        <label for="cb-select-all-1" class="screen-reader-text">
            Select All
        </label>
        <input type="checkbox" id="cb-select-all-1">
    </th>


    <th><?php echo esc_html__("ID") ?></th>

    <th>
        <?php echo viewFuncs::showListHeaderItem(array('url' => $m_parentControl->m_pageUrl . 'action=list&', 'filters_str' => $m_parentControl->m_pageParametersWithoutSort, 'fieldname' => "title", 'field_title' => esc_html__("Title"), 'sort' => $m_parentControl->m_sort, 'sort_direction' => $m_parentControl->m_sort_direction)); ?>&nbsp;
        <img src="<?php echo nsnClass_appLangs::getDefaultLanguage('flag_url')?>" alt="<?php echo nsnClass_appLangs::getDefaultLanguage()?>" name="<?php echo nsnClass_appLangs::getDefaultLanguage()?>">
    </th>

    <?php if ( !empty($languagesList) ) { ?>
        <?php
        foreach( $languagesList as $nextLang ) {
            if ( $nextLang['slug']!=nsnClass_appLangs::getDefaultLanguage() ) { ?>
                <th><img src="<?php echo $nextLang['flag_url']?>" alt="<?php echo $nextLang['name']?>" title="<?php echo $nextLang['name']?>">
                </th>
            <?php } ?>
        <?php } ?>
    <?php } ?>

    <th><?php echo viewFuncs::showListHeaderItem(array('url' => $m_parentControl->m_pageUrl . 'action=list&', 'filters_str' => $m_parentControl->m_pageParametersWithoutSort, 'fieldname' => "ordering", 'field_title' => esc_html__("Ordering"), 'sort' => $m_parentControl->m_sort, 'sort_direction' => $m_parentControl->m_sort_direction)); ?> </th>

    <th><?php echo viewFuncs::showListHeaderItem(array('url' => $m_parentControl->m_pageUrl . 'action=list&', 'filters_str' => $m_parentControl->m_pageParametersWithoutSort, 'fieldname' => "is_active", 'field_title' => esc_html__("Active"), 'sort' => $m_parentControl->m_sort, 'sort_direction' => $m_parentControl->m_sort_direction)); ?> </th>

    <th><?php echo esc_html__("Jenres"); ?> </th>

    <th><?php echo viewFuncs::showListHeaderItem(array('url' => $m_parentControl->m_pageUrl . 'action=list&', 'filters_str' => $m_parentControl->m_pageParametersWithoutSort, 'fieldname' => "created", 'field_title' => esc_html__("Created"), 'sort' => $m_parentControl->m_sort, 'sort_direction' => $m_parentControl->m_sort_direction)); ?> </th>

</tr>
</thead>



<?php $row = 0;
foreach ($m_parentControl->m_songsList as $key => $nextSong) : $row++; ?>
    <tr class="<?php echo($row % 2 == 0 ? "data_row_even" : "data_row_odd") ?>">

        <th class="check-column" scope="row">
            <label for="cb-select-<?php echo $nextSong['ID'] ?>" class="screen-reader-text">
                Select <?php echo esc_html($nextSong['title']) ?>
            </label>
            <input type="checkbox" class="song-bulk-actions" id="cb-select-<?php echo $nextSong['ID'] ?>"   value="<?php echo $nextSong['ID'] ?>" name="cb-select-<?php echo $nextSong['ID'] ?>">
        </th>

        <td>
            <?php echo $nextSong['ID'] ?>
        </td>

        <td class="name column-name">
            <a title="Edit " href="<?php echo $m_parentControl->m_pageUrl; ?>action=edit&song_id=<?php echo $nextSong['ID'] . $m_parentControl->m_pageParametersWithSort ?>" class="row-title">
                <?php echo esc_html($nextSong['title']) ?>
            </a>&nbsp;&nbsp;
                <?php $relatedSongsArtistsCount  = $m_parentControl->m_songsArtistsTblSource->getSongArtistsList(true, '', array('song_id' => $nextSong['ID']));
                if ($relatedSongsArtistsCount > 0) : ?>
                    <small>(<?php echo esc_html__("Used for") . " " . $relatedSongsArtistsCount . " " . esc_html__("artist(s)"); ?>)</small>
                <?php endif; ?>
            <br>
            <div class="row-actions">
                <span class="edit">
                    <a href="<?php echo $m_parentControl->m_pageUrl; ?>action=edit&song_id=<?php echo $nextSong['ID'] . $m_parentControl->m_pageParametersWithSort ?>" style="cursor: pointer">
                        <?php echo esc_html__("Edit") ?>
                    </a> |
                </span>

                <span class="delete">
                    <?php // if ($relatedSongsArtistsCount == 0) : ?>
                        <a onclick="javascript:backendSongsEditorFuncs.onDeleteSong('<?php echo $nextSong['ID'] . $m_parentControl->m_pageParametersWithSort ?>', '<?php echo esc_html(addslashes($nextSong['title'])) ?>', '<?php echo esc_html__("Do you want to delete")?>', '<?php echo esc_html__("Song with all related data")?>' )"  style="cursor: pointer"> <?php echo esc_html__("Delete") ?>
                        </a>

                    <?php // endif; ?>&nbsp;
                </span>

                <span class="view">
                    <a href="http://local-wp-songs.com/tag/blog-of-admin/">
                        View???
                    </a>
                </span>
            </div>
        </td>

        <?php if ( !empty($languagesList) ) { ?>
            <?php $languagesList=  nsnClass_appLangs::getLanguagesList('',true);
            foreach( $languagesList as $nextLang ) { ?>
                <?php if ( $nextLang['slug']!=nsnClass_appLangs::getDefaultLanguage() ) { ?>
                <td>
                    <?php echo $m_parentControl->m_songsTblSource->getSongByLang( $nextSong['ID'], $nextLang['slug'], 'title' ); ?>
                </td>
                <?php } ?>
            <?php } ?>
        <?php } ?>


        <td>
            <?php echo $nextSong['ordering']; ?>
        </td>
        <td>
            <?php echo $m_parentControl->m_songsTblSource->getSongsIsActiveValueLabel($nextSong['is_active']); ?>
        </td>


        <td>
            <?php $m_parentControl->m_songJenresList = $m_parentControl->m_songsJenresTblSource->getSongJenresList(false, '', $filters = array('song_id' => $nextSong['ID']), '', '', true);
            $S = '';
            foreach ($m_parentControl->m_songJenresList as $nextSongJenre) {
                if ($nextSongJenre['music_jenre_is_checked']) {
                    $S .= esc_html( nsnClass_appFuncs::getLSepr($nextSongJenre['music_jenre_name'], '=' ) ) . ', ';
                }
            }
            echo nsnClass_appFuncs::clearLastChar($S, ',');
            ?>
        </td>

        <td>
            <?php echo viewFuncs::formattedDateTime($nextSong['created'], 'Common') ?>
        </td>

    </tr>
<?php endforeach ?>

    <tr>
        <td colspan="4">
            <?php echo $m_parentControl->m_navigationHTML; ?>
        </td>
    </tr>


    <tr>
        <td colspan="4">
            <a class="button" href="<?php echo $m_parentControl->m_pageUrl; ?>action=add<?php echo $m_parentControl->m_pageParametersWithSort ?>" style="cursor: pointer"><?php echo esc_html__("Add Song") ?> </a>
        </td>
    </tr>

</table>
</form>