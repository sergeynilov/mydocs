var this_m_lang = ''
var this_plugin_url = ''
var this_logged_user_id = ''
var this_m_pageUrl = ''
var this_m_songsPageUrl = ''
var this_m_artist_id = ''
var this_m_artist_registered_at = ''
var this_m_is_insert = false
var this_datePickerSelectionFormat = ''
var inArtistAddNewSong= false
var map
function backendArtistsEditorFuncs(Params) { // constructor of backend artist's editor - set all params from server
     //alert( "ajax_object.backend_ajaxurl::"+var_dump(ajax_object.backend_ajaxurl) )
    this_m_lang = Params.lang;
    this_plugin_url = Params.plugin_url;
    this_logged_user_id = Params.logged_user_id;
    this_m_pageUrl = Params.m_pageUrl;
    this_m_songsPageUrl = Params.m_songsPageUrl;
    this_m_artist_id = Params.m_artist_id;
    this_m_artist_registered_at = Params.m_artist_registered_at;
    this_m_is_insert = Params.m_is_insert;
    this_datePickerSelectionFormat = Params.datePickerSelectionFormat;
} // function backendArtistsEditorFuncs(Params) { // constructor of backend artist's editor - set all params from server


/*   ARTIST EDITOR START   */
backendArtistsEditorFuncs.prototype.onSubmit = function (isReopen) { // submit form.If isReopen= 1 - editor will be reopened.
    var theForm = $("#form_artist_editor");
    $("#is_reopen").val(isReopen)
    theForm.submit();

} // backendArtistsEditorFuncs.prototype.onSubmit= function(isReopen) {  // submit form.If isReopen= 1 - editor will be reopened.

backendArtistsEditorFuncs.prototype.onEditorInit = function () { // Init Editor Editor - all query objects init and load sublistings
    //alert("onEditorInit::")
    $(".form-invalid").bind('change keypress', function () {
        $(this).closest("div").removeClass("form-invalid");
    });

    $(".fancybox-button").fancybox({  // http://pishemsite.ru/jquery/podklyuchenie-i-nastroyka-fancybox#
        prevEffect: 'none',
        nextEffect: 'none',
        closeBtn: true,
        openEffect: 'none',
        closeEffect: 'none',
        'titlePosition': 'over',
        helpers: {
            title: {type: 'inside'},
            buttons: {}
        }
    });

    jQuery("#artist_birthday").datepicker({ // http://docs.jquery.com/UI/API/1.8/Datepicker#options
        dateFormat: this_datePickerSelectionFormat,
        minDate: new Date(1920, 1, 1),
        maxDate: new Date(2028, 12, 31)
    });
    $("#artist_birthday").datepicker("option", "changeMonth", true);
    $("#artist_birthday").datepicker("option", "changeYear", true);
    $("#artist_birthday").datepicker("option", "closeText", "Close");
    $("#artist_birthday").datepicker("option", "constrainInput", true);

    jQuery('#artist_registered_at').datetimepicker({ //  http://xdsoft.net/jqplugins/datetimepicker/
        //format: "Y-m-d h:i",
        step: 5,
        animation: true,
        "firstDayOfWeek": 0,
        closeOnDateSelect: false,
        closeOnTimeSelect: true
    });

    $("#artist_registered_at").val(this_m_artist_registered_at);
    if (this_m_is_insert != '1') {
        backendArtistsEditorFuncs.loadArtistReviews(this_m_artist_id, 1);
        backendArtistsEditorFuncs.loadSongsArtists(this_m_artist_id, 1);
        backendArtistsEditorFuncs.loadArtistPhotos(this_m_artist_id, 1)
    }

    $("#tabs").tabs();


    CKEDITOR.replace('artist_info', {  // http://docs.ckeditor.com/#!/guide/dev_howtos_interface
        allowedContent: true,
        height: 380,
        width: 800,
        on: {
            pluginsLoaded: function (evt) {
                var doc = CKEDITOR.document, ed = evt.editor;
                if (!ed.getCommand('bold'))
                    doc.getById('exec-bold').hide();
                if (!ed.getCommand('link'))
                    doc.getById('exec-link').hide();
            }
        }
    });

} // backendArtistsEditorFuncs.prototype.onEditorInit= function() {  // Init Editor Editor - all query objects init and load sublistings
/*   ARTIST EDITOR END   */


/*   ARTIST LIST START   */
backendArtistsEditorFuncs.prototype.onArtistsListInit = function () { // Artist's Listing Page Init
    jQuery("#filter_registered_at_1").datepicker({
        dateFormat: this_datePickerSelectionFormat,
        minDate: new Date(2012, 1, 1),
        maxDate: new Date(2028, 12, 31)
    });  // $( ".selector" ).datepicker( "option", "changeMonth", true );
    $("#filter_registered_at_1").datepicker("option", "changeMonth", true);
    $("#filter_registered_at_1").datepicker("option", "changeYear", true);
    $("#filter_registered_at_1").datepicker("option", "closeText", "Close");
    $("#filter_registered_at_1").datepicker("option", "constrainInput", true);

    jQuery("#filter_registered_at_2").datepicker({ // http://docs.jquery.com/UI/API/1.8/Datepicker#options
        dateFormat: this_datePickerSelectionFormat,
        minDate: new Date(2012, 1, 1),
        maxDate: new Date(2028, 12, 31)
    });  // $( ".selector" ).datepicker( "option", "changeMonth", true );
    $("#filter_registered_at_2").datepicker("option", "changeMonth", true);
    $("#filter_registered_at_2").datepicker("option", "changeYear", true);
    $("#filter_registered_at_2").datepicker("option", "closeText", "Close");
    $("#filter_registered_at_2").datepicker("option", "constrainInput", true);

} // backendArtistsEditorFuncs.prototype.onArtistsListInit = function () {  // Artist's Listing Page Init

backendArtistsEditorFuncs.prototype.deleteArtistsListArtist = function (id, artist_name, Do_you_want_to_delete_label, Artis_with_all_related_data_label) {
    if (!confirm(Do_you_want_to_delete_label + " '" + artist_name + "' " + Artis_with_all_related_data_label + " ?")) return;
    document.location = this_m_pageUrl + "action=confirm_delete&artist_id=" + id
} //   backendArtistsEditorFuncs.prototype.deleteArtistsListArtist = function (id, artist_name, Do_you_want_to_delete_label, Artis_with_all_related_data_label) {


backendArtistsEditorFuncs.prototype.runArtistsListFilter = function () { // run artist's listing filter with page = 1
    $("#bulk_action").val('')
    $("#paged").val(1)
    var theForm = document.getElementById("form_artists");
    theForm.submit();
} // backendArtistsEditorFuncs.prototype.runArtistsListFilter = function () {  // run artist's listing filter with page = 1

/*   ARTIST LIST END   */

/* ARTIST REVIEWS START BLOCK */
backendArtistsEditorFuncs.prototype.loadArtistReviews = function (artist_id, paged) {
    if (typeof paged == "undefined") paged = 1
    $.post(ajax_object.backend_ajaxurl, {
            action: 'get-artist-reviews',
            lang : this_m_lang,
            artist_id: artist_id,
            paged: paged,
            plugin_url : this_plugin_url,
            show_only_text: 0
        },
        function (data) {
            //alert("data::" + var_dump(data))
            if (data.error_code == 0) {
                $("#div-artist-reviews").html(data.content);
                if (data.reviewsCount > 0) {
                    $("#span-artist-reviews-title").html('(' + data.reviewsCount + ')');
                } else {
                    $("#span-artist-reviews-title").html('');
                }
            } else {
                alert(data.error_message)
            }
        }, "json");
} // backendArtistsEditorFuncs.prototype.loadArtistReviews = function () {

backendArtistsEditorFuncs.prototype.editArtistReviewInPopupEditor = function (isInsert, artist_review_id, artistReview) {
    if (isInsert) {
        $("#span_artist_review_mode_label").html( "add" )
        $("#artist_review_user_id").val("")
        $("#artist_review_review_text").val("")
        $("#div_artist_review_created").css("display", "none")
    } else {
        $("#div_artist_review_created").css("display", "block")
        $("#span_artist_review_mode_label").html( "edit" )
        if (parseInt(artistReview.reviewsInappropriatesCount)> 0) {
            $("#div_review_has_inappropriates").css("display", "block")
        } else {
            $("#div_review_has_inappropriates").css("display", "none")
        }
    }

    $("#artist_review_dialog").dialog({
        buttons: {
            "Submit": function () {
                var artist_review_user_id = $("#artist_review_user_id").val()
                if (artist_review_user_id == "") {
                    alert("Select user !")
                    $('#artist_review_user_id').focus();
                    return;
                }

                var artist_review_review_text = $("#artist_review_review_text").val()
                if (artist_review_review_text == "") {
                    alert("Enter Review Text !")
                    $('#artist_review_review_text').focus();
                    return;
                }
                var artist_review_has_inappropriates = $("#artist_review_has_inappropriates").val()

                $.ajax({
                    url: ajax_object.backend_ajaxurl,
                    dataType: "json",
                    type: 'post',
                    data: {
                        action: 'update-artist-review',
                        lang : this_m_lang,
                        artist_review_id: artist_review_id,
                        artist_id: this_m_artist_id,
                        user_id: artist_review_user_id,
                        review_text: artist_review_review_text,
                        artist_review_has_inappropriates: artist_review_has_inappropriates
                    },
                    success: function (result) {
                        if (result.error_code == 0) {
                            backendArtistsEditorFuncs.loadArtistReviews(this_m_artist_id, 1);
                            $("#artist_review_dialog").dialog('destroy');
                        } else {
                            alert(result.error_message)
                        }
                    }
                })

            }, // "Submit": function() {
            "Close": function () {
                $(this).dialog("close");
            }
        },
        width: "50%",
        maxWidth: "678px",
        modal: true
    }).dialog("open");

} // backendArtistsEditorFuncs.prototype.editArtistReviewInPopupEditor = function (isInsert, artist_review_id, artistReview) {

backendArtistsEditorFuncs.prototype.deleteArtistReview = function (artist_review_id, artist_review_review_text) {
    if (!confirm("Do you want to delete '" + artist_review_review_text + "' review ?")) return;
    $.ajax({
        url: ajax_object.backend_ajaxurl,
        dataType: "json",
        type: 'post',
        data: {
            action: 'delete-artist-review',
            lang : this_m_lang,
            artist_review_id: artist_review_id
        },
        success: function (result) {
            if (result.error_code == 0) {
                backendArtistsEditorFuncs.loadArtistReviews(this_m_artist_id, 1);
                $("#artist_review_dialog").dialog('destroy');
            } else {
                alert(result.error_message)
            }
        }
    })

} // backendArtistsEditorFuncs.prototype.deleteArtistReview = function (artist_review_id, artist_review_review_text

backendArtistsEditorFuncs.prototype.editArtistReview = function (artist_review_id) {
    $.ajax({
        url: ajax_object.backend_ajaxurl,
        dataType: "json",
        type: 'post',
        data: {
            action: 'get-artist-review-by-id',
            lang : this_m_lang,
            with_inappropriates_count : 1,
            artist_review_id: artist_review_id
        },
        success: function (result) {
            if (result.error_code == 0) {
                $("#artist_review_ID").val(result.artist_review_id)
                $("#artist_review_user_id").val(result.artistReview.user_id)
                $("#artist_review_review_text").val(stripSlashes(result.artistReview.review_text))
                $("#artist_review_created").val(result.artistReview.created)


                backendArtistsEditorFuncs.editArtistReviewInPopupEditor(false, artist_review_id, result.artistReview);

            } else {
                alert(result.error_message)
            }
        }
    })
} // backendArtistsEditorFuncs.prototype.editArtistReview = function (artist_review_id) {
/* ARTIST REVIEWS END BLOCK */

/* ARTIST SONGS START BLOCK */
backendArtistsEditorFuncs.prototype.loadSongsArtists = function (artist_id, paged) {
    if (typeof paged == "undefined") paged = 1
    $.post(ajax_object.backend_ajaxurl, {
            action: 'get-artist-songs',
            lang : this_m_lang,
            artist_id: artist_id,
            paged: paged,
            m_songsPageUrl: this_m_songsPageUrl,
            show_only_text: 0
        },
        function (data) {
            //alert(" songdata::" + var_dump(data))
            if (data.error_code == 0) {
                $("#div-artist-songs").html(data.content);
                if (data.songsCount > 0) {
                    $("#span-artist-songs-title").html('(' + data.songsCount + ')');
                } else {
                    $("#span-artist-songs-title").html('');
                }
            } else {
                alert(data.error_message)
            }
        }, "json");
} // backendArtistsEditorFuncs.prototype.loadSongsArtists = function (artist_id, paged) {


backendArtistsEditorFuncs.prototype.inArtistAddNewSong = function () {
    $("#artist_song_song_id").val("")
    $("#div_inArtistAddNewSong").css("display", "block")
    $("#div_selectExistingSong").css("display", "none")
    inArtistAddNewSong= true

    $('.add_new_song_artist_photo_fileupload').fileupload({
        url: ajax_object.backend_ajaxurl + '?action=upload-artist-add-new-song&artist_id=' + this_m_artist_id + "&plugin_url=" + this_plugin_url + "&lang="+ this_m_lang,
        dataType: 'json',
        done: function (e, data) {
            alert( "add_new_song_artist_photo_fileupload::"+var_dump(data.result))
            if ( data.result.error_code == 1 ) {
                alert( data.result.error_message )
                return;
            }
            $("#add_new_song_div_save_upload_photo").css("display", "block");
            $("#add_new_song_img_preview_image").attr("src", data.result.files.url);
            $("#add_new_song_img_preview_image").attr("width", data.result.files.FilenameInfo.Width);
            $("#add_new_song_img_preview_image").attr("height", data.result.files.FilenameInfo.Height);
            $("#hidden_add_new_song_selected_photo").val(data.result.files.name);
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .progress-bar').css(
                'width',
                progress + '%'
            );
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
} // backendArtistsEditorFuncs.prototype.inArtistAddNewSong = function (isInsert, artist_song_id, artistSong) {

backendArtistsEditorFuncs.prototype.inArtistSelectExistingSong = function () {
    $("#artist_song_song_id").val("")
    $("#div_inArtistAddNewSong").css("display", "none")
    $("#div_selectExistingSong").css("display", "block")
    inArtistAddNewSong= false
} // backendArtistsEditorFuncs.prototype.inArtistSelectExistingSong = function (isInsert, artist_song_id, artistSong) {


backendArtistsEditorFuncs.prototype.deleteArtistSong = function (artist_song_id, artist_song_title) {
    if (!confirm("Do you want to delete '" + artist_song_title + "' song ?")) return;
    $.ajax({
        url: ajax_object.backend_ajaxurl,
        dataType: "json",
        type: 'post',
        data: {
            action: 'delete-artist-song',
            lang : this_m_lang,
            artist_song_id: artist_song_id
        },
        success: function (result) {
            if (result.error_code == 0) {
                backendArtistsEditorFuncs.loadSongsArtists(this_m_artist_id, 1);
                $("#artist_song_dialog").dialog('destroy');
            } else {
                alert(result.error_message)
            }
        }
    })
} // backendArtistsEditorFuncs.prototype.deleteArtistSong = function (artist_song_id, artist_song_title) {

backendArtistsEditorFuncs.prototype.editArtistSong = function (artist_song_id) {
    $.ajax({
        url: ajax_object.backend_ajaxurl,
        dataType: "json",
        type: 'post',
        data: {
            action: 'get-artist-song-by-id',
            lang : this_m_lang,
            artist_song_id: artist_song_id
        },
        success: function (result) {
            if (result.error_code == 0) {
                $("#artist_song_ID").val(result.artist_song_id)
                $("#artist_song_song_id").val(result.artistSong.song_id)
                $("#artist_song_created").val(result.artistSong.created)
                backendArtistsEditorFuncs.editArtistSongInPopupEditor(false, artist_song_id, result.artistSong);
            } else {
                alert(result.error_message)
            }
        }
    })
} // backendArtistsEditorFuncs.prototype.editArtistSong = function (artist_song_id) {

backendArtistsEditorFuncs.prototype.editArtistSongInPopupEditor = function (isInsert, artist_song_id, artistSong) {
    if (isInsert) {
        $("#artist_song_song_id").val("")
        $("#span_artist_song_mode_label").html("add")
        $("#div_artist_song_created").css("display", "none")
    } else {
        $("#div_artist_song_created").css("display", "block")
        $("#span_artist_song_mode_label").html("edit")
    }

    $("#artist_song_dialog").dialog({
        buttons: {
            "Submit": function () {
                if ( !inArtistAddNewSong ) { // selecting existing song!
                    var artist_song_song_id = $("#artist_song_song_id").val()
                    if (artist_song_song_id == "") {
                        alert("Select song !")
                        $('#artist_song_song_id').focus();
                        return;
                    }
                    $.ajax({
                        url: ajax_object.backend_ajaxurl,
                        dataType: "json",
                        type: 'post',
                        data: {
                            action: 'update-artist-song',
                            lang : this_m_lang,
                            artist_song_id: artist_song_id,
                            artist_id: this_m_artist_id,
                            song_id: artist_song_song_id
                        },
                        success: function (result) {
                            if (result.error_code == 0) {
                                backendArtistsEditorFuncs.loadSongsArtists(this_m_artist_id, 1);
                                $("#artist_song_dialog").dialog('destroy');
                            } else {
                                alert(result.error_message)
                            }
                        }
                    })
                } else {  // creating new song!
                    var song_title = $("#add_new_song_song_title").val()
                    if (song_title == "") {
                        alert("Enter song !")
                        $('#add_new_song_song_title').focus();
                        return;
                    }

                    var song_ordering = $("#add_new_song_song_ordering").val()
                    if (song_ordering == "") {
                        alert("Enter ordering !")
                        $('#add_new_song_song_ordering').focus();
                        return;
                    }

                    var song_is_active = $("#add_new_song_song_is_active").val()
                    if (song_is_active == "") {
                        alert("Select active !")
                        $('#add_new_song_song_is_active').focus();
                        return;
                    }

                    var checked_song_jenre_selection = ''
                    $('input.checkbox_add_new_song_cbx_song_jenre:checked').each(function () {
                        checked_song_jenre_selection = checked_song_jenre_selection + $(this).val() + ','
                    });
                    if (checked_song_jenre_selection == "") {
                        alert("Select at least 1 jenre !")
                        return;
                    }
                    $.post(ajax_object.backend_ajaxurl, {
                            action: 'artist_add_new_song',
                            lang : this_m_lang,
                            artist_id: this_m_artist_id,
                            title: song_title,
                            ordering: song_ordering,
                            is_active: song_is_active,
                            song_jenre_selection : checked_song_jenre_selection,
                            photo : $("#hidden_add_new_song_selected_photo").val()
                        },
                        function (data) {
                            if (data.error_code == 0) {
                                $("#add_new_song_song_title").val("")
                                $("#add_new_song_song_ordering").val("")
                                $("#add_new_song_song_is_active").val("")
                                $("#hidden_add_new_song_selected_photo").val("")
                                backendArtistsEditorFuncs.inArtistSelectExistingSong();
                                $('input.checkbox_add_new_song_cbx_song_jenre:checked').each(function () {
                                    $(this).prop('checked', false);
                                });
                                $("#add_new_song_img_preview_image").attr("src", "");
                                $("#add_new_song_img_preview_image").attr("width", 0);
                                $("#add_new_song_img_preview_image").attr("height", 0);
                                backendArtistsEditorFuncs.loadSongsArtists(this_m_artist_id, 1);
                                $("#artist_song_dialog").dialog('destroy');
                            } else {
                                alert(data.error_message)
                            }
                        }, "json");
                } //

            }, // "Submit": function() {
            "Close": function () {
                $(this).dialog("close");
            }
        },
        width: "50%",
        maxWidth: "678px",
        modal: true
    }).dialog("open");

} // backendArtistsEditorFuncs.prototype.editArtistSongInPopupEditor = function (isInsert, artist_song_id, artistSong) {

/* ARTIST SONGS END BLOCK */


/*  PHOTOS BLOCK START  */
backendArtistsEditorFuncs.prototype.UploadPhoto = function () {
//function UploadPhoto() {
    var input_photo_description = jQuery.trim($("#input_photo_description").val());
    var hidden_selected_photo = $("#hidden_selected_photo").val();

    if (input_photo_description == "") {
        alert("Enter photo description !")
        $("#input_photo_description").focus()
        return;
    }

    $.post(ajax_object.backend_ajaxurl, {
            action: 'save-artist-photo',
            lang : this_m_lang,
            artist_id: this_m_artist_id,
            description: input_photo_description,
            photo_name: hidden_selected_photo
        },
        function (data) {
            //alert("data::" + var_dump(data))
            if (data.error_code == 0) {
                backendArtistsEditorFuncs.CancelUploadPhoto(this_m_artist_id)
                backendArtistsEditorFuncs.loadArtistPhotos(this_m_artist_id, 1)
            } else {
                alert(data.error_message)
            }
        }, "json");

} // backendArtistsEditorFuncs.prototype.UploadPhoto = function () {

backendArtistsEditorFuncs.prototype.CancelUploadPhoto = function (artist_id) {
    $("#div_upload_photo").css("display", "block");
    $("#div_save_upload_photo").css("display", "none");
    $("#img_preview_image").attr("src", "");
    $("#input_photo_description").val("");
    $("#hidden_selected_photo").val("");
} // backendArtistsEditorFuncs.prototype.CancelUploadPhoto = function (artist_id) {

backendArtistsEditorFuncs.prototype.loadArtistPhotos = function (artist_id, paged) {
    $('.artist_photo_fileupload').fileupload({
        url: ajax_object.backend_ajaxurl + '?action=upload-artist-photo&artist_id=' + artist_id + "&plugin_url=" + this_plugin_url + "&lang=" + this_m_lang,
        dataType: 'json',
        done: function (e, data) {
            $("#div_upload_photo").css("display", "none");
            $("#div_save_upload_photo").css("display", "block");
            $("#img_preview_image").attr("src", data.result.files.url);
            $("#img_preview_image").attr("width", data.result.files.FilenameInfo.Width);
            $("#img_preview_image").attr("height", data.result.files.FilenameInfo.Height);
            $("#hidden_selected_photo").val(data.result.files.name);
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .progress-bar').css(
                'width',
                progress + '%'
            );
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');

    $.post(ajax_object.backend_ajaxurl, {
            action: 'get-artist-photos', lang : this_m_lang, artist_id: artist_id, paged: paged, plugin_url: this_plugin_url, show_only_text: 0
        },
        function (data) {
            if (data.error_code == 0) {
                $("#div-artist-photos").html(data.content);
                showFadingMessage(data.photosCount + " photo(s) were loaded!" );

                if ( parseInt(data.ngg_gallery_gid) > 0 ) {
                    $("#div_upload_photo").css("display", "none")
                    $("#div_save_upload_photo").css("display", "none")
                } else {
                    $("#div_upload_photo").css("display", "block")
                }

                if (data.photosCount > 0) {
                    $("#span-artist-photos-title").html('(' + data.photosCount + ( data.inappropriatePhotosCount > 0 ? ( ", " + data.inappropriatePhotosCount + " inappropriate" ) : "" ) + ')');
                } else {
                    $("#span-artist-photos-title").html('');
                }
            } else {
                alert(data.error_message)
            }
        }, "json");


    $(".fancybox-button").fancybox({
        prevEffect: 'none',
        nextEffect: 'none',
        closeBtn: true,
        openEffect: 'none',
        closeEffect: 'none',
        'titlePosition': 'over',
        helpers: {
            title: {type: 'inside'},
            buttons: {}
        }
    });
} // backendArtistsEditorFuncs.prototype.loadArtistPhotos = function (artist_id, paged) {

backendArtistsEditorFuncs.prototype.deleteArtistPhoto = function (artist_photo_id, description) {
    if (!confirm("Do you want to delete '" + description + "' photo ?")) return;
    $.post(ajax_object.backend_ajaxurl, {
            action: 'delete-artist-photo', lang : this_m_lang, artist_photo_id: artist_photo_id
        },
        function (data) {
            if (data.error_code == 0) {
                backendArtistsEditorFuncs.CancelUploadPhoto()
                backendArtistsEditorFuncs.loadArtistPhotos(this_m_artist_id, 1)
            } else {
                alert(data.error_message)
            }
        }, "json");
}

backendArtistsEditorFuncs.prototype.clearArtistPhotoInappropriate = function (artist_photo_id, description) {
    if (!confirm("Do you want to set this clear Inappropriate for '" + description + "' photo ?")) return;
    $.post(ajax_object.backend_ajaxurl, {
            action: 'clear-inappropriate-for-artist-photo', lang : this_m_lang, artist_photo_id: artist_photo_id
        },
        function (data) {
            if (data.error_code == 0) {
                backendArtistsEditorFuncs.CancelUploadPhoto()
                backendArtistsEditorFuncs.loadArtistPhotos(this_m_artist_id, 1)
            } else {
                alert(data.error_message)
            }
        }, "json");

} // backendArtistsEditorFuncs.prototype.clearArtistPhotoInappropriate = function (artist_id, photo_id) {

/*  PHOTOS BLOCK END  */


/*  CONFIRM DELETING OF ARTIST BLOCK START  */
backendArtistsEditorFuncs.prototype.onConfirmDeletingSubmit = function () {
    var theForm = $("#form_artist_confirm_delete");
    theForm.submit();
} // backendArtistsEditorFuncs.prototype.onConfirmDeletingSubmit = function () {

backendArtistsEditorFuncs.prototype.onConfirmDeletingInit = function () {
    $(".fancybox-button").fancybox({  // http://pishemsite.ru/jquery/podklyuchenie-i-nastroyka-fancybox#
        prevEffect: 'none',
        nextEffect: 'none',
        closeBtn: true,
        openEffect: 'none',
        closeEffect: 'none',
        'titlePosition': 'over',
        helpers: {
            title: {type: 'inside'},
            buttons: {}
        }
    });

    backendArtistsEditorFuncs.loadConfirmDeletingArtistReviews(this_m_artist_id);
    backendArtistsEditorFuncs.loadConfirmDeletingArtistSongs(this_m_artist_id);
    backendArtistsEditorFuncs.loadConfirmDeletingArtistPhotos(this_m_artist_id);
} // backendArtistsEditorFuncs.prototype.onConfirmDeletingnit= function() {

backendArtistsEditorFuncs.prototype.loadConfirmDeletingArtistReviews = function (artist_id) {
    $.post(ajax_object.backend_ajaxurl, {
            action: 'get-artist-reviews',
            lang : this_m_lang,
            artist_id: artist_id,
            paged: '',
            plugin_url : this_plugin_url,
            show_only_text: 1
        },
        function (data) {
            if (data.error_code == 0) {
                $("#div-artist-reviews").html(data.content);
                if (data.reviewsCount > 0) {
                    $("#span-artist-reviews-title").html('(' + data.reviewsCount + ')');
                } else {
                    $("#span-artist-reviews-title").html('');
                }
            } else {
                alert(data.error_message)
            }
        }, "json");

} // backendArtistsEditorFuncs.prototype.loadConfirmDeletingArtistReviews = function (artist_id) {

backendArtistsEditorFuncs.prototype.loadConfirmDeletingArtistSongs = function (artist_id) {
    $.post(ajax_object.backend_ajaxurl, {
            action: 'get-artist-songs',
            lang : this_m_lang,
            artist_id: artist_id,
            paged: '',
            show_only_text: 1
        },
        function (data) {
            if (data.error_code == 0) {
                $("#div-artist-songs").html(data.content);
                if (data.songsCount > 0) {
                    $("#span-artist-songs-title").html('(' + data.songsCount + ')');
                } else {
                    $("#span-artist-songs-title").html('');
                }
            } else {
                alert(data.error_message)
            }
        }, "json");

} // backendArtistsEditorFuncs.prototype.loadConfirmDeletingArtistSongs = function (artist_id) {

backendArtistsEditorFuncs.prototype.loadConfirmDeletingArtistPhotos = function (artist_id) {
    $.post(ajax_object.backend_ajaxurl, {
            action: 'get-artist-photos',
            lang : this_m_lang,
            artist_id: artist_id,
            paged: '',
            plugin_url: this_plugin_url,
            show_only_text: 1
        },
        function (data) {
            if (data.error_code == 0) {
                $("#div-artist-photos").html(data.content);
                if (data.photosCount > 0) {
                    $("#span-artist-photos-title").html('(' + data.photosCount + ')');
                } else {
                    $("#span-artist-photos-title").html('');
                }
            } else {
                alert(data.error_message)
            }
        }, "json");

} // backendArtistsEditorFuncs.prototype.loadConfirmDeletingArtistPhotos = function (artist_id) {


backendArtistsEditorFuncs.prototype.bulkArtistsListApply = function (bulk_label1, bulk_label2, bulk_label3, bulk_label4) {
    var actionSelected = $("#bulk-action-selector-top option:selected").val()
    if (actionSelected == "") {
        alert(bulk_label1 + "!")
        $("#bulk-action-selector-top").focus()
        return;
    }

    var artistsCheckedCount = 0
    $('input.artist-bulk-actions:checked').each(function () {
        artistsCheckedCount = artistsCheckedCount + 1
    });
    if (artistsCheckedCount == 0) {
        alert(bulk_label2 + "!")
        return;
    }

    var actionName = $("#bulk-action-selector-top option:selected").text()
    if (!confirm(bulk_label3 + " " + actionName + " " + bulk_label4 + "?")) return;
    $("#bulk_operation_title").val(actionName);
    $("#bulk_action").val(actionSelected);
    var theForm = document.getElementById("form_artists");
    theForm.submit();
} // backendArtistsEditorFuncs.prototype.bulkArtistsListApply = function (artist_id) {



backendArtistsEditorFuncs.prototype.MakeNextGENGalleryLink = function () {
    var linked_ngg_gallery_id = $("#linked_ngg_gallery_id option:selected").val()
    var linked_ngg_gallery_name = $("#linked_ngg_gallery_id option:selected").text()
    var linked_ngg_gallery_id_operation= $('input[name=linked_ngg_gallery_id_operation]:checked').val()

    if (linked_ngg_gallery_id == "" /*&& (linked_ngg_gallery_id_operation!= "no_operation" )  */) {
        alert(" Select Gallery !")
        $("#linked_ngg_gallery_id").focus()
        return;
    }

    if (linked_ngg_gallery_id_operation == "" || typeof linked_ngg_gallery_id_operation == "undefined") {
        alert(" Select Operation !")
        $("#linked_ngg_gallery_id_operation").focus()
        return;
    }
    if (!confirm("Do you want to run Operation ?")) return;

    $.post(ajax_object.backend_ajaxurl, {
            action: 'run-linked-ngg-gallery-operation',
            lang : this_m_lang,
            artist_id: this_m_artist_id,
            linked_ngg_gallery_id: linked_ngg_gallery_id,
            linked_ngg_gallery_name: linked_ngg_gallery_name,
            linked_ngg_gallery_id_operation : linked_ngg_gallery_id_operation,
            plugin_url: this_plugin_url
        },
        function (data) {
            if (data.error_code == 0) {
                backendArtistsEditorFuncs.loadArtistPhotos(this_m_artist_id, 1)
            } else {
                alert(data.error_message)
            }
        }, "json");
}
/*  CONFIRM DELETING OF ARTIST BLOCK END  */


var rad = function(x) {
    return x * Math.PI / 180;
};

var getDistance = function(p1, p2, show_km) {
    //alert( "type p1::" + ( typeof p1) + "  p1:" + var_dump(p1)  )
    // alert( "type p2::" + ( typeof p2) + "  p2:" + var_dump(p2)  )
    var p1_lat= p1.lat()
    var p2_lat= p2.lat()
    var R = 6378137; // Earth’s mean radius in meter
    var dLat = rad(p2.lat() - p1.lat());
    var dLong = rad(p2.lng() - p1.lng());
    var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(rad(p1.lat())) * Math.cos(rad(p2.lat())) *
        Math.sin(dLong / 2) * Math.sin(dLong / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c;

    return ( show_km ? parseInt(d/1000) +" km": parseInt(d)+" meters" ); // returns the distance in meter
};