Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2017-06-02T06:38:57+03:00

'''
 Sergey, can you take a few moments to email me a possible approach / solution for different user titles logging in to client environment?
'''
https://trello.com/c/mKkYSeZa
'''
Originnally it was made in a way, that any user type(title) had his own module, like "volunteer" or "spiritual-counselor".
You can see it in directory application/modules/
That seems reasonable, as user with different title can has different functionality. 
I can propose to make dashboard pages for any user title, which would show main information about logged user.
In groups table we have next groups(titles) http://imgur.com/a/ohefB.
I think according to this data modules under application/modules/ must be created. Usused modules deleted.
I estimate it as 5-6 hours.



1)  >>> Please explain is_multi_auth is simple, basic english. __ did you put this in? If so, what is it used for?
That is boolean fields, it can be true or false.
I see it db with label "Authenticity" , added by BBITS.
But I can not say what is it for


2)        >>>  >>>   https://devk.zntral.net/sys-admin/client-edit/9/ = 404
         >>>  >>> This was the page that was gone from the last devk merge to dev. Is there a way to retrieve the page from git?
           >>>  >>>   I am not sure what do you mean. I think links like sys-admin/client-edit/9 were from prior version of site and now are absolute.
            >>>  >>>  I suppose now clicking on this button 1 more popup page must be opened, but editing clients fields(first name, phone ...)
 >>> From Karine's comment: 4.I remember this page, I think after Sergey's work it was deleted, This page https://dev4.zntral.net/sys-admin/client-edit/9/
If we need it I can find and restore it.
But as I see that was some variant with "invalid app construction", where new user was added by form submitted. We refused from it.
Also what for we need 1 more client editor page ? 
I suppose in https://dev4.zntral.net/sys-admin/client/9/ - client overview page. But ALL editing would be done withing this page with ajax without submitting/reloding
of this page.


3)    >>>    As far as I remember in my original version of site in similar listing of related users/vendors or something like that there were several filters options where admin could selected all users client works with
        >>> or on the contrary all users client does not works with. if we have 1 more free hosting with db I can find and install old version of site and we will watch it.

   >>>  >>>This will not work because a client should not have a list of users to select from. Users must be registered with client. Then if user exists in db, the website will detect it. This function will be detailed later.
That is ok if this function will be detailed later. But I think say as example:
2 different clients in system : Client Tom and Client Todd.
3.1) Say sysadmin for Client Tom added new user as : (username : john_petrow, first_name/last_name : John Petrow, email : john_petrow@mail.com.
On entering this in users would be 1 row with username : john_petrow, first_name/last_name : John Petrow, email : john_petrow@mail.com.john_petrow@mail.com

3.2) But when sysadmin needs to add the same person John Petrow for other Client Todd we would have in users table 2 rows
'''
'' with username : john_petrow, first_name/last_name : John Petrow, email : john_petrow@mail.com.john_petrow@mail.com.''
I am not sure that is what we need.
I added this task into Postponed  Tasks.

4)  >>> 2) In manage users page, fix column sorting function and make default view list newest created as first
Fixed.

5)  >>> 3) Complete USER OVERVIEW client associations described in this comment
As I see these tasks for Karina.

