Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2017-01-21T13:40:13+02:00

Rod: You read my mind :)
[14:12:41] Rod: Yes. Similar for all listings
[14:12:56] Rod: But for users: https://dev4.zntral.net/sys-admin/users/users-view
[14:13:27] Rod: move delete (X) button to the bottom of user-edit page
[14:13:40] Rod: who... what the heck.. I did not want that emoji lol
[14:13:59] Rod: The delete button on user list https://dev4.zntral.net/sys-admin/users/users-view
[14:14:14] Rod: move to bottom of user edit page
[14:15:59] Sergey Nilov: ok

I remade all listings, with link at first column and moved "Remove" button to editor where  "Remove" button is implemented.


== ====== ==
I applied changes from dev4b to dev4.
As I see mostly headers at listing/editor were hidden and some changes in labels file done.
I suppose now it is not vey good looking say
in editor https://dev4.zntral.net/sys-admin/vendors/vendor-types-edit/6/
and in listing https://dev4.zntral.net/sys-admin/vendors/vendor-types-view/
both has the same header "New Vendor " - what is wrong in both cases.

==========

		1) I am not sure that I understand that idea with adding new client having buttons visible,but not inputs as here https://www.dropbox.com/s/oja1mou2jpllpoq/1.jpg?dl=0
		Many fields are required, admin can not ssave client without filling them.
		So I suppose that require
d fields must be visible from the start.

The idea is to move from this: https://www.dropbox.com/s/74hm2o3p4e08gmx/f.gif?dl=0
to this: https://www.dropbox.com/s/oja1mou2jpllpoq/1.jpg?dl=0

It is a simplified version of the form and I want all forms to function this way. Required fields should be red so that they stand out and the user will not be able to submit the information without completing required fields. Also, take note that these are "administrative" forms and not clinical forms as described here https://zntral.net/dox/forms-types.php < subject to change.

		2)Look how I remade phones, but only for the last phone "Phone Type" selection is visible.
		Is it ok or we need "Phone Type" selection for all phones(Now max 5 phones).

Yes - we are on the right path here but there is more work to be done. Example: each phone number should have the ability to give it a custom name or label as described in the first comment of this task.

		3) Also where "Add a phone" button must be located? I made if all 4 phones are hidden.

If a user enters 1 phone, "Add a phone" should automatically be added. This is more intuitive and less clicks for user

		4) I understand for phones : it could be from 1 till 4 phone and we can show only first, but not for the rest inputs

We should include a message "Add up to 5 phone numbers" or something like that

		5) Also I suppose "Add a phone" must be of different disign, not like Submit, filter buttons.

Design should be as close to examples in first comment of this task or from http://materializecss.com/forms.html = simple and clean. :) 

== = ==
div_client_name_btn
div_client_name_input
