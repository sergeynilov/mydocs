Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2017-05-19T14:29:59+03:00


Karina, I remade to "valid app construction", but there are some problems now, with header of client editor page :
https://dev4.zntral.net/sys-admin/client/26/
Modifing file please work with application/modules/sys_admin/views/clients/client.php file, not files in application/views/design/ - they are common for all pages.
If you need to modify other files, but application/modules/sys_admin/views/clients/client.php, please ask me online.

I modified 4 files :
1) application/modules/sys_admin/controllers/Sys_admin.php
I replaced html_topbar_client with html_topbar

2) In application/views/design/html_topbar.php I left only header code till </head> tag.
Not any client mentioned must be here.

3) In application/views/design/html_footer.php file I left only fooetr info and removed references to clients from prior version

4) To application/modules/sys_admin/views/clients/client.php file I added header code from html_topbar_client file.

Please ask me, I would be online. If I am not online I would be online later and answer your questions.



1)  custom-client-overview-view.css is called at top of application/modules/sys_admin/views/clients/client.php


Must be visible  in clients page


Silviu, I have 
$ git status
On branch Dev
Your branch is up-to-date with 'origin/Dev'.


But running your command I got error:
$ git push -u origin dev2
error: src refspec dev2 does not match any.
error: failed to push some refs to 'https://sergeynilov@bitbucket.org/xntral/zntral.git/branch/Dev'

Which commands must be ?
