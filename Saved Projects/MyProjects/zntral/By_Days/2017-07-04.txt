Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2017-06-04T06:54:17+03:00

-
For different profile with each client therte is users_clients table http://imgur.com/a/7cMa5 and it has  uc_active_status field.

To Implement this I start  making on client overview page https://devk.zntral.net/sys-admin/client/9/ next :
clicking on pencil edit button open popup dialog with editing of client fields, like  client_name, client_img, client_owner,	client_address1, client_city etc...
On saving data in popup dialog editor, this popup dialog would be hidden and client overview information would be refreshed without page submitting.

Only filled fields would be shown on client overview page...
I think that could take 10-12 hours.+++++++++++++++++++

**>>> making client editor in client overview page**
1) In client overview page I made in top area main fields of clients. Some of them are grouped : addresses, phones etc...
It seems to me that we have to make several blocks of view  fileds, as client has a lot of fields, including client_notes,  memo field color_scheeme, fax ... .
This top area is too small to show all fileds.


2) Now in client editor( https://devk.zntral.net/sys-admin/client-edit/28/ ) there are fields `client_phone`,`client_phone_2`, `client_phone_3`, `client_phone_4` and the only field `client_phone_type` .
That is not clear, as client_phone_type for which 4 phones is referenced? As I remember in my time I made separate phone table and client(user) could have any type of phones(with related phone type)...
Have I to add to client table `client_phone_type_2`, `client_phone_type_3`, `client_phone_type_4`  fields related to any phone fields?


3) How modify status in client overview or in editor page ? Now client status is not modified, only visible in overview page.


4) In this template we have several client_email, client_email_2, client_email_3, client_email_4, client_email_5 fields.
But in db clients table has only client_email table ? I suppose we do not need client_email_2, client_email_3, client_email_4, client_email_5 fields and I have to remove these inputs from client editor?


5) In client editor page (https://dev4.zntral.net/sys-admin/client-edit/26/)
There is Theme block with 2 inputs, but client table we have `color_scheme` SMALLINT(2), field.
I suppose in this block must be selection input, options for which is defined in config of system?


6) In client editor page (https://dev4.zntral.net/sys-admin/client-edit/26/) there is client_lic_tag selection field with options Slogan, Saying, Quote
have I to add similar field in client table?


7) I added avatar uploading functionality and avatar is visible at overview page.


8) There are a lot of fields which are not visible at client overview page or at client editor page , as : 
	`clients_types_id`,`client_owner` , `client_fax`, `color_scheme`(I suppose it is instead of Theme block in 5) ), `client_notes` TEXT, `client_status` , `created_at` and  `updated_at` 


Please try updating clients, including saving with invalid data.

 
																		</select>



	 as for this >>> Fix the avatar uploading function. The image / avitar should appear on the side navigation
6) >>>   I can make it, as uploading of images in Codeigniter is specific.
	>>> Rod, please remind on which page image is loaded to user ? on https://devk.zntral.net/sys-admin/users/users-overview/62/ ?

User image should appear in following sections

	User overivew page in the circle next to user name. **So image should appear in "N" left of "Nilov Sergey"**
	When user modifies own information with green circle button, image should appear in "General" box: Where it says "PICTURE 1". If image exists, current image should appear above "PICTURE 1" field, This should be responsive design and picture should not break layout / design.



1) Are client_owner, client_name and client_email editable fields? Or in other words these fields can be filled only creating new client?


== ============= ==

>>> @sergei131 For client manage, Please fix "Client Active Status" column function and sorting
Fixed.

>>> CLIENT STATUS
@sergei131 or @karine226
	For client manage, fix "Client Active Status" column function and sorting
	Client st**atus should be displayed at top of cl**ient overview page, similar to user overview page
I added status label under "Pinned" label. Or have i to remove "Pinned" label?

https://trello.com/c/r8aYI9xY/65-users#comment-59325ab14a4b319f60c1221d

@sergei131 only user id 1, 2 and 3 is showing correct client association on user manage page

	>>> Update client column that shows user associations to client. All client associations should be listed. If user registered with 3 clients, all 3 client names should be listed in this field.
	>>> Client names in client(s) column should link to client overview page.
The reason of this problem is that
users_clients table http://imgur.com/a/7cMa5 has 'uc_active_status' / "Relation Status" field, which by default has value 'N'/'New' value.
Other possible values are 'E'/'Employee' OR  'O'/'Out of staff' as far as I remember.
These new clients are not shown in users listing.
It can be solved as next :
1) When sysadmin adds new user for client in "New user for" dialog box to add select input where status of relation would be selected.
2) On saving new user to 'uc_active_status' field set one values 'Employee' / 'Out of staff'.
3) On saving new user to 'uc_active_status'  'N' value is set but confirmation email is sent to this user on his email with link like in user's activation.
As far as I remember from prior discussions 3) is prefereable, which means that some user is invited to cooperate with some client and he can accept this invitation or reject.
also one more this in this point who and on which step selects 'Employee' / 'Out of staff' ? - option, as they both mean that user works with the client.

Or do you some other way?

