Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2017-01-18T09:45:14+02:00


>>>       Remove "Color Scheme *" on the new client page clients-edit/new/ This will be set by the client superuser, not system admin. All new users will have the default layout so no selection is needed. The "Theme color" will later be moved to the client overview page.   replace the color scheme with a temporary notification box. this will be used as a placeholder
Ok for new mode, but in edit mode that could be editable input selection?
~1 hour

>>>       At the bottom, add a button called "Save and Create Superuser". This button will save the client information and redirect to the New user page. On the New user page, there is no client name here but the new user will automatically belong to the new client that was just created. This will be a temporary method until the "Related Users" dialog box is fixed. Hopefully this method does not take very long to implement. Please let me know if it does.
~2-3 >>>   Next: Review the list, enter some feedback comment and include an estimated time frame it will take to complete. When you start, drag this task to "In Progress"

**==========**

I made cms items listing editor.
Pay attention that Content is RTF editor and it could be filled with tags related to this email.
List of tags below of Content field.

These emails now works for User Register, Account Activated , Account Activated and New Password Generated
Do we need similar email based for templates when adding new client, vendor ?
Also when user is activated / deactivated?

Also after probel with dialog opened is fixed I would send email on user/client relation set.
------

I remade Client Status with 'Panding' status. As for the rest of your description as for backe dn part I think now we have what we need.

I remade that in insert mode Color Scheme is not editable, but has default value.
In edit mode it could be editable.

===========
@sergei131 Here's your next task:

	Please review Client Status at https://zntral.net/dox/clients.php#clientstatus . Creating a new client, the status will automatically be set to pending so nothing should be set by admin@admin so Client Active Status can be removed. When the client information is already in the website, the status should be pending and admin@admin can change the status after the client information is in the website. When the client status is set to inactive, all users will not have access to the website and the users will be redirected to another page, contact zntral.com or something like that.
	Remove "Color Scheme *" on the new client page clients-edit/new/ This will be set by the client superuser, not system admin. All new users will have the default layout so no selection is needed. The "Theme color" will later be moved to the client overview page.
	At the bottom, add a button called "Save and Create Superuser". This button will save the client information and redirect to the New user page. On the New user page, there is no client name here but the new user will automatically belong to the new client that was just created. This will be a temporary method until the "Related Users" dialog box is fixed. Hopefully this method does not take very long to implement. Please let me know if it does.

Next: Read this list, enter some feedback and an estimated time frame it will take to complete. When you start, drag this task to "In Progress"




@sergei131 The priority is making sure users can log in and the overview pages are ready.



@sergei131

		As I see current "New" status must be changed to pending,

Yes, I added new information on https://zntral.net/dox/clients.php#clientstatus - clients that are new will have a "pending" status. This is not set by system admin. It is automatic by website.

		as for "redirected to another page" for inactive I think that MUST NOT BE IMPLEMENTED for sys-admin, as I do now?
		~1 hour

Yes. "All users of an inactive client will be redirected to another page with a template". Later we will do this: All users will be able to log in but will not be able to make any changes.

q) 1) If email sent as html 

== ==== ==
@silviubajenarumarcu

		Working with code I see that site is multilanguage and I try to follow to this, so that later other language files could be added to project and make it multilanguage.
		But when you enter some data in pages of admin part and want it multilanguage, we do not need separate language files for it, but enter labels in other languages in backend part,
		something like one of my prior multilanguage site
		http://s017.radikal.ru/i442/1201/8f/4046edd4dd5d.png
		also this multilanguage functionality could be made in /sys-admin/users-role page, for any language in system. I think we need to move to it later.

Got it. The concern is a client naming a title differently than another client. Example:
"Vocational Nurse" title -
Client A will call this "Licensed Vocational Nurse"
Client B will call this "Licensed Practical Nurse"
Both titles have the same jobs

One thing is that most titles will have the same label for all clients. Only a small number will be different. Creating a CMS for http://s017.radikal.ru/i442/1201/8f/4046edd4dd5d.png seems like a lot of work and the faster solution will be to assign a language file for the client and change the value for "Vocational Nurse" title. What are your thoughts?

It might be bad practice, but I want to implement faster solutions (like temporary place holders) then go back and work on them so we can focus on priority tasks like the forms, user-client logins, and different title permissions

Faster solution(as we have now) for musltilanguage can be ok now, but later we could problems with it. Just muist remebre it.
