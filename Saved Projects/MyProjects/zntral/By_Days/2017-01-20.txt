Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2017-01-19T14:49:35+02:00

										<td>
															<i class="fa fa-pencil"></i>

										</td>
										<td>
											<a class="btn btn-sm blue" class="a_link" onclick="javascript:cms_itemRemove(<?php echo $row->ci_id?>, '<?php echo $row->ci_title ?>' )" ><i class="fa fa-remove"></i>
											</a>
										</td>



I am not sure which tasks do you mean ?
We can give prior task with cms editor, we can find link to it.
I made this task 1 day ago, is it reason to give him other task?

=========

As I see from description Locations must be separate listing/editor, as "One location can work with many clients"?
It mean some location (like medical establishment) can provide some services to patients through different clients?
And in client editor must be one more tab " Provides Locations" with functionality  similar "Provides Vendor" but for full list of Locations?

Also similar tab "Provided by clients" must be in location editor, which woul have the same data as above.
Have I to implement this tab at client editor first ?

Also must be Locations Types dictionary.

Later locations data would be used in patient data.

== ======== ==


		as for "Main Attributes"
		I added 3 phones + phone type in Client Editor. Only 1 phoneis required.
		Please pay attemtion how I made "Phone Type" selection ( main, office, mobile... )
		If in selection input there are no Phone Type you need , you can add by selection " - Add New- " item and enter new value in text input which woul be show below and this value would be saved.
		Looks how it workd and I wll rest editors we need it. Do we need similar in users editor?
		In this task you mentioned vendors, but any vendor has "Vendor Contacts" list with phones?
		I reviewed Forms: Types & Status . Seems Administrative part is withing what I do. As for the frontend I do not see sense to touch it.

I would like to implement something like this for all forms: http://materializecss.com/forms.html . This next section is related to https://zntral.net/dox/forms-types.php but I still need to review it. Here is what I am looking for. Review it and let us discuss this before any work is started.

0. Form status: Edit / New: This is a new form with all of the options available. All of the labels are inside the field that the user will enter information.
1. Email is entered: Here the user has entered an email address. The label is moved to the left side and "Add an email" is added below the new email address in case the user wants to add another email address. This will only apply to fields that will have more than one entry such as email addresses or phone numbers.
2. Phone field is selected: The user is about to enter a phone number and the lebel is moved to the left.
3. New phone entered: The flag is extra information for demonstration purposes.
4. Phone type: The user wants to change the phone type and selects the dropdown arrow next to the "Phone"
5. Selection made: The user selects "Main" as the label for the new phone number
6. New phone entered: Another "Add a phone" is automatically added below the existing field.
7. Phone is selected: A menu of preset values are displayed.
8. Custom field: The user can start typing and enter a custom name for the phone number. The form can be saved by clicking on "Save"
9. Form status: Display: A form in display status will only show fields with values. All fields with empty values will be hidden. If the user is the author of the form, or a user with the title of "quality assurance", they will have permission to edit the form. Edit permissions will be indicated by the pencil icon
10. Form status: Edit / New: All options are available to the user


I will  implement adding of phones dynamically(4 phones maximim, but we also need Phone Type be selected, if it must be selected for ALL 4 phones, or only for 1(first or last ) , as you wrote before? 


Rod, as for testing task : now in client editor there is "Related Users" to set users working or not with opened client.
We discussed priorly that in user editor must be similar tab with similar functionality to set working with client or not.
That could be testing task if you do not like ask him to make cms editor(as I did it before).

I hope this task could be done withing 1 day and would give testing developer to see what he has to work with.


== ==== ==
1) I am not sure that I understand that idea with adding new client having buttons visible,but not inputs as here https://www.dropbox.com/s/oja1mou2jpllpoq/1.jpg?dl=0
Many fields are required, admin can not ssave client without filling them.
So I suppose that required fields must be visible from the start.

2)Look how I remade phones, but only for the last phone "Phone Type" selection is visible.
Is it ok or we need  "Phone Type" selection for all phones(Now max 5 phones).

3) Also where "Add a phone" button must be located? I made if all 4 phones are hidden.

4)  I understand for phones : it could be from 1 till 4 phone and we can show only first, but not for the rest inputs

5) Also I suppose "Add a phone" must be of different disign, not like Submit, filter buttons.


[15:43:53] Rod: It would be similar to top part of the page on materializecss
[15:43:56] Sergey Nilov: but name a photo only 1 field - that seems very unconvinient
[15:44:00] Rod: First name, last name, password, etc.
[15:44:33] Rod: name a photo? Do you mean the upload a file?
[15:44:36] Rod: for the profile image?

-----------
Rod: You read my mind :)
[14:12:41] Rod: Yes. Similar for all listings
[14:12:56] Rod: But for users: https://dev4.zntral.net/sys-admin/users/users-view
[14:13:27] Rod: move delete (X) button to the bottom of user-edit page
[14:13:40] Rod: who... what the heck.. I did not want that emoji lol
[14:13:59] Rod: The delete button on user list https://dev4.zntral.net/sys-admin/users/users-view
[14:14:14] Rod: move to bottom of user edit page
[14:15:59] Sergey Nilov: ok



									<td>
											<a class="btn blue waves-effect waves-light" href="<?= base_url($this->uri->segment(1).'/vendors/vendor-types-edit/'.$row->vt_id);?><?= $page_parameters_with_sort ?>">
												<i class="fa fa-pencil"></i>
											</a>
										</td>
										<td>
											<?php if (empty($row->vendors_count) ) :  ?>
												<a class="btn btn-sm blue" class="a_link" onclick="javascript:vendor_typeRemove(<?php echo $row->vt_id?>, '<?php echo $row->vt_name ?>' )" ><i class="fa fa-remove"></i>
												</a>
											<?php endif ?>
										</td>



