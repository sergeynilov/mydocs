Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2016-11-28T08:23:53+02:00


form_user_edit => 
1-541-754-3010


~^[a-zA-Z][\w ]*@[a-zA-Z]+.com$~i
[a-z]\w*@\w+\.[a-z]+
~^[a-z][\w]+@[a-z]+[\.]com$~i
Шаблон: \w@\w\.\w


			$ret = $this->admin_mdl->update_users_clients( $client_id, $new_user_id, 'N', $user_group_id );

			$activation_page_url= $app_config['base_url']."activation/".$activation_code;
			$title= 'You are registered at ' . $app_config['site_name'] . ' site';
			$content = $this->cms_items_mdl->getBodyContentByAlias('user_register',
				array('username' => $username,
					'first_name' => $first_name,
					'last_name' => $last_name,
					'site_name' => $app_config['site_name'],
					'support_signature' => $app_config['support_signature'],
					'activation_page_url' => $activation_page_url,
					'site_url' => $app_config['base_url'],
					'email' => $email
				), true);
				$EmailOutput = $this->common_lib->SendEmail($email, $title, $content );

			$this->session->set_flashdata('editor_message', lang('user') . " '" . $first_name . "' was " . ($is_insert ? "inserted" : "updated") );
			if ($this->db->trans_status() === FALSE) {
				$this->db->trans_rollback();
				$this->output->set_content_type('application/json')->set_output(json_encode(array('ErrorMessage' => 'Error creating user', 'ErrorCode' => 1, 'id' => $id )));
			} else {
				$this->db->trans_commit();
			}

