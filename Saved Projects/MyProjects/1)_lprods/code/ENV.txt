Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2017-05-10T13:52:22+03:00

APP_NAME=Laravel
APP_ENV=local
APP_KEY=base64:kcTkPta7VlfWPJDE/34j+OaevoBeRu1m4LYDs/9H/OQ=
APP_DEBUG=true
APP_LOG_LEVEL=debug
APP_URL=http://localhost

DB_CONNECTION=pgsql
DB_HOST=127.0.0.1
DB_PORT=5432
DB_DATABASE=wprods
DB_USERNAME=postgres
DB_PASSWORD=1

BROADCAST_DRIVER=log
CACHE_DRIVER=file
SESSION_DRIVER=file
QUEUE_DRIVER=sync

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

MAIL_DRIVER=smtp
MAIL_HOST=smtp.mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null

PUSHER_APP_ID=
PUSHER_APP_KEY=
PUSHER_APP_SECRET=

FF_VALID_PHONE_FORMAT = '(01)[0-9]{9}'
VALID_PHONE_FORMAT = 1?\W*([2-9][0-8][0-9])\W*([2-9][0-9]{2})\W*([0-9]{4})(\se?x?t?(\d*))?
PICKDATE_FORMAT_SUBMIT= 'yyyy-mm-dd'
PICKDATE_FORMAT_VIEW= 'd mmmm, yyyy'
POSTGRESQL_FORMAT = 'Y-m-d H:i:s'
POSTGRESQL_FORMAT_MILISECONDS = 'Y-m-d H:i:s.u'

SITE_ROOT= /_wwwroot/lar/gulp_project/
IMAGES_DIR= images/
UPLOADS_IMAGES_URL= uploads/
UPLOADS_IMAGES_DIR= uploads/

UPLOADS_CMSITEM_IMAGES_URL= uploads/cms_items/
UPLOADS_CMSITEM_IMAGES_DIR= uploads/cms_items/

UPLOADS_CATEGORY_IMAGES_URL= uploads/categories/
UPLOADS_CATEGORY_IMAGES_DIR= uploads/categories/
UPLOADS_USER_IMAGES_URL= uploads/users/
UPLOADS_USER_IMAGES_DIR= uploads/users/
DEFAULT_USER_IMAGE= default_user.png

UPLOADS_SETTINGS_IMAGES_URL= uploads/settings/
UPLOADS_SETTINGS_IMAGES_DIR= uploads/settings/

SITE_URL= http://local-gulp_project.com

DATEPICKER_MIN_YEAR= 2010
DATEPICKER_MAX_YEAR= 2030
COUNTRY_DEMO_TEXT = 'In this demo select USA country and valid zipcode to autofill state and city fields';
