Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2017-05-07T11:08:09+03:00

								''<fieldset >''
''									<legend>Common Settings</legend>''


''									<div class="row ">''
''										<div class="form-group  {{ set_field_error_tag("site_name", ' has-error ') }}  col-md-6">''
''											<label for="site_name">Site Name<span class="required"> * </span></label>''
''											<input placeholder="Enter string name" type="text" name="site_name" id="site_name" class="form-control editable_field" value="{% if Settings.site_name is defined %}{{ Settings.site_name }}{% endif %}" size="50" maxlength="50" onchange="javascript:selectionChanged(this, 'commonSettings')" >''
''											<p class="help-block">Site Name, which is visible on pages of backend and frontend.</p>''
''										</div>''

''										<div class="form-group  {{ set_field_error_tag("contact_us_email", ' has-error ') }}  col-md-6 only_md_lg_padding_left_md ">''
''											<label for="contact_us_email">Contact Us email<span class="required"> * </span></label>''
''											<input id="contact_us_email" name="contact_us_email" size="30" maxlength="100" class="form-control editable_field string_input" value="{% if Settings.contact_us_email is defined %}{{ Settings.contact_us_email }}{% endif %}" onchange="javascript:selectionChanged(this, 'commonSettings')" placeholder="Enter valid email">''
''											<p class="help-block">All Contact Us emails will be sent at this email.</p>''
''										</div>''
''									</div>''

''									<div class="row ">''
''										<div class="form-group  {{ set_field_error_tag("support_signature", ' has-error ') }}  col-md-6 ">''
''											<label for="support_signature">Support Signature<span class="required"> * </span></label>''
''											<textarea id="support_signature" name="support_signature" rows="4" cols="60" class="form-control editable_field" onchange="javascript:selectionChanged(this, 'commonSettings')">{% if Settings.support_signature is defined %}{{ Settings.support_signature | raw }}{% endif %}</textarea>''
''											<p class="help-block">This signature will be below of any email to users.</p>''
''										</div>''

''										<div class="form-group  {{ set_field_error_tag("copyright_text", ' has-error ') }}  col-md-6 only_md_lg_padding_left_md ">''
''											<label for="copyright_text">Copyright Text<span class="required"> * </span></label>''
''											<input type="text" name="copyright_text" id="copyright_text" class="form-control editable_field string_input" value="{% if Settings.copyright_text is defined %}{{ Settings.copyright_text }}{% endif %}" size="50" maxlength="50"  onchange="javascript:selectionChanged(this, 'commonSettings')" placeholder="Enter text">''
''											<p class="help-block">Copyright Text, which is visible on pages at footer of backend and frontend.</p>''
''										</div>''
''									</div>''




''									<div class="row">''
''										<div class="form-group  {{ set_field_error_tag("small_icon", ' has-error ') }}  col-md-6 ">''
''											<label for="small_icon">Small Icon<span class="required"> * </span></label>''
''											<input type="file" name="small_icon" id="small_icon" class="form-control editable_field "  onchange="javascript:selectionChanged(this, 'commonSettings')">''
''											<p class="help-block">Select small icon of site.</p>''
''											{% if commonSettings.small_icon is defined and commonSettings.small_icon != "" %}''
''												<img src="{{ base_url }}{{ uploads_settings_dir }}{{ commonSettings.small_icon }}">''
''											{% endif %}''
''										</div>''

''										<div class="form-group  {{ set_field_error_tag("big_icon", ' has-error ') }}  col-md-6 only_md_lg_padding_left_md">''
''											<label for="big_icon">Big Icon<span class="required"> * </span></label>''
''											<input type="file" name="big_icon" id="big_icon" class="form-control editable_field "  onchange="javascript:selectionChanged(this, 'commonSettings')">''
''											<p class="help-block">Select big icon of site.</p>''
''											{% if commonSettings.big_icon is defined and commonSettings.big_icon != "" %}''
''												<img src="{{ base_url }}{{ uploads_settings_dir }}{{ commonSettings.big_icon }}">''
''											{% endif %}''
''										</div>''
''									</div>''

''									<div class="row ">''
''										<div class="form-group  {{ set_field_error_tag("new_users_needs_activating", ' has-error ') }}  col-md-6">''
''											<label for="new_users_needs_activating">New Users needs activating<span class="required"> * </span></label>''
''											<select id="new_users_needs_activating" name="new_users_needs_activating" class="form-control editable_field" onchange="javascript:selectionChanged(this, 'commonSettings')">''
''												<option value="">  -Select If New Users needs activating- </option>''
''												<option value="Y" {%if Settings.new_users_needs_activating is defined and Settings.new_users_needs_activating == "Y" %}selected{% endif %}> Yes, need activating </option>''
''												<option value="N" {%if Settings.new_users_needs_activating is defined and Settings.new_users_needs_activating == "N" %}selected{% endif %}> No, do not need activating </option>''
''											</select>''
''											<p class="help-block">If "Yes" selected, New Users, registered in system, needs activating by Admin.</p>''
''										</div>''
''									</div>''

''									<div class="row ">''
''										<div class="form-group  {{ set_field_error_tag("default_country", ' has-error ') }}  col-md-6">''
''											<label for="chosen_default_country">Default Country</label>''
''											<select id="chosen_default_country" name="chosen_default_country" class="form-control editable_field chosen_select_box" onchange="javascript:selectionChanged(this, 'commonSettings')">''
''												<option value="">  -Select Default Country-  </option>''
''												{% for next_country in countriesList %}''
''													<option value="{{ next_country.key }}" {% if Settings.default_country is defined and next_country.key == Settings.default_country %}selected{% endif %} >{{ next_country.value }}</option>''
''												{% endfor %}''
''											</select>''
''											<input type="hidden" value="" id="default_country" name="default_country">''

''											<p class="help-block">If country selected, New Users, registered in system, needs activating by Admin.</p>''
''										</div>''
''									</div>''

''								</fieldset> <!-- Common Settings -->''
