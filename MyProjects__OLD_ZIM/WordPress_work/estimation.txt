Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2016-12-24T07:37:13+02:00

Hi Zachary ,
Sorry for delay, in these New Year days it is not easy to find time.

I want to make estimation by subtasks with some questions I would like to get answer.
Some things can be implemented in different way and I suppose we would need some options in backend settings of site.
It could be usefull if you would need to change content of site dynamically or you would need install the different versions of site(with different content) on different hosting.


1) Main page http://www.launchfactory.com.au/dev/index.html:=
1.1) Common layout                                **-**     **4 hours**
**question a)** Do you need possibility in admin to upload/change big image at top? 

If yes in backend settings page we would need to make form for this image uploading, like this at demo : http://dev2.softreactor.com/wp-songs/wp-admin/admin.php?page=ArtistsSongs-artists-editor&action=edit&artist_id=2&paged=1&filter_name&filter_is_active&filter_registered_at_1&filter_registered_at_2&filter_with_photos&filter_with_inappropriate_photos&filter_with_reviews&filter_with_inappropriate_reviews&sort&sort_direction
with login : admin 111111
						  If yes                                   **-    2 hours**

1.2) Block "Baunch your startup smarter and faster."            **-    4 hours**
Posts of some specific category would be shown here
link "see our full list of startup innovation services" would open page with all posts of this category.
that woul give you possibilty to change posts/ordering in this block.  
**question b)** about icon. It is made as separate css-style to any post item and is related to css file.
that could be rather difficult to implement if admin would want to add new post with new icon. Usually thumbnail of post's image is used here.

**question c)**  that could be convinient to select in backend settings which category could be used for this block
						  If yes                                   **-    1 hour**
  
1.3) Block "Why work with us?" would be based on specific post  **-    2 hours**
**question d)**  that could be convinient to select in backend settings which post could be used for this block
											   If yes                                   **-    1 hour**
1.4) Block "Who have we helped?"
I am going to use banner-manager plugin in backend part and implement in frontend part  **-    2 hours**

1.5) Block "Meet our partners.". Also I think that only posts of special posts category would be used here.  **-    4 hours**
**question e)**  that could be convinient to select in backend settings which category could be used for this block
						  If yes                                   **-    1 hour**


1.6) Block "We knew that we had a winning idea, but we j..." " would be based on specific post  **-    2 hours**
**question f)**  that could be convinient to select in backend settings which post could be used for this block

1.7) Block "Latest Blog Posts" **-    2 hours**

1.8) Block "Interested? Get in touch" **-    2 hours**
**question g)**  Must be only only email sent to admin of site or do you want to save these messages in your database.
						  If yes                                   **-    6 hours**


2) Blogs   **-    3 hours**

3) Blogs   **-    2 hours**

4) About Page   **-    2 hours**
**question h)**  that could be convinient to select in backend settings which post could be used for About Page
If yes                                   **-    1 hour**


**question i)**   Is it ok that header at main page and header of the rest pages are different ?

**question j) I see links to **facebook,  icon-twitter, icon-linkedin. These settings also can be selected at Backend settings.
If yes                                   **-    2 hours**

**question j) I see links to ABOUT, SERVICES, BLOG, PRIVACY POLICY, CONTACT pages**. These settings also can be selected at Backend settings.
If yes                                   **-    2 hours**

If all my questions are clear ?

Summary :   30  + till 16 hours depending on which additive options you need.
My hour rate is $17.78 per .



Thanks!

== ======= ==
[10:36:00] Zachary Sequoia: http://www.launchfactory.com.au/dev/index.html
[10:36:20] Zachary Sequoia: http://www.launchfactory.com.au/dev/blog-page.html
[10:36:38] Zachary Sequoia: http://www.launchfactory.com.au/dev/blog-article.html
[10:37:05] Zachary Sequoia: http://www.launchfactory.com.au/dev/about.html
