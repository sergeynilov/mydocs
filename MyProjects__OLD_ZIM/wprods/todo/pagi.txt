Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2017-02-27T15:07:41+02:00

/**********************
 * In pagination by click on item link to hook the event and run js reloading of users
 * access public
 * params : none, all param of Page # is rid from linked url
 * return none
 *********************************/
function paginationLinksInit() {
	$(document).on('click', "div.table_pagination a",function(){  // LOAD ON PAGE LOAD AND ON CLICK
		var urls = $(this).attr("href");
		 // alert( "paginationLinksInit urls::"+var_dump(urls) )
		if ( urls== "#" ) {
			load_provides_vendors(1)
			load_related_users(1)
		}
		var arr = urls.split( '/clients_edit_load_provides_vendors/' );
		if (arr.length >= 2) {
			if ( urls== "#" ) {
				load_provides_vendors(1)
				return;
			}
			var value_arr = urls.split( '/page/' );
			if ( value_arr.length == 2 ) { // we have page number
				load_provides_vendors(value_arr[1])
			}
		}


		var arr = urls.split( '/clients_edit_load_related_users/' );
		if (arr.length >= 2) {
			if (urls == "#") {
				load_related_users(1)
				return;
			}
			var value_arr = urls.split('/page/');
			if (value_arr.length == 2) { // we have page number
				load_related_users(value_arr[1])
			}
		}

		return false;
	});
}





/**********************
 * List of related users reloading
 * access public
 * params : page - witch page must be loaded, the rest of parameters are read from inputs.
 * return none
 *********************************/
function load_related_users(page) {
	page= parseInt(page)
	// alert( "load_related_users  page::"+page )
	var sort_field_name= jQuery.trim( $("#sort_field_name").val() )
	if (sort_field_name == "") sort_field_name= 'username'
	var sort_direction = jQuery.trim( $("#sort_direction").val() )
	if (sort_direction == "") sort_direction= 'desc'

	var related_users_type= $("#select_related_users_type").val()
	var related_users_filter= $("#input_related_users_filter").val()
	var select_user_active_status= $("#select_user_active_status").val()
	if (select_user_active_status!= "") {
		select_user_active_status= "/user_active_status/"+select_user_active_status
	}
	var href= base_url+"sys-admin/clients_edit_load_related_users/filter_client_id/"+client_id+"/filter_related_users_type/"+related_users_type+"/sort/"+sort_field_name+"/sort_direction/"+sort_direction+select_user_active_status + "/page/" + page + "/filter_related_users_filter/"+related_users_filter
	$.ajax({
		url: href,
		type: 'GET',
		dataType: 'json',
		success: function(result) {
			if (result.ErrorCode == 0) {
				$('#div_load_related_users').html(result.html)
			}
		}
	});


}



	/**********************
	 * for client load list of related_users, with filters, sorting
	 * access public
	 * @params : filter_client_id - client_id, filter_related_users_filter - filter string by username, filter_related_users_type - users type of relation,
	 * user_active_status - filter by user active_status, sort/sort_direction - order and direction of resulting listing
	 * return list related users
	 *********************************/
	public function clients_edit_load_related_users()
	{
		$UriArray = $this->uri->uri_to_assoc(3);
		$post_array = $this->input->post();
		$filter_client_id = $this->common_lib->getParameter($this, $UriArray, $post_array, 'filter_client_id');
		$page = $this->common_lib->getParameter($this, $UriArray, $post_array, 'page');
		$filter_related_users_filter = $this->common_lib->getParameter($this, $UriArray, $post_array, 'filter_related_users_filter');
		$filter_related_users_type = $this->common_lib->getParameter($this, $UriArray, $post_array, 'filter_related_users_type');
		$db_filter_related_users_type= $filter_related_users_type;
		$user_active_status = $this->common_lib->getParameter($this, $UriArray, $post_array, 'user_active_status');
		$sort= $this->common_lib->getParameter($this, $UriArray, $post_array, 'sort', 'username');
		$sort_direction = $this->common_lib->getParameter($this, $UriArray, $post_array, 'sort_direction', 'desc');
		if ($filter_related_users_type == 'A') { // SHOW ALL USERS
			$db_filter_related_users_type= '';
		}
		if ( empty($page) ) $page= 1;


		$PageParametersWithSort = $this->clientsRelatedUsersPreparePageParameters($UriArray, $post_array, false, true);     // keep all sorting parameters for using in sorting
		$PageParametersWithoutSort = $this->clientsRelatedUsersPreparePageParameters($UriArray, $post_array, false, false); // by column header or at editor submitting to keep current filters

		$this->load->library('pagination');
		$pagination_config= $this->common_lib->getPaginationParams('ajax');
		$pagination_config['base_url'] = base_url() . 'sys-admin/clients_edit_load_related_users' . '/page';
		$filters= array('client_id'=>$filter_client_id, 'uc_active_status'=> $db_filter_related_users_type, 'show_uc_active_status'=> 1, 'username'=> $filter_related_users_filter, 'user_active_status'=> $user_active_status);
		$users_count = $this->admin_mdl->getUsersList( true, 0, $filters );

		$pagination_config['total_rows'] = $users_count;
		$this->pagination->suffix = $this->clientsRelatedUsersPreparePageParameters($UriArray, $post_array, false, true);

		$this->pagination->initialize($pagination_config);
		$this->pagination->cur_page= $page;
		$pagination_links = $this->pagination->create_links();
		$related_users_list= [];
		if ( $users_count > 0 ) {
			$related_users_list = $this->admin_mdl->getUsersList( false, $page, $filters, $sort, $sort_direction );
		}
		$data = array('related_users_list' => $related_users_list, 'client_id' => $filter_client_id, 'users_count'=> $users_count, 'related_users_type'=> $filter_related_users_type, 'related_users_filter'=> $filter_related_users_filter, 'sort_direction'=> $sort_direction, 'sort'=> $sort, 		'PageParametersWithSort'=> $PageParametersWithSort, 'PageParametersWithoutSort'=> $PageParametersWithoutSort,
 'pagination_links'=> 		$pagination_links   );
		$data['page']		= 'clients/load_related_users'; //page view to load
		$data['page_number']		= $page;
		$data['plugins'] 	= array();
		$data['javascript'] = array();
		$views				= array(  'design/page'  );
		ob_start();
		$this->layout->view($views, $data);
		$html = ob_get_contents();
		ob_end_clean();
		$this->output->set_content_type('application/json')->set_output(json_encode(array('ErrorMessage' => '', 'ErrorCode' => 0, 'client_id' => $filter_client_id, 'users_count'=> $users_count, 'html' => $html )));

	}
