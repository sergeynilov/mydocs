Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2017-02-10T16:15:17+02:00

								''{% if ''**'admin'**'' in ''**logged_user_groups_array**'' or 'superadmin' in logged_user_groups_array %}''
''								<div class="row">''

''									<fieldset >''
''										<legend>Debugging(<small>logged under ''**{{ logged_user_groups_label }}**'' </small>)</legend>''
''										<div class="col-xs-6">''
''											<button onclick="javascript:document.location='{{ base_url }}admin/dashboard/view_logs'" class="btn btn-primary btn-sm">View Logs</button>''
''										</div >''
''										<div class="col-xs-6">''
''											<button onclick="javascript:clearLogs(); return false; " class="btn btn-primary btn-sm">Clear Logs</button>''
''										</div >''
''									</fieldset>''

''								</div>''
''								{% endif %}''
