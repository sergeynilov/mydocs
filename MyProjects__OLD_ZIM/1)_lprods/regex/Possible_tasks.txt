Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2017-06-03T14:14:46+03:00

Details

Apple has a directory of several hundred thousand podcasts. We are looking for someone to create a database of podcasts that are listed in the Apple directory. The database needs the following fields, which are listed in the Apple directory or the podcast’s RSS feed:

1. Apple ID for podcast
2. <title>
3. <link>
4. <rss>
5. <itunes:owner><itunes:name>
6. <itunes:owner><itunes:email>
7. <itunes:category>
8. <rss><pubDate>
9. <item><title>
10. <item><link>
11. <item><pubDate>
12. Repeat 9-11 for all items that are in the RSS feed, going in reverse chronological order

Here is a listing of podcasts in the Apple directory: https://itunes.apple.com/us/genre/podcasts/id26?mt=2.

Apple has API access that allows search for podcasts in its directory: https://affiliate.itunes.apple.com/resources/documentation/itunes-store-web-service-search-api/

Final deliverable should be a csv file with one row for each podcast and columns for fields 1-n listed above. 


== ======== ==


We’re working on a tool (in PHP) to scrape lists published on the web for data to add to our database. One particular challenge has been site which dynamically load their content through Javascript or other methods.

Here are some example lists which work this way:
http://www.rollingstone.com/music/lists/100-greatest-hip-hop-songs-of-all-time-w435246
http://collider.com/best-horror-movies-of-2017-so-far/
http://uproxx.com/smokingsection/12-rap-songs-to-live-your-life-by/
http://www.refinery29.com/2017/06/157176/best-tv-shows-of-all-time-critics-reviews

We’re interested in two types of data - the titles of each list item (titles of songs, places, books, movies, etc.) and the artist/author for music and book lists. In addition, we don’t want to just scrape one list per publisher. We would also like to crawl through the publisher’s directory and auto-extract data from any list that is found. Here are some example directory pages:
http://www.rollingstone.com/lists
http://collider.com/?s=the+best
http://uproxx.com/topic/lists/
http://www.refinery29.com/entertainment?page

Ideally, we could run a script to output the extracted data from each list found, plus the list title and original URL, as an XML file.

Please provide an estimate on how much time - in hours - you would require to accomplish this task for the example publishers above and whether there are any challenges you anticipate. Based on the feedback we receive, we are aiming to make a decision early next week.

Looking forward to hearing from you. 



===================================


I have a list of 8-digit numbers that are TCIN identifiers from Target stores. For example, some numbers are:
14592622
16876577
16541434
16876589
14592629
15446711
16984596

From these numbers, I would like to get the Title and Description of the product. A successful result would be to document a way to get information from any 8-digit TCIN number.

Please note that the numbers above are no longer sold at Target, however, there are many sites that have gathered the information, such as:

ShopSavvy.com, ShopCade.com, PolyVore.com, blinq.com, etc.

The images for these items are easily retrievable, such as:
https://target.scene7.com/is/image/Target/14592622

The product information, however, results in "Product Not Available", i.e.
http://www.target.com/p/-/A-14592622

The Target web-site is constructed dynamically from RedSky, which returns a JSON result. For items which are still sold, the result is easily parsed. For items no longer for sale, RedSky returns a 404.

This job requires some investigation to find out a method to get the descriptions and title given a TCIN number of a product.  

For those familiar with JSON, dynamically created web pages, web crawling, etc. you should be able to find the information on the web and document how to do it. I don't need a program, just a simple list of commands, such as:

Navigate to:  someweb-site.com
Open the developers console in Google chrome.
etc.

The end result should be the text of the item's title and description.

The person should know (or at least be familiar with) JavaScript and HTML (as one would probably have to know these things to get the information).

Provide a title and description of the 7 TCINs above and the job is yours. I will pay you, and then you can release the details of how you got the information. 


== ============================ ==









