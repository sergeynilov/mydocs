Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2016-11-28T06:59:54+02:00

Tasks will be assigned to a developer, or a developer may choose to work on a task.
When a task is started, the task/card should be moved to the In Progress list. Do not delete or archive any of the cards
Any issues, comments or questions about the task should be listed in the comment section of the card with myself @xntral tagged.
When the task is complete and ready for review, the developer should post a link to the work completed (git, dropbox, fileshare, etc). Leave a comment for the number of hours it took to complete the task, and tag members of the group. Then move the card to the "Review" list and the completed work will be reviewed. Do not upload any work to trello, just link to it.
The work will be reviewed and moved to "Completed" list or returned to "In Progress" list if changes or corrections are needed.
