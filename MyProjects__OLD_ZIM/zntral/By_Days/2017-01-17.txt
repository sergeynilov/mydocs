Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2017-01-14T14:47:08+02:00

most recent at the top?
 For clients and users - yes

User Group : only 1 value can be selected - it defines with which access logged in backend system, as with different access user can has access to differtent pages.
User's job titles : can has several options selected. It has nothing with login and access, more like logical functions of user.

-------------
[15:14:17] Rod: There should only be one. The user can have more than one title but the user can only be logged in with one title per session

	Combine "User's job titles" with the "User Group". So users job titles should go into user group. The options should be listed alphabetically.
	Remove "User's job titles *"
	Change the name "User Group" to "Title(s)" and change it into a checkbox (multiple selections)


et's narrow down the selection to the following checkbox:

	System Admin
	Super User
	Administration
	Aide
	Doctor
	Quality Assurance
	Registered Nurse
	Social Worker
	Spiritual counselor
	Vocational Nurse
	Volunteer

These titles should have values with the names pointing to a language file that can be client-specific. The reason for this is that "Vocational Nurse" is sometimes called Licensed Vocational Nurse or Licensed Practical Nurse.

In the future we will create sub-titles for each selection and access level will be dependent on the sub-titles. Example: Administration can have sub-title of Accountaint or CEO and the access will depend on sub-title.. For now, this will be the main group.

Question: Should there be a separate section that will list the different titles? This section would also manage the sub-titles.

The plan is to grant system admin (admin@admin.com) full access to the website then restrict the different groups as needed, correct? The titles will have access to a list of forms. Then different titles may only have read access to that information. This will be detailed later.

**I remade "User's job titles"**
>>> These titles should have values with the names pointing to a language file that can be client-specific. The reason for this is that "Vocational Nurse" is sometimes called Licensed Vocational Nurse or Licensed Practical Nurse.
Working with code I see that site is multilanguage and I try to follow to this, so that later other language  files could  be added to project and make it multilanguage.
But when you enter some data in pages of admin part and want it multilanguage, we do not need separate language files for it, but enter labels in other languages in backend part, 
something like one of my prior multilanguage site
http://s017.radikal.ru/i442/1201/8f/4046edd4dd5d.png
also this multilanguage functionality could be made in [[/sys-admin/users-role]] 
page, for any [[language]] in system. I think we need to move to it later.

>>> Question: Should there be a separate section that will list the different titles? This section would also manage the sub-titles.
Sure, if any title has several sub-titles, it also could be added at [[/sys-admin/users-role]]  editor, but this page must be remade for this,

>>> The plan is to grant system admin (admin@admin.com) full access to the website then restrict the different groups as needed, correct? The titles will have access to a list of forms. Then different titles may only have read access to that information. This will be detailed later.
Yes.

== ================ ==

 >>>    Please review Client Status at https://zntral.net/dox/clients.php#clientstatus . Creating a new client, the status will automatically be set to **pending** , so nothing should be set by admin@admin so Client Active Status can be removed. When the client information is already in the website, the status should be pending and admin@admin can change the status after the client information is in the website. When the client status is set to inactive, all users will not have access to the website and the users will be redirected to another page, contact zntral.com or something like that.
As I see current "New" status must be changed to **pending, as for "redirected to another page" for inactive I think that MUST NOT BE IMPLEMENTED for sys-admin, as I do now?**
~1 hour

>>>       Remove "Color Scheme *" on the new client page clients-edit/new/ This will be set by the client superuser, not system admin. All new users will have the default layout so no selection is needed. The "Theme color" will later be moved to the client overview page.   replace the color scheme with a temporary notification box. this will be used as a placeholder
Ok for new mode, but in edit mode that could be editable input selection?
~1 hour

>>>       At the bottom, add a button called "Save and Create Superuser". This button will save the client information and redirect to the New user page. On the New user page, there is no client name here but the new user will automatically belong to the new client that was just created. This will be a temporary method until the "Related Users" dialog box is fixed. Hopefully this method does not take very long to implement. Please let me know if it does.
~2-3 >>>   Next: Review the list, enter some feedback comment and include an estimated time frame it will take to complete. When you start, drag this task to "In Progress"


Also please answer some my questions I asked before.

I think now is time to implement this task and add all email template to email we sends(user registragion, activation, adding user to client, forget password). You see how email looks like, with this editor
admin could edit content of these email and emails could have good looking view.



== ===== ==


@sergei131 Here's your next task:

	Please review Client Status at https://zntral.net/dox/clients.php#clientstatus . Creating a new client, the status will automatically be set to pending so nothing should be set by admin@admin so Client Active Status can be removed. When the client information is already in the website, the status should be pending and admin@admin can change the status after the client information is in the website. When the client status is set to inactive, all users will not have access to the website and the users will be redirected to another page, contact zntral.com or something like that.
	Remove "Color Scheme *" on the new client page clients-edit/new/ This will be set by the client superuser, not system admin. All new users will have the default layout so no selection is needed. The "Theme color" will later be moved to the client overview page.
	At the bottom, add a button called "Save and Create Superuser". This button will save the client information and redirect to the New user page. On the New user page, there is no client name here but the new user will automatically belong to the new client that was just created. This will be a temporary method until the "Related Users" dialog box is fixed. Hopefully this method does not take very long to implement. Please let me know if it does.

Next: Read this list, enter some feedback and an estimated time frame it will take to complete. When you start, drag this task to "In Progress"




@sergei131 The priority is making sure users can log in and the overview pages are ready.



@sergei131

		As I see current "New" status must be changed to pending,

Yes, I added new information on https://zntral.net/dox/clients.php#clientstatus - clients that are new will have a "pending" status. This is not set by system admin. It is automatic by website.

		as for "redirected to another page" for inactive I think that MUST NOT BE IMPLEMENTED for sys-admin, as I do now?
		~1 hour

Yes. "All users of an inactive client will be redirected to another page with a template". Later we will do this: All users will be able to log in but will not be able to make any changes.



q) 1) If email sent as html 
