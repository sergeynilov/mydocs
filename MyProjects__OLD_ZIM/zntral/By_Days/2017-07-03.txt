Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2017-06-03T07:38:42+03:00



Rod, you have my questions/comments on Cards : "PRIORITY LIST 2", "User / super user can log in client environment", "Design issues to fix", maybe some more.




===============


I fixed errors with sorting in [[/sys-admin/users/users-view/]] page.

10) For sorting lists of data next function can be used in Listing view page:
...
								<th><?= $this->common_lib->showListHeaderItem ( '/sys-admin/users/users-view', $page_parameters_without_sort, lang('title'), "user_group_description", $sort_direction, $sort ) ?></th>
								<th><?= $this->common_lib->showListHeaderItem ( '/sys-admin/users/users-view', $page_parameters_without_sort, lang('created'), "users.created_at", $sort_direction, $sort ) ?></th>
...
Pay attention at 4th parameter of showListHeaderItem method: it must name of sql field in resulting set, depending of source. Read item below to specify which coulmns are used in data source.




== == ==
11) The system permits to trace all sql-statements to log file.
For this in application/config/config.php put lines like :
...
if ((!empty($_SERVER["HTTP_HOST"]) and !(strpos($_SERVER["HTTP_HOST"], "local-zntral-dev.com") === false))) {
	$config['log_path'] = '/_wwwroot/zntral/Dev/logs/'; 
	$config['document_root'] = '/_wwwroot/zntral/Dev/';
	$config['sql_queries_to_file'] = '/_wwwroot/zntral/Dev/logs/sql_queries_to_file_';
	$config['base_url'] = 'http://local-zntral-dev.com/';
	$config['is_developer_comp'] = 1;
	$config['log_threshold'] = 4;
//    die("-1 XXZ");

} else {
...
where local-zntral-dev.com hosting under which you need to get  to trace all sql-statements 
and $config['sql_queries_to_file'] is root where trace file must be saved.
Also in system must be modified system/database/DB_driver.php file



== == ==

Karina , 
I am not sure what raised your question ?
If I client editor https://devk.zntral.net/sys-admin/client/26/
create new user : username : Nilov_Sergey, with email nilov@softreactor.com, Dictor title
On saving data listing of all users would be refreshed and newly created user must be in Associations Listing below.
If you have acccess to phpmyadmin to could trace this user in users table with ID#49.
also 1 row is added in `users_groups` table with user_id= 49 - this row specified which title has the user
also 1 row is added in `users_clients`table with uc_user_id= 49 - this row specified to which client any user belongs.
Also please read in Trello Card "Valid application construction" point 11) - it could be usefull if you need to check which sql works on data saving or data retrieving.
Have any more questions ?
Also Karina , in "New user for" dialog, please :
1) now for username, first_name, Verify title are the same icons. I think they must be different?
2) Rod gave task to check valid username in  "New user for" dialog(like no special chars etc...)
Look at assets/custom/admin/client_overview_methods.js file, there are lines :
	$("#form_user_modal_editor_username").rules("add", {
		required: true,
		minlength: 5
		// specialChrs: true
		// regex: /[A-Za-z]+/
		// regex: /^[a-zA-Z\s]+$/
	});
I tried to set these checks, but failed, as I got JS error in my browser. Please try to add these validations.

3)  as for this >>> Fix the avatar uploading function. The image / avitar should appear on the side navigation
I can make it, as uploading of images in Codeigniter is specific.
Rod, please remind on which page image is loaded to user ? on https://devk.zntral.net/sys-admin/users/users-overview/62/ ?
Does any other developer works with user editor ?


users-view
