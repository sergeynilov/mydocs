Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2017-04-26T18:31:11+03:00

So what we can start from? 
1) New client Popup Dialog :
  I see we need validation add here, but we can postpone it.
2) Client Editor for existing client as written here https://trello.com/c/r8aYI9xY 
first what I need to do that is to split current client editor page into 4 blocks :
'design/html_topbar','sidebar','design/page','design/html_footer'
as any page consists of these 4 blocks and the page(client editor).
In other words I have remake new design into parts as it is set in the app.


>It needs to be connected to db. This is for the user overview page: https://trello.com/c/r8aYI9xY I found materializecss.com and I really like the design. Also, it has everything ready so a lot of design and functionality work is already done. It only needs to be modified a little. Karina is helping with the new front end development but I can have some smaller tasks going at the same time. 

A similar method will apply to editing client and other entities.



============================

After quick review  I think first we need is finish client editor with all data.
What actually surprise me that some parts are hardcoded and some functions do not work.
That could be like so :
1) >>> 1) Quick access to client address, phone numbers, important information
1.1) Now only client name, address and phone are shown. I can make that clicking on button "Down" some area would be opened and ALL client's information would be visible.
1.2) At right corner clicking on "Edit" button would be opened modal dialog with ALL clients data we need to edit for client with validation.
But in this case we would need to remake the page a lot : I mean if saving of client's data  would be made with ajax and without page reloading...

2) >>>It is all placeholders. ref: http://imgur.com/a/muRP3
But do I need to replace it with client's data ?
3) >>> Quick access to client address, phone numbers, important information
Yes, links at rigtht

4) >>> 3) +USER button = adds a new user to the current clientYes, 

5) >>> 4) Users, Patients, Vendors will be listed in tabs. If client users = 0, there will be no users listed here.But how better to make it ? I mean in prior my implementation these tabs were below of client editor. Now The same  below of client editor?
Also I can propose to install my prior version at 1 of your hosting and use it as sample. That could be helpfull, at least for me.
I can send archive to your admin.


>>> Creating client superusers and users

========================
… I created new client : https://dev.zntral.net/sys-admin/clients-edit/24
entering fields for new client there is no any data validation, like invalid email or zip can be entered...

>Data validation will be added later


In opened page http://imgur.com/a/muRP3 - look I opened right page?

>Yes


1) I see info on current client img1) but I did not find how to edit fields of existing client?

>When super user logs in (not working), super user can modify the client information. There will be a edit icon / button to modify the information. When super user clicks on the button, it will load a page will all values and options available.


2) I see links to different area img2), img4), but I see that some dummy data are hardcoded (Associations, Activities) and has nothing with real data of existing client.

>It is all placeholders. ref: http://imgur.com/a/muRP3
1) Quick access to client address, phone numbers, important information
2) Quick links to different sections on the page
3) +USER button = adds a new user to the current client
"Pinned" = activity logs can be pinned. If pinned, it will appear in "Pinned" section. If no pins, "Pinned" section will be hidden. Details later
4) Users, Patients, Vendors will be listed in tabs. If client users = 0, there will be no users listed here.


3) By clicking on "+User" button img3) modal dialog is opened, but entered data are saved as NEW CLIENT, but NOT USER OF THIS CLIENT. also there are no data validation.
Also I suppose that there are must be list of client users in similar design?

>Modal dialog is not supposed to work. It is only functional mockup for now. "+User" button will add new "pending" user to client indicated on current client overview page. More info on new user process: http://zntral.net/dox/users.php#new-super-user


4) New activity seems can be added mannually (looks it is added but not reflected in activity listing - as it is hardcoded), but from prior tasks I wander wht for add it mannually?

>Plan: http://zntral.net/dox/overview.php#overview-activity
- Activity log with white background = user activity
- Activity log with no background = system activity


5) Filtering in clients page is opened but filtering dows not work at all. also must be checked.

>Postponed task. User login is priority.


As I whole I see new design for client, 

>New design is for system administrator account. Client function will be different. 


where adding of data was supposed in popup modal ? 

>Not sure what you mean


Did task was changed from the tasks when I worked over it?

>Yes. 


From this https://zntral.net/dox/users.php#user-status looks like similar what I read in my time, but I did not come to this part...

>Correct. Current priority tasks: 
1. Creating client superusers and users
2. User Status
3. Client user / super user is able to log in to client area
4. Client users and super users can modify their own information


But functionality of client editor is strange for me now in many parts...

>It is my vision :)

======================

I created new client : https://dev.zntral.net/sys-admin/clients-edit/24
entering fields for new client there is no any data validation, like invalid email or zip can be entered...
In opened page http://imgur.com/a/muRP3 - look I opened right page? : 
1) I see info on current client img1) but I did not find how to edit fields of existing client?
2) I see links to different area img2), img4), but I see that some dummy data are hardcoded (Associations, Activities) and has nothing with real data of existing client.
3) By clicking on "+User" button img3) modal dialog is opened, but entered data are saved as NEW CLIENT, but NOT USER OF THIS CLIENT. also there are no data validation.
Also I suppose that there are must be list of client users in similar design?
4) New activity seems can be added mannually(looks it is added but not reflected in activity listing - as it is hardcoded), but from prior tasks I wander wht for add it mannually?
5) Filtering in clients page is opened but filtering dows not work at all. also must be checked.

As I whole I see new design for client, where adding of data was supposed in popup modal ? Did task was changed from the tasks when I worked over it?
From this https://zntral.net/dox/users.php#user-status looks like similar what I read in my time, but I did not come to this part...
But functionality of client editor is strange for me now in many parts...
/////////
 Hi Sergey. I do not think we use dev4. Hold on-
[07:45:29] live:rsdelarosa_3: Read this: http://zntral.net/dox/trello.php#dev


==============
Hi Rod, I downloaded working Dev version and try to review current functions of site and what I have to do.
I  have some questions to clarify:
is https://dev4.zntral.net/sys-ad[[https://dev4.zntral.net/sys-adm|m]][[https://dev4.zntral.net/sys-admi|i]][[https://dev4.zntral.net/sys-admin|n]] working branch ?
opening https://dev4.zntral.net/sys-admin/clients-view/ and clicking on new client dialog popup I see next :
http://imgur.com/a/HDV67

Area for client inpiuts is empty.

My my local version (uploaded from Dev branch ) is a bit different.
Locally I created new client but was surprised, as I hardly could work (edit/view) this new client.

I just want to be sure if https://dev4.zntral.net/sys-admin/clients-view/ is  current project ?

1) Filter in clients page https://dev4.zntral.net/sys-admin/clients-view opened but filtering dows not work at all?

nilov@softreactor.com

== ========= ==

USER STATUS

Documentation: 
https://zntral.net/dox/users.php#user-status

Client super user status process flow:
https://zntral.net/dox/users.php#client-super-users-status

Client user status process flow
https://zntral.net/dox/users.php#client-users-status

---------------

Then, send estimated time frame to complete it along with any dependencies. 

Currently, system administrator user (me) is able to create clients. Someone else is working on setting up new users and super users. 
Another developer is helping with the front end. So the users module or feature is not entirely working at this moment.
[18:30:06] live:rsdelarosa_3: Main branch is "dev"
''
