Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2017-06-05T09:13:19+03:00

'''
client_img

        $update_data= array(
            'client_name' => $post_array['data']['client_name'],
            'client_img' => $post_array['data']['client_img'],
            //'clients_types_id' => $post_array['data']['clients_types_id'],
            'clients_types_id' => $post_array['group1'],
            'client_name' => $post_array['data']['client_owner'] ,
            'client_address1' => $post_array['data']['client_address1'] ,
            'client_address2' => $post_array['data']['client_address2'] ,
            'client_city' => $post_array['data']['client_city'] ,
            'client_state' => $post_array['data']['client_state'] ,
            'client_zip' => $post_array['data']['client_zip'],
            'client_phone' => $post_array['data']['client_phone'],
            'client_phone_2' => $post_array['data']['client_phone_2'],
            'client_phone_3' => $post_array['data']['client_phone_3'],
            'client_phone_4' => $post_array['data']['client_phone_4'],
            'client_phone_type' => $post_array['data']['client_phone_type'],
            'client_fax' => $post_array['data']['client_fax'] ,
            'client_email' => $post_array['data']['client_email_first'] ,
            'client_website' => $post_array['data']['client_website']  ,
            'color_scheme' => $color_scheme
        );

        $original_client_img= !empty($post_array['data']['client_img']) ? $post_array['data']['client_img'] : '';

        if  (  !empty( $post_array['cbx_clear_image'])  )  {
            $update_data['client_img']= '';
        }

        if ( !empty( $_FILES['data']['name']['client_img_file_upload'] ) ) {
            $update_data['client_img']= $_FILES['data']['name']['client_img_file_upload'];
        }


        if ( $is_insert ) {
            $this->db->insert($this->clients_mdl->m_clients_table, $update_data);
            $cid= $this->db->insert_id();
        } else {
            $this->db->where( $this->clients_mdl->m_clients_table . '.cid', $cid);
            $this->db->update($this->clients_mdl->m_clients_table, $update_data);
        }

        $client_img_path= $this->clients_mdl->getClientImagePath($cid, $post_array['data']['client_img']);
        $client_dir= $this->clients_mdl->getClientDir($cid);
        if (  !empty( $post_array['cbx_clear_image']) or !empty($_FILES['data']['name']['client_img_file_upload'])  )   {
            $original_img_path= $this->clients_mdl->getClientImagePath($cid, $original_client_img);
            if ( !empty($original_img_path) and file_exists($original_img_path) and !is_dir($original_img_path)) {
                unlink($original_img_path);
            }
        }


        $clientImagesDirs = array( FCPATH . 'uploads', $this->clients_mdl->getClientsDir(), $this->clients_mdl->getClientDir($cid) );
        $src_filename = $_FILES['data']['tmp_name']['client_img_file_upload'];
        $img_basename = $_FILES['data']['name']['client_img_file_upload'];

        $this->common_lib->createDir($clientImagesDirs);
        $ret = move_uploaded_file( $src_filename, $this->clients_mdl->getClientDir($cid) . $img_basename );

        if ($select_on_update == 'reopen_editor') {
//			$redirect_url = base_url() . 'sys-admin/client/' . $cid . $page_parameters_with_sort;
            $redirect_url = base_url() . 'sys-admin/clients-view/'. $cid . $page_parameters_with_sort;
        }
        if ($select_on_update == 'open_editor_for_new') {
//			$redirect_url = base_url() . 'sys-admin/client/new' . $page_parameters_with_sort;
            $redirect_url = base_url() . 'sys-admin/clients-view/' . $page_parameters_with_sort;
        }
        $redirect_url = base_url() . 'sys-admin/client/' . $cid;
        if ($cid) {
            $this->session->set_flashdata('editor_message', lang('client') . " '" . $post_array['data']['client_name'] . "' was " . ($is_insert ? "inserted" : "updated") );
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();
            }
//		die("-1 XXZ");
            redirect($redirect_url);
            return;
        }
'''

